package cl.bci.aplicaciones.productos.servicios.transferenciamonex.to;

import java.io.Serializable;
import java.util.Date;

import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.ConsultaTransferenciaTO;

/**
 * Informaci�n de la transferencia.
 * <p>
 * Registro de Versiones :
 * <ul>
 * <li>1.0 04/10/2013, Mauricio Flores S�enz(Imagemaker IT): Versi�n inicial</li>
 * <li>1.1 28/01/2014, Mauricio Flores S�enz(Imagemaker IT): Incorporacion de nuevos atributos.</li>
 * <li>1.2 07/05/2014, Angel Cris�stomo (Imagemaker IT): Incorporacion de los atributos fechaHoy
 *       y tipoTransferencia.</li>
 * <li>1.3 05/11/2015 Oscar Nahuelpan (SEnTRA) - Heraldo Hernandez (Ing. Soft. BCI): Se agrega los atributo.</li>
 * <li>1.4 25/01/2019 Eneida Diaz (Everis) - Luz Carre�o (Ing. Soft. BCI): Se agregaron atributos cuentaCargoComisionWeb y monedaCuentaComision.
 * 																		   Modificaci�n de m�todo {@link #toString()} para incluir los nuevos atributos.
 * 																		   Se agrega javadoc para m�todo {@link #toString()}. </li>
 * <li>1.5 25/03/2019, Sergio Bustos B. (Everis) - Minheli Mejias (Ing. Soft. BCI): Se agregan atributos para obtener valores relacionados a transferencias Ripple.</li>
 * </ul>
 * </p>
 * <p>
 * <B>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</B>
 * </p>
 */
public class TransferenciaTO implements Serializable {

    /**
     * Id de serializaci�n.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Entidad banco beneficiario.
     */
    private BancoTO bancoBeneficiario = new BancoTO();

    /**
     * Entidad banco intermediario.
     */
    private BancoTO bancoIntermediario = new BancoTO();

    /**
     * Entidad beneficiario.
     */
    private BeneficiarioTO beneficiario = new BeneficiarioTO();

    /**
     * Cuenta cargo comisi�n.
     */
    private CuentaMonexTO cuentaCargoComision = new CuentaMonexTO();

	/**
	* Cuenta cargo comisi�n Web.
	*/
	private String cuentaCargoComisionWeb;    

  	/**
  	* Moneda Cuenta Cargo Comisi�n.
  	*/
  	private String monedaCuentaComision;  	
	
    /**
     * Cuenta de origen.
     */
    private CuentaMonexTO cuentaOrigen = new CuentaMonexTO();
    
    /**
     * Codigo de cambio.
     */
    private CodigoValor codigoCambio = new CodigoValor("", "");
    
    /**
     * Gasto Banco Exterior.
     */
    private CodigoValor gastoBancoExterior = new CodigoValor("", "");

    /**
     * Fecha valuta.
     */
    private CodigoValor valuta = new CodigoValor("", "");

    /**
     * Codigo de estado de la transferencia.
     */
    private String codigoEstado;
    
    /**
     * Estado de transferencia.
     */
    private String estado;

    /**
     * Fecha de creaci�n o actualizaci�n de la transferencia.
     */
    private String fechaActualizacion;

    /**
     * Fecha de Operacion.
     */
    private Date fechaOperacion;
    
    /**
     * Modalidad de operaci�n.
     */
    private String modalidadOperacion;

    /**
     * Monto a transferir.
     */
    private Double montoTransferir;

    /**
     * Detalle de motivo de pago.
     */
    private String motivoPago;

    /**
     * Saldo disponible.
     */
    private Double saldoDisponible;

    /**
     * Tipo de usuario.
     */
    private String tipoUsuario;
    
    /**
     * ID Correlativo Interno.
     */
    private int correlativoInterno;

    /**
     * Flag para habilitar o deshabilitar los enlaces de editar y/o eliminar para los perfiles Apoderado y 
     * Operador.
     */
    private boolean disabledButton;
    
    /**
     * Flag de habilitaci�n o deshabilitaci�n de enlaces autorizar para perfil Apoderado.
     */
    private boolean renderAutorizar;
    
    /**
     * Fecha del Hoy.
     */
    private String fechaHoy;
    
    /**
     * Flag tipo transferencia PENDIENTE o REALIZADA.
     */
    private String tipoTransferencia;
    
    /**
     * Contiene el filtro para consulta.
     */
    private ConsultaTransferenciaTO consulta;
    
	/**
	 * Indicador resultado operacion.
	 */
	private int indicadorResultado;
	
	/**
	 * Mensaje resultado operacion.
	 */
	private String mensajeResultado;
	
	/**
	 * Numero de operacion.
	 */
	private String numeroOperacion;
	
	/**
	 * Codigo Moneda de la ITF.
	 */
	private String codigoMonedaITF;
	
	/**
	 * Cuenta Origen de fondos.
	 */
	private String cuentaOrigenFondos;
	
	/**
	 * Codigo identificador del Beneficiario.
	 */
	private String codigoIdentificadorBene;
	
	/**
	 * Cuenta Corriente del beneficiario.
	 */
	private String cuentaCorrienteBene;
	
	/**
	 * SysTimeStamp.
	 */
	private String sysTimeStamp;
    

    public ConsultaTransferenciaTO getConsulta() {
        return consulta;
    }

    public void setConsulta(ConsultaTransferenciaTO consulta) {
        this.consulta = consulta;
    }

    public BancoTO getBancoBeneficiario() {
	return bancoBeneficiario;
    }

    public BancoTO getBancoIntermediario() {
	return bancoIntermediario;
    }

    public BeneficiarioTO getBeneficiario() {
	return beneficiario;
    }

    public CodigoValor getCodigoCambio() {
	return codigoCambio;
    }

    public int getCorrelativoInterno() {
	return correlativoInterno;
    }

    public CuentaMonexTO getCuentaCargoComision() {
	return cuentaCargoComision;
    }

    public CuentaMonexTO getCuentaOrigen() {
	return cuentaOrigen;
    }

    public String getEstado() {
	return estado;
    }

    public String getFechaActualizacion() {
	return fechaActualizacion;
    }

    public Date getFechaOperacion() {
	return new Date(fechaOperacion.getTime());
    }

    public CodigoValor getGastoBancoExterior() {
	return gastoBancoExterior;
    }

    public String getModalidadOperacion() {
	return modalidadOperacion;
    }

    public Double getMontoTransferir() {
	return montoTransferir;
    }

    public String getMotivoPago() {
	return motivoPago;
    }

    public Double getSaldoDisponible() {
	return saldoDisponible;
    }

    public String getTipoUsuario() {
	return tipoUsuario;
    }

    public CodigoValor getValuta() {
	return valuta;
    }

    public void setBancoBeneficiario(BancoTO bancoBeneficiario) {
	this.bancoBeneficiario = bancoBeneficiario;
    }

    public void setBancoIntermediario(BancoTO bancoIntermediario) {
	this.bancoIntermediario = bancoIntermediario;
    }

    public void setBeneficiario(BeneficiarioTO beneficiario) {
	this.beneficiario = beneficiario;
    }

    public void setCodigoCambio(CodigoValor codigoCambio) {
	this.codigoCambio = codigoCambio;
    }

    public void setCorrelativoInterno(int correlativoInterno) {
	this.correlativoInterno = correlativoInterno;
    }

    public void setCuentaCargoComision(CuentaMonexTO cuentaCargoComision) {
	this.cuentaCargoComision = cuentaCargoComision;
    }

    public void setCuentaOrigen(CuentaMonexTO cuentaOrigen) {
	this.cuentaOrigen = cuentaOrigen;
    }

    public void setEstado(String estado) {
	this.estado = estado;
    }

    public void setFechaActualizacion(String fechaActualizacion) {
	this.fechaActualizacion = fechaActualizacion;
    }

    public void setFechaOperacion(Date fechaOperacion) {
	this.fechaOperacion = new Date(fechaOperacion.getTime());
    }

    public void setGastoBancoExterior(CodigoValor gastoBancoExterior) {
	this.gastoBancoExterior = gastoBancoExterior;
    }

    public void setModalidadOperacion(String modalidadOperacion) {
	this.modalidadOperacion = modalidadOperacion;
    }

    public void setMontoTransferir(Double montoTransferir) {
	this.montoTransferir = montoTransferir;
    }

    public void setMotivoPago(String motivoPago) {
	this.motivoPago = motivoPago;
    }

    public void setSaldoDisponible(Double saldoDisponible) {
	this.saldoDisponible = saldoDisponible;
    }

    public void setTipoUsuario(String tipoUsuario) {
	this.tipoUsuario = tipoUsuario;
    }

    public void setValuta(CodigoValor valuta) {
	this.valuta = valuta;
    }
    
    public boolean isDisabledButton() {
        return disabledButton;
    }

    public void setDisabledButton(boolean disabledButton) {
        this.disabledButton = disabledButton;
    }
    
    public boolean isRenderAutorizar() {
        return renderAutorizar;
    }

    public void setRenderAutorizar(boolean renderAutorizar) {
        this.renderAutorizar = renderAutorizar;
    }
    
    public String getCodigoEstado() {
        return codigoEstado;
    }

    public void setCodigoEstado(String codigoEstado) {
        this.codigoEstado = codigoEstado;
    }
    
    public String getFechaHoy() {
        return fechaHoy;
    }

    public void setFechaHoy(String fechaHoy) {
        this.fechaHoy = fechaHoy;
    }

    public String getTipoTransferencia() {
        return tipoTransferencia;
    }

    public void setTipoTransferencia(String tipoTransferencia) {
        this.tipoTransferencia = tipoTransferencia;
    }

	public String getCuentaCargoComisionWeb() {
		return cuentaCargoComisionWeb;
	}

	public void setCuentaCargoComisionWeb(String cuentaCargoComisionWeb) {
		this.cuentaCargoComisionWeb = cuentaCargoComisionWeb;
	}
	
	public String getMonedaCuentaComision() {
		return monedaCuentaComision;
	}

	public void setMonedaCuentaComision(String monedaCuentaComision) {
		this.monedaCuentaComision = monedaCuentaComision;
	}

	/**
	 * @return the indicadorResultado
	 */
	public int getIndicadorResultado() {
		return indicadorResultado;
	}

	/**
	 * @return the mensajeResultado
	 */
	public String getMensajeResultado() {
		return mensajeResultado;
	}

	/**
	 * @return the numeroOperacion
	 */
	public String getNumeroOperacion() {
		return numeroOperacion;
	}

	/**
	 * @return the codigoMonedaITF
	 */
	public String getCodigoMonedaITF() {
		return codigoMonedaITF;
	}

	/**
	 * @return the cuentaOrigenFondos
	 */
	public String getCuentaOrigenFondos() {
		return cuentaOrigenFondos;
	}

	/**
	 * @return the codigoIdentificadorBene
	 */
	public String getCodigoIdentificadorBene() {
		return codigoIdentificadorBene;
	}

	/**
	 * @return the cuentaCorrienteBene
	 */
	public String getCuentaCorrienteBene() {
		return cuentaCorrienteBene;
	}

	/**
	 * @return the sysTimeStamp
	 */
	public String getSysTimeStamp() {
		return sysTimeStamp;
	}

	/**
	 * @param indicadorResultado the indicadorResultado to set
	 */
	public void setIndicadorResultado(int indicadorResultado) {
		this.indicadorResultado = indicadorResultado;
	}

	/**
	 * @param mensajeResultado the mensajeResultado to set
	 */
	public void setMensajeResultado(String mensajeResultado) {
		this.mensajeResultado = mensajeResultado;
	}

	/**
	 * @param numeroOperacion the numeroOperacion to set
	 */
	public void setNumeroOperacion(String numeroOperacion) {
		this.numeroOperacion = numeroOperacion;
	}

	/**
	 * @param codigoMonedaITF the codigoMonedaITF to set
	 */
	public void setCodigoMonedaITF(String codigoMonedaITF) {
		this.codigoMonedaITF = codigoMonedaITF;
	}

	/**
	 * @param cuentaOrigenFondos the cuentaOrigenFondos to set
	 */
	public void setCuentaOrigenFondos(String cuentaOrigenFondos) {
		this.cuentaOrigenFondos = cuentaOrigenFondos;
	}

	/**
	 * @param codigoIdentificadorBene the codigoIdentificadorBene to set
	 */
	public void setCodigoIdentificadorBene(String codigoIdentificadorBene) {
		this.codigoIdentificadorBene = codigoIdentificadorBene;
	}

	/**
	 * @param cuentaCorrienteBene the cuentaCorrienteBene to set
	 */
	public void setCuentaCorrienteBene(String cuentaCorrienteBene) {
		this.cuentaCorrienteBene = cuentaCorrienteBene;
	}

	/**
	 * @param sysTimeStamp the sysTimeStamp to set
	 */
	public void setSysTimeStamp(String sysTimeStamp) {
		this.sysTimeStamp = sysTimeStamp;
	}

	/**
	 * Metodo obtiene los atributos de la clase TransferenciaTO
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 ??/??/???? ??? (???) - ??? (Ing. Soft. Bci ):  versi�n inicial. </li>
	 * <li>1.1 30/01/2019 Eneida Diaz (Everis)- Luz Carre�o (Ing. Soft. Bci) : Se agrega javadoc y se incorporan atributos cuentaCargoComisionWeb y monedaCuentaComision.</li>
	 * </ul>
	 * </p>
	 * 
	 * @return String.
	 * @since 1.4
	 */
	
    @Override
    public String toString() {
        return "TransferenciaTO [bancoBeneficiario=" + bancoBeneficiario.toString() + ", bancoIntermediario="
                + bancoIntermediario + ", beneficiario=" + beneficiario.toString() + ", cuentaCargoComision="
                + cuentaCargoComision.toString() + ", cuentaOrigen=" + cuentaOrigen.toString() + ", codigoCambio="
                + codigoCambio.toString() + ", gastoBancoExterior=" + gastoBancoExterior.toString() + ", valuta="
                + valuta.toString() + ", codigoEstado=" + codigoEstado + ", estado=" + estado
                + ", fechaActualizacion=" + fechaActualizacion + ", fechaOperacion=" + fechaOperacion
                + ", modalidadOperacion=" + modalidadOperacion + ", montoTransferir=" + montoTransferir
                + ", motivoPago=" + motivoPago + ", saldoDisponible=" + saldoDisponible + ", tipoUsuario="
                + tipoUsuario + ", correlativoInterno=" + correlativoInterno + ", disabledButton="
                + disabledButton + ", renderAutorizar=" + renderAutorizar + ", cuentaCargoComisionWeb=" 
                + cuentaCargoComisionWeb + ", monedaCuentaComision=" + monedaCuentaComision + "]";
    }

}
