package cl.bci.aplicaciones.productos.servicios.transferenciaripple.mb;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.UISelectOne;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import cl.bci.aplicaciones.cliente.mb.ClienteMB;
import cl.bci.aplicaciones.cliente.mb.UsuarioClienteModelMB;
import cl.bci.aplicaciones.productos.servicios.transferenciamonex.constant.JournalTransferMonex;
import cl.bci.aplicaciones.productos.servicios.transferenciamonex.to.CodigoValor;
import cl.bci.aplicaciones.productos.servicios.transferenciamonex.to.CuentaMonexTO;
import cl.bci.aplicaciones.productos.servicios.transferenciamonex.to.TransferenciaComparator;
import cl.bci.aplicaciones.productos.servicios.transferenciamonex.to.TransferenciaTO;
import cl.bci.aplicaciones.seguridad.autenticacion.mb.SegundaClaveMB;
import cl.bci.infraestructura.utilitarios.error.mb.MensajesErrorUtilityMB;
import cl.bci.infraestructura.web.journal.mb.JournalUtilityMB;
import cl.bci.infraestructura.web.seguridad.autorizaciones.mb.AutorizadorUtilityMB;
import cl.bci.infraestructura.web.seguridad.segundaclave.SegundaClaveUIInput;
import cl.bci.infraestructura.web.seguridad.segundaclave.to.CamposDeLlaveTO;
import cl.bci.infraestructura.ws.excepcion.NegocioException;
import wcorp.aplicaciones.productos.servicios.divisas.to.CuentaTO;
import wcorp.aplicaciones.productos.servicios.pagos.pagosmasivos.ServiciosDePagosMasivos;
import wcorp.aplicaciones.productos.servicios.pagos.pagosmasivos.ServiciosDePagosMasivosHome;
import wcorp.aplicaciones.productos.servicios.pagos.pagosmasivos.to.NominaTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.sessionfacade.TtffException;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.Beneficiario1a1TO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.CambiaEstadoTttfMonexTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.CodigoCambioTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.ComisionTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.ConsultaBeneficiariosTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.ConsultaCodigoCambioTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.ConsultaMonedaTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.ConsultaTransferenciaTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.CuentaComisionTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.DatosAutorizacionTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.DetallePago1a1TO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.GastosBancoExteriorTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.Moneda1a1TO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.RespuestaHorarioTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.TransferenciaMonexTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.ValutaTO;
import wcorp.aplicaciones.productos.servicios.transferencias.nominas.ejb.NominaDeTransferencias;
import wcorp.aplicaciones.productos.servicios.transferencias.nominas.ejb.NominaDeTransferenciasHome;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.BeneficiarioRippleTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.ConsultaBeneficiariosRippleTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.ConsultaDetalleBeneficiarioRippleTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.DatosHashTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.DetalleBeneficiarioRippleTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.FirmarEliminarTransferenciaRipple;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.MonedasNodoTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.MonedasPermitidasTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.ObtenerComisionTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.RegistrarTransferenciaTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.RespuestaRegistroTransferenciaTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.TransferenciasRippleTO;
import wcorp.serv.bciexpress.ConApoquehanFirmado;
import wcorp.serv.bciexpress.ResultConsultarApoderadosquehanFirmado;
import wcorp.serv.seguridad.SeguridadException;
import wcorp.util.EnhancedServiceLocator;
import wcorp.util.ErroresUtil;
import wcorp.util.GeneralException;
import wcorp.util.SistemaException;
import wcorp.util.StringUtil;
import wcorp.util.TablaValores;
import wcorp.util.journal.Eventos;
import wcorp.util.xml.ConvierteXml;

/**
 * Bean encargado de realizar el flujo de ingreso, edici�n de transferencias.
 * <p>
 * Registro de Versiones :
 * <ul>
 * <li>1.0 19-02-2019, Danilo Dominguez(Everis) - Minheli Mejias (Ing. Soft.
 * BCI): Versi�n inicial</li> {@link #crearEJBServiciosDePagosMasivos()}
 * {@link #autorizar()} {@link #cambiaSeleccionMoneda()}
 * {@link #desplegarDetalle()} {@link #crearEJBServiciosDePagosMasivos()}
 * {@link #crearEJBServiciosDePagosMasivos()}
 * {@link #crearEJBServiciosDePagosMasivos()}
 * {@link #crearEJBServiciosDePagosMasivos()}
 * {@link #crearEJBServiciosDePagosMasivos()}
 * {@link #crearEJBServiciosDePagosMasivos()}
 * </ul>
 * </p>
 * <p>
 * <B>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</B>
 * </p>
 */
@ManagedBean
@RequestScoped
public class TransferenciasRippleBackingMB implements Serializable {

	/**
	 * Constante para obtener el objeto desde request.
	 */
	protected static final String TRANSFERENCIA = "TransferenciaTO";

	/**
	 * Objeto de transporte de datos de transferencia.
	 */
	protected static final String TRANSFERENCIA_MONEX_TO = "transferenciaMonexTO";

	/**
	 * Archivo parametros Autorizar FyP.
	 */
	private static final String ARCH_PARAM_VAL = "BCIExpressII_Autorizar_FyP.parametros";

	/**
	 * Representa numero uno.
	 */
	private static final int NUM_UNO = 1;

	/**
	 * Codigo de error generico fyp.
	 */
	private static final String ERROR_GENERICO_FYP = "FYP-011";

	/**
	 * Codigo de error no puede registrar la firma.
	 */
	private static final String ERROR_NO_REGISTR_FYP = "ODP-005";

	/**
	 * Valor comision por defecto.
	 */
	private static final String VALOR_COMISION_POR_DEFECTO = "0,00";

	/**
	 * Nombre de archivo de liquidacion.
	 */
	private static final String LIQUIDACION_FILE = "liquidacion.html";

	/**
	 * Content type text/html.
	 */
	private static final String CONTENT_TYPE_TEXT_HTML = "text/html";

	/**
	 * Content Type text/plain.
	 */
	private static final String CONTENT_TYPE_TEXT_PLAIN = "text/plain";

	/**
	 * Accion asociada al autorizador.
	 */
	private static final String ACCION = "Autorizar";

	/**
	 * C�digo pa�s de Chile.
	 */
	private static final String CODIGO_PAIS_CHILE = "CL";

	/**
	 * Constante ITF.
	 */
	private static final String TRANSFERENCIA_PARAMETROS = "transferencia";

	/**
	 * Navegaci�n a p�gina confirmaci�n.
	 */
	private static final String NAV_CONFIRMAR_TRANSFERENCIA_RIPPLE = "confirmarTransferenciasRipple.jsf";

	/**
	 * Navegaci�n a p�gina principal.
	 */
	private static final String NAV_INGRESAR_TRANSFERENCIA_MONEX = "ingresarTransferenciaMonex";

	/**
	 * Navegaci�n a p�gina principal.
	 */
	private static final String NAV_EDITAR_TRANSFERENCIA_MONEX = "editarTransferenciaMonex.jsf";

	/**
	 * Navegaci�n ver transferencias pendientes.
	 */
	private static final String NAV_VER_TRANSFERENCIAS_PENDIENTES = "verTransferenciasPendientes";

	/**
	 * Navegaci�n a pagina de cartola transferencias realizadas.
	 */
	private static final String NAV_VER_CARTOLA_TRANSFERENCIAS = "/cl/bci/aplicaciones/productos/servicios"
			+ "/transferencia/transferenciamonex/cartolaTransferenciasMonex";

	/**
	 * Par�metro de origen de la solicitud.
	 */
	private static final String ORIGEN_SOLICITUD = "WEB";

	/**
	 * Id de serializaci�n de objeto.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Servicio solicitado para Apoderado.
	 */
	private static final String SERVICIO_SOLICITADO_CUR = "CUR";

	/**
	 * Llave de tabla de par�metros.
	 */
	private static final String SERVICIO_SOLICITADO_KEY = "servicioSolicitado";

	/**
	 * Vista asociada al autorizador.
	 */
	private static final String VISTA = "transferencias-Monex-1a1";

	/**
	 * C�digo de error transferencia no corresponde al cliente.
	 */
	private static final String ERROR_TFMX_0020 = "TFMX-0020";

	/**
	 * C�digo de error transferencia ya firmada.
	 */
	private static final String ERROR_TFMX_0070 = "TFMX-0070";

	/**
	 * C�digo Comprobante de transferencia.
	 */
	private static final String CODIGO_COMPROBANTE_TRANSF = "ComprobanteTransferenciasRipple.jsf";

	/**
	 * Constante indicador de archivo XSL.
	 */
	private static final String XSL = "xsl";

	/**
	 * Constante c�digo imagen.
	 */
	private static final String CODIGO_IMAGEN = "imagen";

	/**
	 * Constante c�digo valor.
	 */
	private static final String CODIGO_VALOR = "valor";

	/**
	 * M�todo del servicio de Segunda Clave.
	 */
	protected static final String SERVICIO_SEGUNDA_CLAVE = "TransferenciasMxUnoAUno";

	/**
	 * Constante archivo errores a obtener desde archivo de parametros.
	 */
	private static final String CODIGOS_ERRORES = "errores.codigos";

	/**
	 * Constante mensajes error a obtener desde archivo de parametros.
	 */
	private static final String BAD_MSG = "BADMSG";

	/**
	 * Constante Descripcion a obtener desde archivo de parametros.
	 */
	private static final String DESC = "Desc";

	/**
	 * Constante Comision a obtener desde archivo de parametros.
	 */
	private static final String TARIFA_FIJA = "Tarifa Fija";

	/**
	 * Atributo que representa el archivo de par�metros de Compra y Venta
	 * Divisas.
	 */
	private static final String ARCHIVO_PARAMETROS = "compraVentaDeDivisas.parametros";

	/**
	 * Condicion de cierre "Cerrada" en cuenta monex.
	 */
	private static final String CERRADA = "C";

	/**
	 * Tabla de par�metros de transfer monex.
	 */
	private static final String TABLA_PARAMETROS_TRANSFERRIPPLE = "pyme/transferRipple.parametros";

	/**
	 * Constante global de transacciones.
	 */
	private static final String ACEPTAR_TRANSFERENCIAS_PEN = "aceptarTransferenciaRipple";

	/**
	 * Constante global de pagina de Comprobante.
	 */
	private static final String COMPROBANTE_TRANSFERENCIA_RIPPLE = "comprobanteTransferenciasRipple.jsf";

	/**
	 * Constante global a p�gina inicial de ripple.
	 */
	public static final String FINALIZAR = "ingresarTransferenciaRipple";

	/**
	 * Constante global a p�gina inicial de ripple.
	 */
	public static final String CONFIRMACION = "PEN";

	/**
	 * log de la clase TransferenciasRippleBackingMB.
	 */
	private transient Logger log = Logger.getLogger(TransferenciasRippleBackingMB.class);

	/**
	 * Bean de negocio autorizadorUtilityMB.
	 */
	@ManagedProperty(value = "#{autorizadorUtilityMB}")
	private AutorizadorUtilityMB autorizadorUtilityMB;

	/**
	 * Bean de negocio clienteMB.
	 */
	@ManagedProperty(value = "#{clienteMB}")
	private ClienteMB clienteMB;

	/**
	 * C�digo de cambio seleccionado.
	 */
	private CodigoCambioTO codigoCambio = new CodigoCambioTO();

	/**
	 * Datos combo c�digos de cambio.
	 */
	private List<CodigoCambioTO> codigosCambio;

	/**
	 * Corresponde a la cuenta cargo comisi�n seleccionada.
	 */
	private CuentaMonexTO cuentaCargoComision = new CuentaMonexTO();

	/**
	 * Cuenta cargo comision.
	 */
	private CuentaComisionTO cuentaComisionTo;

	/**
	 * Corresponde al numero cuenta origen.
	 */
	private CuentaMonexTO cuentaOrigen = new CuentaMonexTO();

	/**
	 * Flag deshabilitar select cuenta cargo y moneda.
	 */
	private boolean deshabilitaCuentaCargoMoneda;

	/**
	 * Corresponde al detalle de pago de una transferencia.
	 */
	private DetallePago1a1TO detallePago1a1TO;

	/**
	 * Flag que indica si existe error de negocio.
	 */
	private boolean errorNegocio = false;

	/**
	 * Flag que indica si existe error de validacion.
	 */
	private boolean errorValidacion = false;

	/**
	 * Corresponde a fecha desde panorama transferencia.
	 */
	private Date fechaDesde;

	/**
	 * Corresponde a fecha hasta panorama transferencia.
	 */
	private Date fechaHasta;

	/**
	 * Corresponde a fecha desde minimo para consulta de transferencia.
	 */
	private Date fechaDesdeMinima;

	/**
	 * Corresponde a fecha hasta maxima para consulta de transferencia.
	 */
	private Date fechaHastaMaximo;

	/**
	 * Gastos banco exterior.
	 */
	private GastosBancoExteriorTO gastosBancoExteriorTO = new GastosBancoExteriorTO();

	/**
	 * Inyecci�n de Managed Bean de journal.
	 */
	@ManagedProperty(value = "#{journalUtilityMB}")
	private JournalUtilityMB journalUtilityMB;

	/**
	 * Muestra el tipo de moneda para la comision de envio.
	 */
	private String tipoMonedaComision = "US$";

	/**
	 * Valor a presentar en la segunda columna de comisiones.
	 */
	private String valorAPresentar = " ";

	/**
	 * Valor a presentar en la primera columna de comisiones.
	 */
	private String valorAPresentar1 = " ";

	/**
	 * Muestra la moneda en formato dos decimales.
	 */
	private String formatoDecimal = "###,##0.00";

	/**
	 * Mensaje de error.
	 */
	private String mensajeError;

	/**
	 * Corresponde al mensaje retornado por bcx al confirmar transferencia.
	 */
	private String mensajeErrorNegocio;

	/**
	 * Corresponde al mensaje de error al obtener los beneficiarios.
	 */
	private String mensajeErrorBeneficiario;

	/**
	 * Corresponde a la moneda selecciona.
	 */
	private MonedasPermitidasTO monedaRipple = new MonedasPermitidasTO();

	/**
	 * Corresponde a la moneda selecciona.
	 */
	private Moneda1a1TO monedaOtraCuenta = new Moneda1a1TO();

	/**
	 * Monto a tranferir.
	 */
	private Double montoATransferir;

	/**
	 * Corresponde al motivo de pago de transferencia.
	 */
	private String motivoPago;

	/**
	 * N�mero de correlativo.
	 */
	private int numeroCorrelativo = 0;

	/**
	 * Saldo disponible cuenta corriente.
	 */
	private Double saldoDisponible;

	/**
	 * Referencia a la componente que maneja la segunda Clave.
	 */
	private SegundaClaveUIInput segundaClave;

	/**
	 * Flag para desabilitar select.
	 */
	private boolean selectCodigosCambioDisabled;

	/**
	 * S�mbolo de la moneda.
	 */
	private String simboloMoneda;

	/**
	 * Monto a tranferir string.
	 */
	private String strMontoATransferir;

	/**
	 * Fecha de transacci�n.
	 */
	private String systimestamp = "";

	/**
	 * Tipo de usuario.
	 */
	private String tipoUsuario;

	/**
	 * Valor ingresado de segunda clave.
	 */
	private String segundaClaveValor;

	/**
	 * Objeto seleccionado de transferencia.
	 */
	private TransferenciaTO transferenciaSelected;

	/**
	 * Flag que indica si el dia es feriado.
	 */
	private boolean errorFeriado = false;

	/**
	 * Bean de negocio transferenciasRippleUtilityMB.
	 */
	@ManagedProperty(value = "#{transferenciasRippleUtilityMB}")
	private TransferenciasRippleUtilityMB transferenciasRippleUtilityMB;

	/**
	 * Bean de negocio transferenciasRippleViewMB.
	 */
	@ManagedProperty(value = "#{transferenciasRippleViewMB}")
	private TransferenciasRippleViewMB transferenciasRippleViewMB;

	/**
	 * Bean de negocio usuarioClienteModelMB.
	 */
	@ManagedProperty(value = "#{usuarioClienteModelMB}")
	private UsuarioClienteModelMB usuarioClienteModelMB;

	/**
	 * Bean manejo de mensajes.
	 */
	@ManagedProperty(value = "#{mensajesErrorUtilityMB}")
	private MensajesErrorUtilityMB mbErrorUtilityMB;

	/**
	 * Bean que maneja operaciones de validaci�n de segunda clave.
	 */
	@ManagedProperty(value = "#{segundaClaveMB}")
	private SegundaClaveMB segundaClaveMB;

	/**
	 * Inicializa.
	 */
	private boolean init;

	/**
	 * Corresponde al los datos de la transferencia ingresada.
	 */
	private TransferenciasRippleTO transferenciaRipple = new TransferenciasRippleTO();

	public void setTransferenciaRipple(TransferenciasRippleTO transferenciasRipple) {
		this.transferenciaRipple = transferenciasRipple;
	}

	public TransferenciasRippleTO getTransferenciaRipple() {
		return this.transferenciaRipple;
	}

	/**
	 * M�todo encargado de crear una instacia del EJB ServiciosDePagosMasivos.
	 *
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 12/03/2019, Sergio Bustos B. (Everis) - Minheli Mejias (Ing.
	 * Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @return Instancia del EJB ServiciosDePagosMasivos.
	 * @throws GeneralException
	 *             en caso de error.
	 * @since 1.0
	 */
	private ServiciosDePagosMasivos crearEJBServiciosDePagosMasivos() throws GeneralException {
		if (getLogger().isInfoEnabled()) {
			getLogger().info("[crearEJBServiciosDePagosMasivos][BCI_INI]");
		}
		try {
			EnhancedServiceLocator locator = EnhancedServiceLocator.getInstance();
			ServiciosDePagosMasivosHome serviciosDePagosMasivosHome = (ServiciosDePagosMasivosHome) locator
					.getGenericService(
							"wcorp.aplicaciones.productos.servicios.pagos.pagosmasivos.ServiciosDePagosMasivos",
							ServiciosDePagosMasivosHome.class);
			if (getLogger().isInfoEnabled()) {
				getLogger().info("[crearEJBServiciosDePagosMasivos][BCI_FINOK] Retornando instancia del EJB");
			}
			return serviciosDePagosMasivosHome.create();
		} catch (Exception e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[crearEJBServiciosDePagosMasivos][BCI_FINEX][Exception][" + e.getMessage() + "]", e);
			}

			throw new GeneralException("ESPECIAL", ErroresUtil.extraeStackTrace(e));
		}
	}

	/**
	 * Genera solicitud de autorizacion en paso 2.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * </ul>
	 * </p>
	 * 
	 * @param transferencia Entidad de negocio de transferencia.
	 * @return String
	 * @since 1.0
	 */
	public String autorizar(TransferenciaTO transferencia) {
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[BCI_INI][transferencia][" + transferencia + "]");
		}

		final String nombreMetodo = "autorizar";
		final String identificador = (usuarioClienteModelMB != null
				&& usuarioClienteModelMB.getUsuarioModelMB() != null)
						? String.valueOf(usuarioClienteModelMB.getUsuarioModelMB().getRut()) : "";

		String outcome = "";

		mbErrorUtilityMB.setMensajeError(TablaValores.getValor(CODIGOS_ERRORES, BAD_MSG, DESC));

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[" + nombreMetodo + "][" + identificador + "]");
			getLogger().debug("[" + nombreMetodo + "][" + identificador + "] [Parametro][transferencia]["
					+ StringUtil.contenidoDe(transferencia) + "]");
		}

		try {
			transferenciasRippleUtilityMB.consultarDetalleTransferencia(transferencia);

			if (getLogger().isEnabledFor(Level.DEBUG)) {
				getLogger().debug("[" + nombreMetodo + "][" + identificador + "] [transferencia] ["
						+ StringUtil.contenidoDe(transferencia) + "]");
			}

			if (getLogger().isEnabledFor(Level.DEBUG)) {
				getLogger()
						.debug("[" + nombreMetodo + "][" + identificador + "] [transferenciasRippleViewMB.comision] ["
								+ StringUtil.contenidoDe(transferenciasRippleViewMB.getComision()) + "]");
			}

			ComisionTO comisionTO = this.transferenciasRippleViewMB.getComision();
			if (comisionTO != null && comisionTO.getTipoTarifa() != null) {

				DecimalFormat formatoMontoADesplegar = new DecimalFormat(formatoDecimal);
				if ((comisionTO.getTipoTarifa().equals(TARIFA_FIJA)) || (comisionTO.getTipoTarifa().trim().isEmpty()
						&& comisionTO.getComisionEnvio().compareTo(BigDecimal.ZERO) == 0)) {

					String comisionEnvio = tipoMonedaComision
							+ formatoMontoADesplegar.format(comisionTO.getComisionEnvio()).toString();
					comisionTO.setComsionEnvioParaMostrar(comisionEnvio);

					valorAPresentar1 = formatoMontoADesplegar.format(comisionTO.getMontoGatos1()).toString();
					valorAPresentar = formatoMontoADesplegar.format(comisionTO.getMontoGatos()).toString();

				} else if (!comisionTO.getTipoTarifa().equals(TARIFA_FIJA)) {
					comisionTO.setComsionEnvioParaMostrar(comisionTO.getTipoTarifa());
					valorAPresentar1 = formatoMontoADesplegar.format(comisionTO.getMontoGatos1()).toString();
					valorAPresentar = formatoMontoADesplegar.format(comisionTO.getMontoGatos()).toString();
				}
			} else {

				String comisionEnvio = tipoMonedaComision + VALOR_COMISION_POR_DEFECTO;
				comisionTO.setComsionEnvioParaMostrar(comisionEnvio);

				valorAPresentar1 = VALOR_COMISION_POR_DEFECTO;
				valorAPresentar = VALOR_COMISION_POR_DEFECTO;
			}

			if (getLogger().isEnabledFor(Level.DEBUG)) {
				getLogger().debug("[" + nombreMetodo + "][" + identificador + "] [comisionTO] ["
						+ StringUtil.contenidoDe(comisionTO) + "]");
			}

			FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Comision", comisionTO);
			FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put(TRANSFERENCIA, transferencia);
			outcome = NAV_CONFIRMAR_TRANSFERENCIA_RIPPLE;
		} catch (Exception e) {
			if (getLogger().isEnabledFor(Level.WARN)) {
				getLogger().warn("[" + nombreMetodo + "][" + identificador + "] [Exception][" + e.getMessage() + "]",
						e);
			}
		}

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "] [" + identificador + "] [BCI_FINOK][" + outcome + "]");
		}

		return outcome;
	}

	/**
	 * Carga informaci�n necesaria al cambiar el combo moneda.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 25/02/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing.
	 * Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param ev
	 *            Evento Ajax.
	 * @since 1.0
	 */
	public void cambiaSeleccionMoneda(AjaxBehaviorEvent ev) {
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug(
					"[cambiaSeleccionMoneda] [" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "] [BCI_INI]");
		}
		this.init = true;
		UISelectOne selectMoneda = (UISelectOne) ev.getSource();
		String valor = (String) selectMoneda.getSubmittedValue();
		this.getMonedaRipple().setCodigoIso(valor);
		this.buscaMonedaSeleccionada();
		this.setSaldoDisponible(null);
		this.setSimboloMoneda(null);
		this.transferenciasRippleViewMB.getCuentasOrigen().clear();
		this.transferenciasRippleViewMB.getBeneficiariosRipple().clear();
		this.transferenciasRippleViewMB.getCodigosCambio().clear();
		if (valor != null && StringUtils.isNotEmpty(valor) && !"0".equals(valor)) {
			this.obtenerInformacionDeMoneda();
			this.obtenerCodigosDeCambio();
			transferenciasRippleViewMB.setMuestraDetalleBeneficiario(false);
		}
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug(
					"[cambiaSeleccionMoneda] [" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "] [BCI_FINOK]");
		}
	}

	/**
	 * Carga los datos del detalle de la transferencia pendiente seleccionada
	 * por el usuario.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 12/03/2019, Sergio Bustos B. (Everis) - Minheli Mejias (Ing. Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param transferencia
	 *            Parametro obtenido desde la presentaci�n.
	 * @param tipoTransferencia
	 *            flag si es tranferencia pendiente o realizada.
	 * @since 1.0
	 */
	public void desplegarDetalle(TransferenciaTO transferencia, String tipoTransferencia) {
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[desplegarDetalle] [" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "] [BCI_INI]");
		}
		try {
			transferencia.setTipoTransferencia(tipoTransferencia);
			this.consultarDetalleTransferencia(transferencia);
			this.setTransferenciaSelected(transferencia);
		} catch (Exception e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[desplegarDetalle]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_FINEX][Exception]Error con mensaje " + e.getMessage(), e);
			}

		}
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug(
					"[desplegarDetalle] [" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "] [BCI_FINOK]");
		}
	}

	public AutorizadorUtilityMB getAutorizadorUtilityMB() {
		return autorizadorUtilityMB;
	}

	public ClienteMB getClienteMB() {
		return clienteMB;
	}

	public CodigoCambioTO getCodigoCambio() {
		return codigoCambio;
	}

	/**
	 * Retorna los c�digos de cambio.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 25/02/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing.
	 * Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @return List<CodigoCambioTO>
	 */
	public List<CodigoCambioTO> getCodigosCambio() {
		if (this.getMonedaRipple().getCodigoIso() != null) {
			this.obtenerCodigosDeCambio();
		}
		return codigosCambio;
	}

	public CuentaMonexTO getCuentaCargoComision() {
		return cuentaCargoComision;
	}

	public CuentaComisionTO getCuentaComisionTo() {
		return cuentaComisionTo;
	}

	public CuentaMonexTO getCuentaOrigen() {
		return cuentaOrigen;
	}

	public List<CuentaMonexTO> getCuentasComision() {
		return transferenciasRippleViewMB.getCuentasComision();
	}

	public boolean getDeshabilitaCuentaCargoMoneda() {
		return deshabilitaCuentaCargoMoneda;
	}

	public DetallePago1a1TO getDetallePago1a1TO() {
		return detallePago1a1TO;
	}

	public boolean getErrorNegocio() {
		return errorNegocio;
	}

	public boolean getErrorValidacion() {
		return errorValidacion;
	}

	public Date getFechaDesde() {
		return fechaDesde;
	}

	public Date getFechaHasta() {
		return fechaHasta;
	}

	public GastosBancoExteriorTO getGastosBancoExteriorTO() {
		return gastosBancoExteriorTO;
	}

	public JournalUtilityMB getJournalUtilityMB() {
		return journalUtilityMB;
	}

	public String getMensajeError() {
		return mensajeError;
	}

	public String getMensajeErrorNegocio() {
		return mensajeErrorNegocio;
	}

	public String getMensajeErrorBeneficiario() {
		return mensajeErrorBeneficiario;
	}

	public MonedasPermitidasTO getMonedaRipple() {
		return monedaRipple;
	}

	public Moneda1a1TO getMonedaOtraCuenta() {
		return monedaOtraCuenta;
	}

	/**
	 * Retornas las monedas del sistema.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 25/02/2019, Danilo Dominguez (Everis) - Minheli Mejias (Ing.
	 * Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @return List<Moneda1a1TO>
	 * @since 1.0
	 */
	public List<MonedasPermitidasTO> getMonedas() {
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[getMonedas][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "] [BCI_INI]");
		}
		try {
			if (transferenciasRippleViewMB.getMonedasRipple() == null
					|| transferenciasRippleViewMB.getMonedasRipple().isEmpty()) {
				transferenciasRippleViewMB.setMonedasNodo(transferenciasRippleUtilityMB.obtenerMonedas());
			}
		} catch (GeneralException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[getMonedas][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ "] [BCI_FINEX][GeneralException]Error con mensaje " + e.getMessage(), e);
			}
		}
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[getMonedas][" + clienteMB.getRut() + "] [BCI_FINOK]");
		}
		return transferenciasRippleViewMB.getMonedasRipple();
	}

	/**
	 * Acceso monedas otras cuentas.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 21-10-2013, Angel Cris�stomo (Imagemaker IT): versi�n
	 * inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @return List<Moneda1a1TO>.
	 * @since 1.0
	 */
	public List<Moneda1a1TO> getMonedasOtrasCuentas() {
		try {
			if (getLogger().isEnabledFor(Level.DEBUG)) {
				getLogger().debug("[getMonedasOtrasCuentas][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ "] [BCI_INI]");
			}
			if (transferenciasRippleViewMB.getMonedasOtrasCuentas() == null
					|| transferenciasRippleViewMB.getMonedasOtrasCuentas().isEmpty()) {
				ConsultaMonedaTO consultaMonedaTO = new ConsultaMonedaTO();

				consultaMonedaTO.setCodigoProducto(
						TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "codigosMoneda", "codigoProductoCVD"));
				consultaMonedaTO.setOrigenSolicitud(
						TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "codigosMoneda", "origenSolicitud"));
				transferenciasRippleViewMB
						.setMonedasOtrasCuentas(transferenciasRippleUtilityMB.obtenerMonedas(consultaMonedaTO));
			}
		} catch (GeneralException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[getMonedasOtrasCuentas][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ "] [BCI_FINEX][GeneralException]Error con mensaje " + e.getMessage(), e);
			}
		}
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug(
					"[getMonedasOtrasCuentas][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "] [BCI_FINOK]");
		}
		return transferenciasRippleViewMB.getMonedasOtrasCuentas();
	}

	public Double getMontoATransferir() {
		return montoATransferir;
	}

	public String getMotivoPago() {
		return motivoPago;
	}

	public int getNumeroCorrelativo() {
		return numeroCorrelativo;
	}

	public Double getSaldoDisponible() {
		return saldoDisponible;
	}

	public SegundaClaveUIInput getSegundaClave() {
		return segundaClave;
	}

	public String getSimboloMoneda() {
		return simboloMoneda;
	}

	public String getStrMontoATransferir() {
		return strMontoATransferir;
	}

	public String getSystimestamp() {
		return systimestamp;
	}

	public String getTipoUsuario() {
		return tipoUsuario;
	}

	public TransferenciaTO getTransferenciaSelected() {
		return transferenciaSelected;
	}

	public TransferenciasRippleUtilityMB getTransferenciasRippleUtilityMB() {
		return transferenciasRippleUtilityMB;
	}

	public TransferenciasRippleViewMB getTransferenciasRippleViewMB() {
		return transferenciasRippleViewMB;
	}

	/**
	 * Retorna las transferencias realizadas.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 13/03/2019 Rafael Orellana (Everis) - Minheli Mejias (Ing. Soft.
	 * BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @return List<TransferenciaUnoAUno>
	 * @since 1.0
	 */
	public List<TransferenciaTO> getTransferenciasRealizadas() {
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[getTransferenciasRealizadas][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
					+ "] [BCI_INI]");
		}
		try {
			if (this.transferenciasRippleViewMB.getTransferenciasRealizadas() == null
					|| this.transferenciasRippleViewMB.getTransferenciasRealizadas().isEmpty()) {
				List<TransferenciaTO> lista = this.consultarTransferenciasRealizadas();
				Collections.sort(lista, new TransferenciaComparator<TransferenciaTO>());
				this.transferenciasRippleViewMB.setTransferenciasRealizadas(lista);
			}
		} catch (GeneralException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[getTransferenciasRealizadas][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ "] [BCI_FINEX][GeneralException]Error con mensaje " + e.getMessage(), e);
			}
		}
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[getTransferenciasRealizadas][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
					+ "] [BCI_FINOK]");
		}
		return this.transferenciasRippleViewMB.getTransferenciasRealizadas();
	}

	public UsuarioClienteModelMB getUsuarioClienteModelMB() {
		return usuarioClienteModelMB;
	}

	public MensajesErrorUtilityMB getMbErrorUtilityMB() {
		return mbErrorUtilityMB;
	}

	/**
	 * Navega a p�gina ingreso nueva transferencia.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 22-10-2013, Angel Cris�stomo (Imagemaker IT): versi�n
	 * inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @return String
	 * @since 1.0
	 */
	public String irATransferencias() {
		return NAV_INGRESAR_TRANSFERENCIA_MONEX;
	}

	public boolean isSelectCodigosCambioDisabled() {
		return selectCodigosCambioDisabled;
	}

	/**
	 * Obtiene los c�digos de cambio en base al c�digo moneda.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 25/02/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing.
	 * Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param codMoneda
	 *            C�digo de moneda.
	 * @since 1.0
	 */
	public void obtenerCodigosDeCambio() {
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug(
					"[obtenerCodigosDeCambio][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "] [BCI_INI]");
		}
		try {
			ConsultaCodigoCambioTO consultaCodigoCambioTO = new ConsultaCodigoCambioTO();
			consultaCodigoCambioTO.setCodigoProducto(
					TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "codigosMoneda", "codigoProductoRPP"));
			consultaCodigoCambioTO.setOrigenSolicitud(
					TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "codigosMoneda", "origenSolicitud"));
			consultaCodigoCambioTO.setMonedaOrigen("");
			consultaCodigoCambioTO.setMonedaDestino("");

			this.transferenciasRippleViewMB
					.setCodigosCambio(transferenciasRippleUtilityMB.obtenerCodigosDeCambio(consultaCodigoCambioTO));
			for (CodigoCambioTO codigo : this.transferenciasRippleViewMB.getCodigosCambio()) {
				codigo.setDescripcion(codigo.getCodigoCambio() + " - " + codigo.getDescripcion());
			}

		} catch (GeneralException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[obtenerCodigosDeCambio][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ "] [BCI_FINEX][GeneralException]Error con mensaje " + e.getMessage(), e);
			}
		}
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug(
					"[obtenerCodigosDeCambio][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "] [BCI_FINOK]");
		}
	}

	/**
	 * Obtiene el detalle de un beneficiario seleccionado.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 25/02/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing.
	 * Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param ev
	 *            Evento Ajax.
	 * @since 1.0
	 */
	public void obtenerDetalleDelBeneficiario(AjaxBehaviorEvent ev) {
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[obtenerDetalleDelBeneficiario][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
					+ "] [BCI_INI]");
		}
		this.init = true;
		try {
			UISelectOne selectBeneficiario = (UISelectOne) ev.getSource();
			UIComponent source = ev.getComponent();
			String valor = (String) selectBeneficiario.getSubmittedValue();
			if (valor.equals("0")) {
				transferenciasRippleViewMB.setMuestraDetalleBeneficiario(Boolean.FALSE);
			} else {
				UIInput inputCodigoIMoneda = (UIInput) source.findComponent("transferForm:codigoIso");
				String codMoneda = (String) inputCodigoIMoneda.getSubmittedValue();
				this.monedaRipple.setCodigoIso(codMoneda);
				this.buscaMonedaSeleccionada();
				transferenciasRippleViewMB.getDestinRipple().setIdentificacion(valor);
				transferenciasRippleViewMB.buscarDestinatarioSeleccionado();
				String paisBanco = transferenciasRippleViewMB.getDestinRipple().getPaisBanco();
				transferenciasRippleViewMB
						.setDetBenefRpp(this.consultarDetalleBeneficiario(valor, codMoneda, paisBanco));
				transferenciasRippleViewMB.setMuestraDetalleBeneficiario(Boolean.TRUE);
				if (CODIGO_PAIS_CHILE.equalsIgnoreCase(transferenciasRippleViewMB.getDestinatario().getPaisBco())) {
					this.setSelectCodigosCambioDisabled(true);
				}
			}
		} catch (GeneralException e) {
			transferenciasRippleViewMB.setMuestraDetalleBeneficiario(Boolean.FALSE);
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger()
						.error("[obtenerDetalleDelBeneficiario][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
								+ "] [BCI_FINEX][GeneralException]Error con mensaje " + e.getMessage(), e);
			}
		}
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[obtenerDetalleDelBeneficiario][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
					+ "] [BCI_FINOK]");
		}
	}

	/**
	 * Metodo que obtiene informaci�n relacionada con la moneda seleccionada.
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 25/02/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing.
	 * Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @since 1.0
	 */
	public void obtenerInformacionDeMoneda() {
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[obtenerInformacionDeMoneda][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
					+ "] [BCI_INI]");
		}
		try {
			transferenciasRippleViewMB.getDestinatario().setIdentificacionBeneficiario("0");
			List<CuentaMonexTO> cuentasOrigen = this.transferenciasRippleUtilityMB.getCuentasPorCodigoMoneda(
					clienteMB.getRut(), clienteMB.getDigitoVerif(), this.getMonedaRipple().getCodigoIso());
			cuentasOrigen = this.transferenciasRippleUtilityMB.getCuentasFiltradasApertura(cuentasOrigen);
			if (getLogger().isEnabledFor(Level.DEBUG)) {
				getLogger().debug("[obtenerInformacionDeMoneda][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ "] Antes del if (cuentasOrigen == null || cuentasOrigen.isEmpty()"
						+ "|| cuentasOrigen.get(0).getCondicionCierre().equals(CERRADA))");
			}
			if (cuentasOrigen == null || cuentasOrigen.isEmpty()
					|| cuentasOrigen.get(0).getCondicionCierre().equals(CERRADA)) {

				CuentaTO[] cuentasFondos = this.transferenciasRippleUtilityMB.obtenerFondosProvisionados(
						clienteMB.getRut(), clienteMB.getDigitoVerif(), this.getMonedaRipple().getCodigoIso());
				if (getLogger().isEnabledFor(Level.DEBUG)) {
					getLogger()
							.debug("[obtenerInformacionDeMoneda][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
									+ "] Antes del if (cuentasFondos != null && cuentasFondos.length > 0)");
				}
				if (cuentasFondos != null && cuentasFondos.length > 0) {
					CuentaTO cuentaTO = cuentasFondos[0];
					CuentaMonexTO cuentaFP = new CuentaMonexTO();

					cuentaFP.setMoneda(new CodigoValor(
							this.getMonedaRipple().getCodigoIso() == null ? this.getMonedaRipple().getCodigoIso() : "",
							this.getMonedaRipple().getNombreMoneda() == null ? this.getMonedaRipple().getNombreMoneda()
									: ""));
					cuentaFP.setSimboloMoneda(this.transferenciasRippleUtilityMB
							.obtenerSimboloMoneda(this.getMonedaRipple().getCodigoIso()));
					cuentaFP.setNumeroCuenta(cuentaTO.getNumeroCuenta());
					cuentaFP.setSaldoDisponible(cuentaTO.getSaldo() == null ? 0d : cuentaTO.getSaldo().doubleValue());
					cuentasOrigen = new ArrayList<CuentaMonexTO>();
					cuentasOrigen.add(cuentaFP);
				}

			}
			transferenciasRippleViewMB.setCuentasOrigen(cuentasOrigen);
		} catch (GeneralException e) {
			if (getLogger().isEnabledFor(Level.WARN)) {
				getLogger().warn("[obtenerInformacionDeMoneda][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ "][GeneralException]Error con mensaje " + e.getMessage(), e);
			}
		}
		try {
			ConsultaBeneficiariosRippleTO consulta = new ConsultaBeneficiariosRippleTO();
			consulta.setBicBanco(this.transferenciasRippleViewMB.getNodoRipple().getBic());
			consulta.setMndaCuenta(this.monedaRipple.getCodigoIso());
			consulta.setRutEmpresa(getRutEmpresa());
			transferenciasRippleViewMB.setBeneficiariosRipple(
					transferenciasRippleUtilityMB.obtenerBeneficiariosAutorizadosRipple(consulta));
			errorValidacion = false;
		} catch (GeneralException e) {
			transferenciasRippleViewMB.setBeneficiarios(null);
			this.mensajeErrorBeneficiario = TablaValores.getValor("errores.codigos", "TRANSMONEX", "Desc");

		}
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[obtenerInformacionDeMoneda][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
					+ "] [BCI_FINOK]");
		}
	}

	/**
	 * Obtiene el saldo disponible de la cuenta seleccionada.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 25-09-2013, Angel Cris�stomo (Imagemaker IT): versi�n
	 * inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param ev
	 *            Evento Ajax.
	 * @return String
	 * @since 1.0
	 */
	public String obtenerSaldoCuenta(AjaxBehaviorEvent ev) {
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug(
					"[obtenerSaldoCuenta][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "] [BCI_INI]");
		}
		this.init = true;
		try {
			UISelectOne selectCuenta = (UISelectOne) ev.getSource();
			String valor = (String) selectCuenta.getSubmittedValue();
			if (valor != null && StringUtils.isNotEmpty(valor)) {
				this.getCuentaOrigen().setNumeroCuenta(valor);
				this.buscaCuentaOrigenSeleccionada();
				this.setSimboloMoneda(this.transferenciasRippleUtilityMB
						.obtenerSimboloMoneda(this.getCuentaOrigen().getMoneda().getCodigo()));
				this.setSaldoDisponible(this.getCuentaOrigen().getSaldoDisponible());
			}
		} catch (Exception e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[obtenerSaldoCuenta][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ "] [BCI_FINEX][Exception]Error con mensaje " + e.getMessage(), e);
			}
		}
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug(
					"[obtenerSaldoCuenta][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "] [BCI_FINOK]");
		}
		return null;
	}

	/**
	 * Metodo encargado de presetear valores en combo cargo comision y moneda.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 18-10-2013, Angel Cris�stomo (Imagemaker IT): versi�n
	 * inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @since 1.0
	 */
	public void precargaDatos() {
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[precargaDatos][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "] [BCI_INI]");
		}
		try {
			this.cuentaComisionTo = transferenciasRippleUtilityMB.obtenerCuentaDeCargo(getRutEmpresa());
		} catch (GeneralException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[precargaDatos][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ "] [BCI_FINEX][Exception]Error con mensaje " + e.getInfoAdic(), e);
			}
		}
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger()
					.debug("[precargaDatos][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "] [BCI_FINOK]");
		}
	}

	public void setAutorizadorUtilityMB(AutorizadorUtilityMB autorizadorUtilityMB) {
		this.autorizadorUtilityMB = autorizadorUtilityMB;
	}

	public void setClienteMB(ClienteMB clienteMB) {
		this.clienteMB = clienteMB;
	}

	public void setCodigoCambio(CodigoCambioTO codigoCambio) {
		this.codigoCambio = codigoCambio;
	}

	/**
	 * Acceso codigos cambio.
	 * 
	 * @param codigosCambio
	 *            Lista de codigos de cambio.
	 */
	public void setCodigosCambio(List<CodigoCambioTO> codigosCambio) {
		this.codigosCambio = codigosCambio;
	}

	public void setCuentaCargoComision(CuentaMonexTO cuentaCargoComision) {
		this.cuentaCargoComision = cuentaCargoComision;
	}

	public void setCuentaComisionTo(CuentaComisionTO cuentaComisionTo) {
		this.cuentaComisionTo = cuentaComisionTo;
	}

	public void setCuentaOrigen(CuentaMonexTO cuentaOrigen) {
		this.cuentaOrigen = cuentaOrigen;
	}

	public void setDeshabilitaCuentaCargoMoneda(boolean deshabilitaCuentaCargoMoneda) {
		this.deshabilitaCuentaCargoMoneda = deshabilitaCuentaCargoMoneda;
	}

	public void setDetallePago1a1TO(DetallePago1a1TO detallePago1a1TO) {
		this.detallePago1a1TO = detallePago1a1TO;
	}

	public void setErrorNegocio(boolean errorNegocio) {
		this.errorNegocio = errorNegocio;
	}

	public void setErrorValidacion(boolean errorValidacion) {
		this.errorValidacion = errorValidacion;
	}

	public void setFechaDesde(Date fechaDesde) {
		this.fechaDesde = fechaDesde;

	}

	public void setFechaHasta(Date fechaHasta) {
		this.fechaHasta = fechaHasta;
	}

	public void setGastosBancoExteriorTO(GastosBancoExteriorTO gastosBancoExteriorTO) {
		this.gastosBancoExteriorTO = gastosBancoExteriorTO;
	}

	public void setJournalUtilityMB(JournalUtilityMB journalUtilityMB) {
		this.journalUtilityMB = journalUtilityMB;
	}

	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}

	public void setMensajeErrorNegocio(String mensajeErrorNegocio) {
		this.mensajeErrorNegocio = mensajeErrorNegocio;
	}

	public void setMensajeErrorBeneficiario(String mensajeErrorBeneficiario) {
		this.mensajeErrorBeneficiario = mensajeErrorBeneficiario;
	}

	public void setMonedaOtraCuenta(Moneda1a1TO monedaOtraCuenta) {
		this.monedaOtraCuenta = monedaOtraCuenta;
	}

	public void setMontoATransferir(Double montoATransferir) {
		this.montoATransferir = montoATransferir;
	}

	public void setMotivoPago(String motivoPago) {
		this.motivoPago = motivoPago;
	}

	public void setNumeroCorrelativo(int numeroCorrelativo) {
		this.numeroCorrelativo = numeroCorrelativo;
	}

	public void setSaldoDisponible(Double saldoDisponible) {
		this.saldoDisponible = saldoDisponible;
	}

	public void setSegundaClave(SegundaClaveUIInput segundaClave) {
		this.segundaClave = segundaClave;
	}

	public void setSelectCodigosCambioDisabled(boolean selectCodigosCambioDisabled) {
		this.selectCodigosCambioDisabled = selectCodigosCambioDisabled;
	}

	public void setSimboloMoneda(String simboloMoneda) {
		this.simboloMoneda = simboloMoneda;
	}

	public void setStrMontoATransferir(String strMontoATransferir) {
		this.strMontoATransferir = strMontoATransferir;
	}

	public void setSystimestamp(String systimestamp) {
		this.systimestamp = systimestamp;
	}

	public void setTipoUsuario(String tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

	public void setTransferenciaSelected(TransferenciaTO transferenciaSelected) {
		this.transferenciaSelected = transferenciaSelected;
	}

	public void setSegundaClaveValor(String segundaClaveValor) {
		this.segundaClaveValor = segundaClaveValor;
	}

	public void setTransferenciasRippleUtilityMB(TransferenciasRippleUtilityMB transferenciasRippleUtilityMB) {
		this.transferenciasRippleUtilityMB = transferenciasRippleUtilityMB;
	}

	public void setTransferenciasRippleViewMB(TransferenciasRippleViewMB transferenciasRippleViewMB) {
		this.transferenciasRippleViewMB = transferenciasRippleViewMB;
	}

	public void setUsuarioClienteModelMB(UsuarioClienteModelMB usuarioClienteModelMB) {
		this.usuarioClienteModelMB = usuarioClienteModelMB;
	}

	public void setMbErrorUtilityMB(MensajesErrorUtilityMB mbErrorUtilityMB) {
		this.mbErrorUtilityMB = mbErrorUtilityMB;
	}

	public void setSegundaClaveMB(SegundaClaveMB segundaClaveMB) {
		this.segundaClaveMB = segundaClaveMB;
	}

	/**
	 * Navega a flujo de ver todas las transferencias realizadas.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 15/10/2013, Mauricio Flores S�enz (Imagemaker IT): versi�n
	 * inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @return String
	 * @since 1.0
	 */
	public String verTransferenciasRealizadas() {
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[verTransferenciasRealizadas][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
					+ "] [BCI_INI]");
		}
		String outcome = null;
		try {
			List<TransferenciaTO> transferencias = this.consultarTransferenciasRealizadas();
			if (transferencias != null) {
				Collections.sort(transferencias, new TransferenciaComparator<TransferenciaTO>());
				FacesContext.getCurrentInstance().getExternalContext().getRequestMap()
						.put(TransferenciasRippleViewMB.TRANSFERENCIAS_REALIZADAS, transferencias);
			}
			outcome = NAV_VER_CARTOLA_TRANSFERENCIAS;
		} catch (Exception e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[verTransferenciasRealizadas][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ "] [BCI_FINEX][Exception]Error con mensaje " + e.getMessage(), e);
			}
		}
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[verTransferenciasRealizadas][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
					+ "] [BCI_FINOK]");
		}
		return outcome;
	}

	/**
	 * Volver a p�gina de ingreso de transferencia monex.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 09/10/2013, Mauricio Flores S�enz (Imagemaker IT): versi�n
	 * inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @return String
	 * @since 1.0
	 */
	public String volver() {
		String outcome = null;
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[volver][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "] [BCI_INI]");
		}
		try {
			TransferenciaTO transferencia = this.getTransferenciasRippleViewMB().getTransferenciaSelected();
			this.transferirDatos(transferencia);
			outcome = NAV_EDITAR_TRANSFERENCIA_MONEX;
		} catch (Exception e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[volver][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ "] [BCI_FINEX][Exception]Error con mensaje " + e.getMessage(), e);
			}
		}
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[volver][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "] [BCI_FINOK]");
		}
		return outcome;
	}

	/**
	 * Busca dentro de las cuentas de origen la cuenta seleccionada.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 13-09-2013, Angel Cris�stomo (Imagemaker IT): versi�n
	 * inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @since 1.0
	 */
	private void buscaCuentaOrigenSeleccionada() {
		for (CuentaMonexTO cta : transferenciasRippleViewMB.getCuentasOrigen()) {
			if (cta.getNumeroCuenta().equals(this.getCuentaOrigen().getNumeroCuenta())) {
				this.cuentaOrigen = cta;
				break;
			}
		}
	}

	/**
	 * Busca los valores Monto Maximo y Nombre Moneda de la Moneda seleccionada
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 28/02/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing.
	 * Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @since 1.0
	 */
	private void buscaMonedaSeleccionada() {
		for (MonedasPermitidasTO mda : transferenciasRippleViewMB.getMonedasRipple()) {
			if (mda.getCodigoIso().equals(this.monedaRipple.getCodigoIso())) {
				this.monedaRipple.setMtoMaximo(mda.getMtoMaximo());
				this.monedaRipple.setNombreMoneda(mda.getNombreMoneda());
				break;
			}
		}
	}

	/**
	 * Consulta servicio para obtener beneficiario.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 10/10/2013, Mauricio Flores S�enz (Imagemaker IT): versi�n
	 * inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param codigoMoneda
	 *            Codigo de moneda.
	 * @param rutEmpresa
	 *            RUT de empresa.
	 * @return Beneficiario1a1TO Representa entidad beneficiario.
	 * @throws GeneralException
	 *             Excepci�n de negocio.
	 * @since 1.0
	 */
	private List<Beneficiario1a1TO> consultarBeneficiario(String codigoMoneda, String rutEmpresa)
			throws GeneralException {
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug(
					"[consultarBeneficiario][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "] [BCI_INI]");
		}
		ConsultaBeneficiariosTO consulta = new ConsultaBeneficiariosTO();
		consulta.setMndaCuenta(codigoMoneda);
		consulta.setRutEmpresa(rutEmpresa);

		return this.transferenciasRippleUtilityMB.obtenerBeneficiariosAutorizados(consulta);
	}

	/**
	 * Acceso al servicio para la consulta de c�digo de cambio.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 10/10/2013, Mauricio Flores S�enz (Imagemaker IT): versi�n
	 * inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param codigoMoneda
	 *            C�digo de moneda.
	 * @return List<CodigoCambioTO>
	 * @throws GeneralException
	 *             Exepci�n General de negocio.
	 * @since 1.0
	 */
	private List<CodigoCambioTO> consultarCodigosCambio(String codigoMoneda) throws GeneralException {

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug(
					"[consultarCodigosCambio][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "] [BCI_INI]");
		}

		ConsultaCodigoCambioTO consultaCodigoCambioTO = new ConsultaCodigoCambioTO();

		consultaCodigoCambioTO.setCodigoProducto(
				TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "codigosMoneda", "codigoProducto"));
		consultaCodigoCambioTO.setOrigenSolicitud(
				TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "codigosMoneda", "origenSolicitud"));
		consultaCodigoCambioTO.setMonedaOrigen(codigoMoneda);
		consultaCodigoCambioTO.setMonedaDestino(codigoMoneda);
		return this.transferenciasRippleUtilityMB.obtenerCodigosDeCambio(consultaCodigoCambioTO);
	}

	/**
	 * Consulta detalle del beneficiario.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 25/02/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing.
	 * Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param identificadorBeneficiario
	 *            Identificador Beneficiario.
	 * @param codMoneda
	 *            Codigo de moneda.
	 * @param paisBanco
	 *            C�digo de pais banco.
	 * @return DetalleBeneficiarioTO Entidad de negocio.
	 * @throws GeneralException
	 *             Excepcion de negocio.
	 * @since 1.0
	 */
	private DetalleBeneficiarioRippleTO consultarDetalleBeneficiario(String identificadorBeneficiario, String codMoneda,
			String paisBanco) throws GeneralException {

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[consultarDetalleBeneficiario][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
					+ "] [BCI_INI]");
		}

		this.getTransferenciasRippleViewMB().setCodigoMoneda(codMoneda);
		this.getTransferenciasRippleViewMB().setCodigoPaisBanco(paisBanco);
		this.getTransferenciasRippleViewMB().setIdentificadorBeneficiario(identificadorBeneficiario);

		ConsultaDetalleBeneficiarioRippleTO datosConsulta = new ConsultaDetalleBeneficiarioRippleTO();
		datosConsulta.setRutEmpresa(clienteMB.getRut());
		datosConsulta.setDvEmpresa(clienteMB.getDigitoVerif());
		datosConsulta.setIdentificadorBeneficiario(identificadorBeneficiario);
		datosConsulta.setCodIso(codMoneda);
		datosConsulta.setPaisBanco(paisBanco);

		return transferenciasRippleUtilityMB.obtenerDetalleBeneficiarioRipple(datosConsulta);
	}

	/**
	 * Obtiene el detalle de la transferencia del beneficiario.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 25/03/2019, Sergio Bustos B. (Everis) - Minheli Mejias (Ing. Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param transferencia
	 *            Objeto de negocio de transferencias.
	 * @throws GeneralException
	 *             Excepci�n de servicio de transferencias.
	 * @since 1.0
	 */
	public void consultarDetalleTransferencia(TransferenciaTO transferencia) throws GeneralException {
		ConsultaTransferenciaTO consultaTransferenciaTO = new ConsultaTransferenciaTO();
		consultaTransferenciaTO.setRutEmpresa(this.getRutEmpresa());
		consultaTransferenciaTO.setServicioSolicitado(
				TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, SERVICIO_SOLICITADO_KEY, "PENDIENTES"));
		consultaTransferenciaTO.setTipoUsuario(transferenciasRippleUtilityMB.obtenerTipoUsuario());

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[consultarDetalleTransferencia] consulta["
					+ StringUtil.contenidoDe(consultaTransferenciaTO) + "]");
		}
		if ((consultaTransferenciaTO.getTipoUsuario().length() > 0)
				&& (consultaTransferenciaTO.getRutEmpresa().length() > 0)) {
			transferencia.setConsulta(consultaTransferenciaTO);
		}

		this.transferenciasRippleUtilityMB.consultarDetalleTransferencia(transferencia);
	}

	/**
	 * Obtiene el detalle de la transferencia del beneficiario.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 25/03/2019, Sergio Bustos B. (Everis) - Minheli Mejias (Ing. Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @throws GeneralException
	 *             Excepci�n de servicio de transferencias.
	 * @since 1.0
	 */
	private List<TransferenciaTO> consultarTransferenciasPendientes() throws GeneralException {
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[consultarTransferenciasPendientes][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
					+ "] [BCI_INI]");
		}
		boolean esApoderado = false;

		ConsultaTransferenciaTO consultaTransferenciaTO = new ConsultaTransferenciaTO();
		consultaTransferenciaTO.setRutEmpresa(this.getRutEmpresa());
		consultaTransferenciaTO.setServicioSolicitado(
				TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, SERVICIO_SOLICITADO_KEY, "PENDIENTES"));
		consultaTransferenciaTO.setTipoUsuario(transferenciasRippleUtilityMB.obtenerTipoUsuario());
		try {
			esApoderado = autorizadorUtilityMB.esPermitido(VISTA, ACCION);
		} catch (Exception e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error(
						"[consultarTransferenciasPendientes][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
								+ "] [BCI_FINEX][Exception]Error con mensaje " + e.getMessage(),
						e);
			}
			throw new GeneralException("ESPECIAL", e.getMessage());
		}

		List<TransferenciaTO> tefPendientes = transferenciasRippleUtilityMB
				.obtenerTransferenciasPedientes(consultaTransferenciaTO, true, esApoderado);

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[consultarTransferenciasPendientes][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
					+ "][BCI_FINOK]");
		}
		return tefPendientes;
	}

	/**
	 * Consulta servicio de transferencias realizadas.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 15/10/2013, Mauricio Flores S�enz (Imagemaker IT): versi�n
	 * inicial</li>
	 * <li>1.1 24-06-2014, Luis L�pez Alamos (SEnTRA) : Se modifica obtencion de
	 * tipo de Usuario</li>
	 * </ul>
	 * </p>
	 * 
	 * @return List<TransferenciaTO>
	 * @throws GeneralException
	 *             Excepcion de negocio.
	 * @since 1.0
	 */
	private List<TransferenciaTO> consultarTransferenciasRealizadas() throws GeneralException {
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[consultarTransferenciasRealizadas]["
					+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + "] [BCI_INI]");
		}
		ConsultaTransferenciaTO consultaTransferenciaTO = new ConsultaTransferenciaTO();
		consultaTransferenciaTO.setRutEmpresa(this.getRutEmpresa());
		consultaTransferenciaTO.setServicioSolicitado(
				TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, SERVICIO_SOLICITADO_KEY, "REALIZADAS"));
		consultaTransferenciaTO.setTipoUsuario(transferenciasRippleUtilityMB.obtenerTipoUsuario());

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[consultarTransferenciasRealizadas]["
					+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + "] [BCI_FINOK]");
		}
		return transferenciasRippleUtilityMB.obtenerTransferencias(consultaTransferenciaTO, false, false);
	}

	/**
	 * Se genera TO CambiaEstadoTttfMonexTO.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 25/02/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing.
	 * Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param tipUsr
	 *            Tipo de usuario.
	 * @param numCorr
	 *            N�mero de correlativo.
	 * @param sysTimeStamp
	 *            Fecha de transacci�n.
	 * @return CambiaEstadoTttfMonexTO
	 * @since 1.0
	 */
	private CambiaEstadoTttfMonexTO getCambiaEstadoTttfMonexTO(String tipUsr, int numCorr, String sysTimeStamp) {
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[getCambiaEstadoTttfMonexTO][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
					+ "] [BCI_INI]");
		}
		CambiaEstadoTttfMonexTO cambiaEstadoTttfMonexTO = new CambiaEstadoTttfMonexTO();
		cambiaEstadoTttfMonexTO.setOrigenSolicitud(ORIGEN_SOLICITUD);
		cambiaEstadoTttfMonexTO.setTipoUsuario(tipUsr);
		cambiaEstadoTttfMonexTO.setNumeroCorrelativo(numCorr);
		cambiaEstadoTttfMonexTO.setSystimestamp(sysTimeStamp);
		cambiaEstadoTttfMonexTO.setUsuarioEvento(this.getUsuarioClienteModelMB().getUsuarioModelMB().getRut() + ""
				+ this.getUsuarioClienteModelMB().getUsuarioModelMB().getDigitoVerif());
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[getCambiaEstadoTttfMonexTO][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
					+ "] [BCI_FINOK]");
		}
		return cambiaEstadoTttfMonexTO;
	}

	/**
	 * Obtiene rut empresa desde clienteMB.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 25/02/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing.
	 * Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @return String
	 * @throws GeneralException
	 *             Excepcion negocio.
	 * @since 1.0
	 */
	private String getRutEmpresa() throws GeneralException {
		String rut = "";
		try {
			rut = clienteMB.getRut() + "" + clienteMB.getDigitoVerif();
		} catch (Exception e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[getRutEmpresa][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ "] [BCI_FINEX][Exception]Error con mensaje " + e.getMessage(), e);
			}
			throw new GeneralException("ESPECIAL", e.getMessage());
		}
		return rut;
	}

	/**
	 * Setea datos a transferenciaMonexTO para realizar la aceptaci�n de la
	 * transferencia.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 125/02/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing.
	 * Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param transferenciaTO
	 *            Entidad de negocio.
	 * @return TransferenciaMonexTO
	 * @throws Exception
	 *             Excepcion general.
	 * @since 1.0
	 */
	private TransferenciaMonexTO getTransferenciaMonexTO(TransferenciaTO transferenciaTO) throws Exception {
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug(
					"[getTransferenciaMonexTO][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "] [BCI_INI]");
		}
		TransferenciaMonexTO transferenciaMonex = new TransferenciaMonexTO();
		transferenciaMonex.setBancoBeneficiario(transferenciaTO.getBancoBeneficiario().getCodigoSwift());
		transferenciaMonex.setBicBancoIntermediario(transferenciaTO.getBancoIntermediario().getCodigoSwift());
		transferenciaMonex.setCodigoCambio(transferenciaTO.getCodigoCambio().getCodigo());
		transferenciaMonex.setCodigoIdentificador(transferenciaTO.getBeneficiario().getReferencia());
		transferenciaMonex.setCodigoProducto(
				TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "codigosMoneda", "codigoProducto"));
		transferenciaMonex.setCtaCte(transferenciaTO.getCuentaOrigen().getNumeroCuenta());
		transferenciaMonex.setCtacteDebito(transferenciaTO.getCuentaCargoComision().getNumeroCuenta());
		transferenciaMonex.setCuentaCorriente(transferenciaTO.getCuentaOrigen().getNumeroCuenta());
		transferenciaMonex.setDetalleGastos(transferenciaTO.getMotivoPago());
		transferenciaMonex.setEmail1(transferenciaTO.getBeneficiario().getMail());
		transferenciaMonex.setEmail2(transferenciaTO.getBeneficiario().getMail());
		transferenciaMonex.setEmail3(transferenciaTO.getBeneficiario().getMail());
		transferenciaMonex.setFechaOperacion(new Date());
		transferenciaMonex.setInformacionRemesa(transferenciaTO.getMotivoPago());
		transferenciaMonex.setIpMaquina("");
		transferenciaMonex.setMonedaCtacte(transferenciaTO.getCuentaOrigen().getMoneda().getCodigo());
		transferenciaMonex.setMonedaNomina(transferenciaTO.getCuentaOrigen().getMoneda().getCodigo());
		transferenciaMonex.setMontoDebito(new BigDecimal(transferenciaTO.getMontoTransferir()));
		transferenciaMonex.setNombreBeneficiarioL1(transferenciaTO.getBeneficiario().getNombre());
		transferenciaMonex.setNombreBeneficiarioL2(transferenciaTO.getBeneficiario().getDireccion());
		transferenciaMonex.setNombreBeneficiarioL3(transferenciaTO.getBeneficiario().getDireccion());
		transferenciaMonex.setNombreBeneficiarioL4(transferenciaTO.getBeneficiario().getDireccion());
		transferenciaMonex.setNombreEmpresa(usuarioClienteModelMB.getClienteMB().getRazonSocial());
		transferenciaMonex.setNomDirBancoL1(transferenciaTO.getBancoBeneficiario().getNombreDireccion());
		transferenciaMonex.setNomDirBancoL2(transferenciaTO.getBancoBeneficiario().getNombreDireccion());
		transferenciaMonex.setNomDirBancoL3(transferenciaTO.getBancoBeneficiario().getNombreDireccion());
		transferenciaMonex.setNomDirBancoL4(transferenciaTO.getBancoBeneficiario().getNombreDireccion());
		transferenciaMonex.setNomDirIntermediarioL1(transferenciaTO.getBancoIntermediario().getNombreDireccion());
		transferenciaMonex.setNomDirIntermediarioL2(transferenciaTO.getBancoIntermediario().getNombreDireccion());
		transferenciaMonex.setNomDirIntermediarioL3(transferenciaTO.getBancoIntermediario().getNombreDireccion());
		transferenciaMonex.setNomDirIntermediarioL4(transferenciaTO.getBancoIntermediario().getNombreDireccion());
		transferenciaMonex.setNumeroCorrelativoFbp(transferenciaTO.getCorrelativoInterno());
		transferenciaMonex.setOrigenSolicitud(ORIGEN_SOLICITUD);
		transferenciaMonex.setPaisBeneficiario(transferenciaTO.getBeneficiario().getPaisCuenta().getCodigo());
		transferenciaMonex.setRutaPago(transferenciaTO.getBancoBeneficiario().getNumeroCuenta());
		transferenciaMonex.setRutaPagoIntermediario(transferenciaTO.getBancoIntermediario().getNumeroCuenta());
		transferenciaMonex.setRutEmpresa(usuarioClienteModelMB.getUsuarioModelMB().getRut() + "-"
				+ usuarioClienteModelMB.getUsuarioModelMB().getDigitoVerif());
		transferenciaMonex.setServicioSolicitado(SERVICIO_SOLICITADO_CUR);
		transferenciaMonex.setSystimestamp(transferenciaTO.getSysTimeStamp());
		transferenciaMonex.setTipoOperacion(
				TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, TRANSFERENCIA_PARAMETROS, "tipoOperacion"));
		transferenciaMonex.setUsuario(this.getUsuarioClienteModelMB().getUsuarioModelMB().getRut() + ""
				+ this.getUsuarioClienteModelMB().getUsuarioModelMB().getDigitoVerif());
		transferenciaMonex.setValutaPago(transferenciaTO.getValuta().getCodigo());
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[getTransferenciaMonexTO][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
					+ "] [BCI_FINOK]");
		}
		return transferenciaMonex;
	}

	/**
	 * Journaliza operaciones de transfer monex.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 21/10/2013, Mauricio Flores S�enz (Imagemaker IT): versi�n
	 * inicial</li>
	 * <li>1.1 30/09/2015, Eric Mart�nez (TINet) - Oliver Hidalgo (Ing. Soft.
	 * BCI): Se agregan m�s datos a la journalizaci�n de la operaci�n</li>
	 * </ul>
	 * </p>
	 * 
	 * @param codigoEvento
	 *            C�digo de evento (CREAR | TRANSLINEA | MODIFICA | ELIMINAR).
	 * @param subCodigoEvento
	 *            Sub c�digo evento (OK | NOK | ERROR).
	 * @param idProducto
	 *            Id de producto (TRANMX).
	 * @param estadoEvento
	 *            Estado Evento (P | R).
	 * @param monto
	 *            Monto de transferencia.
	 * @param destinatario
	 *            Destinatario transferencia.
	 * @param numeroOperacion
	 *            N�mero de operaci�n.
	 * @since 1.0
	 */
	private void journalizar(String codigoEvento, String subCodigoEvento, String idProducto, String estadoEvento,
			String monto, String destinatario, String numeroOperacion) {
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[journalizar][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "] [BCI_INI]");
		}
		try {
			String campoVariable = null;
			if (monto != null && destinatario != null) {
				campoVariable = monto + "|" + destinatario;
			}
			HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
					.getRequest();
			String idMedio = request.getHeader("X-FORWARDED-FOR");
			if (idMedio == null) {
				idMedio = request.getRemoteAddr();
			}
			Eventos evento = new Eventos();
			evento.setCodEventoNegocio(codigoEvento);
			evento.setSubCodEventoNegocio(subCodigoEvento);
			evento.setIdProducto(idProducto);
			evento.setEstadoEventoNegocio(estadoEvento);
			evento.setRutCliente(String.valueOf(this.getClienteMB().getRut()));
			evento.setDvCliente(String.valueOf(this.getClienteMB().getDigitoVerif()));
			evento.setRutOperadorCliente(String.valueOf(usuarioClienteModelMB.getUsuarioModelMB().getRut()));
			evento.setDvOperadorCliente(String.valueOf(usuarioClienteModelMB.getUsuarioModelMB().getDigitoVerif()));
			evento.setIdCanal(this.getUsuarioClienteModelMB().getUsuarioModelMB().getSesionMB().getCanalId());
			evento.setIdMedio(idMedio);
			evento.setCampoVariable(campoVariable);
			evento.setClavePrincipal(numeroOperacion);
			if (getLogger().isEnabledFor(Level.DEBUG)) {
				getLogger().debug("[journalizar] Se invoca journalizacion >>" + evento.toString());
			}
			this.getJournalUtilityMB().journalizar(evento);
		} catch (Exception e) {
			if (getLogger().isEnabledFor(Level.WARN)) {
				getLogger().error("[journalizar][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ "] [BCI_FINEX][Exception]Error con mensaje " + e.getMessage(), e);
			}
		}
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[journalizar][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "] [BCI_FINOK]");
		}
	}

	/**
	 * Traspasa informaci�n al objeto transferenciaMonexTO.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 04/03/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing.
	 * Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param transferenciaMonexTO
	 *            Entidad de negocio.
	 * @throws Exception
	 *             Excepci�n general.
	 * @since 1.0
	 */
	private void setDatosTransferenciaTO(TransferenciaMonexTO transferenciaMonexTO) throws Exception {

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug(
					"[setDatosTransferenciaTO][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "] [BCI_INI]");
		}

		final int numeroDecimales = 2;
		Date fechaHoy = new Date();
		transferenciaMonexTO.setRutEmpresa(getRutEmpresa());
		Number monto = NumberFormat.getInstance(new Locale("ES", "CL")).parse(strMontoATransferir);
		this.montoATransferir = new Double(monto.doubleValue());
		BigDecimal montoDecimal = new BigDecimal(getMontoATransferir());
		montoDecimal = montoDecimal.setScale(numeroDecimales, BigDecimal.ROUND_HALF_EVEN);
		transferenciaMonexTO.setServicioSolicitado(SERVICIO_SOLICITADO_CUR);
		transferenciaMonexTO.setNombreEmpresa(clienteMB.getRazonSocial());
		transferenciaMonexTO.setMonedaNomina(this.getMonedaRipple().getCodigoIso());
		transferenciaMonexTO.setFechaOperacion(fechaHoy);
		transferenciaMonexTO.setCtaCte(this.cuentaOrigen.getNumeroCuenta());
		transferenciaMonexTO.setMontoDebito(montoDecimal);
		/*
		 * transferenciaMonexTO.setValutaPago(this.getValuta().getCodigoValuta()
		 * );
		 */
		transferenciaMonexTO.setMontoPago(montoDecimal);
		transferenciaMonexTO.setDetalleGastos(this.getGastosBancoExteriorTO().getCodigoGastos());
		transferenciaMonexTO.setInformacionRemesa(this.getMotivoPago());
		transferenciaMonexTO.setMonedaCtacte(this.getMonedaOtraCuenta().getCodigoIso());
		transferenciaMonexTO.setCtacteDebito(this.cuentaCargoComision.getNumeroCuenta());
		transferenciaMonexTO.setTipoOperacion(
				TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, TRANSFERENCIA_PARAMETROS, "tipoOperacion"));
		transferenciaMonexTO.setCodigoIdentificador(transferenciasRippleViewMB.getDestinRipple().getIdentificacion());
		transferenciaMonexTO.setPaisBeneficiario(transferenciasRippleViewMB.getDestinRipple().getPaisBanco());
		transferenciaMonexTO.setCuentaCorriente(transferenciasRippleViewMB.getDestinRipple().getCtaCorriente());
		transferenciaMonexTO.setNombreBeneficiarioL1(transferenciasRippleViewMB.getDetBenefRpp().getNombreLineaUno());
		transferenciaMonexTO.setNombreBeneficiarioL2(transferenciasRippleViewMB.getDetBenefRpp().getNombreLineaDos());
		transferenciaMonexTO.setNombreBeneficiarioL3(transferenciasRippleViewMB.getDetBenefRpp().getNombreLineaTres());
		transferenciaMonexTO
				.setNombreBeneficiarioL4(transferenciasRippleViewMB.getDetBenefRpp().getNombreLineaCuatro());
		transferenciaMonexTO.setEmail1(transferenciasRippleViewMB.getDetBenefRpp().getEmail());
		/*
		 * transferenciaMonexTO.setEmail2(transferenciasRippleViewMB.
		 * getDetalleBeneficiarioTO().getEmail2());
		 * transferenciaMonexTO.setEmail3(transferenciasRippleViewMB.
		 * getDetalleBeneficiarioTO().getEmail3());
		 */
		transferenciaMonexTO.setBancoBeneficiario(transferenciasRippleViewMB.getDetBenefRpp().getBanco());
		/*
		 * transferenciaMonexTO.setRutaPago(transferenciasRippleViewMB.
		 * getDetalleBeneficiarioTO().getRutaPago());
		 *
		 *
		 * transferenciaMonexTO.setNomDirBancoL1(transferenciasRippleViewMB.
		 * getDetalleBeneficiarioTO().getNomDirBancoL1());
		 * transferenciaMonexTO.setNomDirBancoL2(transferenciasRippleViewMB.
		 * getDetalleBeneficiarioTO().getNomDirBancoL2());
		 * transferenciaMonexTO.setNomDirBancoL3(transferenciasRippleViewMB.
		 * getDetalleBeneficiarioTO().getNomDirBancoL3());
		 * transferenciaMonexTO.setNomDirBancoL4(transferenciasRippleViewMB.
		 * getDetalleBeneficiarioTO().getNomDirBancoL4());
		 * transferenciaMonexTO.setBicBancoIntermediario(
		 * transferenciasRippleViewMB. getDetBenefRpp().getBicBanco());
		 * transferenciaMonexTO.setRutaPagoIntermediario(
		 * transferenciasRippleViewMB.
		 * getDetalleBeneficiarioTO().getRutaPagoIntermediario());
		 * transferenciaMonexTO.setNomDirIntermediarioL1(
		 * transferenciasRippleViewMB.
		 * getDetalleBeneficiarioTO().getNomDirIntermediarioL1());
		 * transferenciaMonexTO.setNomDirIntermediarioL2(
		 * transferenciasRippleViewMB.
		 * getDetalleBeneficiarioTO().getNomDirIntermediarioL2());
		 * transferenciaMonexTO.setNomDirIntermediarioL3(
		 * transferenciasRippleViewMB.
		 * getDetalleBeneficiarioTO().getNomDirIntermediarioL3());
		 * transferenciaMonexTO.setNomDirIntermediarioL4(
		 * transferenciasRippleViewMB.
		 * getDetalleBeneficiarioTO().getNomDirIntermediarioL4());
		 */
		transferenciaMonexTO.setCodigoCambio(codigoCambio.getCodigoCambio());
		transferenciaMonexTO.setUsuario(this.getUsuarioClienteModelMB().getUsuarioModelMB().getRut() + ""
				+ this.getUsuarioClienteModelMB().getUsuarioModelMB().getDigitoVerif());
		transferenciaMonexTO.setOrigenSolicitud(
				TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "codigosMoneda", "origenSolicitud"));
		transferenciaMonexTO.setCodigoProducto(
				TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "codigosMoneda", "codigoProductoRPP"));
		// transferenciaMonexTO.setForFurther(transferenciasRippleViewMB.getDetalleBeneficiarioTO().getForFurther());
		transferenciaMonexTO.setNumeroCorrelativoFbp(this.getNumeroCorrelativo());
		transferenciaMonexTO.setSystimestamp(this.getSystimestamp());

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[setDatosTransferenciaTO][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
					+ "] [BCI_FINOK]");
		}
	}

	/**
	 * Transferencia de datos a controladores.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>AGREGAR VERSION INICIAL</li>
	 * </ul>
	 * </p>
	 * 
	 * @param transferencia
	 *            Entidad de transferencia.
	 * @throws GeneralException
	 *             Error general.
	 * @since 1.0
	 */
	private void transferirDatos(TransferenciaTO transferencia) throws GeneralException {

		final String nombreMetodo = "transferirDatos";
		final String identificador = (usuarioClienteModelMB != null
				&& usuarioClienteModelMB.getUsuarioModelMB() != null)
						? String.valueOf(usuarioClienteModelMB.getUsuarioModelMB().getRut()) : "";

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "][" + identificador + "] [BCI_INI]");
			getLogger().info("[" + nombreMetodo + "][" + identificador + "] [Parametro][transferencia]["
					+ StringUtil.contenidoDe(transferencia) + "]");
		}

		String identificadorBeneficiario = transferencia.getBeneficiario().getReferencia();
		String codigoMoneda = transferencia.getCuentaOrigen().getMoneda().getCodigo();
		String paisBanco = transferencia.getBeneficiario().getPaisCuenta().getCodigo();
		String rutEmpresa = getRutEmpresa();

		List<Beneficiario1a1TO> beneficiarios = this.consultarBeneficiario(codigoMoneda, rutEmpresa);
		List<ValutaTO> fechas = transferenciasRippleUtilityMB.obtenerFechaValuta(codigoMoneda);
		List<CodigoCambioTO> listaCodigosCambio = this.consultarCodigosCambio(codigoMoneda);

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[" + nombreMetodo + "][" + identificador + "] [rutEmpresa] ["
					+ StringUtil.contenidoDe(rutEmpresa) + "]");
			getLogger().debug("[" + nombreMetodo + "][" + identificador + "] [beneficiarios] ["
					+ StringUtil.contenidoDe(beneficiarios) + "]");
			getLogger().debug(
					"[" + nombreMetodo + "][" + identificador + "] [fechas] [" + StringUtil.contenidoDe(fechas) + "]");
			getLogger().debug("[" + nombreMetodo + "][" + identificador + "] [listaCodigosCambio] ["
					+ StringUtil.contenidoDe(listaCodigosCambio) + "]");

			if (clienteMB != null) {
				getLogger().debug("[" + nombreMetodo + "][" + identificador + "] [clienteMB.rut] ["
						+ StringUtil.contenidoDe(clienteMB.getRut()) + "]");
				getLogger().debug("[" + nombreMetodo + "][" + identificador + "] [clienteMB.digitoVerif] ["
						+ StringUtil.contenidoDe(clienteMB.getDigitoVerif()) + "]");
			} else {
				getLogger().debug("[" + nombreMetodo + "][" + identificador + "] [clienteMB.rut] [null]");
				getLogger().debug("[" + nombreMetodo + "][" + identificador + "] [clienteMB.digitoVerif] [null]");
			}
		}

		for (CodigoCambioTO codigo : listaCodigosCambio) {
			codigo.setDescripcion(codigo.getCodigoCambio() + " - " + codigo.getDescripcion());
		}

		List<CuentaMonexTO> cuentasOrigen = this.transferenciasRippleUtilityMB.getCuentasPorCodigoMoneda(
				clienteMB.getRut(), clienteMB.getDigitoVerif(),
				transferencia.getCuentaOrigen().getMoneda().getCodigo());

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[" + nombreMetodo + "][" + identificador + "] [cuentasOrigen] ["
					+ StringUtil.contenidoDe(cuentasOrigen) + "]");
			getLogger().debug("[" + nombreMetodo + "][" + identificador + "] [listaCodigosCambio] ["
					+ StringUtil.contenidoDe(listaCodigosCambio) + "]");
		}

		this.setSaldoDisponible(transferencia.getCuentaOrigen().getSaldoDisponible());

		DetalleBeneficiarioRippleTO detalleBeneficiarioTO = this.consultarDetalleBeneficiario(identificadorBeneficiario,
				codigoMoneda, paisBanco);

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[" + nombreMetodo + "][" + identificador + "] [cuentasOrigen] ["
					+ StringUtil.contenidoDe(cuentasOrigen) + "]");
			getLogger().debug("[" + nombreMetodo + "][" + identificador + "] [detalleBeneficiarioTO] ["
					+ StringUtil.contenidoDe(detalleBeneficiarioTO) + "]");
		}

		this.transferenciasRippleViewMB.setBeneficiarios(beneficiarios);
		this.transferenciasRippleViewMB.setFechasValutas(fechas);
		this.transferenciasRippleViewMB.setCodigosCambio(listaCodigosCambio);
		this.transferenciasRippleViewMB.getDestinatario()
				.setIdentificacionBeneficiario(transferencia.getBeneficiario().getReferencia());
		this.transferenciasRippleViewMB.setMuestraDetalleBeneficiario(Boolean.TRUE);
		this.transferenciasRippleViewMB.getDestinatario()
				.setPaisBco(transferencia.getBeneficiario().getPaisCuenta().getCodigo());
		this.transferenciasRippleViewMB.getDestinatario()
				.setNombrePais(transferencia.getBeneficiario().getPaisCuenta().getValor());
		this.transferenciasRippleViewMB.getDestinatario()
				.setCuentaCorriente(transferencia.getBeneficiario().getCuenta().getNumeroCuenta());
		this.transferenciasRippleViewMB.getDestinatario()
				.setIdentificacionBeneficiario(transferencia.getBeneficiario().getReferencia());
		this.transferenciasRippleViewMB.setDetBenefRpp(detalleBeneficiarioTO);
		this.transferenciasRippleViewMB.setCuentasOrigen(cuentasOrigen);

		this.getMonedaRipple().setCodigoIso(transferencia.getCuentaOrigen().getMoneda().getCodigo());
		this.getMonedaRipple().setNombreMoneda(transferencia.getCuentaOrigen().getMoneda().getValor());
		this.getMonedaOtraCuenta().setCodigoIso(transferencia.getCuentaCargoComision().getMoneda().getCodigo());

		this.cuentaOrigen.setNumeroCuenta(transferencia.getCuentaOrigen().getNumeroCuenta());
		this.setSimboloMoneda(this.transferenciasRippleUtilityMB
				.obtenerSimboloMoneda(transferencia.getCuentaOrigen().getMoneda().getCodigo()));
		this.setMontoATransferir(transferencia.getMontoTransferir());
		String monto = NumberFormat.getInstance(new Locale("ES", "CL")).format(transferencia.getMontoTransferir());
		this.setStrMontoATransferir(monto);
		this.getGastosBancoExteriorTO().setCodigoGastos(transferencia.getGastoBancoExterior().getCodigo());
		this.getCuentaCargoComision().setNumeroCuenta(transferencia.getCuentaCargoComision().getNumeroCuenta());
		this.setMotivoPago(transferencia.getMotivoPago());
		this.getCodigoCambio().setCodigoCambio(transferencia.getCodigoCambio().getCodigo());

		this.cuentaComisionTo = transferenciasRippleUtilityMB.obtenerCuentaDeCargo(getRutEmpresa());

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[" + nombreMetodo + "][" + identificador + "] [cuentaComisionTo] ["
					+ StringUtil.contenidoDe(cuentaComisionTo) + "]");
		}

		if (this.cuentaComisionTo != null) {
			CuentaMonexTO cuenta = new CuentaMonexTO();
			cuenta.setMoneda(new CodigoValor(cuentaComisionTo.getMonedaCtacte(), cuentaComisionTo.getMonedaCtacte()));
			cuenta.setNumeroCuenta(cuentaComisionTo.getCtaDebito());
			this.monedaOtraCuenta.setCodigoIso(this.cuentaComisionTo.getMonedaCtacte());
			this.transferenciasRippleViewMB
					.setCuentasComision(this.transferenciasRippleUtilityMB.getCuentasPorCodigoMoneda(clienteMB.getRut(),
							clienteMB.getDigitoVerif(), this.monedaOtraCuenta.getCodigoIso()));

			if (getLogger().isEnabledFor(Level.DEBUG)) {
				getLogger().debug("[" + nombreMetodo + "][" + identificador + "] [cuentaComisionTo] ["
						+ StringUtil.contenidoDe(cuentaComisionTo) + "]");
			}

			this.cuentaCargoComision.setNumeroCuenta(cuentaComisionTo.getCtaDebito());
		}
		this.setTransferenciasRippleViewMB(transferenciasRippleViewMB);

		this.setNumeroCorrelativo(transferencia.getCorrelativoInterno());
		this.setSystimestamp(transferencia.getFechaActualizacion());

		if (CODIGO_PAIS_CHILE.equalsIgnoreCase(paisBanco)) {
			this.setSelectCodigosCambioDisabled(true);
			this.getCodigoCambio().setCodigoCambio("0");
		}

		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put(TransferenciasRippleViewMB.BEAN_NAME,
				transferenciasRippleViewMB);

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "][" + identificador + "] [BCI_FINOK]");
		}
	}

	/**
	 * Inicializa variables de la pagina ingresarTransferenciaRipple.xhtml.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 05/03/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing.
	 * Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @since 1.0
	 */
	public void initialize() {
		final String nombreMetodo = "initialize";
		final String identificador = (usuarioClienteModelMB != null
				&& usuarioClienteModelMB.getUsuarioModelMB() != null)
						? String.valueOf(usuarioClienteModelMB.getUsuarioModelMB().getRut()) : "";

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "][" + identificador + "] [BCI_INI]");
		}

//		Calendar fechaSistema = Calendar.getInstance();
//
//		// Validaciones para d�as feriados
//		String pattern = TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "feriados", "pattern");
//
//		int yearD = fechaSistema.get(Calendar.YEAR);
//		String feriados = TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "feriados", "dias" + yearD);
//
//		String[] arregloFeriados = feriados.split(" ");
//		Date fechaDate = fechaSistema.getTime();
//		SimpleDateFormat dateFormatter = new SimpleDateFormat(pattern);
//		for (int i = 0; i < arregloFeriados.length; i++) {
//			if (arregloFeriados[i].equals(dateFormatter.format(fechaDate))) {
//				this.errorFeriado = true;
//				break;
//			}
//		}
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "][" + identificador + "] [BCI_FINOK]");
		}
	}

	public boolean isInit() {
		return init;
	}

	public void setInit(boolean init) {
		this.init = init;
	}

	/**
	 * M�todo encargado de generar el objeto PDF para el comprobante de
	 * transferencia monex.
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * 
	 * <li>1.0 12/03/2019, Sergio Bustos B. (Everis) - Minheli Mejias (Ing.
	 * Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * <p>
	 * 
	 * @param correlativoInterno
	 *            Identificador de transferencia monex.
	 * @return Objeto PDF del comprobante de transferencia.
	 * @since 1.4
	 */
	public ByteArrayOutputStream generarObjetoPDFComprobanteTransferencia(int correlativoInterno) {
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[generarObjetoPDFComprobante][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
					+ "][BCI_INI] entrando a generarObjetoPDFComprobante.");
		}
		TransferenciaTO transferencia = this.getTransferenciaSelected();

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		try {
			ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext()
					.getContext();

			DecimalFormat decimalFormat = new DecimalFormat("###,###.00");

			builder = factory.newDocumentBuilder();
			Document document = builder.newDocument();
			Element root = (Element) document.createElement(CODIGO_COMPROBANTE_TRANSF);
			document.appendChild(root);

			String rutaImagenLogo = servletContext.getRealPath("/")
					+ TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, XSL, CODIGO_IMAGEN);
			String rutaXsl = servletContext.getRealPath("/")
					+ TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, XSL, CODIGO_VALOR);
			String mensajeTransferencia = TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE,
					"textoInfoTransferencia", "mensajeTransferenciaExitosa");

			String monto = decimalFormat.format(transferencia.getMontoTransferir());

			if (this.getTransferenciasRippleViewMB().isPedienteFirma()) {
				root.appendChild(ConvierteXml.generaNodo(document, "pendiente",
						TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "textoInfoTransferencia", "pendiente")));
				root.appendChild(ConvierteXml.generaNodo(document, "apoderados", TablaValores
						.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "textoInfoTransferencia", "apoderados")));
				rutaImagenLogo = servletContext.getRealPath("/")
						+ TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "textoInfoTransferencia", "imagen");
				rutaXsl = servletContext.getRealPath("/")
						+ TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "textoInfoTransferencia", "xslPath");
				mensajeTransferencia = TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "textoInfoTransferencia",
						"mensajeTransferenciaPendiente");

				ResultConsultarApoderadosquehanFirmado result = this.transferenciasRippleUtilityMB
						.consultarApoderadosQueHanFirmado(transferencia.getCorrelativoInterno(),
								this.transferenciasRippleUtilityMB.getDatosAutorizacionTO(),
								this.transferenciasRippleUtilityMB.getEventos());
				ConApoquehanFirmado[] apoderados = result.getConApoquehanFirmado();
				if (apoderados != null) {
					for (int i = 0; i < apoderados.length; i++) {
						String nombreCompleto = apoderados[i].getNombres() + " " + apoderados[i].getApellidoPaterno()
								+ " " + apoderados[i].getApellidoMaterno();
						Element apoderado = document.createElement("listaApoderado");
						apoderado.appendChild(ConvierteXml.generaNodo(document, "nombreApoderado", nombreCompleto));
						root.appendChild(apoderado);
					}
				}
			}

			root.appendChild(ConvierteXml.generaNodo(document, "mensaje", mensajeTransferencia));
			root.appendChild(ConvierteXml.generaNodo(document, "nombreBeneficiario",
					transferencia.getBeneficiario().getNombre()));
			root.appendChild(ConvierteXml.generaNodo(document, "numeroCuenta",
					String.valueOf(transferencia.getBeneficiario().getCuenta().getNumeroCuenta())));
			root.appendChild(ConvierteXml.generaNodo(document, "montoTransferencia", this.transferenciasRippleUtilityMB
					.getResourceValue(transferencia.getCuentaOrigen().getMoneda().getCodigo()) + " " + monto));
			root.appendChild(ConvierteXml.generaNodo(document, "cuentaCargo",
					String.valueOf(transferencia.getCuentaCargoComision().getNumeroCuenta())));
			root.appendChild(ConvierteXml.generaNodo(document, "valuta", transferencia.getValuta().getValor()));
			root.appendChild(ConvierteXml.generaNodo(document, "email", transferencia.getBeneficiario().getMail()));

			if (getLogger().isEnabledFor(Level.DEBUG)) {
				getLogger().debug("[generaComprobanteAumentoCupo] rutCliente:"
						+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + " rutaXsl: " + rutaXsl
						+ " rutaImagenLogo: " + rutaImagenLogo);
			}

			ConvierteXml xml = new ConvierteXml();
			Transformer transformer = TransformerFactory.newInstance().newTransformer(new StreamSource(rutaXsl));
			transformer.setParameter("img1", rutaImagenLogo);
			return xml.transformaXslContrato(document, transformer);
		} catch (Exception e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[generarObjetoPDFComprobanteTransferencia]["
						+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + "][BCI_FINEX][Exception] "
						+ "Error al generar PDF. Y mensaje: " + e.getMessage(), e);
			}
			throw new SistemaException(e);
		}
	}

	/**
	 * M�todo encargado de crear una instacia del EJB NominaDeTransferencias.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 05/03/2019, Sergio Bustos B. (Everis) - Minheli Mejias (Ing.
	 * Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * <p>
	 * 
	 * @return Instancia de ejb nominaDeTransferencias.
	 * @throws Exception
	 *             en caso de error.
	 * @since 1.9
	 */
	private NominaDeTransferencias crearEJBNominaDeTransferencias() throws Exception {
		if (getLogger().isInfoEnabled()) {
			getLogger().info("[crearEJBNominaDeTransferencias][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
					+ "][BCI_INI] Inicio metodo.");
		}
		try {
			EnhancedServiceLocator locator = EnhancedServiceLocator.getInstance();
			NominaDeTransferenciasHome nominaDeTransferenciasHome = (NominaDeTransferenciasHome) locator
					.getGenericService(TablaValores.getValor(ARCHIVO_PARAMETROS, "JNDI_NOM", "Desc"),
							NominaDeTransferenciasHome.class);
			if (getLogger().isInfoEnabled()) {
				getLogger()
						.info("[crearEJBNominaDeTransferencias][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
								+ "][BCI_FINOK] retornando instancia del EJB.");
			}
			return nominaDeTransferenciasHome.create();
		} catch (Exception e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[crearEJBNominaDeTransferencias]["
						+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + "][BCI_FINEX][" + e.getClass()
						+ "] error en instancia, mensaje=<" + e.getMessage() + ">", e);
			}
			throw new GeneralException("ESPECIAL", "Error al crear EJB crearEJBNominaDeTransferencias");
		}
	}

	/**
	 * Inicializa variables de la pagina confirmarTransferenciaRipple.xhtml.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 05/03/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing.
	 * Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @since 1.0
	 */
	public void initializeConfirmar() {

		final String nombreMetodo = "initializeConfirmar";
		final String identificador = (usuarioClienteModelMB != null
				&& usuarioClienteModelMB.getUsuarioModelMB() != null)
						? String.valueOf(usuarioClienteModelMB.getUsuarioModelMB().getRut()) : "";

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "][" + identificador + "] [BCI_INI]");
		}

		try {
			/*
			 * getLogger().
			 * info("Danaro: initializeconfirmar monto a transferir: "+this.
			 * strMontoATransferir); this.strMontoATransferir =
			 * this.strMontoATransferir.replace(',', '.'); Double montoPaso =
			 * Double.valueOf(this.strMontoATransferir);
			 * this.transferenciasRippleViewMB.setMontoTransferencia(BigDecimal.
			 * valueOf( montoPaso));
			 */

			String rutEmpresa = getRutEmpresa();
			this.cuentaComisionTo = transferenciasRippleUtilityMB.obtenerCuentaDeCargo(rutEmpresa);

			if (getLogger().isEnabledFor(Level.DEBUG)) {
				getLogger().debug("[" + nombreMetodo + "][" + identificador + "] [rutEmpresa] ["
						+ StringUtil.contenidoDe(rutEmpresa) + "]");
				getLogger().debug(
						"[" + nombreMetodo + "][" + identificador + "] [init] [" + StringUtil.contenidoDe(init) + "]");
				getLogger().debug("[" + nombreMetodo + "][" + identificador + "] [cuentaComisionTo] ["
						+ StringUtil.contenidoDe(cuentaComisionTo) + "]");
			}

			if (!this.init && this.cuentaComisionTo != null) {
				CuentaMonexTO cuenta = new CuentaMonexTO();
				cuenta.setMoneda(
						new CodigoValor(cuentaComisionTo.getMonedaCtacte(), cuentaComisionTo.getMonedaCtacte()));
				cuenta.setNumeroCuenta(cuentaComisionTo.getCtaDebito());

				if (getLogger().isEnabledFor(Level.DEBUG)) {
					getLogger().debug("[" + nombreMetodo + "][" + identificador + "] [cuenta] ["
							+ StringUtil.contenidoDe(cuenta) + "]");
				}

				this.monedaOtraCuenta.setCodigoIso(this.cuentaComisionTo.getMonedaCtacte());
				this.transferenciasRippleViewMB.setCuentasComision(
						this.transferenciasRippleUtilityMB.getCuentasPorCodigoMoneda(clienteMB.getRut(),
								clienteMB.getDigitoVerif(), this.monedaOtraCuenta.getCodigoIso()));
				this.cuentaCargoComision.setNumeroCuenta(cuentaComisionTo.getCtaDebito());

				if (getLogger().isEnabledFor(Level.DEBUG)) {
					getLogger().debug("[" + nombreMetodo + "][" + identificador
							+ "] [transferenciasMonexViewMB.cuentasComision] ["
							+ StringUtil.contenidoDe(transferenciasRippleViewMB.getCuentasComision()) + "]");
					getLogger().debug("[" + nombreMetodo + "][" + identificador + "] [cuentaCargoComision] ["
							+ StringUtil.contenidoDe(cuentaCargoComision) + "]");
				}
				this.init = true;
			}

			ObtenerComisionTO obtenerComision = new ObtenerComisionTO();
			generarObtenerComision(obtenerComision);
			this.transferenciasRippleViewMB
					.setComisionRipple(this.transferenciasRippleUtilityMB.obtenerComisiones(obtenerComision));
			getLogger().info("Danaro: se obtuvo comision y se setio el objeto con: "
					+ this.transferenciasRippleViewMB.getComisionRipple());

		} catch (GeneralException e) {
			if (getLogger().isEnabledFor(Level.WARN)) {
				getLogger().warn(
						"[" + nombreMetodo + "][" + identificador + "] [GeneralException][" + e.getMessage() + "]", e);
			}
		}

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "][" + identificador + "] [BCI_FINOK]");
		}
	}

	/**
	 * Generaci�n de objeto con los datos necesarios para obtener la comision
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 04/03/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing.
	 * Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @since 1.0
	 */
	private void generarObtenerComision(ObtenerComisionTO obtenerComision) {
		obtenerComision.setRutEmpresa(String.valueOf(clienteMB.getRut()) + "-" + clienteMB.getDigitoVerif());
		obtenerComision.setMonedaTransf(this.monedaRipple.getCodigoIso());

		strMontoATransferir = strMontoATransferir.replace(".", "");
		strMontoATransferir = strMontoATransferir.replace(",", ".");

		obtenerComision.setMontoPago(strMontoATransferir);
		obtenerComision.setBicBanco(this.transferenciasRippleViewMB.getNodoRipple().getBic());
	}

	/**
	 * M�todo encargado de aceptar la transferencia pendiente.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 08/03/2019, Sergio Bustos B (Everis) - Minheli Mejias (Ing. Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @return String
	 * @since 1.0
	 */
	public String aceptarTransferenciaRipplePen() {

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][BCI_INI]");
		}

		final String identificador = (usuarioClienteModelMB != null
				&& usuarioClienteModelMB.getUsuarioModelMB() != null)
						? String.valueOf(usuarioClienteModelMB.getUsuarioModelMB().getRut()) : "";

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador + "] [BCI_INI]");
		}

		try {
			getTransferenciaPorAutorizar();

			if (getLogger().isEnabledFor(Level.DEBUG)) {
				getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
						+ "] [transferenciaSelected] [" + StringUtil.contenidoDe(getTransferenciaSelected()) + "]");
			}

			RespuestaHorarioTO validarHorario = this.transferenciasRippleUtilityMB.consultarLimiteHorarioIngreso();
			if (getLogger().isEnabledFor(Level.DEBUG)) {
				getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador + "] [horarioTO] ["
						+ StringUtil.contenidoDe(validarHorario) + "]");
			}

			if (validarHorario.getIndicadorResultado() != 0) {

				if (getLogger().isEnabledFor(Level.DEBUG)) {
					getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "] [" + identificador + "] [BCI_FINOK] ["
							+ validarHorario.getMensajeResultado() + "]");
				}

				FacesMessage mensaje = new FacesMessage(validarHorario.getMensajeResultado());
				mensaje.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage("segundaClave", mensaje);
				return null;
			}
		} catch (TtffException e1) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
						+ "] [BCI_FINEX][TtffException][" + e1.getMessage() + "]", e1);
			}

			mbErrorUtilityMB.setMensajeError(TablaValores.getValor(CODIGOS_ERRORES, "BADMSG", "Desc"));
			mbErrorUtilityMB.setOcultaBotonVolver(true);
			return mbErrorUtilityMB.obtenerPaginaError();
		} catch (GeneralException e1) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
						+ "] [BCI_FINEX][GeneralException][" + e1.getMessage() + "]", e1);
			}

			mbErrorUtilityMB.setMensajeError(TablaValores.getValor(CODIGOS_ERRORES, "BADMSG", "Desc"));
			mbErrorUtilityMB.setOcultaBotonVolver(true);
			return mbErrorUtilityMB.obtenerPaginaError();
		}

		this.transferenciasRippleViewMB.setPedienteFirma(true);
		DatosAutorizacionTO autorizacionTO = this.transferenciasRippleUtilityMB.getDatosAutorizacionTO();
		TransferenciaMonexTO transferenciaMonexTO = null;
		Boolean esPermitido;
		final String usuario = usuarioClienteModelMB.getUsuarioModelMB().getRut() + "" + usuarioClienteModelMB.getUsuarioModelMB().getDigitoVerif();

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador + "] [autorizacionTO] ["
					+ StringUtil.contenidoDe(autorizacionTO) + "]");
		}

		try {
			esPermitido = autorizadorUtilityMB.esPermitido(VISTA, ACCION);

			if (getLogger().isEnabledFor(Level.DEBUG)) {
				getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador + "] [esPermitido] ["
						+ StringUtil.contenidoDe(esPermitido) + "]");
			}
		} catch (Exception e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador + "] [BCI_FINEX][Exception]["
						+ e.getMessage() + "]", e);
			}

			mbErrorUtilityMB.setOcultaBotonVolver(true);
			mbErrorUtilityMB.setMensajeError(TablaValores.getValor(CODIGOS_ERRORES, "BADMSG", "Desc"));
			return mbErrorUtilityMB.obtenerPaginaError();
		}
		try {
			this.consultarDetalleTransferencia(getTransferenciaSelected());
			transferenciaMonexTO = getTransferenciaMonexTO(getTransferenciaSelected());

			if (getLogger().isEnabledFor(Level.DEBUG)) {
				getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador + "] [transferenciaMonexTO] ["
						+ StringUtil.contenidoDe(transferenciaMonexTO) + "]");
			}
		} catch (Exception e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador + "] [BCI_FINEX][Exception]["
						+ e.getMessage() + "]", e);
			}

			mbErrorUtilityMB.setOcultaBotonVolver(true);
			mbErrorUtilityMB.setMensajeError(TablaValores.getValor(CODIGOS_ERRORES, "BADMSG", "Desc"));
			return mbErrorUtilityMB.obtenerPaginaError();
		}
		try {

			if (getLogger().isEnabledFor(Level.DEBUG)) {
				getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
						+ "] [transferenciasRippleUtilityMB.obtenerTipoUsuario] ["
						+ StringUtil.contenidoDe(transferenciasRippleUtilityMB.obtenerTipoUsuario()) + "]");
				getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador + "] [transferenciaMonexTO] ["
						+ StringUtil.contenidoDe(transferenciaMonexTO) + "]");
			}

			CambiaEstadoTttfMonexTO cambiaEstadoTttfMonexTO = getCambiaEstadoTttfMonexTO(
					transferenciasRippleUtilityMB.obtenerTipoUsuario(), transferenciaMonexTO.getNumeroCorrelativoFbp(),
					transferenciaMonexTO.getSystimestamp());

			if (getLogger().isEnabledFor(Level.DEBUG)) {
				getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
						+ "] [cambiaEstadoTttfMonexTO] [" + StringUtil.contenidoDe(cambiaEstadoTttfMonexTO) + "]");
				getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador + "] [esPermitido] ["
						+ esPermitido.booleanValue() + "]");
			}

			if (esPermitido.booleanValue()) {
				try {
					boolean segundaClaveValida = false;

					if (segundaClave != null) {
						segundaClaveValida = segundaClave.verificarAutenticacion();
					} else {
						segundaClaveMB.setServicio("TransferenciasMxUnoAUno");
						CamposDeLlaveTO camposLlave = null;
						segundaClaveMB.validarSegundaClave(segundaClaveValor, camposLlave);
						segundaClaveValida = true;
					}

					if (getLogger().isEnabledFor(Level.DEBUG)) {
						getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
								+ "] [segundaClaveValida] [" + segundaClaveValida + "]");
					}

					if (segundaClaveValida) {
						boolean esApoderadoReal = autorizadorUtilityMB.esPermitido("validacionFirmaYPoderes", ACCION);
						String perfil = usuarioClienteModelMB.getUsuarioModelMB().getPerfil();
						String tipoUsu = transferenciasRippleUtilityMB.obtenerTipoUsuario();
						String perfilValida = TablaValores.getValor(ARCH_PARAM_VAL, "tipoPerfilValido", "Desc");
						String tipoUsuarioValido = TablaValores.getValor(ARCH_PARAM_VAL, "tipoUsuarioValido", "Desc");
						String flagvalidarRut50MM = TablaValores.getValor(ARCH_PARAM_VAL, "flagValidaRutMenor50MM",
								"flag");
						
						if (getLogger().isEnabledFor(Level.DEBUG)) {
							getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "] perfilValida[" + perfilValida + "],"
									+ "tipoUsuarioValido[" + tipoUsuarioValido + "]," + "flagvalidarRut50MM["
									+ flagvalidarRut50MM + "]," + "tipoUsuarioValido[" + tipoUsuarioValido + "]");
						}
						if (perfilValida == null && tipoUsuarioValido == null && flagvalidarRut50MM == null) {
							if (getLogger().isEnabledFor(Level.ERROR)) {
								getLogger().error("[" + ACEPTAR_TRANSFERENCIAS_PEN
										+ "][BCI_FINOK][ERROR][Problema tabla de parametros]");
							}
							throw new GeneralException(ERROR_GENERICO_FYP);
						}

						StringTokenizer stTok = new StringTokenizer(tipoUsuarioValido, ",");
						boolean tipoUsuValido = false;
						while (stTok.hasMoreTokens()) {
							if (tipoUsu.trim().equals(stTok.nextToken())) {
								tipoUsuValido = true;
							}
						}
						if (getLogger().isEnabledFor(Level.DEBUG)) {
							getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "] esApoderadoReal[" + esApoderadoReal
									+ "]," + "perfil[" + perfil + "]," + "tipoUsu[" + tipoUsu + "]," + "tipoUsuValido["
									+ tipoUsuValido + "]");
						}
						if (esApoderadoReal == false && tipoUsuValido && perfil.equalsIgnoreCase(perfilValida)
								&& Integer.parseInt(flagvalidarRut50MM) == NUM_UNO) {

							if (getLogger().isEnabledFor(Level.DEBUG)) {
								getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "] cambiaEstadoTttfMonexTO["
										+ cambiaEstadoTttfMonexTO + "]");
							}

							FirmarEliminarTransferenciaRipple firmar = new FirmarEliminarTransferenciaRipple();
							firmar.setUsuario(usuario);
							firmar.setCorrelativoFbp(getTransferenciaSelected().getCorrelativoInterno());
							firmar.setSysTimeStamp(getTransferenciaSelected().getSysTimeStamp());
							firmar.setAccion(TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "accionSolicitada", "aprobar"));
							
							DatosHashTO datoshash = new DatosHashTO();
							datoshash.setCuentaOrigen(getTransferenciaSelected().getCuentaOrigen().getNumeroCuenta());
							datoshash.setMonto(String.valueOf(getTransferenciaSelected().getMontoTransferir()));
							datoshash.setMoneda(getTransferenciaSelected().getCodigoMonedaITF());
							datoshash.setIdBeneficiario(getTransferenciaSelected().getBeneficiario().getReferencia());
							datoshash.setCuentaDestino(getTransferenciaSelected().getCuentaCorrienteBene());
							firmar.setDatosHash(datoshash);
							cambiaEstadoTttfMonexTO.setSystimestamp(getTransferenciaSelected().getSysTimeStamp());
							cambiaEstadoTttfMonexTO.setServicioSolicitado(TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "accionSolicitada", "aprobar"));
							this.transferenciasRippleUtilityMB.firmarOEliminarTransferenciaRipple(firmar);
							this.transferenciasRippleViewMB.setPedienteFirma(false);

							if (getLogger().isEnabledFor(Level.DEBUG)) {
								getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "] timestamp[" + getTransferenciaSelected().getSysTimeStamp() + "],"
										+ "cambiaEstadoTttfMonexTO[" + cambiaEstadoTttfMonexTO + "]");
							}

							String numAutorizacion = (String.valueOf(transferenciaMonexTO.getNumeroCorrelativoFbp()));

							Date pagFecCreacion = new Date();
							NominaTO nomina = new NominaTO();
							nomina.setRutApoderado(autorizacionTO.getRutUsuario());
							nomina.setDigitoVerificadorApoderado(autorizacionTO.getDvUsuario());
							nomina.setNumeroConvenioBANELE(autorizacionTO.getConvenio());
							nomina.setRutEmpresa(autorizacionTO.getRutEmpresa());
							nomina.setDigitoVerificadorEmpresa(autorizacionTO.getDvEmpresa());
							nomina.setNumeroFolio(numAutorizacion);
							boolean resRegistroApoVirtual;
							try {
								if (getLogger().isEnabledFor(Level.DEBUG)) {
									getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "] nomina[" + nomina + "],"
											+ "pagFecCreacion[" + pagFecCreacion + "]," + "numero[" + NUM_UNO + "]");
								}

								resRegistroApoVirtual = crearEJBServiciosDePagosMasivos().registraApoVirtual(nomina,
										pagFecCreacion, NUM_UNO);
								if (resRegistroApoVirtual == false) {
									if (getLogger().isEnabledFor(Level.ERROR)) {
										getLogger().error("[" + ACEPTAR_TRANSFERENCIAS_PEN
												+ "][BCI_FINOK][ERROR][Imposible registrar la firma]");
									}
									throw new GeneralException(ERROR_NO_REGISTR_FYP);
								}
							} catch (RemoteException e) {
								if (getLogger().isEnabledFor(Level.ERROR)) {
									getLogger().error("[" + ACEPTAR_TRANSFERENCIAS_PEN
											+ "][BCI_FINEX][RemoteException][" + e.getMessage() + "]", e);
								}
								throw new GeneralException(ERROR_GENERICO_FYP);
							}

						} else {
							if (getLogger().isEnabledFor(Level.DEBUG)) {
								getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
										+ "] [esApoderadoReal] [" + esApoderadoReal + "]");
								getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
										+ "] [transferenciasRippleUtilityMB.eventos] ["
										+ transferenciasRippleUtilityMB.getEventos() + "]");
							}

							this.transferenciasRippleViewMB.setPedienteFirma(false);

							String subCodigoEvento = JournalTransferMonex.SUBCODIGO_EVENTO_OK.getValue();

							if (getLogger().isEnabledFor(Level.DEBUG)) {
								getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
										+ "] [transferenciasRippleViewMB.pendienteFirma] ["
										+ transferenciasRippleViewMB.isPedienteFirma() + "]");
								getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
										+ "] [subCodigoEvento] [" + subCodigoEvento + "]");
							}

							if (this.transferenciasRippleViewMB.isPedienteFirma()) {
								subCodigoEvento = JournalTransferMonex.SUBCODIGO_EVENTO_FIRMA.getValue();

								ResultConsultarApoderadosquehanFirmado result = this.transferenciasRippleUtilityMB
										.consultarApoderadosQueHanFirmado(
												transferenciaMonexTO.getNumeroCorrelativoFbp(), autorizacionTO,
												this.transferenciasRippleUtilityMB.getEventos());

								if (getLogger().isEnabledFor(Level.DEBUG)) {
									getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
											+ "] [subCodigoEvento] [" + subCodigoEvento + "]");
								}

								ConApoquehanFirmado[] apoderados = result.getConApoquehanFirmado();
								if (apoderados != null) {
									for (int i = 0; i < apoderados.length; i++) {
										String nombreCompleto = apoderados[i].getNombres() + " "
												+ apoderados[i].getApellidoPaterno() + " "
												+ apoderados[i].getApellidoMaterno();

										if (getLogger().isEnabledFor(Level.DEBUG)) {
											getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
													+ "] [apoderados<" + i + ">] ["
													+ StringUtil.contenidoDe(apoderados[i]) + "]");
											getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
													+ "] [nombreCompleto] [" + nombreCompleto + "]");
										}

										this.transferenciasRippleViewMB.getApoderadosQueHanFirmado()
												.add(nombreCompleto);
									}
								}
							}

							if (getLogger().isEnabledFor(Level.DEBUG)) {
								getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
										+ "] [subCodigoEvento] [" + StringUtil.contenidoDe(subCodigoEvento) + "]");
								getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
										+ "] [transferenciaMonexTO] [" + StringUtil.contenidoDe(transferenciaMonexTO)
										+ "]");
								getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
										+ "] [JournalTransferMonex.CODIGO_EVENTO_TRX_LINEA] [" + StringUtil.contenidoDe(
												JournalTransferMonex.CODIGO_EVENTO_TRX_LINEA.getValue())
										+ "]");
								getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
										+ "] [JournalTransferMonex.ID_PRODUCTO] ["
										+ StringUtil.contenidoDe(JournalTransferMonex.ID_PRODUCTO.getValue()) + "]");
								getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
										+ "] [JournalTransferMonex.ESTADO_P] ["
										+ StringUtil.contenidoDe(JournalTransferMonex.ESTADO_P.getValue()) + "]");
								getLogger()
										.debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
												+ "] [JournalTransferMonex.NUMERO_OPERACION] [" + StringUtil
														.contenidoDe(JournalTransferMonex.NUMERO_OPERACION.getValue())
												+ "]");
							}

							this.journalizar(JournalTransferMonex.CODIGO_EVENTO_TRX_LINEA.getValue(), subCodigoEvento,
									JournalTransferMonex.ID_PRODUCTO.getValue(),
									JournalTransferMonex.ESTADO_P.getValue(),
									String.valueOf(transferenciaMonexTO.getMontoPago()),
									transferenciaMonexTO.getNombreBeneficiarioL1(),
									JournalTransferMonex.NUMERO_OPERACION.getValue());
						}
					}
				} catch (SeguridadException ex) {
					if (getLogger().isEnabledFor(Level.ERROR)) {
						getLogger().error("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
								+ "] [BCI_FINEX][SeguridadException][" + ex.getMessage() + "]", ex);
					}

					FacesMessage mensaje = new FacesMessage(ex.getInfoAdic());
					mensaje.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage("segundaClave", mensaje);
					return null;
				}
			} 
			else {
				FirmarEliminarTransferenciaRipple eliminarTransferenciaPen = new FirmarEliminarTransferenciaRipple();
				eliminarTransferenciaPen.setUsuario(usuario);
				eliminarTransferenciaPen.setCorrelativoFbp(getTransferenciaSelected().getCorrelativoInterno());
				eliminarTransferenciaPen.setSysTimeStamp(getTransferenciaSelected().getSysTimeStamp());
				eliminarTransferenciaPen.setAccion(TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "accionSolicitada", "firmar"));
				
				cambiaEstadoTttfMonexTO.setSystimestamp(getTransferenciaSelected().getSysTimeStamp());
				cambiaEstadoTttfMonexTO.setServicioSolicitado(TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "accionSolicitada", "firmar"));
				this.transferenciasRippleViewMB.setPedienteFirma(true);


				if (getLogger().isEnabledFor(Level.DEBUG)) {
					getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
							+ "] [cambiaEstadoTttfMonexTO] [" + StringUtil.contenidoDe(cambiaEstadoTttfMonexTO) + "]");
					getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
							+ "] [JournalTransferMonex.CODIGO_EVENTO_MODIFICAR] ["
							+ StringUtil.contenidoDe(JournalTransferMonex.CODIGO_EVENTO_MODIFICAR.getValue()) + "]");
					getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
							+ "] [JournalTransferMonex.SUBCODIGO_EVENTO_OK] ["
							+ StringUtil.contenidoDe(JournalTransferMonex.SUBCODIGO_EVENTO_OK.getValue()) + "]");
					getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
							+ "] [JournalTransferMonex.ID_PRODUCTO] ["
							+ StringUtil.contenidoDe(JournalTransferMonex.ID_PRODUCTO.getValue()) + "]");
					getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
							+ "] [JournalTransferMonex.ESTADO_P] ["
							+ StringUtil.contenidoDe(JournalTransferMonex.ESTADO_P.getValue()) + "]");
					getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
							+ "] [JournalTransferMonex.NUMERO_OPERACION] ["
							+ StringUtil.contenidoDe(JournalTransferMonex.NUMERO_OPERACION.getValue()) + "]");
				}

				this.journalizar(JournalTransferMonex.CODIGO_EVENTO_MODIFICAR.getValue(),
						JournalTransferMonex.SUBCODIGO_EVENTO_OK.getValue(),
						JournalTransferMonex.ID_PRODUCTO.getValue(), JournalTransferMonex.ESTADO_P.getValue(), null,
						null, JournalTransferMonex.NUMERO_OPERACION.getValue());
			}
		} catch (GeneralException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
						+ "] [BCI_FINEX][GeneralException][" + e.getMessage() + "]", e);
			}

			if (esPermitido.booleanValue()) {

				if (getLogger().isEnabledFor(Level.DEBUG)) {
					getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
							+ "] [JournalTransferMonex.CODIGO_EVENTO_TRX_LINEA] ["
							+ StringUtil.contenidoDe(JournalTransferMonex.CODIGO_EVENTO_TRX_LINEA.getValue()) + "]");
					getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
							+ "] [JournalTransferMonex.SUBCODIGO_EVENTO_NOK] ["
							+ StringUtil.contenidoDe(JournalTransferMonex.SUBCODIGO_EVENTO_NOK.getValue()) + "]");
					getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
							+ "] [JournalTransferMonex.ID_PRODUCTO] ["
							+ StringUtil.contenidoDe(JournalTransferMonex.ID_PRODUCTO.getValue()) + "]");
					getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
							+ "] [JournalTransferMonex.ESTADO_P] ["
							+ StringUtil.contenidoDe(JournalTransferMonex.ESTADO_P.getValue()) + "]");
					getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
							+ "] [JournalTransferMonex.NUMERO_OPERACION] ["
							+ StringUtil.contenidoDe(JournalTransferMonex.NUMERO_OPERACION.getValue()) + "]");
				}

				this.journalizar(JournalTransferMonex.CODIGO_EVENTO_TRX_LINEA.getValue(),
						JournalTransferMonex.SUBCODIGO_EVENTO_NOK.getValue(),
						JournalTransferMonex.ID_PRODUCTO.getValue(), JournalTransferMonex.ESTADO_P.getValue(), null,
						null, JournalTransferMonex.NUMERO_OPERACION.getValue());
			} else {

				if (getLogger().isEnabledFor(Level.DEBUG)) {
					getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
							+ "] [JournalTransferMonex.CODIGO_EVENTO_MODIFICAR] ["
							+ StringUtil.contenidoDe(JournalTransferMonex.CODIGO_EVENTO_MODIFICAR.getValue()) + "]");
					getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
							+ "] [JournalTransferMonex.SUBCODIGO_EVENTO_NOK] ["
							+ StringUtil.contenidoDe(JournalTransferMonex.SUBCODIGO_EVENTO_NOK.getValue()) + "]");
					getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
							+ "] [JournalTransferMonex.ID_PRODUCTO] ["
							+ StringUtil.contenidoDe(JournalTransferMonex.ID_PRODUCTO.getValue()) + "]");
					getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
							+ "] [JournalTransferMonex.ESTADO_P] ["
							+ StringUtil.contenidoDe(JournalTransferMonex.ESTADO_P.getValue()) + "]");
					getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
							+ "] [JournalTransferMonex.NUMERO_OPERACION] ["
							+ StringUtil.contenidoDe(JournalTransferMonex.NUMERO_OPERACION.getValue()) + "]");
				}

				this.journalizar(JournalTransferMonex.CODIGO_EVENTO_MODIFICAR.getValue(),
						JournalTransferMonex.SUBCODIGO_EVENTO_NOK.getValue(),
						JournalTransferMonex.ID_PRODUCTO.getValue(), JournalTransferMonex.ESTADO_P.getValue(), null,
						null, JournalTransferMonex.NUMERO_OPERACION.getValue());
			}
			mbErrorUtilityMB.setOcultaBotonVolver(true);
			mbErrorUtilityMB.setMensajeError(TablaValores.getValor(CODIGOS_ERRORES, "BADMSG", "Desc"));
			return mbErrorUtilityMB.obtenerPaginaError();

		} catch (NegocioException e) {

			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
						+ "] [BCI_FINEX][GeneralException][" + e.getMessage() + "]", e);
			}

			if (esPermitido.booleanValue()) {

				if (getLogger().isEnabledFor(Level.DEBUG)) {
					getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
							+ "] [JournalTransferMonex.CODIGO_EVENTO_TRX_LINEA] ["
							+ StringUtil.contenidoDe(JournalTransferMonex.CODIGO_EVENTO_TRX_LINEA.getValue()) + "]");
					getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
							+ "] [JournalTransferMonex.SUBCODIGO_EVENTO_ERROR] ["
							+ StringUtil.contenidoDe(JournalTransferMonex.SUBCODIGO_EVENTO_ERROR.getValue()) + "]");
					getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
							+ "] [JournalTransferMonex.ID_PRODUCTO] ["
							+ StringUtil.contenidoDe(JournalTransferMonex.ID_PRODUCTO.getValue()) + "]");
					getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
							+ "] [JournalTransferMonex.ESTADO_R] ["
							+ StringUtil.contenidoDe(JournalTransferMonex.ESTADO_R.getValue()) + "]");
					getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
							+ "] [JournalTransferMonex.NUMERO_OPERACION] ["
							+ StringUtil.contenidoDe(JournalTransferMonex.NUMERO_OPERACION.getValue()) + "]");
				}

				this.journalizar(JournalTransferMonex.CODIGO_EVENTO_TRX_LINEA.getValue(),
						JournalTransferMonex.SUBCODIGO_EVENTO_ERROR.getValue(),
						JournalTransferMonex.ID_PRODUCTO.getValue(), JournalTransferMonex.ESTADO_R.getValue(), null,
						null, JournalTransferMonex.NUMERO_OPERACION.getValue());
			} else {

				if (getLogger().isEnabledFor(Level.DEBUG)) {
					getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
							+ "] [JournalTransferMonex.CODIGO_EVENTO_MODIFICAR] ["
							+ StringUtil.contenidoDe(JournalTransferMonex.CODIGO_EVENTO_MODIFICAR.getValue()) + "]");
					getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
							+ "] [JournalTransferMonex.SUBCODIGO_EVENTO_ERROR] ["
							+ StringUtil.contenidoDe(JournalTransferMonex.SUBCODIGO_EVENTO_ERROR.getValue()) + "]");
					getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
							+ "] [JournalTransferMonex.ID_PRODUCTO] ["
							+ StringUtil.contenidoDe(JournalTransferMonex.ID_PRODUCTO.getValue()) + "]");
					getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
							+ "] [JournalTransferMonex.ESTADO_R] ["
							+ StringUtil.contenidoDe(JournalTransferMonex.ESTADO_R.getValue()) + "]");
					getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
							+ "] [JournalTransferMonex.NUMERO_OPERACION] ["
							+ StringUtil.contenidoDe(JournalTransferMonex.NUMERO_OPERACION.getValue()) + "]");
				}

				this.journalizar(JournalTransferMonex.CODIGO_EVENTO_MODIFICAR.getValue(),
						JournalTransferMonex.SUBCODIGO_EVENTO_ERROR.getValue(),
						JournalTransferMonex.ID_PRODUCTO.getValue(), JournalTransferMonex.ESTADO_R.getValue(), null,
						null, JournalTransferMonex.NUMERO_OPERACION.getValue());
			}
			mbErrorUtilityMB.setOcultaBotonVolver(true);
			mbErrorUtilityMB.setMensajeError(TablaValores.getValor(CODIGOS_ERRORES, "EXP-015", "Desc"));
			return mbErrorUtilityMB.obtenerPaginaError();
		} catch (Exception e) {

			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
						+ "] [BCI_FINEX][GeneralException][" + e.getMessage() + "]", e);
			}

			if (esPermitido.booleanValue()) {

				if (getLogger().isEnabledFor(Level.DEBUG)) {
					getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
							+ "] [JournalTransferMonex.CODIGO_EVENTO_TRX_LINEA] ["
							+ StringUtil.contenidoDe(JournalTransferMonex.CODIGO_EVENTO_TRX_LINEA.getValue()) + "]");
					getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
							+ "] [JournalTransferMonex.SUBCODIGO_EVENTO_ERROR] ["
							+ StringUtil.contenidoDe(JournalTransferMonex.SUBCODIGO_EVENTO_ERROR.getValue()) + "]");
					getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
							+ "] [JournalTransferMonex.ID_PRODUCTO] ["
							+ StringUtil.contenidoDe(JournalTransferMonex.ID_PRODUCTO.getValue()) + "]");
					getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
							+ "] [JournalTransferMonex.ESTADO_R] ["
							+ StringUtil.contenidoDe(JournalTransferMonex.ESTADO_R.getValue()) + "]");
					getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
							+ "] [JournalTransferMonex.NUMERO_OPERACION] ["
							+ StringUtil.contenidoDe(JournalTransferMonex.NUMERO_OPERACION.getValue()) + "]");
				}

				this.journalizar(JournalTransferMonex.CODIGO_EVENTO_TRX_LINEA.getValue(),
						JournalTransferMonex.SUBCODIGO_EVENTO_ERROR.getValue(),
						JournalTransferMonex.ID_PRODUCTO.getValue(), JournalTransferMonex.ESTADO_R.getValue(), null,
						null, JournalTransferMonex.NUMERO_OPERACION.getValue());
			} else {

				if (getLogger().isEnabledFor(Level.DEBUG)) {
					getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
							+ "] [JournalTransferMonex.CODIGO_EVENTO_MODIFICAR] ["
							+ StringUtil.contenidoDe(JournalTransferMonex.CODIGO_EVENTO_MODIFICAR.getValue()) + "]");
					getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
							+ "] [JournalTransferMonex.SUBCODIGO_EVENTO_ERROR] ["
							+ StringUtil.contenidoDe(JournalTransferMonex.SUBCODIGO_EVENTO_ERROR.getValue()) + "]");
					getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
							+ "] [JournalTransferMonex.ID_PRODUCTO] ["
							+ StringUtil.contenidoDe(JournalTransferMonex.ID_PRODUCTO.getValue()) + "]");
					getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
							+ "] [JournalTransferMonex.ESTADO_R] ["
							+ StringUtil.contenidoDe(JournalTransferMonex.ESTADO_R.getValue()) + "]");
					getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "][" + identificador
							+ "] [JournalTransferMonex.NUMERO_OPERACION] ["
							+ StringUtil.contenidoDe(JournalTransferMonex.NUMERO_OPERACION.getValue()) + "]");
				}

				this.journalizar(JournalTransferMonex.CODIGO_EVENTO_MODIFICAR.getValue(),
						JournalTransferMonex.SUBCODIGO_EVENTO_ERROR.getValue(),
						JournalTransferMonex.ID_PRODUCTO.getValue(), JournalTransferMonex.ESTADO_R.getValue(), null,
						null, JournalTransferMonex.NUMERO_OPERACION.getValue());
			}
			mbErrorUtilityMB.setOcultaBotonVolver(true);
			mbErrorUtilityMB.setMensajeError(TablaValores.getValor(CODIGOS_ERRORES, "BADMSG", "Desc"));
			return mbErrorUtilityMB.obtenerPaginaError();
		}

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[" + ACEPTAR_TRANSFERENCIAS_PEN + "] [" + identificador
					+ "] [BCI_FINOK][comprobanteTransferenciasRipple.jsf]");
		}

		this.transferenciasRippleViewMB.setConfirmacion(CONFIRMACION);
		this.transferenciasRippleViewMB.setConfirmacion("Aceptada");
		this.transferenciasRippleViewMB.setTransferenciaSelected(getTransferenciaSelected());
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put(TransferenciasRippleViewMB.BEAN_NAME,
				transferenciasRippleViewMB);

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + ACEPTAR_TRANSFERENCIAS_PEN + "] [" + identificador + "] [BCI_FINOK]");
		}

		return COMPROBANTE_TRANSFERENCIA_RIPPLE;
	}

	/**
	 * Generaci�n de objeto con los datos necesarios registrar la transferencia
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 07/03/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing.
	 * Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @throws GeneralException
	 * @since 1.0
	 */
	private RespuestaRegistroTransferenciaTO generarTransferencia() throws GeneralException {
		RegistrarTransferenciaTO regTransferencia = new RegistrarTransferenciaTO();
		regTransferencia.setRutEmpresa(String.valueOf(clienteMB.getRut()) + "-" + clienteMB.getDigitoVerif());
		regTransferencia.setNombreEmpresa(usuarioClienteModelMB.getClienteMB().getRazonSocial());
		// regTransferencia.setMoneda("USD");
		regTransferencia.setMoneda(this.monedaRipple.getCodigoIso());
		// regTransferencia.setCuentaDebito("11010037");
		regTransferencia.setCuentaDebito(getCuentaOrigen().getNumeroCuenta());
		// regTransferencia.setMontoPago(BigDecimal.valueOf(1.00));
		regTransferencia.setMontoPago(transferenciasRippleViewMB.getMontoTransferencia());
		// regTransferencia.setRemesa("Prueba");
		regTransferencia.setRemesa(this.getMotivoPago());
		// regTransferencia.setMonedaComision("USD");
		regTransferencia.setMonedaComision(this.getMonedaOtraCuenta().getCodigoIso());
		// regTransferencia.setCuentaComision("11052031");
		regTransferencia.setCuentaComision(this.cuentaCargoComision.getNumeroCuenta());
		// regTransferencia.setCodIdentifBenef("E0003743");
		regTransferencia.setCodIdentifBenef(this.getTransferenciaRipple().getDatosDestin().getReferencia());
		// regTransferencia.setPaisBcoBenef("US");
		regTransferencia.setPaisBcoBenef(this.transferenciasRippleViewMB.getDestinRipple().getPaisBanco());
		// regTransferencia.setCodigoCambio("20075");
		regTransferencia.setCodigoCambio(this.getTransferenciaRipple().getDatosBanco().getCodigoCambio());
		// regTransferencia.setKeyOperacion("18:07:46.226");
		regTransferencia.setKeyOperacion(this.transferenciasRippleViewMB.getComisionRipple().getKey());
		regTransferencia.setRutUsuario(usuarioClienteModelMB.getUsuarioModelMB().getRut() + "-"
				+ usuarioClienteModelMB.getUsuarioModelMB().getDigitoVerif());
		regTransferencia.setIpMaquina("");
		// regTransferencia.setCuentaDestino("120009436");
		regTransferencia.setCuentaDestino(this.getTransferenciaRipple().getDatosDestin().getNumeroCuenta());
		getLogger().info("DANARO, datos para generar la transferencia: " + regTransferencia.toString());
		RespuestaRegistroTransferenciaTO respRegTransf = transferenciasRippleUtilityMB
				.registrarTransferencia(regTransferencia);
		getLogger().info("DANARO, respRegTransf: " + respRegTransf.toString());
		return respRegTransf;
	}

	private void crearTransferenciaTO(TransferenciaTO transferenciaTO, int numCorrelativoFbp) throws GeneralException {
		getLogger().info("DANARO, en crearTransferenciaTO");
		MonedasNodoTO monedasNodo = transferenciasRippleUtilityMB.obtenerMonedas();
		MonedasPermitidasTO moneda = (MonedasPermitidasTO) monedasNodo.getLstMonedas().get(0);

		// Obtener beneficiarios y rescatar los datos del ya seleccionado
		ConsultaBeneficiariosRippleTO consulta = new ConsultaBeneficiariosRippleTO();
		consulta.setBicBanco(monedasNodo.getNodo().getBic());
		consulta.setMndaCuenta(moneda.getCodigoIso());
		consulta.setRutEmpresa(getRutEmpresa());
		getLogger().info("DANARO, en crearTransferenciaTO, voy a buscar los beneficiarios con los datos: "
				+ consulta.toString());
		List<BeneficiarioRippleTO> lstBenefsRipple = transferenciasRippleUtilityMB
				.obtenerBeneficiariosAutorizadosRipple(consulta);
		getLogger().info("DANARO, lstBenefsRipple: " + lstBenefsRipple.toString());
		BeneficiarioRippleTO benefSeleccionado = null;
		getLogger().info("DANARO, se va a buscar el beneficiario seleccionado");
		for (BeneficiarioRippleTO beneficiarioRippleTO : lstBenefsRipple) {
			if (beneficiarioRippleTO.getIdentificacion().equalsIgnoreCase("E0003743")) {
				// if(beneficiarioRippleTO.getIdentificacion().equalsIgnoreCase(transferenciaRipple.getDatosDestin().getReferencia()))
				// {
				benefSeleccionado = beneficiarioRippleTO;
				getLogger().info("DANARO, se encontro al beneficiario");
				break;
			}
		}

		if (benefSeleccionado != null) {
			getLogger()
					.info("DANARO, sse procede a obtener el detalle del beneficiario seleccionado con los datos: identificacion: "
							+ benefSeleccionado.getIdentificacion() + ", moneda: " + moneda.getCodigoIso()
							+ ", pasi banco: " + benefSeleccionado.getPaisBanco());
			DetalleBeneficiarioRippleTO detalleBenefSelec = this.consultarDetalleBeneficiario(
					benefSeleccionado.getIdentificacion(), moneda.getCodigoIso(), benefSeleccionado.getPaisBanco());
			getLogger().info("DANARO, se obtubo el detalle del beneficiario, se procede a armar el TO");
			transferenciaTO.setCorrelativoInterno(numCorrelativoFbp);
			transferenciaTO.setTipoUsuario(transferenciasRippleUtilityMB.obtenerTipoUsuario());
			transferenciaTO.setModalidadOperacion("SHW");
			transferenciaTO.getBeneficiario().setNombre2(detalleBenefSelec.getNombreCont());
			transferenciaTO.getBeneficiario().setDireccion1(detalleBenefSelec.getDireccion());
			transferenciaTO.getBeneficiario().setDireccion2(detalleBenefSelec.getDireccionCont());
			transferenciaTO.getBeneficiario().setNombrePaisBeneficiario(detalleBenefSelec.getNombrePaisRes());
			transferenciaTO.getBeneficiario().setCiudad1(detalleBenefSelec.getInfoAdicional());
			transferenciaTO.getBeneficiario().setInformacionAdicionalDos(detalleBenefSelec.getInfoAdicionalCont());
			getLogger().info("DANARO, el trasnferenciaTO generado es: " + transferenciaTO.toString());

			this.transferenciasRippleViewMB.setTransferenciaSelected(transferenciaTO);
		} else {
			getLogger().info("DANARO, no se encontro al beneficiario ");
		}
	}

	/**
	 * Metodo para cargar consultas por primera vez.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 13/03/2019 Raul E Perez. (Everis) - Minheli Mejias (Ing. Soft.
	 * BCI): Versi�n Inicial.</li>
	 * </ul>
	 * <p>
	 * 
	 * @since 1.0
	 */
	public void cargarTransferenciaRealizadasRipple() {
		boolean nu = true;
		if (this.transferenciasRippleViewMB.getTransferenciasRealizadas() == null) {
			nu = false;
			this.transferenciasRippleViewMB.setTransferenciasRealizadas(new ArrayList<TransferenciaTO>());
		}
		if (this.transferenciasRippleViewMB.getTransferenciasRealizadas().isEmpty() && nu) {
			if (getLogger().isEnabledFor(Level.DEBUG)) {
				getLogger().debug("[cargarTransferenciaRealizadasRipple] ["
						+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + "] [BCI_INI]");
			}
			try {
				Calendar calendarMax = Calendar.getInstance();
				Calendar calendarMin = Calendar.getInstance();
				calendarMax.add(Calendar.DATE, 1);
				this.setFechaHastaMaximo(calendarMax.getTime());
				calendarMin.add(Calendar.DATE, -180);
				this.setFechaDesdeMinima(calendarMin.getTime());
				this.transferenciasRippleViewMB
						.setTransferenciasRealizadas(new ArrayList<TransferenciaTO>(getTransferenciasRealizadas()));
			} catch (Exception e) {
				if (getLogger().isEnabledFor(Level.ERROR)) {
					getLogger().error("[cargarTransferenciaRealizadasRipple]["
							+ usuarioClienteModelMB.getUsuarioModelMB().getRut()
							+ "] [BCI_FINEX][GeneralException]Error con mensaje " + e.getMessage(), e);
				}
			}
			if (getLogger().isEnabledFor(Level.DEBUG)) {
				getLogger().debug("[cargarTransferenciaRealizadasRipple] ["
						+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + "] [BCI_FINOK]");
			}
		}
	}

	/**
	 * Metodo para filtras transferencias que son consultadas por fechas.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 13/03/2019 Raul E Perez. (Everis) - Minheli Mejias (Ing. Soft.
	 * BCI): Versi�n Inicial.</li>
	 * </ul>
	 * <p>
	 * 
	 * @since 1.0
	 */

	public void filtrarTransferenciasRealizadasRipple() {
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[filtrarTransferenciasRealizadasRipple] ["
					+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + "] [BCI_INI]");
		}
		try {
			Calendar calendarMax = Calendar.getInstance();
			Calendar calendarMin = Calendar.getInstance();
			calendarMax.add(Calendar.DATE, 1);
			this.setFechaHastaMaximo(calendarMax.getTime());
			calendarMin.add(Calendar.DATE, -180);
			this.setFechaDesdeMinima(calendarMin.getTime());
			this.transferenciasRippleViewMB
					.setTransferenciasRealizadas(new ArrayList<TransferenciaTO>(getTransferenciasRealizadas()));
			if (!this.transferenciasRippleViewMB.getTransferenciasRealizadas().isEmpty()) {
				List<TransferenciaTO> filtros = new ArrayList<TransferenciaTO>();
				for (TransferenciaTO transferencia : this.transferenciasRippleViewMB.getTransferenciasRealizadas()) {
					if (transferencia.getFechaOperacion().compareTo(fechaDesde) >= 0
							&& transferencia.getFechaOperacion().compareTo(fechaHasta) <= 0) {
						filtros.add(transferencia);
					}
				}
				this.transferenciasRippleViewMB.setTransferenciasRealizadas(null);
				if (filtros.size() > 0) {
					this.transferenciasRippleViewMB
							.setTransferenciasRealizadas(new ArrayList<TransferenciaTO>(filtros));
				}
			}
		} catch (Exception e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error(
						"[filtrarTransferenciasRealizadasRipple][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
								+ "] [BCI_FINEX][GeneralException]Error con mensaje " + e.getMessage(),
						e);
			}
		}
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[filtrarTransferenciasRealizadasRipple] ["
					+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + "] [BCI_FINOK]");
		}
	}


	/**
	 * Metodo encargado de hacer la eliminaci�n de una transferencia
	 * seleccionada.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 01/10/2013, Sergio Bustos (Everis) - Minheli Mejias (Ing. Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @since 1.0
	 */
	public void eliminarTransferenciaPendienteRipple() {
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[eliminarTransferenciaPendienteRipple] ["
					+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + "] [BCI_INI]");
		}
		getTransferenciaPorAutorizar();
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[eliminarTransferenciaPendienteRipple]["
					+ StringUtil.contenidoDe(getTransferenciaSelected()) + "]");
		}
		final String usuario = usuarioClienteModelMB.getUsuarioModelMB().getRut() + "" + usuarioClienteModelMB.getUsuarioModelMB().getDigitoVerif();
		try {
			if (getTransferenciaSelected() != null) {
				this.transferenciasRippleUtilityMB.consultarDetalleTransferencia(getTransferenciaSelected());
				
				FirmarEliminarTransferenciaRipple eliminarTransferenciaPen = new FirmarEliminarTransferenciaRipple();
				eliminarTransferenciaPen.setUsuario(usuario);
				eliminarTransferenciaPen.setCorrelativoFbp(getTransferenciaSelected().getCorrelativoInterno());
				eliminarTransferenciaPen.setSysTimeStamp(getTransferenciaSelected().getSysTimeStamp());
				eliminarTransferenciaPen.setAccion(TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "accionSolicitada", "eliminar"));
				
				DatosAutorizacionTO autorizacionTO = this.transferenciasRippleUtilityMB.getDatosAutorizacionTO();
				
				this.transferenciasRippleUtilityMB.firmarOEliminarTransferenciaRipple(eliminarTransferenciaPen);
				
				this.transferenciasRippleUtilityMB.borraTransferenciaAUT(autorizacionTO, this.getTransferenciaMonexTO(getTransferenciaSelected()));

				this.journalizar(JournalTransferMonex.CODIGO_EVENTO_ELIMINAR.getValue(),
						JournalTransferMonex.SUBCODIGO_EVENTO_OK.getValue(),
						JournalTransferMonex.ID_PRODUCTO.getValue(), JournalTransferMonex.ESTADO_P.getValue(),
						String.valueOf(this.transferenciasRippleViewMB.getTransferenciaSelected().getMontoTransferir()),
						getTransferenciaSelected().getBeneficiario().getNombre(),
						String.valueOf(getTransferenciaSelected().getCorrelativoInterno()));
				setTransferenciaSelected(null);
			}
		} catch (TtffException e) {
			this.transferenciasRippleViewMB.setEliminacionError(true);
			if (getLogger().isEnabledFor(Level.WARN)) {
				getLogger().warn(
						"[eliminarTransferenciaPendienteRipple][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
								+ "][" + getTransferenciaSelected().getCorrelativoInterno()
								+ "][TtffException]Error con mensaje " + e.getMessage(),
						e);
			}

				this.transferenciasRippleViewMB.setMensajeErrorEliminacion(TablaValores
						.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "mensajesError", "mensajeErrorGenerico"));

			this.journalizar(JournalTransferMonex.CODIGO_EVENTO_ELIMINAR.getValue(),
					JournalTransferMonex.SUBCODIGO_EVENTO_ERROR.getValue(), JournalTransferMonex.ID_PRODUCTO.getValue(),
					JournalTransferMonex.ESTADO_R.getValue(), null, null,
					String.valueOf(getTransferenciaSelected().getCorrelativoInterno()));
		} catch (Exception e) {
			this.transferenciasRippleViewMB.setEliminacionError(true);
			this.transferenciasRippleViewMB.setMensajeErrorEliminacion(
					TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "mensajesError", "mensajeErrorGenerico"));
			if (getLogger().isEnabledFor(Level.WARN)) {
				getLogger().warn(
						"[eliminarTransferenciaPendienteRipple][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
								+ "][" + getTransferenciaSelected().getCorrelativoInterno()
								+ "][Exception]Error con mensaje " + e.getMessage(),
						e);
			}
			this.journalizar(JournalTransferMonex.CODIGO_EVENTO_ELIMINAR.getValue(),
					JournalTransferMonex.SUBCODIGO_EVENTO_ERROR.getValue(), JournalTransferMonex.ID_PRODUCTO.getValue(),
					JournalTransferMonex.ESTADO_R.getValue(), null, null,
					String.valueOf(getTransferenciaSelected().getCorrelativoInterno()));
		}
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[eliminarTransferenciaPendienteRipple] ["
					+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + "] [BCI_FINOK]");
		}
	}

	/**
	 * Metodo auxiliar para setear valores seleccionados.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 12/03/2019 Sergio Bustos B. (Everis) - Minheli Mejias (Ing. Soft. BCI): Versi�n Inicial.</li>
	 * </ul>
	 * <p>
	 * 
	 * @since 1.0
	 */
	public void transfereciasSeleccionadasRipple() {

		if (getLogger().isInfoEnabled()) {
			getLogger().info("[transfereciasSeleccionadasRipple][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
					+ "][BCI_INI] Inicio metodo.");
		}

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger()
					.debug("[transfereciasSeleccionadasRipple][" + StringUtil.contenidoDe(transferenciaSelected) + "]");
		}
		numeroCorrelativo = transferenciaSelected.getCorrelativoInterno();

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[transfereciasSeleccionadasRipple][numeroCorrelativo][" + numeroCorrelativo + "]");
		}
		if (getLogger().isInfoEnabled()) {
			getLogger().info("[transfereciasSeleccionadasRipple][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
					+ "][BCI_FINOK] Fin metodo.");
		}
	}

	/**
	 * Metodo para filtras transferencias que son consultadas por fechas.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 18/03/2019 Raul E Perez. (Everis) - Minheli Mejias (Ing. Soft.
	 * BCI): Versi�n Inicial.</li>
	 * </ul>
	 * <p>
	 * 
	 * @since 1.0
	 */
	public void getTransferenciaPorAutorizar() {
		if (getLogger().isInfoEnabled()) {
			getLogger().info("[getTransferenciaPorAutorizar][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
					+ "][BCI_INI] Inicio metodo.");
		}

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger()
					.debug("[transfereciasSeleccionadasRipple][" + StringUtil.contenidoDe(transferenciaSelected) + "]");
		}
		this.transferenciaSelected = null;
		if (this.transferenciasRippleViewMB.getRadioSeleccionado() != null) {
			this.transferenciaSelected = new TransferenciaTO();
			for (TransferenciaTO transf : this.transferenciasRippleViewMB.getTransferenciasPendientes()) {
				if (transf.getCorrelativoInterno() == this.numeroCorrelativo) {
					setTransferenciaSelected(transf);
				}
			}
		}
		if (getLogger().isInfoEnabled()) {
			getLogger().info("[getTransferenciaPorAutorizar][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
					+ "][BCI_FINOK] Fin metodo.");
		}
	}

	/**
	 * Retorna las transferencias pendientes.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 12/03/2019 Sergio Bustos B. (Everis) - Minheli Mejias (Ing. Soft. BCI): Versi�n Inicial.</li>
	 * </ul>
	 * <p>
	 * 
	 * @return List<TransferenciaUnoAUno>
	 * @since 1.0
	 */
	public List<TransferenciaTO> getTransferenciasPendientes() {
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[getTransferenciasPendientes][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
					+ "] [BCI_INI]");
		}
		try {
			if (this.transferenciasRippleViewMB.getTransferenciasPendientes() == null
					|| this.transferenciasRippleViewMB.getTransferenciasPendientes().isEmpty()) {
				List<TransferenciaTO> lista = this.consultarTransferenciasPendientes();
				Collections.sort(lista, new TransferenciaComparator<TransferenciaTO>());
				this.transferenciasRippleViewMB.setTransferenciasPendientes(lista);
			}
		} catch (GeneralException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[getTransferenciasPendientes][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ "] [BCI_FINEX][GeneralException]Error con mensaje " + e.getMessage(), e);
			}
		}
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[getTransferenciasPendientes][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
					+ "] [BCI_FINOK]");
		}
		return this.transferenciasRippleViewMB.getTransferenciasPendientes();
	}

	/**
	 * <p>
	 * M�todo get del logger.
	 * </p>
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 12/03/2019 Sergio Bustos B. (Everis) - Minheli Mejias (Ing. Soft. BCI): Versi�n Inicial.</li>
	 * </ul>
	 * </p>
	 * 
	 * @return Logger
	 * @since 1.0
	 */
	public Logger getLogger() {
		if (log == null) {
			log = Logger.getLogger(this.getClass());
		}
		return log;
	}

	public String getValorAPresentar1() {
		return valorAPresentar1;
	}

	public void setValorAPresentar1(String valorAPresentar1) {
		this.valorAPresentar1 = valorAPresentar1;
	}

	public String getValorAPresentar() {
		return valorAPresentar;
	}

	public void setValorAPresentar(String valorAPresentar) {
		this.valorAPresentar = valorAPresentar;
	}

	public boolean getErrorFeriado() {
		return errorFeriado;
	}

	public void setErrorFeriado(boolean errorFeriado) {
		this.errorFeriado = errorFeriado;
	}

	public Date getFechaDesdeMinima() {
		return fechaDesdeMinima;
	}

	public void setFechaDesdeMinima(Date fechaDesdeMinima) {
		this.fechaDesdeMinima = fechaDesdeMinima;
	}

	public Date getFechaHastaMaximo() {
		return fechaHastaMaximo;
	}

	public void setFechaHastaMaximo(Date fechaHastaMaximo) {
		this.fechaHastaMaximo = fechaHastaMaximo;
	}

	/**
	 * Metodo encargado de generar comprobante PDF.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 20/03/2019 Raul E Perez. (Everis) - Minheli Mejias (Ing. Soft.
	 * BCI): Versi�n Inicial.</li>
	 * </ul>
	 * <p>
	 * 
	 * @return String
	 * @since 1.0
	 */
	public String generarComprobanteTransferenciaRipple() {
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[generarComprobanteTransferenciaRipple]["
					+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + "] [BCI_INI]");
		}
		ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext()
				.getContext();
		try {
			DecimalFormat decimalFormat = new DecimalFormat("###,###.00");
			String monto = decimalFormat
					.format(this.transferenciasRippleViewMB.getTransferenciaSelected().getMontoTransferir());

			ConvierteXml xml = new ConvierteXml();
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder;
			builder = factory.newDocumentBuilder();
			Document document = builder.newDocument();
			Element root = (Element) document.createElement("ComprobanteTransferencia");
			document.appendChild(root);
			root = (Element) document.getFirstChild();
			String rutaImagenLogo = servletContext.getRealPath("/")
					+ TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "xsl", "imagen");
			String rutaXsl;
			String mensajeTransferencia;
			if (!this.transferenciasRippleViewMB.getEstadoTransaccion().equals("rechazada")) {
				String mensajeTras = TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "textoInfoTransferencia",
						"mensajeTransferenciaExitosa");
				mensajeTransferencia = mensajeTras.replace("--estado--",
						"this.transferenciasRippleViewMB.getEstadoTransaccion()");
				root.appendChild(ConvierteXml.generaNodo(document, "mensaje", mensajeTransferencia));
				root.appendChild(ConvierteXml.generaNodo(document, "nombreBeneficiario",
						this.transferenciasRippleViewMB.getTransferenciaSelected().getBeneficiario().getNombre()));
				root.appendChild(
						ConvierteXml.generaNodo(document, "numeroCuenta", String.valueOf(this.transferenciasRippleViewMB
								.getTransferenciaSelected().getBeneficiario().getCuenta().getNumeroCuenta())));
				root.appendChild(ConvierteXml.generaNodo(document, "montoTransferencia",
						this.transferenciasRippleViewMB.getTransferenciaSelected().getMontoTransferir() + " " + monto));
				root.appendChild(ConvierteXml.generaNodo(document, "cuentaCargo", String
						.valueOf(this.transferenciasRippleViewMB.getTransferenciaSelected().getCuentaCargoComision())));
				if (this.transferenciasRippleViewMB.getTransferenciaSelected().getBeneficiario().getMail() != null) {
					root.appendChild(ConvierteXml.generaNodo(document, "email",
							this.transferenciasRippleViewMB.getTransferenciaSelected().getBeneficiario().getMail()));
					rutaXsl = servletContext.getRealPath("/")
							+ TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "xsl", "template");
				} else {
					root.appendChild(ConvierteXml.generaNodo(document, "titulo",
							this.transferenciasRippleViewMB.getEstadoTransaccion()));
					rutaXsl = servletContext.getRealPath("/")
							+ TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "xsl", "templateSM");
				}
			} else {
				mensajeTransferencia = TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "textoInfoTransferencia",
						"mensajeTransferenciaRechazada");
				root.appendChild(ConvierteXml.generaNodo(document, "mensaje", mensajeTransferencia));
				rutaXsl = servletContext.getRealPath("/")
						+ TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "xsl", "templateRechazo");
			}
			String patternDate = TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "pattern", "date");
			String patternHour = TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "pattern", "hour");
			SimpleDateFormat dateFormat = new SimpleDateFormat(patternDate);
			SimpleDateFormat hourFormat = new SimpleDateFormat(patternHour);
			root.appendChild(ConvierteXml.generaNodo(document, "fecha",
					dateFormat.format(this.transferenciasRippleViewMB.getFechaActual()) + " a las "
							+ hourFormat.format(this.transferenciasRippleViewMB.getFechaActual())));
			HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext()
					.getResponse();
			response.setContentType("application/pdf");
			response.setHeader("Content-Disposition", "attachment; filename=ComprobanteTransferencia.pdf; ");
			if (getLogger().isDebugEnabled()) {
				getLogger().debug("[generarComprobanteTransferenciaRipple] rutCliente:"
						+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + " rutaXsl: " + rutaXsl
						+ " rutaImagenLogo: " + rutaImagenLogo);
			}
			xml.transformaXsl(document, response, "PDF", rutaXsl, rutaImagenLogo);
			FacesContext.getCurrentInstance().renderResponse();
			FacesContext.getCurrentInstance().responseComplete();
		} catch (Exception e) {
			if (getLogger().isEnabledFor(Level.WARN)) {
				getLogger().warn(
						"[generarComprobanteTransferenciaRipple][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
								+ "] [BCI_FINEX][Exception]Error con mensaje " + e.getMessage(),
						e);
			}
		}
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[generarComprobanteTransferenciaRipple] ["
					+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + "] [BCI_FINOK]");
		}
		return null;
	}

	/**
	 * Metodo encargado de generar comprobante PDF.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 22/03/2019 Raul E Perez. (Everis) - Minheli Mejias (Ing. Soft.
	 * BCI): Versi�n Inicial.</li>
	 * </ul>
	 * <p>
	 * 
	 * @return String
	 * @since 1.0
	 */
	public void generarComprobanteTransferenciaRippleRealizada(TransferenciaTO transferenciaTO) {
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[generarComprobanteTransferenciaRippleRealizada]["
					+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + "] [BCI_INI]");
		}
		ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext()
				.getContext();
		try {
			DecimalFormat decimalFormat = new DecimalFormat("###,###.00");
			String monto = decimalFormat.format(transferenciaTO.getMontoTransferir());
			ConvierteXml xml = new ConvierteXml();
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder;
			builder = factory.newDocumentBuilder();
			Document document = builder.newDocument();
			Element root = (Element) document.createElement("ComprobanteTransferencia");
			document.appendChild(root);
			root = (Element) document.getFirstChild();
			String rutaImagenLogo = servletContext.getRealPath("/")
					+ TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "xsl", "imagen");
			String rutaXsl = servletContext.getRealPath("/")
					+ TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "xsl", "templateSM");
			String mensajeTransferencia = TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE,
					"textoInfoTransferencia", "mensajeTransferenciaEnviada");
			root.appendChild(ConvierteXml.generaNodo(document, "titulo", "Operaci�n enviada"));
			root.appendChild(ConvierteXml.generaNodo(document, "mensaje", mensajeTransferencia));
			root.appendChild(ConvierteXml.generaNodo(document, "nombreBeneficiario",
					transferenciaTO.getBeneficiario().getNombre()));
			root.appendChild(ConvierteXml.generaNodo(document, "numeroCuenta",
					String.valueOf(transferenciaTO.getBeneficiario().getCuenta().getNumeroCuenta())));
			root.appendChild(ConvierteXml.generaNodo(document, "montoTransferencia",
					transferenciaTO.getCuentaOrigen().getMoneda().getCodigo() + " " + monto));
			root.appendChild(ConvierteXml.generaNodo(document, "cuentaCargo",
					String.valueOf(transferenciaTO.getCuentaCargoComision())));

			String patternDate = TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "pattern", "date");
			String patternHour = TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "pattern", "hour");
			SimpleDateFormat dateFormat = new SimpleDateFormat(patternDate);
			SimpleDateFormat hourFormat = new SimpleDateFormat(patternHour);
			root.appendChild(
					ConvierteXml.generaNodo(document, "fecha", dateFormat.format(transferenciaTO.getFechaOperacion())
							+ " a las " + hourFormat.format(transferenciaTO.getFechaOperacion())));
			HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext()
					.getResponse();
			response.setContentType("application/pdf");
			response.setHeader("Content-Disposition", "attachment; filename=ComprobanteTransferencia.pdf; ");
			if (getLogger().isDebugEnabled()) {
				getLogger().debug("[generarComprobanteTransferenciaRippleRealizada] rutCliente:"
						+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + " rutaXsl: " + rutaXsl
						+ " rutaImagenLogo: " + rutaImagenLogo);
			}
			xml.transformaXsl(document, response, "PDF", rutaXsl, rutaImagenLogo);
			FacesContext.getCurrentInstance().renderResponse();
			FacesContext.getCurrentInstance().responseComplete();
		} catch (Exception e) {
			if (getLogger().isEnabledFor(Level.WARN)) {
				getLogger().warn("[generarComprobanteTransferenciaRippleRealizada]["
						+ usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ "] [BCI_FINEX][Exception]Error con mensaje " + e.getMessage(), e);
			}
		}
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[generarComprobanteTransferenciaRippleRealizada] ["
					+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + "] [BCI_FINOK]");
		}
	}
	
	/**
	 * Metodo para validar permison de segunda clase.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 23/03/2019 Raul E Perez. (Everis) - Minheli Mejias (Ing. Soft.
	 * BCI): Versi�n Inicial.</li>
	 * </ul>
	 * <p>
	 * 
	 * @return String
	 * @since 1.0
	 */
	public void validarPermisoSegundaClave() {
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[validarPermisoSegundaClave][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
					+ "] [BCI_INI]");
		}
		try {
			this.transferenciasRippleViewMB.setMostrarSegundaClave((autorizadorUtilityMB.esPermitido("transferencias-Monex-1a1","Autorizar")));
		} catch (Exception e) {
			if (getLogger().isEnabledFor(Level.WARN)) {
				getLogger().warn("[validarPermisoSegundaClave][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ "] [BCI_FINEX][Exception]Error con mensaje " + e.getMessage(), e);
			}
		}
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[validarPermisoSegundaClave] [" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
					+ "] [BCI_FINOK]");
		}
	}

	/**
	 * Metodo para ocultar y desrenderizar componente de segunda clave.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 23/03/2019 Raul E Perez. (Everis) - Minheli Mejias (Ing. Soft.
	 * BCI): Versi�n Inicial.</li>
	 * </ul>
	 * <p>
	 * 
	 * @return String
	 * @since 1.0
	 */
	public void ocultarComponenteSegundaClave() {
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[ocultarComponenteSegundaClave][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
					+ "] [BCI_INI]");
		}
		try {
			this.transferenciasRippleViewMB.setMostrarSegundaClave(false);
		} catch (Exception e) {
			if (getLogger().isEnabledFor(Level.WARN)) {
				getLogger().warn("[ocultarComponenteSegundaClave][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ "] [BCI_FINEX][Exception]Error con mensaje " + e.getMessage(), e);
			}
		}
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[ocultarComponenteSegundaClave] [" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
					+ "] [BCI_FINOK]");
		}
	}

}