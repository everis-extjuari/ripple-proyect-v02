package cl.bci.aplicaciones.productos.servicios.transferenciaripple.mb;

import java.io.Serializable;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Currency;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.UUID;

import javax.ejb.CreateException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.xml.namespace.QName;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import cl.bci.aplicaciones.cliente.mb.ClienteMB;
import cl.bci.aplicaciones.cliente.mb.UsuarioClienteModelMB;
import cl.bci.aplicaciones.productos.servicios.transferenciamonex.constant.ComparatorPaisTO;
import cl.bci.aplicaciones.productos.servicios.transferenciamonex.to.BancoTO;
import cl.bci.aplicaciones.productos.servicios.transferenciamonex.to.BeneficiarioIngresoTO;
import cl.bci.aplicaciones.productos.servicios.transferenciamonex.to.BeneficiarioTO;
import cl.bci.aplicaciones.productos.servicios.transferenciamonex.to.CodigoValor;
import cl.bci.aplicaciones.productos.servicios.transferenciamonex.to.CuentaMonexTO;
import cl.bci.aplicaciones.productos.servicios.transferenciamonex.to.DireccionBeneficiarioTO;
import cl.bci.aplicaciones.productos.servicios.transferenciamonex.to.EmailTO;
import cl.bci.aplicaciones.productos.servicios.transferenciamonex.to.NombreBeneficiarioTO;
import cl.bci.aplicaciones.productos.servicios.transferenciamonex.to.TransferenciaTO;
import cl.bci.arquitectura.ripple.xcurrent.client.XCurrentOnlineClient;
import cl.bci.arquitectura.ripple.xcurrent.client.impl.XCurrentOnlineClientJerseyImpl;
import cl.bci.arquitectura.ripple.xcurrent.dto.AcceptQuoteRequest;
import cl.bci.arquitectura.ripple.xcurrent.dto.Payment;
import cl.bci.arquitectura.ripple.xcurrent.dto.QuoteCollectionRequest;
import cl.bci.arquitectura.ripple.xcurrent.dto.QuoteCollectionResponse;
import cl.bci.arquitectura.ripple.xcurrent.dto.QuoteType;
import cl.bci.arquitectura.ripple.xcurrent.exception.RippleException;
import cl.bci.arquitectura.ripple.xcurrent.info.Account;
import cl.bci.arquitectura.ripple.xcurrent.info.AccountId;
import cl.bci.arquitectura.ripple.xcurrent.info.Creditor;
import cl.bci.arquitectura.ripple.xcurrent.info.Debtor;
import cl.bci.arquitectura.ripple.xcurrent.info.Iso20022UserInfo;
import cl.bci.arquitectura.ripple.xcurrent.info.OrganizationId;
import cl.bci.arquitectura.ripple.xcurrent.info.OtherId;
import cl.bci.arquitectura.ripple.xcurrent.info.PaymentId;
import cl.bci.arquitectura.ripple.xcurrent.info.PersonId;
import cl.bci.arquitectura.ripple.xcurrent.info.PostalAddress;
import cl.bci.arquitectura.ripple.xcurrent.info.RemmitanceInformation;
import cl.bci.arquitectura.ripple.xcurrent.info.SchemeName;
import cl.bci.infraestructura.web.seguridad.mb.SesionMB;
import cl.bci.infraestructura.web.seguridad.util.ConectorStruts;
import cl.bci.infraestructura.ws.excepcion.NegocioException;
import wcorp.aplicaciones.cuentacorriente.monedaextranjera.delegate.CuentaCorrienteDelegate;
import wcorp.aplicaciones.cuentacorriente.monedaextranjera.to.SalidaCuentaCorrienteTO;
import wcorp.aplicaciones.productos.servicios.divisas.to.CuentaTO;
import wcorp.aplicaciones.productos.servicios.divisas.to.MonedaTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.exception.TransferenciaDeFondosException;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.sessionfacade.TransferenciaDeFondosFacade;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.sessionfacade.TransferenciaDeFondosFacadeHome;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.sessionfacade.TtffException;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.Beneficiario1a1TO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.CambiaEstadoTttfMonexTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.CodigoCambioTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.ConsultaBeneficiariosTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.ConsultaCodigoCambioTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.ConsultaDetBeneficiarioTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.ConsultaDetPagoTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.ConsultaMonedaTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.ConsultaTransferenciaTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.CuentaComisionTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.DatosAutorizacionTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.DatosBeneficiarioTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.DatosTransferMonexTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.DetalleBeneficiarioTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.DetallePago1a1TO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.GastosBancoExteriorTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.Moneda1a1TO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.PaisTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.RespuestaHorarioTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.Transferencia1a1TO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.TransferenciaMonexTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.ValutaTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.BeneficiarioRippleTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.ComisionTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.ConsultaBeneficiariosRippleTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.ConsultaDetalleBeneficiarioRippleTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.DatosxCurrentTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.DetalleBeneficiarioRippleTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.FirmarEliminarTransferenciaRipple;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.MonedasNodoTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.MonedasPermitidasTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.NodosRippleTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.ObtenerComisionTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.RegistrarTransferenciaTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.RespuestaRegistroTransferenciaTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.RespuestaSpTO;
import wcorp.model.seguridad.SessionBCI;
import wcorp.model.seguridad.UyPDatos;
import wcorp.serv.bciexpress.AutorizaPagoCtaCte;
import wcorp.serv.bciexpress.BCIExpress;
import wcorp.serv.bciexpress.BCIExpressDelegate;
import wcorp.serv.bciexpress.BCIExpressHome;
import wcorp.serv.bciexpress.ConsultaListaCtasCtesAsociadas;
import wcorp.serv.bciexpress.ObtenCtasCtes;
import wcorp.serv.bciexpress.ResultAutorizaPagoCuentaCorriente;
import wcorp.serv.bciexpress.ResultConsultaListaCuentasCorrientesAsociadas;
import wcorp.serv.bciexpress.ResultConsultarApoderadosquehanFirmado;
import wcorp.serv.bciexpress.ResultObtieneCtasCtes;
import wcorp.serv.comex.ComexException;
import wcorp.serv.comex.ServiciosComex;
import wcorp.serv.comex.ServiciosComexHome;
import wcorp.serv.monex.MonexSaldo;
import wcorp.serv.monex.ServiciosMonex;
import wcorp.serv.monex.ServiciosMonexHome;
import wcorp.serv.seguridad.NoSessionException;
import wcorp.util.AdaptadorDeContextos;
import wcorp.util.EnhancedServiceLocator;
import wcorp.util.EnhancedServiceLocatorException;
import wcorp.util.ErroresUtil;
import wcorp.util.GeneralException;
import wcorp.util.StringUtil;
import wcorp.util.TablaValores;
import wcorp.util.journal.Eventos;
import wcorp.util.workstation.ServicioNoDisponibleException;
import ws.bci.productos.servicios.transferencias.serviciotransferenciaripple.wscliente.RippleOAuthRS;
import ws.bci.productos.servicios.transferencias.serviciotransferenciaripple.wscliente.ServicioTransferenciaRipple;
import ws.bci.productos.servicios.transferencias.serviciotransferenciaripple.wscliente.ServicioTransferenciaRippleService;

/**
 * Bean utilitario de transferencias ripple.
 * <p>
 * Registro de Versiones :
 * <ul>
 * <li>1.0 05/03/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft.
 * BCI): Versi�n inicial</li>
 * </ul>
 * </p>
 * <p>
 * <B>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</B>
 * </p>
 */
@ManagedBean
@SessionScoped
public class TransferenciasRippleUtilityMB implements Serializable {

	/**
	 * Cliente de API REST de xCurrent
	 */
	 private XCurrentOnlineClient xCurrentClient;

	/**
	 * Codigo moneda nacional.
	 */
	public static final String MONEDA_NACIONAL = "CLP";

	/**
	 * Constante c�digo error de negocio.
	 */
	protected static final String ERROR_NEGOCIO = "TTFF-001";

	/**
	 * Constante c�digo error de sistema.
	 */
	protected static final String ERROR_SISTEMA = "TTFF-003";

	/**
	 * Constante c�digo error de negocio.
	 */
	protected static final String NO_HAY_DATOS = "TTFF-002";

	/**
	 * Tabla de parametros de pyme transfer monex uno a uno.
	 */
	protected static final String PYME_TRANSFER_TABLE = "pyme/transferMonexUnoAUno.parametros";

	/**
	 * Tabla de parametros de pyme transfer monex uno a uno.
	 */
	protected static final String PYME_TRANSFER_RIPPLE = "pyme/transferRipple.parametros";

	/**
	 * C�digo de pa�s Chile.
	 */
	protected static final String COD_PAIS_CHILE = TablaValores.getValor(PYME_TRANSFER_TABLE, "codigoPais", "CL");

	/**
	 * C�digo de pa�s Chile.
	 */
	protected static final String BENEFICIARIO_PENDIENTE = TablaValores.getValor(PYME_TRANSFER_TABLE,
			"codigoTipoBenenficiario", "PENDIENTE");

	/**
	 * C�digo de pa�s Chile.
	 */
	protected static final String BENEFICIARIO_INSCRITO = TablaValores.getValor(PYME_TRANSFER_TABLE,
			"codigoTipoBenenficiario", "INSCRITO");

	/**
	 * Par�metro que representa el c�digo de autorizaci�n.
	 */
	private static final String CODIGO_AUTORIZACION = "2";

	/**
	 * Codigo para mostrar mensajes de GeneralException.
	 */
	private static final String ESPECIAL = "ESPECIAL";

	/**
	 * Par�metro que representa la fecha inicial.
	 */
	private static final String FECHA_INICIAL = "01/01/1900";

	/**
	 * Identificador para el usuario Apoderado del cliente empresa y
	 * empresarios.
	 */
	private static final String USUARIO_APODERADO = "Apoderado";

	/**
	 * Identificador para el usuario Operador del cliente empresa y empresarios.
	 */
	private static final String USUARIO_OPERADOR = "Operador";

	/**
	 * jndi de TransferenciaDeFondosBusinessDelegate.
	 */
	private static final String JNDI_TRANSFERENCIA = "wcorp.aplicaciones.productos.servicios.transferencias"
			+ ".monedaextranjera.sessionfacade.TransferenciaDeFondosFacade";

	/** Nombre JNDI del EJB BCIExpress. */
	private static final String JNDI_NAME_BCI_EXPRESS = "wcorp.serv.bciexpress.BCIExpress";

	/** Nombre JNDI del EJB de Comex. */
	private static final String JNDI_NAME = "wcorp.serv.comex.ServiciosComex";

	/**
	 * Variable que contiene el largo del convenio.
	 */
	private static final int LARGO_CONVENIO = 10;

	/**
	 * Variable que contiene el largo a rellenar con ceros.
	 */
	private static final int RELLENA_CON_CEROS = 11;

	/**
	 * Variable que contiene el largo de rut.
	 */
	private static final int LARGO_RUT = 8;

	/**
	 * Constante c�digo pendiente de firma.
	 */
	private static final String CODIGO_TFMX_0068 = "TFMX-0068";

	/**
	 * Constante c�digo usuario no autorizado.
	 */
	private static final String CODIGO_TFMX_0069 = "TRMX-0069";

	/**
	 * Constante c�digo firma repetida.
	 */
	private static final String CODIGO_TFMX_0070 = "TFMX-0070";

	/**
	 * Constante c�digo error de sistema.
	 */
	private static final String CODIGO_TFMX_0071 = "TRMX-0071";

	/**
	 * Constante c�digo de error en conversi�n de monedas.
	 */
	private static final String CODIGO_TFMX_0089 = "TFMX-0089";

	/**
	 * Identificador error usuario no autorizado.
	 */
	private static final String CODIGO_USUARIO_NO_AUTORIZADO = "USUARIO_NO_AUTORIZADO";

	/**
	 * Identificador firma repetida.
	 */
	private static final String CODIGO_FIRMA_REPETIDA = "FIRMA_REPETIDA";

	/**
	 * Identificador error de sistema.
	 */
	private static final String CODIGO_ERROR_SISTEMA = "ERROR_SISTEMA";

	/**
	 * Llave estado solicitado SIG del cambio de transferencia.
	 */
	private static final String SOLICITADO_SIG = "SIG";

	/**
	 * Llave estado solicitado EXE del cambio de transferencia.
	 */
	private static final String SOLICITADO_EXE = "EXE";

	/**
	 * Condicion de cierre "Aperturada" en cuenta monex.
	 */
	private static final String APERTURADA = "A";

	/**
	 * Llave de tabla de par�metros.
	 */
	private static final String SERVICIO_SOLICITADO_KEY = "servicioSolicitado";

	/**
	 * Id de serializaci�n.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Tabla de par�metros de transfer monex.
	 */
	private static final String TABLA_PARAMETROS_TRANSFERRIPPLE = "pyme/transferRipple.parametros";

	/**
	 * log de la clase TransferenciasMonexUtilityMB.
	 */
	private transient Logger log = Logger.getLogger(TransferenciasRippleUtilityMB.class);

	/**
	 * remote instancia TransferenciaDeFondosFacade.
	 */
	private TransferenciaDeFondosFacade servicioTransferencia;

	/**
	 * Variable contiene lista de monedas por nodo miami;
	 */
	private MonedasNodoTO monedasNodo;

	/**
	 * Referencia al EJB de COMEX.
	 */
	private ServiciosComex serviciosComex;

	/**
	 * Identificador de la transferencia pendiente de firma.
	 */
	private Integer correlativoInternoTransferencia;

	/**
	 * Estado de la transferencia monex.
	 */
	private Boolean transferenciaPendienteFirma;

	/**
	 * Inyecci�n de sesionMB.
	 */
	@ManagedProperty(value = "#{sesionMB}")
	private SesionMB sesion;

	/**
	 * Bean de negocio usuarioClienteModelMB.
	 */
	@ManagedProperty(value = "#{usuarioClienteModelMB}")
	private UsuarioClienteModelMB usuarioClienteModelMB;

	/**
	 * Constante firma eliminacion de transferencia
	 */
	private static String FIRMAR_ELIMINAR_TRANSFERENCIA = "firmarOEliminarTransferenciaRipple";

	/**
	 * Constante debitar cuenta corriente
	 */
	private static String DEBITAR_CUENTA_CORRIENTE = "debitarCuentaCorrienteMX";

	/**
	 * Constante debitar cuenta corriente
	 */
	private static String REGISTRAR_RESULTADO_XCURRENT = "registrarResulatadoxCurrent";

	/**
	 * Constante debitar cuenta corriente
	 */
	private static String CONSULTAR_DATOS_XCURRENT = "consultarDatosxCurrent";

	/**
	 * Constante debitar cuenta corriente
	 */
	private static String OBTENER_XCURRENT_TOKEN = "obtenerXCurrentToken";

	/**
	 * Constante para obtener el objeto desde request.
	 */
	protected static final String FIRMAR_TRANSFERENCIA = "firmarTransferencia";

	/**
	 * Bean de negocio clienteMB.
	 */
	@ManagedProperty(value = "#{clienteMB}")
	private ClienteMB clienteMB;

	public ClienteMB getClienteMB() {
		return clienteMB;
	}

	public void setClienteMB(ClienteMB clienteMB) {
		this.clienteMB = clienteMB;
	}

	/**
	 * Constructor de clase.
	 */
	public TransferenciasRippleUtilityMB() {
		try {
			createEjb();
		} catch (TransferenciaDeFondosException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[TransferenciasMonexUtilityMB] [BCI_FINEX][TransferenciaDeFondosException] "
						+ "Error al instanciar EJB TransferenciaMonexFacadeBean : " + e.getMessage(), e);
			}
		}

		try {
			crearEJBServiciosComex();
		} catch (ServicioNoDisponibleException e1) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[TransferenciasMonexUtilityMB] [BCI_FINEX][ServicioNoDisponibleException] "
						+ "Error al instanciar EJB ServiciosComexHomeBean : " + e1.getMessage(), e1);
			}
		}

		 xCurrentClient = new XCurrentOnlineClientJerseyImpl(TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "xCurrentClient", "url"),
		 Integer.parseInt(TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "xCurrentClient", "timeOut")));
	}

	/**
	 * LLamada al servicio que cambia estado transferencia.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 25/02/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing.
	 * Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param cambiaEstadoTttfMonexTO
	 *            Entidad de cambio de estado.
	 * @return String
	 * @throws TtffException
	 *             Excepci�n de negocio.
	 * @throws RemoteException
	 *             Excepci�n de EJB.
	 * @since 1.0
	 */
	public String cambiaEstadoTransferencia(CambiaEstadoTttfMonexTO cambiaEstadoTttfMonexTO)
			throws TtffException, RemoteException {
		String timestamp = null;
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug(
					"[cambiaEstadoTransferencia][BCI_INI][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}
		timestamp = this.servicioTransferencia.cambiarEstadoTtffMonex(cambiaEstadoTttfMonexTO);
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[cambiaEstadoTransferencia][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
					+ "] timestamp cambio  estado:" + timestamp);
		}
		return timestamp;
	}

	/**
	 * Validaci�n consulta horario transferencias.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 25/02/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing.
	 * Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @throws TtffException
	 *             Excepci�n especifica TTFF.
	 * @throws GeneralException
	 *             Excepci�n General.
	 * @since 1.0
	 */
	public void consultaHorarioIngreso() throws TtffException, GeneralException {
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug(
					"[consultaHorarioIngreso][BCI_INI][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}
		try {
			this.servicioTransferencia.consultarHorarioIngreso();
		} catch (RemoteException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[consultaHorarioIngreso]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_FINEX][RemoteException] mensaje de error " + e.getMessage(), e);
			}
			throw new GeneralException(e.getMessage());
		}
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug(
					"[consultaHorarioIngreso][BCI_FIN][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}
	}

	/**
	 * Acceso a servicio de consulta los detalle de pago.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 25/03/2019, Sergio Bustos B. (Everis) - Minheli Mejias (Ing.
	 * Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param parIn
	 *            Consulta de pago to.
	 * @return DetallePago1a1TO
	 * @throws GeneralException
	 *             Excepcion general.
	 * @since 1.0
	 */
	public DetallePago1a1TO consultarDetallePago(ConsultaDetPagoTO parIn) throws GeneralException {
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			if (usuarioClienteModelMB != null && usuarioClienteModelMB.getUsuarioModelMB() != null) {
				getLogger().debug(
						"[consultarDetallePago][BCI_INI][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
			} else {
				getLogger().debug("[consultarDetallePago][BCI_INI][0]");
			}
		}
		DetallePago1a1TO detallePago1a1TO = null;
		try {
			detallePago1a1TO = this.servicioTransferencia.consultarDetallePagoRipple(parIn);
		} catch (TtffException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				if (usuarioClienteModelMB != null && usuarioClienteModelMB.getUsuarioModelMB() != null) {
					getLogger().error("[consultarDetallePago]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
							+ " [BCI_FINEX][TtffException] mensaje de error " + e.getCodigo(), e);
				} else {
					getLogger().error(
							"[consultarDetallePago] [BCI_FINEX][TtffException] mensaje de error " + e.getCodigo(), e);
				}
			}
			throw new GeneralException(e.getCodigo(), e.getFullMessage());
		} catch (RemoteException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				if (usuarioClienteModelMB != null && usuarioClienteModelMB.getUsuarioModelMB() != null) {
					getLogger().error("[consultarDetallePago]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
							+ " [BCI_FINEX][RemoteException] mensaje de error " + e.getMessage(), e);
				}
			}

			else {
				getLogger().error(
						"[consultarDetallePago] [BCI_FINEX][TtffException] mensaje de error " + e.getMessage(), e);

			}
			throw new GeneralException(e.getMessage(), e.getLocalizedMessage());
		}
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			if (usuarioClienteModelMB != null && usuarioClienteModelMB.getUsuarioModelMB() != null) {
				getLogger().debug("[consultarDetallePago][BCI_FINOK]["
						+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
			} else {
				getLogger().debug("[consultarDetallePago][BCI_FINOK][0]");
			}
		}
		return detallePago1a1TO;
	}

	/**
	 * Consulta detalle de transferencia.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 25/03/2019, Sergio Bustos B. (Everis) - Minheli Mejias (Ing.
	 * Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param transferencia
	 *            Entidad de negocio.
	 * @throws GeneralException
	 *             Excepci�n general de negocio.
	 * @since 1.0
	 */
	public void consultarDetalleTransferencia(TransferenciaTO transferencia) throws GeneralException {

		final String nombreMetodo = "consultarDetalleTransferencia";
		final String identificador = (usuarioClienteModelMB != null
				&& usuarioClienteModelMB.getUsuarioModelMB() != null)
						? String.valueOf(usuarioClienteModelMB.getUsuarioModelMB().getRut()) : "";

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "][" + identificador + "] [BCI_INI]");
			getLogger().info("[" + nombreMetodo + "][" + identificador + "] [Parametro][transferencia]["
					+ StringUtil.contenidoDe(transferencia) + "]");
		}

		if (transferencia != null) {

			ConsultaDetPagoTO consultaDetPagoTO = new ConsultaDetPagoTO();
			consultaDetPagoTO.setCorrelativoInterno(transferencia.getCorrelativoInterno());
			consultaDetPagoTO.setModalidadOperacion(transferencia.getModalidadOperacion());
			consultaDetPagoTO.setTipoUsuario(transferencia.getTipoUsuario());

			Transferencia1a1TO[] beneficiario = null;

			try {
				beneficiario = this.servicioTransferencia.obtenerTransferenciasPenRipple(transferencia.getConsulta());

				if (getLogger().isEnabledFor(Level.DEBUG)) {
					getLogger().debug("[" + nombreMetodo + "][" + identificador + "] [beneficiario] ["
							+ StringUtil.contenidoDe(beneficiario) + "]");
				}
			} catch (RemoteException e) {
				if (getLogger().isEnabledFor(Level.WARN)) {
					getLogger().warn(
							"[" + nombreMetodo + "][" + identificador + "] [RemoteException][" + e.getMessage() + "]",
							e);
				}
			}

			if (getLogger().isEnabledFor(Level.DEBUG)) {
				getLogger().debug("[" + nombreMetodo + "][" + identificador + "] [consultaDetPagoTO] ["
						+ StringUtil.contenidoDe(consultaDetPagoTO) + "]");
			}

			try {
				DetallePago1a1TO detallePago1a1TO = this.consultarDetallePago(consultaDetPagoTO);

				if (getLogger().isEnabledFor(Level.DEBUG)) {
					getLogger().debug("[" + nombreMetodo + "][" + identificador + "] [detallePago1a1TO] ["
							+ StringUtil.contenidoDe(detallePago1a1TO) + "]");
				}

				if (beneficiario != null) {
					for (Transferencia1a1TO transferencia1a1to : beneficiario) {

						if (transferencia1a1to.getCorrelativoInterno() == consultaDetPagoTO.getCorrelativoInterno()) {
							detallePago1a1TO.setNombreBeneficiarioL1(transferencia1a1to.getNombreBeneficiario());
							detallePago1a1TO.setNombrePaisBeneficiario(transferencia1a1to.getNombrePais());

							break;
						}
					}
				}

				crearTransferenciaTO(detallePago1a1TO, transferencia);

				if (getLogger().isEnabledFor(Level.DEBUG)) {
					getLogger().debug("[" + nombreMetodo + "][" + identificador + "] [detallePago1a1TO] ["
							+ StringUtil.contenidoDe(detallePago1a1TO) + "]");
					getLogger().debug("[" + nombreMetodo + "][" + identificador + "] [transferencia] ["
							+ StringUtil.contenidoDe(transferencia) + "]");
				}
			} catch (GeneralException e) {
				if (getLogger().isEnabledFor(Level.ERROR)) {
					getLogger().error("[" + nombreMetodo + "][" + identificador + "] [BCI_FINEX][GeneralException]["
							+ e.getMessage() + "]", e);
				}
				throw new GeneralException(ESPECIAL, e.getFullMessage());
			}
		}
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "][" + identificador + "] [BCI_FINOK]");
		}
	}

	/**
	 * Crea entidad Banco para presentaci�n.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 25/02/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing.
	 * Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param codigoSwift
	 *            Codigo Swift.
	 * @param nombreDireccion
	 *            Nomber de direcci�n.
	 * @param numeroCuenta
	 *            N�mero de cuenta.
	 * @return BancoTO
	 * @since 1.0
	 */
	public BancoTO crearBanco(String codigoSwift, String nombreDireccion, String numeroCuenta) {
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			if (usuarioClienteModelMB != null && usuarioClienteModelMB.getUsuarioModelMB() != null) {
				getLogger().debug("[crearBanco][BCI_INI][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
			} else {
				getLogger().debug("[crearBanco][BCI_INI][0]");
			}
		}
		BancoTO banco = new BancoTO();
		banco.setCodigoSwift(codigoSwift);
		banco.setNombreDireccion(nombreDireccion);
		banco.setNumeroCuenta(numeroCuenta);
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			if (usuarioClienteModelMB != null && usuarioClienteModelMB.getUsuarioModelMB() != null) {
				getLogger().debug("[crearBanco][BCI_INI][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
			} else {
				getLogger().debug("[crearBanco][BCI_INI][0]");
			}
		}
		return banco;
	}

	/**
	 * Crear beneficiario.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 25/02/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing.
	 * Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param direccion
	 *            Direcci�n de beneficiario.
	 * @param mail
	 *            Email de beneficiario.
	 * @param nombreDestinatario
	 *            Nombre de destinatario.
	 * @param paisCuenta
	 *            Pa�s de la cuenta.
	 * @return BeneficiarioTO
	 * @since 1.0
	 */
	public BeneficiarioTO crearBeneficiario(String direccion, String mail, String nombreDestinatario,
			CodigoValor paisCuenta) {
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger()
					.debug("[crearBeneficiario][BCI_INI][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}
		BeneficiarioTO beneficiario = new BeneficiarioTO();
		beneficiario.setDireccion(direccion);
		beneficiario.setMail(mail);
		beneficiario.setNombre(nombreDestinatario);
		beneficiario.setPaisCuenta(paisCuenta);
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug(
					"[crearBeneficiario][BCI_FINOK][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}
		return beneficiario;
	}

	/**
	 * Obtiene las cuentas de origen por codigo moneda.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 25/02/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing.
	 * Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param aRut
	 *            RUT de empresa.
	 * @param aDv
	 *            DV de empresa.
	 * @param moneda
	 *            Moneda.
	 * @return List<CuentaCorrienteTO>
	 * @since 1.0
	 */
	public List<CuentaMonexTO> getCuentasPorCodigoMoneda(long aRut, char aDv, String moneda) {
		List<CuentaMonexTO> cuentasCorrientes = new ArrayList<CuentaMonexTO>();

		final String nombreMetodo = "getCuentasPorCodigoMoneda";
		final String identificador = (usuarioClienteModelMB != null
				&& usuarioClienteModelMB.getUsuarioModelMB() != null)
						? String.valueOf(usuarioClienteModelMB.getUsuarioModelMB().getRut()) : "";

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "][" + identificador + "] [BCI_INI]");
			getLogger().info("[" + nombreMetodo + "][" + identificador + "] [Parametro][aRut]["
					+ StringUtil.contenidoDe(aRut) + "]");
			getLogger().info("[" + nombreMetodo + "][" + identificador + "] [Parametro][aDv]["
					+ StringUtil.contenidoDe(aDv) + "]");
			getLogger().info("[" + nombreMetodo + "][" + identificador + "] [Parametro][moneda]["
					+ StringUtil.contenidoDe(moneda) + "]");
		}

		try {
			CuentaTO[] cuentas = obtenerCuentasMonedaExtranjera(aRut, aDv, moneda);
			String simboloMoneda = this.obtenerSimboloMoneda(moneda);

			if (getLogger().isEnabledFor(Level.DEBUG)) {
				getLogger().debug("[" + nombreMetodo + "][" + identificador + "] [cuentas] ["
						+ StringUtil.contenidoDe(cuentas) + "]");
				getLogger()
						.debug("[" + nombreMetodo + "][" + identificador + "] [simboloMoneda] [" + simboloMoneda + "]");
			}

			CuentaMonexTO corrienteTO = null;
			for (CuentaTO cuentaTO : cuentas) {
				corrienteTO = new CuentaMonexTO();
				corrienteTO.setMoneda(
						new CodigoValor(cuentaTO.getMoneda() == null ? "" : cuentaTO.getMoneda().getCodigoISO(),
								cuentaTO.getMoneda() == null ? "" : cuentaTO.getMoneda().getDescripcion()));
				corrienteTO.setNumeroCuenta(cuentaTO.getNumeroCuenta());
				corrienteTO.setSaldoDisponible(cuentaTO.getSaldo().doubleValue());
				corrienteTO.setSimboloMoneda(simboloMoneda);
				corrienteTO.setCondicionCierre(cuentaTO.getCondicionCierre());
				cuentasCorrientes.add(corrienteTO);
			}
		} catch (GeneralException e) {
			if (getLogger().isEnabledFor(Level.WARN)) {
				getLogger().warn(
						"[" + nombreMetodo + "][" + identificador + "] [GeneralException][" + e.getMessage() + "]", e);
			}
		}

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "][" + identificador + "] [BCI_FINOK]["
					+ StringUtil.contenidoDe(cuentasCorrientes) + "]");
		}

		return cuentasCorrientes;
	}

	/**
	 * Obtiener simbolo de moneda.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 25/02/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing.
	 * Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param moneda
	 *            Codigo de moneda.
	 * @return String
	 * @since 1.0
	 */
	public String obtenerSimboloMoneda(String moneda) {
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug(
					"[obtenerSimboloMoneda][BCI_INI][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}
		String retorno = "";
		try {
			retorno = this.getResourceValue(moneda);
		} catch (Exception e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[obtenerSimboloMoneda]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_FINEX][Exception] mensaje de error " + e.getMessage(), e);
			}
		}
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug(
					"[obtenerSimboloMoneda][BCI_FINOK][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}
		return retorno;
	}

	/**
	 * Obtiene los recursos desde archivo.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 25/02/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing.
	 * Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param key
	 *            Llave de recurso de archivo transferenciamonex.properties.
	 * @return String
	 * @since 1.0
	 */
	public String getResourceValue(String key) {

		final String nombreMetodo = "getResourceValue";
		final String identificador = (usuarioClienteModelMB != null
				&& usuarioClienteModelMB.getUsuarioModelMB() != null)
						? String.valueOf(usuarioClienteModelMB.getUsuarioModelMB().getRut()) : "";

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "][" + identificador + "] [BCI_INI]");
			getLogger().info("[" + nombreMetodo + "][" + identificador + "] [Parametro][key][" + key + "]");
		}

		String resource = "cl.bci.aplicaciones.productos.servicios.transferenciamonex.transferenciamonex";
		ResourceBundle resourceBundle = null;

		try {
			if (getLogger().isEnabledFor(Level.DEBUG)) {
				getLogger().debug("[" + nombreMetodo + "][" + identificador + "] [resource_sesion.canalId] ["
						+ StringUtil.contenidoDe(resource + "_" + getSesion().getCanalId()) + "]");
			}

			resourceBundle = ResourceBundle.getBundle(resource + "_" + getSesion().getCanalId());
		} catch (Exception e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error(
						"[" + nombreMetodo + "][" + identificador + "] [BCI_FINEX][Exception][" + e.getMessage() + "]",
						e);
			}

			resourceBundle = ResourceBundle.getBundle(resource);
		}

		if (resourceBundle == null) {
			if (getLogger().isEnabledFor(Level.INFO)) {
				getLogger().info("[" + nombreMetodo + "] [" + identificador + "] [BCI_FINOK]['']");
			}
			return "";
		}

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "] [" + identificador + "] [BCI_FINOK]["
					+ resourceBundle.getString(key) + "]");
		}

		return resourceBundle.getString(key);
	}

	public SesionMB getSesion() {
		return sesion;
	}

	/**
	 * Retorna la consulta de beneficiarios.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 25/02/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing.
	 * Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param consulta
	 *            Entidad para consulta de beneficiario.
	 * @return List<Beneficiario1a1TO>
	 * @throws GeneralException
	 *             Excepci�n de negocio.
	 * @since 1.0
	 */
	public List<Beneficiario1a1TO> obtenerBeneficiariosAutorizados(ConsultaBeneficiariosTO consulta)
			throws GeneralException {

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[obtenerBeneficiariosAutorizados][BCI_INI]["
					+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}

		List<Beneficiario1a1TO> beneficiarios = null;
		Beneficiario1a1TO[] beneficiariosTOs = null;
		try {
			beneficiariosTOs = this.servicioTransferencia.obtenerBeneficiariosAutorizados(consulta);
			beneficiarios = Arrays.asList(beneficiariosTOs);
		} catch (TtffException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger()
						.error("[obtenerBeneficiariosAutorizados]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
								+ " [BCI_FINEX][TtffException] mensaje de error " + e.getMessage(), e);
			}
			throw new GeneralException(e.getCodigo(), e.getInfoAdic());
		} catch (RemoteException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger()
						.error("[obtenerBeneficiariosAutorizados]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
								+ " [BCI_FINEX][RemoteException] mensaje de error " + e.getMessage(), e);
			}
			throw new GeneralException(e.getMessage());
		}

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[obtenerBeneficiariosAutorizados][BCI_FINOK]["
					+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}

		return beneficiarios;
	}

	/**
	 * Retorna lista de c�digos de cambio.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 25/02/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing.
	 * Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param consultaCodigoCambioTO
	 *            Entidad consulta codigo cambio.
	 * @return List<CodigoCambioTO>
	 * @throws GeneralException
	 *             Excepci�n de negocio.
	 * @since 1.0
	 */
	public List<CodigoCambioTO> obtenerCodigosDeCambio(ConsultaCodigoCambioTO consultaCodigoCambioTO)
			throws GeneralException {

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug(
					"[obtenerCodigosDeCambio][BCI_INI][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}

		List<CodigoCambioTO> codigos = null;
		CodigoCambioTO[] cambioTOs = null;
		try {
			cambioTOs = this.servicioTransferencia.obtenerCodigosDeCambio(consultaCodigoCambioTO);
			codigos = Arrays.asList(cambioTOs);
		} catch (TtffException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[obtenerCodigosDeCambio]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_FINEX][TtffException] mensaje de error " + e.getMessage(), e);
			}
			throw new GeneralException(e.getCodigo(), e.getInfoAdic());
		} catch (RemoteException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[obtenerCodigosDeCambio]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_FINEX][RemoteException] mensaje de error " + e.getMessage(), e);
			}
			throw new GeneralException(e.getMessage());
		}
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug(
					"[obtenerCodigosDeCambio][BCI_FINOK][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}
		return codigos;
	}

	/**
	 * Obtiene la cuenta de cargo de un cliente.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 25/02/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing.
	 * Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param rutCliente
	 *            RUT Cliente.
	 * @return CuentaComisionTO
	 * @throws GeneralException
	 *             Excepci�n de neogcio.
	 * @since 1.0
	 */
	public CuentaComisionTO obtenerCuentaDeCargo(String rutCliente) throws GeneralException {

		final String nombreMetodo = "obtenerCuentaDeCargo";
		final String identificador = (usuarioClienteModelMB != null
				&& usuarioClienteModelMB.getUsuarioModelMB() != null)
						? String.valueOf(usuarioClienteModelMB.getUsuarioModelMB().getRut()) : "";

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "][" + identificador + "] [BCI_INI]");
			getLogger().info("[" + nombreMetodo + "][" + identificador + "] [Parametro][rutCliente]["
					+ StringUtil.contenidoDe(rutCliente) + "]");
		}

		CuentaComisionTO comisionTO = new CuentaComisionTO();
		try {
			comisionTO = servicioTransferencia.obtenerCuentaDeCargo(rutCliente);
		} catch (RemoteException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[" + nombreMetodo + "][" + identificador + "] [BCI_FINEX][RemoteException]["
						+ e.getMessage() + "]", e);
			}
			throw new GeneralException(ERROR_SISTEMA, e.getMessage());
		} catch (TtffException e) {
			if (getLogger().isEnabledFor(Level.WARN)) {
				getLogger().warn(
						"[" + nombreMetodo + "][" + identificador + "] [tipoExcepcion][" + e.getMessage() + "]", e);
			}
			if (e.noHayDatos(e.getCodigo())) {
				if (getLogger().isEnabledFor(Level.INFO)) {
					getLogger().info("[" + nombreMetodo + "][" + identificador + "] [BCI_FINOK] Retornando nulo.");
				}
				return null;
			}
		}

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "][" + identificador + "] [BCI_FINOK]["
					+ StringUtil.contenidoDe(comisionTO) + "]");
		}

		return comisionTO;
	}

	/**
	 * Retorna el detalle de un beneficiario a traves de nueva consulta.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 25/02/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing.
	 * Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param consultaDetBeneficiario
	 *            Entidad de consulta detalle de beneficiario.
	 * @return DetalleBeneficiarioTO
	 * @throws GeneralException
	 *             Excepci�n de negocio.
	 * @since 1.0
	 */
	public DetalleBeneficiarioTO obtenerDetalleDelBeneficiario(DatosBeneficiarioTO consultaDetBeneficiario)
			throws GeneralException {

		final String nombreMetodo = "obtenerDetalleDelBeneficiario";
		final String identificador = (usuarioClienteModelMB != null
				&& usuarioClienteModelMB.getUsuarioModelMB() != null)
						? String.valueOf(usuarioClienteModelMB.getUsuarioModelMB().getRut()) : "";

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "][" + identificador + "] [BCI_INI]");
			getLogger().info("[" + nombreMetodo + "][" + identificador + "] [Parametro][consultaDetBeneficiario]["
					+ StringUtil.contenidoDe(consultaDetBeneficiario) + "]");
		}

		DetalleBeneficiarioTO beneficiarioTO = new DetalleBeneficiarioTO();
		wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.BeneficiarioTO benefTO = null;

		try {
			benefTO = this.servicioTransferencia.consultarDatosBeneficiario(consultaDetBeneficiario);

			if (getLogger().isEnabledFor(Level.DEBUG)) {
				getLogger().debug("[" + nombreMetodo + "][" + identificador + "] [benefTO] ["
						+ StringUtil.contenidoDe(benefTO) + "]");
			}

			beneficiarioTO.setNombreBeneficiarioL1(benefTO.getDatosBeneficiarioLinea1());
			beneficiarioTO.setNombre2(benefTO.getNombre2());
			beneficiarioTO.setDireccion1(benefTO.getDireccion1());
			beneficiarioTO.setDireccion2(benefTO.getDireccion2());
			beneficiarioTO.setIsoPaisBeneficiario(benefTO.getIsoPaisBeneficiario());
			beneficiarioTO.setNombrePaisBeneficiario(benefTO.getNombrePaisBeneficiario());
			beneficiarioTO.setCiudad1(benefTO.getCiudad1());
			beneficiarioTO.setInformacionAdicionalDos(benefTO.getInformacionAdicionalDos());

			beneficiarioTO.setCuentaCorriente(benefTO.getNumeroCuentaBeneficiario());
			beneficiarioTO.setBancoBeneficiario(benefTO.getSwiftBcoBeneficiario());
			beneficiarioTO.setRutaPago(benefTO.getRutaPagoBcoBeneficiario());
			beneficiarioTO.setNomDirBancoL1(benefTO.getDatosBcoBeneficiarioLinea1());
			beneficiarioTO.setNomDirBancoL2(benefTO.getDatosBcoBeneficiarioLinea2());
			beneficiarioTO.setNomDirBancoL3(benefTO.getDatosBcoBeneficiarioLinea3());
			beneficiarioTO.setNomDirBancoL4(benefTO.getDatosBcoBeneficiarioLinea4());
			beneficiarioTO.setBicBanco(benefTO.getSwiftBcoIntermediario());
			beneficiarioTO.setRutaPagoIntermediario(benefTO.getRutaPagoBcoIntermediario());
			beneficiarioTO.setNomDirIntermediarioL1(benefTO.getDatosBcoIntermediarioLinea1());
			beneficiarioTO.setNomDirIntermediarioL2(benefTO.getDatosBcoIntermediarioLinea2());
			beneficiarioTO.setNomDirIntermediarioL3(benefTO.getDatosBcoIntermediarioLinea3());
			beneficiarioTO.setNomDirIntermediarioL4(benefTO.getDatosBcoIntermediarioLinea4());
			beneficiarioTO.setEmail1(benefTO.getEmailBeneficiario1());
			beneficiarioTO.setEmail2(benefTO.getEmailBeneficiario2());
			beneficiarioTO.setEmail3(benefTO.getEmailBeneficiario3());
			beneficiarioTO.setForFurther(benefTO.getForFurtherCredit());
		} catch (RemoteException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[" + nombreMetodo + "][" + identificador + "] [BCI_FINEX][RemoteException]["
						+ e.getMessage() + "]", e);
			}
			throw new GeneralException(e.getMessage());
		}

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "][" + identificador + "] [BCI_FINOK]["
					+ StringUtil.contenidoDe(beneficiarioTO) + "]");
		}
		return beneficiarioTO;
	}

	/**
	 * Retorna el deralle de un beneficiario.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 25-09-2013, Angel Cris�stomo (Imagemaker IT): versi�n inicial
	 * </li>
	 * </ul>
	 * </p>
	 * 
	 * @param consultaDetBeneficiario
	 *            Entidad de consulta detalle de beneficiario.
	 * @return DetalleBeneficiarioTO
	 * @throws GeneralException
	 *             Excepci�n de negocio.
	 * @since 1.0
	 */
	public DetalleBeneficiarioTO obtenerDetalleDelBeneficiario(ConsultaDetBeneficiarioTO consultaDetBeneficiario)
			throws GeneralException {

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[obtenerDetalleDelBeneficiario][BCI_INI]["
					+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}

		DetalleBeneficiarioTO beneficiarioTO = null;
		try {
			beneficiarioTO = this.servicioTransferencia.obtenerDetalleDelBeneficiario(consultaDetBeneficiario);
		} catch (TtffException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[obtenerDetalleDelBeneficiario]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_FINEX][TtffException] mensaje de error " + e.getMessage(), e);
			}
			throw new GeneralException(e.getCodigo(), e.getInfoAdic());
		} catch (RemoteException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[obtenerDetalleDelBeneficiario]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_FINEX][RemoteException] mensaje de error " + e.getMessage(), e);
			}
			throw new GeneralException(e.getMessage());
		}

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[obtenerDetalleDelBeneficiario][BCI_FINOK]["
					+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}
		return beneficiarioTO;
	}

	/**
	 * Retorna lista de fechas de valutas dada una moneda.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 25-09-2013, Angel Cris�stomo (Imagemaker IT): versi�n inicial
	 * </li>
	 * <li>1.1 16/02/2017 Marcos Abraham Hernandez Bravo (ImageMaker IT) -
	 * Miguel Anza (BCI) : Se agrega y normaliza log.</li>
	 * </ul>
	 * </p>
	 * 
	 * @param monedaDestino
	 *            Moneda de destino.
	 * @return List<ValutaTO>
	 * @throws GeneralException
	 *             Excepci�n de negocio.
	 * @since 1.0
	 */
	public List<ValutaTO> obtenerFechaValuta(String monedaDestino) throws GeneralException {

		final String nombreMetodo = "obtenerFechaValuta";
		final String identificador = (usuarioClienteModelMB != null
				&& usuarioClienteModelMB.getUsuarioModelMB() != null)
						? String.valueOf(usuarioClienteModelMB.getUsuarioModelMB().getRut()) : "";

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "][" + identificador + "] [BCI_INI]");
			getLogger().info("[" + nombreMetodo + "][" + identificador + "] [Parametro][monedaDestino]["
					+ StringUtil.contenidoDe(monedaDestino) + "]");
		}

		List<ValutaTO> valutas = null;
		ValutaTO[] valutaTOs = null;

		try {
			valutaTOs = this.servicioTransferencia.obtenerFechaValuta(monedaDestino);
			valutas = Arrays.asList(valutaTOs);
		} catch (TtffException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[" + nombreMetodo + "][" + identificador + "] [BCI_FINEX][TtffException]["
						+ e.getMessage() + "]", e);
			}
			throw new GeneralException(e.getCodigo(), e.getInfoAdic());
		} catch (RemoteException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[" + nombreMetodo + "][" + identificador + "] [BCI_FINEX][RemoteException]["
						+ e.getMessage() + "]", e);
			}
			throw new GeneralException(e.getMessage());
		}

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "][" + identificador + "] [BCI_FINOK]["
					+ StringUtil.contenidoDe(valutas) + "]");
		}

		return valutas;
	}

	/**
	 * Retorna lista de gastos banco exterior.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 25-09-2013, Angel Cris�stomo (Imagemaker IT): versi�n inicial
	 * </li>
	 * </ul>
	 * </p>
	 * 
	 * @return List<GastosBancoExteriorTO>
	 * @throws GeneralException
	 *             Excepci�n de negocio.
	 * @since 1.0
	 */
	public List<GastosBancoExteriorTO> obtenerGastosBancoExterior() throws GeneralException {

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[obtenerGastosBancoExterior][BCI_INI]["
					+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}

		List<GastosBancoExteriorTO> result = null;
		GastosBancoExteriorTO[] gastosBancoExteriorTOs = null;
		try {
			gastosBancoExteriorTOs = this.servicioTransferencia.obtenerGastosBancoExterior();
			result = Arrays.asList(gastosBancoExteriorTOs);
		} catch (TtffException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[obtenerGastosBancoExterior]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_FINEX][TtffException] mensaje de error " + e.getMessage(), e);
			}
			throw new GeneralException(e.getCodigo(), e.getInfoAdic());
		} catch (RemoteException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[obtenerGastosBancoExterior]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_FINEX][RemoteException] mensaje de error " + e.getMessage(), e);
			}
			throw new GeneralException(e.getMessage());
		}
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[obtenerGastosBancoExterior][BCI_FINOK]["
					+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}
		return result;
	}

	/**
	 * Retorna arreglo de string con informaci�n del pdf.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 25-09-2013, Angel Cris�stomo (Imagemaker IT): versi�n inicial
	 * </li>
	 * </ul>
	 * </p>
	 * 
	 * @param correlativoInterno
	 *            N�mero de correlativo interno.
	 * @return String[]
	 * @throws GeneralException
	 *             Excepci�n de negocio..
	 * @since 1.0
	 */
	public String[] obtenerLiquidacion(int correlativoInterno) throws GeneralException {

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger()
					.debug("[obtenerLiquidacion][BCI_INI][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}

		String[] mensaje = null;
		try {
			mensaje = this.servicioTransferencia.obtenerLiquidacion(correlativoInterno);
		} catch (TtffException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[obtenerLiquidacion]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_FINEX][TtffException] mensaje de error " + e.getMessage(), e);
			}
		} catch (RemoteException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[obtenerLiquidacion]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_FINEX][RemoteException] mensaje de error " + e.getMessage(), e);
			}
		}
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug(
					"[obtenerLiquidacion][BCI_FINOK][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}
		return mensaje;
	}

	/**
	 * Retorna arreglo de string con informaci�n del pdf.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 25-09-2013, Angel Cris�stomo (Imagemaker IT): versi�n inicial
	 * </li>
	 * </ul>
	 * </p>
	 * 
	 * @param correlativoInterno
	 *            Correlativo Interno.
	 * @return String[]
	 * @throws GeneralException
	 *             Excepci�n general de negocio.
	 * @since 1.0
	 */
	public String[] obtenerMensajeSwift(int correlativoInterno) throws GeneralException {

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug(
					"[obtenerMensajeSwift][BCI_INI][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}
		String[] mensaje = null;
		try {
			mensaje = this.servicioTransferencia.obtenerMensajeSwift(correlativoInterno);
		} catch (TtffException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[obtenerMensajeSwift]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_FINEX][TtffException] mensaje de error " + e.getMessage(), e);
			}
		} catch (RemoteException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[obtenerMensajeSwift]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_FINEX][RemoteException] mensaje de error " + e.getMessage(), e);
			}
		}
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug(
					"[obtenerMensajeSwift][BCI_FINOK][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}
		return mensaje;
	}

	/**
	 * Retorna lista de monedas.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 20/02/2019, Danilo Dominguez (Everis) - Minheli Mejias (Ing.
	 * Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param consultaMonedaTO
	 *            Entidad para consulta de moneda.
	 * @return List<Moneda1a1TO>
	 * @throws GeneralException
	 *             Excepci�n de negocio.
	 * @since 1.0
	 */
	public MonedasNodoTO obtenerMonedas() throws GeneralException {

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[obtenerMonedas][BCI_INI]");
		}

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[obtenerMonedas][usuarioClienteModelMB.getUsuarioModelMB().getRut()]["
					+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}

		List<MonedasPermitidasTO> listaMonedas = null;
		MonedasPermitidasTO[] monedasArray = null;
		NodosRippleTO[] nodosArray = null;
		List<NodosRippleTO> listaNodos = null;
		try {
			nodosArray = servicioTransferencia.obtenerNodosRippleDisp();
			listaNodos = Arrays.asList(nodosArray);
			NodosRippleTO nodoMiami = new NodosRippleTO();

			if (getLogger().isEnabledFor(Level.DEBUG)) {
				getLogger().debug("[obtenerMonedas][nodosArray][" + nodosArray + "], [listaNodos][" + listaNodos + "]");
			}

			for (NodosRippleTO nodosRippleTO : listaNodos) {
				getLogger().debug("NodosRIpple: " + nodosRippleTO.toString());
				if (nodosRippleTO.getNombreBanco().contains("BCI Miami")) {
					getLogger().debug("Se encotnro el nodo para BCI Miami, BIC: " + nodosRippleTO.getBic());
					nodoMiami = nodosRippleTO;
					break;
				}
			}

			monedasArray = servicioTransferencia.obtenerMonedasRipple(nodoMiami.getBic());
			listaMonedas = Arrays.asList(monedasArray);
			this.monedasNodo = new MonedasNodoTO();
			this.getMonedasNodo().setNodo(nodoMiami);
			this.getMonedasNodo().setLstMonedas(listaMonedas);

		} catch (RemoteException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				if (usuarioClienteModelMB != null && usuarioClienteModelMB.getUsuarioModelMB() != null) {
					getLogger().error("[obtenerMonedas]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
							+ " [BCI_FINEX][RemoteException] mensaje de error " + e.getMessage(), e);
				} else {
					getLogger().error("[obtenerMonedas][BCI_FINEX][RemoteException] mensaje de error " + e.getMessage(),
							e);
				}
			}
			throw new GeneralException(e.getMessage());
		} catch (TtffException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				if (usuarioClienteModelMB != null && usuarioClienteModelMB.getUsuarioModelMB() != null) {
					getLogger().error("[obtenerMonedas]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
							+ " [BCI_FINEX][TtffException] mensaje de error " + e.getMessage(), e);
				} else {
					getLogger().error("[obtenerMonedas] [BCI_FINEX][TtffException] mensaje de error " + e.getMessage(),
							e);
				}
			}
			throw new GeneralException(e.getCodigo(), e.getInfoAdic());
		}
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			if (usuarioClienteModelMB != null && usuarioClienteModelMB.getUsuarioModelMB() != null) {
				getLogger().info(
						"[obtenerMonedas][BCI_FINOK][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
			} else {
				getLogger().debug("[obtenerMonedas][BCI_FINOK][0]");
			}
		}

		return monedasNodo;
	}

	/**
	 * Servicio de consulta de transferencias.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 16/10/2013, Mauricio Flores S�enz (Imagemaker IT): versi�n
	 * inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param consultaTransferenciaTO
	 *            TO de consulta.
	 * @param pendiente
	 *            flag de transferencias pendientes.
	 * @param esApoderado
	 *            flag si usuario es apoderado.
	 * @return List<TransferenciaTO>
	 * @throws GeneralException
	 *             Excepcion de negocio.
	 * @since 1.0
	 */
	public List<TransferenciaTO> obtenerTransferencias(ConsultaTransferenciaTO consultaTransferenciaTO,
			boolean pendiente, boolean esApoderado) throws GeneralException {
		List<TransferenciaTO> response = new ArrayList<TransferenciaTO>();
		if (getLogger().isDebugEnabled()) {
			getLogger().debug("[obtenerTransferencias][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
					+ "][BCI_INI] >> " + consultaTransferenciaTO.toString());
		}
		try {
			Transferencia1a1TO[] array = this.servicioTransferencia.obtenerTransferencias(consultaTransferenciaTO);
			if (pendiente) {

				if (array != null) {
					for (int index = 0; index < array.length; index++) {
						response.add(this.crearTransferenciaTO(array[index], esApoderado));
					}
				}
			} else {
				if (array != null) {
					for (int index = 0; index < array.length; index++) {
						response.add(this.crearTransferenciaTO(array[index]));
					}
				}
			}
		} catch (RemoteException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[obtenerTransferencias]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_FINEX][RemoteException] mensaje de error " + e.getMessage(), e);
			}
			throw new GeneralException("ESPECIAL", e.getMessage());
		}

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug(
					"[obtenerTransferencias][BCI_FINOK][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}
		return response;
	}

	/**
	 * Servicio de consulta de transferencias.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 16/10/2013, Mauricio Flores S�enz (Imagemaker IT): versi�n
	 * inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param consultaTransferenciaTO
	 *            TO de consulta.
	 * @param pendiente
	 *            flag de transferencias pendientes.
	 * @param esApoderado
	 *            flag si usuario es apoderado.
	 * @return List<TransferenciaTO>
	 * @throws GeneralException
	 *             Excepcion de negocio.
	 * @since 1.0
	 */
	public List<TransferenciaTO> obtenerTransferenciasPedientes(ConsultaTransferenciaTO consultaTransferenciaTO,
			boolean pendiente, boolean esApoderado) throws GeneralException {
		List<TransferenciaTO> response = new ArrayList<TransferenciaTO>();
		if (getLogger().isDebugEnabled()) {
			getLogger().debug("[obtenerTransferencias][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
					+ "][BCI_INI] >> " + consultaTransferenciaTO.toString());
		}
		try {
			Transferencia1a1TO[] array = this.servicioTransferencia
					.obtenerTransferenciasPenRipple(consultaTransferenciaTO);
			if (array != null) {
				for (int index = 0; index < array.length; index++) {
					response.add(this.crearTransferenciaTO(array[index], esApoderado));
				}
			}
		} catch (RemoteException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[obtenerTransferencias]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_FINEX][RemoteException] mensaje de error " + e.getMessage(), e);
			}
			throw new GeneralException("ESPECIAL", e.getMessage());
		}

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug(
					"[obtenerTransferencias][BCI_FINOK][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}
		return response;
	}

	/**
	 * M�todo encargado de realizar l�gica de curse de una transferencia.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 21/03/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing.
	 * Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param transferenciaMonexTO
	 *            Entidad de transferencia.
	 * @param autorizacionTO
	 *            Entidad de autorizaci�n.
	 * @param eventos
	 *            Entidad de eventos.
	 * @param cambiaEstadoTttfMonexTO
	 *            Entidad de cambio de estado.
	 * @param esApoderadoReal
	 *            flag si es apoderadoReal.
	 * @return boolean
	 * @throws GeneralException
	 *             Excepci�n de negocio.
	 * @since 1.0
	 */
	public boolean procesarTransferenciaMonex(TransferenciaMonexTO transferenciaMonexTO,
			DatosAutorizacionTO autorizacionTO, Eventos eventos, CambiaEstadoTttfMonexTO cambiaEstadoTttfMonexTO,
			boolean esApoderadoReal) throws GeneralException {

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[procesarTransferenciaMonex][BCI_INI]["
					+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}

		boolean pendienteFirma = false;
		try {
			if (esApoderadoReal) {
				boolean resultado = false;
				DatosTransferMonexTO datosAutorizacion = new DatosTransferMonexTO();
				datosAutorizacion.setRutEmpresa(autorizacionTO.getRutEmpresa());
				datosAutorizacion.setDvEmpresa(autorizacionTO.getDvEmpresa());
				String convenio = StringUtil.completaConEspacios(autorizacionTO.getConvenio(), LARGO_CONVENIO);
				datosAutorizacion.setConvenio(convenio);
				datosAutorizacion.setRutUsuario(autorizacionTO.getRutUsuario());
				datosAutorizacion.setDvUsuario(autorizacionTO.getDvUsuario());
				datosAutorizacion.setIdAutorizacion(String.valueOf(transferenciaMonexTO.getNumeroCorrelativoFbp()));
				datosAutorizacion.setMonedaCuenta(transferenciaMonexTO.getMonedaCtacte());
				datosAutorizacion.setMontoDetallesAprobados(transferenciaMonexTO.getMontoDebito().doubleValue());
				try {
					if (getLogger().isDebugEnabled()) {
						getLogger().debug(
								"[procesarTransferenciaMonex]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
										+ "Autorizando transferencia con validacion de monto");
					}
					servicioTransferencia.autorizaPagoCuentaCorrienteMonto(datosAutorizacion);
					if (getLogger().isDebugEnabled()) {
						getLogger().debug("[procesarTransferenciaMonex]"
								+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + "Transferencia autorizada");
					}
					resultado = true;
				} catch (TransferenciaDeFondosException e) {
					if (getLogger().isEnabledFor(Level.WARN)) {
						getLogger().warn(
								"[procesarTransferenciaMonex]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
										+ " [TransferenciaDeFondosException] mensaje de error " + e.getMessage(),
								e);
					}
					String codigoError = e.getCodigo();
					if (codigoError.equals(CODIGO_TFMX_0089)) {
						if (getLogger().isEnabledFor(Level.WARN)) {
							getLogger().warn("[procesarTransferenciaMonex]"
									+ usuarioClienteModelMB.getUsuarioModelMB().getRut()
									+ " Error al realizar conversion de moneda. Se realiza autorizacion de transferencia sin validacion de monto");
						}
						BCIExpressDelegate delegate = new BCIExpressDelegate(eventos);
						String rutEmpresa = StringUtil.rellenaConCeros(String.valueOf(autorizacionTO.getRutEmpresa()),
								LARGO_RUT);
						String rutUsuario = StringUtil.rellenaConCeros(autorizacionTO.getRutUsuario() + "", LARGO_RUT);
						char dvEmpresa = autorizacionTO.getDvEmpresa();
						char dvUsuario = autorizacionTO.getDvUsuario();
						ResultAutorizaPagoCuentaCorriente result = delegate.autorizaPagoCuentaCorriente(rutUsuario,
								dvUsuario, convenio, rutEmpresa, dvEmpresa,
								String.valueOf(transferenciaMonexTO.getNumeroCorrelativoFbp()),
								CODIGO_AUTORIZACION.charAt(0), FECHA_INICIAL);
						AutorizaPagoCtaCte[] autPagoCtaCte = result.getAutorizaPagoCtaCte();
						resultado = getProcesaResultadoAutorizacion(autPagoCtaCte);
					} else if (codigoError.equals(CODIGO_TFMX_0068)) {
						if (getLogger().isEnabledFor(Level.INFO)) {
							getLogger().info(
									"[procesarTransferenciaMonex]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
											+ " Transferencia qued pendiente de firma");
						}
						resultado = false;
					} else if (codigoError.equals(CODIGO_TFMX_0069)) {
						throw new NegocioException(CODIGO_USUARIO_NO_AUTORIZADO);
					} else if (codigoError.equals(CODIGO_TFMX_0070)) {
						throw new NegocioException(CODIGO_FIRMA_REPETIDA);
					} else if (e.getMessage().equals(CODIGO_TFMX_0071)) {
						throw new NegocioException(CODIGO_ERROR_SISTEMA);
					} else {
						throw e;
					}
				}
				if (getLogger().isEnabledFor(Level.INFO)) {
					getLogger().info("[procesarTransferenciaMonex] Firma registrada ["
							+ transferenciaMonexTO.getNumeroCorrelativoFbp() + "], operacion pendiente de firma["
							+ resultado + "]");
				}
				if (resultado) {
					/*
					 * //LOGICA COMENTADA DE ACUERDO A DEFINICION, SE ESPERA
					 * VOLVER A REVISAR ESTA LOGICA SI APLICA REEMPLAZARLA POR
					 * LLAMADA A SP 09 if (getLogger().isEnabledFor(Level.INFO))
					 * { getLogger().info(
					 * "[procesarTransferenciaMonex] Autorizacion OK [" +
					 * transferenciaMonexTO.getNumeroCorrelativoFbp() + "]"); }
					 * cambiaEstadoTttfMonexTO.setServicioSolicitado(
					 * SOLICITADO_SIG); String timestamp =
					 * cambiaEstadoTransferencia(cambiaEstadoTttfMonexTO);
					 * cambiaEstadoTttfMonexTO.setSystimestamp(timestamp);
					 * cambiaEstadoTttfMonexTO.setServicioSolicitado(
					 * SOLICITADO_EXE);
					 */
					pendienteFirma = false;
				} else {
					cambiaEstadoTttfMonexTO.setServicioSolicitado(SOLICITADO_SIG);
					pendienteFirma = true;
				}
			} else {
				cambiaEstadoTttfMonexTO.setServicioSolicitado(SOLICITADO_SIG);
				pendienteFirma = false;
			}
			cambiaEstadoTransferencia(cambiaEstadoTttfMonexTO);
		} catch (TtffException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[procesarTransferenciaMonex]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_FINEX][TtffException] mensaje de error " + e.getMessage(), e);
			}
			try {
				if (getLogger().isDebugEnabled()) {
					getLogger().debug("[procesarTransferenciaMonex]"
							+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + " eliminando firma transferencia");
				}
				borraTransferenciaAUT(autorizacionTO, transferenciaMonexTO);
				if (getLogger().isDebugEnabled()) {
					getLogger().debug("[procesarTransferenciaMonex]"
							+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + " firma eliminada");
				}
			} catch (GeneralException ge) {
				if (getLogger().isEnabledFor(Level.ERROR)) {
					getLogger()
							.error("[procesarTransferenciaMonex]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
									+ " [GeneralException] error al borra firma transferencia, mensaje=<"
									+ e.getMessage() + ">", e);
				}
			}
			throw new GeneralException(e.getCodigo(), e.getInfoAdic());
		} catch (NegocioException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[procesarTransferenciaMonex]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_FINEX][NegocioException] mensaje de error " + e.getMessage(), e);
			}
			if (!CODIGO_FIRMA_REPETIDA.equals(e.getMessage())) {
				try {
					if (getLogger().isDebugEnabled()) {
						getLogger().debug(
								"[procesarTransferenciaMonex]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
										+ " eliminando firma transferencia");
					}
					borraTransferenciaAUT(autorizacionTO, transferenciaMonexTO);
					if (getLogger().isDebugEnabled()) {
						getLogger().debug("[procesarTransferenciaMonex]"
								+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + " firma eliminada");
					}
				} catch (GeneralException ge) {
					if (getLogger().isEnabledFor(Level.ERROR)) {
						getLogger().error(
								"[procesarTransferenciaMonex]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
										+ " [GeneralException] error al borra firma transferencia, mensaje=<"
										+ e.getMessage() + ">",
								e);
					}
				}
			}
			if (CODIGO_FIRMA_REPETIDA.equals(e.getMessage())) {
				throw new NegocioException(CODIGO_FIRMA_REPETIDA);
			}
			throw new GeneralException(ESPECIAL, e.getMessage());
		} catch (RemoteException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[procesarTransferenciaMonex]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_FINEX][RemoteException] mensaje de error " + e.getMessage(), e);
			}
			try {
				if (getLogger().isDebugEnabled()) {
					getLogger().debug("[procesarTransferenciaMonex]"
							+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + " eliminando firma transferencia");
				}
				borraTransferenciaAUT(autorizacionTO, transferenciaMonexTO);
				if (getLogger().isDebugEnabled()) {
					getLogger().debug("[procesarTransferenciaMonex]"
							+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + " firma eliminada");
				}
			} catch (GeneralException ge) {
				if (getLogger().isEnabledFor(Level.ERROR)) {
					getLogger()
							.error("[procesarTransferenciaMonex]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
									+ " [GeneralException] error al borra firma transferencia, mensaje=<"
									+ e.getMessage() + ">", e);
				}
			}
			throw new GeneralException(e.getMessage(), e.getMessage());
		} catch (TransferenciaDeFondosException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[procesarTransferenciaMonex]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_FINEX][TransferenciaDeFondosException] mensaje de error " + e.getMessage(), e);
			}
			try {
				if (getLogger().isDebugEnabled()) {
					getLogger().debug("[procesarTransferenciaMonex]"
							+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + " eliminando firma transferencia");
				}
				borraTransferenciaAUT(autorizacionTO, transferenciaMonexTO);
				if (getLogger().isDebugEnabled()) {
					getLogger().debug("[procesarTransferenciaMonex]"
							+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + " firma eliminada");
				}
			} catch (GeneralException ge) {
				if (getLogger().isEnabledFor(Level.ERROR)) {
					getLogger()
							.error("[procesarTransferenciaMonex]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
									+ " [GeneralException] error al borra firma transferencia, mensaje=<"
									+ e.getMessage() + ">", e);
				}
			}
			throw new GeneralException(e.getCodigo(), e.getInfoAdic());
		} catch (Exception e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[procesarTransferenciaMonex]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_FINEX][Exception] mensaje de error " + e.getMessage(), e);
			}
			try {
				if (getLogger().isDebugEnabled()) {
					getLogger().debug("[procesarTransferenciaMonex]"
							+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + " eliminando firma transferencia");
				}
				borraTransferenciaAUT(autorizacionTO, transferenciaMonexTO);
				if (getLogger().isDebugEnabled()) {
					getLogger().debug("[procesarTransferenciaMonex]"
							+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + " firma eliminada");
				}
			} catch (GeneralException ge) {
				if (getLogger().isEnabledFor(Level.ERROR)) {
					getLogger()
							.error("[procesarTransferenciaMonex]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
									+ " [GeneralException] error al borra firma transferencia, mensaje=<"
									+ e.getMessage() + ">", e);
				}
			}
			throw new GeneralException(ESPECIAL, e.getMessage());
		}
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[procesarTransferenciaMonex][BCI_FINOK]["
					+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}
		return pendienteFirma;
	}

	/**
	 * Retorna los apoderados que han firmado una transferencia.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 25-10-2013, Angel Cris�stomo (Imagemaker IT): versi�n inicial
	 * </li>
	 * </ul>
	 * </p>
	 * 
	 * @param numeroCorrelativoFbp
	 *            numero de transferencia.
	 * @param autorizacionTO
	 *            Entidad de autorizacionTO.
	 * @param eventos
	 *            Entidad de eventos.
	 * @return ResultConsultarApoderadosquehanFirmado
	 * @throws GeneralException
	 *             Excepci�n de negocio.
	 * @since 1.0
	 */
	public ResultConsultarApoderadosquehanFirmado consultarApoderadosQueHanFirmado(int numeroCorrelativoFbp,
			DatosAutorizacionTO autorizacionTO, Eventos eventos) throws GeneralException {

		final String nombreMetodo = "consultarApoderadosQueHanFirmado";
		final String identificador = (usuarioClienteModelMB != null
				&& usuarioClienteModelMB.getUsuarioModelMB() != null)
						? String.valueOf(usuarioClienteModelMB.getUsuarioModelMB().getRut()) : "";

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "][" + identificador + "] [BCI_INI]");
			getLogger().info("[" + nombreMetodo + "][" + identificador + "] [Parametro][numeroCorrelativoFbp]["
					+ StringUtil.contenidoDe(numeroCorrelativoFbp) + "]");
			getLogger().info("[" + nombreMetodo + "][" + identificador + "] [Parametro][autorizacionTO]["
					+ StringUtil.contenidoDe(autorizacionTO) + "]");
			getLogger().info("[" + nombreMetodo + "][" + identificador + "] [Parametro][eventos]["
					+ StringUtil.contenidoDe(eventos) + "]");
		}

		BCIExpressDelegate delegate = new BCIExpressDelegate(eventos);
		ResultConsultarApoderadosquehanFirmado apoderadosquehanFirmado = null;

		try {
			apoderadosquehanFirmado = delegate.consultarApoderadosquehanFirmado(autorizacionTO.getRutEmpresa(),
					autorizacionTO.getConvenio(), String.valueOf(numeroCorrelativoFbp));
		} catch (GeneralException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[" + nombreMetodo + "][" + identificador + "] [BCI_FINEX][GeneralException]["
						+ e.getMessage() + "]", e);
			}
			throw new GeneralException(e.getMessage(), e.getMessage());
		}

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "][" + identificador + "] [BCI_FINOK]["
					+ StringUtil.contenidoDe(apoderadosquehanFirmado) + "]");
		}
		return apoderadosquehanFirmado;
	}

	public void setSesion(SesionMB sesion) {
		this.sesion = sesion;
	}

	/**
	 * M�todo que permite borrar el registro de la autorizaci�n que realiz� el
	 * apoderado a una transferencia.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 21/03/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing.
	 * Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param autorizacionTO
	 *            Entidad de negocio.
	 * @param transferenciaMonexTO
	 *            Entidad de negocio.
	 * @return int
	 * @throws GeneralException
	 *             Excepci�n de negocio.
	 * @since 1.0
	 */
	public int borraTransferenciaAUT(DatosAutorizacionTO autorizacionTO, TransferenciaMonexTO transferenciaMonexTO)
			throws GeneralException {

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug(
					"[borraTransferenciaAUT][BCI_INI][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}

		final int noBorro = 2; // <-- NUMERO MAGICO
		int resultado = noBorro;
		BCIExpress bciExpress = getBciExpress();
		try {
			String rutEmpresa = StringUtil.rellenaConCeros(String.valueOf(autorizacionTO.getRutEmpresa()), LARGO_RUT);
			String rutUsuario = StringUtil.rellenaConCeros(autorizacionTO.getRutUsuario() + "", LARGO_RUT);
			char dvEmpresa = autorizacionTO.getDvEmpresa();
			char dvUsuario = autorizacionTO.getDvUsuario();
			resultado = bciExpress.borraTransferenciaAUT(rutUsuario, dvUsuario, autorizacionTO.getConvenio(),
					rutEmpresa, dvEmpresa, String.valueOf(transferenciaMonexTO.getNumeroCorrelativoFbp()));
			if (resultado == noBorro) {
				throw new GeneralException("2", "Error al borrar transferencia");
			}
		} catch (RemoteException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[borraTransferenciaAUT]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_FINEX][RemoteException] mensaje de error " + e.getMessage(), e);
			}
			throw new GeneralException(e.getMessage(), e.getMessage());
		}
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug(
					"[borraTransferenciaAUT][BCI_FINOK][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}
		return resultado;
	}

	public UsuarioClienteModelMB getUsuarioClienteModelMB() {
		return usuarioClienteModelMB;
	}

	public void setUsuarioClienteModelMB(UsuarioClienteModelMB usuarioClienteModelMB) {
		this.usuarioClienteModelMB = usuarioClienteModelMB;
	}

	public Integer getCorrelativoInternoTransferencia() {
		return correlativoInternoTransferencia;
	}

	public void setCorrelativoInternoTransferencia(Integer correlativoInternoTransferencia) {
		this.correlativoInternoTransferencia = correlativoInternoTransferencia;
	}

	public Boolean getTransferenciaPendienteFirma() {
		return transferenciaPendienteFirma;
	}

	public void setTransferenciaPendienteFirma(Boolean transferenciaPendienteFirma) {
		this.transferenciaPendienteFirma = transferenciaPendienteFirma;
	}

	/**
	 * Consulta de monedas por c�digos.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 17/10/2013, Mauricio Flores S�enz (Imagemaker IT): versi�n
	 * inicial</li>
	 * <li>1.1 20/08/2018, Bastian Nelson (Sermaluc) - Gonzalo Munoz(Ing. Soft.
	 * BCI): se modifican log.</li>
	 * </ul>
	 * </p>
	 * 
	 * @param codigoProducto
	 *            Codigo producto.
	 * @param origenSolicitud
	 *            Origen de solicitud.
	 * @return List<Moneda1a1TO>
	 * @throws GeneralException
	 *             Excepci�n General.
	 * @since 1.0
	 */
	private List<Moneda1a1TO> consultarMonedas(String codigoProducto, String origenSolicitud) throws GeneralException {

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			if (getLogger().isEnabledFor(Level.INFO)) {
				if (usuarioClienteModelMB != null && usuarioClienteModelMB.getUsuarioModelMB() != null) {
					getLogger().info("[crearTransferenciaTO][BCI_INI]["
							+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
				}

			} else {

				getLogger().debug("[consultarDetallePago][BCI_INI][0]");
			}
		}

		ConsultaMonedaTO consultaMonedaTO = new ConsultaMonedaTO();
		consultaMonedaTO.setCodigoProducto(codigoProducto);
		consultaMonedaTO.setOrigenSolicitud(origenSolicitud);
		return this.obtenerMonedas(consultaMonedaTO);
	}

	/**
	 * Consulta saldo.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 18/10/2013, Mauricio Flores S�enz (Imagemaker IT): versi�n
	 * inicial</li>
	 * <li>1.1 20/08/2018, Bastian Nelson (Sermaluc) - Gonzalo Munoz(Ing. Soft.
	 * BCI): Se modifican log.</li>
	 * </ul>
	 * </p>
	 * 
	 * @param ctaCte
	 *            Cta Cte.
	 * @return Double
	 * @since 1.0
	 */
	private Double consultarSaldoDisponible(String ctaCte) {
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			if (getLogger().isEnabledFor(Level.DEBUG)) {
				if (usuarioClienteModelMB != null && usuarioClienteModelMB.getUsuarioModelMB() != null) {
					getLogger().debug("[consultarSaldoDisponible][BCI_INI]["
							+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
				} else {
					getLogger().debug("[consultarSaldoDisponible][BCI_INI][0]");
				}
			}
		}
		Double saldo = null;
		try {
			MonexSaldo monexSaldo = this.getServicioMonex().entregaSaldo(ctaCte);
			if (monexSaldo != null) {
				saldo = monexSaldo.getSaldoDisp();
			}
		} catch (Exception e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				if (usuarioClienteModelMB != null && usuarioClienteModelMB.getUsuarioModelMB() != null) {
					getLogger().error("[consultarSaldoDisponible]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
							+ " [BCI_FINEX][Exception] mensaje de error " + e.getMessage(), e);
				} else {
					getLogger().error(
							"[consultarSaldoDisponible][BCI_FINEX][Exception] mensaje de error " + e.getMessage(), e);
				}
			}
		}
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			if (getLogger().isEnabledFor(Level.DEBUG)) {
				if (usuarioClienteModelMB != null && usuarioClienteModelMB.getUsuarioModelMB() != null) {
					getLogger().debug("[consultarSaldoDisponible][BCI_FINOK]["
							+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
				} else {
					getLogger().debug("[consultarSaldoDisponible][BCI_FINOK][0]");
				}
			}
		}
		return saldo;
	}

	/**
	 * Crea una cuenta monex.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 17/10/2013, Mauricio Flores S�enz (Imagemaker IT): versi�n
	 * inicial</li>
	 * <li>1.1 20/08/2018, Bastian Nelson (Sermaluc) - Gonzalo Munoz(Ing. Soft.
	 * BCI): Se modifican log.</li>
	 * </ul>
	 * </p>
	 * 
	 * @param moneda
	 *            Codigo Moneda.
	 * @param numeroCuenta
	 *            N�mero de Cuenta.
	 * @param saldoDisponible
	 *            Saldo disponible.
	 * @param simboloMoneda
	 *            Simbolo de moneda.
	 * @return CuentaMonexTO
	 * @since 1.0
	 */
	private CuentaMonexTO crearCuentaMonexTO(CodigoValor moneda, String numeroCuenta, Double saldoDisponible,
			String simboloMoneda) {
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			if (usuarioClienteModelMB != null && usuarioClienteModelMB.getUsuarioModelMB() != null) {
				getLogger().debug(
						"[crearCuentaMonexTO][BCI_INI][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
			} else {
				getLogger().debug("[crearCuentaMonexTO][BCI_INI][0]");
			}
		}

		CuentaMonexTO cuentaMonexTO = new CuentaMonexTO();
		cuentaMonexTO.setMoneda(moneda);
		cuentaMonexTO.setNumeroCuenta(numeroCuenta);
		cuentaMonexTO.setSaldoDisponible(saldoDisponible);
		cuentaMonexTO.setSimboloMoneda(simboloMoneda);
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			if (usuarioClienteModelMB != null && usuarioClienteModelMB.getUsuarioModelMB() != null) {
				getLogger().debug(
						"[crearCuentaMonexTO][BCI_FINOK][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
			} else {
				getLogger().debug("[crearCuentaMonexTO][BCI_FINOK][0]");
			}
		}

		return cuentaMonexTO;
	}

	/**
	 * Crear Cuenta Monex.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 17/10/2013, Mauricio Flores S�enz (Imagemaker IT): versi�n
	 * inicial</li>
	 * <li>1.1 20/08/2018, Bastian Nelson (Sermaluc) - Gonzalo Munoz(Ing. Soft.
	 * BCI): se mofifican log.</li>
	 * </ul>
	 * </p>
	 * 
	 * @param cuentaComisionTO
	 *            Parametro de entrada de servicio.
	 * @return CuentaMonexTO
	 * @since 1.0
	 */
	private CuentaMonexTO crearCuentaMonexTO(CuentaComisionTO cuentaComisionTO) {
		if (getLogger().isEnabledFor(Level.INFO)) {
			if (usuarioClienteModelMB != null && usuarioClienteModelMB.getUsuarioModelMB() != null) {
				getLogger().info(
						"[crearTransferenciaTO][BCI_INI][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
			} else {
				getLogger().debug("[consultarDetallePago][BCI_INI][0]");
			}

		}
		CuentaMonexTO cuentaMonexTO = new CuentaMonexTO();
		cuentaMonexTO.setMoneda(new CodigoValor(cuentaComisionTO.getMonedaCtacte(), ""));
		cuentaMonexTO.setNumeroCuenta(cuentaComisionTO.getCtaDebito());
		List<Moneda1a1TO> listadoMonedas = null;
		try {
			listadoMonedas = this.consultarMonedas(
					TablaValores.getValor(PYME_TRANSFER_TABLE, "codigosMoneda", "codigoProductoCVD"),
					TablaValores.getValor(PYME_TRANSFER_TABLE, "codigosMoneda", "origenSolicitud"));
			for (Moneda1a1TO moneda1a1to : listadoMonedas) {
				if (moneda1a1to.getCodigoIso().equals(cuentaMonexTO.getMoneda().getCodigo())) {
					cuentaMonexTO.getMoneda().setValor(moneda1a1to.getNombreMoneda());
					break;
				}
			}
		} catch (GeneralException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				if (usuarioClienteModelMB != null && usuarioClienteModelMB.getUsuarioModelMB() != null) {
					getLogger().error("[crearCuentaMonexTO]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
							+ " [BCI_FINEX][GeneralException] mensaje de error " + e.getMessage(), e);
				} else {
					getLogger().error(
							"[crearCuentaMonexTO][BCI_FINEX][GeneralException] mensaje de error " + e.getMessage(), e);
				}
			}
		}

		if (getLogger().isEnabledFor(Level.INFO)) {
			if (getLogger().isEnabledFor(Level.INFO)) {
				if (usuarioClienteModelMB != null && usuarioClienteModelMB.getUsuarioModelMB() != null) {
					getLogger().info("[crearTransferenciaTO][BCI_FINOK]["
							+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
				}
			} else {
				getLogger().debug("[consultarDetallePago][BCI_FINOK][0]");
			}
		}
		return cuentaMonexTO;
	}

	/**
	 * Llena campos de transferencia.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 25/03/2019, Sergio Bustos B. (Everis) - Minheli Mejias (Ing.
	 * Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param detallePago1a1TO
	 *            Entidad de detalle.
	 * @param transferencia
	 *            Entidad transferencia.
	 * @since 1.0
	 */
	private void crearTransferenciaTO(DetallePago1a1TO detallePago1a1TO, TransferenciaTO transferencia) {

		if (getLogger().isEnabledFor(Level.INFO)) {
			if (usuarioClienteModelMB != null && usuarioClienteModelMB.getUsuarioModelMB() != null) {
				getLogger().info(
						"[crearTransferenciaTO][BCI_INI][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
			}
		} else {
			getLogger().debug("[consultarDetallePago][BCI_INI][0]");
		}
		CuentaMonexTO cuentaCargoComision = null;
		try {
			usuarioClienteModelMB.getUsuarioModelMB().getDigitoVerif();
			final String rutEmpresa = StringUtil.rellenaConCeros(
					StringUtil.sacaCeros(transferencia.getConsulta().getRutEmpresa()), RELLENA_CON_CEROS);
			CuentaComisionTO cuentaComisionTO = this.obtenerCuentaDeCargo(rutEmpresa);
			cuentaCargoComision = this.crearCuentaMonexTO(cuentaComisionTO);
		} catch (GeneralException e) {
			getLogger().error("[crearTransferenciaTO] ERRROR EN EL SERVICIO obtenerCuentaDeCargo >> ", e);
		}
		String simboloMoneda = this.getResourceValue(detallePago1a1TO.getMonedaCta());
		transferencia.setMontoTransferir(detallePago1a1TO.getMontoPago().doubleValue());
		transferencia.setCuentaCargoComision(cuentaCargoComision);

		Double saldoDisponible = 0d;

		saldoDisponible = this.consultarSaldoDisponible(detallePago1a1TO.getCuentaDebito());

		transferencia.setCuentaOrigen(this.crearCuentaMonexTO(
				new CodigoValor(detallePago1a1TO.getMonedaCta(), detallePago1a1TO.getNombreMoneda()),
				detallePago1a1TO.getCuentaDebito(), saldoDisponible, simboloMoneda));

		transferencia.setGastoBancoExterior(
				new CodigoValor(detallePago1a1TO.getDetalleGastos(), detallePago1a1TO.getDescripcionDetalle()));
		transferencia.setMotivoPago(detallePago1a1TO.getInformacionRemesa());
		transferencia.setCodigoCambio(
				new CodigoValor(detallePago1a1TO.getCodigoCambio(), detallePago1a1TO.getNombreCodigo()));
		transferencia.getBeneficiario().setReferencia(detallePago1a1TO.getCodigoIdentificador());
		transferencia.getBeneficiario().setDireccion(detallePago1a1TO.getDireccion1());
		transferencia.getBeneficiario().setNombre(detallePago1a1TO.getNombreBeneficiarioL1());
		transferencia.getBeneficiario()
				.setPaisCuenta(new CodigoValor(detallePago1a1TO.getPaisBco(), detallePago1a1TO.getNombrePaisBcoBene()));
		transferencia.getBeneficiario().setMail(detallePago1a1TO.getEmail1());
		transferencia.getBeneficiario().getCuenta()
				.setMoneda(new CodigoValor(detallePago1a1TO.getMonedaCta(), detallePago1a1TO.getNombreMoneda()));
		transferencia.getBeneficiario().getCuenta().setNumeroCuenta(detallePago1a1TO.getCuentaCorriente());
		transferencia.setBancoBeneficiario(this.crearBanco(detallePago1a1TO.getBicBanco(),
				detallePago1a1TO.getNomDirBancoL1(), detallePago1a1TO.getRutaPago()));
		transferencia.setFechaActualizacion(detallePago1a1TO.getSystimestamp());

		Date fecha = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		transferencia.setFechaHoy(dateFormat.format(fecha));
		transferencia.getBeneficiario().setNombre2(detallePago1a1TO.getNombre2());
		transferencia.getBeneficiario().setDireccion1(detallePago1a1TO.getDireccion1());
		transferencia.getBeneficiario().setDireccion2(detallePago1a1TO.getDireccion2());
		transferencia.getBeneficiario().setNombrePaisBeneficiario(detallePago1a1TO.getNombrePaisBeneficiario());
		transferencia.getBeneficiario().setCiudad1(detallePago1a1TO.getCiudad1());
		transferencia.getBeneficiario().setInformacionAdicionalDos(detallePago1a1TO.getInformacionAdicionalDos());

		if (getLogger().isEnabledFor(Level.INFO)) {

			if (usuarioClienteModelMB != null && usuarioClienteModelMB.getUsuarioModelMB() != null) {
				getLogger().debug(
						"[consultarDetallePago][FIN_OK][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
			} else {
				getLogger().debug("[consultarDetallePago][BCI_OK][0]");
			}
		}
	}

	/**
	 * Obtiene datos del usuario necesario para realizar consulta firma y
	 * poderes.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 15-10-2013, Angel Cris�stomo (Imagemaker IT): versi�n inicial
	 * </li>
	 * </ul>
	 * </p>
	 * 
	 * @return Eventos
	 * @throws NoSessionException
	 *             Excepci�n de negocio.
	 * @since 1.0
	 */
	@SuppressWarnings("unchecked")
	protected Eventos getEventos() throws NoSessionException {
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[getEventos][BCI_INI][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
				.getRequest();
		AdaptadorDeContextos adaptador = new AdaptadorDeContextos(request);
		adaptador.getEventos().atributos.put("tipoUsuario", usuarioClienteModelMB.getClienteMB().getTipoUsuario());
		adaptador.getEventos().atributos.put("perfil", usuarioClienteModelMB.getPerfilUsuarioCliente());
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[getEventos][BCI_FINOK][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}
		return adaptador.getEventos();
	}

	/**
	 * LLena informaci�n de TO necesario para la l�gica de validafirmaYpoderes y
	 * autorizaPagoCuentaCorriente.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 25/02/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing.
	 * Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @return DatosAutorizacionTO
	 * @since 1.0
	 */
	protected DatosAutorizacionTO getDatosAutorizacionTO() {
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug(
					"[getDatosAutorizacionTO][BCI_INI][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}
		DatosAutorizacionTO autorizacionTO = new DatosAutorizacionTO();
		autorizacionTO.setRutEmpresa(usuarioClienteModelMB.getClienteMB().getRut());
		autorizacionTO.setDvEmpresa(usuarioClienteModelMB.getClienteMB().getDigitoVerif());
		autorizacionTO.setRutUsuario(usuarioClienteModelMB.getUsuarioModelMB().getRut());
		autorizacionTO.setDvUsuario(usuarioClienteModelMB.getUsuarioModelMB().getDigitoVerif());
		autorizacionTO.setConvenio(usuarioClienteModelMB.getClienteMB().getNumeroConvenio());
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug(
					"[getDatosAutorizacionTO][BCI_FINOK][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}
		return autorizacionTO;
	}

	/**
	 * Valida l�gica para habilitar o deshabilitar los enlaces de edici�n y/o
	 * eliminaci�n.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 26-12-2013, Angel Cris�stomo L�pez (Imagemaker IT): versi�n
	 * inicial</li>
	 * <li>1.1 07-01-2014, Angel Cris�stomo L�pez (Imagemaker IT): se agregan
	 * nuevos codigos para habilitar o deshabilitar los enlaces</li>
	 * </ul>
	 * </p>
	 * 
	 * @param estadoTransferencia
	 *            estado de la transferencia.
	 * @param esApoderado
	 *            flag boton disabled.
	 * @return boolean
	 * @since 1.0
	 */
	private boolean isDisableButton(String estadoTransferencia, boolean esApoderado) {
		try {
			if (getLogger().isEnabledFor(Level.DEBUG)) {
				getLogger().debug("[isDisableButton][BCI_INI][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ "] Estado transferencia : " + estadoTransferencia);
				getLogger().debug("[isDisableButton][BCI_INI][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ "] Es apoderado : " + esApoderado);
			}
			if ("PAPP".equalsIgnoreCase(estadoTransferencia) || "PROC".equalsIgnoreCase(estadoTransferencia)) {
				return true;
			}
			if ("PFUN".equalsIgnoreCase(estadoTransferencia)) {
				return false;
			}
			if (!esApoderado) {
				if ("PSIG".equalsIgnoreCase(estadoTransferencia)) {
					return false;
				}
			}
		} catch (Exception e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[isDisableButton]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_FINEX][Exception] mensaje de error " + e.getMessage(), e);
			}
		}

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger()
					.debug("[isDisableButton][BCI_FINOK][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}

		return true;
	}

	/**
	 * Valida l�gica para habilitar o deshabilitar enlace de apoderado y
	 * operador en la p�gina de panorama de transferencia.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.2 28-01-2014, Mauricio Flores S�enz (Imagemaker IT): versi�n
	 * inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param estadoTransferencia
	 *            C�digo de estado de la transferencia.
	 * @param esApoderado
	 *            Flag que indica si es apoderado. El valor true indica que es,
	 *            sino es operador.
	 * @return boolean
	 * @since 1.2
	 */
	private boolean renderApoderado(String estadoTransferencia, boolean esApoderado) {
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[renderApoderado][BCI_INI][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}

		boolean render = true;
		try {
			if ((esApoderado && "PFUN".equalsIgnoreCase(estadoTransferencia)) || !esApoderado) {
				render = false;
			}
		} catch (Exception e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[renderApoderado]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_FINEX][Exception] mensaje de error " + e.getMessage(), e);
			}
		}
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger()
					.debug("[renderApoderado][BCI_FINOK][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}
		return render;
	}

	/**
	 * Crea objeto de presentaci�n transferencia a partir de To de servicio para
	 * transferencias realizadas.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 16/10/2013, Mauricio Flores S�enz (Imagemaker IT): versi�n
	 * inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param input
	 *            Entidad de transferencia.
	 * @return TransferenciaTO Entidad de negocio transferencia.
	 * @since 1.0
	 */
	private TransferenciaTO crearTransferenciaTO(Transferencia1a1TO input) {
		TransferenciaTO output = null;

		if (input != null) {
			if (getLogger().isEnabledFor(Level.DEBUG)) {
				getLogger().debug("[crearTransferenciaTO]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_INI] Correlativo >>" + input.getCorrelativoInterno());
			}
			Date fechaOperacion = null;
			String pattern = TablaValores.getValor(PYME_TRANSFER_TABLE, "pattern", "date");

			SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
			try {

				fechaOperacion = dateFormat.parse(input.getFechaOperacion());
			} catch (ParseException e) {
				if (getLogger().isEnabledFor(Level.ERROR)) {
					getLogger().error("[crearTransferenciaTO]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
							+ " [BCI_FINEX][ParseException] mensaje de error " + e.getMessage(), e);
				}
			}
			String mail = null;
			CodigoValor paisCuenta = new CodigoValor(input.getPaisCuenta(), input.getNombrePais());
			CodigoValor monedaTransferencia = new CodigoValor(input.getMonedaTransferencia(),
					input.getMonedaTransferencia());
			String nombreDestinatario = input.getNombreBeneficiario();
			String direccion = null;
			output = new TransferenciaTO();
			output.setBeneficiario(this.crearBeneficiario(direccion, mail, nombreDestinatario, paisCuenta));
			output.setCuentaOrigen(this.crearCuentaMonexTO(monedaTransferencia, null, null, null));
			output.setFechaOperacion(fechaOperacion);
			output.setMontoTransferir(input.getMontoTransferencia() == null ? new Double(0)
					: input.getMontoTransferencia().doubleValue());
			output.setEstado(input.getDescripcionEstado() == null ? "" : input.getDescripcionEstado());
			output.setCorrelativoInterno(input.getCorrelativoInterno());
		}
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[crearTransferenciaTO]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
					+ " [BCI_FINOK] Correlativo >>" + input.getCorrelativoInterno());
		}
		return output;
	}

	/**
	 * Crea objeto de presentaci�n transferencia a partir de To de servicio para
	 * transferencias pendientes.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 25/03/2019, Sergio Bustos B. (Everis) - Minheli Mejias (Ing.
	 * Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param input
	 *            Entidad de transferencia.
	 * @param esApoderado
	 *            flag si es apoderado.
	 * @return TransferenciaTO
	 * @since 1.0
	 */
	private TransferenciaTO crearTransferenciaTO(Transferencia1a1TO input, boolean esApoderado) {

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug(
					"[crearTransferenciaTO][BCI_INI][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}
		TransferenciaTO output = null;
		if (input != null) {
			Date fechaOperacion = null;
			String pattern = TablaValores.getValor(PYME_TRANSFER_RIPPLE, "patternR", "date");
			SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
			try {
				fechaOperacion = dateFormat.parse(input.getFechaOperacion());
			} catch (ParseException e) {
				if (getLogger().isEnabledFor(Level.ERROR)) {
					getLogger().error("[crearTransferenciaTO]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
							+ " [BCI_FINEX][ParseException] mensaje de error " + e.getMessage(), e);
				}
			}
			String mail = null;
			CodigoValor paisCuenta = new CodigoValor(input.getPaisCuenta(), input.getNombrePais());
			CodigoValor monedaTransferencia = new CodigoValor(input.getMonedaTransferencia(),
					input.getMonedaTransferencia());
			String nombreDestinatario = input.getNombreBeneficiario();
			String direccion = null;
			output = new TransferenciaTO();
			output.setBeneficiario(this.crearBeneficiario(direccion, mail, nombreDestinatario, paisCuenta));
			output.setCuentaOrigen(this.crearCuentaMonexTO(monedaTransferencia, null, null, null));
			output.setFechaOperacion(fechaOperacion);
			output.setMontoTransferir(input.getMontoTransferencia() == null ? new Double(0)
					: input.getMontoTransferencia().doubleValue());
			output.setEstado(input.getDescripcionEstado() == null ? "" : input.getDescripcionEstado());
			output.setCorrelativoInterno(input.getCorrelativoInterno());
			output.setCodigoEstado(input.getEstadoTransferencia());
			output.setDisabledButton(isDisableButton(input.getEstadoTransferencia(), esApoderado));
			output.setRenderAutorizar(this.renderApoderado(input.getEstadoTransferencia(), esApoderado));
			output.setIndicadorResultado(input.getIndicadorResultado());
			output.setMensajeResultado(input.getMensajeResultado());
			output.setNumeroOperacion(input.getNumeroOperacion());
			output.setCodigoMonedaITF(input.getCodigoMonedaITF());
			output.setCuentaOrigenFondos(input.getCuentaOrigenFondos());
			output.setCodigoIdentificadorBene(input.getCodigoIdentificadorBene());
			output.setCuentaCorrienteBene(input.getCuentaCorrienteBene());
			output.setSysTimeStamp(input.getSysTimeStamp());

		}
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug(
					"[crearTransferenciaTO][BCI_FINOK][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}
		return output;
	}

	/**
	 * Realiza el lookup del EJB TransferenciaDeFondosFacade.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 28-10-2013, Angel Cris�stomo (Imagemaker IT): versi�n inicial
	 * </li>
	 * </ul>
	 * </p>
	 * 
	 * @throws TransferenciaDeFondosException
	 *             Excepci�n de negocio.
	 * @since 1.0
	 */
	private void createEjb() throws TransferenciaDeFondosException {
		TransferenciaDeFondosFacadeHome serTTFFMonexHome = null;
		try {
			serTTFFMonexHome = (TransferenciaDeFondosFacadeHome) EnhancedServiceLocator.getInstance()
					.getHome(JNDI_TRANSFERENCIA, TransferenciaDeFondosFacadeHome.class);
			servicioTransferencia = serTTFFMonexHome.create();
		} catch (EnhancedServiceLocatorException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[createEjb]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_FINEX][TransferenciaDeFondosBusinessDelegate] mensaje de error "
						+ ErroresUtil.extraeStackTrace(e), e);
			}
			throw new TransferenciaDeFondosException("TFMX-0101");
		} catch (RemoteException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error(
						"[createEjb]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
								+ " [BCI_FINEX][RemoteException] mensaje de error " + ErroresUtil.extraeStackTrace(e),
						e);
			}
			throw new TransferenciaDeFondosException("TFMX-0100");
		} catch (CreateException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error(
						"[createEjb]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
								+ " [BCI_FINEX][CreateException] mensaje de error " + ErroresUtil.extraeStackTrace(e),
						e);
			}
			throw new TransferenciaDeFondosException("TFMX-0101");
		}
	}

	/**
	 * Crea instancia el EJB de Servicios COMEX.
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 06/02/2014 Angel Crisostomo (ImageMaker): Versi�n inicial.</li>
	 * </ul>
	 * <p>
	 * 
	 * @throws ServicioNoDisponibleException
	 *             en caso de que servicio no este disponible
	 * @since 1.0
	 */
	private void crearEJBServiciosComex() throws ServicioNoDisponibleException {
		try {
			if (getLogger().isEnabledFor(Level.DEBUG)) {
				getLogger().debug("[BCI_INI] [crearEJB]");
				getLogger().debug("[BCI_INI] [crearEJB] Obteniendo referencia del EJB" + JNDI_NAME);
			}

			this.serviciosComex = ((ServiciosComexHome) EnhancedServiceLocator.getInstance().getHome(JNDI_NAME,
					ServiciosComexHome.class)).create();
			if (getLogger().isEnabledFor(Level.DEBUG)) {
				getLogger().debug("[BCI_INI] [crearEJB]");
				getLogger().debug("[BCI_INI] [crearEJB] Referencia obtenida");
			}
		} catch (EnhancedServiceLocatorException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error(ErroresUtil.extraeStackTrace(e));
			}
			throw new ServicioNoDisponibleException("ESPECIAL", e);
		} catch (CreateException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error(ErroresUtil.extraeStackTrace(e));
			}
			throw new ServicioNoDisponibleException("ESPECIAL", e);
		} catch (RemoteException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error(ErroresUtil.extraeStackTrace(e));
			}
			throw new ServicioNoDisponibleException("ESPECIAL", e);
		}
	}

	/**
	 * Retorna codigo retornado en l�gica autorizaci�n firma.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 14-10-2013, Angel Cris�stomo (Imagemaker IT): versi�n inicial
	 * </li>
	 * </ul>
	 * </p>
	 * 
	 * @param autPagoCtaCte
	 *            Entidad de autorizaci�n de cuenta corriente.
	 * @throws NegocioException
	 *             Excepci�n espec�fica de negocio.
	 * @return String
	 * @since 1.0
	 */
	private boolean getProcesaResultadoAutorizacion(AutorizaPagoCtaCte[] autPagoCtaCte) throws NegocioException {
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[getProcesaResultadoAutorizacion][BCI_INI]["
					+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}
		boolean autorizacion = false;
		String result = "";
		for (int i = 0; i < autPagoCtaCte.length; i++) {
			if (autPagoCtaCte[i] == null) {
				break;
			}
			result = autPagoCtaCte[i].getCodigo();
		}
		if ("0".equals(result)) {
			if (getLogger().isEnabledFor(Level.DEBUG)) {
				getLogger().debug("[getProcesaResultadoAutorizacion]"
						+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + " Autorizacion Pendiente");
			}
			autorizacion = false;
		} else if ("1".equals(result)) {
			if (getLogger().isEnabledFor(Level.DEBUG)) {
				getLogger().debug("[getProcesaResultadoAutorizacion]"
						+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + " Autorizacion Firmada");
			}
			autorizacion = true;
		} else if ("101".equals(result)) {
			throw new NegocioException("FIRMA_REPETIDA");
		} else if ("102".equals(result)) {
			throw new NegocioException("ERROR_SISTEMA");
		} else if ("100".equals(result)) {
			throw new NegocioException("USUARIO_NO_AUTORIZADO");
		}

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[getProcesaResultadoAutorizacion][BCI_FINOK]["
					+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}

		return autorizacion;
	}

	/**
	 * Obtiene servicio ServiciosMonex.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 18/10/2013, Mauricio Flores S�enz (Imagemaker IT): versi�n
	 * inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @return ServiciosMonex
	 * @throws GeneralException
	 *             Excepci�n general de negocio.
	 * @since 1.0
	 */
	private ServiciosMonex getServicioMonex() throws GeneralException {

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger()
					.debug("[getServicioMonex][BCI_INI][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}

		ServiciosMonex service = null;
		try {
			ServiciosMonexHome serviciosMonexHome = (ServiciosMonexHome) EnhancedServiceLocator.getInstance()
					.getHome("wcorp.serv.monex.ServiciosMonex", ServiciosMonexHome.class);
			service = serviciosMonexHome.create();
		} catch (EnhancedServiceLocatorException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error(
						"[getServicioMonex]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
								+ " [BCI_FINEX][EnhancedServiceLocatorException] mensaje de error " + e.getMessage(),
						e);
			}
			throw new GeneralException("ESPECIAL", "[ServiciosMonex] >> " + e.getMessage());
		} catch (RemoteException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[getServicioMonex]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_FINEX][RemoteException] mensaje de error " + e.getMessage(), e);
			}
			throw new GeneralException("ESPECIAL", "[ServiciosMonex] >> " + e.getMessage());
		} catch (CreateException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[getServicioMonex]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_FINEX][CreateException] mensaje de error " + e.getMessage(), e);
			}
			throw new GeneralException("ESPECIAL", "[ServiciosMonex] >> " + e.getMessage());
		}
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger()
					.debug("[getServicioMonex][BCI_FINOK][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}
		return service;
	}

	/**
	 * Obtiene servicio BCIExpress.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 18/10/2013, Angel Cris�stomo (Imagemaker IT): versi�n inicial
	 * </li>
	 * </ul>
	 * </p>
	 * 
	 * @return BCIExpress
	 * @throws GeneralException
	 *             Excepci�n general de negocio.
	 * @since 1.0
	 */
	private BCIExpress getBciExpress() throws GeneralException {
		BCIExpress serviciosBCIExpress;
		try {
			if (getLogger().isEnabledFor(Level.DEBUG)) {
				getLogger().debug("[getBciExpress][BCI_INI]Obteniendo referencia del EJB " + JNDI_NAME_BCI_EXPRESS);
			}
			serviciosBCIExpress = ((BCIExpressHome) EnhancedServiceLocator.getInstance().getHome(JNDI_NAME_BCI_EXPRESS,
					BCIExpressHome.class)).create();
			if (getLogger().isEnabledFor(Level.DEBUG)) {
				getLogger().debug("[getBciExpress][BCI_INI]Referencia obtenida a " + JNDI_NAME_BCI_EXPRESS);
			}
		} catch (EnhancedServiceLocatorException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error(
						"[getBciExpress]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
								+ " [BCI_FINEX][EnhancedServiceLocatorException] mensaje de error " + e.getMessage(),
						e);
			}
			throw new GeneralException("ESPECIAL", "Error al crear Servicio BCI Express");
		} catch (CreateException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[getBciExpress]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_FINEX][CreateException] mensaje de error " + e.getMessage(), e);
			}
			throw new GeneralException("ESPECIAL", "Error al crear Servicio BCI Express");
		} catch (RemoteException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[getBciExpress]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_FINEX][RemoteException] mensaje de error " + e.getMessage(), e);
			}
			throw new GeneralException("ESPECIAL", "Error al crear Servicio BCI Express");
		}

		return serviciosBCIExpress;
	}

	/**
	 * Obtiene cuentas de moneda extranjera.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 25/02/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing.
	 * Soft. BCI): version inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param aRut
	 *            RUT de empresa.
	 * @param aDv
	 *            DV de empresa.
	 * @param moneda
	 *            Moneda
	 * @return CuentaTO[]
	 * @throws ServicioNoDisponibleException
	 *             Excepci�n de negocio especifica.
	 * @throws GeneralException
	 *             Excepci�n de negocio general.
	 * @since 1.0
	 */
	public CuentaTO[] obtenerCuentasMonedaExtranjera(long aRut, char aDv, String moneda)
			throws ServicioNoDisponibleException, GeneralException {

		final String nombreMetodo = "obtenerCuentasMonedaExtranjera";
		final String identificador = (usuarioClienteModelMB != null
				&& usuarioClienteModelMB.getUsuarioModelMB() != null)
						? String.valueOf(usuarioClienteModelMB.getUsuarioModelMB().getRut()) : "";

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "][" + identificador + "] [BCI_INI]");
			getLogger().info("[" + nombreMetodo + "][" + identificador + "] [Parametro][aRut]["
					+ StringUtil.contenidoDe(aRut) + "]");
			getLogger().info("[" + nombreMetodo + "][" + identificador + "] [Parametro][aDv]["
					+ StringUtil.contenidoDe(aDv) + "]");
			getLogger().info("[" + nombreMetodo + "][" + identificador + "] [Parametro][moneda]["
					+ StringUtil.contenidoDe(moneda) + "]");
		}

		CuentaTO[] cuentas;
		CuentaCorrienteDelegate monexDelegate;
		SalidaCuentaCorrienteTO[] salida;
		monexDelegate = new CuentaCorrienteDelegate();
		salida = monexDelegate.obtenerCuentasCorrientesSaldo(aRut, aDv);

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug(
					"[" + nombreMetodo + "][" + identificador + "][salida][" + StringUtil.contenidoDe(salida) + "]");
		}

		int contMonedaCoincidentes = 0;
		for (int i = 0; i < salida.length; i++) {
			if (salida[i].getTipoMoneda().equals(moneda)) {
				contMonedaCoincidentes++;
			}
		}
		cuentas = new CuentaTO[contMonedaCoincidentes];
		int contCuentasValidas = 0;
		final int numeroDecimales = 2;
		for (int c = 0; c < salida.length; c++) {
			if (!salida[c].getTipoMoneda().equals(moneda)) {
				continue;
			}
			CuentaTO cuenta = new CuentaTO();
			MonedaTO monedaCuenta = new MonedaTO();
			cuenta.setId(String.valueOf(salida[c].getNumeroCuenta()));

			monedaCuenta.setCodigoISO(salida[c].getTipoMoneda());
			cuenta.setMoneda(monedaCuenta);
			cuenta.setTipo("MNX");
			cuenta.setRut(aRut);
			BigDecimal saldo = new BigDecimal(salida[c].getSaldos().getSaldoDisp());
			cuenta.setSaldo(saldo.setScale(numeroDecimales, BigDecimal.ROUND_HALF_EVEN));

			BigDecimal saldoDisponible = new BigDecimal(salida[c].getSaldos().getSaldoDisp());
			cuenta.setSaldoDisponible(saldoDisponible.setScale(numeroDecimales, BigDecimal.ROUND_HALF_EVEN));

			cuenta.setCondicionCierre(salida[c].getCondicionCierre());

			cuentas[contCuentasValidas++] = cuenta;
		}

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "][" + identificador + "] [BCI_FINOK]["
					+ StringUtil.contenidoDe(cuentas) + "]");
		}

		return cuentas;
	}

	/**
	 * Obtiene las cuentas en moneda nacional.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 17-10-2013, Angel Cris�stomo (Imagemaker IT): versi�n inicial
	 * </li>
	 * <li>1.0 17-06-2014, Angel Cris�stomo (Imagemaker IT): se reemplaza por
	 * vacio el caracter $ que viene con el saldo contable</li>
	 * <li>2.0 13/10/2015 Rafael Pizarro (TINet) - Robinson Hidalgo (Ing. Soft.
	 * BCI): Se modifica el m�todo con el fin de establecer el saldo contable y
	 * el saldo disponible de las cuentas obtenidas. Aparte de esto se modifica
	 * el modificador de acceso de privado a p�blico.</li>
	 * <li>2.1 29/03/2017, Marcos Abraham Hernandez Bravo (ImageMaker IT) -
	 * Miguel Angel Anza Cruz: Se normaliza y a�ade log.</li>
	 * <li>2.2 15/05/2017 Victor Morales (Imagemaker IT) - Miguel Anza (Ing.
	 * Soft. BCI): Se controla excepcion NullPointer cuando no existen cuentas
	 * moneda nacional y en caso de ocurrir se devuelve una lista vacia.</li>
	 * </ul>
	 * </p>
	 * 
	 * @param aRut
	 *            RUT de empresa.
	 * @return CuentaTO[]
	 * @throws GeneralException
	 *             Excepci�n Gneral.
	 * @since 1.0
	 */
	public CuentaTO[] obtenerCuentasMonedaNacional(long aRut) throws GeneralException {

		final String nombreMetodo = "obtenerCuentasMonedaNacional";
		final String identificador = (usuarioClienteModelMB != null
				&& usuarioClienteModelMB.getUsuarioModelMB() != null)
						? String.valueOf(usuarioClienteModelMB.getUsuarioModelMB().getRut()) : "";

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "][" + identificador + "] [BCI_INI]");
			getLogger().info("[" + nombreMetodo + "][" + identificador + "] [Parametro][aRut]["
					+ StringUtil.contenidoDe(aRut) + "]");
		}

		SessionBCI sesionBci = ConectorStruts.getSessionBCI();
		CuentaTO[] cuentas = new CuentaTO[] {};
		ResultConsultaListaCuentasCorrientesAsociadas resultCuentasAsociadas = (ResultConsultaListaCuentasCorrientesAsociadas) sesionBci
				.getAttrib("ResultConsultaListaCuentasCorrientesAsociadas");
		ResultObtieneCtasCtes resultTipoCuentas = (ResultObtieneCtasCtes) sesionBci.getAttrib("ResultObtieneCtasCtes");

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[" + nombreMetodo + "][" + identificador + "] [resultCuentasAsociadas] ["
					+ StringUtil.contenidoDe(resultCuentasAsociadas) + "]");
			getLogger().debug("[" + nombreMetodo + "][" + identificador + "] [resultTipoCuentas] ["
					+ StringUtil.contenidoDe(resultTipoCuentas) + "]");
		}

		ObtenCtasCtes[] tipoCuentas = resultTipoCuentas.getObtenCtasCtes();
		ConsultaListaCtasCtesAsociadas[] cuentasAsociadas = null;

		try {

			cuentasAsociadas = resultCuentasAsociadas.getConsultaListaCtasCtesAsociadas();

		} catch (Exception e) {
			if (getLogger().isEnabledFor(Level.WARN)) {
				getLogger().warn("[obtenerCuentasMonedaNacional][BCI_FINEX][Error al obtener cuentas moneda nacional: "
						+ e.getMessage() + ". Se retorna lista vacia]");

			}
			return cuentas;
		}

		Map<String, ConsultaListaCtasCtesAsociadas> listaCuentas = new HashMap<String, ConsultaListaCtasCtesAsociadas>();

		for (int i = 0; i < tipoCuentas.length; i++) {
			for (int j = 0; j < cuentasAsociadas.length; j++) {
				String cuenta = cuentasAsociadas[j].getCuenta();
				Integer iCuenta = new Integer(tipoCuentas[i].getNumCuentaCorriente());
				String cuentaSinPrefijo = iCuenta.toString();
				if (cuenta.indexOf(cuentaSinPrefijo) >= 0) {
					if ("CCT".equals(tipoCuentas[i].getTipo().trim())) {
						listaCuentas.put(cuentaSinPrefijo, cuentasAsociadas[j]);
					}
				}
			}
		}
		List<CuentaTO> listaCuentasTo = new ArrayList<CuentaTO>();
		final int numeroDecimales = 2;
		for (Iterator<String> it = listaCuentas.keySet().iterator(); it.hasNext();) {
			String nroCuenta = it.next();
			ConsultaListaCtasCtesAsociadas cuentasAsociada = listaCuentas.get(nroCuenta);
			CuentaTO paso = new CuentaTO();
			paso.setId(nroCuenta);
			MonedaTO mon = new MonedaTO();
			mon.setCodigoISO(MONEDA_NACIONAL);
			paso.setMoneda(mon);
			paso.setRut(aRut);
			paso.setTipo(MONEDA_NACIONAL);
			String saldoContable = cuentasAsociada.getSaldoContable();
			saldoContable = StringUtil.reemplazaCaracteres(saldoContable, "$", "");
			saldoContable = StringUtil.reemplazaCaracteres(saldoContable, ",", ".");
			saldoContable = saldoContable.trim();
			int n = 0;
			n = saldoContable.length();
			String isCredito = saldoContable.substring(n - numeroDecimales, n);
			String monto = saldoContable.substring(0, n - numeroDecimales);
			try {
				if ("CR".equals(isCredito)) {
					paso.setMontoNegativo("SI");
					paso.setSaldo(new BigDecimal(monto));
					paso.setMontoNegativo("SI");
				} else {
					paso.setSaldo(new BigDecimal(saldoContable));
				}
			} catch (Exception e) {
				if (getLogger().isEnabledFor(Level.ERROR)) {
					getLogger().error("[" + nombreMetodo + "][" + identificador + "] [BCI_FINEX][Exception]["
							+ e.getMessage() + "]", e);
				}
				throw new GeneralException("CVD-023");
			}
			paso.setSaldoDisponible(new BigDecimal(obtenerMontoCuentaTandem(cuentasAsociada.getSaldoDisponible())));
			paso.setSaldoContable(new BigDecimal(obtenerMontoCuentaTandem(cuentasAsociada.getSaldoContable())));
			listaCuentasTo.add(paso);
		}

		cuentas = new CuentaTO[listaCuentas.size()];
		listaCuentasTo.toArray(cuentas);
		listaCuentas.clear();

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "][" + identificador + "] [BCI_FINOK]["
					+ StringUtil.contenidoDe(cuentas) + "]");
		}

		return cuentas;
	}

	/**
	 * Obtener cuentas de operaci�n.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 25/02/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing.
	 * Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param aRut
	 *            RUT de la empresa.
	 * @param aDv
	 *            DV de la empresa.
	 * @param moneda
	 *            Moneda de transferencia.
	 * @return CuentaTO[]
	 * @throws GeneralException
	 *             Excepci�n de negocio.
	 * @since 1.0
	 */
	private CuentaTO[] obtenerCuentasOperacionDeCambio(long aRut, char aDv, String moneda) throws GeneralException {
		// ESTE METODO NO DEBE DE USARSE, YA QUE SIEMPRE SERAN MONEDAS
		// EXTRANJERAS
		if (MONEDA_NACIONAL.equals(moneda)) {
			return obtenerCuentasMonedaNacional(aRut);
		} else {
			return obtenerCuentasMonedaExtranjera(aRut, aDv, moneda);
		}
	}

	/**
	 * <p>
	 * M�todo get del logger.
	 * </p>
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 28-10-2013, Angel Cris�stomo (Imagemaker IT): versi�n inicial
	 * </li>
	 * </ul>
	 * </p>
	 * 
	 * @return Logger
	 * @since 1.0
	 */
	private Logger getLogger() {
		if (log == null) {
			log = Logger.getLogger(this.getClass());
		}
		return log;
	}

	/**
	 * Retorna monedas para el ingreso de un beneficiario.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 16-12-2013, Angel Cris�stomo L�pez (Imagemaker IT): versi�n
	 * inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @return List<wcorp.aplicaciones.productos.servicios.transferencias.
	 *         monedaextranjera.to.MonedaTO>
	 * @throws GeneralException
	 *             Excepci�n de negocio.
	 * @since 1.0
	 */
	public List<wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.MonedaTO> listarMonedas()
			throws GeneralException {
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[listarMonedas][BCI_INI][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}
		List<wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.MonedaTO> listadoMonedas = null;
		try {
			wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.MonedaTO[] array = this.servicioTransferencia
					.listarMonedas();
			if (array != null) {
				listadoMonedas = Arrays.asList(array);
			}
		} catch (TransferenciaDeFondosException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[listarMonedas]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_FINEX][TransferenciaDeFondosException] mensaje de error " + e.getCodigo(), e);
			}
			throw new GeneralException(e.getCodigo(), e.getFullMessage());
		} catch (RemoteException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[listarMonedas]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_FINEX][RemoteException] mensaje de error " + e.getMessage(), e);
			}
			throw new GeneralException(e.getMessage(), e.getLocalizedMessage());
		}
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[listarMonedas][BCI_FINOK][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}
		return listadoMonedas;
	}

	/**
	 * Retorna lista de paises para el ingreso de un beneficiario.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 16-12-2013, Angel Cris�stomo L�pez (Imagemaker IT): versi�n
	 * inicial</li>
	 * <li>1.1 17-06-2014, Angel Cris�stomo L�pez (Imagemaker IT): Se agrega
	 * funcionalidad ordenar lista de paises</li>
	 * </ul>
	 * </p>
	 * 
	 * @return List<PaisTO>
	 * @throws GeneralException
	 *             Excepci�n de negocio.
	 * @since 1.0
	 */
	public List<PaisTO> listarPaises() throws GeneralException {
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[listarPaises][BCI_INI][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}
		List<PaisTO> paises = null;
		try {
			PaisTO[] array = this.servicioTransferencia.listarPaises();
			if (array != null) {
				paises = Arrays.asList(array);
				Collections.sort(paises, new ComparatorPaisTO());
			}
		} catch (TransferenciaDeFondosException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[listarPaises]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_FINEX][TransferenciaDeFondosException] mensaje de error " + e.getCodigo(), e);
			}
			throw new GeneralException(e.getCodigo(), e.getFullMessage());
		} catch (RemoteException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[listarPaises]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_FINEX][RemoteException] mensaje de error " + e.getMessage(), e);
			}
			throw new GeneralException(e.getMessage(), e.getLocalizedMessage());
		}
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[listarPaises][BCI_FINOK][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}
		return paises;
	}

	/**
	 * Retorna lista de paises de residencia para el ingreso de un beneficiario.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 13/11/2015 Oscar Nahuelpan (SEnTRA) - Heraldo Hernandez (Ing.
	 * Soft. BCI): version inicial.</li>
	 * </ul>
	 * </p>
	 * 
	 * @return List<PaisTO> Con el listado de paises.
	 * @throws GeneralException
	 *             Excepci�n de negocio.
	 * @since 1.5
	 */
	public List<PaisTO> listarPaisesResidencia() throws GeneralException {
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug(
					"[listarPaisesResidencia][BCI_INI][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}
		List<PaisTO> paises = null;
		try {
			PaisTO[] array = this.servicioTransferencia.listarPaisesResidencia();
			if (array != null) {
				paises = Arrays.asList(array);
				Collections.sort(paises, new ComparatorPaisTO());
			}
		} catch (TransferenciaDeFondosException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[listarPaisesResidencia]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_FINEX][TransferenciaDeFondosException] mensaje de error " + e.getCodigo(), e);
			}
			throw new GeneralException(e.getCodigo(), e.getFullMessage());
		} catch (RemoteException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[listarPaisesResidencia]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_FINEX][RemoteException] mensaje de error " + e.getMessage(), e);
			}
			throw new GeneralException(e.getMessage(), e.getLocalizedMessage());
		}
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug(
					"[listarPaisesResidencia][BCI_FINOK][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}
		return paises;
	}

	/**
	 * Crea un nuevo beneficiario.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 26-12-2013, Angel Cris�stomo L�pez (Imagemaker IT): versi�n
	 * inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param beneficiarioIn
	 *            benefeciario a ingresar.
	 * @return BeneficiarioTO beneficiario ingresado.
	 * @throws GeneralException
	 *             Excepci�n de negocio.
	 * @throws TtffException
	 *             Excepci�n de servicio Transfer Monex.
	 * @since 1.0
	 */
	public wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.BeneficiarioTO crearBeneficiarioPendiente(
			wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.BeneficiarioTO beneficiarioIn)
			throws TtffException, GeneralException {

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[crearBeneficiarioPendiente][BCI_INI]["
					+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}

		wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.BeneficiarioTO beneficiario = null;
		try {
			beneficiario = this.servicioTransferencia.crearBeneficiarioPendiente(beneficiarioIn);
			if (getLogger().isEnabledFor(Level.INFO)) {
				getLogger().info("[crearBeneficiarioPendiente][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ "] codigo respuesta creacion beneficiario pendiente : "
						+ beneficiario.getIndicadorResultadoOperacion());
			}
		} catch (TtffException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[crearBeneficiarioPendiente]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_FINEX][TtffException] mensaje de error " + e.getMessage(), e);
			}
			throw e;
		} catch (RemoteException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[crearBeneficiarioPendiente]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_FINEX][RemoteException] mensaje de error " + e.getMessage(), e);
			}
			throw new GeneralException(e.getMessage(), e.getMessage());
		}
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug(
					"[crearBeneficiarioPendiente][BCI_FINOK][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
							+ "] beneficiario >> " + beneficiario.toString());
		}

		return beneficiario;
	}

	/**
	 * Crea un nuevo beneficiario.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 26-12-2013, Angel Cris�stomo L�pez (Imagemaker IT): versi�n
	 * inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param beneficiarioIn
	 *            benefeciario a ingresar.
	 * @return BeneficiarioTO beneficiario ingresado.
	 * @throws GeneralException
	 *             Excepci�n de negocio.
	 * @since 1.0
	 */
	public wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.BeneficiarioTO actualizarBeneficiarioPendiente(
			wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.BeneficiarioTO beneficiarioIn)
			throws GeneralException {

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[actualizarBeneficiarioPendiente][BCI_INI]["
					+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}

		wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.BeneficiarioTO beneficiario = null;
		try {
			beneficiario = this.servicioTransferencia.actualizarBeneficiarioPendiente(beneficiarioIn);
			if (getLogger().isEnabledFor(Level.INFO)) {
				getLogger()
						.info("[actualizarBeneficiarioPendiente][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
								+ "] codigo respuesta actualizacion beneficiario pendiente : "
								+ beneficiario.getIndicadorResultadoOperacion());
			}
		} catch (TtffException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger()
						.error("[actualizarBeneficiarioPendiente]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
								+ " [BCI_FINEX][TtffException] mensaje de error " + e.getMessage(), e);
			}
			throw new GeneralException(e.getCodigo(), e.getInfoAdic());
		} catch (RemoteException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger()
						.error("[actualizarBeneficiarioPendiente]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
								+ " [BCI_FINEX][RemoteException] mensaje de error " + e.getMessage(), e);
			}
			throw new GeneralException(e.getMessage(), e.getMessage());
		}
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[actualizarBeneficiarioPendiente][BCI_FINOK]["
					+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}
		return beneficiario;
	}

	/**
	 * Buscar beneficiario.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 10-01-2014, Angel Cris�stomo L�pez (Imagemaker IT): versi�n
	 * inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param datosBeneficiarioTO
	 *            Entidad de negocio.
	 * @return BeneficiarioTO
	 * @throws GeneralException
	 *             Excepcion de negocio.
	 * @since 1.0
	 */
	public wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.BeneficiarioTO buscarBeneficario(
			DatosBeneficiarioTO datosBeneficiarioTO) throws GeneralException {

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger()
					.debug("[buscarBeneficario][BCI_INI][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}

		wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.BeneficiarioTO beneficiario = null;
		try {
			beneficiario = this.servicioTransferencia.consultarDatosBeneficiario(datosBeneficiarioTO);
		} catch (TtffException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[buscarBeneficario]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_FINEX][TtffException] mensaje de error " + e.getMessage(), e);
			}
			throw new GeneralException(e.getCodigo(), e.getInfoAdic());
		} catch (RemoteException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[buscarBeneficario]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_FINEX][RemoteException] mensaje de error " + e.getMessage(), e);
			}
			throw new GeneralException(e.getMessage(), e.getMessage());
		}

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug(
					"[buscarBeneficario][BCI_FINOK][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}

		return beneficiario;
	}

	/**
	 * Autoriza un beneficiario.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 10-01-2014, Angel Cris�stomo L�pez (Imagemaker IT): versi�n
	 * inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param datosBeneficiarioTO
	 *            Entidad de negocio.
	 * @return DatosBeneficiarioTO
	 * @throws GeneralException
	 *             Excepci�n de negocio.
	 * @throws TtffException
	 *             Excepci�n de negocio.
	 * @since 1.0
	 */
	public DatosBeneficiarioTO autorizarBeneficiario(DatosBeneficiarioTO datosBeneficiarioTO)
			throws GeneralException, TtffException {
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[autorizarBeneficiario][BCI_INI][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
					+ "] datosBeneficiacio = " + datosBeneficiarioTO.toString());
		}
		DatosBeneficiarioTO datosRetorno = null;
		try {
			datosRetorno = this.servicioTransferencia.autorizarBeneficiario(datosBeneficiarioTO);
			if (getLogger().isEnabledFor(Level.INFO)) {
				getLogger().info("[autorizarBeneficiario][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ "] codigo respuesta autorizar beneficiario : "
						+ datosRetorno.getIndicadorResultadoOperacion());
			}
		} catch (TtffException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[autorizarBeneficiario]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_FINEX][TtffException] mensaje de error " + e.getMessage(), e);
			}
			throw e;
		} catch (RemoteException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[autorizarBeneficiario]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_FINEX][RemoteException] mensaje de error " + e.getMessage(), e);
			}
			throw new GeneralException(e.getMessage(), e.getMessage());
		}
		return datosRetorno;
	}

	/**
	 * Elimina un beneficiario.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 10-01-2014, Angel Cris�stomo L�pez (Imagemaker IT): versi�n
	 * inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param datosBeneficiarioTO
	 *            Entidad de negocio.
	 * @return DatosBeneficiarioTO
	 * @throws GeneralException
	 *             Excepci�n de negocio.
	 * @since 1.0
	 */
	public DatosBeneficiarioTO eliminarBeneficiarioPendiente(DatosBeneficiarioTO datosBeneficiarioTO)
			throws GeneralException {
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[eliminarBeneficiarioPendiente][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
					+ "] datosBeneficiarioTO >> " + datosBeneficiarioTO == null ? "Null"
							: datosBeneficiarioTO.toString());
		}
		DatosBeneficiarioTO datosRetorno = null;
		try {
			datosRetorno = this.servicioTransferencia.eliminarBeneficiarioPendiente(datosBeneficiarioTO);
			if (getLogger().isEnabledFor(Level.INFO)) {
				getLogger().info("[eliminarBeneficiarioPendiente][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ "] codigo respuesta eliminar beneficiario pendiente : "
						+ datosRetorno.getIndicadorResultadoOperacion());
			}
		} catch (TtffException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[eliminarBeneficiarioPendiente]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_FINEX][TtffException] mensaje de error " + e.getMessage(), e);
			}
			throw new GeneralException(e.getCodigo(), e.getInfoAdic());
		} catch (RemoteException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[eliminarBeneficiarioPendiente]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_FINEX][RemoteException] mensaje de error " + e.getMessage(), e);
			}
			throw new GeneralException(e.getMessage(), e.getMessage());
		}
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[eliminarBeneficiarioPendiente][BCI_FINOK]["
					+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}
		return datosRetorno;
	}

	/**
	 * Elimina un beneficiario inscrito.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 11-02-2014, Mauricio Flores S�enz (Imagemaker IT): versi�n
	 * inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param datosBeneficiarioTO
	 *            Objeto de transporte de eliminaci�n.
	 * @throws GeneralException
	 *             Excepcion de negocio.
	 * @since 1.0
	 */
	public void eliminarBeneficiario(DatosBeneficiarioTO datosBeneficiarioTO) throws GeneralException {
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[eliminarBeneficiario][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
					+ "] datosBeneficiarioTO >> " + datosBeneficiarioTO == null ? "Null"
							: datosBeneficiarioTO.toString());
		}
		try {
			this.servicioTransferencia.eliminarBeneficiario(datosBeneficiarioTO);
			if (getLogger().isEnabledFor(Level.INFO)) {
				getLogger().info("[eliminarBeneficiario][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ "] Eliminacion beneficiario inscristo Exitosa [" + datosBeneficiarioTO.getIdBeneficiario()
						+ "]");
			}
		} catch (RemoteException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[eliminarBeneficiario]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_FINEX][RemoteException] mensaje de error " + e.getMessage(), e);
			}
			throw new GeneralException(e.getMessage(), e.getMessage());
		}
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug(
					"[eliminarBeneficiario][BCI_FINOK][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}
	}

	/**
	 * Consulta la lista de beneficiarios pendientes.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 30-01-2014, Mauricio Flores S�enz (Imagemaker IT): versi�n
	 * inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param rut
	 *            RUT de empresa.
	 * @param dvEmpresa
	 *            DV empresa.
	 * @return wcorp.aplicaciones.productos.servicios.transferencias.
	 *         monedaextranjera.to.BeneficiarioTO
	 * @throws GeneralException
	 *             Excepcion General.
	 * @since 1.0
	 */
	public List<wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.BeneficiarioTO> listarBeneficiariosPendientes(
			long rut, char dvEmpresa) throws GeneralException {

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[listarBeneficiariosPendientes][BCI_INI]["
					+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}

		wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.BeneficiarioTO[] beneficiarios = null;
		wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.BeneficiarioTO beneficiario = new wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.BeneficiarioTO();
		beneficiario.setRutEmpresa(rut);
		beneficiario.setDvEmpresa(dvEmpresa);
		try {
			beneficiarios = this.servicioTransferencia.listarBeneficiariosPendientes(beneficiario);
		} catch (TtffException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[listarBeneficiariosPendientes]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_FINEX][TtffException] mensaje de error " + e.getMessage(), e);
			}
			throw new GeneralException(ESPECIAL, e.getMessage());
		} catch (RemoteException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[listarBeneficiariosPendientes]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_FINEX][RemoteException] mensaje de error " + e.getMessage(), e);
			}
			throw new GeneralException(ESPECIAL, e.getMessage());
		}
		if (beneficiarios == null) {
			throw new GeneralException(ESPECIAL, "No se encontraron registros.");
		}

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[listarBeneficiariosPendientes][BCI_FINOK][" + rut + "]");
		}
		return Arrays.asList(beneficiarios);
	}

	/**
	 * B�squeda de beneficairios inscritos de una empresa.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 30-01-2014, Mauricio Flores S�enz (Imagemaker IT): versi�n
	 * inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param rutEmpresa
	 *            RUT de empresa.
	 * @param dvEmpresa
	 *            Digito Verificador de empresa.
	 * @return List
	 *         <wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.BeneficiarioTO>
	 * @throws GeneralException
	 *             Excepci�n general de negocio.
	 * @since 1.0
	 */
	public List<wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.BeneficiarioTO> listarBeneficiarios(
			long rutEmpresa, char dvEmpresa) throws GeneralException {

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug(
					"[listarBeneficiarios][BCI_INI][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}

		wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.BeneficiarioTO[] beneficiarios = null;
		try {
			beneficiarios = this.servicioTransferencia.listarBeneficiarios(rutEmpresa, dvEmpresa);
		} catch (TransferenciaDeFondosException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[listarBeneficiarios]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_FINEX][TransferenciaDeFondosException] mensaje de error " + e.getMessage(), e);
			}

			throw new GeneralException(ESPECIAL, e.getMessage());
		} catch (RemoteException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[listarBeneficiarios]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_FINEX][RemoteException] mensaje de error " + e.getMessage(), e);
			}
			throw new GeneralException(ESPECIAL, e.getMessage());
		}
		if (beneficiarios == null) {
			throw new GeneralException(ESPECIAL, "No se encontraron registros.");
		}

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug(
					"[listarBeneficiarios][BCI_FINOK][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}
		return Arrays.asList(beneficiarios);
	}

	/**
	 * Retorna beneficiarios pendientes de autorizacion.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 23-12-2013, Angel Cris�stomo L�pez (Imagemaker IT): versi�n
	 * inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param rut
	 *            rut cliente.
	 * @param digitoVerif
	 *            dv cliente.
	 * @return List<BeneficiarioIngresoTO>
	 * @since 1.0
	 */
	public List<BeneficiarioIngresoTO> buscarBeneficiariosPendientes(long rut, char digitoVerif) {
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[buscarBeneficiariosPendientes][BCI_INI]["
					+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}
		List<BeneficiarioIngresoTO> lista = new ArrayList<BeneficiarioIngresoTO>();
		try {
			List<wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.BeneficiarioTO> beneficiarios = this
					.listarBeneficiariosPendientes(rut, digitoVerif);

			for (wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.BeneficiarioTO beneficiario : beneficiarios) {
				lista.add(this.crearBeneficiarioIngreso(beneficiario, BENEFICIARIO_PENDIENTE));
			}
		} catch (GeneralException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[buscarBeneficiariosPendientes]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_FINEX][GeneralException] mensaje de error " + e.getMessage(), e);
			}
		}
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[buscarBeneficiariosPendientes][BCI_FINOK][" + rut + "]");
		}
		return lista;
	}

	/**
	 * Retorna beneficiarios inscritos.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 23-12-2013, Angel Cris�stomo L�pez (Imagemaker IT): versi�n
	 * inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param rut
	 *            rut cliente.
	 * @param digitoVerif
	 *            dv cliente.
	 * @return List<BeneficiarioIngresoTO>
	 * @since 1.0
	 */
	public List<BeneficiarioIngresoTO> buscarBeneficiariosInscritos(long rut, char digitoVerif) {
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[buscarBeneficiariosInscritos][BCI_INI]["
					+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}
		List<BeneficiarioIngresoTO> lista = new ArrayList<BeneficiarioIngresoTO>();
		try {
			List<wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.BeneficiarioTO> beneficiarios = this
					.listarBeneficiarios(rut, digitoVerif);

			for (wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.BeneficiarioTO beneficiario : beneficiarios) {
				lista.add(this.crearBeneficiarioIngreso(beneficiario, BENEFICIARIO_INSCRITO));
			}
		} catch (GeneralException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[buscarBeneficiariosInscritos]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_FINEX][GeneralException] mensaje de error " + e.getMessage(), e);
			}
		}
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[buscarBeneficiariosInscritos][BCI_FINOK]["
					+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}
		return lista;
	}

	/**
	 * obtener detalle de un beneficiario.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 10-01-2014, Angel Cris�stomo L�pez (Imagemaker IT): versi�n
	 * inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param datosBeneficiarioTO
	 *            Entidad de negocio.
	 * @return beneficiario
	 * @throws GeneralException
	 *             Excepci�n de negocio.
	 * @since 1.0
	 */
	public wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.BeneficiarioTO obtenerDetalleBeneficiario(
			DatosBeneficiarioTO datosBeneficiarioTO) throws GeneralException {
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[obtenerDetalleBeneficiario][BCI_INI]["
					+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}
		wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.BeneficiarioTO beneficiario = null;
		try {
			beneficiario = this.servicioTransferencia.buscarBeneficiario(datosBeneficiarioTO);
		} catch (TransferenciaDeFondosException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[obtenerDetalleBeneficiario]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_FINEX][TransferenciaDeFondosException] mensaje de error " + e.getMessage(), e);
			}
			throw new GeneralException(e.getCodigo(), e.getInfoAdic());
		} catch (RemoteException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[obtenerDetalleBeneficiario]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_FINEX][RemoteException] mensaje de error " + e.getMessage(), e);
			}
			throw new GeneralException(e.getMessage(), e.getMessage());
		}
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[obtenerDetalleBeneficiario][BCI_FINOK]["
					+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}
		return beneficiario;
	}

	/**
	 * Crea entidad beneficiario de presentaci�n.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 30-01-2014, Mauricio Flores S�enz (Imagemaker IT): versi�n
	 * inicial</li>
	 * <li>1.1 05/11/2015 Oscar Nahuelpan (SEnTRA) - Heraldo Hernandez (Ing.
	 * Soft. BCI): Se agregan nuevos set para beneficiario a ingresar.</li>
	 * </ul>
	 * </p>
	 * 
	 * @param beneficiario
	 *            Entidad beneficiario.
	 * @param tipoBeneficiario
	 *            Indica si el beneficiario es de la lista pendiente o inscrito.
	 *            [PEN, INS]
	 * @return BeneficiarioIngresoTO Entidad de beneficiario.
	 * @throws GeneralException
	 *             Excepci�n general de negocio.
	 * @since 1.0
	 */
	protected BeneficiarioIngresoTO crearBeneficiarioIngreso(
			wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.BeneficiarioTO beneficiario,
			String tipoBeneficiario) throws GeneralException {
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info(
					"[crearBeneficiarioIngreso][BCI_INI][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}
		BeneficiarioIngresoTO beneficiarioIngresoTO = new BeneficiarioIngresoTO();
		final int largoCuentaFormatear = 14;
		try {
			if (beneficiario == null) {
				throw new GeneralException(ESPECIAL, "[crearBeneficiacioIngreso] No data.");
			}

			beneficiarioIngresoTO.setNombreBeneficiario(
					(beneficiario.getDatosBeneficiarioLinea1() == null ? "" : beneficiario.getDatosBeneficiarioLinea1())
							+ (beneficiario.getDatosBeneficiarioLinea2() == null ? ""
									: beneficiario.getDatosBeneficiarioLinea2()));
			beneficiarioIngresoTO.getPaisCuenta()
					.setCodigo(beneficiario.getIsoPais() == null ? "" : beneficiario.getIsoPais());
			beneficiarioIngresoTO.getPaisCuenta().setValor(
					beneficiario.getNombrePaisBeneficiario() == null ? "" : beneficiario.getNombrePaisBeneficiario());
			beneficiarioIngresoTO.setDireccionBeneficiario(
					(beneficiario.getDatosBeneficiarioLinea3() == null ? "" : beneficiario.getDatosBeneficiarioLinea3())
							+ (beneficiario.getDatosBeneficiarioLinea4() == null ? ""
									: beneficiario.getDatosBeneficiarioLinea4()));
			beneficiarioIngresoTO.setEmail1(
					beneficiario.getEmailBeneficiario1() == null ? "" : beneficiario.getEmailBeneficiario1());

			if (!isBlank(beneficiario.getEmailBeneficiario2())) {
				EmailTO emailTO = new EmailTO();
				emailTO.setEmail(beneficiario.getEmailBeneficiario2());
				beneficiarioIngresoTO.getEmails().add(emailTO);
			}

			if (!isBlank(beneficiario.getEmailBeneficiario3())) {
				EmailTO emailTO = new EmailTO();
				emailTO.setEmail(beneficiario.getEmailBeneficiario3());
				beneficiarioIngresoTO.getEmails().add(emailTO);
			}
			if (!isBlank(beneficiario.getNombre2())) {
				NombreBeneficiarioTO nombre = new NombreBeneficiarioTO();
				nombre.setNombre(beneficiario.getNombre2());
				beneficiarioIngresoTO.getNombres().add(nombre);
			}
			if (!isBlank(beneficiario.getCiudad1())) {
				beneficiarioIngresoTO.setCiudadBeneficiario(beneficiario.getCiudad1());
			}
			if (!isBlank(beneficiario.getDireccion2())) {
				DireccionBeneficiarioTO direccion = new DireccionBeneficiarioTO();
				direccion.setDireccion(beneficiario.getDireccion2());
				beneficiarioIngresoTO.getDirecciones().add(direccion);
			}
			beneficiarioIngresoTO.setDireccionBeneficiario(beneficiario.getDireccion1());
			beneficiarioIngresoTO.getDatosPaisBeneficiario().setCodigo(
					beneficiario.getIsoPaisBeneficiario() == null ? "" : beneficiario.getIsoPaisBeneficiario());
			beneficiarioIngresoTO.setInformacionAdicional(beneficiario.getInformacionAdicionalDos());

			beneficiarioIngresoTO.setNumeroCuenta(beneficiario.getNumeroCuentaBeneficiario() == null ? ""
					: beneficiario.getNumeroCuentaBeneficiario());

			if (beneficiario.getNumeroCuentaBeneficiario() == null) {
				beneficiarioIngresoTO.setNumeroCuenta("");
				beneficiarioIngresoTO.setNumeroCuentaFormateada("");
			} else {
				beneficiarioIngresoTO.setNumeroCuenta(beneficiario.getNumeroCuentaBeneficiario());
				if (beneficiario.getNumeroCuentaBeneficiario().length() > largoCuentaFormatear) {
					beneficiarioIngresoTO.setNumeroCuentaFormateada(
							beneficiario.getNumeroCuentaBeneficiario().substring(0, largoCuentaFormatear) + "...");

				} else {
					beneficiarioIngresoTO.setNumeroCuentaFormateada(beneficiario.getNumeroCuentaBeneficiario());
				}
			}

			if (COD_PAIS_CHILE.equalsIgnoreCase(beneficiario.getIsoPais())) {
				beneficiarioIngresoTO.setRut(beneficiario.getIdBeneficiario());
			} else {
				beneficiarioIngresoTO.setReferencia(
						beneficiario.getIdBeneficiario() == null ? "" : beneficiario.getIdBeneficiario());
			}
			beneficiarioIngresoTO.getMoneda()
					.setCodigo(beneficiario.getMoneda() == null ? "" : beneficiario.getMoneda());
			beneficiarioIngresoTO.getMoneda()
					.setValor(beneficiario.getMoneda() == null ? "" : beneficiario.getMoneda());
			beneficiarioIngresoTO.getDatosBancoBeneficiario().setCodigoSwift(beneficiario.getSwiftBcoBeneficiario());
			beneficiarioIngresoTO.getDatosBancoBeneficiario()
					.setNombreDireccion((beneficiario.getDatosBcoBeneficiarioLinea1() == null ? ""
							: beneficiario.getDatosBcoBeneficiarioLinea1())
							+ (beneficiario.getDatosBcoBeneficiarioLinea2() == null ? ""
									: beneficiario.getDatosBcoBeneficiarioLinea2())
							+ (beneficiario.getDatosBcoBeneficiarioLinea3() == null ? ""
									: beneficiario.getDatosBcoBeneficiarioLinea3())
							+ (beneficiario.getDatosBcoBeneficiarioLinea4() == null ? ""
									: beneficiario.getDatosBcoBeneficiarioLinea4()));

			beneficiarioIngresoTO.getDatosBancoBeneficiario().setNumeroCuenta(
					beneficiario.getRutaPagoBcoBeneficiario() == null ? "" : beneficiario.getRutaPagoBcoBeneficiario());
			beneficiarioIngresoTO.getDatosBancoIntermediario().setCodigoSwift(
					beneficiario.getSwiftBcoIntermediario() == null ? "" : beneficiario.getSwiftBcoIntermediario());
			beneficiarioIngresoTO.getDatosBancoIntermediario()
					.setNombreDireccion((beneficiario.getDatosBcoIntermediarioLinea1() == null ? ""
							: beneficiario.getDatosBcoIntermediarioLinea1())
							+ (beneficiario.getDatosBcoIntermediarioLinea2() == null ? ""
									: beneficiario.getDatosBcoIntermediarioLinea2())
							+ (beneficiario.getDatosBcoIntermediarioLinea3() == null ? ""
									: beneficiario.getDatosBcoIntermediarioLinea3())
							+ (beneficiario.getDatosBcoIntermediarioLinea4() == null ? ""
									: beneficiario.getDatosBcoIntermediarioLinea4()));

			beneficiarioIngresoTO.getDatosBancoIntermediario()
					.setNumeroCuenta(beneficiario.getRutaPagoBcoIntermediario() == null ? ""
							: beneficiario.getRutaPagoBcoIntermediario());
			beneficiarioIngresoTO.setCodigoVerificacion(beneficiario.getCodigoVerificacion());
			beneficiarioIngresoTO.setTimeStamp(beneficiario.getTimeStampSistema());
			beneficiarioIngresoTO.setTipoBeneficiario(tipoBeneficiario);

			beneficiarioIngresoTO.setNecesitaIntermediario(!isBlank(beneficiario.getSwiftBcoIntermediario()));
		} catch (Exception e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[crearBeneficiacioIngreso]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_FINEX][Exception] mensaje de error " + e.getMessage(), e);
			}
			throw new GeneralException(ESPECIAL, "Error al crear beneficiario.");
		}
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info(
					"[crearBeneficiarioIngreso][BCI_FINOK] BeneficiarioIngresoTO " + beneficiarioIngresoTO.toString());
		}
		return beneficiarioIngresoTO;
	}

	/**
	 * Valida si tiene transferencias pendientes.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 31-01-2014, Mauricio Flores S�enz (Imagemaker IT): versi�n
	 * inicial</li>
	 * <li>1.1 24-06-2014, Luis L�pez Alamos (SEnTRA): Se modifica obtencion de
	 * tipo de Usuario</li>
	 * </ul>
	 * </p>
	 * 
	 * @return boolean
	 * @throws GeneralException
	 *             Excepci�n general de negocio.
	 * @since 1.0
	 */
	public boolean tieneTransferenciaspendientes() throws GeneralException {
		boolean response = false;
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[tieneTransferenciaspendientes][BCI_INI]["
					+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}
		ConsultaTransferenciaTO consultaTransferenciaTO = new ConsultaTransferenciaTO();
		consultaTransferenciaTO.setRutEmpresa(usuarioClienteModelMB.getClienteMB().getRut() + ""
				+ usuarioClienteModelMB.getClienteMB().getDigitoVerif());
		consultaTransferenciaTO.setServicioSolicitado(
				TablaValores.getValor(PYME_TRANSFER_TABLE, SERVICIO_SOLICITADO_KEY, "PENDIENTES"));
		consultaTransferenciaTO.setTipoUsuario(obtenerTipoUsuario());
		try {
			Transferencia1a1TO[] array = this.servicioTransferencia.obtenerTransferencias(consultaTransferenciaTO);
			if (array == null) {
				throw new GeneralException(ESPECIAL, "El servicio no entrego datos. Array null.");
			}
			if (array.length > 0) {
				response = true;
			}
		} catch (RemoteException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[tieneTransferenciaspendientes]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_FINEX][RemoteException] mensaje de error " + e.getMessage(), e);
			}
		}
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[tieneTransferenciaspendientes][BCI_FINOK]["
					+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}
		return response;
	}

	/**
	 * Crea objeto para realizar consulta a servicio detalle.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 03-02-2014, Mauricio Flores S�enz (Imagemaker IT): versi�n
	 * inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param beneficiarioIngresoTO
	 *            Entidad de beneficiario de vista.
	 * @return DatosBeneficiarioTO
	 * @throws GeneralException
	 *             Excepci�n general de negocio.
	 * @since 1.0
	 */
	public DatosBeneficiarioTO crearDatosBeneficiarioTO(BeneficiarioIngresoTO beneficiarioIngresoTO)
			throws GeneralException {

		if (beneficiarioIngresoTO == null) {
			throw new GeneralException(ESPECIAL, "BeneficiarioIngresoTO no puede ser null.");
		}
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[crearDatosBeneficiarioTO][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
					+ "] [BCI_INI] >> " + beneficiarioIngresoTO.toString());
		}
		DatosBeneficiarioTO output = new DatosBeneficiarioTO();
		output.setMoneda(beneficiarioIngresoTO.getMoneda().getCodigo());
		output.setIsoPais(beneficiarioIngresoTO.getPaisCuenta().getCodigo());
		if (COD_PAIS_CHILE.equalsIgnoreCase(beneficiarioIngresoTO.getPaisCuenta().getCodigo())) {
			output.setIdBeneficiario(beneficiarioIngresoTO.getRut());
		} else {
			output.setIdBeneficiario(beneficiarioIngresoTO.getReferencia());
		}
		output.setCodigoVerificacion(beneficiarioIngresoTO.getCodigoVerificacion());
		output.setTimeStamp(beneficiarioIngresoTO.getTimeStamp());
		output.setRutEmpresa(this.usuarioClienteModelMB.getClienteMB().getRut());
		output.setDvEmpresa(this.usuarioClienteModelMB.getClienteMB().getDigitoVerif());
		output.setTipoBeneficiario(beneficiarioIngresoTO.getTipoBeneficiario());
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[crearDatosBeneficiarioTO][BCI_FINOK]["
					+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}
		return output;
	}

	/**
	 * Journaliza operaciones de transfer monex.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 14-03-2014, Angel Cris�stomo L�pez (Imagemaker IT): versi�n
	 * inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param codigoEvento
	 *            C�digo de evento (INGRESA | ELIMINA).
	 * @param idReferencia
	 *            Identificador beneficiario.
	 * @param codMoneda
	 *            Codigo de la moneda.
	 * @param numCuentaIBAN
	 *            Numero de cuenta IBAN.
	 * @return Eventos
	 * @since 1.0
	 */
	public Eventos obtenerDatosJournalBeneficiario(String codigoEvento, String idReferencia, String codMoneda,
			String numCuentaIBAN) {

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[obtenerDatosJournalBeneficiario][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
					+ "] [BCI_INI]");
		}
		Eventos evento = new Eventos();
		try {

			HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
					.getRequest();

			String idMedio = request.getHeader("X-FORWARDED-FOR");
			if (idMedio == null) {
				idMedio = request.getRemoteAddr();
			}

			evento.setRutCliente(String.valueOf(usuarioClienteModelMB.getUsuarioModelMB().getRut()));
			evento.setDvCliente(String.valueOf(usuarioClienteModelMB.getUsuarioModelMB().getDigitoVerif()));
			evento.setRutOperadorCliente(String.valueOf(usuarioClienteModelMB.getClienteMB().getRut()));
			evento.setDvOperadorCliente(String.valueOf(usuarioClienteModelMB.getClienteMB().getDigitoVerif()));
			evento.setIdCanal(this.getUsuarioClienteModelMB().getUsuarioModelMB().getSesionMB().getCanalId());
			evento.setIdMedio(idMedio);
			evento.setIdProducto(TablaValores.getValor(PYME_TRANSFER_TABLE, "journalBeneficiario", "idProducto"));
			evento.setCodEventoNegocio(codigoEvento);
			evento.setSubCodEventoNegocio(
					TablaValores.getValor(PYME_TRANSFER_TABLE, "journalBeneficiario", "subCodEventoNegocio"));

			evento.setClavePrincipal(idReferencia);
			evento.setClaveSecundaria(codMoneda);
			evento.setCampoVariable(numCuentaIBAN);

		} catch (Exception e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger()
						.error("[obtenerDatosJournalBeneficiario][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
								+ "] [BCI_FINEX][Exception]Error con mensaje " + e.getMessage(), e);
			}
		}

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[obtenerDatosJournalBeneficiario][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
					+ "] [BCI_FINOK]");
		}

		return evento;
	}

	/**
	 * Evalua si una string es blanco o nulo.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 17-03-2014, Angel Cris�stomo L�pez (Imagemaker IT): versi�n
	 * inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param str
	 *            String a evaluar.
	 * @return boolean
	 * @since 1.0
	 */
	public static boolean isBlank(String str) {
		int strLen = 0;

		if (str != null) {
			strLen = str.length();
		}

		if ((str == null) || strLen == 0)
			return true;

		for (int i = 0; i < strLen; i++) {
			if (!Character.isWhitespace(str.charAt(i))) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Validaci�n consulta horario aprobacion transferencias.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 25/02/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing.
	 * Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param correlativo
	 *            correlativo transferencia.
	 * @throws TtffException
	 *             Excepci�n especifica TTFF.
	 * @throws GeneralException
	 *             Excepci�n General.
	 * @return RespuestaHorarioTO
	 * @since 1.0
	 */
	public RespuestaHorarioTO consultaHorarioIngresoAprobacion(int correlativo) throws TtffException, GeneralException {

		final String nombreMetodo = "consultaHorarioIngresoAprobacion";
		final String identificador = (usuarioClienteModelMB != null
				&& usuarioClienteModelMB.getUsuarioModelMB() != null)
						? String.valueOf(usuarioClienteModelMB.getUsuarioModelMB().getRut()) : "";

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "][" + identificador + "] [BCI_INI]");
			getLogger().info("[" + nombreMetodo + "][" + identificador + "] [Parametro][correlativo]["
					+ StringUtil.contenidoDe(correlativo) + "]");
		}

		RespuestaHorarioTO horarioTO = null;

		try {
			horarioTO = this.servicioTransferencia.consultarHorarioAprobacion(correlativo);
		} catch (RemoteException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[" + nombreMetodo + "][" + identificador + "] [BCI_FINEX][tipoExcepcion]["
						+ e.getMessage() + "]", e);
			}
			throw new GeneralException(e.getMessage());
		}

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "][" + identificador + "] [BCI_FINOK]["
					+ StringUtil.contenidoDe(horarioTO) + "]");
		}
		return horarioTO;
	}

	/**
	 * Retorna saldos fondos provisionados.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 28-04-2014, Angel Cris�stomo L�pez (Imagemaker IT): versi�n
	 * inicial</li>
	 * <li>1.1 20/08/2018, Bastian Nelson (Sermaluc) - Gonzalo Munoz(Ing. Soft.
	 * BCI): Se modifican log.</li>
	 * </ul>
	 * </p>
	 * 
	 * @param rutEmpresa
	 *            Rut empresa.
	 * @param dv
	 *            Dv empresa.
	 * @param codMoneda
	 *            Codigo moneda.
	 * @return CuentaTO[]
	 * @throws GeneralException
	 *             Excepcion de negocio.
	 * @since 1.0
	 */
	public CuentaTO[] obtenerFondosProvisionados(long rutEmpresa, char dv, String codMoneda) throws GeneralException {
		String rut = rutEmpresa + "-" + dv;
		CuentaTO[] cuentas = null;
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			if (usuarioClienteModelMB != null && usuarioClienteModelMB.getUsuarioModelMB() != null) {
				getLogger().debug("[obtenerSaldoFondoProvisionado][BCI_INI]["
						+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
			} else {
				getLogger().debug("[obtenerFondosProvisionados][BCI_INI]");
			}
		}

		try {
			cuentas = this.serviciosComex.obtenerFondosProvisionados(rut, codMoneda);
		} catch (ComexException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				if (usuarioClienteModelMB != null && usuarioClienteModelMB.getUsuarioModelMB() != null) {
					getLogger()
							.error("[obtenerFondosProvisionados]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
									+ " [BCI_FINEX][ComexException] error mensaje >> " + e.getCodigo(), e);
				} else {
					getLogger().error(
							"[obtenerFondosProvisionados][BCI_FINEX][ComexException] error mensaje >> " + e.getCodigo(),
							e);
				}
			}
			throw new GeneralException("ESPECIAL", e.getInfoAdic());
		} catch (RemoteException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				if (usuarioClienteModelMB != null && usuarioClienteModelMB.getUsuarioModelMB() != null) {
					getLogger()
							.error("[obtenerFondosProvisionados]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
									+ " [BCI_FINEX][RemoteException] error mensaje >> " + e.getMessage(), e);
				} else {
					getLogger().error("[obtenerFondosProvisionados] [BCI_FINEX][RemoteException] error mensaje >> "
							+ e.getMessage(), e);
				}
			}
			throw new GeneralException("ESPECIAL", e.getMessage());
		}
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			if (usuarioClienteModelMB != null && usuarioClienteModelMB.getUsuarioModelMB() != null) {
				getLogger().debug("[obtenerSaldoFondoProvisionado][BCI_FINOK]["
						+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
			} else {
				getLogger().debug("[obtenerFondosProvisionados][BCI_FINOK]");
			}
		}
		return cuentas;
	}

	/**
	 * M�todo que permite obtener el tipo de usuario, seg�n la l�gica de pyme
	 * donde s�lo existe Apoderado y Operador, sin importar que existan otros
	 * perfiles (Para que la aplicaci�n pueda ser soportada en el canal
	 * empresa).
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 25/02/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing.
	 * Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @return String
	 * @since 1.3
	 */

	public String obtenerTipoUsuario() {
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[obtenerTipoUsuario][BCI_INI]");
		}
		String tipoUsuario = "";

		if ((!USUARIO_APODERADO.equalsIgnoreCase(usuarioClienteModelMB.getUsuarioModelMB().getTipoUsuario()))) {
			tipoUsuario = USUARIO_OPERADOR;
		} else {
			tipoUsuario = usuarioClienteModelMB.getUsuarioModelMB().getTipoUsuario();
		}
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[obtenerTipoUsuario][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
					+ "] Usuario de session : [ " + usuarioClienteModelMB.getUsuarioModelMB().getTipoUsuario()
					+ "] Tipo Usuario Usado : [" + tipoUsuario + "]");
		}
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[obtenerTipoUsuario][BCI_FINOK]");
		}
		return tipoUsuario;

	}

	/**
	 * M�todo que formatea un monto correspondiente a una cuenta en moenda
	 * nacional las cuales son obtenidas desde tandem y tienen un formato en
	 * particular.
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 13/10/2015 Rafael Pizarro (TINet) - Robinson Hidalgo (Ing. Soft.
	 * BCI): Versi�n inicial</li>
	 * </ul>
	 * <p>
	 * 
	 * @param dato
	 *            Dato a formatear
	 * @return El dato recibido como par�metro en formato num�rico
	 * @since 1.5
	 */
	private double obtenerMontoCuentaTandem(String dato) {
		int cantidadDeCR = StringUtil.cuentaOcurrencias("CR", dato);
		String montoTexto = dato.trim();
		montoTexto = StringUtil.eliminaCaracteres(montoTexto, "CR");
		montoTexto = StringUtil.eliminaCaracteres(montoTexto, "$");
		montoTexto = StringUtil.reemplazaCaracteres(montoTexto, ",", ".");
		if (cantidadDeCR > 0) {
			return Double.parseDouble("-" + montoTexto);
		} else {
			return Double.parseDouble(montoTexto);
		}
	}

	/**
	 * M�todo encargado de retornar una lista filtrada de cuentas cuando su
	 * condicion de cierre sea "Aperturada".
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 07/11/2018 Marcelo Carrillo (Everis) - Ivan Sanchez(Ing. Soft.
	 * BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param cuentasOrigen
	 *            listado de cuentas.
	 * @return List<CuentaMonexTO> resultado.
	 * @since 1.12
	 */
	public List<CuentaMonexTO> getCuentasFiltradasApertura(List<CuentaMonexTO> cuentasOrigen) {
		List<CuentaMonexTO> resultado = null;
		final String nombreMetodo = "getCuentasFiltradasApertura";
		final String identificador = (usuarioClienteModelMB != null
				&& usuarioClienteModelMB.getUsuarioModelMB() != null)
						? String.valueOf(usuarioClienteModelMB.getUsuarioModelMB().getRut()) : "";

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "][" + identificador + "] [BCI_INI]");
		}
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[" + nombreMetodo + "][" + identificador
					+ "] Antes del if(cuentasOrigen!= null && !cuentasOrigen.isEmpty())");
		}
		if (cuentasOrigen != null && !cuentasOrigen.isEmpty()) {
			resultado = new ArrayList<CuentaMonexTO>();

			for (CuentaMonexTO cuentaMonexTO : cuentasOrigen) {
				if (cuentaMonexTO.getCondicionCierre() != null
						&& cuentaMonexTO.getCondicionCierre().equalsIgnoreCase(APERTURADA)) {
					resultado.add(cuentaMonexTO);
				}
			}
		}
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[" + nombreMetodo + "][" + identificador + "] [Cuentas Filtradas][ " + resultado + "]");
		}
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "][" + identificador + "] [BCI_FINOK]");
		}
		return resultado;
	}

	/**
	 * Retorna lista de monedas.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 25-09-2013, Angel Cris�stomo (Imagemaker IT): versi�n inicial
	 * </li>
	 * <li>1.1 20/08/2018, Bastian Nelson (Sermaluc) - Gonzalo Munoz(Ing. Soft.
	 * BCI): Se modifican log.</li>
	 * </ul>
	 * </p>
	 * 
	 * @param consultaMonedaTO
	 *            Entidad para consulta de moneda.
	 * @return List<Moneda1a1TO>
	 * @throws GeneralException
	 *             Excepci�n de negocio.
	 * @since 1.0
	 */
	public List<Moneda1a1TO> obtenerMonedas(ConsultaMonedaTO consultaMonedaTO) throws GeneralException {

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			if (usuarioClienteModelMB != null && usuarioClienteModelMB.getUsuarioModelMB() != null) {
				getLogger()
						.info("[obtenerMonedas][BCI_INI][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
			} else {
				getLogger().debug("[obtenerMonedas][BCI_INI][0]");
			}
		}

		List<Moneda1a1TO> listaMonedas = null;
		Moneda1a1TO[] monedasArray = null;
		try {
			monedasArray = servicioTransferencia.obtenerMonedas(consultaMonedaTO);
			listaMonedas = Arrays.asList(monedasArray);
		} catch (RemoteException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				if (usuarioClienteModelMB != null && usuarioClienteModelMB.getUsuarioModelMB() != null) {
					getLogger().error("[obtenerMonedas]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
							+ " [BCI_FINEX][RemoteException] mensaje de error " + e.getMessage(), e);
				} else {
					getLogger().error("[obtenerMonedas][BCI_FINEX][RemoteException] mensaje de error " + e.getMessage(),
							e);
				}
			}
			throw new GeneralException(e.getMessage());
		} catch (TtffException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				if (usuarioClienteModelMB != null && usuarioClienteModelMB.getUsuarioModelMB() != null) {
					getLogger().error("[obtenerMonedas]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
							+ " [BCI_FINEX][TtffException] mensaje de error " + e.getMessage(), e);
				} else {
					getLogger().error("[obtenerMonedas] [BCI_FINEX][TtffException] mensaje de error " + e.getMessage(),
							e);
				}
			}
			throw new GeneralException(e.getCodigo(), e.getInfoAdic());
		}
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			if (usuarioClienteModelMB != null && usuarioClienteModelMB.getUsuarioModelMB() != null) {
				getLogger().info(
						"[obtenerMonedas][BCI_FINOK][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
			} else {
				getLogger().debug("[obtenerMonedas][BCI_FINOK][0]");
			}
		}

		return listaMonedas;
	}

	/**
	 * Retorna la consulta de beneficiarios.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 25-09-2013, Angel Cris�stomo (Imagemaker IT): versi�n inicial
	 * </li>
	 * </ul>
	 * </p>
	 * 
	 * @param consulta
	 *            Entidad para consulta de beneficiario.
	 * @return List<Beneficiario1a1TO>
	 * @throws GeneralException
	 *             Excepci�n de negocio.
	 * @since 1.0
	 */
	public List<BeneficiarioRippleTO> obtenerBeneficiariosAutorizadosRipple(ConsultaBeneficiariosRippleTO consulta)
			throws GeneralException {

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[obtenerBeneficiariosAutorizados][BCI_INI]["
					+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}

		List<BeneficiarioRippleTO> beneficiarios = null;
		BeneficiarioRippleTO[] beneficiariosTOs = null;
		try {
			beneficiariosTOs = this.servicioTransferencia.obtenerBeneficiariosAutorizadosRipple(consulta);
			beneficiarios = Arrays.asList(beneficiariosTOs);
		} catch (TtffException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger()
						.error("[obtenerBeneficiariosAutorizados]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
								+ " [BCI_FINEX][TtffException] mensaje de error " + e.getMessage(), e);
			}
			throw new GeneralException(e.getCodigo(), e.getInfoAdic());
		} catch (RemoteException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger()
						.error("[obtenerBeneficiariosAutorizados]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
								+ " [BCI_FINEX][RemoteException] mensaje de error " + e.getMessage(), e);
			}
			throw new GeneralException(e.getMessage());
		}

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[obtenerBeneficiariosAutorizados][BCI_FINOK]["
					+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}

		return beneficiarios;
	}

	/**
	 * Retorna el detalle de un beneficiario a traves de nueva consulta.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 25/02/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing.
	 * Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param consDetBenRpp
	 *            Entidad de consulta detalle de beneficiario.
	 * @return DetalleBeneficiarioRippleTO
	 * @throws GeneralException
	 *             Excepci�n de negocio.
	 * @since 1.0
	 */
	public DetalleBeneficiarioRippleTO obtenerDetalleBeneficiarioRipple(
			ConsultaDetalleBeneficiarioRippleTO consDetBenRpp) throws GeneralException {

		final String nombreMetodo = "obtenerDetalleBeneficiarioRipple";
		final String identificador = (usuarioClienteModelMB != null
				&& usuarioClienteModelMB.getUsuarioModelMB() != null)
						? String.valueOf(usuarioClienteModelMB.getUsuarioModelMB().getRut()) : "";

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "][" + identificador + "] [BCI_INI]");
			getLogger().info("[" + nombreMetodo + "][" + identificador + "] [Parametro][consultaDetBeneficiario]["
					+ StringUtil.contenidoDe(consDetBenRpp) + "]");
		}

		DetalleBeneficiarioRippleTO beneficiarioTO = new DetalleBeneficiarioRippleTO();
		DetalleBeneficiarioRippleTO detBenefRpp = null;

		try {
			beneficiarioTO = this.servicioTransferencia.obtenerDetalleBeneficiarioRipple(consDetBenRpp);

			if (getLogger().isEnabledFor(Level.DEBUG)) {
				getLogger().debug("[" + nombreMetodo + "][" + identificador + "] [benefTO] ["
						+ StringUtil.contenidoDe(detBenefRpp) + "]");
			}
		} catch (RemoteException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[" + nombreMetodo + "][" + identificador + "] [BCI_FINEX][RemoteException]["
						+ e.getMessage() + "]", e);
			}
			throw new GeneralException(e.getMessage());
		}

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "][" + identificador + "] [BCI_FINOK]["
					+ StringUtil.contenidoDe(beneficiarioTO) + "]");
		}
		return beneficiarioTO;
	}

	/**
	 * Retorna comisi�n asociada a una transferencia.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 25/02/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing.
	 * Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param obtenerComision
	 *            Objeto con datos para la obtencion de la comision
	 * @return ComisionTO
	 * @throws GeneralException
	 *             Excepci�n de negocio.
	 * @since 1.0
	 */
	public ComisionTO obtenerComisiones(ObtenerComisionTO obtenerComision) throws GeneralException {
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger()
					.debug("[obtenerComisiones][BCI_INI][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}
		ComisionTO comisionTO = null;
		try {
			comisionTO = this.servicioTransferencia.obtenerComisionesRipple(obtenerComision);
		} catch (TtffException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[obtenerComisiones]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_FINEX][TtffException] mensaje de error " + e.getMessage(), e);
			}
			throw new GeneralException(e.getCodigo(), e.getInfoAdic());
		} catch (RemoteException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[obtenerComisiones]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_FINEX][RemoteException] mensaje de error " + e.getMessage(), e);
			}
			throw new GeneralException(e.getMessage());
		}
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug(
					"[obtenerComisiones][BCI_FINOK][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}
		return comisionTO;
	}

	/**
	 * Registro de transferencia confirmada y se obtiene el correlativo FBP
	 * asociado
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 07/03/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing.
	 * Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param regTransferencia
	 *            Objeto con datos para el registro de la transferencia
	 * @return RespuestaRegistroTransferenciaTO
	 * @throws GeneralException
	 *             Excepci�n de negocio.
	 * @since 1.0
	 */
	public RespuestaRegistroTransferenciaTO registrarTransferencia(RegistrarTransferenciaTO regTransferencia)
			throws GeneralException {
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug(
					"[registrarTransferencia][BCI_INI][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "]");
		}
		RespuestaRegistroTransferenciaTO resRegTransf = null;
		try {
			resRegTransf = this.servicioTransferencia.registrarTransferenciaRipple(regTransferencia);
		} catch (TtffException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[registrarTransferencia]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_FINEX][TtffException] mensaje de error " + e.getMessage(), e);
			}
			throw new GeneralException(e.getCodigo(), e.getInfoAdic());
		} catch (RemoteException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[registrarTransferencia]" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ " [BCI_FINEX][RemoteException] mensaje de error " + e.getMessage(), e);
			}
			throw new GeneralException(e.getMessage());
		}
		return resRegTransf;
	}

	public MonedasNodoTO getMonedasNodo() {
		return monedasNodo;
	}

	public void setMonedasNodo(MonedasNodoTO monedasNodo) {
		this.monedasNodo = monedasNodo;
	}

	/**
	 * Validaci�n Consultar L�mite horario Ingreso operaciones RIPPLE
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 19/03/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing.
	 * Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @return RespuestaHorarioTO
	 * @throws TtffException
	 *             Excepci�n especifica TTFF.
	 * @throws GeneralException
	 *             Excepci�n General.
	 * @since 1.0
	 */
	public RespuestaHorarioTO consultarLimiteHorarioIngreso() throws TtffException, GeneralException {

		final String nombreMetodo = "consultarLimiteHorarioIngreso";
		final String identificador = (usuarioClienteModelMB != null
				&& usuarioClienteModelMB.getUsuarioModelMB() != null)
						? String.valueOf(usuarioClienteModelMB.getUsuarioModelMB().getRut()) : "";

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "][" + identificador + "] [BCI_INI]");
		}

		RespuestaHorarioTO horarioTO = null;

		try {
			horarioTO = this.servicioTransferencia.consultarLimiteHorarioIngreso();
		} catch (RemoteException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[" + nombreMetodo + "][" + identificador + "] [BCI_FINEX][tipoExcepcion]["
						+ e.getMessage() + "]", e);
			}
			throw new GeneralException(e.getMessage());
		}

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "][" + identificador + "] [BCI_FINOK]["
					+ StringUtil.contenidoDe(horarioTO) + "]");
		}
		return horarioTO;
	}

	/**
	 * Firmar o Eliminar una transferencia Ripple
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 21/03/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing.
	 * Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param transferencia
	 *            Objeto con datos para realizar la firma o eliminacion de una
	 *            transferencia
	 * @return RespuestaSpTO
	 * @throws TtffException
	 *             Excepci�n especifica TTFF.
	 * @throws GeneralException
	 *             Excepci�n General.
	 * @since 1.0
	 */
	public RespuestaSpTO firmarOEliminarTransferenciaRipple(FirmarEliminarTransferenciaRipple transferencia)
			throws TtffException, GeneralException {

		final String identificador = (usuarioClienteModelMB != null
				&& usuarioClienteModelMB.getUsuarioModelMB() != null)
						? String.valueOf(usuarioClienteModelMB.getUsuarioModelMB().getRut()) : "";

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + FIRMAR_ELIMINAR_TRANSFERENCIA + "][" + identificador + "] [BCI_INI]");
		}

		RespuestaSpTO respuestaSpTO = null;
		try {
			respuestaSpTO = this.servicioTransferencia.firmarOEliminarTransferenciaRipple(transferencia);
		} catch (RemoteException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[" + FIRMAR_ELIMINAR_TRANSFERENCIA + "][" + identificador
						+ "] [BCI_FINEX][tipoExcepcion][" + e.getMessage() + "]", e);
			}
			throw new GeneralException(e.getMessage());
		}

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + FIRMAR_ELIMINAR_TRANSFERENCIA + "][" + identificador + "] [BCI_FINOK]["
					+ StringUtil.contenidoDe(respuestaSpTO) + "]");
		}
		return respuestaSpTO;
	}

	/**
	 * debitar cuenta corriente MX
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 21/03/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing.
	 * Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param correlativoFBP
	 *            numero de correlativo interno de una transferencia
	 * @return RespuestaSpTO
	 * @throws TtffException
	 *             Excepci�n especifica TTFF.
	 * @throws GeneralException
	 *             Excepci�n General.
	 * @since 1.0
	 */
	public RespuestaSpTO debitarCuentaCorrienteMX(int correlativoFBP) throws TtffException, GeneralException {

		final String identificador = (usuarioClienteModelMB != null
				&& usuarioClienteModelMB.getUsuarioModelMB() != null)
						? String.valueOf(usuarioClienteModelMB.getUsuarioModelMB().getRut()) : "";

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + DEBITAR_CUENTA_CORRIENTE + "][" + identificador + "] [BCI_INI]");
		}

		RespuestaSpTO respuestaSpTO = null;
		try {
			respuestaSpTO = this.servicioTransferencia.debitarCuentaCorrienteMX(correlativoFBP);
		} catch (RemoteException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[" + DEBITAR_CUENTA_CORRIENTE + "][" + identificador
						+ "] [BCI_FINEX][tipoExcepcion][" + e.getMessage() + "]", e);
			}
			throw new GeneralException(e.getMessage());
		}

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + DEBITAR_CUENTA_CORRIENTE + "][" + identificador + "] [BCI_FINOK]["
					+ StringUtil.contenidoDe(respuestaSpTO) + "]");
		}
		return respuestaSpTO;
	}

	/**
	 * consultar datos xCurrent
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 22/03/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing.
	 * Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param correlativoFBP
	 *            numero de correlativo interno de una transferencia
	 * @return DatosxCurrentTO
	 * @throws TtffException
	 *             Excepci�n especifica TTFF.
	 * @throws GeneralException
	 *             Excepci�n General.
	 * @since 1.0
	 */
	public DatosxCurrentTO consultarDatosxCurrent(int correlativoFBP) throws TtffException, GeneralException {
		final String identificador = (usuarioClienteModelMB != null
				&& usuarioClienteModelMB.getUsuarioModelMB() != null)
						? String.valueOf(usuarioClienteModelMB.getUsuarioModelMB().getRut()) : "";

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + CONSULTAR_DATOS_XCURRENT + "][" + identificador + "] [BCI_INI]");
		}

		DatosxCurrentTO datosxCurrent = null;
		try {
			datosxCurrent = this.servicioTransferencia.consultarDatosxCurrent(correlativoFBP);
		} catch (RemoteException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[" + DEBITAR_CUENTA_CORRIENTE + "][" + identificador
						+ "] [BCI_FINEX][tipoExcepcion][" + e.getMessage() + "]", e);
			}
			throw new GeneralException(e.getMessage());
		}

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + DEBITAR_CUENTA_CORRIENTE + "][" + identificador + "] [BCI_FINOK]["
					+ StringUtil.contenidoDe(datosxCurrent) + "]");
		}
		return datosxCurrent;
	}

	/**
	 * Este metodo rescata el token OAuth del OSB con el cual se autentifica en
	 * los servicios de Ripple
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 23/03/2019, Juan Riquelme (Everis) - Minheli Mejias (Ing. Soft.
	 * BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param datosUyP
	 * @return String
	 * @since 1.0
	 */
	 public String obtenerXCurrentToken(UyPDatos datosUyP){
	 final String identificador = (usuarioClienteModelMB != null &&
	 usuarioClienteModelMB.getUsuarioModelMB() != null) ?
	 String.valueOf(usuarioClienteModelMB.getUsuarioModelMB().getRut()) : "";
	
	 if (getLogger().isEnabledFor(Level.INFO)) {
	 getLogger().info("[" + OBTENER_XCURRENT_TOKEN + "][" + identificador + "][BCI_INI]");
	 }
	
	 URL oauthWSDL = null;
	
	 String namespaceURI =
	 "http://transferencias.servicios.productos.bci.cl/";
	 String localPart = "ServicioTransferenciaRippleService";
	 String endpointOSB =
	 TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "OSB", "url");
	
	 try {
	 oauthWSDL = new URL(endpointOSB);
	 } catch (MalformedURLException e) {
	 e.printStackTrace(System.out);
	 throw new RuntimeException("URL mal formada: " + endpointOSB, e);
	 }
	
	 ServicioTransferenciaRippleService servicioTransferenciaRippleService =
	 new ServicioTransferenciaRippleService(oauthWSDL, new QName(namespaceURI,
	 localPart));
	
	 ServicioTransferenciaRipple servicioTransferenciaRipplePortType =
	 servicioTransferenciaRippleService.getServicioTransferenciaRipplePort();
	 RippleOAuthRS rippleOAuthRS =
	 servicioTransferenciaRipplePortType.getOAuthToken();
	
	 if (getLogger().isEnabledFor(Level.INFO)) {
	 getLogger().info("[" + OBTENER_XCURRENT_TOKEN + "][" + identificador + "][BCI_FINOK][" + StringUtil.contenidoDe(rippleOAuthRS.getAccessToken()) +"]");
	 }
	 return rippleOAuthRS.getAccessToken();
	 }

	/**
	 * Este metodo consulta la disponibilida de rutas para enviar los pagos
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 23/03/2019, Juan Riquelme (Everis) - Minheli Mejias (Ing. Soft.
	 * BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param xCurrentToken
	 * @param datosxCurrent
	 * @return QuoteCollectionResponse
	 * @throws TtffException
	 *             Excepci�n especifica TTFF.
	 * @since 1.0
	 */
	 public QuoteCollectionResponse createQuoteCollection(String
	 xCurrentToken, DatosxCurrentTO datosxCurrent) throws TtffException{
	 final String identificador = (usuarioClienteModelMB != null &&
	 usuarioClienteModelMB.getUsuarioModelMB() != null) ?
	 String.valueOf(usuarioClienteModelMB.getUsuarioModelMB().getRut()) : "";
	
	 if (getLogger().isEnabledFor(Level.INFO)) {
	 getLogger().info("[createQuoteCollection][" + identificador + "][BCI_INI]");
	 }
	
	 QuoteCollectionRequest quoteColReq = new QuoteCollectionRequest();
	
	 quoteColReq.setSendingAddress(TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE,
	 "createQuote", "sendingAddress"));
	 quoteColReq.setReceivingAddress(TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE,
	 "createQuote", "receivingAddress"));
	 quoteColReq.setAmount(datosxCurrent.getMontoTransf());
	 quoteColReq.setCurrency(Currency.getInstance(datosxCurrent.getMonedaTransf()));//moneda
//	 transferencia
	 quoteColReq.setCustomFee(datosxCurrent.getMontoComision());//comision
	 quoteColReq.setQuoteType(QuoteType.RECEIVER_AMOUNT);
	
	 try {
	 QuoteCollectionResponse quoteColResp =
	 xCurrentClient.createQuoteCollection(xCurrentToken, quoteColReq);
	 if ( quoteColResp == null || quoteColResp.getQuotes() == null ||
	 quoteColResp.getQuotes().length == 0){
	 if (getLogger().isEnabledFor(Level.ERROR)) {
	 getLogger().info("[createQuoteCollection] [" + identificador + "][BCI_FINEX][Error en la configuracion de cuentas]");//MEJORAR MENSAJE A MOSTRAR EN LOG
	 }
	 throw new TtffException("9");
	 }
	
	 if (getLogger().isEnabledFor(Level.INFO)) {
	 getLogger().info("[createQuoteCollection][" + identificador + "][BCI_FINOK][" + StringUtil.contenidoDe(quoteColResp) + "]");
	 }
	 return quoteColResp;
	 }
	 catch (RippleException e) {
	 if (getLogger().isEnabledFor(Level.ERROR)) {
	 getLogger().info("[createQuoteCollection] [" + identificador + "][BCI_FINEX][" + e.getResponseError() + "]");
	 }
	 throw new TtffException("9", e.getResponseError().getDetail());
	 }
	 }

	/**
	 * Este metodo crea las transaccion en ripple asignando un id
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 23/03/2019, Juan Riquelme (Everis) - Minheli Mejias (Ing. Soft.
	 * BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param xCurrentToken
	 * @param datosxCurrent
	 * @param quoteId
	 * @return UUID
	 * @since 1.0
	 */
	 public UUID acceptQuote(String xCurrentToken, DatosxCurrentTO
	 datosxCurrent, UUID quoteId) throws TtffException{
	 final String identificador = (usuarioClienteModelMB != null &&
	 usuarioClienteModelMB.getUsuarioModelMB() != null) ?
	 String.valueOf(usuarioClienteModelMB.getUsuarioModelMB().getRut()) : "";
	
	 if (getLogger().isEnabledFor(Level.INFO)) {
	 getLogger().info("[acceptQuote][" + identificador + "] [BCI_INI]");
	 }
	
	 AcceptQuoteRequest accQuoteReq = new AcceptQuoteRequest();
	
	 accQuoteReq.setQuoteId(quoteId);
	 Map<String, Object> userInfo = createUserInfoMap(datosxCurrent);
	 accQuoteReq.setUserInfo(userInfo);
	 accQuoteReq.setSenderEndToEndId(TablaValores.getValor(PYME_TRANSFER_TABLE,
	 "senderEndToEnd", "id"));
	 accQuoteReq.setInternalId(TablaValores.getValor(PYME_TRANSFER_TABLE,
	 "senderEnd", "id"));
	
	 try {
	 Payment payment = xCurrentClient.acceptQuote(xCurrentToken, accQuoteReq);
	 if (getLogger().isEnabledFor(Level.INFO)) {
	 getLogger().info("[acceptQuote][" + identificador + "] [BCI_FINOK][" +
	 payment.getPaymentId() + "]");
	 }
	 return payment.getPaymentId();
	 } catch (RippleException e) {
	 if (getLogger().isEnabledFor(Level.ERROR)) {
	 getLogger().info("[acceptQuote] [" + identificador + "] [BCI_FINEX][" +
	 e.getResponseError() + "]");
	 }
	 throw new TtffException("9", e.getResponseError().getDetail());
	 }
	 }

	private Map<String, Object> createUserInfoMap(DatosxCurrentTO datosxCurrent) {
		final String identificador = (usuarioClienteModelMB != null
				&& usuarioClienteModelMB.getUsuarioModelMB() != null)
						? String.valueOf(usuarioClienteModelMB.getUsuarioModelMB().getRut()) : "";

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[createUserInfoMap][" + identificador + "] [BCI_INI]");
		}

		 Iso20022UserInfo iso20022UserInfo =
		 createIso20022UserInfo(datosxCurrent);
		 iso20022UserInfo.validate();

		Map<String, Object> userInfo = new LinkedHashMap<String, Object>();
		 userInfo.put("PmtInf", iso20022UserInfo);

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[createUserInfoMap][" + identificador + "] [BCI_FINOK][" + userInfo + "]");
		}
		return userInfo;
	}

	private Iso20022UserInfo createIso20022UserInfo(DatosxCurrentTO
	 datosxCurrent) {
	 final String identificador = (usuarioClienteModelMB != null &&
	 usuarioClienteModelMB.getUsuarioModelMB() != null) ?
	 String.valueOf(usuarioClienteModelMB.getUsuarioModelMB().getRut()) : "";
	
	 if (getLogger().isEnabledFor(Level.INFO)) {
	 getLogger().info("[createIso20022UserInfo][" + identificador + "][BCI_INI]");
	 }
	
	 Iso20022UserInfo ui = new Iso20022UserInfo();
	
	 ui.setPaymentId(new PaymentId());
	 ui.getPaymentId().setTransactionId(datosxCurrent.getTrn());
	
	 ui.setDebtor(new Debtor());
	 ui.getDebtor().setName(datosxCurrent.getNombreOrdenante());
	 ui.getDebtor().setPostalAddress(new PostalAddress());
	
	 ui.getDebtor().getPostalAddress().setAddressLine(new
	 String[]{datosxCurrent.getDireccionOrdenante()});
	 ui.getDebtor().getPostalAddress().setCountry(datosxCurrent.getPaisOrdenante());
	 ui.getDebtor().setId(new PersonId());
	 ui.getDebtor().getId().setOrganizationId(new OrganizationId());
	 ui.getDebtor().getId().getOrganizationId().setOther(new OtherId());
	 ui.getDebtor().getId().getOrganizationId().getOther().setSchemeName(new
	 SchemeName());
	 ui.getDebtor().getId().getOrganizationId().getOther().getSchemeName().setCode("TXID");;
	 ui.getDebtor().getId().getOrganizationId().getOther().setId(datosxCurrent.getRutOrdenante());
	
	 ui.setDebtorAccount(new Account());
	 ui.getDebtorAccount().setId(new AccountId());
	 ui.getDebtorAccount().getId().setOther(new OtherId());
	 ui.getDebtorAccount().getId().getOther().setId(datosxCurrent.getNumCtaOrdenante());
	
	 ui.setCreditor(new Creditor());
	 ui.getCreditor().setName(datosxCurrent.getNombreBenef());
	 ui.getCreditor().setPostalAddress(new PostalAddress());
	
	 String[] creditorAddress = null;
	 if(datosxCurrent.getDireccionBenefL2()!=null &&
	 !datosxCurrent.getDireccionBenefL2().trim().isEmpty()){
	 creditorAddress = new String[]
	 {datosxCurrent.getDireccionBenefL1(),datosxCurrent.getDireccionBenefL2()};
	 }
	 else{
	 creditorAddress = new String[] {datosxCurrent.getDireccionBenefL1()};
	 }
	
	 ui.getCreditor().getPostalAddress().setAddressLine(creditorAddress);
	 ui.getCreditor().getPostalAddress().setCountry(datosxCurrent.getPaisBenef());
	
	 ui.setCreditorAccount(new Account());
	 ui.getCreditorAccount().setId(new AccountId());
	 ui.getCreditorAccount().getId().setOther(new OtherId());
	 ui.getCreditorAccount().getId().getOther().setId(datosxCurrent.getNumCtaBenef());
	
	 ui.setRemmitanceInformation(new RemmitanceInformation());
	 ui.getRemmitanceInformation().setUnstructured(new
	 String[]{datosxCurrent.getRemesa()});
	
	 if (getLogger().isEnabledFor(Level.INFO)) {
	 getLogger().info("[createIso20022UserInfo][" + identificador + "][BCI_FINOK][" + ui + "]");
	 }
	 return ui;
	 }

	/**
	 * Este metodo crea las transaccion en ripple asignando un id
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 23/03/2019, Danilo Dominguez (Everis) - Minheli Mejias (Ing.
	 * Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param correlativoFBP
	 * @param resultado
	 * @param paymentId
	 * @return RespuestaSpTO
	 * @since 1.0
	 */
	public RespuestaSpTO registrarResulatadoxCurrent(String correlativoFBP, String resultado, String paymentId)
			throws TtffException, GeneralException {
		final String identificador = (usuarioClienteModelMB != null
				&& usuarioClienteModelMB.getUsuarioModelMB() != null)
						? String.valueOf(usuarioClienteModelMB.getUsuarioModelMB().getRut()) : "";

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + REGISTRAR_RESULTADO_XCURRENT + "][" + identificador + "] [BCI_INI]");
		}

		RespuestaSpTO respuestaSpTO = null;
		try {
			respuestaSpTO = this.servicioTransferencia.registrarResulatadoxCurrent(correlativoFBP, resultado,
					paymentId);
		} catch (RemoteException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[" + DEBITAR_CUENTA_CORRIENTE + "][" + identificador
						+ "] [BCI_FINEX][tipoExcepcion][" + e.getMessage() + "]", e);
			}
			throw new GeneralException(e.getMessage());
		}

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + REGISTRAR_RESULTADO_XCURRENT + "][" + identificador + "] [BCI_FINOK]["
					+ StringUtil.contenidoDe(respuestaSpTO) + "]");
		}
		return respuestaSpTO;
	}
}