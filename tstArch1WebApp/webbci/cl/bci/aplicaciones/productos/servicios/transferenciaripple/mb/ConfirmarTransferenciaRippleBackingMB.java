package cl.bci.aplicaciones.productos.servicios.transferenciaripple.mb;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import java.util.UUID;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UISelectOne;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import cl.bci.aplicaciones.cliente.mb.ClienteMB;
import cl.bci.aplicaciones.cliente.mb.UsuarioClienteModelMB;
import cl.bci.aplicaciones.productos.servicios.transferenciamonex.constant.JournalTransferMonex;
import cl.bci.aplicaciones.productos.servicios.transferenciamonex.to.CodigoValor;
import cl.bci.aplicaciones.productos.servicios.transferenciamonex.to.CuentaMonexTO;
import cl.bci.aplicaciones.productos.servicios.transferenciamonex.to.TransferenciaTO;
import cl.bci.aplicaciones.seguridad.autenticacion.mb.SegundaClaveMB;
import cl.bci.arquitectura.ripple.xcurrent.dto.QuoteCollectionResponse;
//import cl.bci.arquitectura.ripple.xcurrent.dto.QuoteCollectionResponse;
import cl.bci.infraestructura.utilitarios.error.mb.MensajesErrorUtilityMB;
import cl.bci.infraestructura.web.journal.mb.JournalUtilityMB;
import cl.bci.infraestructura.web.seguridad.autorizaciones.mb.AutorizadorUtilityMB;
import cl.bci.infraestructura.web.seguridad.segundaclave.SegundaClaveUIInput;
import cl.bci.infraestructura.web.seguridad.segundaclave.to.CamposDeLlaveTO;
import cl.bci.infraestructura.ws.excepcion.NegocioException;
import wcorp.aplicaciones.productos.servicios.pagos.pagosmasivos.ServiciosDePagosMasivos;
import wcorp.aplicaciones.productos.servicios.pagos.pagosmasivos.ServiciosDePagosMasivosHome;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.sessionfacade.TtffException;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.CambiaEstadoTttfMonexTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.CodigoCambioTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.ConsultaMonedaTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.CuentaComisionTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.DatosAutorizacionTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.Moneda1a1TO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.RespuestaHorarioTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.TransferenciaMonexTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.BeneficiarioRippleTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.ComisionTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.ConsultaBeneficiariosRippleTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.ConsultaDetalleBeneficiarioRippleTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.DatosHashTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.DatosxCurrentTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.DetalleBeneficiarioRippleTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.FirmarEliminarTransferenciaRipple;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.MonedasNodoTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.MonedasPermitidasTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.ObtenerComisionTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.RegistrarTransferenciaTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.RespuestaRegistroTransferenciaTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.RespuestaSpTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.TransferenciasRippleTO;
import wcorp.model.seguridad.UyPCache;
import wcorp.model.seguridad.UyPDatos;
import wcorp.serv.bciexpress.ConApoquehanFirmado;
import wcorp.serv.bciexpress.ResultConsultarApoderadosquehanFirmado;
import wcorp.serv.seguridad.NoSessionException;
import wcorp.serv.seguridad.SeguridadException;
import wcorp.util.EnhancedServiceLocator;
import wcorp.util.ErroresUtil;
import wcorp.util.GeneralException;
import wcorp.util.StringUtil;
import wcorp.util.TablaValores;
import wcorp.util.journal.Eventos;

/**
 * Bean encargado de realizar el flujo de confirmacion de transferencias.
 * <p>
 * Registro de Versiones :
 * <ul>
 * <li>1.0 09-03-2019, Danilo Dominguez(Everis) - Minheli Mejias (Ing. Soft. BCI): Version inicial</li>
 * </ul>
 * </p>
 * <p>
 * <B>Todos los derechos reservados por Banco de Credito e Inversiones.</B>
 * </p>
 */
@ManagedBean
@RequestScoped
public class ConfirmarTransferenciaRippleBackingMB implements Serializable {

	/**
	 * Metodo del servicio de Segunda Clave.
	 */
	protected static final String SERVICIO_SEGUNDA_CLAVE = "TransferenciasMxUnoAUno";
	/**
	 * Objeto de transporte de datos de transferencia.
	 */
	protected static final String TRANSFERENCIA_MONEX_TO = "transferenciaMonexTO";
	/**
	 * Archivo parametros Autorizar FyP.
	 */
	private static final String ARCH_PARAM_VAL = "BCIExpressII_Autorizar_FyP.parametros";

	/**
	 * Codigo de error generico fyp.
	 */
    private static final String ERROR_GENERICO_FYP= "FYP-011";

	/**
    *Representa numero uno.
	 */
	private static final int NUM_UNO = 1;

	/**
	 * Codigo de error no puede registrar la firma.
	 */
    private static final String ERROR_NO_REGISTR_FYP= "ODP-005";

	/**
	 * Servicio solicitado para Apoderado.
	 */
	private static final String SERVICIO_SOLICITADO_CUR = "CUR";	
	/**
	 * Vista asociada al autorizador.
	 */
	private static final String VISTA = "transferencias-Monex-1a1";
	/**
	 * Accion asociada al autorizador.
	 */
	private static final String ACCION = "Autorizar";
	/**
	 * Tabla de parametros de transfer monex.
	 */
	private static final String TABLA_PARAMETROS_TRANSFERRIPPLE = "pyme/transferRipple.parametros";
	/**
	 * Parametro de origen de la solicitud.
	 */
	private static final String ORIGEN_SOLICITUD = "WEB";
	/**
	 * Constante ITF.
	 */
	private static final String TRANSFERENCIA_PARAMETROS = "transferencia";
	/**
	 * Constante monto maximo transferencia.
	 */
	private static final String MONTO_MAXIMO_TRANSFER = "montoMaximoTransferencia";

	/**
	 * Constante mensaje a obtener desde archivo de parametros.
	 */
	private static final String MENSAJE = "mensaje";
	/**
	 *Representa numero 0,0.
	 */
	private static final double NUM_CERO_COMA_CERO = 0.0;	
	/**
	 * Bean de negocio usuarioClienteModelMB.
	 */
	@ManagedProperty(value = "#{usuarioClienteModelMB}")
	private UsuarioClienteModelMB usuarioClienteModelMB;
	/**
	 * Bean de negocio usuarioClienteModelMB.
	 */
	@ManagedProperty(value = "#{transferenciasRippleBackingMB}")
	private TransferenciasRippleBackingMB transferenciasRippleBackingMB;
	/**
	 * Bean de negocio transferenciasRippleUtilityMB.
	 */
	@ManagedProperty(value = "#{transferenciasRippleUtilityMB}")
	private TransferenciasRippleUtilityMB transferenciasRippleUtilityMB;
	/**
	 * Bean de negocio clienteMB.
	 */
	@ManagedProperty(value = "#{clienteMB}")
	private ClienteMB clienteMB;
	/**
	 * Cuenta cargo comision.
	 */
	private CuentaComisionTO cuentaComisionTo;
	/**
	 * Inicializa.
	 */
	private boolean init;
	/**
	 * Bean de negocio transferenciasRippleViewMB.
	 */
	@ManagedProperty(value = "#{transferenciasRippleViewMB}")
	private TransferenciasRippleViewMB transferenciasRippleViewMB;
	/**
	 * Monto a tranferir string.
	 */
	private String strMontoATransferir;
	/**
	 * Bean manejo de mensajes.
	 */
	@ManagedProperty(value = "#{mensajesErrorUtilityMB}")
	private MensajesErrorUtilityMB mbErrorUtilityMB;
	/**
	 * Bean de negocio autorizadorUtilityMB.
	 */
	@ManagedProperty(value = "#{autorizadorUtilityMB}")
	private AutorizadorUtilityMB autorizadorUtilityMB;
	/**
	 * Referencia a la componente que maneja la segunda Clave.
	 */
	private SegundaClaveUIInput segundaClave;
	/**
	 * Bean que maneja operaciones de validacion de segunda clave.
	 */
	@ManagedProperty(value = "#{segundaClaveMB}")
	private SegundaClaveMB segundaClaveMB;
	/**
	 * Valor ingresado de segunda clave.
	 */
	private String segundaClaveValor;
	/**
	 * cuenta de la comision.
	 */
	private String cuentaComision;
	/**
	 * Simbolo de la moneda.
	 */
	private String simboloMoneda;
	/**
	 * Monto a tranferir string.
	 */
	private double montoMax = 0;
	/**
	 * Error en monto.
	 */
	private boolean errorMonto;	
	/**
	 * Mensaje de error.
	 */
	private String mensajeError;
	/**
	 * Saldo disponible cuenta corriente.
	 */
	private Double saldoDisponible;
	/**
	 * comision de Ripple.
	 */
	private ComisionTO comisionRipple = new ComisionTO();
	/**
	 * key .
	 */
	private String key;
	/**
	 * monto a transferir.
	 */
	private Double montoATransferir;
	/**
	 * Inyeccion de Managed Bean de journal.
	 */
	@ManagedProperty(value = "#{journalUtilityMB}")
	private JournalUtilityMB journalUtilityMB;
	/**
	 * log de la clase TransferenciasRippleBackingMB.
	 */
	private transient Logger log = Logger.getLogger(ConfirmarTransferenciaRippleBackingMB.class);
	/**
	 * Corresponde a la moneda selecciona.
	 */
	private Moneda1a1TO monedaOtraCuenta = new Moneda1a1TO();
	/**
	 * Corresponde a la cuenta cargo comision seleccionada.
	 */
	private CuentaMonexTO cuentaCargoComision = new CuentaMonexTO();
	/**
	 * Corresponde a la moneda selecciona.
	 */
	private MonedasPermitidasTO monedaRipple = new MonedasPermitidasTO();
	/**
	 * Corresponde al numero cuenta origen.
	 */
	private CuentaMonexTO cuentaOrigen = new CuentaMonexTO();
	/**
	 * Corresponde al motivo de pago de transferencia.
	 */
	private String motivoPago;
	/**
	 * Corresponde al los datos de la transferencia ingresada.
	 */
	private TransferenciasRippleTO transferenciaRipple = new TransferenciasRippleTO();
	/**
	 * Codigo Cambio.
	 */
	private CodigoCambioTO codigoCambio = new CodigoCambioTO();

	/**
	 * paisBcoBenef.
	 */
	private String paisBcoBenef; 
	

	public UsuarioClienteModelMB getUsuarioClienteModelMB() {
		return usuarioClienteModelMB;
	}

	public void setUsuarioClienteModelMB(UsuarioClienteModelMB usuarioClienteModelMB) {
		this.usuarioClienteModelMB = usuarioClienteModelMB;
	}

	public TransferenciasRippleBackingMB getTransferenciasRippleBackingMB() {
		return transferenciasRippleBackingMB;
	}

	public void setTransferenciasRippleBackingMB(TransferenciasRippleBackingMB transferenciasRippleBackingMB) {
		this.transferenciasRippleBackingMB = transferenciasRippleBackingMB;
	}

	public ClienteMB getClienteMB() {
		return clienteMB;
	}

	public void setClienteMB(ClienteMB clienteMB) {
		this.clienteMB = clienteMB;
	}

	public TransferenciasRippleUtilityMB getTransferenciasRippleUtilityMB() {
		return transferenciasRippleUtilityMB;
	}

	public void setTransferenciasRippleUtilityMB(TransferenciasRippleUtilityMB transferenciasRippleUtilityMB) {
		this.transferenciasRippleUtilityMB = transferenciasRippleUtilityMB;
	}

	public CuentaComisionTO getCuentaComisionTo() {
		return cuentaComisionTo;
	}

	public void setCuentaComisionTo(CuentaComisionTO cuentaComisionTo) {
		this.cuentaComisionTo = cuentaComisionTo;
	}

	public boolean isInit() {
		return init;
	}

	public void setInit(boolean init) {
		this.init = init;
	}

	public TransferenciasRippleViewMB getTransferenciasRippleViewMB() {
		return transferenciasRippleViewMB;
	}

	public void setTransferenciasRippleViewMB(TransferenciasRippleViewMB transferenciasRippleViewMB) {
		this.transferenciasRippleViewMB = transferenciasRippleViewMB;
	}

	public Moneda1a1TO getMonedaOtraCuenta() {
		return monedaOtraCuenta;
	}

	public void setMonedaOtraCuenta(Moneda1a1TO monedaOtraCuenta) {
		this.monedaOtraCuenta = monedaOtraCuenta;
	}

	public CuentaMonexTO getCuentaCargoComision() {
		return cuentaCargoComision;
	}

	public void setCuentaCargoComision(CuentaMonexTO cuentaCargoComision) {
		this.cuentaCargoComision = cuentaCargoComision;
	}

	public MonedasPermitidasTO getMonedaRipple() {
		return monedaRipple;
	}

	public void setMonedaRipple(MonedasPermitidasTO monedaRipple) {
		this.monedaRipple = monedaRipple;
	}

	public String getStrMontoATransferir() {
		return strMontoATransferir;
	}

	public void setStrMontoATransferir(String strMontoATransferir) {
		this.strMontoATransferir = strMontoATransferir;
	}

	public MensajesErrorUtilityMB getMbErrorUtilityMB() {
		return mbErrorUtilityMB;
	}

	public void setMbErrorUtilityMB(MensajesErrorUtilityMB mbErrorUtilityMB) {
		this.mbErrorUtilityMB = mbErrorUtilityMB;
	}

	public AutorizadorUtilityMB getAutorizadorUtilityMB() {
		return autorizadorUtilityMB;
	}

	public void setAutorizadorUtilityMB(AutorizadorUtilityMB autorizadorUtilityMB) {
		this.autorizadorUtilityMB = autorizadorUtilityMB;
	}

	public SegundaClaveUIInput getSegundaClave() {
		return segundaClave;
	}

	public void setSegundaClave(SegundaClaveUIInput segundaClave) {
		this.segundaClave = segundaClave;
	}

	public SegundaClaveMB getSegundaClaveMB() {
		return segundaClaveMB;
	}

	public void setSegundaClaveMB(SegundaClaveMB segundaClaveMB) {
		this.segundaClaveMB = segundaClaveMB;
	}

	public String getSegundaClaveValor() {
		return segundaClaveValor;
	}

	public void setSegundaClaveValor(String segundaClaveValor) {
		this.segundaClaveValor = segundaClaveValor;
	}

	public CuentaMonexTO getCuentaOrigen() {
		return cuentaOrigen;
	}

	public void setCuentaOrigen(CuentaMonexTO cuentaOrigen) {
		this.cuentaOrigen = cuentaOrigen;
	}

	public String getMotivoPago() {
		return motivoPago;
	}

	public void setMotivoPago(String motivoPago) {
		this.motivoPago = motivoPago;
	}

	public void setTransferenciaRipple(TransferenciasRippleTO transferenciasRipple) {
		this.transferenciaRipple = transferenciasRipple;
	}

	public TransferenciasRippleTO getTransferenciaRipple() {
		return this.transferenciaRipple;
	}

	public JournalUtilityMB getJournalUtilityMB() {
		return journalUtilityMB;
	}

	public void setJournalUtilityMB(JournalUtilityMB journalUtilityMB) {
		this.journalUtilityMB = journalUtilityMB;
	}

	public Double getSaldoDisponible() {
		return saldoDisponible;
	}

	public void setSaldoDisponible(Double saldoDisponible) {
		this.saldoDisponible = saldoDisponible;
	}

	/**
	 * Retorna Cuentas Comex y cuentas CorrientesPrimas.
	 * 
	 * @return List<CuentaMonexTO>
	 * @since 1.0
	 */
	public List<CuentaMonexTO> getCuentasComision() {
		return transferenciasRippleViewMB.getCuentasComision();
	}

	public CodigoCambioTO getCodigoCambio() {
		return codigoCambio;
	}

	public void setCodigoCambio(CodigoCambioTO codigoCambio) {
		this.codigoCambio = codigoCambio;
	}
	
	public String getCuentaComision() {
		return cuentaComision;
	}

	public void setCuentaComision(String cuentaComision) {
		this.cuentaComision = cuentaComision;
	}
	
	public String getSimboloMoneda() {
		return simboloMoneda;
	}

	public void setSimboloMoneda(String simboloMoneda) {
		this.simboloMoneda = simboloMoneda;
	}

	public boolean isErrorMonto() {
		return errorMonto;
	}

	public void setErrorMonto(boolean errorMonto) {
		this.errorMonto = errorMonto;
	}

	public String getMensajeError() {
		return mensajeError;
	}

	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}

	public ComisionTO getComisionRipple() {
		return comisionRipple;
	}

	public void setComisionRipple(ComisionTO comisionRipple) {
		this.comisionRipple = comisionRipple;
	}
	
	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
	
	public Double getMontoATransferir() {
		return montoATransferir;
	}

	public void setMontoATransferir(Double montoATransferir) {
		this.montoATransferir = montoATransferir;
	}
	
	public String getPaisBcoBenef() {
		return paisBcoBenef;
	}

	public void setPaisBcoBenef(String paisBcoBenef) {
		this.paisBcoBenef = paisBcoBenef;
	}

	/**
     * Constante global a pagina inicial de ripple.
     */
    public static final String CONFIRMACION = "CON";
    
    /**
     * Constante global de pagina de Comprobante.
     */
    private static final String COMPROBANTE_TRANSFERENCIA_RIPPLE = "comprobanteTransferenciasRipple.jsf";
    
    /**
     * Constante para obtener el objeto desde request.
     */
    protected static final String TRANSFERENCIA = "TransferenciaTO";
    
    /**
     * Constante para obtener el objeto desde request.
     */
    protected static final String FIRMAR_TRANSFERENCIA = "firmarTransferencia";
    
	/**
	 * Obtiene rut empresa desde clienteMB.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 25/02/2019, Danilo Dominguez (Everis) - Minheli Mejias (Ing. Soft. BCI): version inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @return String
	 * @throws GeneralException Excepcion negocio.
	 * @since 1.0
	 */
	private String getRutEmpresa() throws GeneralException {
		
		String nombreMetodo = "getRutEmpresa";
		
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "]["+ usuarioClienteModelMB.getUsuarioModelMB().getRut() +"][BCI_INI]");
		}
		
		String rut = "";
		try {
			rut = clienteMB.getRut() + "" + clienteMB.getDigitoVerif();
		} catch (Exception e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[getRutEmpresa][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ "] [BCI_FINEX][Exception]Error con mensaje " + e.getMessage(), e);
			}
			throw new GeneralException("ESPECIAL", e.getMessage());
		}
		
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "]["+ usuarioClienteModelMB.getUsuarioModelMB().getRut() +"][BCI_FINOK][" + StringUtil.contenidoDe(rut) + "]");
		}		
		
		return rut;
	}

	/**
	 * Generacion de objeto con los datos necesarios para obtener la comision
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 04/03/2019, Danilo Dominguez (Everis) - Minheli Mejias (Ing. Soft. BCI): version inicial</li>
	 * </ul>
	 * </p>
     * @param obtenerComision Entidad de negocio de comision. 
	 * @since 1.0
	 */
	private void generarObtenerComision(ObtenerComisionTO obtenerComision) {
		
		String nombreMetodo = "generarObtenerComision";
		
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "]["+ usuarioClienteModelMB.getUsuarioModelMB().getRut() +"][BCI_INI]");
		}
		
		obtenerComision.setRutEmpresa(String.valueOf(clienteMB.getRut()) + "-" + clienteMB.getDigitoVerif());
		obtenerComision.setMonedaTransf(this.transferenciasRippleBackingMB.getMonedaRipple().getCodigoIso());
		obtenerComision.setMontoPago(this.transferenciasRippleBackingMB.getStrMontoATransferir());
		obtenerComision.setBicBanco(this.transferenciasRippleViewMB.getNodoRipple().getBic());
		
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "]["+ usuarioClienteModelMB.getUsuarioModelMB().getRut() + obtenerComision.toString()+"][BCI_FINOK]");
		}	
	}

	
	/**
	 * Generacion de objeto con los datos necesarios registrar la transferencia
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 07/03/2019, Danilo Dominguez (Everis) - Minheli Mejias (Ing. Soft. BCI): version inicial</li>
	 * </ul>
	 * </p>
     * @return RespuestaRegistroTransferenciaTO
	 * @throws GeneralException Excepcion de negocio.
	 * @since 1.0
	 */
	private RespuestaRegistroTransferenciaTO generarTransferencia() throws GeneralException {
		
		String nombreMetodo = "generarTransferencia";
		
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "]["+ usuarioClienteModelMB.getUsuarioModelMB().getRut() +"][BCI_INI]");
		}
		
		RegistrarTransferenciaTO regTransferencia = new RegistrarTransferenciaTO();
		regTransferencia.setRutEmpresa(String.valueOf(clienteMB.getRut()) + "-" + clienteMB.getDigitoVerif());
		regTransferencia.setNombreEmpresa(usuarioClienteModelMB.getClienteMB().getRazonSocial());
		regTransferencia.setMoneda(this.monedaRipple.getCodigoIso());
		regTransferencia.setCuentaDebito(getCuentaOrigen().getNumeroCuenta());
		
		this.setMontoATransferir(Double.valueOf(strMontoATransferir.replace(',', '.')));
		regTransferencia.setMontoPago(BigDecimal.valueOf(this.getMontoATransferir()));
		regTransferencia.setRemesa(this.getMotivoPago());
		regTransferencia.setMonedaComision(this.getMonedaOtraCuenta().getCodigoIso());
		regTransferencia.setCuentaComision(this.getCuentaCargoComision().getNumeroCuenta());
		regTransferencia.setCodIdentifBenef(this.getTransferenciaRipple().getDatosDestin().getReferencia());
		regTransferencia.setPaisBcoBenef(this.getPaisBcoBenef());
		regTransferencia.setCodigoCambio(this.getTransferenciaRipple().getDatosBanco().getCodigoCambio().split("-")[0].trim());
		regTransferencia.setKeyOperacion(this.getKey());
		regTransferencia.setRutUsuario(usuarioClienteModelMB.getUsuarioModelMB().getRut() + "-"
				+ usuarioClienteModelMB.getUsuarioModelMB().getDigitoVerif());
		regTransferencia.setIpMaquina("");
		regTransferencia.setCuentaDestino(this.getTransferenciaRipple().getDatosDestin().getNumeroCuenta());
		RespuestaRegistroTransferenciaTO respRegTransf = transferenciasRippleUtilityMB.registrarTransferencia(regTransferencia);
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "]["+ usuarioClienteModelMB.getUsuarioModelMB().getRut() +"][BCI_FINOK][" + StringUtil.contenidoDe(respRegTransf) + "]");
		}	
		
		return respRegTransf;
	}

	/**
	 * Generacion de objeto
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 11/03/2019, Danilo Dominguez (Everis) - Minheli Mejias (Ing. Soft. BCI): version inicial</li>
	 * </ul>
	 * </p>
     * @param transferenciaTO Entidad de negocio para transferencia.
     * @param numCorrelativoFbp numero correlativo en fbp.
	 * @throws GeneralException Excepcion de negocio.
	 * @since 1.0
	 */
	private void crearTransferenciaTO(TransferenciaTO transferenciaTO) throws GeneralException {
		String nombreMetodo = "crearTransferenciaTO";
		
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "]["+ usuarioClienteModelMB.getUsuarioModelMB().getRut() +"][BCI_INI]");
		}
		
		MonedasNodoTO monedasNodo = transferenciasRippleUtilityMB.obtenerMonedas();
		MonedasPermitidasTO moneda = (MonedasPermitidasTO) monedasNodo.getLstMonedas().get(0);

		transferenciaTO.setMontoTransferir(Double.valueOf(this.getStrMontoATransferir().replace(',', '.')));
		
		// Obtener beneficiarios y rescatar los datos del ya seleccionado
		ConsultaBeneficiariosRippleTO consulta = new ConsultaBeneficiariosRippleTO();
		consulta.setBicBanco(monedasNodo.getNodo().getBic());
		consulta.setMndaCuenta(moneda.getCodigoIso());
		consulta.setRutEmpresa(getRutEmpresa());
		List<BeneficiarioRippleTO> lstBenefsRipple = transferenciasRippleUtilityMB
				.obtenerBeneficiariosAutorizadosRipple(consulta);
		BeneficiarioRippleTO benefSeleccionado = null;
		
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[ "+nombreMetodo+" ] benefSeleccionado: " + (benefSeleccionado!=null?benefSeleccionado:"null"));
		}
		
		for (BeneficiarioRippleTO beneficiarioRippleTO : lstBenefsRipple) {
			if(beneficiarioRippleTO.getIdentificacion().equalsIgnoreCase(this.getTransferenciaRipple().getDatosDestin().getReferencia()))
				{
				benefSeleccionado = beneficiarioRippleTO;
				break;
			}
		}

		if (benefSeleccionado != null) {
			this.setPaisBcoBenef(benefSeleccionado.getPaisBanco());
			DetalleBeneficiarioRippleTO detalleBenefSelec = this.consultarDetalleBeneficiario(
					benefSeleccionado.getIdentificacion(), moneda.getCodigoIso(), benefSeleccionado.getPaisBanco());
			transferenciaTO.setTipoUsuario(transferenciasRippleUtilityMB.obtenerTipoUsuario());
			transferenciaTO.setModalidadOperacion("SHW");
			transferenciaTO.getBeneficiario().setNombre(detalleBenefSelec.getNombre());
			transferenciaTO.getBeneficiario().setNombre2(detalleBenefSelec.getNombreCont());
			transferenciaTO.getBeneficiario().setDireccion1(detalleBenefSelec.getDireccion());
			transferenciaTO.getBeneficiario().setDireccion2(detalleBenefSelec.getDireccionCont());
			transferenciaTO.getBeneficiario().setNombrePaisBeneficiario(detalleBenefSelec.getNombrePaisRes());
			transferenciaTO.getBeneficiario().setCiudad1(detalleBenefSelec.getInfoAdicional());
			transferenciaTO.getBeneficiario().setInformacionAdicionalDos(detalleBenefSelec.getInfoAdicionalCont());
			CuentaMonexTO cuentaDestino = new CuentaMonexTO();
			cuentaDestino.setNumeroCuenta(this.getTransferenciaRipple().getDatosDestin().getNumeroCuenta());
			transferenciaTO.getBeneficiario().setCuenta(cuentaDestino);
			transferenciaTO.setMontoTransferir(this.getMontoATransferir());
			transferenciaTO.getBeneficiario().setReferencia(this.getTransferenciaRipple().getDatosDestin().getReferencia());
			CuentaMonexTO cuentaOrigen = new CuentaMonexTO();
			cuentaOrigen.setNumeroCuenta(getCuentaOrigen().getNumeroCuenta());
			transferenciaTO.setCuentaOrigen(cuentaOrigen);
			this.transferenciasRippleViewMB.setTransferenciaSelected(transferenciaTO);
		
		} else {
			if (getLogger().isEnabledFor(Level.INFO)) {
				getLogger().info("[" + nombreMetodo + "] [Caso No se encontro Beneficiario]");
			}
		}
		
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "]["+ usuarioClienteModelMB.getUsuarioModelMB().getRut() +"][BCI_FINOK]");
		}
		
	}

	/**
	 * Consulta detalle del beneficiario.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 25/02/2019, Danilo Dominguez (Everis) - Minheli Mejias (Ing. Soft. BCI): version inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param identificadorBeneficiario Identificador Beneficiario.
	 * @param codMoneda                 Codigo de moneda.
	 * @param paisBanco                 Codigo de pais banco.
	 * @return DetalleBeneficiarioTO Entidad de negocio.
	 * @throws GeneralException Excepcion de negocio.
	 * @since 1.0
	 */
	private DetalleBeneficiarioRippleTO consultarDetalleBeneficiario(String identificadorBeneficiario, String codMoneda,
			String paisBanco) throws GeneralException {

		String nombreMetodo = "consultarDetalleBeneficiario";
		
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "]["+ usuarioClienteModelMB.getUsuarioModelMB().getRut() +"][BCI_INI]");
		}

		this.getTransferenciasRippleViewMB().setCodigoMoneda(codMoneda);
		this.getTransferenciasRippleViewMB().setCodigoPaisBanco(paisBanco);
		this.getTransferenciasRippleViewMB().setIdentificadorBeneficiario(identificadorBeneficiario);

		ConsultaDetalleBeneficiarioRippleTO datosConsulta = new ConsultaDetalleBeneficiarioRippleTO();
		datosConsulta.setRutEmpresa(clienteMB.getRut());
		datosConsulta.setDvEmpresa(clienteMB.getDigitoVerif());
		datosConsulta.setIdentificadorBeneficiario(identificadorBeneficiario);
		datosConsulta.setCodIso(codMoneda);
		datosConsulta.setPaisBanco(paisBanco);
		
		DetalleBeneficiarioRippleTO retorno = transferenciasRippleUtilityMB.obtenerDetalleBeneficiarioRipple(datosConsulta);
		
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "]["+ usuarioClienteModelMB.getUsuarioModelMB().getRut() +"][BCI_FINOK][" + StringUtil.contenidoDe(retorno) + "]");
		}
		
		return retorno;
	}

	/**
	 * Setea datos a transferenciaMonexTO para realizar la aceptacion de la
	 * transferencia.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 25/02/2019, Danilo Domínguez (Everis) - Minheli Mejias (Ing. Soft. BCI): version inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param transferenciaTO Entidad de negocio.
	 * @return TransferenciaMonexTO
	 * @throws Exception Excepcion general.
	 * @since 1.0
	 */
	private TransferenciaMonexTO getTransferenciaMonexTO(TransferenciaTO transferenciaTO) throws Exception {

		String nombreMetodo = "getTransferenciaMonexTO";

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "]["+ usuarioClienteModelMB.getUsuarioModelMB().getRut() +"][BCI_INI]");
		}
		TransferenciaMonexTO transferenciaMonex = new TransferenciaMonexTO();
		transferenciaMonex.setBancoBeneficiario(transferenciaTO.getBancoBeneficiario().getCodigoSwift());
		transferenciaMonex.setBicBancoIntermediario(transferenciaTO.getBancoIntermediario().getCodigoSwift());
		transferenciaMonex.setCodigoCambio(transferenciaTO.getCodigoCambio().getCodigo());
		transferenciaMonex.setCodigoIdentificador(transferenciaTO.getBeneficiario().getReferencia());
		transferenciaMonex.setCodigoProducto(
				TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "codigosMoneda", "codigoProducto"));
		transferenciaMonex.setCtaCte(transferenciaTO.getCuentaOrigen().getNumeroCuenta());
		transferenciaMonex.setCtacteDebito(transferenciaTO.getCuentaCargoComision().getNumeroCuenta());
		transferenciaMonex.setCuentaCorriente(transferenciaTO.getCuentaOrigen().getNumeroCuenta());
		transferenciaMonex.setDetalleGastos(transferenciaTO.getMotivoPago());
		transferenciaMonex.setEmail1(transferenciaTO.getBeneficiario().getMail());
		transferenciaMonex.setEmail2(transferenciaTO.getBeneficiario().getMail());
		transferenciaMonex.setEmail3(transferenciaTO.getBeneficiario().getMail());
		transferenciaMonex.setFechaOperacion(new Date());
		transferenciaMonex.setInformacionRemesa("");
		transferenciaMonex.setIpMaquina("");
		transferenciaMonex.setMonedaCtacte(transferenciaTO.getCuentaOrigen().getMoneda().getCodigo());
		transferenciaMonex.setMonedaNomina(transferenciaTO.getCuentaOrigen().getMoneda().getCodigo());
		transferenciaMonex.setMontoDebito(new BigDecimal(this.getMontoATransferir()));
		transferenciaMonex.setNombreBeneficiarioL1(transferenciaTO.getBeneficiario().getNombre());
		transferenciaMonex.setNombreBeneficiarioL2(transferenciaTO.getBeneficiario().getDireccion());
		transferenciaMonex.setNombreBeneficiarioL3(transferenciaTO.getBeneficiario().getDireccion());
		transferenciaMonex.setNombreBeneficiarioL4(transferenciaTO.getBeneficiario().getDireccion());
		transferenciaMonex.setNombreEmpresa(usuarioClienteModelMB.getClienteMB().getRazonSocial());
		transferenciaMonex.setNomDirBancoL1(transferenciaTO.getBancoBeneficiario().getNombreDireccion());
		transferenciaMonex.setNomDirBancoL2(transferenciaTO.getBancoBeneficiario().getNombreDireccion());
		transferenciaMonex.setNomDirBancoL3(transferenciaTO.getBancoBeneficiario().getNombreDireccion());
		transferenciaMonex.setNomDirBancoL4(transferenciaTO.getBancoBeneficiario().getNombreDireccion());
		transferenciaMonex.setNomDirIntermediarioL1(transferenciaTO.getBancoIntermediario().getNombreDireccion());
		transferenciaMonex.setNomDirIntermediarioL2(transferenciaTO.getBancoIntermediario().getNombreDireccion());
		transferenciaMonex.setNomDirIntermediarioL3(transferenciaTO.getBancoIntermediario().getNombreDireccion());
		transferenciaMonex.setNomDirIntermediarioL4(transferenciaTO.getBancoIntermediario().getNombreDireccion());
		transferenciaMonex.setNumeroCorrelativoFbp(transferenciaTO.getCorrelativoInterno());
		transferenciaMonex.setOrigenSolicitud(ORIGEN_SOLICITUD);
		transferenciaMonex.setPaisBeneficiario(transferenciaTO.getBeneficiario().getPaisCuenta().getCodigo());
		transferenciaMonex.setRutaPago(transferenciaTO.getBancoBeneficiario().getNumeroCuenta());
		transferenciaMonex.setRutaPagoIntermediario(transferenciaTO.getBancoIntermediario().getNumeroCuenta());
		transferenciaMonex.setRutEmpresa(usuarioClienteModelMB.getUsuarioModelMB().getRut() + "-"
				+ usuarioClienteModelMB.getUsuarioModelMB().getDigitoVerif());
		transferenciaMonex.setServicioSolicitado(SERVICIO_SOLICITADO_CUR);
		transferenciaMonex.setSystimestamp(transferenciaTO.getFechaActualizacion());
		transferenciaMonex.setTipoOperacion(
				TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, TRANSFERENCIA_PARAMETROS, "tipoOperacion"));
		transferenciaMonex.setUsuario(this.getUsuarioClienteModelMB().getUsuarioModelMB().getRut() + ""
				+ this.getUsuarioClienteModelMB().getUsuarioModelMB().getDigitoVerif());
		transferenciaMonex.setValutaPago(transferenciaTO.getValuta().getCodigo());
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[getTransferenciaMonexTO][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
					+ "] [BCI_FINOK]");
		}
		return transferenciaMonex;
	}

	/**
	 * Se genera TO CambiaEstadoTttfMonexTO.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 25/02/2019, Danilo Dominguez (Everis) - Minheli Mejias (Ing.Soft.BCI): version inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param tipUsr       Tipo de usuario.
	 * @param numCorr      Numero de correlativo.
	 * @param sysTimeStamp Fecha de transaccion.
	 * @return CambiaEstadoTttfMonexTO
	 * @since 1.0
	 */
	private CambiaEstadoTttfMonexTO getCambiaEstadoTttfMonexTO(String tipUsr, int numCorr, String sysTimeStamp) {
		
		String nombreMetodo = "getCambiaEstadoTttfMonexTO";
		
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "]["+ usuarioClienteModelMB.getUsuarioModelMB().getRut() +"][BCI_INI]");
		}
		
		CambiaEstadoTttfMonexTO cambiaEstadoTttfMonexTO = new CambiaEstadoTttfMonexTO();
		cambiaEstadoTttfMonexTO.setOrigenSolicitud(ORIGEN_SOLICITUD);
		cambiaEstadoTttfMonexTO.setTipoUsuario(tipUsr);
		cambiaEstadoTttfMonexTO.setNumeroCorrelativo(numCorr);
		cambiaEstadoTttfMonexTO.setSystimestamp(sysTimeStamp);
		cambiaEstadoTttfMonexTO.setUsuarioEvento(this.getUsuarioClienteModelMB().getUsuarioModelMB().getRut() + ""
				+ this.getUsuarioClienteModelMB().getUsuarioModelMB().getDigitoVerif());
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "]["+ usuarioClienteModelMB.getUsuarioModelMB().getRut() +"][BCI_FINOK][" + StringUtil.contenidoDe(cambiaEstadoTttfMonexTO) + "]");
		}
		return cambiaEstadoTttfMonexTO;
	}

	/**
	 * Metodo encargado de crear una instacia del EJB ServiciosDePagosMasivos.
	 *
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 11/03/2019, Danilo Dominguez (Everis) - Minheli Mejias (Ing. Soft. BCI): version inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @since 1.10
	 * @return Instancia del EJB ServiciosDePagosMasivos.
	 * @throws GeneralException en caso de error.
	 */
	private ServiciosDePagosMasivos crearEJBServiciosDePagosMasivos() throws GeneralException {
		
		String nombreMetodo = "crearEJBServiciosDePagosMasivos";
		
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "]["+ usuarioClienteModelMB.getUsuarioModelMB().getRut() +"][BCI_INI]");
		}
		
		try {
			
			EnhancedServiceLocator locator = EnhancedServiceLocator.getInstance();
			ServiciosDePagosMasivosHome serviciosDePagosMasivosHome = (ServiciosDePagosMasivosHome) locator
					.getGenericService(
							"wcorp.aplicaciones.productos.servicios.pagos.pagosmasivos.ServiciosDePagosMasivos",
							ServiciosDePagosMasivosHome.class);
			
			if (getLogger().isEnabledFor(Level.INFO)) {
				getLogger().info("[" + nombreMetodo + "]["+ usuarioClienteModelMB.getUsuarioModelMB().getRut() +"][BCI_FINOK] ");
			}
			
			return serviciosDePagosMasivosHome.create();
			
		} catch (Exception e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("["+nombreMetodo+"][BCI_FINEX][Exception][" + e.getMessage() + "]", e);
			}

			throw new GeneralException("ESPECIAL", ErroresUtil.extraeStackTrace(e));
		}
		
	}

	/**
	 * Journaliza operaciones de transfer monex.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 11/03/2019, Danilo Domínguez (Everis) - Minheli Mejias (Ing. Soft.BCI): version inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param codigoEvento    Codigo de evento (SIMULA | FIRMA | CREAR | ELIMINA | PAGO).
	 * @param subCodigoEvento Sub codigo evento (OK | NOK).
	 * @param idProducto      Id de producto (TRFRPL).
	 * @param estadoEvento    Estado Evento (P | R).
	 * @param monto           Monto de transferencia.
	 * @param destinatario    Destinatario transferencia.
	 * @param numeroOperacion Numero de operacion.
	 * @since 1.0
	 */
	private void journalizar(String codigoEvento, String subCodigoEvento, String idProducto, String estadoEvento,
			String monto, String destinatario, String numeroOperacion) {
		
		String nombreMetodo = "journalizar";
		
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "]["+ usuarioClienteModelMB.getUsuarioModelMB().getRut() +"][BCI_INI]");
		}
		try {
			String campoVariable = null;
			if (monto != null && destinatario != null) {
				campoVariable = monto + "|" + destinatario;
			}
			HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
					.getRequest();
			String idMedio = request.getHeader("X-FORWARDED-FOR");
			
			if (getLogger().isEnabledFor(Level.DEBUG)) {
				getLogger().debug("[ "+nombreMetodo+" ] idMedio: " + (idMedio!=null?idMedio:"null"));
			}
			
			if (idMedio == null) {
				idMedio = request.getRemoteAddr();
			}
			
			Eventos evento = new Eventos();
			evento.setCodEventoNegocio(codigoEvento);
			evento.setSubCodEventoNegocio(subCodigoEvento);
			evento.setIdProducto(idProducto);
			evento.setEstadoEventoNegocio(estadoEvento);
			evento.setRutCliente(String.valueOf(this.getClienteMB().getRut()));
			evento.setDvCliente(String.valueOf(this.getClienteMB().getDigitoVerif()));
			evento.setRutOperadorCliente(String.valueOf(usuarioClienteModelMB.getUsuarioModelMB().getRut()));
			evento.setDvOperadorCliente(String.valueOf(usuarioClienteModelMB.getUsuarioModelMB().getDigitoVerif()));
			evento.setIdCanal(this.getUsuarioClienteModelMB().getUsuarioModelMB().getSesionMB().getCanalId());
			evento.setIdMedio(idMedio);
			evento.setCampoVariable(campoVariable);
			evento.setClavePrincipal(numeroOperacion);

			this.getJournalUtilityMB().journalizar(evento);
			
		} catch (Exception e) {
			
			if(getLogger().isEnabledFor(Level.WARN)){
				getLogger().warn("["+nombreMetodo+"] ["+usuarioClienteModelMB.getUsuarioModelMB().getRut()+"] [Exception]" + e.getMessage(), e);
			}
			
		}

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "]["+ usuarioClienteModelMB.getUsuarioModelMB().getRut() +"][BCI_FINOK]");
		}		
	}

	/**
	 * Acceso monedas otras cuentas.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 11/03/2019, Danilo Dominguez (Everis) - Minheli Mejias (Ing. Soft. BCI): version inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @return List<Moneda1a1TO>.
	 * @since 1.0
	 */
	public List<Moneda1a1TO> getMonedasOtrasCuentas() {
		
		String nombreMetodo = "getMonedasOtrasCuentas";
		
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "]["+ usuarioClienteModelMB.getUsuarioModelMB().getRut() +"][BCI_INI]");
		}
		
		try {
			
			if (getLogger().isEnabledFor(Level.DEBUG)) {
				getLogger().debug("["+ nombreMetodo +"] ["+usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ "] transferenciasRippleViewMB.getMonedasOtrasCuentas()");
			}
			
			if (transferenciasRippleViewMB.getMonedasOtrasCuentas() == null
					|| transferenciasRippleViewMB.getMonedasOtrasCuentas().isEmpty()) {
				ConsultaMonedaTO consultaMonedaTO = new ConsultaMonedaTO();

				consultaMonedaTO.setCodigoProducto(
						TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "codigosMoneda", "codigoProductoCVD"));
				consultaMonedaTO.setOrigenSolicitud(
						TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "codigosMoneda", "origenSolicitud"));
				transferenciasRippleViewMB
						.setMonedasOtrasCuentas(transferenciasRippleUtilityMB.obtenerMonedas(consultaMonedaTO));
			}
		} catch (GeneralException e) {
			if(getLogger().isEnabledFor(Level.WARN)){
				getLogger().warn("["+nombreMetodo+"] ["+usuarioClienteModelMB.getUsuarioModelMB().getRut()+"] [Exception]" + e.getMessage(), e);
			}
		}
		
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "]["+ usuarioClienteModelMB.getUsuarioModelMB().getRut() +"][BCI_FINOK]");
		}
		
		return transferenciasRippleViewMB.getMonedasOtrasCuentas();
	}

	/**
	 * Carga informaci�n necesaria al cambiar el combo moneda cargo comisi�n.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 25/02/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft.BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * @param ev Evento Ajax.
	 * @return String
	 * @since 1.0
	 */
	public String cambiaSeleccionOtraMoneda(AjaxBehaviorEvent ev) {
		
		String nombreMetodo = "cambiaSeleccionOtraMoneda";
		
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "]["+ usuarioClienteModelMB.getUsuarioModelMB().getRut() +"][BCI_INI]");
		}
		
		this.init = true;
		UISelectOne selectMoneda = (UISelectOne) ev.getSource();
		String valor = (String) selectMoneda.getSubmittedValue();
		this.monedaOtraCuenta.setCodigoIso(valor);
		this.cuentaCargoComision.setNumeroCuenta(null);
		this.buscaOtraMonedaSeleccionada();
		this.transferenciasRippleViewMB.getCuentasComision().clear();
		
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[ "+nombreMetodo+" ] valor: " + (valor!=null?valor:"null"));
		}	
		
		if (valor != null && StringUtils.isNotEmpty(valor) && !"0".equals(valor)) {
			List<CuentaMonexTO> cuentasComision = this.transferenciasRippleUtilityMB
					.getCuentasPorCodigoMoneda(clienteMB.getRut(), clienteMB.getDigitoVerif(), valor);
			if (null == cuentasComision || cuentasComision.isEmpty()) {
				CuentaMonexTO cuentaMonexTO = new CuentaMonexTO();
				cuentaMonexTO.setMoneda(new CodigoValor(valor, ""));
				cuentaMonexTO.setNumeroCuenta(TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "mensajesError",
						"noPoseeCuentaMensaje"));
				this.transferenciasRippleViewMB.getCuentasComision().add(cuentaMonexTO);
			} else {
				transferenciasRippleViewMB.setCuentasComision(cuentasComision);
			}

		}

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "]["+ usuarioClienteModelMB.getUsuarioModelMB().getRut() +"][BCI_FINOK][" + StringUtil.contenidoDe(null) + "]");
		}
		
		return null;
	}

	/**
	 * Busca dentro de las monedas la moneda seleccionada.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 25/02/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft.
	 * BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @since 1.0
	 */
	private void buscaOtraMonedaSeleccionada() {
		
		String nombreMetodo = "buscaOtraMonedaSeleccionada";
		
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "]["+ usuarioClienteModelMB.getUsuarioModelMB().getRut() +"][BCI_INI]");
		}
		
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[ "+nombreMetodo+" ] transferenciasRippleViewMB.getMonedasOtrasCuentas() ");
		}	
		
		for (Moneda1a1TO mda : transferenciasRippleViewMB.getMonedasOtrasCuentas()) {
			if (mda.getCodigoIso().equals(this.getMonedaOtraCuenta().getCodigoIso())) {
				this.setMonedaOtraCuenta(mda);
				break;
			}
		}
		
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "]["+ usuarioClienteModelMB.getUsuarioModelMB().getRut() +"][BCI_FINOK]");
		}
	}

	/**
	 * M�todo Para validar Monto Maximo.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 01/03/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft. BCI): Versi�n inicial.</li>
	 * </ul>
	 * </p>
	 * 
	 * @return String
	 * @throws GeneralException Excepcion de negocio.
	 * @since 1.6
	 */
	private boolean validaMonto() throws GeneralException {
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[validaMonto][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "][BCI_INI]");
		}

		String monedaElegida = "";
		monedaElegida = monedaRipple.getCodigoIso();

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[validaMonto] moneda Elegida: " + monedaElegida);
		}

		transferenciasRippleViewMB.setMonedasNodo(transferenciasRippleUtilityMB.obtenerMonedas());

		if (monedaElegida != null && !monedaElegida.equals("")) {
			for (MonedasPermitidasTO monedasPermitidas : transferenciasRippleViewMB.getMonedasRipple()) {
				if (monedasPermitidas.getCodigoIso().equalsIgnoreCase(monedaElegida)) {
					montoMax = monedasPermitidas.getMtoMaximo().doubleValue();
				}
			}
		} else {
			if (getLogger().isEnabledFor(Level.INFO)) {
				getLogger()
						.info("[validaMonto][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "] [BCI_FINOK]");
			}
			return false;
		}

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[validaMonto] monto Maximo:" + montoMax);
		}
		Double montoFloat = NUM_CERO_COMA_CERO;
		try {

			if (getLogger().isEnabledFor(Level.DEBUG)) {
				getLogger().debug("[validaMonto] str Monto A Transferir: " + strMontoATransferir);
			}
			String montoMaxString = strMontoATransferir;
			montoMaxString = montoMaxString.replace(".", "");
			montoMaxString = montoMaxString.replace(",", ".");
			montoFloat = Double.parseDouble(montoMaxString);
			if (getLogger().isEnabledFor(Level.DEBUG)) {
				getLogger().debug("[validaMonto] montoFloat: " + montoFloat);
			}
		} catch (Exception e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[validaMonto][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ "][BCI_FINEX] [Exception]: ERROR [" + montoFloat + " " + e.getMessage(), e);
			}
			return false;
		}
		if (montoFloat > montoMax) {
			if (getLogger().isEnabledFor(Level.DEBUG)) {
				getLogger().debug("[validaMonto]: ERROR Monto excede el maximo[" + montoFloat + "]");
			}
			if (getLogger().isEnabledFor(Level.INFO)) {
				getLogger()
						.info("[validaMonto][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "] [BCI_FINOK]");
			}
			return false;
		} else {
			if (getLogger().isEnabledFor(Level.INFO)) {
				getLogger()
						.info("[validaMonto][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "] [BCI_FINOK]");
			}
			return true;
		}
	}

	/**
	 * M�todo encargado de realizar la confirmaci�n de la transferencia.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 20/02/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @return String
	 * @throws GeneralException Excepcion de negocio.
	 * @since 1.0
	 */
	public String confirmarTransferenciaRipple() throws GeneralException {
		
		final String nombreMetodo = "confirmarTransferenciaRipple";
		final String identificador = (usuarioClienteModelMB != null
				&& usuarioClienteModelMB.getUsuarioModelMB() != null)
						? String.valueOf(usuarioClienteModelMB.getUsuarioModelMB().getRut())
						: "";

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "][" + identificador + "] [BCI_INI]");
		}

		ObtenerComisionTO obtenerComision = new ObtenerComisionTO();
		generarObtenerComision(obtenerComision);

		this.setComisionRipple(this.transferenciasRippleUtilityMB.obtenerComisiones(obtenerComision));
		this.setKey(this.getComisionRipple().getKey());

		String rutEmpresa = getRutEmpresa();
		this.cuentaComisionTo = transferenciasRippleUtilityMB.obtenerCuentaDeCargo(rutEmpresa);

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[" + nombreMetodo + "][" + identificador + "] [rutEmpresa] ["
					+ StringUtil.contenidoDe(rutEmpresa) + "]");
			getLogger().debug(
					"[" + nombreMetodo + "][" + identificador + "] [init] [" + StringUtil.contenidoDe(init) + "]");
			getLogger().debug("[" + nombreMetodo + "][" + identificador + "] [cuentaComisionTo] ["
					+ StringUtil.contenidoDe(cuentaComisionTo) + "]");
		}

		if (!this.init && this.cuentaComisionTo != null) {
			CuentaMonexTO cuenta = new CuentaMonexTO();
			cuenta.setMoneda(new CodigoValor(cuentaComisionTo.getMonedaCtacte(), cuentaComisionTo.getMonedaCtacte()));
			cuenta.setNumeroCuenta(cuentaComisionTo.getCtaDebito());

			if (getLogger().isEnabledFor(Level.DEBUG)) {
				getLogger().debug("[" + nombreMetodo + "][" + identificador + "] [cuenta] ["
						+ StringUtil.contenidoDe(cuenta) + "]");
			}

			this.monedaOtraCuenta.setCodigoIso(this.cuentaComisionTo.getMonedaCtacte());
			this.transferenciasRippleViewMB
					.setCuentasComision(this.transferenciasRippleUtilityMB.getCuentasPorCodigoMoneda(clienteMB.getRut(),
							clienteMB.getDigitoVerif(), this.monedaOtraCuenta.getCodigoIso()));
			this.cuentaCargoComision.setNumeroCuenta(cuentaComisionTo.getCtaDebito());
			
			
				
			
			if (getLogger().isEnabledFor(Level.DEBUG)) {
				getLogger().debug(
						"[" + nombreMetodo + "][" + identificador + "] [transferenciasMonexViewMB.cuentasComision] ["
								+ StringUtil.contenidoDe(transferenciasRippleViewMB.getCuentasComision()) + "]");
				getLogger().debug("[" + nombreMetodo + "][" + identificador + "] [cuentaCargoComision] ["
						+ StringUtil.contenidoDe(cuentaCargoComision) + "]");
			}
		}
		
		String retorno = "confirmarTransferenciasRipple";
		
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "]["+ usuarioClienteModelMB.getUsuarioModelMB().getRut() +"][BCI_FINOK][" + retorno + "]");
		}	
		
		return retorno;
	}
	
	/**
	 * Metodo encargado de ingresar y aceptar la transferencia.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 07/03/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @return String
	 * @since 1.0
	 */
	public String aceptarTransferenciaRipple() {
		final String nombreMetodo = "aceptarTransferenciaRipple";
		final String identificador = (usuarioClienteModelMB != null
				&& usuarioClienteModelMB.getUsuarioModelMB() != null)
						? String.valueOf(usuarioClienteModelMB.getUsuarioModelMB().getRut())
						: "";

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "][" + identificador + "] [BCI_INI]");
		}
		
		TransferenciaTO transferenciaTO = new TransferenciaTO();
		
		try {
			if (getLogger().isEnabledFor(Level.DEBUG)) {
				getLogger().debug("[" + nombreMetodo + "][" + identificador
						+ "] [transferenciasMonexViewMB.transferenciaSelected] ["
						+ StringUtil.contenidoDe(transferenciasRippleViewMB.getTransferenciaSelected()) + "]");
			}

			RespuestaHorarioTO validarHorario = this.transferenciasRippleUtilityMB.consultarLimiteHorarioIngreso();
			if (getLogger().isEnabledFor(Level.DEBUG)) { 
			  	getLogger().debug("[" +nombreMetodo + "][" + identificador + "] [validarHorario] [" +StringUtil.contenidoDe(validarHorario) + "]"); 
			}
			
			if(validarHorario.getIndicadorResultado() != 0){
				if (getLogger().isEnabledFor(Level.INFO)) { 
					getLogger().info("[" +nombreMetodo + "] [" + identificador + "] [BCI_FINOK] Retorno nulo."); 
				}
				FacesMessage mensaje = new FacesMessage(validarHorario.getMensajeResultado());
				mensaje.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage("segundaClave", mensaje); 
				return null; 	
				}
			
			crearTransferenciaTO(transferenciaTO);
			
			RespuestaRegistroTransferenciaTO respRegistro = generarTransferencia();
			if(respRegistro.getIndicadorResultado() != 0){
				this.journalizar(TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "journalizarEvento", "crea"),
						TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "journalizarSubEvento", "nok"),
						TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "journalizarProducto", "producto"),
						TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "journalizarEstado", "eventoRechazado"),
						String.valueOf(transferenciaTO.getMontoTransferir()),
						transferenciaTO.getBeneficiario().getNombre(),
						String.valueOf(respRegistro.getNumeroCorrelativo()));
				
				//LOGICA PROVISORIA, SE TIENE QUE DESPLEGAR MENSAJE EN FUNCION DEL TIPO DE ERROR (1 O 9)
				if (getLogger().isEnabledFor(Level.INFO)) { 
					getLogger().info("[" +nombreMetodo + "] [" + identificador + "] [BCI_FINOK] Retorno nulo."); 
				}
				FacesMessage mensaje = new FacesMessage(validarHorario.getMensajeResultado());
				mensaje.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage("segundaClave", mensaje); 
				return null; 
				//************************************************************************
			}
			
			this.journalizar(TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "journalizarEvento", "crea"),
					TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "journalizarSubEvento", "ok"),
					TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "journalizarProducto", "producto"),
					TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "journalizarEstado", "eventoProcesado"),
					String.valueOf(transferenciaTO.getMontoTransferir()),
					transferenciaTO.getBeneficiario().getNombre(),
					String.valueOf(respRegistro.getNumeroCorrelativo()));
					
			transferenciaTO.setCorrelativoInterno(respRegistro.getNumeroCorrelativo());
			transferenciaTO.setFechaActualizacion(respRegistro.getSystimestamp());

		} catch (TtffException e1) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[" + nombreMetodo + "][" + identificador + "] [BCI_FINEX][TtffException]["
						+ e1.getMessage() + "]", e1);
			}

			mbErrorUtilityMB.setMensajeError(TablaValores.getValor("errores.codigos", "BADMSG", "Desc"));
			mbErrorUtilityMB.setOcultaBotonVolver(true);
			return mbErrorUtilityMB.obtenerPaginaError();
		} catch (GeneralException e1) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[" + nombreMetodo + "][" + identificador + "] [BCI_FINEX][GeneralException]["
						+ e1.getMessage() + "]", e1);
			}

			mbErrorUtilityMB.setMensajeError(TablaValores.getValor("errores.codigos", "BADMSG", "Desc"));
			mbErrorUtilityMB.setOcultaBotonVolver(true);
			return mbErrorUtilityMB.obtenerPaginaError();
		}

		this.transferenciasRippleViewMB.setPedienteFirma(true);
		DatosAutorizacionTO autorizacionTO = this.transferenciasRippleUtilityMB.getDatosAutorizacionTO();
		TransferenciaMonexTO transferenciaMonexTO = null;
		Boolean esPermitido;

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[" + nombreMetodo + "][" + identificador + "] [autorizacionTO] ["
					+ StringUtil.contenidoDe(autorizacionTO) + "]");
		}

		try {
			esPermitido = autorizadorUtilityMB.esPermitido(VISTA, ACCION);//Se tiene que modificar la variable VISTA, por una asociada al proyecto
			
			if (getLogger().isEnabledFor(Level.DEBUG)) {
				getLogger().debug("[" + nombreMetodo + "][" + identificador + "] [esPermitido] ["
						+ StringUtil.contenidoDe(esPermitido) + "]");
			}
		} catch (Exception e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error(
						"[" + nombreMetodo + "][" + identificador + "] [BCI_FINEX][Exception][" + e.getMessage() + "]",
						e);
			}

			mbErrorUtilityMB.setOcultaBotonVolver(true);
			mbErrorUtilityMB.setMensajeError(TablaValores.getValor("errores.codigos", "BADMSG", "Desc"));
			return mbErrorUtilityMB.obtenerPaginaError();
		}
		try {
			transferenciaMonexTO = getTransferenciaMonexTO(transferenciasRippleViewMB.getTransferenciaSelected());

			if (getLogger().isEnabledFor(Level.DEBUG)) {
				getLogger().debug("[" + nombreMetodo + "][" + identificador + "] [transferenciaMonexTO] ["
						+ StringUtil.contenidoDe(transferenciaMonexTO) + "]");
			}
		} catch (Exception e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error(
						"[" + nombreMetodo + "][" + identificador + "] [BCI_FINEX][Exception][" + e.getMessage() + "]",
						e);
			}

			mbErrorUtilityMB.setOcultaBotonVolver(true);
			mbErrorUtilityMB.setMensajeError(TablaValores.getValor("errores.codigos", "BADMSG", "Desc"));
			return mbErrorUtilityMB.obtenerPaginaError();
		}
		try {

			if (getLogger().isEnabledFor(Level.DEBUG)) {
				getLogger().debug("[" + nombreMetodo + "][" + identificador
						+ "] [transferenciasMonexUtilityMB.obtenerTipoUsuario] ["
						+ StringUtil.contenidoDe(transferenciasRippleUtilityMB.obtenerTipoUsuario()) + "]");
				getLogger().debug("[" + nombreMetodo + "][" + identificador + "] [transferenciaMonexTO] ["
						+ StringUtil.contenidoDe(transferenciaMonexTO) + "]");
			}

			CambiaEstadoTttfMonexTO cambiaEstadoTttfMonexTO = getCambiaEstadoTttfMonexTO(
					transferenciasRippleUtilityMB.obtenerTipoUsuario(), transferenciaMonexTO.getNumeroCorrelativoFbp(),
					transferenciaMonexTO.getSystimestamp());

			if (getLogger().isEnabledFor(Level.DEBUG)) {
				getLogger().debug("[" + nombreMetodo + "][" + identificador + "] [cambiaEstadoTttfMonexTO] ["
						+ StringUtil.contenidoDe(cambiaEstadoTttfMonexTO) + "]");
			}
			
			if (esPermitido.booleanValue()) {
				//Flujo Apoderado
				
				firmarTransferencia(transferenciaTO, transferenciaMonexTO, this.monedaRipple.getCodigoIso());
				
				
			} else {
				//Flujo Operador
				
			}
		
		} catch (NegocioException e) {

			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[" + nombreMetodo + "][" + identificador + "] [BCI_FINEX][GeneralException]["+ e.getMessage() + "]", e);
			}

			if (esPermitido.booleanValue()) {

				if (getLogger().isEnabledFor(Level.DEBUG)) {
					getLogger().debug("[" + nombreMetodo + "][" + identificador
							+ "] [JournalTransferMonex.CODIGO_EVENTO_TRX_LINEA] ["
							+ StringUtil.contenidoDe(JournalTransferMonex.CODIGO_EVENTO_TRX_LINEA.getValue()) + "]");
					getLogger().debug("[" + nombreMetodo + "][" + identificador
							+ "] [JournalTransferMonex.SUBCODIGO_EVENTO_ERROR] ["
							+ StringUtil.contenidoDe(JournalTransferMonex.SUBCODIGO_EVENTO_ERROR.getValue()) + "]");
					getLogger()
							.debug("[" + nombreMetodo + "][" + identificador + "] [JournalTransferMonex.ID_PRODUCTO] ["
									+ StringUtil.contenidoDe(JournalTransferMonex.ID_PRODUCTO.getValue()) + "]");
					getLogger().debug("[" + nombreMetodo + "][" + identificador + "] [JournalTransferMonex.ESTADO_R] ["
							+ StringUtil.contenidoDe(JournalTransferMonex.ESTADO_R.getValue()) + "]");
					getLogger().debug(
							"[" + nombreMetodo + "][" + identificador + "] [JournalTransferMonex.NUMERO_OPERACION] ["
									+ StringUtil.contenidoDe(JournalTransferMonex.NUMERO_OPERACION.getValue()) + "]");
				}

				this.journalizar(JournalTransferMonex.CODIGO_EVENTO_TRX_LINEA.getValue(),
						JournalTransferMonex.SUBCODIGO_EVENTO_ERROR.getValue(),
						JournalTransferMonex.ID_PRODUCTO.getValue(), JournalTransferMonex.ESTADO_R.getValue(), null,
						null, JournalTransferMonex.NUMERO_OPERACION.getValue());
			} else {

				if (getLogger().isEnabledFor(Level.DEBUG)) {
					getLogger().debug("[" + nombreMetodo + "][" + identificador
							+ "] [JournalTransferMonex.CODIGO_EVENTO_MODIFICAR] ["
							+ StringUtil.contenidoDe(JournalTransferMonex.CODIGO_EVENTO_MODIFICAR.getValue()) + "]");
					getLogger().debug("[" + nombreMetodo + "][" + identificador
							+ "] [JournalTransferMonex.SUBCODIGO_EVENTO_ERROR] ["
							+ StringUtil.contenidoDe(JournalTransferMonex.SUBCODIGO_EVENTO_ERROR.getValue()) + "]");
					getLogger()
							.debug("[" + nombreMetodo + "][" + identificador + "] [JournalTransferMonex.ID_PRODUCTO] ["
									+ StringUtil.contenidoDe(JournalTransferMonex.ID_PRODUCTO.getValue()) + "]");
					getLogger().debug("[" + nombreMetodo + "][" + identificador + "] [JournalTransferMonex.ESTADO_R] ["
							+ StringUtil.contenidoDe(JournalTransferMonex.ESTADO_R.getValue()) + "]");
					getLogger().debug(
							"[" + nombreMetodo + "][" + identificador + "] [JournalTransferMonex.NUMERO_OPERACION] ["
									+ StringUtil.contenidoDe(JournalTransferMonex.NUMERO_OPERACION.getValue()) + "]");
				}

				this.journalizar(JournalTransferMonex.CODIGO_EVENTO_MODIFICAR.getValue(),
						JournalTransferMonex.SUBCODIGO_EVENTO_ERROR.getValue(),
						JournalTransferMonex.ID_PRODUCTO.getValue(), JournalTransferMonex.ESTADO_R.getValue(), null,
						null, JournalTransferMonex.NUMERO_OPERACION.getValue());
			}
			mbErrorUtilityMB.setOcultaBotonVolver(true);
			mbErrorUtilityMB.setMensajeError(TablaValores.getValor("errores.codigos", "EXP-015", "Desc"));
			return mbErrorUtilityMB.obtenerPaginaError();
		} catch (Exception e) {

			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[" + nombreMetodo + "][" + identificador + "] [BCI_FINEX][GeneralException]["
						+ e.getMessage() + "]", e);
			}

			if (esPermitido.booleanValue()) {

				if (getLogger().isEnabledFor(Level.DEBUG)) {
					getLogger().debug("[" + nombreMetodo + "][" + identificador
							+ "] [JournalTransferMonex.CODIGO_EVENTO_TRX_LINEA] ["
							+ StringUtil.contenidoDe(JournalTransferMonex.CODIGO_EVENTO_TRX_LINEA.getValue()) + "]");
					getLogger().debug("[" + nombreMetodo + "][" + identificador
							+ "] [JournalTransferMonex.SUBCODIGO_EVENTO_ERROR] ["
							+ StringUtil.contenidoDe(JournalTransferMonex.SUBCODIGO_EVENTO_ERROR.getValue()) + "]");
					getLogger()
							.debug("[" + nombreMetodo + "][" + identificador + "] [JournalTransferMonex.ID_PRODUCTO] ["
									+ StringUtil.contenidoDe(JournalTransferMonex.ID_PRODUCTO.getValue()) + "]");
					getLogger().debug("[" + nombreMetodo + "][" + identificador + "] [JournalTransferMonex.ESTADO_R] ["
							+ StringUtil.contenidoDe(JournalTransferMonex.ESTADO_R.getValue()) + "]");
					getLogger().debug(
							"[" + nombreMetodo + "][" + identificador + "] [JournalTransferMonex.NUMERO_OPERACION] ["
									+ StringUtil.contenidoDe(JournalTransferMonex.NUMERO_OPERACION.getValue()) + "]");
				}

				this.journalizar(JournalTransferMonex.CODIGO_EVENTO_TRX_LINEA.getValue(),
						JournalTransferMonex.SUBCODIGO_EVENTO_ERROR.getValue(),
						JournalTransferMonex.ID_PRODUCTO.getValue(), JournalTransferMonex.ESTADO_R.getValue(), null,
						null, JournalTransferMonex.NUMERO_OPERACION.getValue());
			} else {

				if (getLogger().isEnabledFor(Level.DEBUG)) {
					getLogger().debug("[" + nombreMetodo + "][" + identificador
							+ "] [JournalTransferMonex.CODIGO_EVENTO_MODIFICAR] ["
							+ StringUtil.contenidoDe(JournalTransferMonex.CODIGO_EVENTO_MODIFICAR.getValue()) + "]");
					getLogger().debug("[" + nombreMetodo + "][" + identificador
							+ "] [JournalTransferMonex.SUBCODIGO_EVENTO_ERROR] ["
							+ StringUtil.contenidoDe(JournalTransferMonex.SUBCODIGO_EVENTO_ERROR.getValue()) + "]");
					getLogger()
							.debug("[" + nombreMetodo + "][" + identificador + "] [JournalTransferMonex.ID_PRODUCTO] ["
									+ StringUtil.contenidoDe(JournalTransferMonex.ID_PRODUCTO.getValue()) + "]");
					getLogger().debug("[" + nombreMetodo + "][" + identificador + "] [JournalTransferMonex.ESTADO_R] ["
							+ StringUtil.contenidoDe(JournalTransferMonex.ESTADO_R.getValue()) + "]");
					getLogger().debug(
							"[" + nombreMetodo + "][" + identificador + "] [JournalTransferMonex.NUMERO_OPERACION] ["
									+ StringUtil.contenidoDe(JournalTransferMonex.NUMERO_OPERACION.getValue()) + "]");
				}

				this.journalizar(JournalTransferMonex.CODIGO_EVENTO_MODIFICAR.getValue(),
						JournalTransferMonex.SUBCODIGO_EVENTO_ERROR.getValue(),
						JournalTransferMonex.ID_PRODUCTO.getValue(), JournalTransferMonex.ESTADO_R.getValue(), null,
						null, JournalTransferMonex.NUMERO_OPERACION.getValue());
			}
			mbErrorUtilityMB.setOcultaBotonVolver(true);
			mbErrorUtilityMB.setMensajeError(TablaValores.getValor("errores.codigos", "BADMSG", "Desc"));
			return mbErrorUtilityMB.obtenerPaginaError();
		}

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "] [" + identificador + "] [BCI_FINOK]");
		}

		this.transferenciasRippleViewMB.setConfirmacion(CONFIRMACION);
        FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put(TRANSFERENCIA, transferenciasRippleViewMB.getTransferenciaSelected());

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info(
					"[" + nombreMetodo + "] [" + identificador + "] [BCI_FINOK][comprobanteTransferenciasMonex.jsf]");
		}

		return COMPROBANTE_TRANSFERENCIA_RIPPLE;
	}
	
	
	/**
	 * M�etodo encargado la logica para realizar la firma de una transferencia
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 21/03/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * @param transferenciaTO Datos de trnasferencia
	 * @param transferenciaMonexTO Entidad de transferencia.
     * @return String
	 * @since 1.0
	 */
	public void firmarTransferencia(TransferenciaTO transferenciaTO,TransferenciaMonexTO transferenciaMonexTO, String codigoMoneda){
		final String identificador = (usuarioClienteModelMB != null 
				&& usuarioClienteModelMB.getUsuarioModelMB() != null)
						? String.valueOf(usuarioClienteModelMB.getUsuarioModelMB().getRut())
						: "";
		try{
			boolean segundaClaveValida = false;
	
			if (segundaClave != null) { 
				segundaClaveValida = segundaClave.verificarAutenticacion(); 
			} else {
			  segundaClaveMB.setServicio(SERVICIO_SEGUNDA_CLAVE); 
			  CamposDeLlaveTO camposLlave = null; 
			  segundaClaveMB.validarSegundaClave(segundaClaveValor,camposLlave); 
			  segundaClaveValida = true; 
			}
			 
			if (getLogger().isEnabledFor(Level.DEBUG)) {
				getLogger().debug("[" + FIRMAR_TRANSFERENCIA + "][" + identificador + "] [segundaClaveValida] ["
						+ segundaClaveValida + "]");
			}
	
			if (segundaClaveValida) {
				//Validado multipass ingresado
				boolean esApoderadoReal = autorizadorUtilityMB.esPermitido("validacionFirmaYPoderes", ACCION);
				String perfil = usuarioClienteModelMB.getUsuarioModelMB().getPerfil();
				String tipoUsu = this.transferenciasRippleUtilityMB.obtenerTipoUsuario();
				String perfilValida = TablaValores.getValor(ARCH_PARAM_VAL, "tipoPerfilValido", "Desc");
				String tipoUsuarioValido = TablaValores.getValor(ARCH_PARAM_VAL, "tipoUsuarioValido", "Desc");
				String flagvalidarRut50MM = TablaValores.getValor(ARCH_PARAM_VAL, "flagValidaRutMenor50MM", "flag");
	
				if (getLogger().isEnabledFor(Level.DEBUG)) {
					getLogger().debug("[" + FIRMAR_TRANSFERENCIA + "] perfilValida[" + perfilValida + "],"
							+ "tipoUsuarioValido[" + tipoUsuarioValido + "]," + "flagvalidarRut50MM["
							+ flagvalidarRut50MM + "]," + "tipoUsuarioValido[" + tipoUsuarioValido + "]");
				}
				if (perfilValida == null && tipoUsuarioValido == null && flagvalidarRut50MM == null) {
					if (getLogger().isInfoEnabled()) {
						getLogger().info("[firmarTransaccion][BCI_FINOK][ERROR][Problema tabla de parametros]");
					}
					throw new GeneralException(ERROR_GENERICO_FYP);
				}
	
				StringTokenizer stTok = new StringTokenizer(tipoUsuarioValido, ",");
				boolean tipoUsuValido = false;
				while (stTok.hasMoreTokens()) {
					if (tipoUsu.trim().equals(stTok.nextToken())) {
						tipoUsuValido = true;
					}
				}
				if (getLogger().isEnabledFor(Level.DEBUG)) {
					getLogger().debug("[" + FIRMAR_TRANSFERENCIA + "] esApoderadoReal[" + esApoderadoReal + "],"
							+ "perfil[" + perfil + "]," + "tipoUsu[" + tipoUsu + "]," + "tipoUsuValido["
							+ tipoUsuValido + "]");
				}
				
				DatosAutorizacionTO autorizacionTO = this.transferenciasRippleUtilityMB.getDatosAutorizacionTO();
				
				if (esApoderadoReal == false && tipoUsuValido && perfil.equalsIgnoreCase(perfilValida)
						&& Integer.parseInt(flagvalidarRut50MM) == NUM_UNO) {
					//ULTIMA FIRMA
					
					//REGISTRAR FIRMA FBP (SP 09-APR)
					
					FirmarEliminarTransferenciaRipple registrarFirma = new FirmarEliminarTransferenciaRipple();
					registrarFirma.setAccion(TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE,"accionSolicitada","aprobar"));
					registrarFirma.setCorrelativoFbp(transferenciaTO.getCorrelativoInterno());
					registrarFirma.setSysTimeStamp(transferenciaTO.getFechaActualizacion());
					registrarFirma.setUsuario(String.valueOf(usuarioClienteModelMB.getUsuarioModelMB().getRut()));
					registrarFirma.setIpTerminal("");
					registrarFirma.setComentarios("");
					
					DatosHashTO datoshash = new DatosHashTO();
					datoshash.setCuentaOrigen(transferenciaTO.getCuentaOrigen().getNumeroCuenta());
					datoshash.setMonto(String.valueOf(transferenciaTO.getMontoTransferir()));
					datoshash.setMoneda(codigoMoneda);
					datoshash.setIdBeneficiario(transferenciaTO.getBeneficiario().getReferencia());
					datoshash.setCuentaDestino(transferenciaTO.getBeneficiario().getCuenta().getNumeroCuenta());
					
					registrarFirma.setDatosHash(datoshash);
					//*******************************
					
					RespuestaSpTO respuestaFirma = this.transferenciasRippleUtilityMB.firmarOEliminarTransferenciaRipple(registrarFirma);
					
					if(respuestaFirma.getIndicadorResultado() != 0){
						if(respuestaFirma.getIndicadorResultado() == 1){
							//ERROR DE NEGOCIO
							
							
						}
						else{
							//ERROR DE SISTEMA O DE VALIDACION DE HASH
							
							
						}
					}
					
					this.transferenciasRippleViewMB.setPedienteFirma(false);
	
					if (getLogger().isEnabledFor(Level.DEBUG)) {
						getLogger().debug("[" + FIRMAR_TRANSFERENCIA + "] FirmarEliminarTransferenciaRipple[" + registrarFirma + "]");
					}
					
					//REALIZAR FIRMA AUT
					this.transferenciasRippleViewMB.setPedienteFirma(this.transferenciasRippleUtilityMB.procesarTransferenciaMonex(transferenciaMonexTO,
									autorizacionTO, 
									this.transferenciasRippleUtilityMB.getEventos(),
									null,//cambiaEstadoTttfMonexTO, //PENDIENTE DE VALIDAR ESTE DATO, YA QUE SE USA PARA LLAMAR AL VIEJO ACUTALIZAR ESTADO DE TRANSFERENCIA
									esApoderadoReal));
					
					this.journalizar(TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "journalizarEvento", "firma"),
							TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "journalizarSubEvento", "ok"),
							TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "journalizarProducto", "producto"),
							TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "journalizarEstado", "eventoProcesado"),
							String.valueOf(transferenciaTO.getMontoTransferir()),
							transferenciaTO.getBeneficiario().getNombre(),
							String.valueOf(transferenciaTO.getCorrelativoInterno()));
					
					try {
					
						//LLAMADA A SP 14
						RespuestaSpTO respuestaDebitar = this.transferenciasRippleUtilityMB.debitarCuentaCorrienteMX(transferenciaMonexTO.getNumeroCorrelativoFbp());
						if(respuestaDebitar.getIndicadorResultado() != 0){
							throw new TtffException(String.valueOf(respuestaDebitar.getIndicadorResultado()), respuestaDebitar.getMensajeResultado());
						}
						
						//UyP
						UyPDatos datosUyP = UyPCache.getInstance().getUyPSybUser(TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE,"uypSistema","user"));
						
						String token = this.transferenciasRippleUtilityMB.obtenerXCurrentToken(datosUyP);//obtenerXCurrentToken
						
						//Obtener datos para xCurrent (SP 15)
						DatosxCurrentTO datosxCurrent = this.transferenciasRippleUtilityMB.consultarDatosxCurrent(transferenciaTO.getCorrelativoInterno());
						if(datosxCurrent.getIndicadorResultado() != 0){
							//DESPLEGAR MENSAJE DE ERROR SEGUN TIPO
							throw new TtffException(String.valueOf(datosxCurrent.getIndicadorResultado()), datosxCurrent.getMensajeResultado());
							
						}
						
						//createQuoteCollection
						QuoteCollectionResponse collection = this.transferenciasRippleUtilityMB.createQuoteCollection(token, datosxCurrent);
						
						UUID paymentId = this.transferenciasRippleUtilityMB.acceptQuote(token, datosxCurrent, collection.getQuotes()[0].getQuoteId());
						
						//Registrar Pago en FBP (SP 16)
						RespuestaSpTO respuesta = this.transferenciasRippleUtilityMB.registrarResulatadoxCurrent(String.valueOf(transferenciaTO.getCorrelativoInterno()), "ACK", paymentId.toString());
						if(respuesta.getIndicadorResultado() != 0){
							//DESPLEGAR MENSAJE DE ERROR SEGUN TIPO
							throw new TtffException(String.valueOf(respuesta.getIndicadorResultado()), respuesta.getMensajeResultado());
						}
					} catch (TtffException e) {
						//REVERSO DE FIRMA
						int borrarFirma = this.transferenciasRippleUtilityMB.borraTransferenciaAUT(autorizacionTO, transferenciaMonexTO);
						if (getLogger().isEnabledFor(Level.DEBUG)) {
							getLogger().debug("[" + FIRMAR_TRANSFERENCIA + "] FirmarEliminarTransferenciaRipple[" + borrarFirma + "]");
						}
						if(e.getCodigo().equals("1")){
							//mensaje de negocio
							e.getInfoAdic(); //mensaje a mostrar por pantalla
						}
						else{
							//mensaje de sistema
							//despliegue mensaje de error generica
						}
						//llamar al SP 16 con cod ACK, payment ID
					}
				} 
				else {
					//NO ES ULTIMA FIRMA
					if (getLogger().isEnabledFor(Level.DEBUG)) {
						getLogger().debug("[" + FIRMAR_TRANSFERENCIA + "][" + identificador + "] [esApoderadoReal] ["+ esApoderadoReal + "]");
						getLogger().debug("[" + FIRMAR_TRANSFERENCIA + "][" + identificador	+ "] [transferenciasMonexUtilityMB.eventos] ["
								+ this.transferenciasRippleUtilityMB.getEventos() + "]");
					}
	
					//REGISTRAR FIRMA FBP (SP 09-SIG)
					
					FirmarEliminarTransferenciaRipple registrar = new FirmarEliminarTransferenciaRipple();
					registrar.setAccion(TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE,"accionSolicitada","firmar"));
					registrar.setCorrelativoFbp(transferenciaTO.getCorrelativoInterno());
					registrar.setSysTimeStamp(transferenciaTO.getFechaActualizacion());
					registrar.setUsuario(String.valueOf(usuarioClienteModelMB.getUsuarioModelMB().getRut()));
					registrar.setIpTerminal("");
					registrar.setComentarios("");
					//*******************************
					
					RespuestaSpTO respuestaSpTO = this.transferenciasRippleUtilityMB.firmarOEliminarTransferenciaRipple(registrar);
					
					if(respuestaSpTO.getIndicadorResultado() != 0){
						if(respuestaSpTO.getIndicadorResultado() == 1){
							//ERROR DE NEGOCIO
							
							
						}
						else{
							//ERROR DE SISTEMA 
							
							
						}
					}
					
					//REALIZAR FIRMA AUT
					this.transferenciasRippleViewMB.setPedienteFirma(this.transferenciasRippleUtilityMB.procesarTransferenciaMonex(transferenciaMonexTO,
									autorizacionTO, 
									this.transferenciasRippleUtilityMB.getEventos(),
									null,//cambiaEstadoTttfMonexTO, //PENDIENTE DE VALIDAR ESTE DATO, YA QUE SE USA PARA LLAMAR AL VIEJO ACUTALIZAR ESTADO DE TRANSFERENCIA
									esApoderadoReal));
					
					String subCodigoEvento = TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "journalizarSubEvento", "ok");
	
					if (getLogger().isEnabledFor(Level.DEBUG)) {
						getLogger().debug("[" + FIRMAR_TRANSFERENCIA + "][" + identificador	+ "] [transferenciasMonexViewMB.pendienteFirma] ["
										+ transferenciasRippleViewMB.isPedienteFirma() + "]");
						getLogger().debug("[" + FIRMAR_TRANSFERENCIA + "][" + identificador + "] [subCodigoEvento] [" + subCodigoEvento + "]");
					}
	
					if (this.transferenciasRippleViewMB.isPedienteFirma()) {
						subCodigoEvento = TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "journalizarSubEvento", "ok");
	
						ResultConsultarApoderadosquehanFirmado result = this.transferenciasRippleUtilityMB.consultarApoderadosQueHanFirmado(transferenciaMonexTO.getNumeroCorrelativoFbp(), 
										autorizacionTO,
										this.transferenciasRippleUtilityMB.getEventos());
	
						if (getLogger().isEnabledFor(Level.DEBUG)) {
							getLogger().debug("[" + FIRMAR_TRANSFERENCIA + "][" + identificador	+ "] [subCodigoEvento] [" + subCodigoEvento + "]");
						}
	
						ConApoquehanFirmado[] apoderados = result.getConApoquehanFirmado();
						if (apoderados != null) {
							for (int i = 0; i < apoderados.length; i++) {
								String nombreCompleto = apoderados[i].getNombres() + " "+ apoderados[i].getApellidoPaterno() + " "+ apoderados[i].getApellidoMaterno();
								if (getLogger().isEnabledFor(Level.DEBUG)) {
									getLogger().debug("[" + FIRMAR_TRANSFERENCIA + "][" + identificador + "] [apoderados<"+ i + ">] [" + StringUtil.contenidoDe(apoderados[i]) + "]");
									getLogger().debug("[" + FIRMAR_TRANSFERENCIA + "][" + identificador	+ "] [nombreCompleto] [" + nombreCompleto + "]");
								}
								this.transferenciasRippleViewMB.getApoderadosQueHanFirmado().add(nombreCompleto);
							}
						}
					}
	
					if (getLogger().isEnabledFor(Level.DEBUG)) {
						getLogger().debug("[" + FIRMAR_TRANSFERENCIA + "][" + identificador + "] [subCodigoEvento] ["+ StringUtil.contenidoDe(subCodigoEvento) + "]");
						getLogger().debug("[" + FIRMAR_TRANSFERENCIA + "][" + identificador + "] [transferenciaMonexTO] ["+ StringUtil.contenidoDe(transferenciaMonexTO) + "]");
						getLogger().debug("[" + FIRMAR_TRANSFERENCIA + "][" + identificador	+ "] [JournalTransferMonex.CODIGO_EVENTO_TRX_LINEA] [" 
										+ StringUtil.contenidoDe(JournalTransferMonex.CODIGO_EVENTO_TRX_LINEA.getValue())+ "]");
						getLogger().debug("[" + FIRMAR_TRANSFERENCIA + "][" + identificador	+ "] [JournalTransferMonex.ID_PRODUCTO] ["
										+ StringUtil.contenidoDe(JournalTransferMonex.ID_PRODUCTO.getValue()) + "]");
						getLogger().debug("[" + FIRMAR_TRANSFERENCIA + "][" + identificador	+ "] [JournalTransferMonex.ESTADO_P] ["
								+ StringUtil.contenidoDe(JournalTransferMonex.ESTADO_P.getValue()) + "]");
						getLogger().debug("[" + FIRMAR_TRANSFERENCIA + "][" + identificador + "] [JournalTransferMonex.NUMERO_OPERACION] [" 
										+ StringUtil.contenidoDe(JournalTransferMonex.NUMERO_OPERACION.getValue())+ "]");
					}
	
					this.journalizar(TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "journalizarEvento", "firma"),
							TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "journalizarSubEvento", "ok"),
							TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "journalizarProducto", "producto"),
							TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "journalizarEstado", "eventoProcesado"),
							String.valueOf(transferenciaTO.getMontoTransferir()),
							transferenciaTO.getBeneficiario().getNombre(),
							String.valueOf(transferenciaTO.getCorrelativoInterno()));
				}
			}
		} catch (SeguridadException ex) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[" + FIRMAR_TRANSFERENCIA + "][" + identificador
						+ "] [BCI_FINEX][SeguridadException][" + ex.getMessage() + "]", ex);
			}
	
			FacesMessage mensaje = new FacesMessage(ex.getInfoAdic());
			mensaje.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage("segundaClave", mensaje);
			
		} catch (NoSessionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (GeneralException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * <p>
	 * M�todo get del logger.
	 * </p>
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 07/03/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @return Logger
	 * @since 1.0
	 */
	private Logger getLogger() {
		if (log == null) {
			log = Logger.getLogger(this.getClass());
		}
		return log;
	}
}