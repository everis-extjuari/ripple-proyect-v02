package cl.bci.aplicaciones.productos.servicios.transferenciaripple.mb;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UISelectOne;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import cl.bci.aplicaciones.cliente.mb.ClienteMB;
import cl.bci.aplicaciones.cliente.mb.UsuarioClienteModelMB;
import cl.bci.aplicaciones.productos.servicios.transferenciamonex.to.CodigoValor;
import cl.bci.aplicaciones.productos.servicios.transferenciamonex.to.CuentaMonexTO;
import cl.bci.aplicaciones.productos.servicios.transferenciamonex.to.TransferenciaTO;
import cl.bci.aplicaciones.seguridad.autenticacion.mb.SegundaClaveMB;
import cl.bci.infraestructura.utilitarios.error.mb.MensajesErrorUtilityMB;
import cl.bci.infraestructura.web.journal.mb.JournalUtilityMB;
import cl.bci.infraestructura.web.seguridad.autorizaciones.mb.AutorizadorUtilityMB;
import wcorp.aplicaciones.productos.servicios.pagos.pagosmasivos.ServiciosDePagosMasivos;
import wcorp.aplicaciones.productos.servicios.pagos.pagosmasivos.ServiciosDePagosMasivosHome;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.CambiaEstadoTttfMonexTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.CodigoCambioTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.ConsultaMonedaTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.CuentaComisionTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.Moneda1a1TO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.TransferenciaMonexTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.BeneficiarioRippleTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.ComisionTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.ConsultaBeneficiariosRippleTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.ConsultaDetalleBeneficiarioRippleTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.DetalleBeneficiarioRippleTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.MonedasNodoTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.MonedasPermitidasTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.ObtenerComisionTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.RegistrarTransferenciaTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.RespuestaRegistroTransferenciaTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.TransferenciasRippleTO;
import wcorp.util.EnhancedServiceLocator;
import wcorp.util.ErroresUtil;
import wcorp.util.GeneralException;
import wcorp.util.StringUtil;
import wcorp.util.TablaValores;
import wcorp.util.journal.Eventos;

/**
 * Bean encargado de realizar la logica de los comprobantes que se deben desplegar.
 * <p>
 * Registro de Versiones :
 * <ul>
 * <li>1.0 10-03-2019, Danilo Dominguez(Everis) - Minheli Mejias (Ing. Soft. BCI): Versi�n inicial</li>
 * </ul>
 * </p>
 * <p>
 * <B>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</B>
 * </p>
 */
@ManagedBean
@RequestScoped
public class ComprobanteTransferenciaRippleBackingMB implements Serializable {

	/**
	 * M�todo del servicio de Segunda Clave.
	 */
	protected static final String SERVICIO_SEGUNDA_CLAVE = "TransferenciasMxUnoAUno";
	/**
	 * Vista asociada al autorizador.
	 */
	private static final String VISTA = "transferencias-Monex-1a1";
	/**
	 * Accion asociada al autorizador.
	 */
	private static final String ACCION = "Autorizar";
	/**
	 * Archivo parametros Autorizar FyP.
	 */
	private static final String ARCH_PARAM_VAL = "BCIExpressII_Autorizar_FyP.parametros";
	/**
	 * Codigo de error generico fyp.
	 */
	private static final String ERROR_GENERICO_FYP = "FYP-011";

	/**
	 * Representa numero uno.
	 */
	private static final int NUM_UNO = 1;

	/**
	 * Codigo de error no puede registrar la firma.
	 */
	private static final String ERROR_NO_REGISTR_FYP = "ODP-005";

	/**
	 * Servicio solicitado para Apoderado.
	 */
	private static final String SERVICIO_SOLICITADO_CUR = "CUR";
	/**
	 * Constante monto maximo transferencia.
	 */
	private static final String MONTO_MAXIMO_TRANSFER = "montoMaximoTransferencia";
	/**
	 * Constante mensaje a obtener desde archivo de parametros.
	 */
	private static final String MENSAJE = "mensaje";
	/**
	 * Tabla de par�metros de transfer monex.
	 */
	private static final String TABLA_PARAMETROS_TRANSFERRIPPLE = "pyme/transferRipple.parametros";
	/**
	 * Par�metro de origen de la solicitud.
	 */
	private static final String ORIGEN_SOLICITUD = "WEB";
	/**
	 * Constante ITF.
	 */
	private static final String TRANSFERENCIA_PARAMETROS = "transferencia";
	/**
	 * Bean de negocio usuarioClienteModelMB.
	 */
	@ManagedProperty(value = "#{usuarioClienteModelMB}")
	private UsuarioClienteModelMB usuarioClienteModelMB;
	/**
	 * log de la clase TransferenciasRippleBackingMB.
	 */
	private transient Logger logger = Logger.getLogger(ComprobanteTransferenciaRippleBackingMB.class);
	/**
	 * Bean de negocio clienteMB.
	 */
	@ManagedProperty(value = "#{clienteMB}")
	private ClienteMB clienteMB;
	/**
	 * Bean de negocio usuarioClienteModelMB.
	 */
	@ManagedProperty(value = "#{transferenciasRippleBackingMB}")
	private TransferenciasRippleBackingMB transferenciasRippleBackingMB;
	/**
	 * Bean de negocio transferenciasRippleUtilityMB.
	 */
	@ManagedProperty(value = "#{transferenciasRippleUtilityMB}")
	private TransferenciasRippleUtilityMB transferenciasRippleUtilityMB;
	/**
	 * Cuenta cargo comision.
	 */
	private CuentaComisionTO cuentaComisionTo;
	/**
	 * Inicializa.
	 */
	private boolean init;
	/**
	 * Corresponde a la moneda selecciona.
	 */
	private Moneda1a1TO monedaOtraCuenta = new Moneda1a1TO();
	/**
	 * Bean de negocio transferenciasRippleViewMB.
	 */
	@ManagedProperty(value = "#{transferenciasRippleViewMB}")
	private TransferenciasRippleViewMB transferenciasRippleViewMB;
	/**
	 * Corresponde a la cuenta cargo comisi�n seleccionada.
	 */
	private CuentaMonexTO cuentaCargoComision = new CuentaMonexTO();
	/**
	 * Corresponde a la moneda selecciona.
	 */
	private MonedasPermitidasTO monedaRipple = new MonedasPermitidasTO();
	/**
	 * Monto a tranferir string.
	 */
	private String strMontoATransferir;
	/**
	 * Bean manejo de mensajes.
	 */
	@ManagedProperty(value = "#{mensajesErrorUtilityMB}")
	private MensajesErrorUtilityMB mbErrorUtilityMB;
	/**
	 * Bean de negocio autorizadorUtilityMB.
	 */
	@ManagedProperty(value = "#{autorizadorUtilityMB}")
	private AutorizadorUtilityMB autorizadorUtilityMB;
	/**
	 * Bean que maneja operaciones de validaci�n de segunda clave.
	 */
	@ManagedProperty(value = "#{segundaClaveMB}")
	private SegundaClaveMB segundaClaveMB;
	/**
	 * Valor ingresado de segunda clave.
	 */
	private String segundaClaveValor;
	/**
	 * Corresponde al numero cuenta origen.
	 */
	private CuentaMonexTO cuentaOrigen = new CuentaMonexTO();
	/**
	 * Corresponde al motivo de pago de transferencia.
	 */
	private String motivoPago;
	/**
	 * Corresponde al los datos de la transferencia ingresada.
	 */
	private TransferenciasRippleTO transferenciaRipple = new TransferenciasRippleTO();
	/**
	 * Inyecci�n de Managed Bean de journal.
	 */
	@ManagedProperty(value = "#{journalUtilityMB}")
	private JournalUtilityMB journalUtilityMB;
	/**
	 * Saldo disponible cuenta corriente.
	 */
	private Double saldoDisponible;
	/**
	 * C�digo de cambio seleccionado.
	 */
	private CodigoCambioTO codigoCambio = new CodigoCambioTO();
	/**
	 * S�mbolo de la moneda.
	 */
	private String simboloMoneda;
	/**
	 * Monto a tranferir string.
	 */
	private double montoMax = 0;
	/**
	 * Error en monto.
	 */
	private boolean errorMonto;
	/**
	 * Mensaje de error.
	 */
	private String mensajeError;
	/**
	 * comision de ripple.
	 */
	private ComisionTO comisionRipple = new ComisionTO();
	

	public UsuarioClienteModelMB getUsuarioClienteModelMB() {
		return usuarioClienteModelMB;
	}

	public void setUsuarioClienteModelMB(UsuarioClienteModelMB usuarioClienteModelMB) {
		this.usuarioClienteModelMB = usuarioClienteModelMB;
	}

	public TransferenciasRippleBackingMB getTransferenciasRippleBackingMB() {
		return transferenciasRippleBackingMB;
	}

	public void setTransferenciasRippleBackingMB(TransferenciasRippleBackingMB transferenciasRippleBackingMB) {
		this.transferenciasRippleBackingMB = transferenciasRippleBackingMB;
	}

	public ClienteMB getClienteMB() {
		return clienteMB;
	}

	public void setClienteMB(ClienteMB clienteMB) {
		this.clienteMB = clienteMB;
	}

	public TransferenciasRippleUtilityMB getTransferenciasRippleUtilityMB() {
		return transferenciasRippleUtilityMB;
	}

	public void setTransferenciasRippleUtilityMB(TransferenciasRippleUtilityMB transferenciasRippleUtilityMB) {
		this.transferenciasRippleUtilityMB = transferenciasRippleUtilityMB;
	}

	public CuentaComisionTO getCuentaComisionTo() {
		return cuentaComisionTo;
	}

	public void setCuentaComisionTo(CuentaComisionTO cuentaComisionTo) {
		this.cuentaComisionTo = cuentaComisionTo;
	}

	public boolean isInit() {
		return init;
	}

	public void setInit(boolean init) {
		this.init = init;
	}

	public TransferenciasRippleViewMB getTransferenciasRippleViewMB() {
		return transferenciasRippleViewMB;
	}

	public void setTransferenciasRippleViewMB(TransferenciasRippleViewMB transferenciasRippleViewMB) {
		this.transferenciasRippleViewMB = transferenciasRippleViewMB;
	}

	public Moneda1a1TO getMonedaOtraCuenta() {
		return monedaOtraCuenta;
	}

	public void setMonedaOtraCuenta(Moneda1a1TO monedaOtraCuenta) {
		this.monedaOtraCuenta = monedaOtraCuenta;
	}

	public CuentaMonexTO getCuentaCargoComision() {
		return cuentaCargoComision;
	}

	public void setCuentaCargoComision(CuentaMonexTO cuentaCargoComision) {
		this.cuentaCargoComision = cuentaCargoComision;
	}

	public MonedasPermitidasTO getMonedaRipple() {
		return monedaRipple;
	}

	public void setMonedaRipple(MonedasPermitidasTO monedaRipple) {
		this.monedaRipple = monedaRipple;
	}

	public String getStrMontoATransferir() {
		return strMontoATransferir;
	}

	public void setStrMontoATransferir(String strMontoATransferir) {
		this.strMontoATransferir = strMontoATransferir;
	}

	public MensajesErrorUtilityMB getMbErrorUtilityMB() {
		return mbErrorUtilityMB;
	}

	public void setMbErrorUtilityMB(MensajesErrorUtilityMB mbErrorUtilityMB) {
		this.mbErrorUtilityMB = mbErrorUtilityMB;
	}

	public AutorizadorUtilityMB getAutorizadorUtilityMB() {
		return autorizadorUtilityMB;
	}

	public void setAutorizadorUtilityMB(AutorizadorUtilityMB autorizadorUtilityMB) {
		this.autorizadorUtilityMB = autorizadorUtilityMB;
	}

	/**
	 * Referencia a la componente que maneja la segunda Clave.
	 */
	/*
		 * private SegundaClaveUIInput segundaClave;
		 * 
		 * public SegundaClaveUIInput getSegundaClave() { return segundaClave; }
		 * 
		 * public void setSegundaClave(SegundaClaveUIInput segundaClave) {
		 * this.segundaClave = segundaClave; }
	 */

	public SegundaClaveMB getSegundaClaveMB() {
		return segundaClaveMB;
	}

	public void setSegundaClaveMB(SegundaClaveMB segundaClaveMB) {
		this.segundaClaveMB = segundaClaveMB;
	}

	public String getSegundaClaveValor() {
		return segundaClaveValor;
	}

	public void setSegundaClaveValor(String segundaClaveValor) {
		this.segundaClaveValor = segundaClaveValor;
	}

	public CuentaMonexTO getCuentaOrigen() {
		return cuentaOrigen;
	}

	public void setCuentaOrigen(CuentaMonexTO cuentaOrigen) {
		this.cuentaOrigen = cuentaOrigen;
	}

	public String getMotivoPago() {
		return motivoPago;
	}

	public void setMotivoPago(String motivoPago) {
		this.motivoPago = motivoPago;
	}

	public void setTransferenciaRipple(TransferenciasRippleTO transferenciasRipple) {
		this.transferenciaRipple = transferenciasRipple;
	}

	public TransferenciasRippleTO getTransferenciaRipple() {
		return this.transferenciaRipple;
	}

	public JournalUtilityMB getJournalUtilityMB() {
		return journalUtilityMB;
	}

	public void setJournalUtilityMB(JournalUtilityMB journalUtilityMB) {
		this.journalUtilityMB = journalUtilityMB;
	}

	public Double getSaldoDisponible() {
		return saldoDisponible;
	}

	public void setSaldoDisponible(Double saldoDisponible) {
		this.saldoDisponible = saldoDisponible;
	}

	/**
	 * Retorna Cuentas Comex y cuentas CorrientesPrimas.
	 * 
	 * @return List<CuentaMonexTO>
	 * @since 1.0
	 */
	public List<CuentaMonexTO> getCuentasComision() {
		return transferenciasRippleViewMB.getCuentasComision();
	}

	public CodigoCambioTO getCodigoCambio() {
		return codigoCambio;
	}

	public void setCodigoCambio(CodigoCambioTO codigoCambio) {
		this.codigoCambio = codigoCambio;
	}
	
	public String getSimboloMoneda() {
		return simboloMoneda;
	}

	public void setSimboloMoneda(String simboloMoneda) {
		this.simboloMoneda = simboloMoneda;
	}
	
	public boolean isErrorMonto() {
		return errorMonto;
	}

	public void setErrorMonto(boolean errorMonto) {
		this.errorMonto = errorMonto;
	}

	public String getMensajeError() {
		return mensajeError;
	}

	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}

	public ComisionTO getComisionRipple() {
		return comisionRipple;
	}

	public void setComisionRipple(ComisionTO comisionRipple) {
		this.comisionRipple = comisionRipple;
	}
	

	/**
	 * Obtiene rut empresa desde clienteMB.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 25/02/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft.
	 * BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @return String
	 * @throws GeneralException Excepcion negocio.
	 * @since 1.0
	 */
	private String getRutEmpresa() throws GeneralException {
		String rut = "";
		try {
			rut = clienteMB.getRut() + "" + clienteMB.getDigitoVerif();
		} catch (Exception e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[getRutEmpresa][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ "] [BCI_FINEX][Exception]Error con mensaje " + e.getMessage(), e);
			}
			throw new GeneralException("ESPECIAL", e.getMessage());
		}
		return rut;
	}

	/**
	 * Generaci�n de objeto con los datos necesarios para obtener la comision
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 04/03/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft.
	 * BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * @param obtenerComision Comision a aplicar.
	 * @since 1.0
	 */
	private void generarObtenerComision(ObtenerComisionTO obtenerComision) {
		obtenerComision.setRutEmpresa(String.valueOf(clienteMB.getRut()) + "-" + clienteMB.getDigitoVerif());
		obtenerComision.setMonedaTransf(this.transferenciasRippleBackingMB.getMonedaRipple().getCodigoIso());
		obtenerComision.setMontoPago(this.transferenciasRippleBackingMB.getStrMontoATransferir());
		obtenerComision.setBicBanco(this.transferenciasRippleViewMB.getNodoRipple().getBic());
	}

	/**
	 * Generaci�n de objeto con los datos necesarios registrar la transferencia
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 07/03/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft.
	 * BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * @return RespuestaRegistroTransferenciaTO Entidad de negocio.
	 * @throws GeneralException Excepcion de negocio.
	 * @since 1.0
	 */
	private RespuestaRegistroTransferenciaTO generarTransferencia() throws GeneralException {
		RegistrarTransferenciaTO regTransferencia = new RegistrarTransferenciaTO();
		regTransferencia.setRutEmpresa(String.valueOf(clienteMB.getRut()) + "-" + clienteMB.getDigitoVerif());
		regTransferencia.setNombreEmpresa(usuarioClienteModelMB.getClienteMB().getRazonSocial());
		// regTransferencia.setMoneda("USD");
		regTransferencia.setMoneda(this.monedaRipple.getCodigoIso());
		// regTransferencia.setCuentaDebito("11010037");
		regTransferencia.setCuentaDebito(getCuentaOrigen().getNumeroCuenta());
		// regTransferencia.setMontoPago(BigDecimal.valueOf(1.00));
		regTransferencia.setMontoPago(transferenciasRippleViewMB.getMontoTransferencia());
		// regTransferencia.setRemesa("Prueba");
		regTransferencia.setRemesa(this.getMotivoPago());
		// regTransferencia.setMonedaComision("USD");
		regTransferencia.setMonedaComision(this.getMonedaOtraCuenta().getCodigoIso());
		// regTransferencia.setCuentaComision("11052031");
		regTransferencia.setCuentaComision(this.cuentaCargoComision.getNumeroCuenta());
		// regTransferencia.setCodIdentifBenef("E0003743");
		regTransferencia.setCodIdentifBenef(this.getTransferenciaRipple().getDatosDestin().getReferencia());
		// regTransferencia.setPaisBcoBenef("US");
		regTransferencia.setPaisBcoBenef(this.transferenciasRippleViewMB.getDestinRipple().getPaisBanco());
		// regTransferencia.setCodigoCambio("20075");
		regTransferencia.setCodigoCambio(this.getTransferenciaRipple().getDatosBanco().getCodigoCambio());
		// regTransferencia.setKeyOperacion("18:07:46.226");
		regTransferencia.setKeyOperacion(this.transferenciasRippleViewMB.getComisionRipple().getKey());
		regTransferencia.setRutUsuario(usuarioClienteModelMB.getUsuarioModelMB().getRut() + "-"
				+ usuarioClienteModelMB.getUsuarioModelMB().getDigitoVerif());
		regTransferencia.setIpMaquina("");
		// regTransferencia.setCuentaDestino("120009436");
		regTransferencia.setCuentaDestino(this.getTransferenciaRipple().getDatosDestin().getNumeroCuenta());
		getLogger().info("DANARO, datos para generar la transferencia: " + regTransferencia.toString());
		RespuestaRegistroTransferenciaTO respRegTransf = transferenciasRippleUtilityMB
				.registrarTransferencia(regTransferencia);
		getLogger().info("DANARO, respRegTransf: " + respRegTransf.toString());
		return respRegTransf;
	}

	/**
	 * Generaci�n de objeto
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 11/03/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft.
	 * BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * @param transferenciaTO entidad de negocio.
	 * @param numCorrelativoFbp identificador de operacion.
	 * @throws GeneralException en caso de error.
	 * @since 1.0
	 */
	private void crearTransferenciaTO(TransferenciaTO transferenciaTO, int numCorrelativoFbp) throws GeneralException {
		getLogger().info("DANARO, en crearTransferenciaTO");
		MonedasNodoTO monedasNodo = transferenciasRippleUtilityMB.obtenerMonedas();
		MonedasPermitidasTO moneda = (MonedasPermitidasTO) monedasNodo.getLstMonedas().get(0);

		// Obtener beneficiarios y rescatar los datos del ya seleccionado
		ConsultaBeneficiariosRippleTO consulta = new ConsultaBeneficiariosRippleTO();
		consulta.setBicBanco(monedasNodo.getNodo().getBic());
		consulta.setMndaCuenta(moneda.getCodigoIso());
		consulta.setRutEmpresa(getRutEmpresa());
		getLogger().info("DANARO, en crearTransferenciaTO, voy a buscar los beneficiarios con los datos: "
				+ consulta.toString());
		List<BeneficiarioRippleTO> lstBenefsRipple = transferenciasRippleUtilityMB
				.obtenerBeneficiariosAutorizadosRipple(consulta);
		getLogger().info("DANARO, lstBenefsRipple: " + lstBenefsRipple.toString());
		BeneficiarioRippleTO benefSeleccionado = null;
		getLogger().info("DANARO, se va a buscar el beneficiario seleccionado");
		for (BeneficiarioRippleTO beneficiarioRippleTO : lstBenefsRipple) {
			if (beneficiarioRippleTO.getIdentificacion().equalsIgnoreCase("E0003743")) {
				// if(beneficiarioRippleTO.getIdentificacion().equalsIgnoreCase(transferenciaRipple.getDatosDestin().getReferencia()))
				// {
				benefSeleccionado = beneficiarioRippleTO;
				getLogger().info("DANARO, se encontro al beneficiario");
				break;
			}
		}

		if (benefSeleccionado != null) {
			getLogger().info(
					"DANARO, sse procede a obtener el detalle del beneficiario seleccionado con los datos: identificacion: "
							+ benefSeleccionado.getIdentificacion() + ", moneda: " + moneda.getCodigoIso()
							+ ", pasi banco: " + benefSeleccionado.getPaisBanco());
			DetalleBeneficiarioRippleTO detalleBenefSelec = this.consultarDetalleBeneficiario(
					benefSeleccionado.getIdentificacion(), moneda.getCodigoIso(), benefSeleccionado.getPaisBanco());
			getLogger().info("DANARO, se obtubo el detalle del beneficiario, se procede a armar el TO");
			transferenciaTO.setCorrelativoInterno(numCorrelativoFbp);
			transferenciaTO.setTipoUsuario(transferenciasRippleUtilityMB.obtenerTipoUsuario());
			transferenciaTO.setModalidadOperacion("SHW");
			transferenciaTO.getBeneficiario().setNombre2(detalleBenefSelec.getNombreCont());
			transferenciaTO.getBeneficiario().setDireccion1(detalleBenefSelec.getDireccion());
			transferenciaTO.getBeneficiario().setDireccion2(detalleBenefSelec.getDireccionCont());
			transferenciaTO.getBeneficiario().setNombrePaisBeneficiario(detalleBenefSelec.getNombrePaisRes());
			transferenciaTO.getBeneficiario().setCiudad1(detalleBenefSelec.getInfoAdicional());
			transferenciaTO.getBeneficiario().setInformacionAdicionalDos(detalleBenefSelec.getInfoAdicionalCont());
			getLogger().info("DANARO, el trasnferenciaTO generado es: " + transferenciaTO.toString());

			this.transferenciasRippleViewMB.setTransferenciaSelected(transferenciaTO);
		} else {
			getLogger().info("DANARO, no se encontro al beneficiario ");
		}
	}

	/**
	 * Consulta detalle del beneficiario.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 25/02/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft.
	 * BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param identificadorBeneficiario Identificador Beneficiario.
	 * @param codMoneda                 Codigo de moneda.
	 * @param paisBanco                 C�digo de pais banco.
	 * @return DetalleBeneficiarioTO Entidad de negocio.
	 * @throws GeneralException Excepcion de negocio.
	 * @since 1.0
	 */
	private DetalleBeneficiarioRippleTO consultarDetalleBeneficiario(String identificadorBeneficiario, String codMoneda,
			String paisBanco) throws GeneralException {

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[consultarDetalleBeneficiario][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
					+ "] [BCI_INI]");
		}

		this.getTransferenciasRippleViewMB().setCodigoMoneda(codMoneda);
		this.getTransferenciasRippleViewMB().setCodigoPaisBanco(paisBanco);
		this.getTransferenciasRippleViewMB().setIdentificadorBeneficiario(identificadorBeneficiario);

		ConsultaDetalleBeneficiarioRippleTO datosConsulta = new ConsultaDetalleBeneficiarioRippleTO();
		datosConsulta.setRutEmpresa(clienteMB.getRut());
		datosConsulta.setDvEmpresa(clienteMB.getDigitoVerif());
		datosConsulta.setIdentificadorBeneficiario(identificadorBeneficiario);
		datosConsulta.setCodIso(codMoneda);
		datosConsulta.setPaisBanco(paisBanco);

		return transferenciasRippleUtilityMB.obtenerDetalleBeneficiarioRipple(datosConsulta);
	}

	/**
	 * Setea datos a transferenciaMonexTO para realizar la aceptaci�n de la
	 * transferencia.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 125/02/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft.
	 * BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param transferenciaTO Entidad de negocio.
	 * @return TransferenciaMonexTO
	 * @throws Exception Excepcion general.
	 * @since 1.0
	 */
	private TransferenciaMonexTO getTransferenciaMonexTO(TransferenciaTO transferenciaTO) throws Exception {
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug(
					"[getTransferenciaMonexTO][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "] [BCI_INI]");
		}
		TransferenciaMonexTO transferenciaMonex = new TransferenciaMonexTO();
		transferenciaMonex.setBancoBeneficiario(transferenciaTO.getBancoBeneficiario().getCodigoSwift());
		transferenciaMonex.setBicBancoIntermediario(transferenciaTO.getBancoIntermediario().getCodigoSwift());
		transferenciaMonex.setCodigoCambio(transferenciaTO.getCodigoCambio().getCodigo());
		transferenciaMonex.setCodigoIdentificador(transferenciaTO.getBeneficiario().getReferencia());
		transferenciaMonex.setCodigoProducto(
				TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "codigosMoneda", "codigoProducto"));
		transferenciaMonex.setCtaCte(transferenciaTO.getCuentaOrigen().getNumeroCuenta());
		transferenciaMonex.setCtacteDebito(transferenciaTO.getCuentaCargoComision().getNumeroCuenta());
		transferenciaMonex.setCuentaCorriente(transferenciaTO.getCuentaOrigen().getNumeroCuenta());
		transferenciaMonex.setDetalleGastos(transferenciaTO.getMotivoPago());
		transferenciaMonex.setEmail1(transferenciaTO.getBeneficiario().getMail());
		transferenciaMonex.setEmail2(transferenciaTO.getBeneficiario().getMail());
		transferenciaMonex.setEmail3(transferenciaTO.getBeneficiario().getMail());
		transferenciaMonex.setFechaOperacion(new Date());
		transferenciaMonex.setInformacionRemesa("");
		transferenciaMonex.setIpMaquina("");
		transferenciaMonex.setMonedaCtacte(transferenciaTO.getCuentaOrigen().getMoneda().getCodigo());
		transferenciaMonex.setMonedaNomina(transferenciaTO.getCuentaOrigen().getMoneda().getCodigo());
		transferenciaMonex.setMontoDebito(new BigDecimal(transferenciaTO.getMontoTransferir()));
		transferenciaMonex.setNombreBeneficiarioL1(transferenciaTO.getBeneficiario().getNombre());
		transferenciaMonex.setNombreBeneficiarioL2(transferenciaTO.getBeneficiario().getDireccion());
		transferenciaMonex.setNombreBeneficiarioL3(transferenciaTO.getBeneficiario().getDireccion());
		transferenciaMonex.setNombreBeneficiarioL4(transferenciaTO.getBeneficiario().getDireccion());
		transferenciaMonex.setNombreEmpresa(usuarioClienteModelMB.getClienteMB().getRazonSocial());
		transferenciaMonex.setNomDirBancoL1(transferenciaTO.getBancoBeneficiario().getNombreDireccion());
		transferenciaMonex.setNomDirBancoL2(transferenciaTO.getBancoBeneficiario().getNombreDireccion());
		transferenciaMonex.setNomDirBancoL3(transferenciaTO.getBancoBeneficiario().getNombreDireccion());
		transferenciaMonex.setNomDirBancoL4(transferenciaTO.getBancoBeneficiario().getNombreDireccion());
		transferenciaMonex.setNomDirIntermediarioL1(transferenciaTO.getBancoIntermediario().getNombreDireccion());
		transferenciaMonex.setNomDirIntermediarioL2(transferenciaTO.getBancoIntermediario().getNombreDireccion());
		transferenciaMonex.setNomDirIntermediarioL3(transferenciaTO.getBancoIntermediario().getNombreDireccion());
		transferenciaMonex.setNomDirIntermediarioL4(transferenciaTO.getBancoIntermediario().getNombreDireccion());
		transferenciaMonex.setNumeroCorrelativoFbp(transferenciaTO.getCorrelativoInterno());
		transferenciaMonex.setOrigenSolicitud(ORIGEN_SOLICITUD);
		transferenciaMonex.setPaisBeneficiario(transferenciaTO.getBeneficiario().getPaisCuenta().getCodigo());
		transferenciaMonex.setRutaPago(transferenciaTO.getBancoBeneficiario().getNumeroCuenta());
		transferenciaMonex.setRutaPagoIntermediario(transferenciaTO.getBancoIntermediario().getNumeroCuenta());
		transferenciaMonex.setRutEmpresa(usuarioClienteModelMB.getUsuarioModelMB().getRut() + "-"
				+ usuarioClienteModelMB.getUsuarioModelMB().getDigitoVerif());
		transferenciaMonex.setServicioSolicitado(SERVICIO_SOLICITADO_CUR);
		transferenciaMonex.setSystimestamp(transferenciaTO.getFechaActualizacion());
		transferenciaMonex.setTipoOperacion(
				TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, TRANSFERENCIA_PARAMETROS, "tipoOperacion"));
		transferenciaMonex.setUsuario(this.getUsuarioClienteModelMB().getUsuarioModelMB().getRut() + ""
				+ this.getUsuarioClienteModelMB().getUsuarioModelMB().getDigitoVerif());
		transferenciaMonex.setValutaPago(transferenciaTO.getValuta().getCodigo());
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[getTransferenciaMonexTO][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
					+ "] [BCI_FINOK]");
		}
		return transferenciaMonex;
	}

	/**
	 * Se genera TO CambiaEstadoTttfMonexTO.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 25/02/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft.
	 * BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param tipUsr       Tipo de usuario.
	 * @param numCorr      N�mero de correlativo.
	 * @param sysTimeStamp Fecha de transacci�n.
	 * @return CambiaEstadoTttfMonexTO
	 * @since 1.0
	 */
	private CambiaEstadoTttfMonexTO getCambiaEstadoTttfMonexTO(String tipUsr, int numCorr, String sysTimeStamp) {
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[getCambiaEstadoTttfMonexTO][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
					+ "] [BCI_INI]");
		}
		CambiaEstadoTttfMonexTO cambiaEstadoTttfMonexTO = new CambiaEstadoTttfMonexTO();
		cambiaEstadoTttfMonexTO.setOrigenSolicitud(ORIGEN_SOLICITUD);
		cambiaEstadoTttfMonexTO.setTipoUsuario(tipUsr);
		cambiaEstadoTttfMonexTO.setNumeroCorrelativo(numCorr);
		cambiaEstadoTttfMonexTO.setSystimestamp(sysTimeStamp);
		cambiaEstadoTttfMonexTO.setUsuarioEvento(this.getUsuarioClienteModelMB().getUsuarioModelMB().getRut() + ""
				+ this.getUsuarioClienteModelMB().getUsuarioModelMB().getDigitoVerif());
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[getCambiaEstadoTttfMonexTO][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
					+ "] [BCI_FINOK]");
		}
		return cambiaEstadoTttfMonexTO;
	}

	/**
	 * M�todo encargado de crear una instacia del EJB ServiciosDePagosMasivos.
	 *
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 11/03/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft.
	 * BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @since 1.0
	 * @return Instancia del EJB ServiciosDePagosMasivos.
	 * @throws GeneralException en caso de error.
	 */
	private ServiciosDePagosMasivos crearEJBServiciosDePagosMasivos() throws GeneralException {
		try {
			if (getLogger().isInfoEnabled()) {
				getLogger().info("[crearEJBServiciosDePagosMasivos][BCI_INI]");
			}
			EnhancedServiceLocator locator = EnhancedServiceLocator.getInstance();
			ServiciosDePagosMasivosHome serviciosDePagosMasivosHome = (ServiciosDePagosMasivosHome) locator
					.getGenericService(
							"wcorp.aplicaciones.productos.servicios.pagos.pagosmasivos.ServiciosDePagosMasivos",
							ServiciosDePagosMasivosHome.class);
			if (getLogger().isInfoEnabled()) {
				getLogger().info("[crearEJBServiciosDePagosMasivos][BCI_FINOK] Retornando instancia del EJB");
			}
			return serviciosDePagosMasivosHome.create();
		} catch (Exception e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[crearEJBServiciosDePagosMasivos][BCI_FINEX][Exception][" + e.getMessage() + "]", e);
			}

			throw new GeneralException("ESPECIAL", ErroresUtil.extraeStackTrace(e));
		}
	}

	/**
	 * Journaliza operaciones de transfer monex.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 11/03/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft.
	 * BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param codigoEvento    C�digo de evento (CREAR | TRANSLINEA | MODIFICA |
	 *                        ELIMINAR).
	 * @param subCodigoEvento Sub c�digo evento (OK | NOK | ERROR).
	 * @param idProducto      Id de producto (TRANMX).
	 * @param estadoEvento    Estado Evento (P | R).
	 * @param monto           Monto de transferencia.
	 * @param destinatario    Destinatario transferencia.
	 * @param numeroOperacion N�mero de operaci�n.
	 * @since 1.0
	 */
	private void journalizar(String codigoEvento, String subCodigoEvento, String idProducto, String estadoEvento,
			String monto, String destinatario, String numeroOperacion) {
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[journalizar][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "] [BCI_INI]");
		}
		try {
			String campoVariable = null;
			if (monto != null && destinatario != null) {
				campoVariable = monto + "|" + destinatario;
			}
			HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
					.getRequest();
			String idMedio = request.getHeader("X-FORWARDED-FOR");
			if (idMedio == null) {
				idMedio = request.getRemoteAddr();
			}
			Eventos evento = new Eventos();
			evento.setCodEventoNegocio(codigoEvento);
			evento.setSubCodEventoNegocio(subCodigoEvento);
			evento.setIdProducto(idProducto);
			evento.setEstadoEventoNegocio(estadoEvento);
			evento.setRutCliente(String.valueOf(this.getClienteMB().getRut()));
			evento.setDvCliente(String.valueOf(this.getClienteMB().getDigitoVerif()));
			evento.setRutOperadorCliente(String.valueOf(usuarioClienteModelMB.getUsuarioModelMB().getRut()));
			evento.setDvOperadorCliente(String.valueOf(usuarioClienteModelMB.getUsuarioModelMB().getDigitoVerif()));
			evento.setIdCanal(this.getUsuarioClienteModelMB().getUsuarioModelMB().getSesionMB().getCanalId());
			evento.setIdMedio(idMedio);
			evento.setCampoVariable(campoVariable);
			evento.setClavePrincipal(numeroOperacion);
			if (getLogger().isEnabledFor(Level.DEBUG)) {
				getLogger().debug("[journalizar] Se invoca journalizacion >>" + evento.toString());
			}
			this.getJournalUtilityMB().journalizar(evento);
		} catch (Exception e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[journalizar][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ "] [BCI_FINEX][Exception]Error con mensaje " + e.getMessage(), e);
			}
		}
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[journalizar][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "] [BCI_FINOK]");
		}
	}

	/**
	 * Acceso monedas otras cuentas.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 11/03/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft.
	 * BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @return List<Moneda1a1TO>.
	 * @since 1.0
	 */
	public List<Moneda1a1TO> getMonedasOtrasCuentas() {
		try {
			if (getLogger().isEnabledFor(Level.DEBUG)) {
				getLogger().debug("[getMonedasOtrasCuentas][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ "] [BCI_INI]");
			}
			if (transferenciasRippleViewMB.getMonedasOtrasCuentas() == null
					|| transferenciasRippleViewMB.getMonedasOtrasCuentas().isEmpty()) {
				ConsultaMonedaTO consultaMonedaTO = new ConsultaMonedaTO();

				consultaMonedaTO.setCodigoProducto(
						TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "codigosMoneda", "codigoProductoCVD"));
				consultaMonedaTO.setOrigenSolicitud(
						TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "codigosMoneda", "origenSolicitud"));
				transferenciasRippleViewMB
						.setMonedasOtrasCuentas(transferenciasRippleUtilityMB.obtenerMonedas(consultaMonedaTO));
			}
		} catch (GeneralException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[getMonedasOtrasCuentas][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ "] [BCI_FINEX][GeneralException]Error con mensaje " + e.getMessage(), e);
			}
		}
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug(
					"[getMonedasOtrasCuentas][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "] [BCI_FINOK]");
		}
		return transferenciasRippleViewMB.getMonedasOtrasCuentas();
	}

	/**
	 * Carga informaci�n necesaria al cambiar el combo moneda cargo comisi�n.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 25/02/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft.
	 * BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @param ev Evento Ajax.
	 * @return String
	 * @since 1.0
	 */
	public String cambiaSeleccionOtraMoneda(AjaxBehaviorEvent ev) {
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[cambiaSeleccionOtraMoneda] [" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
					+ "] [BCI_INI]");
		}
		this.init = true;
		UISelectOne selectMoneda = (UISelectOne) ev.getSource();
		String valor = (String) selectMoneda.getSubmittedValue();
		this.monedaOtraCuenta.setCodigoIso(valor);
		this.cuentaCargoComision.setNumeroCuenta(null);
		this.buscaOtraMonedaSeleccionada();
		this.transferenciasRippleViewMB.getCuentasComision().clear();
		if (valor != null && StringUtils.isNotEmpty(valor) && !"0".equals(valor)) {
			List<CuentaMonexTO> cuentasComision = this.transferenciasRippleUtilityMB
					.getCuentasPorCodigoMoneda(clienteMB.getRut(), clienteMB.getDigitoVerif(), valor);
			if (null == cuentasComision || cuentasComision.isEmpty()) {
				CuentaMonexTO cuentaMonexTO = new CuentaMonexTO();
				cuentaMonexTO.setMoneda(new CodigoValor(valor, ""));
				cuentaMonexTO.setNumeroCuenta(TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE, "mensajesError",
						"noPoseeCuentaMensaje"));
				this.transferenciasRippleViewMB.getCuentasComision().add(cuentaMonexTO);
			} else {
				transferenciasRippleViewMB.setCuentasComision(cuentasComision);
			}

		}
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[cambiaSeleccionOtraMoneda] [" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
					+ "] [BCI_FINOK]");
		}
		return null;
	}

	/**
	 * Busca dentro de las monedas la moneda seleccionada.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 25/02/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft.
	 * BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @since 1.0
	 */
	private void buscaOtraMonedaSeleccionada() {
		for (Moneda1a1TO mda : transferenciasRippleViewMB.getMonedasOtrasCuentas()) {
			if (mda.getCodigoIso().equals(this.getMonedaOtraCuenta().getCodigoIso())) {
				this.setMonedaOtraCuenta(mda);
				break;
			}
		}
	}

	/**
	 * M�todo Para validar Monto Maximo.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 01/03/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft.
	 * BCI): Versi�n inicial.</li>
	 * </ul>
	 * </p>
	 * 
	 * @return String
	 * @throws GeneralException Excepcion de negocio.
	 * @since 1.0
	 */
	private boolean validaMonto() throws GeneralException {
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[validaMonto][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "][BCI_INI]");
		}

		String monedaElegida = "";
		monedaElegida = monedaRipple.getCodigoIso();

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[validaMonto] moneda Elegida: " + monedaElegida);
		}

		transferenciasRippleViewMB.setMonedasNodo(transferenciasRippleUtilityMB.obtenerMonedas());

		if (monedaElegida != null && !monedaElegida.equals("")) {
			for (MonedasPermitidasTO monedasPermitidas : transferenciasRippleViewMB.getMonedasRipple()) {
				if (monedasPermitidas.getCodigoIso().equalsIgnoreCase(monedaElegida)) {
					montoMax = monedasPermitidas.getMtoMaximo().doubleValue();
				}
			}
		} else {
			if (getLogger().isEnabledFor(Level.INFO)) {
				getLogger()
						.info("[validaMonto][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "] [BCI_FINOK]");
			}
			return false;
		}

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[validaMonto] monto Maximo:" + montoMax);
		}
		Double montoFloat = 0.0;
		try {

			if (getLogger().isEnabledFor(Level.DEBUG)) {
				getLogger().debug("[validaMonto] str Monto A Transferir: " + strMontoATransferir);
			}
			String montoMaxString = strMontoATransferir;
			montoMaxString = montoMaxString.replace(".", "");
			montoMaxString = montoMaxString.replace(",", ".");
			montoFloat = Double.parseDouble(montoMaxString);
			if (getLogger().isEnabledFor(Level.DEBUG)) {
				getLogger().debug("[validaMonto] montoFloat: " + montoFloat);
			}
		} catch (Exception e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[validaMonto][" + usuarioClienteModelMB.getUsuarioModelMB().getRut()
						+ "][BCI_FINEX] [Exception]: ERROR [" + montoFloat + " " + e.getMessage(), e);
			}
			return false;
		}
		if (montoFloat > montoMax) {
			if (getLogger().isEnabledFor(Level.DEBUG)) {
				getLogger().debug("[validaMonto]: ERROR Monto excede el maximo[" + montoFloat + "]");
			}
			if (getLogger().isEnabledFor(Level.INFO)) {
				getLogger()
						.info("[validaMonto][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "] [BCI_FINOK]");
			}
			return false;
		} else {
			if (getLogger().isEnabledFor(Level.INFO)) {
				getLogger()
						.info("[validaMonto][" + usuarioClienteModelMB.getUsuarioModelMB().getRut() + "] [BCI_FINOK]");
			}
			return true;
		}
	}

	/**
	 * M�todo encargado de realizar la confirmaci�n de la transferencia.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 20/02/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft.
	 * BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @return String
	 * @throws GeneralException Excepcion de negocio.
	 * @since 1.0
	 */
	public String confirmarTransferenciaRipple() throws GeneralException {
		getLogger().info("OSO");
		final String nombreMetodo = "confirmarTransferenciaRipple";
		final String identificador = (usuarioClienteModelMB != null
				&& usuarioClienteModelMB.getUsuarioModelMB() != null)
						? String.valueOf(usuarioClienteModelMB.getUsuarioModelMB().getRut())
						: "";

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "][" + identificador + "] [BCI_INI]");
		}

		ObtenerComisionTO obtenerComision = new ObtenerComisionTO();
		generarObtenerComision(obtenerComision);
		getLogger().info("Danaro: se buscara la comision con los datos: " + obtenerComision.toString());

		this.setComisionRipple(this.transferenciasRippleUtilityMB.obtenerComisiones(obtenerComision));
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "][" + identificador + "] [BCI_FINOK][confirmarTransferenciasMonex]");
		}

		String rutEmpresa = getRutEmpresa();
		this.cuentaComisionTo = transferenciasRippleUtilityMB.obtenerCuentaDeCargo(rutEmpresa);

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[" + nombreMetodo + "][" + identificador + "] [rutEmpresa] ["
					+ StringUtil.contenidoDe(rutEmpresa) + "]");
			getLogger().debug(
					"[" + nombreMetodo + "][" + identificador + "] [init] [" + StringUtil.contenidoDe(init) + "]");
			getLogger().debug("[" + nombreMetodo + "][" + identificador + "] [cuentaComisionTo] ["
					+ StringUtil.contenidoDe(cuentaComisionTo) + "]");
		}

		if (!this.init && this.cuentaComisionTo != null) {
			CuentaMonexTO cuenta = new CuentaMonexTO();
			cuenta.setMoneda(new CodigoValor(cuentaComisionTo.getMonedaCtacte(), cuentaComisionTo.getMonedaCtacte()));
			cuenta.setNumeroCuenta(cuentaComisionTo.getCtaDebito());

			if (getLogger().isEnabledFor(Level.DEBUG)) {
				getLogger().debug("[" + nombreMetodo + "][" + identificador + "] [cuenta] ["
						+ StringUtil.contenidoDe(cuenta) + "]");
			}

			this.monedaOtraCuenta.setCodigoIso(this.cuentaComisionTo.getMonedaCtacte());
			this.transferenciasRippleViewMB
					.setCuentasComision(this.transferenciasRippleUtilityMB.getCuentasPorCodigoMoneda(clienteMB.getRut(),
							clienteMB.getDigitoVerif(), this.monedaOtraCuenta.getCodigoIso()));
			this.cuentaCargoComision.setNumeroCuenta(cuentaComisionTo.getCtaDebito());

			if (getLogger().isEnabledFor(Level.DEBUG)) {
				getLogger().debug(
						"[" + nombreMetodo + "][" + identificador + "] [transferenciasMonexViewMB.cuentasComision] ["
								+ StringUtil.contenidoDe(transferenciasRippleViewMB.getCuentasComision()) + "]");
				getLogger().debug("[" + nombreMetodo + "][" + identificador + "] [cuentaCargoComision] ["
						+ StringUtil.contenidoDe(cuentaCargoComision) + "]");
			}
		}

		return "confirmarTransferenciasRipple";
	}

	/**
	 * <p>
	 * M�todo get del logger.
	 * </p>
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 07/03/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft. BCI): versi�n inicial</li>
	 * </ul>
	 * </p>
	 * 
	 * @return Logger
	 * @since 1.0
	 */
	private Logger getLogger() {
		if (logger == null) {
			logger = Logger.getLogger(this.getClass());
		}
		return logger;
	}
}
