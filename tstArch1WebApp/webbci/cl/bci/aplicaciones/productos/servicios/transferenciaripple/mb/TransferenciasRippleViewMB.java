package cl.bci.aplicaciones.productos.servicios.transferenciaripple.mb;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import cl.bci.aplicaciones.productos.servicios.transferenciamonex.to.CuentaMonexTO;
import cl.bci.aplicaciones.productos.servicios.transferenciamonex.to.TransferenciaTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.Beneficiario1a1TO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.CodigoCambioTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.ComisionTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.GastosBancoExteriorTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.Moneda1a1TO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.TransferenciaMonexTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.ValutaTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.BeneficiarioRippleTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.DetalleBeneficiarioRippleTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.MonedasNodoTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.MonedasPermitidasTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.NodosRippleTO;

/**
 * Bean de transferencias Ripple, complemento de bean de transferencia Ripple backing bean.
 * <p>
 * Registro de Versiones :
 * <ul>
 * <li>1.0 05/03/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft. BCI): Versi�n inicial</li>
 * </ul>
 * </p>
 * <p>
 * <B>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</B>
 * </p>
 */
@ManagedBean
@ViewScoped
public class TransferenciasRippleViewMB implements Serializable {

    /**
     * Nombre de bean de transferencia.
     */
    public static final String BEAN_NAME = "transferenciasRippleViewMB";

    /**
     * Llave de transferencias realizadas para filtro.
     */
    protected static final String TRANSFERENCIAS_REALIZADAS = "TransferenciasRealizadas";

    /**
     * Id de serializaci�n.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Lista de beneficiarios.
     */
    private List<Beneficiario1a1TO> beneficiarios = new ArrayList<Beneficiario1a1TO>();
    
    /**
     * Lista de beneficiarios Ripple.
     */
    private List<BeneficiarioRippleTO> beneficiariosRipple = new ArrayList<BeneficiarioRippleTO>();

    /**
     * Lista de c�digos de cambio.
     */
    private List<CodigoCambioTO> codigosCambio = new ArrayList<CodigoCambioTO>();

    /**
     * Corresponde a la cuenta cargo seleccionada.
     */
    private ComisionTO comision;

    /**
     * Lista de cuentas de comisi�n.
     */
    private List<CuentaMonexTO> cuentasComision = new ArrayList<CuentaMonexTO>();

    /**
     * Lista de cuentas origen.
     */
    private List<CuentaMonexTO> cuentasOrigen = new ArrayList<CuentaMonexTO>();

    /**
     * Beneficiario.
     */
    private Beneficiario1a1TO destinatario = new Beneficiario1a1TO();
    
    /**
     * Beneficiario Ripple.
     */
    private BeneficiarioRippleTO destinRipple = new BeneficiarioRippleTO();

   /**
     * Detalle de beneficiario (Ripple).
     */
    private DetalleBeneficiarioRippleTO detBenefRpp = new DetalleBeneficiarioRippleTO();

    /**
     * Flag que indica si la eliminaci�n se hizo en forma correcta.
     */
    private boolean eliminacionCorrecta;
    
    /**
     * Flag que indica si la eliminaci�n se hizo en forma erronea.
     */
    private boolean eliminacionError;
    
    /**
     * Mensaje de error si la eliminacion es erronea.
     */
    private String mensajeErrorEliminacion;

    /**
     * Fecha valuta.
     */
    private List<ValutaTO> fechasValutas = new ArrayList<ValutaTO>();

    /**
     * Lista de gastos de banco exterior.
     */
    private List<GastosBancoExteriorTO> gastosBancoExterior = new ArrayList<GastosBancoExteriorTO>();

    /**
     * Lista de monedas.
     */
    private List<MonedasPermitidasTO> monedasRipple = new ArrayList<MonedasPermitidasTO>();
    
    /**
     * Datos del Nodo Ripple.
     */
    private NodosRippleTO nodoRipple = new NodosRippleTO();
    
    public void setNodoRipple(NodosRippleTO nodoRipple) {
		this.nodoRipple = nodoRipple;
	}

    /**
     * Objeto con lista de monedas correspondientes asociada a un nodo.
     */
	private MonedasNodoTO monedasNodo = new MonedasNodoTO();

    /**
     * Lista de monedas.
     */
    private List<Moneda1a1TO> monedasOtrasCuentas = new ArrayList<Moneda1a1TO>();

    /**
     * Flag detalle beneficiario.
     */
    private Boolean muestraDetalleBeneficiario = Boolean.FALSE;

    /**
     * Flag que indica si apoderado tiene firmas pendientes.
     */
    private boolean pedienteFirma = false;
    
    
    /**
     * Flag que indica si despliega comprobante desde autorizar pendientes.
     */
    private boolean autorizaTransferenciaPen;

    /**
     * Entidad de transferencia.
     */
    private TransferenciaMonexTO transferenciaMonexTO;

    /**
     * Transferencia seleccionada.
     */
    private TransferenciaTO transferenciaSelected;

    /**
     * Lista de transferencia pendientes.
     */
    private List<TransferenciaTO> transferenciasPendientes = new ArrayList<TransferenciaTO>();

    /**
     * Lista de transferencias.
     */
    private List<TransferenciaTO> transferenciasRealizadas = new ArrayList<TransferenciaTO>();

    /**
     * Lista de transferencias filtro.
     */
    private List<TransferenciaTO> transferenciasRealizadasFiltro = new ArrayList<TransferenciaTO>();

    /**
     * Lista con los nombres que han firmado una transferencia.
     */
    private List<String> apoderadosQueHanFirmado = new ArrayList<String>();

    /**
     * Indica el origen de navegacion para la cartola de transferencia. Si tiene valor el origen proviene desde el
     * menu.
     */
    private String navegacion;

    /**
     * C�digo de Moneda.
     */
    private String codigoMoneda;
    
    /**
     * Nombre de moneda del beneficiario.
     */
    private String nombreMoneda;

    /**
     * Referencia de beneficiario.
     */
    private String referencia;

    /**
     * RUT de beneficiario.
     */
    private String rut;
    
    /**
     * C�digo de pa�s del banco del beneficiario.
     */
    private String codigoPaisBanco;
    
    /**
     * Nombre de pa�s del banco del beneficiario.
     */
    private String nombrePaisBanco;

    /**
     * Identificador del beneficiario seleccionado.
     */
    private String identificadorBeneficiario;
    
    /**
     * Variable que evalua si hay error de negocio.
     */
    private boolean mostrarSegundaClave=false;
    
    /**
     * Variable que evalua si hay error de negocio.
     */
    private boolean errorNegocio=false;
    
    /**
     * Variable que contiene el error de negocio obtenido.
     */
    private String mensajeErrorNegocio;
    
    /**
     * Variable que evalua el origen de la confirmaci?n.
	*/
    private String confirmacion;
    
    /**
 	 * Registros por p�gina.
 	*/
     private Integer registrosPorPagina = 10;
     
     /**
      * montoTransferencia
      */
     private BigDecimal montoTransferencia;
    
    /**
     *  Variable de control para seleccion de radio chequeado
     */
    private String radioSeleccionado;
    
    /**
     * Variable que evalua el origen del llamado al comprobante.
     */
    private String origenComprobante;
    
    /**
     * Variable que trae el valor de la transaccion ripple.
     */
    private String estadoTransaccion;
    
    /**
     * Fecha actual que se muestra en comprobante.
     */
    private Date fechaActual;
    
    public String getIdentificadorBeneficiario() {
        return identificadorBeneficiario;
    }

    public void setIdentificadorBeneficiario(String identificadorBeneficiario) {
        this.identificadorBeneficiario = identificadorBeneficiario;
    }
    
    /**
     * Datos de la comision.
     */
    private wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.ComisionTO comisionRipple = new wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.ComisionTO();
    

    public wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.ComisionTO getComisionRipple() {
		return comisionRipple;
	}

	public void setComisionRipple(
			wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.ComisionTO comisionRipple) {
		this.comisionRipple = comisionRipple;
	}

	/**
     * log de la clase TransferenciasMonexUtilityMB.
     */
    private transient Logger log = Logger.getLogger(TransferenciasRippleViewMB.class);
	
	/**
     * Busca el destinatario por el identificador dentro de la lista de beneficiarios disponible.
     * <p>
     * <b>Registro de versiones:</b>
     * <ul>
     * <li>1.0 28/02/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft. BCI): versi�n inicial</li>
     * </ul>
     * </p>
     * @since 1.0
     */
    public void buscarDestinatarioSeleccionado() {
        String nombreMetodo = "buscarDestinatarioSeleccionado";
		
		if(getLogger().isEnabledFor(Level.INFO)){
			getLogger().info("[" + nombreMetodo + "] [BCI_INI]");
		}
        for (BeneficiarioRippleTO beneficiario : this.beneficiariosRipple) {
            if (beneficiario.getIdentificacion().equals(destinRipple.getIdentificacion())) {
            	destinRipple.setBicBanco(beneficiario.getBicBanco());
            	destinRipple.setCtaCorriente(beneficiario.getCtaCorriente());
            	destinRipple.setNombre(beneficiario.getNombre());
            	destinRipple.setPaisBanco(beneficiario.getPaisBanco());
                break;
            }
        }
        if(getLogger().isEnabledFor(Level.INFO)){
           getLogger().info("[" + nombreMetodo + "] [BCI_FINOK]");
		}	
    }

    public List<Beneficiario1a1TO> getBeneficiarios() {
        return beneficiarios;
    }

    public List<CodigoCambioTO> getCodigosCambio() {
        return codigosCambio;
    }

    public ComisionTO getComision() {
        return comision;
    }

    public List<CuentaMonexTO> getCuentasComision() {
        return cuentasComision;
    }

    public List<CuentaMonexTO> getCuentasOrigen() {
        return cuentasOrigen;
    }

    public Beneficiario1a1TO getDestinatario() {
        return destinatario;
    }
    
    public BeneficiarioRippleTO getDestinRipple() {
        return destinRipple;
    }

    public BeneficiarioRippleTO setDestinRipple() {
        return destinRipple;
    }
    
    public List<ValutaTO> getFechasValutas() {
        return fechasValutas;
    }

    public List<GastosBancoExteriorTO> getGastosBancoExterior() {
        return gastosBancoExterior;
    }

    
    public List<MonedasPermitidasTO> getMonedasRipple() {
        return monedasRipple;
    }
    
    public NodosRippleTO getNodoRipple() {
    	return nodoRipple;
    }

    public List<Moneda1a1TO> getMonedasOtrasCuentas() {
        return monedasOtrasCuentas;
    }

    public Boolean getMuestraDetalleBeneficiario() {
        return muestraDetalleBeneficiario;
    }

    public TransferenciaMonexTO getTransferenciaMonexTO() {
        return transferenciaMonexTO;
    }

    public TransferenciaTO getTransferenciaSelected() {
        return transferenciaSelected;
    }

    public List<TransferenciaTO> getTransferenciasPendientes() {
        return transferenciasPendientes;
    }

    public List<TransferenciaTO> getTransferenciasRealizadas() {
        return transferenciasRealizadas;
    }

    public List<TransferenciaTO> getTransferenciasRealizadasFiltro() {
        return transferenciasRealizadasFiltro;
    }

    public List<String> getApoderadosQueHanFirmado() {
        return apoderadosQueHanFirmado;
    }

    /**
     * Flag eliminacion.
     * <p>
     * <b>Registro de versiones:</b>
     * <ul>
     * <li>1.0 28/02/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft. BCI): versi�n inicial</li>
     * </ul>
     * </p>
     * @return boolean
     * @since 1.0
     */
    public boolean isEliminacionCorrecta() {
        if (this.getTransferenciaSelected() == null) {
            this.setEliminacionCorrecta(Boolean.TRUE);
        }
        else {
            this.setEliminacionCorrecta(Boolean.FALSE);
        }
        return eliminacionCorrecta;
    }

    public boolean isPedienteFirma() {
        return pedienteFirma;
    }

    public void setBeneficiarios(List<Beneficiario1a1TO> beneficiarios) {
        this.beneficiarios = beneficiarios;
    }
    
    public void setBeneficiariosRipple(List<BeneficiarioRippleTO> beneficiariosRipple) {
        this.beneficiariosRipple = beneficiariosRipple;
    }
    
    public List<BeneficiarioRippleTO> getBeneficiariosRipple() {
		return beneficiariosRipple;
	}

	public void setCodigosCambio(List<CodigoCambioTO> codigosCambio) {
        this.codigosCambio = codigosCambio;
    }

    public void setComision(ComisionTO comision) {
        this.comision = comision;
    }

    public void setCuentasComision(List<CuentaMonexTO> cuentasComision) {
        this.cuentasComision = cuentasComision;
    }

    public void setCuentasOrigen(List<CuentaMonexTO> cuentasOrigen) {
        this.cuentasOrigen = cuentasOrigen;
    }

    public void setDestinatario(Beneficiario1a1TO destinatario) {
        this.destinatario = destinatario;
    }

    public DetalleBeneficiarioRippleTO getDetBenefRpp() {
		return detBenefRpp;
	}

	public void setDetBenefRpp(DetalleBeneficiarioRippleTO detBenefRpp) {
		this.detBenefRpp = detBenefRpp;
	}
	
	public void setDestinRipple(BeneficiarioRippleTO destinRipple) {
        this.destinRipple = destinRipple;
    }

	public Integer getRegistrosPorPagina() {
		return registrosPorPagina;
	}

	public void setRegistrosPorPagina(Integer registrosPorPagina) {
		this.registrosPorPagina = registrosPorPagina;
	}

    public void setEliminacionCorrecta(boolean eliminacionCorrecta) {
        this.eliminacionCorrecta = eliminacionCorrecta;
    }

    public void setFechasValutas(List<ValutaTO> fechasValutas) {
        this.fechasValutas = fechasValutas;
    }

    public void setGastosBancoExterior(List<GastosBancoExteriorTO> gastosBancoExterior) {
        this.gastosBancoExterior = gastosBancoExterior;
    }

    public void setMonedasOtrasCuentas(List<Moneda1a1TO> monedasOtrasCuentas) {
        this.monedasOtrasCuentas = monedasOtrasCuentas;
    }

    public void setMuestraDetalleBeneficiario(Boolean muestraDetalleBeneficiario) {
        this.muestraDetalleBeneficiario = muestraDetalleBeneficiario;
    }

    public void setPedienteFirma(boolean pedienteFirma) {
        this.pedienteFirma = pedienteFirma;
    }

    public void setTransferenciaMonexTO(TransferenciaMonexTO transferenciaMonexTO) {
        this.transferenciaMonexTO = transferenciaMonexTO;
    }

    public void setTransferenciaSelected(TransferenciaTO transferenciaSelected) {
        this.transferenciaSelected = transferenciaSelected;
    }

    public void setTransferenciasPendientes(List<TransferenciaTO> transferenciasPendientes) {
        this.transferenciasPendientes = transferenciasPendientes;
    }

    public void setTransferenciasRealizadas(List<TransferenciaTO> transferenciasRealizadas) {
        this.transferenciasRealizadas = transferenciasRealizadas;
    }

    public void setTransferenciasRealizadasFiltro(List<TransferenciaTO> transferenciasRealizadasFiltro) {
        this.transferenciasRealizadasFiltro = transferenciasRealizadasFiltro;
    }

    public void setApoderadosQueHanFirmado(List<String> apoderadosQueHanFirmado) {
        this.apoderadosQueHanFirmado = apoderadosQueHanFirmado;
    }

    public String getNavegacion() {
        return navegacion;
    }

    public void setNavegacion(String navegacion) {
        this.navegacion = navegacion;
    }

    public String getCodigoMoneda() {
        return codigoMoneda;
    }

    public void setCodigoMoneda(String codigoMoneda) {
        this.codigoMoneda = codigoMoneda;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getCodigoPaisBanco() {
        return codigoPaisBanco;
    }

    public void setCodigoPaisBanco(String codigoPaisBanco) {
        this.codigoPaisBanco = codigoPaisBanco;
    }

    public String getNombreMoneda() {
        return nombreMoneda;
    }

    public void setNombreMoneda(String nombreMoneda) {
        this.nombreMoneda = nombreMoneda;
    }

    public String getNombrePaisBanco() {
        return nombrePaisBanco;
    }

    public void setNombrePaisBanco(String nombrePaisBanco) {
        this.nombrePaisBanco = nombrePaisBanco;
    }

	public boolean isEliminacionError() {
		
        
		return eliminacionError;
	}

	public void setEliminacionError(boolean eliminacionError) {
		this.eliminacionError = eliminacionError;
	}

	public String getMensajeErrorEliminacion() {
		return mensajeErrorEliminacion;
	}

	public void setMensajeErrorEliminacion(String mensajeErrorEliminacion) {
		this.mensajeErrorEliminacion = mensajeErrorEliminacion;
	}

	public MonedasNodoTO getMonedasNodo() {
		return monedasNodo;
	}

	 /**
     * <p>
     * Establece el valor de las variables asociadas a Ripple.
     * </p>
     * <p>
     * <b>Registro de versiones:</b>
     * <ul>
     * <li>1.0 28/02/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft. BCI): versi�n inicial</li>
     * </ul>
     * </p>
     * @param monedasNodo monedas disponibles en el nodo Ripple
     * @since 1.0
     */
	public void setMonedasNodo(MonedasNodoTO monedasNodo) {
		this.monedasNodo = monedasNodo;
		this.monedasRipple = monedasNodo.getLstMonedas();
        this.nodoRipple = monedasNodo.getNodo();
	}

	public void setMonedasRipple(List<MonedasPermitidasTO> monedasRipple) {
		this.monedasRipple = monedasRipple;
	}   
	    
    public BigDecimal getMontoTransferencia() {
		return montoTransferencia;
	}

	public void setMontoTransferencia(BigDecimal montoTransferencia) {
		this.montoTransferencia = montoTransferencia;
	}
	
    @PostConstruct
    public void setearTransferenciaMonexTO() {

        TransferenciasRippleViewMB transferenciasRippleViewMB = (TransferenciasRippleViewMB) FacesContext
                .getCurrentInstance().getExternalContext().getRequestMap().get(BEAN_NAME);
        if(transferenciasRippleViewMB != null){
	        setOrigenComprobante(transferenciasRippleViewMB.getConfirmacion());
	        setTransferenciaSelected(transferenciasRippleViewMB.getTransferenciaSelected());
	        setEstadoTransaccion(transferenciasRippleViewMB.estadoTransaccion);
        }

    }
	
	/**
     * <p>
     * M�todo get del logger.
     * </p>
     * <p>
     * <b>Registro de versiones:</b>
     * <ul>
     * <li>1.0 28/02/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft. BCI): versi�n inicial</li>
     * </ul>
     * </p>
     * @return Logger
     * @since 1.0
     */
    private Logger getLogger() {
        if (log == null) {
            log = Logger.getLogger(this.getClass());
        }
        return log;
    }

	public boolean isAutorizaTransferenciaPen() {
		return autorizaTransferenciaPen;
	}

	public void setAutorizaTransferenciaPen(boolean autorizaTransferenciaPen) {
		this.autorizaTransferenciaPen = autorizaTransferenciaPen;
	}

	public String getConfirmacion() {
		return confirmacion;
	}

	public void setConfirmacion(String confirmacion) {
		this.confirmacion = confirmacion;
	}
	
	public String getRadioSeleccionado() {
		return radioSeleccionado;
	}

	public void setRadioSeleccionado(String radioSeleccionado) {
		this.radioSeleccionado = radioSeleccionado;
	}
	
	public String getOrigenComprobante() {
		return origenComprobante;
	}

	public void setOrigenComprobante(String origenComprobante) {
		this.origenComprobante = origenComprobante;
	}

	public String getEstadoTransaccion() {
		return estadoTransaccion;
	}

	public void setEstadoTransaccion(String estadoTransaccion) {
		this.estadoTransaccion = estadoTransaccion;
	}

	public Date getFechaActual() {
		return fechaActual;
	}

	public void setFechaActual(Date fechaActual) {
		this.fechaActual = fechaActual;
	}

	public boolean isErrorNegocio() {
		return errorNegocio;
	}

	public void setErrorNegocio(boolean errorNegocio) {
		this.errorNegocio = errorNegocio;
	}

	public String getMensajeErrorNegocio() {
		return mensajeErrorNegocio;
	}

	public void setMensajeErrorNegocio(String mensajeErrorNegocio) {
		this.mensajeErrorNegocio = mensajeErrorNegocio;
	}

	public boolean isMostrarSegundaClave() {
		return mostrarSegundaClave;
	}

	public void setMostrarSegundaClave(boolean mostrarSegundaClave) {
		this.mostrarSegundaClave = mostrarSegundaClave;
	}
	
}