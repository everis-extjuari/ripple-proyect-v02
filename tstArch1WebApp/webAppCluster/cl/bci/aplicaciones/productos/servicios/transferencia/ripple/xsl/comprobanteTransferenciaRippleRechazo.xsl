<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" version="1.0">
	<xsl:param name="img1" />
	<xsl:template match="ComprobanteTransferencia">
		<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
		<fo:layout-master-set>
				<!-- layout for the first page -->
				<fo:simple-page-master master-name="first"
					page-height="29.7cm" page-width="21cm" margin-top="0cm"
					margin-bottom="2cm" margin-left="2.5cm" margin-right="2.5cm"
					border="thick solid red">
					<fo:region-body margin-top="2cm" />
					<fo:region-before extent="2cm" />
					<fo:region-after extent="2cm" />
				</fo:simple-page-master>
				<fo:page-sequence-master master-name="basicPSM">
					<fo:repeatable-page-master-alternatives>
						<fo:conditional-page-master-reference
							master-reference="first" page-position="first" />
					</fo:repeatable-page-master-alternatives>
				</fo:page-sequence-master>
			</fo:layout-master-set>
			<!-- end: defines page layout -->
			<!-- actual layout -->
			<fo:page-sequence master-reference="basicPSM">
				<fo:flow flow-name="xsl-region-body">
					<fo:block border-top="dashed 0.0pt #FFFFFF" border-bottom="dashed 0.0pt #FFFFFF"
						border-right="dashed 0.0pt #FFFFF" border-left="dashed 0.0pt #FFFFFF"
						space-before="5mm" space-after="5mm"
						line-height="15pt"
						background-color="#EEEEEE">
						<fo:table text-align="left" padding-bottom="25pt">
							<fo:table-column column-width="4cm" />
							<fo:table-column column-width="6cm" />
							<fo:table-column column-width="6cm" />
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell padding-top="25pt" padding-left="8pt">
										<xsl:element name="fo:external-graphic">
											<xsl:attribute name="src" >
											url('<xsl:value-of select="$img1" />')</xsl:attribute>
											<xsl:attribute name="height">4cm</xsl:attribute>
											<xsl:attribute name="width">4cm</xsl:attribute>
										</xsl:element>
									</fo:table-cell>
									<fo:table-cell text-align="left" padding-top="8pt" padding-left="8pt" number-columns-spanned="2">
										<fo:block>
											<fo:table text-align="left" font-family="arial" font-size="10pt">
												<fo:table-column column-width="6cm" />
												<fo:table-column column-width="6cm" />
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell text-align="left" padding-top="8pt" padding-left="1pt" number-columns-spanned="2">
															<fo:block>
															<xsl:value-of select="mensaje" />
															
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell text-align="left" padding-top="4pt" padding-left="1pt"
														 number-columns-spanned="2">
															<fo:block>
															Fecha
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row font-weight="bold" font-family="arial" font-size="10pt">
														<fo:table-cell text-align="left" padding-top="0pt" padding-left="1pt"
														number-columns-spanned="2">
															<fo:block>
															<xsl:value-of select="fecha" />
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
					</fo:block>
				</fo:flow>
			</fo:page-sequence>
		</fo:root>
	</xsl:template>	
</xsl:stylesheet>
