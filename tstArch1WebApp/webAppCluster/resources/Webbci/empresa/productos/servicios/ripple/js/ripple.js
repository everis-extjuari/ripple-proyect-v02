$(document)
		.ready(
				function() {
					$('.validaciones-entrada')
							.click(
									function() {

										var selects = $('select[class~=validar]');
										for (var i = 0; i < selects.length; i++) {
											if (!$(selects[i]).val()
													|| $(selects[i]).val() == 0) {
												$(selects[i]).closest(
														'.form-group')
														.addClass('has-error');
												$(selects[i]).focus();
												$(selects[i])
														.change(
																function() {
																	$(
																			selects[i])
																			.closest(
																					'.form-group')
																			.removeClass(
																					'has-error');
																});
												return false;
											}
										}

										var ValidMonto = $('input[class~=validartext]');
										console.log("valida monto");
										console.log(parseFloat(ValidMonto.val()
												.replace('.', '')));
										console
												.log(!ValidMonto
														|| isNaN(parseFloat(ValidMonto
																.val()))
														|| (parseFloat(ValidMonto
																.val()
																.replace('.',
																		'')) < 1.00 || parseFloat(ValidMonto
																.val()
																.replace('.',
																		'')) > 100000.00));
										if (!ValidMonto
												|| isNaN(parseFloat(ValidMonto
														.val()))
												|| (parseFloat(ValidMonto.val()
														.replace('.', '')) < 1.00 || parseFloat(ValidMonto
														.val().replace('.', '')) > 100000.00)) {
											ValidMonto.focus();
											ValidMonto.closest('.form-group')
													.addClass('has-error');
											ValidMonto.keypress(function() {
												ValidMonto.closest(
														'.form-group')
														.removeClass(
																'has-error');
											});
											return false;
										}


										var motivo = document.getElementById("transferForm:motivo").value;
										   console.log(motivo);
										console.log('validar motivo con regex');
										
										var validaRegex = /^[A-Za-z0-9 ]+$/.test(motivo);
										console.log("validaRegex: ",validaRegex);
										
										var mt = $('textarea[class~=validartextarea]');
										
										if (!motivo || motivo == '' || motivo.trim() == '' || validaRegex== false ) {
											console.log(motivo);
											var mt = $('textarea[class~=validartextarea]');
											console.log("mt: ",mt);
											mt.focus();																	
											mt.closest('.form-group').addClass('has-error');
											mt.keypress(function () {
												mt.closest('.form-group').removeClass('has-error');
											});
											return false;						                	
										}
										

									});

				});

function validarTextarea(objetoTextarea, maxLength) {
	console.log('validarTextarea');
	if (objetoTextarea.value.length > maxLength) {
		objetoTextarea.value = objetoTextarea.value.substr(0, maxLength);
	}
	return false;
}

function formatNumero(field) {
	var number = new String(field.value);
	var nuevoNumero = number.replace(/\./g, ""); // quita todos los
	// puntos de la
	// cadena
	var posicion = posicion = nuevoNumero.indexOf(",");
	var decimales;

	if (posicion >= 0) {

		decimales = nuevoNumero.substring(posicion + 1, nuevoNumero.length);
		nuevoNumero = nuevoNumero.substring(0, nuevoNumero.indexOf(","));
	} else {
		decimales = '00';
	}

	var result = "";
	var cont = 0;

	// Elimina Ceros izquierda

	for (var i = 0; i < nuevoNumero.length; i++) {
		if (nuevoNumero[i] == '0') {
			cont++;
		} else {
			break;
		}
	}

	nuevoNumero = nuevoNumero.substring(cont, nuevoNumero.length);

	while (nuevoNumero.length > 3) {
		result = "." + nuevoNumero.substr(nuevoNumero.length - 3) + result;
		nuevoNumero = nuevoNumero.substring(0, nuevoNumero.length - 3);
	}

	result = nuevoNumero + result;

	if (result.length > 15) {
		result = "";
	}

	if (decimales.length > 2) {
		decimales = decimales.substring(0, 2);

	}
	if (result.length > 0) {
		result = result + "," + decimales;
	}

	field.value = result;
}

function validarNumeros(evt) {
	console.log('validarNumeros');
	var charCode = (evt.which) ? evt.which : evt.keyCode;

	if ((charCode > 47 && charCode < 58) || (charCode == 8) || (charCode == 13)
			|| (charCode == 9) || (charCode == 44)) {
		return true;
	} else {
		return false;
	}
}

function confirmarFirmar() {
	$('#confirmaFirma').css('display', 'block');
	return;
}

function validarFechas() {
	console.log("validarFechas");
	return;
}

function seleccionCheckBox() {
	var borrar = document.getElementById('panoramaForm:vSele').value;
	var nb = borrar.replace('panoramaForm:tablaPendiente:', '').replace(
			':rbCheck', '');
	if (borrar != "") {
		if (document.getElementById(borrar) != null) {
			document.getElementById(borrar).checked = false;
		}
		document.getElementById('panoramaForm:vSele').value = "";
		document.getElementById('panoramaForm:btnFirmar').disabled = true;
		document.getElementById('panoramaForm:btnEliminar').disabled = true;
		$('#confirmaFirma').css('display', 'none');

	}
	var ini = -1;
	var j = 0;
	while (ini == -1) {
		if (document.getElementById('panoramaForm:tablaPendiente:' + j
				+ ':rbCheck') != null) {
			ini = 0;
		} else {
			j = j + 10;
		}
	}
	length = document.getElementById('panoramaForm:tablaPendiente')
			.getElementsByTagName("tr").length
			- 1 + j;
	for (var i = j; i < length; i++) {
		if (document.getElementById('panoramaForm:tablaPendiente:' + i
				+ ':rbCheck') != null
				&& document.getElementById('panoramaForm:tablaPendiente:' + i
						+ ':rbCheck').checked) {
			document.getElementById('panoramaForm:vSele').value = document
					.getElementById('panoramaForm:tablaPendiente:' + i
							+ ':rbCheck').id;
			document.getElementById('panoramaForm:tablaPendiente:' + i
					+ ':rbCheck').checked = true;
			document.getElementById('panoramaForm:btnFirmar').disabled = false;
			document.getElementById('panoramaForm:btnEliminar').disabled = false;
			break;
		}
	}
}

$(document)
		.ready(
				function() {
					$('.checkbox-radio')
							.click(
									function() {
										var borrar = document
												.getElementById('panoramaForm:vSele').value;
										var nb = borrar.replace(
												'panoramaForm:tablaPendiente:',
												'').replace(':rbCheck', '');

										var ind = (parseInt(nb) + 1);
										console.log("Indice: " + ind);
										var table = document
												.getElementById('panoramaForm:tablaPendiente');

										var mm = document.getElementById(
												'panoramaForm:tablaPendiente')
												.getElementsByTagName("tr")[ind].cells[1].innerText;
										document
												.getElementById('panoramaForm:iHNumeroCorrelativo').value = mm;
									});
				});

$(document)
		.ready(
				// Esta funcion esta hecha debido que Primefaces coloca un valor
				// por defecto en
				// el buscador inteligente
				function() {
					$('.filtering')
							.click(
									function() {
										document
												.getElementById('panoramaForm:tablaPendiente:globalFilter').value = "";
									});
				});

$(document)
		.ready(

				function() {
					$('.ingresar-a-confirmar')
							.click(
									function() {
										// Datos del destinatario
										document
												.getElementById("transferForm:iHNombreDest").value = document
												.getElementById("transferForm:iHDestinatario").value;
										document
												.getElementById("transferForm:iHPaisCuenta").value = document
												.getElementById("transferForm:paisCuenta").value;
										document
												.getElementById("transferForm:iHNumeroDeCuentaIBAN").value = document
												.getElementById("transferForm:numeroDeCuentaIBAN").value;
										document
												.getElementById("transferForm:iHRefDest").value = document
												.getElementById("transferForm:iHIdentDest").value;

										// Datos del beneficiario
										document
												.getElementById("transferForm:iHNombre").value = document
												.getElementById("transferForm:nombre").value;
										document
												.getElementById("transferForm:iHDireccion").value = document
												.getElementById("transferForm:direccion").value;
										document
												.getElementById("transferForm:iHMail").value = document
												.getElementById("transferForm:mail").value;
										document
												.getElementById("transferForm:iHPais").value = document
												.getElementById("transferForm:pais").value;

										// Datos del banco beneficiario
										document
												.getElementById("transferForm:iHBIC").value = document
												.getElementById("transferForm:bic").value;
										document
												.getElementById("transferForm:iHNombreDireccion").value = document
												.getElementById("transferForm:nombreD").value
												+ "/"
												+ document
														.getElementById("transferForm:nDireccion").value;

										var el = document
												.getElementById('transferForm:selectCodigosCambio');
										document
												.getElementById("transferForm:iHCodCambio").value = el.options[el.selectedIndex].innerHTML;

										// Faltantes
										document
												.getElementById("transferForm:iHNCodMon").value = document
												.getElementById("transferForm:codigoIso").value;
										document
												.getElementById("transferForm:iHNCtaOrig").value = document
												.getElementById("transferForm:numeroCuentaOrigen").value;
										document
												.getElementById("transferForm:iHSalSim").value = document
												.getElementById("transferForm:simboloMoneda").value
												+ " "
												+ document
														.getElementById("transferForm:saldoDisponible").value;

										var selMoneda = document
												.getElementById("transferForm:codigoIso");
										document
												.getElementById("transferForm:iHNNomMon").value = selMoneda.options[selMoneda.selectedIndex].innerHTML;

										document
												.getElementById("transferForm:iHMontoTransf").value = document
												.getElementById("transferForm:ValidMonto").value;
									});
				});

function seleccionado(evento) {
	var idFila = $("input[name=" + evento.name + "]").attr("class");
	alert(idFila);
	var objetoFila = new Fila();
	objetoFila.idFila = idFila;
}

function despliega(ev) {
	alert(idFila);
	$("#display").show();
}

$(document)
		.ready(
				function() {
					$('.btnConfirmar')
							.click(
									function() {
										var selMoneda = document
												.getElementById("form_contratar:monedasComisionN");
										document
												.getElementById("transferForm:iHCuentaComision").value = selMoneda.options[selMoneda.selectedIndex].innerHTML;

									});

				});

$(document)
		.ready(
				function() {
					$('.checkbox-selection')
							.click(
									function() {
										$('#confirmaFirma').css('display',
												'none');
										var borrar = document
												.getElementById('panoramaForm:vSele').value;
										var nb = borrar.replace(
												'panoramaForm:tablaPendiente:',
												'').replace(':rbCheck', '');
										if (borrar != "") {
											if (document.getElementById(borrar) != null) {
												document.getElementById(borrar).checked = false;
											}
											document
													.getElementById('panoramaForm:vSele').value = "";
											document
													.getElementById('panoramaForm:btnFirmar').disabled = true;
											document
													.getElementById('panoramaForm:btnEliminar').disabled = true;
											$('#confirmaFirma').css('display',
													'none');

										}
										var ini = -1;
										var j = 0;
										while (ini == -1) {
											if (document
													.getElementById('panoramaForm:tablaPendiente:'
															+ j + ':rbCheck') != null) {
												ini = 0;
											} else {
												j = j + 10;
											}
										}
										length = document.getElementById(
												'panoramaForm:tablaPendiente')
												.getElementsByTagName("tr").length
												- 1 + j;
										for (var i = j; i < length; i++) {
											if (document
													.getElementById('panoramaForm:tablaPendiente:'
															+ i + ':rbCheck') != null
													&& document
															.getElementById('panoramaForm:tablaPendiente:'
																	+ i
																	+ ':rbCheck').checked) {
												document
														.getElementById('panoramaForm:vSele').value = document
														.getElementById('panoramaForm:tablaPendiente:'
																+ i
																+ ':rbCheck').id;
												document
														.getElementById('panoramaForm:tablaPendiente:'
																+ i
																+ ':rbCheck').checked = true;
												document
														.getElementById('panoramaForm:btnFirmar').disabled = false;
												document
														.getElementById('panoramaForm:btnEliminar').disabled = false;
												break;
											}
										}
									});
				});

$(document)
		.ready(
				function() {
					$('.select-sc')
							.click(
									function() {

										var borrar = document
												.getElementById('panoramaForm:vSele').value;
										var nb = borrar.replace(
												'panoramaForm:tablaPendiente:',
												'').replace(':rbCheck', '');
										if (borrar != "") {
											if (document.getElementById(borrar) != null) {
												document.getElementById(borrar).checked = false;
											}
											document
													.getElementById('panoramaForm:vSele').value = "";
											document
													.getElementById('panoramaForm:btnFirmar').disabled = true;
											document
													.getElementById('panoramaForm:btnEliminar').disabled = true;
											$('#confirmaFirma').css('display',
													'none');

										}

										var openEnderContent = '&lt;p&gt;&lt;span style="color: transparent;"&gt;DDD&lt;/span&gt;!!!!!&lt;strong&gt;666666666666&lt;/strong&gt;&lt;/p&gt;';

										$('#tf_confirmacion').html(
										// create an element where the html
										// content as the string
										$('<div/>', {
											html : openEnderContent
										// get text content from element for
										// decoded text
										}).text())

									});
				});

function segundC() {
	var openEnderContent = '';
	$('#tf_confirmacion').html(
	// create an element where the html content as the string
	$('<div/>', {
		html : openEnderContent
	// get text content from element for decoded text
	}).text())
	return;
}
