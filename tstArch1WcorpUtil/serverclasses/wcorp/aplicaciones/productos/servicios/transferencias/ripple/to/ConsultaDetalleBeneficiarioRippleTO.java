package wcorp.aplicaciones.productos.servicios.transferencias.ripple.to;

import java.io.Serializable;

/**
* Información del detalle consulta Beneficiario Ripple
* <p>
* Registro de versiones:
* <ul>
* <li>1.0 20/02/2019 Danilo Dominguez (Everis) - Minheli Mejias(Ing. de Soft. BCI): Versión inicial.</li>
* </ul>
* <p>
* <B>Todos los derechos reservados por Banco de Crédito e Inversiones.</B>
* <p>
*/
public class ConsultaDetalleBeneficiarioRippleTO implements Serializable {

	/**
	 * Id de Serializacion. 
	 */
	private static final long serialVersionUID = 5071205632679014271L;
	/**
	 * rutEmpresa.
	 */
	private long rutEmpresa;
	/**
	 * dvEmpresa. 
	 */
	private char dvEmpresa;
	/**
	 * identificadorBeneficiario.
	 */
	private String identificadorBeneficiario;
	/**
	 * codIso.
	 */
	private String codIso;
	/**
	 * paisBanco.
	 */
	private String paisBanco;
	
	public long getRutEmpresa() {
		return rutEmpresa;
	}
	public void setRutEmpresa(long rutEmpresa) {
		this.rutEmpresa = rutEmpresa;
	}
	public char getDvEmpresa() {
		return dvEmpresa;
	}
	public void setDvEmpresa(char dvEmpresa) {
		this.dvEmpresa = dvEmpresa;
	}
	public String getIdentificadorBeneficiario() {
		return identificadorBeneficiario;
	}
	public void setIdentificadorBeneficiario(String identificadorBeneficiario) {
		this.identificadorBeneficiario = identificadorBeneficiario;
	}
	public String getCodIso() {
		return codIso;
	}
	public void setCodIso(String codIso) {
		this.codIso = codIso;
	}
	public String getPaisBanco() {
		return paisBanco;
	}
	public void setPaisBanco(String paisBanco) {
		this.paisBanco = paisBanco;
	}
    /**
     * Representación String de entidad.
     * @return String
     */
	public String toString() {
		String salida = ""
		+ "[rutEmpresa = " + this.rutEmpresa + "]\n" 
		+ "[dvEmpresa = " + this.dvEmpresa + "]\n"
		+ "[identificadorBeneficiario = " + this.identificadorBeneficiario + "]\n" 
		+ "[codIso = " + this.codIso + "]\n" 
		+ "[paisBanco = " + this.paisBanco + "]\n"; 
		return salida;
	}
	
}
