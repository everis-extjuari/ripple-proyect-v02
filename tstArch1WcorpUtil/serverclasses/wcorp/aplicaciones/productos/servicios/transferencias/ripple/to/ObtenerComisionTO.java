package wcorp.aplicaciones.productos.servicios.transferencias.ripple.to;

import java.io.Serializable;

/**
* Comision Ripple.
*
* <p>
* Registro de versiones:
* <ul>
* <li>1.0 27/02/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft. BCI): Versi�n inicial.</li>
* </ul>
* <p>
* <B>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</B>
* <p>
*
*/
public class ObtenerComisionTO implements Serializable{
	/**
	 * Id de serializacion.
	 */
	private static final long serialVersionUID = 1835830285620415453L;
	/**
	 * rutEmpresa.
	 */
	private String rutEmpresa;
	/**
	 * monedaTransf.
	 */
	private String monedaTransf;
	/**
	 * montoPago.
	 */
	private String montoPago;
	/**
	 * bicBanco.
	 */
	private String bicBanco;
	
	public String getRutEmpresa() {
		return rutEmpresa;
	}
	public void setRutEmpresa(String rutEmpresa) {
		this.rutEmpresa = rutEmpresa;
	}
	public String getMonedaTransf() {
		return monedaTransf;
	}
	public void setMonedaTransf(String monedaTransf) {
		this.monedaTransf = monedaTransf;
	}
	public String getMontoPago() {
		return montoPago;
	}
	public void setMontoPago(String montoPago) {
		this.montoPago = montoPago;
	}
	public String getBicBanco() {
		return bicBanco;
	}
	public void setBicBanco(String bicBanco) {
		this.bicBanco = bicBanco;
	}
    /**
     * Representaci�n String de entidad.
     * @return String
     */
	public String toString() {
		String salida = ""
		+ "[rutEmpresa = " + this.rutEmpresa + "]\n" 
		+ "[monedaTransf = " + this.monedaTransf + "]\n"
		+ "[montoPago = " + this.montoPago + "]\n"
		+ "[bicBanco = " + this.bicBanco + "]\n";
		return salida;
	}
	
}
