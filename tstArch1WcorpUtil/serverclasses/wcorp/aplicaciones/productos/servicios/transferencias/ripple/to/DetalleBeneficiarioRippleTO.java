package wcorp.aplicaciones.productos.servicios.transferencias.ripple.to;

import java.io.Serializable;

/**
* Detalle Beneficiario Ripple
* <p>
* Registro de versiones:
* <ul>
* <li>1.0 20/02/2019 Danilo Dominguez (Everis) - Minheli Mejias(Ing. de Soft. BCI): Versi�n inicial.</li>
* </ul>
* <p>
* <B>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</B>
* <p>
*/
public class DetalleBeneficiarioRippleTO implements Serializable{
	
	
	/**
	 * Id.
	 */
	private static final long serialVersionUID = -7349501488885167836L;
	/**
	 * nombre.
	 */
	private String nombre;
	/**
	 * nombreCont.
	 */
	private String nombreCont;
	/**
	 * direccion.
	 */
	private String direccion;
	/**
	 * direccionCont.
	 */
	private String direccionCont;
	/**
	 * paisResidencia.
	 */
	private String paisResidencia;
	/**
	 * nombrePaisRes.
	 */
	private String nombrePaisRes;
	/**
	 * infoAdicional.
	 */
	private String infoAdicional;
	/**
	 * infoAdicionalCont.
	 */
	private String infoAdicionalCont;
	/**
	 * ctaCorriente.
	 */
	private String ctaCorriente;
	/**
	 * banco.
	 */
	private String banco;
	/**
	 * nombreBanco.
	 */
	private String nombreBanco;
	/**
	 * direccionBanco.
	 */
	private String direccionBanco;
	/**
	 * email.
	 */
	private String email;
	/**
	 * nombrePaisBco.
	 */
	private String nombrePaisBco;
	/**
	 * ctaCorrienteBenef.
	 */
	private String ctaCorrienteBenef;
	/**
	 * nombreLineaUno.
	 */
	private String nombreLineaUno;
	/**
	 * nombreLineaDos.
	 */
	private String nombreLineaDos;
	/**
	 * nombreLineaTres.
	 */
	private String nombreLineaTres;
	/**
	 * nombreLineaCuatro.
	 */
	private String nombreLineaCuatro;
	
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getNombreCont() {
		return nombreCont;
	}
	public void setNombreCont(String nombreCont) {
		this.nombreCont = nombreCont;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getDireccionCont() {
		return direccionCont;
	}
	public void setDireccionCont(String direccionCont) {
		this.direccionCont = direccionCont;
	}
	public String getPaisResidencia() {
		return paisResidencia;
	}
	public void setPaisResidencia(String paisResidencia) {
		this.paisResidencia = paisResidencia;
	}
	public String getNombrePaisRes() {
		return nombrePaisRes;
	}
	public void setNombrePaisRes(String nombrePaisRes) {
		this.nombrePaisRes = nombrePaisRes;
	}
	public String getInfoAdicional() {
		return infoAdicional;
	}
	public void setInfoAdicional(String infoAdicional) {
		this.infoAdicional = infoAdicional;
	}
	public String getInfoAdicionalCont() {
		return infoAdicionalCont;
	}
	public void setInfoAdicionalCont(String infoAdicionalCont) {
		this.infoAdicionalCont = infoAdicionalCont;
	}
	public String getCtaCorriente() {
		return ctaCorriente;
	}
	public void setCtaCorriente(String ctaCorriente) {
		this.ctaCorriente = ctaCorriente;
	}
	public String getBanco() {
		return banco;
	}
	public void setBanco(String banco) {
		this.banco = banco;
	}
	public String getNombreBanco() {
		return nombreBanco;
	}
	public void setNombreBanco(String nombreBanco) {
		this.nombreBanco = nombreBanco;
	}
	public String getDireccionBanco() {
		return direccionBanco;
	}
	public void setDireccionBanco(String direccionBanco) {
		this.direccionBanco = direccionBanco;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNombrePaisBco() {
		return nombrePaisBco;
	}
	public void setNombrePaisBco(String nombrePaisBco) {
		this.nombrePaisBco = nombrePaisBco;
	}
	public String getCtaCorrienteBenef() {
		return ctaCorrienteBenef;
	}
	public void setCtaCorrienteBenef(String ctaCorrienteBenef) {
		this.ctaCorrienteBenef = ctaCorrienteBenef;
	}
	public String getNombreLineaUno() {
		return nombreLineaUno;
	}
	public void setNombreLineaUno(String nombreLineaUno) {
		this.nombreLineaUno = nombreLineaUno;
	}
	public String getNombreLineaDos() {
		return nombreLineaDos;
	}
	public void setNombreLineaDos(String nombreLineaDos) {
		this.nombreLineaDos = nombreLineaDos;
	}
	public String getNombreLineaTres() {
		return nombreLineaTres;
	}
	public void setNombreLineaTres(String nombreLineaTres) {
		this.nombreLineaTres = nombreLineaTres;
	}
	public String getNombreLineaCuatro() {
		return nombreLineaCuatro;
	}
	public void setNombreLineaCuatro(String nombreLineaCuatro) {
		this.nombreLineaCuatro = nombreLineaCuatro;
	}
	
    /**
     * Representaci�n String de entidad.
     * @return String
     */
	public String toString() {
		String salida = ""
		+ "[nombre = " + this.nombre + "]\n"
		+ "[nombreCont = " + this.nombreCont + "]\n" 
		+ "[direccion = " + this.direccion + "]\n" 
		+ "[direccionCont = " + this.direccionCont + "]\n" 
		+ "[paisResidencia = " + this.paisResidencia + "]\n" 
		+ "[nombrePaisRes = " + this.nombrePaisRes + "]\n" 
		+ "[infoAdicional = " + this.infoAdicional + "]\n" 
		+ "[infoAdicionalCont = " + this.infoAdicionalCont + "]\n" 
		+ "[ctaCorriente = " + this.ctaCorriente + "]\n" 
		+ "[banco = " + this.banco + "]\n" 
		+ "[nombreBanco = " + this.nombreBanco + "]\n" 
		+ "[direccionBanco = " + this.direccionBanco + "]\n" 
		+ "[email = " + this.email + "]\n" 
		+ "[nombrePaisBco = " + this.nombrePaisBco + "]\n" 
		+ "[ctaCorrienteBenef = " + this.ctaCorrienteBenef + "]\n" 
		+ "[nombreLineaUno = " + this.nombreLineaUno + "]\n" 
		+ "[nombreLineaDos = " + this.nombreLineaDos + "]\n" 
		+ "[nombreLineaTres = " + this.nombreLineaTres + "]\n" 
		+ "[nombreLineaCuatro = " + this.nombreLineaCuatro + "]\n"; 
		return salida;
	}
	
}
