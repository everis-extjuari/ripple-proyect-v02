package wcorp.aplicaciones.productos.servicios.transferencias.ripple.to;

import java.io.Serializable;


/**
* Nodos Ripple.
*
* <p>
* Registro de versiones:
* <ul>
* <li>1.0 27/02/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft. BCI): Versi�n inicial.</li>
* </ul>
* <p>
* <B>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</B>
* <p>
*
*/
public class NodosRippleTO implements Serializable {
	/**
	 * Id de serializacion.
	 */
	private static final long serialVersionUID = 1775352208124998438L;

	/**
	 * BIC del Banco.
	 */
	private String bic;
	
	/**
	 * Nombre del Banco.
	 */
	private String nombreBanco;

	public String getBic() {
		return bic;
	}

	public void setBic(String bic) {
		this.bic = bic;
	}

	public String getNombreBanco() {
		return nombreBanco;
	}

	public void setNombreBanco(String nombreBanco) {
		this.nombreBanco = nombreBanco;
	}
    /**
     * Representaci�n String de entidad.
     * @return String
     */
	public String toString() {
		String salida = ""
		+ "[bic = " + this.bic + "]\n"
		+ "[nombreBanco = " + this.nombreBanco + "]\n"; 
		return salida;
	}
}
