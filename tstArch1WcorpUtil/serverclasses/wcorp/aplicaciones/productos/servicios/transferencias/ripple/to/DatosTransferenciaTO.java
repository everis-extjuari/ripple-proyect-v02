package wcorp.aplicaciones.productos.servicios.transferencias.ripple.to;

import java.io.Serializable;

/**
* datos de transferencia ripple
* <p>
* Registro de versiones:
* <ul>
* <li>1.0 20/02/2019 Danilo Dominguez (Everis) - Minheli Mejias(Ing. de Soft. BCI): Versi�n inicial.</li>
* </ul>
* <p>
* <B>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</B>
* <p>
*/
public class DatosTransferenciaTO implements Serializable{
	
	
	/**
	 * Id.
	 */
	private static final long serialVersionUID = 1246034171657181366L;
	/**
	 * monedaTransf.
	 */
	private String monedaTransf;
	/**
	 * cuentaOrigen.
	 */
	private String cuentaOrigen;
	/**
	 * saldoDisponible.
	 */
	private Double saldoDisponible;
	/**
	 * montoTransferir.
	 */
	private String montoTransferir;
	/**
	 * motivoTransf.
	 */
	private String motivoTransf;
	
	public String getMonedaTransf() {
		return monedaTransf;
	}
	public void setMonedaTransf(String monedaTransf) {
		this.monedaTransf = monedaTransf;
	}
	public String getCuentaOrigen() {
		return cuentaOrigen;
	}
	public void setCuentaOrigen(String cuentaOrigen) {
		this.cuentaOrigen = cuentaOrigen;
	}
	public Double getSaldoDisponible() {
		return saldoDisponible;
	}
	public void setSaldoDisponible(Double saldoDisponible) {
		this.saldoDisponible = saldoDisponible;
	}
	public String getMontoTransferir() {
		return montoTransferir;
	}
	public void setMontoTransferir(String montoTransferir) {
		this.montoTransferir = montoTransferir;
	}
	public String getMotivoTransf() {
		return motivoTransf;
	}
	public void setMotivoTransf(String motivoTransf) {
		this.motivoTransf = motivoTransf;
	}
    /**
     * Representaci�n String de entidad.
     * @return String
     */
	public String toString() {
		String salida = ""
		+ "[monedaTransf = " + this.monedaTransf + "]\n" 
		+ "[cuentaOrigen = " + this.cuentaOrigen + "]\n"
		+ "[saldoDisponible = " + this.saldoDisponible + "]\n"
		+ "[montoTransferir = " + this.montoTransferir + "]\n"
		+ "[motivoTransf = " + this.motivoTransf + "]\n";
		return salida;
	}
}
