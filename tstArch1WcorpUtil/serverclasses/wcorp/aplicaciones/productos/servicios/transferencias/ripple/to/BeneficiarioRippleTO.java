package wcorp.aplicaciones.productos.servicios.transferencias.ripple.to;

import java.io.Serializable;

/**
 * Información de datos del beneficiario.
 * <p>
 * Registro de Versiones :
 * <ul>
 * <li>1.0 19-02-2019, Danilo Dominguez(Everis) - Minheli Mejias (Ing. Soft. BCI): Versión inicial</li>
 * </ul>
 * </p>
 * <p>
 * <B>Todos los derechos reservados por Banco de Crédito e Inversiones.</B>
 * </p>
 */
public class BeneficiarioRippleTO implements Serializable {
	
	/**
	 * Id de serializacion.
	 */
	private static final long serialVersionUID = 7500140924650307731L;
	/**
	 * identificacion beneficiario.
	 */
	private String identificacion;
	/**
	 * nombre del beneficiario.
	 */
	private String nombre;
	/**
	 * cuenta corriente del beneficiario.
	 */
	private String ctaCorriente;
	/**
	 * codigo del banco correspondiente al beneficiario.
	 */
	private String bicBanco;
	/**
	 * paisBanco beneficiario.
	 */
	private String paisBanco;
	
	public String getIdentificacion() {
		return identificacion;
	}
	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getCtaCorriente() {
		return ctaCorriente;
	}
	public void setCtaCorriente(String ctaCorriente) {
		this.ctaCorriente = ctaCorriente;
	}
	public String getBicBanco() {
		return bicBanco;
	}
	public void setBicBanco(String bicBanco) {
		this.bicBanco = bicBanco;
	}
	public String getPaisBanco() {
		return paisBanco;
	}
	public void setPaisBanco(String paisBanco) {
		this.paisBanco = paisBanco;
	}
    /**
     * Representación String de entidad.
     * @return String
     */
	public String toString() {
		String salida = ""
		+ "[identificacion = " + this.identificacion + "]\n" 
		+ "[nombre = " + this.nombre + "]\n" 
		+ "[ctaCorriente = " + this.ctaCorriente + "]\n"
		+ "[bicBanco = " + this.bicBanco + "]\n"
		+ "[paisBanco = " + this.paisBanco + "]\n";
		
		return salida;
	}
}
