package wcorp.aplicaciones.productos.servicios.transferencias.ripple.to;

import java.io.Serializable;

/**
 * Entidad Respuesta SP en Transferencia Ripple.
 * <p>
 * Registro de Versiones :
 * <ul>
 * <li>1.0 22-03-2019, Danilo Dominguez(Everis) - Minheli Mejias (Ing. Soft. BCI): Versi�n inicial</li>
 * </ul>
 * </p>
 * <p>
 * <B>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</B>
 * </p>
 */
public class RespuestaSpTO implements Serializable{
	
	/**
	 * serialVersionUID.
	 */
	private static final long serialVersionUID = -5652063572171830955L;

	/**
	* indicador de resultado de registro.
	*/
	private int indicadorResultado;
	
	/**
	* mensaje asociado al resultado de registro.
	*/
	private String mensajeResultado;
	
	public int getIndicadorResultado() {
		return indicadorResultado;
	}
	public void setIndicadorResultado(int indicadorResultado) {
		this.indicadorResultado = indicadorResultado;
	}
	public String getMensajeResultado() {
		return mensajeResultado;
	}
	public void setMensajeResultado(String mensajeResultado) {
		this.mensajeResultado = mensajeResultado;
	}
	
	/**
     * Representaci�n String de entidad.
     * @return String
     */
	public String toString() {
		return "RespuestaSpTO [indicadorResultado=" + indicadorResultado + ", mensajeResultado=" + mensajeResultado+ "]";
	}
}
