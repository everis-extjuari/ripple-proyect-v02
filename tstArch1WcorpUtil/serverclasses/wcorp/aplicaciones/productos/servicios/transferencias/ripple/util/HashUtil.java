package wcorp.aplicaciones.productos.servicios.transferencias.ripple.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.codec.binary.Hex;

/**
 * Utility for SHA-256 Digest
 * 
 * Based on Apache commons-codec. Built because commons-codec-1.3 doen't support SHA-256.
 * 
 */
public class HashUtil {

	private static final String SHA_256 = "SHA-256";

	/**
	 * private utility constructor
	 */
	private HashUtil() {
		
	}
	
    /**
     * Calculates the SHA-256 digest and returns the value as a hex string.
     *
     * @param data Data to digest
     * @return SHA-256 digest as a hex string
     */
    public static String sha256Hex(final String data) {
        return new String(Hex.encodeHex(sha256(data)));
    }
	
    /**
     * Calculates the SHA-256 digest and returns the value as a 
     * <code>byte[]</code>.
     *
     * @param data Data to digest
     * @return SHA-256 digest
     */
    public static byte[] sha256(final String data) {
        return sha256(data.getBytes());
    }
	
    /**
     * Calculates the SHA digest and returns the value as a 
     * <code>byte[]</code>.
     *
     * @param data Data to digest
     * @return SHA digest
     */
    public static byte[] sha256(final byte[] data) {
        return getSha256Digest().digest(data);
    }

    /**
     * Returns an SHA digest.
     *
     * @return An SHA digest instance.
     * @throws RuntimeException when a {@link java.security.NoSuchAlgorithmException} is caught,
     */
    private static MessageDigest getSha256Digest() {
        return getDigest(SHA_256);
    }

    /**
     * Returns a MessageDigest for the given <code>algorithm</code>.
     *
     * @param algorithm The MessageDigest algorithm name.
     * @return An MD5 digest instance.
     * @throws RuntimeException when a {@link java.security.NoSuchAlgorithmException} is caught,
     */
    static MessageDigest getDigest(String algorithm) {
        try {
            return MessageDigest.getInstance(algorithm);
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }
 }
