package wcorp.aplicaciones.productos.servicios.transferencias.ripple.to;
import java.io.Serializable;

/**
* Encapsula los par�metros de entrada para el SP  FBPsrv_shw_csb_1a1. (SE UTILIZARA UNO NUEVO)
* <p>
* Registro de versiones:
* <ul>
* <li>1.0 20/02/2019 Danilo Dominguez (Everis) - Minheli Mejias(Ing. de Soft. BCI) : Versi�n inicial.</li>
* </ul>
* <p>
* <B>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</B>
* <p>
*/
public class ConsultaBeneficiariosRippleTO implements Serializable {
	/**
	* N�mero de versi�n usado durante la Serializaci�n.
	*/
	private static final long serialVersionUID = 1L;


	/**
	* RUT Empresa.
	*/
	private String rutEmpresa;
	
	/**
	* BIC Banco.
	*/
	private String bicBanco;
	
	/**
	* MNDA de la cuenta.
	*/
	private String mndaCuenta;


	public String getRutEmpresa() {
		return rutEmpresa;
	}
  
	public void setRutEmpresa(String rut) {
		this.rutEmpresa = rut;
	}

	public String getBicBanco() {
		return bicBanco;
	}

	public void setBicBanco(String bicBanco) {
		this.bicBanco = bicBanco;
	}

	public String getMndaCuenta() {
		return mndaCuenta;
	}

	public void setMndaCuenta(String mndaCuenta) {
		this.mndaCuenta = mndaCuenta;
	}
    /**
     * Representaci�n String de entidad.
     * @return String
     */
	public String toString() {
		String salida = ""
		+ "[rutEmpresa = " + this.rutEmpresa + "]\n" 
		+ "[bicBanco = " + this.bicBanco + "]\n" 
		+ "[mndaCuenta = " + this.mndaCuenta + "]\n"; 
		return salida;
	}
	
}
