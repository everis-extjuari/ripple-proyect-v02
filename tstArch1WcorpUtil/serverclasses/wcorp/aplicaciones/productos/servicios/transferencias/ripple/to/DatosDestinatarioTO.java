package wcorp.aplicaciones.productos.servicios.transferencias.ripple.to;

import java.io.Serializable;

/**
* Información datos del destinatario Ripple
* <p>
* Registro de versiones:
* <ul>
* <li>1.0 20/02/2019 Danilo Dominguez (Everis) - Minheli Mejias(Ing. de Soft. BCI): Versión inicial.</li>
* </ul>
* <p>
* <B>Todos los derechos reservados por Banco de Crédito e Inversiones.</B>
* <p>
*/
public class DatosDestinatarioTO implements Serializable{

	
	/**
	 * Id.
	 */
	private static final long serialVersionUID = 8772789909308624223L;
	/**
	 * destinatario.
	 */
	private String destinatario;
	/**
	 * monedaTransf.
	 */
	private String monedaTransf;
	/**
	 * paisCuenta.
	 */
	private String paisCuenta;
	/**
	 * numeroCuenta.
	 */
	private String numeroCuenta;
	/**
	 * referencia.
	 */
	private String referencia;
	
	public String getDestinatario() {
		return destinatario;
	}
	public void setDestinatario(String destinatario) {
		this.destinatario = destinatario;
	}
	public String getMonedaTransf() {
		return monedaTransf;
	}
	public void setMonedaTransf(String monedaTransf) {
		this.monedaTransf = monedaTransf;
	}
	public String getPaisCuenta() {
		return paisCuenta;
	}
	public void setPaisCuenta(String paisCuenta) {
		this.paisCuenta = paisCuenta;
	}
	public String getNumeroCuenta() {
		return numeroCuenta;
	}
	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
    /**
     * Representación String de entidad.
     * @return String
     */
	public String toString() {
		String salida = ""
		+ "[destinatario = " + this.destinatario + "]\n" 
		+ "[monedaTransf = " + this.monedaTransf + "]\n"
		+ "[paisCuenta = " + this.paisCuenta + "]\n" 
		+ "[numeroCuenta = " + this.numeroCuenta + "]\n"
		+ "[referencia = " + this.referencia + "]\n"; 
		return salida;
	}
}
