package wcorp.aplicaciones.productos.servicios.transferencias.ripple.to;

import java.io.Serializable;

/**
* Entidad de negocio para respuesta registro transferencia Ripple.
*
* <p>
* Registro de versiones:
* <ul>
* <li>1.0 27/02/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft. BCI): Versi�n inicial.</li>
* </ul>
* <p>
* <B>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</B>
* <p>
*
*/
public class RespuestaRegistroTransferenciaTO implements Serializable{

	/**
	 * Id de serializacion.
	 */
	private static final long serialVersionUID = 9032555230161445308L;
	/**
	* indicador de resultado de registro.
	*/
	private int indicadorResultado;
	
	/**
	* mensaje asociado al resultado de registro.
	*/
	private String mensajeResultado;
	
	/**
	* Numero correlativo FBP.
	*/
	private int numeroCorrelativo; 
	/**
	* SysTimeStamp.
	*/
	private String systimestamp;
	
	public int getIndicadorResultado() {
		return indicadorResultado;
	}
	public void setIndicadorResultado(int indicadorResultado) {
		this.indicadorResultado = indicadorResultado;
	}
	public String getMensajeResultado() {
		return mensajeResultado;
	}
	public void setMensajeResultado(String mensajeResultado) {
		this.mensajeResultado = mensajeResultado;
	}
	public int getNumeroCorrelativo() {
		return numeroCorrelativo;
	}
	public void setNumeroCorrelativo(int numeroCorrelativo) {
		this.numeroCorrelativo = numeroCorrelativo;
	}
	public String getSystimestamp() {
		return systimestamp;
	}
	public void setSystimestamp(String systimestamp) {
		this.systimestamp = systimestamp;
	}
    /**
     * Representaci�n String de entidad.
     * @return String
     */
	public String toString() {
		String salida = ""
		+ "[indicadorResultado = " + this.indicadorResultado + "]\n"
		+ "[mensajeResultado = " + this.mensajeResultado + "]\n"
		+ "[numeroCorrelativo = " + this.numeroCorrelativo + "]\n"
		+ "[systimestamp = " + this.systimestamp + "]\n"; 
		return salida;
	}
}
