package wcorp.aplicaciones.productos.servicios.transferencias.ripple.to;

import java.io.Serializable;

/**
 * Entidad Hash para mapeo de datos en Transferencia Ripple.
 * <p>
 * Registro de Versiones :
 * <ul>
 * <li>1.0 22-03-2019, Danilo Dominguez(Everis) - Minheli Mejias (Ing. Soft. BCI): Versi�n inicial</li>
 * </ul>
 * </p>
 * <p>
 * <B>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</B>
 * </p>
 */
public class DatosHashTO implements Serializable{
	
	/**
	 * serialVersionUID.
	 */
	private static final long serialVersionUID = -5783029357606546006L;

	/**
	 * Cuenta de origen de la transferencia.
	 */
	private String cuentaOrigen;
	
	/**
	 * Monto de la transferencia.
	 */
	private String monto;
	
	/**
	 * Moneda de la transferencia.
	 */
	private String moneda; 
	
	/**
	 * ID del beneficiadio.
	 */
	private String idBeneficiario;
	
	/**
	 * Cuenta de destino de la transferencia.
	 */
	private String cuentaDestino;

	public String getCuentaOrigen() {
		return cuentaOrigen;
	}

	public void setCuentaOrigen(String cuentaOrigen) {
		this.cuentaOrigen = cuentaOrigen;
	}

	public String getMonto() {
		return monto;
	}

	public void setMonto(String monto) {
		this.monto = monto;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public String getIdBeneficiario() {
		return idBeneficiario;
	}

	public void setIdBeneficiario(String idBeneficiario) {
		this.idBeneficiario = idBeneficiario;
	}

	public String getCuentaDestino() {
		return cuentaDestino;
	}

	public void setCuentaDestino(String cuentaDestino) {
		this.cuentaDestino = cuentaDestino;
	}
	
	/**
     * Representaci�n String de entidad.
     * @return String
     */
	public String toString() {
		return "DatosHashTO [cuentaOrigen=" + cuentaOrigen + ", monto=" + monto + ", moneda=" + moneda
				+ ", idBeneficiario=" + idBeneficiario + ", cuentaDestino=" + cuentaDestino + "]";
	}
}