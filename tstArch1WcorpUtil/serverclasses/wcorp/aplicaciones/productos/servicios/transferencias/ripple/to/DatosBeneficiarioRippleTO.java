package wcorp.aplicaciones.productos.servicios.transferencias.ripple.to;

import java.io.Serializable;

/**
* Información datos del beneficiario para Ripple
* <p>
* Registro de versiones:
* <ul>
* <li>1.0 20/02/2019 Danilo Dominguez (Everis) - Minheli Mejias(Ing. de Soft. BCI): Versión inicial.</li>
* </ul>
* <p>
* <B>Todos los derechos reservados por Banco de Crédito e Inversiones.</B>
* <p>
*/
public class DatosBeneficiarioRippleTO implements Serializable{

	/**
	 * Id.
	 */
	private static final long serialVersionUID = -4796230924618520087L;
	/**
	 * nombre.
	 */
	private String nombre;
	/**
	 * direccion.
	 */
	private String direccion;
	/**
	 * email.
	 */
	private String email;
	/**
	 * pais.
	 */
	private String pais;
	/**
	 * ciudad.
	 */
	private String ciudad;
	/**
	 * nombreLineaUno.
	 */
	private String nombreLineaUno;
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getCiudad() {
		return ciudad;
	}
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	public String getNombreLineaUno() {
		return nombreLineaUno;
	}
	public void setNombreLineaUno(String nombreLineaUno) {
		this.nombreLineaUno = nombreLineaUno;
	}
    /**
     * Representación String de entidad.
     * @return String
     */
	public String toString() {
		String salida = ""
		+ "[nombre = " + this.nombre + "]\n" 
		+ "[direccion = " + this.direccion + "]\n"
		+ "[email = " + this.email + "]\n"
		+ "[pais = " + this.pais + "]\n"
		+ "[ciudad = " + this.ciudad + "]\n"
		+ "[nombreLineaUno = " + this.nombreLineaUno + "]\n";
		return salida;
	}
}
