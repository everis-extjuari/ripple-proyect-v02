package wcorp.aplicaciones.productos.servicios.transferencias.ripple.to;

import java.io.Serializable;

/**
 * Entidad FirmarEliminar Transferencia Ripple.
 * <p>
 * Registro de Versiones :
 * <ul>
 * <li>1.0 22-03-2019, Danilo Dominguez(Everis) - Minheli Mejias (Ing. Soft. BCI): Versi�n inicial</li>
 * </ul>
 * </p>
 * <p>
 * <B>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</B>
 * </p>
 */
public class FirmarEliminarTransferenciaRipple implements Serializable{
	
	/**
	 * serialVersionUID.
	 */
	private static final long serialVersionUID = 177648965348783611L;

	/**
	 * Accion a realizar.
	 */
	private String accion;
	
	/**
	 * Numero correlativo FBP de la transferencia.
	 */
	private int correlativoFbp;
	
	/**
	 * sysTimeStamp del registro de la transferencia.
	 */
	private String sysTimeStamp;
	
	/**
	 * Hash de la operacion.
	 */
	private String hash;

	/**
	 * Rut del apoderado.
	 */
	private String usuario;
	
	/**
	 * ip terminal del evento.
	 */
	private String ipTerminal;
	
	/**
	 * Motivo de la accion a realizar (solo para DEL).
	 */
	private String Comentarios;

	private DatosHashTO datosHash;
	
	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public int getCorrelativoFbp() {
		return correlativoFbp;
	}

	public void setCorrelativoFbp(int correlativoFbp) {
		this.correlativoFbp = correlativoFbp;
	}

	public String getSysTimeStamp() {
		return sysTimeStamp;
	}

	public void setSysTimeStamp(String sysTimeStamp) {
		this.sysTimeStamp = sysTimeStamp;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getIpTerminal() {
		return ipTerminal;
	}

	public void setIpTerminal(String ipTerminal) {
		this.ipTerminal = ipTerminal;
	}

	public String getComentarios() {
		return Comentarios;
	}

	public void setComentarios(String comentarios) {
		Comentarios = comentarios;
	}

	public DatosHashTO getDatosHash() {
		return datosHash;
	}

	public void setDatosHash(DatosHashTO datosHash) {
		this.datosHash = datosHash;
	}

	/**
     * Representaci�n String de entidad.
     * @return String
     */
	public String toString() {
		return "FirmarEliminarTransferenciaRipple [accion=" + accion + ", correlativoFbp=" + correlativoFbp
				+ ", sysTimeStamp=" + sysTimeStamp + ", hash=" + hash + ", usuario=" + usuario + ", ipTerminal="
				+ ipTerminal + ", Comentarios=" + Comentarios + ", datosHash=" + datosHash + "]";
	}
}