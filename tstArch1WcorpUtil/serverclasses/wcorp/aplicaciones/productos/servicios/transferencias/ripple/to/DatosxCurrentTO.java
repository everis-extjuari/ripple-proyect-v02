package wcorp.aplicaciones.productos.servicios.transferencias.ripple.to;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Datos a enviar a xCurrent.
 * <p>
 * Registro de Versiones :
 * <ul>
 * <li>1.0 22-03-2019, Danilo Dominguez(Everis) - Minheli Mejias (Ing. Soft. BCI): Versi�n inicial</li>
 * </ul>
 * </p>
 * <p>
 * <B>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</B>
 * </p>
 */
public class DatosxCurrentTO implements Serializable{
	
	
	
	/**
	 * Id de serializacion.
	 */
	private static final long serialVersionUID = -6266101851401901370L;

	/**
	* indicador de resultado de registro.
	*/
	private int indicadorResultado;
	
	/**
	* mensaje asociado al resultado de registro.
	*/
	private String mensajeResultado;
	
	/**
	* numero de cuenta del ordenante.
	*/
	private String numCtaOrdenante;
	
	/**
	* nombre del Ordenante.
	*/
	private String nombreOrdenante;
	
	/**
	* direccion del Ordenante.
	*/
	private String direccionOrdenante;
	
	/**
	* lugar de residencia del Ordenante.
	*/
	private String residenciaOrdenante;
	
	/**
	* rut del Ordenante.
	*/
	private String rutOrdenante;
	
	/**
	* pais del Ordenante.
	*/
	private String paisOrdenante;
	
	/**
	* TRN del mensaje.
	*/
	private String trn;
	
	/**
	* numero de cuenta del Beneficiario.
	*/
	private String numCtaBenef;
	
	/**
	* nombre del Beneficiario.
	*/
	private String nombreBenef;
	
	/**
	* direccion del Beneficiario linea 1.
	*/
	private String direccionBenefL1;
	
	/**
	* direccion del Beneficiario linea 2.
	*/
	private String direccionBenefL2;
	
	/**
	* pais de residencia del Beneficiario.
	*/
	private String paisBenef;
	
	/**
	* moneda de la Transferencia.
	*/
	private String monedaTransf;
	
	/**
	* monto de la Transferencia.
	*/
	private BigDecimal montoTransf;
	
	/**
	* monto de la comision.
	*/
	private BigDecimal montoComision;
	
	/**
	* informacion de la remesa.
	*/
	private String remesa;
	
	/**
	* banco del beneficiario.
	*/
	private String bancoBenef;

	public int getIndicadorResultado() {
		return indicadorResultado;
	}

	public void setIndicadorResultado(int indicadorResultado) {
		this.indicadorResultado = indicadorResultado;
	}

	public String getMensajeResultado() {
		return mensajeResultado;
	}

	public void setMensajeResultado(String mensajeResultado) {
		this.mensajeResultado = mensajeResultado;
	}

	public String getNumCtaOrdenante() {
		return numCtaOrdenante;
	}

	public void setNumCtaOrdenante(String numCtaOrdenante) {
		this.numCtaOrdenante = numCtaOrdenante;
	}

	public String getNombreOrdenante() {
		return nombreOrdenante;
	}

	public void setNombreOrdenante(String nombreOrdenante) {
		this.nombreOrdenante = nombreOrdenante;
	}

	public String getDireccionOrdenante() {
		return direccionOrdenante;
	}

	public void setDireccionOrdenante(String direccionOrdenante) {
		this.direccionOrdenante = direccionOrdenante;
	}

	public String getResidenciaOrdenante() {
		return residenciaOrdenante;
	}

	public void setResidenciaOrdenante(String residenciaOrdenante) {
		this.residenciaOrdenante = residenciaOrdenante;
	}

	public String getRutOrdenante() {
		return rutOrdenante;
	}

	public void setRutOrdenante(String rutOrdenante) {
		this.rutOrdenante = rutOrdenante;
	}

	public String getPaisOrdenante() {
		return paisOrdenante;
	}

	public void setPaisOrdenante(String paisOrdenante) {
		this.paisOrdenante = paisOrdenante;
	}

	public String getTrn() {
		return trn;
	}

	public void setTrn(String trn) {
		this.trn = trn;
	}

	public String getNumCtaBenef() {
		return numCtaBenef;
	}

	public void setNumCtaBenef(String numCtaBenef) {
		this.numCtaBenef = numCtaBenef;
	}

	public String getNombreBenef() {
		return nombreBenef;
	}

	public void setNombreBenef(String nombreBenef) {
		this.nombreBenef = nombreBenef;
	}

	public String getDireccionBenefL1() {
		return direccionBenefL1;
	}

	public void setDireccionBenefL1(String direccionBenefL1) {
		this.direccionBenefL1 = direccionBenefL1;
	}

	public String getDireccionBenefL2() {
		return direccionBenefL2;
	}

	public void setDireccionBenefL2(String direccionBenefL2) {
		this.direccionBenefL2 = direccionBenefL2;
	}

	public String getPaisBenef() {
		return paisBenef;
	}

	public void setPaisBenef(String paisBenef) {
		this.paisBenef = paisBenef;
	}

	public String getMonedaTransf() {
		return monedaTransf;
	}

	public void setMonedaTransf(String monedaTransf) {
		this.monedaTransf = monedaTransf;
	}

	public BigDecimal getMontoTransf() {
		return montoTransf;
	}

	public void setMontoTransf(BigDecimal montoTransf) {
		this.montoTransf = montoTransf;
	}

	public BigDecimal getMontoComision() {
		return montoComision;
	}

	public void setMontoComision(BigDecimal montoComision) {
		this.montoComision = montoComision;
	}

	public String getRemesa() {
		return remesa;
	}

	public void setRemesa(String remesa) {
		this.remesa = remesa;
	}

	public String getBancoBenef() {
		return bancoBenef;
	}

	public void setBancoBenef(String bancoBenef) {
		this.bancoBenef = bancoBenef;
	}

	/**
     * Representaci�n String de entidad.
     * @return String
     */
	public String toString() {
		String salida = ""
		+ "[indicadorResultado=" + this.indicadorResultado + "]\n"
		+ "[mensajeResultado=" + this.mensajeResultado + "]\n"
		+ "[numCtaOrdenante=" + this.numCtaOrdenante + "]\n"
		+ "[nombreOrdenante=" + this.nombreOrdenante + "]\n"
		+ "[direccionOrdenante=" + this.direccionOrdenante + "]\n"
		+ "[residenciaOrdenante=" + this.residenciaOrdenante + "]\n"
		+ "[rutOrdenante=" + this.rutOrdenante + "]\n"
		+ "[paisOrdenante=" + this.paisOrdenante + "]\n"
		+ "[trn=" + this.trn + "]\n"
		+ "[numCtaBenef=" + this.numCtaBenef + "]\n"
		+ "[nombreBenef=" + this.nombreBenef + "]\n"
		+ "[direccionBenefL1=" + this.direccionBenefL1 + "]\n"
		+ "[direccionBenefL2=" + this.direccionBenefL2 + "]\n"
		+ "[paisBenef=" + this.paisBenef + "]\n"
		+ "[monedaTransf=" + this.monedaTransf + "]\n"
		+ "[montoTransf=" + this.montoTransf + "]\n"
		+ "[montoComision=" + this.montoComision + "]\n"
		+ "[remesa=" + this.remesa + "]\n"
		+ "[bancoBenef=" + this.bancoBenef + "]\n";
		
		return salida;
	}
}
