package wcorp.aplicaciones.productos.servicios.transferencias.ripple.to;

import java.io.Serializable;
import java.math.BigDecimal;

/**
* Entidad de negocio para registro transferencia Ripple.
*
* <p>
* Registro de versiones:
* <ul>
* <li>1.0 27/02/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft. BCI): Versi�n inicial.</li>
* </ul>
* <p>
* <B>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</B>
* <p>
*
*/
public class RegistrarTransferenciaTO implements Serializable{

	/**
	 * Id.
	 */
	private static final long serialVersionUID = 212281792375429627L;
	/**
	 * rutEmpresa.
	 */
	private String rutEmpresa;
	/**
	 * nombreEmpresa.
	 */
	private String nombreEmpresa;
	/**
	 * moneda.
	 */
	private String moneda;
	/**
	 * cuentaDebito.
	 */
	private String cuentaDebito;
	/**
	 * montoPago.
	 */
	private BigDecimal montoPago;
	/**
	 * remesa.
	 */
	private String remesa;
	/**
	 * monedaComision.
	 */
	private String monedaComision;
	/**
	 * cuentaComision.
	 */
	private String cuentaComision;
	/**
	 * codIdentifBenef.
	 */
	private String codIdentifBenef;
	/**
	 * paisBcoBenef.
	 */
	private String paisBcoBenef;
	/**
	 * codigoCambio.
	 */
	private String codigoCambio;
	/**
	 * hashOperacion.
	 */
	private String hashOperacion;
	/**
	 * keyOperacion.
	 */
	private String keyOperacion;
	/**
	 * rutUsuario.
	 */
	private String rutUsuario;
	/**
	 * ipMaquina.
	 */
	private String ipMaquina;
	/**
	 * cuentaDestino.
	 */
	private String cuentaDestino;
	
	public String getRutEmpresa() {
		return rutEmpresa;
	}
	public void setRutEmpresa(String rutEmpresa) {
		this.rutEmpresa = rutEmpresa;
	}
	public String getNombreEmpresa() {
		return nombreEmpresa;
	}
	public void setNombreEmpresa(String nombreEmpresa) {
		this.nombreEmpresa = nombreEmpresa;
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	public String getCuentaDebito() {
		return cuentaDebito;
	}
	public void setCuentaDebito(String cuentaDebito) {
		this.cuentaDebito = cuentaDebito;
	}
	public BigDecimal getMontoPago() {
		return montoPago;
	}
	public void setMontoPago(BigDecimal montoPago) {
		this.montoPago = montoPago;
	}
	public String getRemesa() {
		return remesa;
	}
	public void setRemesa(String remesa) {
		this.remesa = remesa;
	}
	public String getMonedaComision() {
		return monedaComision;
	}
	public void setMonedaComision(String monedaComision) {
		this.monedaComision = monedaComision;
	}
	public String getCuentaComision() {
		return cuentaComision;
	}
	public void setCuentaComision(String cuentaComision) {
		this.cuentaComision = cuentaComision;
	}
	public String getCodIdentifBenef() {
		return codIdentifBenef;
	}
	public void setCodIdentifBenef(String codIdentifBenef) {
		this.codIdentifBenef = codIdentifBenef;
	}
	public String getPaisBcoBenef() {
		return paisBcoBenef;
	}
	public void setPaisBcoBenef(String paisBcoBenef) {
		this.paisBcoBenef = paisBcoBenef;
	}
	public String getCodigoCambio() {
		return codigoCambio;
	}
	public void setCodigoCambio(String codigoCambio) {
		this.codigoCambio = codigoCambio;
	}
	public String getHashOperacion() {
		return hashOperacion;
	}
	public void setHashOperacion(String hashOperacion) {
		this.hashOperacion = hashOperacion;
	}
	public String getKeyOperacion() {
		return keyOperacion;
	}
	public void setKeyOperacion(String keyOperacion) {
		this.keyOperacion = keyOperacion;
	}
	public String getRutUsuario() {
		return rutUsuario;
	}
	public void setRutUsuario(String rutUsuario) {
		this.rutUsuario = rutUsuario;
	}
	public String getIpMaquina() {
		return ipMaquina;
	}
	public void setIpMaquina(String ipMaquina) {
		this.ipMaquina = ipMaquina;
	}
	
	public String getCuentaDestino() {
		return cuentaDestino;
	}
	public void setCuentaDestino(String cuentaDestino) {
		this.cuentaDestino = cuentaDestino;
	}
    /**
     * Representaci�n String de entidad.
     * @return String
     */
	public String toString() {
		String salida = ""
		+ "[rutEmpresa = " + this.rutEmpresa + "]\n"
		+ "[nombreEmpresa = " + this.nombreEmpresa + "]\n"
		+ "[moneda = " + this.moneda + "]\n"
		+ "[cuentaDebito = " + this.cuentaDebito + "]\n"
		+ "[montoPago = " + this.montoPago + "]\n"
		+ "[remesa = " + this.remesa + "]\n" 
		+ "[monedaComision = " + this.monedaComision + "]\n" 
		+ "[cuentaComision = " + this.cuentaComision + "]\n"
		+ "[codIdentifBenef = " + this.codIdentifBenef + "]\n"
		+ "[paisBcoBenef = " + this.paisBcoBenef + "]\n" 
		+ "[codigoCambio = " + this.codigoCambio + "]\n" 
		+ "[hashOperacion = " + this.hashOperacion + "]\n"
		+ "[keyOperacion = " + this.keyOperacion + "]\n" 
		+ "[rutUsuario = " + this.rutUsuario + "]\n" 
		+ "[ipMaquina = " + this.ipMaquina + "]\n"
		+ "[cuentaDestino = " + this.cuentaDestino + "]\n";
		return salida;
	}
}
