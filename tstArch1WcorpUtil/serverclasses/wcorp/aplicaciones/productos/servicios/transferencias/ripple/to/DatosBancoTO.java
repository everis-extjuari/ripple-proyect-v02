package wcorp.aplicaciones.productos.servicios.transferencias.ripple.to;

import java.io.Serializable;

/**
* Información datos del banco para Ripple
* <p>
* Registro de versiones:
* <ul>
* <li>1.0 20/02/2019 Danilo Dominguez (Everis) - Minheli Mejias(Ing. de Soft. BCI): Versión inicial.</li>
* </ul>
* <p>
* <B>Todos los derechos reservados por Banco de Crédito e Inversiones.</B>
* <p>
*/
public class DatosBancoTO implements Serializable{

    /**
	 * Id de Serializacion.
	 */
	private static final long serialVersionUID = -3992359839922556346L;
	/**
	 * codigoSwift.
	 */
	private String codigoSwift;
	/**
	 * nombreDireccion.
	 */
    private String nombreDireccion;
    /**
     * codigoCambio.
     */
    private String codigoCambio;
    
    public String getCodigoSwift() {
		return codigoSwift;
	}

	public void setCodigoSwift(String codigoSwift) {
		this.codigoSwift = codigoSwift;
	}

	public String getNombreDireccion() {
		return nombreDireccion;
	}

	public void setNombreDireccion(String nombreDireccion) {
		this.nombreDireccion = nombreDireccion;
	}
	
	public String getCodigoCambio() {
		return codigoCambio;
	}

	public void setCodigoCambio(String codigoCambio) {
		this.codigoCambio = codigoCambio;
	}
    /**
     * Representación String de entidad.
     * @return String
     */
	public String toString() {
		String salida = ""
		+ "[codigoSwift = " + this.codigoSwift + "]\n"
		+ "[nombreDireccion = " + this.nombreDireccion + "]\n" 
		+ "[codigoCambio = " + this.codigoCambio + "]\n"; 
		return salida;
	}
}
