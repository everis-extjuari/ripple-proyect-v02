package wcorp.aplicaciones.productos.servicios.transferencias.ripple.to;

import java.io.Serializable;
import java.math.BigDecimal;

/**
* Monedas Permitidas Ripple
* <p>
* Registro de versiones:
* <ul>
* <li>1.0 20/02/2019 Danilo Dominguez (Everis) - Minheli Mejias(Ing. de Soft. BCI): Versi�n inicial.</li>
* </ul>
* <p>
* <B>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</B>
* <p>
*/
public class MonedasPermitidasTO implements Serializable{
	/**
	 * Id de serializacion.
	 */
	private static final long serialVersionUID = -7945516909493355444L;
	/**
	* Codigo ISO de moneda.
	*/
	private String codigoIso; 
	/**
	* Nombre de la moneda.
	*/
	private String nombreMoneda; 
	/**
	* Monto Maximo.
	*/
	private BigDecimal mtoMaximo;

	public String getCodigoIso() {
		return codigoIso;
	}
  
	public void setCodigoIso(String codigo) {
		this.codigoIso = codigo;
	}
 
	public String getNombreMoneda() {
		return nombreMoneda;
	}
  
	public void setNombreMoneda(String nombre) {
		this.nombreMoneda = nombre;
	}
 
	public BigDecimal getMtoMaximo() {
		return mtoMaximo;
	}
  
	public void setMtoMaximo(BigDecimal mto) {
		this.mtoMaximo = mto;
	}
    /**
     * Representaci�n String de entidad.
     * @return String
     */
	public String toString() {
		String salida = ""
		+ "[codigoIso = " + this.codigoIso + "]\n" 
		+ "[nombreMoneda = " + this.nombreMoneda + "]\n" 
		+ "[mtoMaximo = " + this.mtoMaximo + "]\n";
		return salida;
	}
}
