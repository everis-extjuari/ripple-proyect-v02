package wcorp.aplicaciones.productos.servicios.transferencias.ripple.to;

import java.io.Serializable;
import java.util.List;

/**
* Monedas Nodo Ripple
* <p>
* Registro de versiones:
* <ul>
* <li>1.0 20/02/2019 Danilo Dominguez (Everis) - Minheli Mejias(Ing. de Soft. BCI): Versi�n inicial.</li>
* </ul>
* <p>
* <B>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</B>
* <p>
*/
public class MonedasNodoTO implements Serializable {
	
	/**
	 * Id.
	 */
	private static final long serialVersionUID = -6849804827186990319L;
	/**
	 * nodo ripple.
	 */
	private NodosRippleTO nodo;
	/**
	 * lstMonedas.
	 */
	private List lstMonedas;

	public NodosRippleTO getNodo() {
		return nodo;
	}

	public void setNodo(NodosRippleTO nodo) {
		this.nodo = nodo;
	}

	public List getLstMonedas() {
		return lstMonedas;
	}

	public void setLstMonedas(List lstMonedas) {
		this.lstMonedas = lstMonedas;
	}
    /**
     * Representaci�n String de entidad.
     * @return String
     */
	public String toString() {
		String salida = ""
		+ "[nodo = " + this.nodo + "]\n" 
		+ "[lstMonedas = " + this.lstMonedas + "]\n"; 
		return salida;
	}

}
