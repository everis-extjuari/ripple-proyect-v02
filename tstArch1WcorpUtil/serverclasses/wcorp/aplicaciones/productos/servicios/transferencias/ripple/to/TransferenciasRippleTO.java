package wcorp.aplicaciones.productos.servicios.transferencias.ripple.to;

import java.io.Serializable;

/**
* Entidad de negocio de Transferencias Ripple.
*
* <p>
* Registro de versiones:
* <ul>
* <li>1.0 27/02/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft. BCI): Versi�n inicial.</li>
* </ul>
* <p>
* <B>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</B>
* <p>
*
*/
public class TransferenciasRippleTO implements Serializable{

	/**
	 * Id.
	 */
	private static final long serialVersionUID = -5855674122595200050L;
	/**
	 * objeto datos de transferencia.
	 */
	private DatosTransferenciaTO datosTransf= new DatosTransferenciaTO();
	/**
	 * objeto datos destinatario.
	 */
	private DatosDestinatarioTO datosDestin= new DatosDestinatarioTO();
	/**
	 * onjeto datos beneficiario.
	 */
	private DatosBeneficiarioRippleTO datosBenef= new DatosBeneficiarioRippleTO();
	/**
	 * objeto datos banco.
	 */
	private DatosBancoTO datosBanco= new DatosBancoTO();

	public DatosTransferenciaTO getDatosTransf() {
		return datosTransf;
	}
	public void setDatosTransf(DatosTransferenciaTO datosTransf) {
		this.datosTransf = datosTransf;
	}
	public DatosDestinatarioTO getDatosDestin() {
		return datosDestin;
	}
	public void setDatosDestin(DatosDestinatarioTO datosDestin) {
		this.datosDestin = datosDestin;
	}
	public DatosBeneficiarioRippleTO getDatosBenef() {
		return datosBenef;
	}
	public void setDatosBenef(DatosBeneficiarioRippleTO datosBenef) {
		this.datosBenef = datosBenef;
	}
	
	public DatosBancoTO getDatosBanco() {
		return datosBanco;
	}
	public void setDatosBanco(DatosBancoTO datosBanco) {
		this.datosBanco = datosBanco;
	}
    /**
     * Representaci�n String de entidad.
     * @return String
     */
	public String toString() {
		String salida = ""
		+ "[datosTransf = " + this.datosTransf + "]\n" 
		+ "[datosDestin = " + this.datosDestin + "]\n"
		+ "[datosBenef = " + this.datosBenef + "]\n" 
		+ "[datosBanco = " + this.datosBanco + "]\n";
		return salida;
	}
	
}
