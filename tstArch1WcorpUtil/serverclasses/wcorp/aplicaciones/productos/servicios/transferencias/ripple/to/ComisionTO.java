package wcorp.aplicaciones.productos.servicios.transferencias.ripple.to;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Información de datos del beneficiario.
 * <p>
 * Registro de Versiones :
 * <ul>
 * <li>1.0 19-02-2019, Danilo Dominguez(Everis) - Minheli Mejias (Ing. Soft. BCI): Versión inicial</li>
 * </ul>
 * </p>
 * <p>
 * <B>Todos los derechos reservados por Banco de Crédito e Inversiones.</B>
 * </p>
 */
public class ComisionTO implements Serializable {

	
	/**
	 * Id serializacion.
	 */
	private static final long serialVersionUID = 8591251712830689141L;
	/**
	 * codigoIso.
	 */
	private String codigoIso;
	/**
	 * ctaDebitoComisiones.
	 */
	private String ctaDebitoComisiones;
	/**
	 * comisionEnvio.
	 */
	private BigDecimal comisionEnvio;
	/**
	 * tipoComision.
	 */
	private int tipoComision;
	/**
	 * descTipoComision.
	 */
	private String descTipoComision;
	/**
	 * tipoTarifa.
	 */
	private String tipoTarifa;
	/**
	 * key.
	 */
	private String key;
	
	
	public String getCodigoIso() {
		return codigoIso;
	}
	public void setCodigoIso(String codigoIso) {
		this.codigoIso = codigoIso;
	}
	public String getCtaDebitoComisiones() {
		return ctaDebitoComisiones;
	}
	public void setCtaDebitoComisiones(String ctaDebitoComisiones) {
		this.ctaDebitoComisiones = ctaDebitoComisiones;
	}
	public BigDecimal getComisionEnvio() {
		return comisionEnvio;
	}
	public void setComisionEnvio(BigDecimal comisionEnvio) {
		this.comisionEnvio = comisionEnvio;
	}
	public int getTipoComision() {
		return tipoComision;
	}
	public void setTipoComision(int tipoComision) {
		this.tipoComision = tipoComision;
	}
	public String getDescTipoComision() {
		return descTipoComision;
	}
	public void setDescTipoComision(String descTipoComision) {
		this.descTipoComision = descTipoComision;
	}
	public String getTipoTarifa() {
		return tipoTarifa;
	}
	public void setTipoTarifa(String tipoTarifa) {
		this.tipoTarifa = tipoTarifa;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
    /**
     * Representación String de entidad.
     * @return String
     */
	public String toString() {
		String salida = ""
		+ "[codigoIso = " + this.codigoIso + "]\n" 
		+ "[ctaDebitoComisiones = " + this.ctaDebitoComisiones + "]\n"
		+ "[comisionEnvio = " + this.comisionEnvio + "]\n"
		+ "[tipoComision = " + this.tipoComision + "]\n"
		+ "[descTipoComision = " + this.descTipoComision + "]\n"
		+ "[tipoTarifa = " + this.tipoTarifa + "]\n"
		+ "[key = " + this.key + "]\n";
		
		return salida;
	}
	
}
