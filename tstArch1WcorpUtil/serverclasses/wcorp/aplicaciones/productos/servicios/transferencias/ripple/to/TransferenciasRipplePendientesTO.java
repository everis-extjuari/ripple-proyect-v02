package wcorp.aplicaciones.productos.servicios.transferencias.ripple.to;

import java.io.Serializable;

/**
* Entidad de negocio de Transferencias Pendientes Ripple.
*
* <p>
* Registro de versiones:
* <ul>
* <li>1.0 27/02/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft. BCI): Versi�n inicial.</li>
* </ul>
* <p>
* <B>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</B>
* <p>
*
*/
public class TransferenciasRipplePendientesTO implements Serializable {

	/**
	 * Id.
	 */
	private static final long serialVersionUID = 739560948812373929L;
	/**
	 * numeroOperacion.
	 */
	private String numeroOperacion;
	/**
	 * beneficiario.
	 */
	private String beneficiario;
	/**
	 * pais.
	 */
	private String pais;
	/**
	 * moneda.
	 */
	private String moneda;
	/**
	 * monto.
	 */
	private String monto;
	/**
	 * fechaOperacion.
	 */
	private String fechaOperacion;
	
	public String getNumeroOperacion() {
		return numeroOperacion;
	}
	public void setNumeroOperacion(String numeroOperacion) {
		this.numeroOperacion = numeroOperacion;
	}
	public String getBeneficiario() {
		return beneficiario;
	}
	public void setBeneficiario(String beneficiario) {
		this.beneficiario = beneficiario;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	public String getMonto() {
		return monto;
	}
	public void setMonto(String monto) {
		this.monto = monto;
	}
	public String getFechaOperacion() {
		return fechaOperacion;
	}
	public void setFechaOperacion(String fechaOperacion) {
		this.fechaOperacion = fechaOperacion;
	}
    /**
     * Representaci�n String de entidad.
     * @return String
     */
	public String toString() {
		return "TransferenciasRipplePendientesTO [numeroOperacion=" + numeroOperacion + ", beneficiario=" + beneficiario
				+ ", pais=" + pais + ", moneda=" + moneda + ", monto=" + monto + ", fechaOperacion=" + fechaOperacion
				+ "]";
	}

}
