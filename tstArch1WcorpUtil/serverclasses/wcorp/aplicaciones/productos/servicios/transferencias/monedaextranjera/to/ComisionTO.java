package wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to;
import java.io.Serializable;
import java.math.BigDecimal;

/**
* Encapsula la salida del SP  FBPsrv_shw_cyg_1a1.
*
* <p>
* Registro de versiones:
* <ul>
* <li>1.0 20/08/2013 14:06:45 BcxWSql (Bcx Solutions) : Versi�n inicial.</li>
* <li>1.1 28/03/2018 Angelo Venegas (Everis)- Minheli Mejias (Ing. Soft. Bci) : Se modifica m�todo {@link #toString()} 
*  para agregar atributos "tipoTarifa" y "comisionEnvioParaMostrar,se crean metodos get y set para los atributos creados</li>
* </ul>
* <p>
* <B>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</B>
* <p>
*
*/
public class ComisionTO implements Serializable {
	/**
	* N�mero de versi�n usado durante la serializaci�n.
	*/
	private static final long serialVersionUID = 1L;

 
	/**
	* Comision de envio.
	*/
	private BigDecimal comisionEnvio; 
	/**
	* Monto Gatos OUR.
	*/
	private BigDecimal montoGatos; 
	/**
	* Monto Gatos SWIFT.
	*/
	private BigDecimal montoGatos1; 
	/**
	* Tipo de Comision.
	*/
	private int tipoComision;
	/**
	* Tipo de Tarifa.
	*/
	private String tipoTarifa;
	/**
	* Tipo de comision de Envio.
	*/
	private String comsionEnvioParaMostrar;
	/**
	* Descripcion Tipo de Comision.
	*/
	private String descripcionTipo;
 
	public BigDecimal getComisionEnvio() {
		return comisionEnvio;
	}
  
	public void setComisionEnvio(BigDecimal comision) {
		this.comisionEnvio = comision;
	}
 
	public BigDecimal getMontoGatos() {
		return montoGatos;
	}
  
	public void setMontoGatos(BigDecimal monto) {
		this.montoGatos = monto;
	}
 
	public BigDecimal getMontoGatos1() {
		return montoGatos1;
	}
  
	public void setMontoGatos1(BigDecimal monto) {
		this.montoGatos1 = monto;
	}
 
	public int getTipoComision() {
		return tipoComision;
	}
  
	public void setTipoComision(int tipo) {
		this.tipoComision = tipo;
	}

	public String getDescripcionTipo() {
		return descripcionTipo;
	}
  
	public void setDescripcionTipo(String desc) {
		this.descripcionTipo = desc;
	}
	/**
	* Para obtener y loggear las propiedades.
	* <p>
	*
	* Registro de versiones:
	* <ul>
	* <li>1.0 20/08/2013 11:43:01 BcxWSql (Bcx Solutions) : Versi�n inicial.</li>
	* <li>1.1 28/03/2018 Angelo Venegas (Everis)- Minheli Mejias (Ing. Soft. Bci): Se modifica metodo para agregar 
	*  los atributos nuevos a la salida</li>
	* </ul>
	* <p>
	*
	* @return String todas las propiedades de esta clase.
	* 
	* @since 1.0
	*/
	public String toString() {
		String salida = "";
 
		salida += "[comisionEnvio = " + this.comisionEnvio + "]\n"; 
		salida += "[montoGatos = " + this.montoGatos + "]\n"; 
		salida += "[montoGatos1 = " + this.montoGatos1 + "]\n"; 
		salida += "[tipoComision = " + this.tipoComision + "]\n";
		salida += "[descripcionTipo = " + this.descripcionTipo + "]\n";
		salida += "[tipoTarifa = " + this.tipoTarifa + "]\n";
		salida += "[comsionEnvioParaMostrar = " + this.comsionEnvioParaMostrar + "]\n";

		return salida;
	}

	public String getTipoTarifa() {
		return tipoTarifa;
	}

	public void setTipoTarifa(String tipoTarifa) {
		this.tipoTarifa = tipoTarifa;
	}

	public String getComsionEnvioParaMostrar() {
		return comsionEnvioParaMostrar;
	}

	public void setComsionEnvioParaMostrar(String comsionEnvioParaMostrar) {
		this.comsionEnvioParaMostrar = comsionEnvioParaMostrar;
	}
}
