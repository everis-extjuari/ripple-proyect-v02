package wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Informaci�n de la transferencia ripple.
 * <p>
 * Registro de Versiones :
 * <ul>
 * <li>1.0 20/03/2019, Sergio Bustos B. (Everis) - Minheli Mejias (Ing.Soft. BCI): versi�n inicial</li>
 * </ul>
 * </p>
 * <p>
 * <B>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</B>
 * </p>
 */
public class TransferenciaPendienteRippleTO implements Serializable {

	/**
	* N�mero de versi�n usado durante la serializaci�n.
	*/
	private static final long serialVersionUID = 1L;
	
	/**
	 * Indicador resultado operacion.
	 */
	private int indicadorResultado;
	
	/**
	 * Mensaje resultado operacion.
	 */
	private String mensajeResultado;
	
	/**
	 * Numero de operacion.
	 */
	private String numeroOperacion;
	
	/**
	 * Nombre Beneficiario.
	 */
	private String nombreBeneficiario;
	
	/**
	 * Nombre Pais Bco del beneficiario.
	 */
	private String nombrePais;
	
	/**
	 * Nombre Moneda de la ITF.
	 */
	private String monedaTransferencia;
	
	/**
	 * Monto del pago.
	 */
	private BigDecimal montoTransferencia;
	
	/**
	 * Fecha de la operacion.
	 */
	private String fechaOperacion;
	
	/**
	 * Estado de la Transferencia.
	 */
	private String estadoTransferencia;
	
	/**
	 * Descripcion Estado.
	 */
	private String descripcionEstado;
	
	/**
	 * Codigo Pais Bco del beneficiario.
	 */
	private String paisBcoBeneficiario;
	
	/**
	 * Codigo Moneda de la ITF.
	 */
	private String codigoMonedaITF;
	
	/**
	 * Cuenta Origen de fondos.
	 */
	private String cuentaOrigenFondos;
	
	/**
	 * Codigo identificador del Beneficiario.
	 */
	private String codigoIdentificadorBene;
	
	/**
	 * Cuenta Corriente del beneficiario.
	 */
	private String cuentaCorrienteBene;
	
	/**
	 * Numero correlativo FBP.
	 */
	private int correlativoFBP;
	
	/**
	 * SysTimeStamp.
	 */
	private String sysTimeStamp;


	public int getIndicadorResultado() {
		return indicadorResultado;
	}

	public void setIndicadorResultado(int indicadorResultado) {
		this.indicadorResultado = indicadorResultado;
	}

	public String getMensajeResultado() {
		return mensajeResultado;
	}

	public void setMensajeResultado(String mensajeResultado) {
		this.mensajeResultado = mensajeResultado;
	}

	public String getNumeroOperacion() {
		return numeroOperacion;
	}

	public void setNumeroOperacion(String numeroOperacion) {
		this.numeroOperacion = numeroOperacion;
	}

	public String getNombreBeneficiario() {
		return nombreBeneficiario;
	}

	public void setNombreBeneficiario(String nombreBeneficiario) {
		this.nombreBeneficiario = nombreBeneficiario;
	}

	public String getNombrePais() {
		return nombrePais;
	}

	public void setNombrePais(String nombrePais) {
		this.nombrePais = nombrePais;
	}

	public String getMonedaTransferencia() {
		return monedaTransferencia;
	}

	public void setMonedaTransferencia(String monedaTransferencia) {
		this.monedaTransferencia = monedaTransferencia;
	}

	public BigDecimal getMontoTransferencia() {
		return montoTransferencia;
	}

	public void setMontoTransferencia(BigDecimal montoTransferencia) {
		this.montoTransferencia = montoTransferencia;
	}

	public String getFechaOperacion() {
		return fechaOperacion;
	}

	public void setFechaOperacion(String fechaOperacion) {
		this.fechaOperacion = fechaOperacion;
	}

	public String getEstadoTransferencia() {
		return estadoTransferencia;
	}

	public void setEstadoTransferencia(String estadoTransferencia) {
		this.estadoTransferencia = estadoTransferencia;
	}

	public String getDescripcionEstado() {
		return descripcionEstado;
	}

	public void setDescripcionEstado(String descripcionEstado) {
		this.descripcionEstado = descripcionEstado;
	}

	public String getPaisBcoBeneficiario() {
		return paisBcoBeneficiario;
	}

	public void setPaisBcoBeneficiario(String paisBcoBeneficiario) {
		this.paisBcoBeneficiario = paisBcoBeneficiario;
	}

	public String getCodigoMonedaITF() {
		return codigoMonedaITF;
	}

	public void setCodigoMonedaITF(String codigoMonedaITF) {
		this.codigoMonedaITF = codigoMonedaITF;
	}

	public String getCuentaOrigenFondos() {
		return cuentaOrigenFondos;
	}

	public void setCuentaOrigenFondos(String cuentaOrigenFondos) {
		this.cuentaOrigenFondos = cuentaOrigenFondos;
	}

	public String getCodigoIdentificadorBene() {
		return codigoIdentificadorBene;
	}

	public void setCodigoIdentificadorBene(String codigoIdentificadorBene) {
		this.codigoIdentificadorBene = codigoIdentificadorBene;
	}

	public String getCuentaCorrienteBene() {
		return cuentaCorrienteBene;
	}

	public void setCuentaCorrienteBene(String cuentaCorrienteBene) {
		this.cuentaCorrienteBene = cuentaCorrienteBene;
	}

	public int getCorrelativoFBP() {
		return correlativoFBP;
	}

	public void setCorrelativoFBP(int correlativoFBP) {
		this.correlativoFBP = correlativoFBP;
	}

	public String getSysTimeStamp() {
		return sysTimeStamp;
	}

	public void setSysTimeStamp(String sysTimeStamp) {
		this.sysTimeStamp = sysTimeStamp;
	}

	public String toString() {
		return "TransferenciaPendienteRippleTO [indicadorResultado=" + indicadorResultado + ", mensajeResultado="
				+ mensajeResultado + ", numeroOperacion=" + numeroOperacion + ", nombreBeneficiario="
				+ nombreBeneficiario + ", nombrePais=" + nombrePais + ", monedaTransferencia=" + monedaTransferencia
				+ ", montoTransferencia=" + montoTransferencia + ", fechaOperacion=" + fechaOperacion
				+ ", estadoTransferencia=" + estadoTransferencia + ", descripcionEstado=" + descripcionEstado
				+ ", PaisBcoBeneficiario=" + paisBcoBeneficiario + ", CodigoMonedaITF=" + codigoMonedaITF
				+ ", CuentaOrigenFondos=" + cuentaOrigenFondos + ", CodigoIdentificadorBene=" + codigoIdentificadorBene
				+ ", CuentaCorrienteBene=" + cuentaCorrienteBene + ", correlativoFBP=" + correlativoFBP
				+ ", sysTimeStamp=" + sysTimeStamp + "]";
	}
	
}
