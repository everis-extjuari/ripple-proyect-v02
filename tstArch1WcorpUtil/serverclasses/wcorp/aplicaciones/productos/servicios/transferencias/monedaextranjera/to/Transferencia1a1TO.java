package wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to;
import java.io.Serializable;
import java.math.BigDecimal;

/**
* Encapsula la salida del SP  FBPsrv_shh_ndp_1a1.
*
* <p>
* Registro de versiones:
* <ul>
* <li>1.0 20/08/2013 14:12:36 BcxWSql (Bcx Solutions) : Versi�n inicial.</li>
* <li>1.1 25/01/2019 Eneida Diaz (Everis)- Luz Carre�o (Ing. Soft. Bci) : Se agregan atributos cuentaCargoComisionWeb y monedaCuentaComision.
* 																		  Modificaci�n de m�todo {@link #toString()} para incluir los nuevos atributos</li>
* <li>1.2 25/03/2019, Sergio Bustos B. (Everis) - Minheli Mejias (Ing. Soft. BCI): Se agregan atributos para obtener valores relacionados a transferencias Ripple.</li>
* </ul>
* <p>
* <B>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</B>
* <p>
*
*/
public class Transferencia1a1TO implements Serializable {
	/**
	* N�mero de versi�n usado durante la serializaci�n.
	*/
	private static final long serialVersionUID = 1L;

 
	/**
	* Nombre Beneficiario.
	*/
	private String nombreBeneficiario; 
	/**
	* Pais de la cuenta.
	*/
	private String paisCuenta; 
	/**
	* Nombre de Pais.
	*/
	private String nombrePais; 
	/**
	* Moneda de la Transferencia.
	*/
	private String monedaTransferencia; 
	/**
	* Monto de la Transferencia.
	*/
	private BigDecimal montoTransferencia; 
	/**
	* Fecha de Operacion.
	*/
	private String fechaOperacion; 
	/**
	* Estado de la Transferencia.
	*/
	private String estadoTransferencia; 
	/**
	* Descripcion Estado.
	*/
	private String descripcionEstado; 
	/**
	* Origen de la Solicitud.
	*/
	private String origenSolicitud; 
	/**
	* Correlativo Interno FBP.
	*/
	private int correlativoInterno;
	/**
	* Cuenta Cargo Comision Web.
	*/
	private String cuentaCargoComisionWeb;
	/**
	* Moneda Cuenta Cargo Comisi�n.
	*/
	private String monedaCuentaComision;
	
	/**
	 * Indicador resultado operacion.
	 */
	private int indicadorResultado;
	
	/**
	 * Mensaje resultado operacion.
	 */
	private String mensajeResultado;
	
	/**
	 * Numero de operacion.
	 */
	private String numeroOperacion;
	
	/**
	 * Codigo Moneda de la ITF.
	 */
	private String codigoMonedaITF;
	
	/**
	 * Cuenta Origen de fondos.
	 */
	private String cuentaOrigenFondos;
	
	/**
	 * Codigo identificador del Beneficiario.
	 */
	private String codigoIdentificadorBene;
	
	/**
	 * Cuenta Corriente del beneficiario.
	 */
	private String cuentaCorrienteBene;
	
	/**
	 * SysTimeStamp.
	 */
	private String sysTimeStamp;
	
	public String getNombreBeneficiario() {
		return nombreBeneficiario;
	}
  
	public void setNombreBeneficiario(String nombre) {
		this.nombreBeneficiario = nombre;
	}
 
	public String getPaisCuenta() {
		return paisCuenta;
	}
  
	public void setPaisCuenta(String pais) {
		this.paisCuenta = pais;
	}
 
	public String getNombrePais() {
		return nombrePais;
	}
  
	public void setNombrePais(String nombre) {
		this.nombrePais = nombre;
	}
 
	public String getMonedaTransferencia() {
		return monedaTransferencia;
	}
  
	public void setMonedaTransferencia(String moneda) {
		this.monedaTransferencia = moneda;
	}
 
	public BigDecimal getMontoTransferencia() {
		return montoTransferencia;
	}
  
	public void setMontoTransferencia(BigDecimal monto) {
		this.montoTransferencia = monto;
	}
 
	public String getFechaOperacion() {
		return fechaOperacion;
	}
  
	public void setFechaOperacion(String fecha) {
		this.fechaOperacion = fecha;
	}
 
	public String getEstadoTransferencia() {
		return estadoTransferencia;
	}
  
	public void setEstadoTransferencia(String estado) {
		this.estadoTransferencia = estado;
	}
 
	public String getDescripcionEstado() {
		return descripcionEstado;
	}
  
	public void setDescripcionEstado(String descripcion) {
		this.descripcionEstado = descripcion;
	}
 
	public String getOrigenSolicitud() {
		return origenSolicitud;
	}
  
	public void setOrigenSolicitud(String origen) {
		this.origenSolicitud = origen;
	}
 
	public int getCorrelativoInterno() {
		return correlativoInterno;
	}
  
	public void setCorrelativoInterno(int correlativo) {
		this.correlativoInterno = correlativo;
	}

	public String getCuentaCargoComisionWeb() {
		return cuentaCargoComisionWeb;
	}

	public void setCuentaCargoComisionWeb(String cuentaCargoComisionWeb) {
		this.cuentaCargoComisionWeb = cuentaCargoComisionWeb;
	}
	
	public String getMonedaCuentaComision() {
		return monedaCuentaComision;
	}

	public void setMonedaCuentaComision(String monedaCuentaComision) {
		this.monedaCuentaComision = monedaCuentaComision;
	}

	/**
	 * @return the indicadorResultado
	 */
	public int getIndicadorResultado() {
		return indicadorResultado;
	}

	/**
	 * @return the mensajeResultado
	 */
	public String getMensajeResultado() {
		return mensajeResultado;
	}

	/**
	 * @return the numeroOperacion
	 */
	public String getNumeroOperacion() {
		return numeroOperacion;
	}

	/**
	 * @return the codigoMonedaITF
	 */
	public String getCodigoMonedaITF() {
		return codigoMonedaITF;
	}

	/**
	 * @return the cuentaOrigenFondos
	 */
	public String getCuentaOrigenFondos() {
		return cuentaOrigenFondos;
	}

	/**
	 * @return the codigoIdentificadorBene
	 */
	public String getCodigoIdentificadorBene() {
		return codigoIdentificadorBene;
	}

	/**
	 * @return the cuentaCorrienteBene
	 */
	public String getCuentaCorrienteBene() {
		return cuentaCorrienteBene;
	}

	/**
	 * @return the sysTimeStamp
	 */
	public String getSysTimeStamp() {
		return sysTimeStamp;
	}

	/**
	 * @param indicadorResultado the indicadorResultado to set
	 */
	public void setIndicadorResultado(int indicadorResultado) {
		this.indicadorResultado = indicadorResultado;
	}

	/**
	 * @param mensajeResultado the mensajeResultado to set
	 */
	public void setMensajeResultado(String mensajeResultado) {
		this.mensajeResultado = mensajeResultado;
	}

	/**
	 * @param numeroOperacion the numeroOperacion to set
	 */
	public void setNumeroOperacion(String numeroOperacion) {
		this.numeroOperacion = numeroOperacion;
	}

	/**
	 * @param codigoMonedaITF the codigoMonedaITF to set
	 */
	public void setCodigoMonedaITF(String codigoMonedaITF) {
		this.codigoMonedaITF = codigoMonedaITF;
	}

	/**
	 * @param cuentaOrigenFondos the cuentaOrigenFondos to set
	 */
	public void setCuentaOrigenFondos(String cuentaOrigenFondos) {
		this.cuentaOrigenFondos = cuentaOrigenFondos;
	}

	/**
	 * @param codigoIdentificadorBene the codigoIdentificadorBene to set
	 */
	public void setCodigoIdentificadorBene(String codigoIdentificadorBene) {
		this.codigoIdentificadorBene = codigoIdentificadorBene;
	}

	/**
	 * @param cuentaCorrienteBene the cuentaCorrienteBene to set
	 */
	public void setCuentaCorrienteBene(String cuentaCorrienteBene) {
		this.cuentaCorrienteBene = cuentaCorrienteBene;
	}

	/**
	 * @param sysTimeStamp the sysTimeStamp to set
	 */
	public void setSysTimeStamp(String sysTimeStamp) {
		this.sysTimeStamp = sysTimeStamp;
	}

	/**
	* Para obtener y loggear las propiedades.
	* <p>
	*
	* Registro de versiones:
	* <ul>
	*
	* <li>1.0 20/08/2013 11:43:01 BcxWSql (Bcx Solutions) : Versi�n inicial.</li>
	* <li>1.1 25/01/2019 Eneida Diaz (Everis)- Luz Carre�o (Ing. Soft. Bci) : Se incorporan atributos cuentaCargoComisionWeb y monedaCuentaComision.</li>
	* </ul>
	* <p>
	*
	* @return String todas las propiedades de esta clase.
	* 
	* @since 1.0
	*/

	public String toString() {
		String salida = "";
 
		salida += "[nombreBeneficiario = " + this.nombreBeneficiario + "]\n"; 
		salida += "[paisCuenta = " + this.paisCuenta + "]\n"; 
		salida += "[nombrePais = " + this.nombrePais + "]\n"; 
		salida += "[monedaTransferencia = " + this.monedaTransferencia + "]\n"; 
		salida += "[montoTransferencia = " + this.montoTransferencia + "]\n"; 
		salida += "[fechaOperacion = " + this.fechaOperacion + "]\n"; 
		salida += "[estadoTransferencia = " + this.estadoTransferencia + "]\n"; 
		salida += "[descripcionEstado = " + this.descripcionEstado + "]\n"; 
		salida += "[origenSolicitud = " + this.origenSolicitud + "]\n"; 
		salida += "[correlativoInterno = " + this.correlativoInterno + "]\n";
		salida += "[cuentaCargoComisionWeb = " + this.cuentaCargoComisionWeb + "]\n";
		salida += "[monedaCuentaComision = " + this.monedaCuentaComision + "]\n";
		
		return salida;
	}
}
