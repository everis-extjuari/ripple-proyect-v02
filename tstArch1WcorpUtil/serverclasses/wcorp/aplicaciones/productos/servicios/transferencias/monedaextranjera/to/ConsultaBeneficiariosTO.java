package wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to;
import java.io.Serializable;

/**
* Encapsula los par�metros de entrada para el SP  FBPsrv_shw_csb_1a1.
*
* <p>
* Registro de versiones:
* <ul>
* <li>1.0 20/08/2013 14:06:27 BcxWSql (Bcx Solutions) : Versi�n inicial.</li>
* </ul>
* <p>
* <B>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</B>
* <p>
*
*/
public class ConsultaBeneficiariosTO implements Serializable {
	/**
	* N�mero de versi�n usado durante la serializaci�n.
	*/
	private static final long serialVersionUID = 1L;


	/**
	* RUT Empresa.
	*/
	private String rutEmpresa;
	/**
	* MNDA de la cuenta.
	*/
	private String mndaCuenta;


	public String getRutEmpresa() {
		return rutEmpresa;
	}
  
	public void setRutEmpresa(String rut) {
		this.rutEmpresa = rut;
	}

	public String getMndaCuenta() {
		return mndaCuenta;
	}
  
	public void setMndaCuenta(String mnda) {
		this.mndaCuenta = mnda;
	}


	/**
	* Para obtener y loggear las propiedades.
	* <p>
	*
	* Registro de versiones:
	* <ul>
	*
	* <li>1.0 20/08/2013 11:43:01 BcxWSql (Bcx Solutions) : Versi�n inicial.</li>
	* </ul>
	* <p>
	*
	* @return String todas las propiedades de esta clase.
	* 
	* @since 1.0
	*/
	public String toString() {
		String salida = "";

		salida += "[rutEmpresa = " + this.rutEmpresa + "]\n";
		salida += "[mndaCuenta = " + this.mndaCuenta + "]\n";

		return salida;
	}
}
