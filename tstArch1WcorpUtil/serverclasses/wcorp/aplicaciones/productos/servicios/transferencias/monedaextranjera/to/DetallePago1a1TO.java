package wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to;
import java.io.Serializable;
import java.math.BigDecimal;

/**
* Encapsula la salida del SP  FBPsrv_shd_ndp_1a1.
*
* <p>
* Registro de versiones:
* <ul>
* <li>1.0 20/08/2013 15:00:13 BcxWSql (Bcx Solutions) : Versi�n inicial.</li>
* <li>1.1 1/10/2013 Benjamin Leon (Bcx Solutions) : Agregamos cinco propiedades al final.</li>
* <li>1.2 05/11/2015 Oscar Nahuelpan (SEnTRA) - Heraldo Hernandez (Ing. Soft. BCI): Se agregan los atributos
* {@link #nombre2}, {@link #nombre3}, {@link #direccion1}, {@link #direccion2}, {@link #ciudad1}, {@link #ciudad2},
* {@link #isoPaisBeneficiario}, {@link #nombrePaisBeneficiario}. Se modifica m�todo {@link #toString()}
* </li>
* <li>1.3 25/03/2019, Sergio Bustos B. (Everis) - Minheli Mejias (Ing. Soft. BCI): Se agregan atributos para obtener valores relacionados a transferencias Ripple.</li>
* </ul>
* <p>
* <B>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</B>
* <p>
*
*/
public class DetallePago1a1TO implements Serializable {
	/**
	* N�mero de versi�n usado durante la serializaci�n.
	*/
	private static final long serialVersionUID = 1L;

 
	/**
	* RUT Empresa.
	*/
	private String rutEmpresa; 
	/**
	* Moneda de la Cta de debito.
	*/
	private String monedaCta; 
	/**
	* Codigo Cuenta Debito Operacion.
	*/
	private String codigoCuenta; 
	/**
	* Cuenta Debito Operacion.
	*/
	private String cuentaDebito; 
	/**
	* Moneda de la nomina.
	*/
	private String monedaNomina; 
	/**
	* Numero de Folio.
	*/
	private String numeroFolio; 
	/**
	* Fecha de Operacion.
	*/
	private String fechaOperacion; 
	/**
	* Numero de operacion.
	*/
	private String numeroOperacion; 
	/**
	* Tipo de Operacion.
	*/
	private String tipoOperacion; 
	/**
	* Codigo identificador del Beneficiario.
	*/
	private String codigoIdentificador; 
	/**
	* Pais Bco del beneficiario.
	*/
	private String paisBco; 
	/**
	* Cuenta Corriente del beneficiario.
	*/
	private String cuentaCorriente; 
	/**
	* Nombre Beneficiario - linea 1.
	*/
	private String nombreBeneficiarioL1; 
	/**
	* Nombre Beneficiario - linea 2.
	*/
	private String nombreBeneficiarioL2; 
	/**
	* Nombre Beneficiario - linea 3.
	*/
	private String nombreBeneficiarioL3; 
	/**
	* Nombre Beneficiario - linea 4.
	*/
	private String nombreBeneficiarioL4; 
	/**
	* Direccion e-mail 1 del beneficiario.
	*/
	private String email1; 
	/**
	* Direccion e-mail 2 del beneficiario.
	*/
	private String email2; 
	/**
	* Direccion e-mail 3 del beneficiario.
	*/
	private String email3; 
	/**
	* BIC Banco del beneficiario.
	*/
	private String bicBanco; 
	/**
	* Ruta de pago banco del beneficiario.
	*/
	private String rutaPago; 
	/**
	* Nom. y dir. Bco del benef - linea 1.
	*/
	private String nomDirBancoL1; 
	/**
	* Nom. y dir. Bco del benef - linea 2.
	*/
	private String nomDirBancoL2; 
	/**
	* Nom. y dir. Bco del benef - linea 3.
	*/
	private String nomDirBancoL3; 
	/**
	* Nom. y dir. Bco del benef - linea 4.
	*/
	private String nomDirBancoL4; 
	/**
	* BIC del banco Intermediario.
	*/
	private String bicBancoIntermediario; 
	/**
	* Ruta de pago banco intermediario.
	*/
	private String rutaPagoIntermediario; 
	/**
	* Nom. y dir. Bco Intermediario - linea 1.
	*/
	private String nomDirIntermediarioL1; 
	/**
	* Nom. y dir. Bco Intermediario - linea 2.
	*/
	private String nomDirIntermediarioL2; 
	/**
	* Nom. y dir. Bco Intermediario - linea 3.
	*/
	private String nomDirIntermediarioL3; 
	/**
	* Nom. y dir. Bco Intermediario - linea 4.
	*/
	private String nomDirIntermediarioL4; 
	/**
	* Fecha de pago.
	*/
	private String fechaPago; 
	/**
	* Monto del pago.
	*/
	private BigDecimal montoPago; 
	/**
	* Detalle de gastos.
	*/
	private String detalleGastos; 
	/**
	* Informacion de la remesa.
	*/
	private String informacionRemesa; 
	/**
	* Codigo de Rechazo.
	*/
	private String codigoRechazo; 
	/**
	* Descripcion del rechazo.
	*/
	private String descripcionRechazo; 
	/**
	* Codigo de cambio.
	*/
	private String codigoCambio; 
	/**
	* For Further Credit.
	*/
	private String forFurther; 
	/**
	* Codigo Valuta.
	*/
	private String codigoValuta; 
	/**
	* SysTimeStamp.
	*/
	private String systimestamp;
	/**
	* Nombre Moneda.
	*/
	private String nombreMoneda; 
	/**
	* Nombre de Pais.
	*/
	private String nombrePais; 
	/**
	* Descripcion Detalle de gastos.
	*/
	private String descripcionDetalle; 
	/**
	* Nombre Cuenta Debito.
	*/
	private String nombreCuenta; 
	/**
	* Nombre Codigo de cambio.
	*/
	private String nombreCodigo;
	
    /**
     * Segundo nombre ingresado.
     */
    private String nombre2;
    
    /**
     * Direccion principal del beneficiario.
     */
    private String direccion1;
    
    /**
     * Segunda direccion ingresada.
     */
    private String direccion2;
    
    /**
     * Primera ciudad ingresada.
     */
    private String ciudad1;
    
    /**
     * Codigo Pais de Beneficiario.
     */
    private String isoPaisBeneficiario;
    
    /**
     * Nombre de Pais de Beneficiario.
     */
    private String nombrePaisBeneficiario;

    /**
     * Informaci�n adicional del beneficiario.
     */
    private String informacionAdicionalDos;
    
    
    private String nombreMonedaITF;     
    private String nombreMonedaCtaCte;   
    private String informacionAdicional;
    private String nombreBancoBeneficiario;
    private String nombrePaisBcoBene;
    private String comisionEnvio;        
    private String desTipoComision;
    private String tipoTarifa;        
    private int numeroPresPlanilla;
    private String CodigoGastosbancariosExt;
    private String CodigoPaisResidencia;
    
    
 
	public String getRutEmpresa() {
		return rutEmpresa;
	}
  
	public void setRutEmpresa(String rut) {
		this.rutEmpresa = rut;
	}
 
	public String getMonedaCta() {
		return monedaCta;
	}
  
	public void setMonedaCta(String moneda) {
		this.monedaCta = moneda;
	}
 
	public String getCodigoCuenta() {
		return codigoCuenta;
	}
  
	public void setCodigoCuenta(String codigo) {
		this.codigoCuenta = codigo;
	}
 
	public String getCuentaDebito() {
		return cuentaDebito;
	}
  
	public void setCuentaDebito(String cuenta) {
		this.cuentaDebito = cuenta;
	}
 
	public String getMonedaNomina() {
		return monedaNomina;
	}
  
	public void setMonedaNomina(String moneda) {
		this.monedaNomina = moneda;
	}
 
	public String getNumeroFolio() {
		return numeroFolio;
	}
  
	public void setNumeroFolio(String numero) {
		this.numeroFolio = numero;
	}
 
	public String getFechaOperacion() {
		return fechaOperacion;
	}
  
	public void setFechaOperacion(String fecha) {
		this.fechaOperacion = fecha;
	}
 
	public String getNumeroOperacion() {
		return numeroOperacion;
	}
  
	public void setNumeroOperacion(String numero) {
		this.numeroOperacion = numero;
	}
 
	public String getTipoOperacion() {
		return tipoOperacion;
	}
  
	public void setTipoOperacion(String tipo) {
		this.tipoOperacion = tipo;
	}
 
	public String getCodigoIdentificador() {
		return codigoIdentificador;
	}
  
	public void setCodigoIdentificador(String codigo) {
		this.codigoIdentificador = codigo;
	}
 
	public String getPaisBco() {
		return paisBco;
	}
  
	public void setPaisBco(String pais) {
		this.paisBco = pais;
	}
 
	public String getCuentaCorriente() {
		return cuentaCorriente;
	}
  
	public void setCuentaCorriente(String cuenta) {
		this.cuentaCorriente = cuenta;
	}
 
	public String getNombreBeneficiarioL1() {
		return nombreBeneficiarioL1;
	}
  
	public void setNombreBeneficiarioL1(String nombre) {
		this.nombreBeneficiarioL1 = nombre;
	}
 
	public String getNombreBeneficiarioL2() {
		return nombreBeneficiarioL2;
	}
  
	public void setNombreBeneficiarioL2(String nombre) {
		this.nombreBeneficiarioL2 = nombre;
	}
 
	public String getNombreBeneficiarioL3() {
		return nombreBeneficiarioL3;
	}
  
	public void setNombreBeneficiarioL3(String nombre) {
		this.nombreBeneficiarioL3 = nombre;
	}
 
	public String getNombreBeneficiarioL4() {
		return nombreBeneficiarioL4;
	}
  
	public void setNombreBeneficiarioL4(String nombre) {
		this.nombreBeneficiarioL4 = nombre;
	}
 
	public String getEmail1() {
		return email1;
	}
  
	public void setEmail1(String direccion) {
		this.email1 = direccion;
	}
 
	public String getEmail2() {
		return email2;
	}
  
	public void setEmail2(String direccion) {
		this.email2 = direccion;
	}
 
	public String getEmail3() {
		return email3;
	}
  
	public void setEmail3(String direccion) {
		this.email3 = direccion;
	}
 
	public String getBicBanco() {
		return bicBanco;
	}
  
	public void setBicBanco(String bic) {
		this.bicBanco = bic;
	}
 
	public String getRutaPago() {
		return rutaPago;
	}
  
	public void setRutaPago(String ruta) {
		this.rutaPago = ruta;
	}
 
	public String getNomDirBancoL1() {
		return nomDirBancoL1;
	}
  
	public void setNomDirBancoL1(String nom) {
		this.nomDirBancoL1 = nom;
	}
 
	public String getNomDirBancoL2() {
		return nomDirBancoL2;
	}
  
	public void setNomDirBancoL2(String nom) {
		this.nomDirBancoL2 = nom;
	}
 
	public String getNomDirBancoL3() {
		return nomDirBancoL3;
	}
  
	public void setNomDirBancoL3(String nom) {
		this.nomDirBancoL3 = nom;
	}
 
	public String getNomDirBancoL4() {
		return nomDirBancoL4;
	}
  
	public void setNomDirBancoL4(String nom) {
		this.nomDirBancoL4 = nom;
	}
 
	public String getBicBancoIntermediario() {
		return bicBancoIntermediario;
	}
  
	public void setBicBancoIntermediario(String bic) {
		this.bicBancoIntermediario = bic;
	}
 
	public String getRutaPagoIntermediario() {
		return rutaPagoIntermediario;
	}
  
	public void setRutaPagoIntermediario(String ruta) {
		this.rutaPagoIntermediario = ruta;
	}
 
	public String getNomDirIntermediarioL1() {
		return nomDirIntermediarioL1;
	}
  
	public void setNomDirIntermediarioL1(String nom) {
		this.nomDirIntermediarioL1 = nom;
	}
 
	public String getNomDirIntermediarioL2() {
		return nomDirIntermediarioL2;
	}
  
	public void setNomDirIntermediarioL2(String nom) {
		this.nomDirIntermediarioL2 = nom;
	}
 
	public String getNomDirIntermediarioL3() {
		return nomDirIntermediarioL3;
	}
  
	public void setNomDirIntermediarioL3(String nom) {
		this.nomDirIntermediarioL3 = nom;
	}
 
	public String getNomDirIntermediarioL4() {
		return nomDirIntermediarioL4;
	}
  
	public void setNomDirIntermediarioL4(String nom) {
		this.nomDirIntermediarioL4 = nom;
	}
 
	public String getFechaPago() {
		return fechaPago;
	}
  
	public void setFechaPago(String fecha) {
		this.fechaPago = fecha;
	}
 
	public BigDecimal getMontoPago() {
		return montoPago;
	}
  
	public void setMontoPago(BigDecimal monto) {
		this.montoPago = monto;
	}
 
	public String getDetalleGastos() {
		return detalleGastos;
	}
  
	public void setDetalleGastos(String detalle) {
		this.detalleGastos = detalle;
	}
 
	public String getInformacionRemesa() {
		return informacionRemesa;
	}
  
	public void setInformacionRemesa(String informacion) {
		this.informacionRemesa = informacion;
	}
 
	public String getCodigoRechazo() {
		return codigoRechazo;
	}
  
	public void setCodigoRechazo(String codigo) {
		this.codigoRechazo = codigo;
	}
 
	public String getDescripcionRechazo() {
		return descripcionRechazo;
	}
  
	public void setDescripcionRechazo(String descripcion) {
		this.descripcionRechazo = descripcion;
	}
 
	public String getCodigoCambio() {
		return codigoCambio;
	}
  
	public void setCodigoCambio(String codigo) {
		this.codigoCambio = codigo;
	}
 
	public String getForFurther() {
		return forFurther;
	}
  
	public void setForFurther(String xfor) {
		this.forFurther = xfor;
	}
 
	public String getCodigoValuta() {
		return codigoValuta;
	}
  
	public void setCodigoValuta(String codigo) {
		this.codigoValuta = codigo;
	}
 
	public String getSystimestamp() {
		return systimestamp;
	}
  
	public void setSystimestamp(String sys) {
		this.systimestamp = sys;
	}

	 
	public String getNombreMoneda() {
		return nombreMoneda;
	}
  
	public void setNombreMoneda(String nombre) {
		this.nombreMoneda = nombre;
	}
 
	public String getNombrePais() {
		return nombrePais;
	}
  
	public void setNombrePais(String nombre) {
		this.nombrePais = nombre;
	}
 
	public String getDescripcionDetalle() {
		return descripcionDetalle;
	}
  
	public void setDescripcionDetalle(String descripcion) {
		this.descripcionDetalle = descripcion;
	}
 
	public String getNombreCuenta() {
		return nombreCuenta;
	}
  
	public void setNombreCuenta(String nombre) {
		this.nombreCuenta = nombre;
	}
 
	public String getNombreCodigo() {
		return nombreCodigo;
	}
  
	public void setNombreCodigo(String nombre) {
		this.nombreCodigo = nombre;
	}

	public String getNombre2() {
        return nombre2;
    }

    public void setNombre2(String nombre2) {
        this.nombre2 = nombre2;
    }

    public String getDireccion1() {
        return direccion1;
    }

    public void setDireccion1(String direccion1) {
        this.direccion1 = direccion1;
    }

    public String getDireccion2() {
        return direccion2;
    }

    public void setDireccion2(String direccion2) {
        this.direccion2 = direccion2;
    }

    public String getCiudad1() {
        return ciudad1;
    }

    public void setCiudad1(String ciudad1) {
        this.ciudad1 = ciudad1;
    }

    public String getIsoPaisBeneficiario() {
        return isoPaisBeneficiario;
    }

    public void setIsoPaisBeneficiario(String isoPaisBeneficiario) {
        this.isoPaisBeneficiario = isoPaisBeneficiario;
    }

    public String getNombrePaisBeneficiario() {
        return nombrePaisBeneficiario;
    }

    public void setNombrePaisBeneficiario(String nombrePaisBeneficiario) {
        this.nombrePaisBeneficiario = nombrePaisBeneficiario;
    }

    public String getInformacionAdicionalDos() {
        return informacionAdicionalDos;
    }

    public void setInformacionAdicionalDos(String informacionAdicionalDos) {
        this.informacionAdicionalDos = informacionAdicionalDos;
    }


	/**
	 * @return the nombreMonedaITF
	 */
	public String getNombreMonedaITF() {
		return nombreMonedaITF;
	}

	/**
	 * @return the nombreMonedaCtaCte
	 */
	public String getNombreMonedaCtaCte() {
		return nombreMonedaCtaCte;
	}

	/**
	 * @return the informacionAdicional
	 */
	public String getInformacionAdicional() {
		return informacionAdicional;
	}

	/**
	 * @return the nombreBancoBeneficiario
	 */
	public String getNombreBancoBeneficiario() {
		return nombreBancoBeneficiario;
	}

	/**
	 * @return the nombrePaisBcoBene
	 */
	public String getNombrePaisBcoBene() {
		return nombrePaisBcoBene;
	}

	/**
	 * @return the comisionEnvio
	 */
	public String getComisionEnvio() {
		return comisionEnvio;
	}

	/**
	 * @return the desTipoComision
	 */
	public String getDesTipoComision() {
		return desTipoComision;
	}

	/**
	 * @return the tipoTarifa
	 */
	public String getTipoTarifa() {
		return tipoTarifa;
	}

	/**
	 * @return the numeroPresPlanilla
	 */
	public int getNumeroPresPlanilla() {
		return numeroPresPlanilla;
	}

	/**
	 * @return the codigoGastosbancariosExt
	 */
	public String getCodigoGastosbancariosExt() {
		return CodigoGastosbancariosExt;
	}

	/**
	 * @return the codigoPaisResidencia
	 */
	public String getCodigoPaisResidencia() {
		return CodigoPaisResidencia;
	}

	/**
	 * @param nombreMonedaITF the nombreMonedaITF to set
	 */
	public void setNombreMonedaITF(String nombreMonedaITF) {
		this.nombreMonedaITF = nombreMonedaITF;
	}

	/**
	 * @param nombreMonedaCtaCte the nombreMonedaCtaCte to set
	 */
	public void setNombreMonedaCtaCte(String nombreMonedaCtaCte) {
		this.nombreMonedaCtaCte = nombreMonedaCtaCte;
	}

	/**
	 * @param informacionAdicional the informacionAdicional to set
	 */
	public void setInformacionAdicional(String informacionAdicional) {
		this.informacionAdicional = informacionAdicional;
	}

	/**
	 * @param nombreBancoBeneficiario the nombreBancoBeneficiario to set
	 */
	public void setNombreBancoBeneficiario(String nombreBancoBeneficiario) {
		this.nombreBancoBeneficiario = nombreBancoBeneficiario;
	}

	/**
	 * @param nombrePaisBcoBene the nombrePaisBcoBene to set
	 */
	public void setNombrePaisBcoBene(String nombrePaisBcoBene) {
		this.nombrePaisBcoBene = nombrePaisBcoBene;
	}

	/**
	 * @param comisionEnvio the comisionEnvio to set
	 */
	public void setComisionEnvio(String comisionEnvio) {
		this.comisionEnvio = comisionEnvio;
	}

	/**
	 * @param desTipoComision the desTipoComision to set
	 */
	public void setDesTipoComision(String desTipoComision) {
		this.desTipoComision = desTipoComision;
	}

	/**
	 * @param tipoTarifa the tipoTarifa to set
	 */
	public void setTipoTarifa(String tipoTarifa) {
		this.tipoTarifa = tipoTarifa;
	}

	/**
	 * @param numeroPresPlanilla the numeroPresPlanilla to set
	 */
	public void setNumeroPresPlanilla(int numeroPresPlanilla) {
		this.numeroPresPlanilla = numeroPresPlanilla;
	}

	/**
	 * @param codigoGastosbancariosExt the codigoGastosbancariosExt to set
	 */
	public void setCodigoGastosbancariosExt(String codigoGastosbancariosExt) {
		CodigoGastosbancariosExt = codigoGastosbancariosExt;
	}

	/**
	 * @param codigoPaisResidencia the codigoPaisResidencia to set
	 */
	public void setCodigoPaisResidencia(String codigoPaisResidencia) {
		CodigoPaisResidencia = codigoPaisResidencia;
	}

	/**
	* Para obtener y loggear las propiedades.
	* <p>
	*
	* Registro de versiones:
	* <ul>
	*
	* <li>1.0 20/08/2013 11:43:01 BcxWSql (Bcx Solutions) : Versi�n inicial.</li>
	* <li>1.1 1/10/2013 Benjamin Leon (Bcx Solutions) : Agregamos cinco propiedades al final.</li>
	* </ul>
	* <p>
	*
	* @return String todas las propiedades de esta clase.
	* 
	* @since 1.0
	*/
	public String toString() {
		String salida = "";
 
		salida += "[rutEmpresa = " + this.rutEmpresa + "]\n"; 
		salida += "[monedaCta = " + this.monedaCta + "]\n"; 
		salida += "[codigoCuenta = " + this.codigoCuenta + "]\n"; 
		salida += "[cuentaDebito = " + this.cuentaDebito + "]\n"; 
		salida += "[monedaNomina = " + this.monedaNomina + "]\n"; 
		salida += "[numeroFolio = " + this.numeroFolio + "]\n"; 
		salida += "[fechaOperacion = " + this.fechaOperacion + "]\n"; 
		salida += "[numeroOperacion = " + this.numeroOperacion + "]\n"; 
		salida += "[tipoOperacion = " + this.tipoOperacion + "]\n"; 
		salida += "[codigoIdentificador = " + this.codigoIdentificador + "]\n"; 
		salida += "[paisBco = " + this.paisBco + "]\n"; 
		salida += "[cuentaCorriente = " + this.cuentaCorriente + "]\n"; 
		salida += "[nombreBeneficiarioL1 = " + this.nombreBeneficiarioL1 + "]\n"; 
		salida += "[nombreBeneficiarioL2 = " + this.nombreBeneficiarioL2 + "]\n"; 
		salida += "[nombreBeneficiarioL3 = " + this.nombreBeneficiarioL3 + "]\n"; 
		salida += "[nombreBeneficiarioL4 = " + this.nombreBeneficiarioL4 + "]\n"; 
		salida += "[email1 = " + this.email1 + "]\n"; 
		salida += "[email2 = " + this.email2 + "]\n"; 
		salida += "[email3 = " + this.email3 + "]\n"; 
		salida += "[bicBanco = " + this.bicBanco + "]\n"; 
		salida += "[rutaPago = " + this.rutaPago + "]\n"; 
		salida += "[nomDirBancoL1 = " + this.nomDirBancoL1 + "]\n"; 
		salida += "[nomDirBancoL2 = " + this.nomDirBancoL2 + "]\n"; 
		salida += "[nomDirBancoL3 = " + this.nomDirBancoL3 + "]\n"; 
		salida += "[nomDirBancoL4 = " + this.nomDirBancoL4 + "]\n"; 
		salida += "[bicBancoIntermediario = " + this.bicBancoIntermediario + "]\n"; 
		salida += "[rutaPagoIntermediario = " + this.rutaPagoIntermediario + "]\n"; 
		salida += "[nomDirIntermediarioL1 = " + this.nomDirIntermediarioL1 + "]\n"; 
		salida += "[nomDirIntermediarioL2 = " + this.nomDirIntermediarioL2 + "]\n"; 
		salida += "[nomDirIntermediarioL3 = " + this.nomDirIntermediarioL3 + "]\n"; 
		salida += "[nomDirIntermediarioL4 = " + this.nomDirIntermediarioL4 + "]\n"; 
		salida += "[fechaPago = " + this.fechaPago + "]\n"; 
		salida += "[montoPago = " + this.montoPago + "]\n"; 
		salida += "[detalleGastos = " + this.detalleGastos + "]\n"; 
		salida += "[informacionRemesa = " + this.informacionRemesa + "]\n"; 
		salida += "[codigoRechazo = " + this.codigoRechazo + "]\n"; 
		salida += "[descripcionRechazo = " + this.descripcionRechazo + "]\n"; 
		salida += "[codigoCambio = " + this.codigoCambio + "]\n"; 
		salida += "[forFurther = " + this.forFurther + "]\n"; 
		salida += "[codigoValuta = " + this.codigoValuta + "]\n"; 
		salida += "[systimestamp = " + this.systimestamp + "]\n";
		salida += "[nombreMoneda = " + this.nombreMoneda + "]\n"; 
		salida += "[nombrePais = " + this.nombrePais + "]\n"; 
		salida += "[descripcionDetalle = " + this.descripcionDetalle + "]\n"; 
		salida += "[nombreCuenta = " + this.nombreCuenta + "]\n"; 
		salida += "[nombreCodigo = " + this.nombreCodigo + "]\n";
		salida += "[nombre2 = " + this.nombre2 + "]\n"; 
		salida += "[direccion1 = " + this.direccion1 + "]\n"; 
		salida += "[direccion2 = " + this.direccion2 + "]\n"; 
		salida += "[ciudad1 = " + this.ciudad1 + "]\n"; 
		salida += "[isoPaisBeneficiario = " + this.isoPaisBeneficiario + "]\n"; 
		salida += "[nombrePaisBeneficiario = " + this.nombrePaisBeneficiario + "]\n"; 
		salida += "[informacionAdicionalDos = " + this.informacionAdicionalDos + "]\n"; 

		return salida;
	}

}
