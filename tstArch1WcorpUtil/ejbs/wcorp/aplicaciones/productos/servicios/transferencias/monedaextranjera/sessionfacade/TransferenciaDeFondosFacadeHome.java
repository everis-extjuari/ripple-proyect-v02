package wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.sessionfacade;

import java.rmi.RemoteException;

import javax.ejb.CreateException;
import javax.ejb.EJBHome;

/**
 * Clase Home del Session Facade que conecta los diferentes servicios del banco.
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 15/05/2009, Pei Kang Fu (Neoris Chile) - version inicial
 * </ul>
 * </p>
 * 
 * 
 * <p>
 * <b>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</b>
 * <p>
 * 
 */
public interface TransferenciaDeFondosFacadeHome extends EJBHome {

    public TransferenciaDeFondosFacade create() throws CreateException, RemoteException;

}
