package wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.dao.jdbc;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.sessionfacade.TtffException;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.Beneficiario1a1TO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.CambiaEstadoTttfMonexTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.CodigoCambioTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.ComisionTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.ConsultaBeneficiariosTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.ConsultaCodigoCambioTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.ConsultaDetBeneficiarioTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.ConsultaDetPagoTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.ConsultaMonedaTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.ConsultaTransferenciaTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.CuentaComisionTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.DetalleBeneficiarioTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.DetallePago1a1TO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.GastosBancoExteriorTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.Moneda1a1TO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.RespuestaActualizaTtffTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.RespuestaHorarioTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.Transferencia1a1TO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.TransferenciaMonexTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.TransferenciaPendienteRippleTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.ValutaTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.ConsultaBeneficiariosRippleTO;
import wcorp.util.StringUtil;
import wcorp.util.TablaValores;

import cl.bci.middleware.aplicacion.persistencia.ConectorServicioDePersistenciaDeDatos;
import cl.bci.middleware.aplicacion.persistencia.ejb.ServicioDatosException;

/**
 * Clase Dao que accede mediante el servicio de persistencia de datos los 
 * servicios disponibles desde la base de datos bcicomex.
 * 
 * <p>
 * Registro de versiones:<ul>
 * 
 * <li>1.0 20/08/2013 Benjamin Leon (Bcx Solutions): versi�n inicial.
 * <li>1.1 05/11/2015 Oscar Nahuelpan (SEnTRA) - Heraldo Hernandez (Ing. Soft. BCI): Se modifican los m�todos 
 * {@link #consultarDetallePago(ConsultaDetPagoTO), {@link #obtenerDetalleDelBeneficiario(ConsultaDetBeneficiarioTO)}.
 * Se normaliza log y se crea m�todo {@link #getLogger()}.</li>
 * <li>1.2 16/02/2017 Marcos Abraham Hernandez Bravo (ImageMaker IT) - Miguel Anza (BCI) : Se agrega/modifica log a los siguientes m�todos:
 * 		- {@link #obtenerCuentaDeCargo(String)}
 * 		- {@link #consultarHorarioAprobacion(int)}
 * 		- {@link #obtenerTransferencias(ConsultaTransferenciaTO)}
 * 		- {@link #obtenerFechaValuta(String)}
 * 		- {@link #obtenerDetalleDelBeneficiario(ConsultaDetBeneficiarioTO)}
 * </li>
 * <li>1.3 28/03/2018 Angelo Venegas (Everis)- Minheli Mejias (Ing. Soft. Bci) : Se modifica m�todo {@link #obtenerComisiones(int)} para obtener el tipo de tarifa.</li>
 * <li>1.4 20/03/2019 Sergio Bustos B. (Everis)- Minheli Mejias (Ing. Soft. Bci) : Se agrega el m�todo - {@link #obtenerTransferenciasPenRipple(ConsultaTransferenciaTO)}
 * 																									   - {@link #consultarDetallePagoRipple(ConsultaDetPagoTO)}.</li>
 * </ul><P>
 * 
 * <B>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</B
 * <P>
 */
public class TransferenciasUnoAUnoDAO {

    /**
     * Archivo de configuracion acceso a bd.
     */
    private static final String sqlMapBaseDatosTtff = "bcicomexunoauno";
	
    /**
     * Registro de eventos.
     */
    private transient Logger logger = (Logger) Logger.getLogger(this.getClass());
 
	/**
	* Invoca al procedimiento almacenado CVDsrv_shw_cur_vps.
	* <p>
	*
	* Registro de versiones:
	* <ul>
	*
	* <li>1.0 20/08/2013 10:06:15 BcxWSql (Bcx Solutions) : Versi�n inicial.</li>
	* </ul>
	* <p>
	*
	* @param resultSw obtenido desde la capa de datos.
	* @param resultMsg obtenido desde la capa de datos.
	* @param metodo nombre del metodo.
	* @throws TtffException En caso de ocurrir un error de negocio.
	* 
	* @since 1.0
	*/    
	private void procesaError(Integer resultSw, String resultMsg, String metodo) throws TtffException {
		
		final int errorOK = 0;
		final int errorNegocioDB = 1;
		final int errorNoHayDatosDB = 2;
		
		final String errorNegocio = "TTFF-023";
		final String errorNoHayDatos = "TTFF-024";
		final String errorSistema = "TTFF-025";
		final String errorIndicadorResultado = "TTFF-026";
		String errorCode = errorSistema;
		
		if (resultSw == null) {
			logger.error(metodo + "::'indicadorResultado' es un valor nulo");
			throw (new TtffException(errorIndicadorResultado));
		}
		if (resultSw.intValue() == errorOK)
			return;
		// Manejo de errores
		if (resultSw.intValue() == errorNegocioDB) {
			errorCode = errorNegocio;
		} 
		else if (resultSw.intValue() == errorNoHayDatosDB) {
			errorCode = errorNoHayDatos;	
		} 
		throw (new TtffException(errorCode, resultMsg));
			
	}    
	/**
	* Invoca al procedimiento almacenado CVDsrv_shw_cur_vps.
	* <p>
	*
	* Registro de versiones:
	* <ul>
	*
	* <li>1.0 20/08/2013 10:06:15 BcxWSql (Bcx Solutions) : Versi�n inicial.</li>
	* </ul>
	* <p>
	*
	* @param parIn ConsultaMonedaTO�Entrada para el SP CVDsrv_shw_cur_vps.
	* @return Moneda1a1TO[]�Salida del SP CVDsrv_shw_cur_vps.
	* @throws ServicioDatosException En caso de ocurrir un error en el servicio de datos.
	* @throws TtffException En caso de ocurrir un error de negocio.
	* 
	* @since 1.0
	*/
	public Moneda1a1TO[] obtenerMonedas(ConsultaMonedaTO parIn)
	throws ServicioDatosException,TtffException {
		List documento = null;
		HashMap salida = null;	
		Moneda1a1TO[] objeto = null;
	
		HashMap mapa = new HashMap();
		mapa.put("CHAINED_SUPPORT", "true");

		mapa.put("fld_cvd_cod_int", parIn.getOrigenSolicitud());
		mapa.put("fld_cvd_cod_pro", parIn.getCodigoProducto());
		
		String contexto = TablaValores.getValor("JNDIConfig.parametros", "cluster", "param");

		if (logger.isDebugEnabled()) {
			logger.debug("obtenerMonedas");
			logger.debug("[ConectorServicioDePersistenciaDeDatos] - getConnection");
		}
	
		ConectorServicioDePersistenciaDeDatos conector = new
			ConectorServicioDePersistenciaDeDatos(contexto);
		documento = conector.consultar(sqlMapBaseDatosTtff, "obtenerMonedas", mapa);
		if (documento != null) {
			if (logger.isDebugEnabled()) {
				logger.info("obtenerMonedas::Cantidad de registros retornados: " 
						+ documento.size());
			}		
			if (documento.size() > 0) {
		
				objeto = new Moneda1a1TO[documento.size()];
				for (int c = 0; c < documento.size(); c++) {
					Moneda1a1TO paso = new Moneda1a1TO();
					salida = (HashMap) documento.get(c);
								
					Integer resultSw = (Integer) salida.get("indicadorResultado");	
					
					String resultMsg = String.valueOf(salida.get("mensajeResultado"));
					procesaError(resultSw, resultMsg, "obtenerMonedas");
					paso.setCodigoIso(String.valueOf(salida.get("codigoIso")));
					paso.setNombreMoneda(String.valueOf(salida.get("nombreMoneda")));
					paso.setMtoMaximo((BigDecimal)(salida.get("mtoMaximo")));
			
					objeto[c] = paso;
					
				}
			}	
		
		}
		return objeto;
	}

	/**
	* Invoca al procedimiento almacenado FBPsrv_shw_acc_cyg.
	* <p>
	*
	* Registro de versiones:
	* <ul>
	*
	* <li>1.0 20/08/2013 11:00:47 BcxWSql (Bcx Solutions) : Versi�n inicial.</li>
	* <li> 1.1 16/02/2017 Marcos Abraham Hernandez Bravo (ImageMaker IT) - Miguel Anza (BCI) : Se agrega y normaliza log.</li>
	* </ul>
	* <p>
	*
	* @param parIn String�Entrada para el SP FBPsrv_shw_acc_cyg.
	* @return CuentaComisionTO�Salida del SP FBPsrv_shw_acc_cyg.
	* @throws ServicioDatosException En caso de ocurrir un error en el servicio de datos.
	* @throws TtffException En caso de ocurrir un error de negocio.
	* 
	* @since 1.0
	*/
	public CuentaComisionTO obtenerCuentaDeCargo(String parIn) throws ServicioDatosException, TtffException {
		
		final String nombreMetodo = "obtenerCuentaDeCargo";
		
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "] [BCI_INI]");
			getLogger().info("[" + nombreMetodo + "] [Parametro][parIn][" + StringUtil.contenidoDe(parIn) + "]");
		}
		
		List documento = null;
		HashMap salida = null;
		CuentaComisionTO objeto = null;
	
		HashMap parametros = new HashMap();
		parametros.put("CHAINED_SUPPORT", "true");
		parametros.put("fld_cln_rut_emp", parIn);
				
		String contexto = TablaValores.getValor("JNDIConfig.parametros", "cluster", "param");

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[" + nombreMetodo + "] [parametros] [" + StringUtil.contenidoDe(parametros) + "]");
			getLogger().debug("[" + nombreMetodo + "] [contexto] [" + StringUtil.contenidoDe(contexto) + "]");
		}
		
		ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
		documento = conector.consultar(sqlMapBaseDatosTtff, "obtenerCuentaDeCargo", parametros);
		
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[" + nombreMetodo + "] [documento.size] [" + (documento != null ? documento.size() : 0) + "]");
			getLogger().debug("[" + nombreMetodo + "] [documento] [" + StringUtil.contenidoDe(documento) + "]");
		}
		
		if (documento != null && documento.size() > 0) {
			objeto = new CuentaComisionTO();
			
			salida = (HashMap) documento.get(0);		
			Integer resultSw = (Integer) salida.get("indicadorResultado");
			String resultMsg = String.valueOf(salida.get("mensajeResultado"));
			
			procesaError(resultSw, resultMsg, "obtenerCuentaDeCargo");
			
			objeto.setMonedaCtacte(String.valueOf(salida.get("monedaCtacte")));
			objeto.setCtaDebito(String.valueOf(salida.get("ctaDebito")));
		}
		
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "] [BCI_FINOK][" + StringUtil.contenidoDe(objeto) + "]");
		}
		return objeto;
	}


	/**
	* Invoca al procedimiento almacenado RTMsrv_shw_cmb_exi.
	* <p>
	*
	* Registro de versiones:
	* <ul>
	*
	* <li>1.0 21/08/2013 07:33:55 BcxWSql (Bcx Solutions) : Versi�n inicial.</li>
	* </ul>
	* <p>
	*
	* @param parIn ConsultaCodigoCambioTO Entrada para el SP RTMsrv_shw_cmb_exi.
	* @return CodigoCambioTO[] Salida del SP RTMsrv_shw_cmb_exi.
	* @throws ServicioDatosException En caso de ocurrir un error en el servicio de datos.
	* @throws TtffException En caso de ocurrir un error de negocio.
	* @since 1.0
	*/
	public CodigoCambioTO[] obtenerCodigosDeCambio(ConsultaCodigoCambioTO parIn)
	throws ServicioDatosException,TtffException {
		List documento = null;
		HashMap salida = null;	
		CodigoCambioTO[] objeto = null;
	
		HashMap mapa = new HashMap();
		mapa.put("CHAINED_SUPPORT", "true");

		mapa.put("fld_rtm_cod_int", parIn.getOrigenSolicitud());
		mapa.put("fld_cvd_cod_pro", parIn.getCodigoProducto());
		mapa.put("fld_rtm_mda_orf", parIn.getMonedaOrigen());
		mapa.put("fld_rtm_mda_dtf", parIn.getMonedaDestino());
		
		String contexto = TablaValores.getValor("JNDIConfig.parametros", "cluster", "param");

		if (logger.isDebugEnabled()) {
			logger.debug("obtenerCodigosDeCambio");
			logger.debug("[ConectorServicioDePersistenciaDeDatos] - getConnection");
		}
	
		ConectorServicioDePersistenciaDeDatos conector = new
			ConectorServicioDePersistenciaDeDatos(contexto);
		documento = conector.consultar(sqlMapBaseDatosTtff, "obtenerCodigosDeCambio", mapa);
		if (documento != null) {
			if (logger.isDebugEnabled()) {
				logger.info("obtenerCodigosDeCambio::Cantidad de registros retornados: " 
						+ documento.size());
			}		
			if (documento.size() > 0) {
		
				objeto = new CodigoCambioTO[documento.size()];
				for (int c = 0; c < documento.size(); c++) {
					CodigoCambioTO paso = new CodigoCambioTO();
					salida = (HashMap) documento.get(c);
			
					Integer resultSw = (Integer) salida.get("indicadorResultado");	
					
					String resultMsg = String.valueOf(salida.get("mensajeResultado"));
			
					procesaError(resultSw, resultMsg, "obtenerCodigosDeCambio");
			
					paso.setCodigoCambio(String.valueOf(salida.get("codigoCambio")));
					paso.setDescripcion(String.valueOf(salida.get("descripcion")));
			
					objeto[c] = paso;
					
				}
			}	
		
		}
		return objeto;
	}

	/**
	* Invoca al procedimiento almacenado FBPsrv_shw_fvl_sts.
	* <p>
	*
	* Registro de versiones:
	* <ul>
	*
	* <li>1.0 20/08/2013 13:05:59 BcxWSql (Bcx Solutions) : Versi�n inicial.</li>
	* <li> 1.1 16/02/2017 Marcos Abraham Hernandez Bravo (ImageMaker IT) - Miguel Anza (BCI) : Se agrega y normaliza log.</li>
	* </ul>
	* <p>
	*
	* @param parIn String Entrada para el SP FBPsrv_shw_fvl_sts.
	* @return ValutaTO[] Salida del SP FBPsrv_shw_fvl_sts.
	* @throws ServicioDatosException En caso de ocurrir un error en el servicio de datos.
	* @throws TtffException En caso de ocurrir un error de negocio.
	* 
	* @since 1.0
	*/
	public ValutaTO[] obtenerFechaValuta(String parIn) throws ServicioDatosException, TtffException {
		
		final String nombreMetodo = "obtenerFechaValuta";
		
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "] [BCI_INI]");
			getLogger().info("[" + nombreMetodo + "] [Parametro][parIn][" + StringUtil.contenidoDe(parIn) + "]");
		}
		
		List documento = null;
		HashMap salida = null;	
		ValutaTO[] objeto = null;
	
		HashMap parametros = new HashMap();
		parametros.put("CHAINED_SUPPORT", "true");
		parametros.put("fld_ndp_mda_dtf", parIn);
		
		String contexto = TablaValores.getValor("JNDIConfig.parametros", "cluster", "param");

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[" + nombreMetodo + "] [parametros] [" + StringUtil.contenidoDe(parametros) + "]");
			getLogger().debug("[" + nombreMetodo + "] [contexto] [" + StringUtil.contenidoDe(contexto) + "]");
		}
		
		ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
		documento = conector.consultar(sqlMapBaseDatosTtff, "obtenerFechaValuta", parametros);
		
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[" + nombreMetodo + "] [documento.size] [" + (documento != null ? documento.size() : 0) + "]");
			getLogger().debug("[" + nombreMetodo + "] [documento] [" + StringUtil.contenidoDe(documento) + "]");
		}
		
		if (documento != null && documento.size() > 0) {
			objeto = new ValutaTO[documento.size()];
			for (int c = 0; c < documento.size(); c++) {
				ValutaTO paso = new ValutaTO();
				salida = (HashMap) documento.get(c);
		
				Integer resultSw = (Integer) salida.get("indicadorResultado");
				
				String resultMsg = String.valueOf(salida.get("mensajeResultado"));
		
				procesaError(resultSw, resultMsg, "obtenerFechaValuta");
		
				paso.setCodigoValuta(String.valueOf(salida.get("codigoValuta")));
				paso.setDescripcion(String.valueOf(salida.get("descripcion")));
		
				objeto[c] = paso;
				
			}
		}
		
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "] [BCI_FINOK][" + StringUtil.contenidoDe(objeto) + "]");
		}
		
		return objeto;
	}

	/**
	* Invoca al procedimiento almacenado FBPsrv_cap_tim_lim.
	* <p>
	*
	* Registro de versiones:
	* <ul>
	*
	* <li>1.0 20/08/2013 13:13:16 BcxWSql (Bcx Solutions) : Versi�n inicial.</li>
	* <li> 1.1 16/02/2017 Marcos Abraham Hernandez Bravo (ImageMaker IT) - Miguel Anza (BCI) : Se agrega y normaliza log.</li>
	* </ul>
	* <p>
	*
	* @param parIn int Entrada para el SP FBPsrv_cap_tim_lim.
	* @return RespuestaHorarioTO Salida del SP FBPsrv_cap_tim_lim.
	* @throws ServicioDatosException En caso de ocurrir un error en el servicio de datos.
	* @throws TtffException En caso de ocurrir un error de negocio.
	* 
	* @since 1.0
	*/
	public RespuestaHorarioTO consultarHorarioAprobacion(int parIn) throws ServicioDatosException, TtffException {
		
		final String nombreMetodo = "consultarHorarioAprobacion";
		
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "] [BCI_INI]");
			getLogger().info("[" + nombreMetodo + "] [Parametro][parIn][" + StringUtil.contenidoDe(new Integer(parIn)) + "]");
		}
		
		List documento = null;
		HashMap salida = null;	
		RespuestaHorarioTO objeto = null;
	
		HashMap parametros = new HashMap();
		parametros.put("CHAINED_SUPPORT", "true");
		parametros.put("fld_ndp_sys_num", new Integer(parIn));
		
		String contexto = TablaValores.getValor("JNDIConfig.parametros", "cluster", "param");
		
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[" + nombreMetodo + "] [parametros] [" + StringUtil.contenidoDe(parametros) + "]");
			getLogger().debug("[" + nombreMetodo + "] [contexto] [" + StringUtil.contenidoDe(contexto) + "]");
		}
		
		ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);		
		documento = conector.consultar(sqlMapBaseDatosTtff, "consultarHorarioAprobacion", parametros);
		
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[" + nombreMetodo + "] [documento.size] [" + (documento != null ? documento.size() : 0) + "]");
			getLogger().debug("[" + nombreMetodo + "] [documento] [" + StringUtil.contenidoDe(documento) + "]");
		}
		
		if (documento != null && documento.size() > 0) {
			objeto = new RespuestaHorarioTO();
			
			salida = (HashMap) documento.get(0);
			Integer resultSw = (Integer) salida.get("indicadorResultado");			
			String resultMsg = String.valueOf(salida.get("mensajeResultado"));
			
			objeto.setIndicadorResultado(resultSw.intValue());
			objeto.setMensajeResultado(resultMsg);
		}
		
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "] [BCI_FINOK][" + StringUtil.contenidoDe(objeto) + "]");
		}
		
		return objeto;
	}

	/**
	* Invoca al procedimiento almacenado FBPsrv_jct_tim_lim.
	* <p>
	*
	* Registro de versiones:
	* <ul>
	*
	* <li>1.0 20/08/2013 14:05:27 BcxWSql (Bcx Solutions) : Versi�n inicial.</li>
	* </ul>
	* <p>
	*
	* @throws ServicioDatosException En caso de ocurrir un error en el servicio de datos.
	* @throws TtffException En caso de ocurrir un error de negocio.
	* 
	* @since 1.0
	*/
	public void consultarHorarioIngreso()
	throws ServicioDatosException,TtffException {
		List documento = null;
		HashMap salida = null;	
	
		HashMap mapa = new HashMap();
		mapa.put("CHAINED_SUPPORT", "true");

		Integer resultSw = null;
		
		
		String resultMsg = "";
		
		String contexto = TablaValores.getValor("JNDIConfig.parametros", "cluster", "param");

		if (logger.isDebugEnabled()) {
			logger.debug("consultarHorarioIngreso");
			logger.debug("[ConectorServicioDePersistenciaDeDatos] - getConnection");
		}
	
		ConectorServicioDePersistenciaDeDatos conector = new
			ConectorServicioDePersistenciaDeDatos(contexto);
		documento = conector.consultar(sqlMapBaseDatosTtff, "consultarHorarioIngreso", mapa);
		if (documento != null) {
			if (logger.isDebugEnabled()) {
				logger.info("consultarHorarioIngreso::Cantidad de registros retornados: " 
						+ documento.size());
			}		
			if (documento.size() > 0) {
				
				
				salida = (HashMap) documento.get(0);	
			
				resultSw = (Integer) salida.get("indicadorResultado");
				
				resultMsg = String.valueOf(salida.get("mensajeResultado"));
				
					
			}
		}
		procesaError(resultSw, resultMsg, "consultarHorarioIngreso");				
		
	}

	/**
	* Invoca al procedimiento almacenado FBPsrv_upd_itf_dta.
	* <p>
	*
	* Registro de versiones:
	* <ul>
	*
	* <li>1.0 20/08/2013 13:18:36 BcxWSql (Bcx Solutions) : Versi�n inicial.</li>
	* </ul>
	* <p>
	*
	* @param parIn TransferenciaMonexTO Entrada para el SP FBPsrv_upd_itf_dta.
	* @return RespuestaActualizaTtffTO Salida del SP FBPsrv_upd_itf_dta.
	* @throws ServicioDatosException En caso de ocurrir un error en el servicio de datos.
	* @throws TtffException En caso de ocurrir un error de negocio.
	* 
	* @since 1.0
	*/
	public RespuestaActualizaTtffTO actualizarTransferenciaMonex(TransferenciaMonexTO parIn)
	throws ServicioDatosException,TtffException {
		List documento = null;
		HashMap salida = null;	
		RespuestaActualizaTtffTO objeto = null;
	
		HashMap mapa = new HashMap();
		mapa.put("CHAINED_SUPPORT", "true");

		mapa.put("fld_1a1_cod_int", parIn.getOrigenSolicitud());
		mapa.put("fld_1a1_cod_sys", parIn.getCodigoProducto());
		mapa.put("fld_1a1_tip_acc", parIn.getServicioSolicitado());
		mapa.put("fld_hdr_rut_ord", parIn.getRutEmpresa());
		mapa.put("fld_hdr_nom_ord", parIn.getNombreEmpresa());
		mapa.put("fld_hdr_mda_itf", parIn.getMonedaNomina());
		mapa.put("fld_hdr_fec_ope", parIn.getFechaOperacion());
		mapa.put("fld_hdr_cta_orf", parIn.getCtaCte());
		mapa.put("fld_hdr_mto_orf", parIn.getMontoDebito());
		mapa.put("fld_hdr_cod_fvl", parIn.getValutaPago());
		mapa.put("fld_det_mto_pag", parIn.getMontoPago());
		mapa.put("fld_det_det_gto", parIn.getDetalleGastos());
		mapa.put("fld_det_inf_rem", parIn.getInformacionRemesa());
		mapa.put("fld_cyg_mda_cta", parIn.getMonedaCtacte());
		mapa.put("fld_cyg_cta_cte", parIn.getCtacteDebito());
		mapa.put("fld_det_tip_ope", parIn.getTipoOperacion());
		mapa.put("fld_det_cod_ben", parIn.getCodigoIdentificador());
		mapa.put("fld_det_ctr_ben", parIn.getPaisBeneficiario());
		mapa.put("fld_det_num_cta", parIn.getCuentaCorriente());
		mapa.put("fld_det_nbn_ln1", parIn.getNombreBeneficiarioL1());
		mapa.put("fld_det_nbn_ln2", parIn.getNombreBeneficiarioL2());
		mapa.put("fld_det_nbn_ln3", parIn.getNombreBeneficiarioL3());
		mapa.put("fld_det_nbn_ln4", parIn.getNombreBeneficiarioL4());
		mapa.put("fld_det_eml_dr1", parIn.getEmail1());
		mapa.put("fld_det_eml_dr2", parIn.getEmail2());
		mapa.put("fld_det_eml_dr3", parIn.getEmail3());
		mapa.put("fld_det_bco_bbn", parIn.getBancoBeneficiario());
		mapa.put("fld_det_rpg_bbn", parIn.getRutaPago());
		mapa.put("fld_det_nd1_bbn", parIn.getNomDirBancoL1());
		mapa.put("fld_det_nd2_bbn", parIn.getNomDirBancoL2());
		mapa.put("fld_det_nd3_bbn", parIn.getNomDirBancoL3());
		mapa.put("fld_det_nd4_bbn", parIn.getNomDirBancoL4());
		mapa.put("fld_det_bic_bin", parIn.getBicBancoIntermediario());
		mapa.put("fld_det_rpg_bin", parIn.getRutaPagoIntermediario());
		mapa.put("fld_det_nd1_bin", parIn.getNomDirIntermediarioL1());
		mapa.put("fld_det_nd2_bin", parIn.getNomDirIntermediarioL2());
		mapa.put("fld_det_nd3_bin", parIn.getNomDirIntermediarioL3());
		mapa.put("fld_det_nd4_bin", parIn.getNomDirIntermediarioL4());
		mapa.put("fld_det_cod_cmb", parIn.getCodigoCambio());
		mapa.put("fld_hdr_sys_num", new Integer(parIn.getNumeroCorrelativoFbp()));
		mapa.put("fld_det_SysTms", parIn.getSystimestamp());
		mapa.put("fld_det_usr_eve", parIn.getUsuario());
		mapa.put("fld_det_nip_usr", parIn.getIpMaquina());
		mapa.put("fld_det_ffc_bnf", parIn.getForFurther());
		
		String contexto = TablaValores.getValor("JNDIConfig.parametros", "cluster", "param");

		if (logger.isDebugEnabled()) {
			logger.debug("actualizarTransferenciaMonex");
			logger.debug("[ConectorServicioDePersistenciaDeDatos] - getConnection");
		}
	
		ConectorServicioDePersistenciaDeDatos conector = new
			ConectorServicioDePersistenciaDeDatos(contexto);
		documento = conector.consultar(sqlMapBaseDatosTtff, "actualizarTransferenciaMonex", mapa);
		if (documento != null) {
			if (logger.isDebugEnabled()) {
				logger.info("actualizarTransferenciaMonex::Cantidad de registros retornados: " 
						+ documento.size());
			}		
			if (documento.size() > 0) {
				
				objeto = new RespuestaActualizaTtffTO();
				
				salida = (HashMap) documento.get(0);	
			
				Integer resultSw = (Integer) salida.get("indicadorResultado");
							
				String resultMsg = String.valueOf(salida.get("mensajeResultado"));
				
				procesaError(resultSw, resultMsg, "actualizarTransferenciaMonex");
				
                objeto.setNumeroCorrelativo(Integer.parseInt(salida.get("numeroCorrelativo").toString()));
				objeto.setNumeroOperacion(String.valueOf(salida.get("numeroOperacion")));
				objeto.setSystimestamp(String.valueOf(salida.get("systimestamp")));	
			}
		}
		return objeto;
	}


	/**
	* Invoca al procedimiento almacenado FBPsrv_shw_bex_cyg.
	* <p>
	*
	* Registro de versiones:
	* <ul>
	*
	* <li>1.0 20/08/2013 14:05:16 BcxWSql (Bcx Solutions) : Versi�n inicial.</li>
	* </ul>
	* <p>
	*
	* @return GastosBancoExteriorTO[] Salida del SP FBPsrv_shw_bex_cyg.
	* @throws ServicioDatosException En caso de ocurrir un error en el servicio de datos.
	* @throws TtffException En caso de ocurrir un error de negocio.
	* 
	* @since 1.0
	*/
	public GastosBancoExteriorTO[] obtenerGastosBancoExterior()
	throws ServicioDatosException,TtffException {
		List documento = null;
		HashMap salida = null;	
		GastosBancoExteriorTO[] objeto = null;
	
		HashMap mapa = new HashMap();
		mapa.put("CHAINED_SUPPORT", "true");

		
		String contexto = TablaValores.getValor("JNDIConfig.parametros", "cluster", "param");

		if (logger.isDebugEnabled()) {
			logger.debug("obtenerGastosBancoExterior");
			logger.debug("[ConectorServicioDePersistenciaDeDatos] - getConnection");
		}
	
		ConectorServicioDePersistenciaDeDatos conector = new
			ConectorServicioDePersistenciaDeDatos(contexto);
		documento = conector.consultar(sqlMapBaseDatosTtff, "obtenerGastosBancoExterior", mapa);
		if (documento != null) {
			if (logger.isDebugEnabled()) {
				logger.info("obtenerGastosBancoExterior::Cantidad de registros retornados: " 
						+ documento.size());
			}		
			if (documento.size() > 0) {
		
				objeto = new GastosBancoExteriorTO[documento.size()];
				for (int c = 0; c < documento.size(); c++) {
					GastosBancoExteriorTO paso = new GastosBancoExteriorTO();
					salida = (HashMap) documento.get(c);
			
					Integer resultSw = (Integer) salida.get("indicadorResultado");
					String resultMsg = String.valueOf(salida.get("mensajeResultado"));
					logger.info("obtenerGastosBancoExterior::" + resultSw + ":" + resultMsg);
					procesaError(resultSw, resultMsg, "obtenerGastosBancoExterior");
					paso.setCodigoGastos(String.valueOf(salida.get("codigoGastos")));
					paso.setDescripcion(String.valueOf(salida.get("descripcion")));

					objeto[c] = paso;
					
				}
			}	
		
		}
		return objeto;
	}

	/**
	* Invoca al procedimiento almacenado FBPsrv_chg_sts_itf.
	* <p>
	*
	* Registro de versiones:
	* <ul>
	*
	* <li>1.0 20/08/2013 14:05:40 BcxWSql (Bcx Solutions) : Versi�n inicial.</li>
	* </ul>
	* <p>
	*
	* @param parIn ambiaEstadoTttfMonexTO Entrada para el SP FBPsrv_chg_sts_itf.
	* @return String Salida del SP FBPsrv_chg_sts_itf.
	* @throws ServicioDatosException En caso de ocurrir un error en el servicio de datos.
	* @throws TtffException En caso de ocurrir un error de negocio.
	* 
	* @since 1.0
	*/
	public String cambiarEstadoTtffMonex(CambiaEstadoTttfMonexTO parIn)
	throws ServicioDatosException,TtffException {
		List documento = null;
		HashMap salida = null;	
		String objeto = null;
	
		HashMap mapa = new HashMap();
		mapa.put("CHAINED_SUPPORT", "true");

		mapa.put("fld_1a1_cod_int", parIn.getOrigenSolicitud());
		mapa.put("fld_1a1_usr_typ", parIn.getTipoUsuario());
		mapa.put("fld_1a1_tip_acc", parIn.getServicioSolicitado());
		mapa.put("fld_hdr_sys_num", new Integer(parIn.getNumeroCorrelativo()));
		mapa.put("fld_hdr_SysTms", parIn.getSystimestamp());
		mapa.put("fld_fbp_usr_eve", parIn.getUsuarioEvento());
		mapa.put("fld_fbp_nip_usr", parIn.getIpTerminal());
		mapa.put("fld_fbp_txt_eve", parIn.getComentarios());
		
		String contexto = TablaValores.getValor("JNDIConfig.parametros", "cluster", "param");

		if (logger.isDebugEnabled()) {
			logger.debug("cambiarEstadoTtffMonex");
			logger.debug("[ConectorServicioDePersistenciaDeDatos] - getConnection");
		}
	
		ConectorServicioDePersistenciaDeDatos conector = new
			ConectorServicioDePersistenciaDeDatos(contexto);
		documento = conector.consultar(sqlMapBaseDatosTtff, "cambiarEstadoTtffMonex", mapa);
		if (documento != null) {
			if (logger.isDebugEnabled()) {
				logger.info("cambiarEstadoTtffMonex::Cantidad de registros retornados: " 
						+ documento.size());
			}		
			if (documento.size() > 0) {
		
				salida = (HashMap) documento.get(0);	
			
				Integer resultSw = (Integer) salida.get("indicadorResultado");
								
				String resultMsg = String.valueOf(salida.get("mensajeResultado"));
				
				procesaError(resultSw, resultMsg, "cambiarEstadoTtffMonex");
				objeto = String.valueOf(salida.get("systimestamp"));	
			}
		}
		return objeto;
	}

	/**
	* Invoca al procedimiento almacenado FBPsrv_shw_swf_1a1.
	* <p>
	*
	* Registro de versiones:
	* <ul>
	*
	* <li>1.0 20/08/2013 18:08:52 BcxWSql (Bcx Solutions) : Versi�n inicial.</li>
	* </ul>
	* <p>
	*
	* @param parIn int Entrada para el SP FBPsrv_shw_swf_1a1.
	* @return String[] Salida del SP FBPsrv_shw_swf_1a1.
	* @throws ServicioDatosException En caso de ocurrir un error en el servicio de datos.
	* @throws TtffException En caso de ocurrir un error de negocio.
	* 
	* @since 1.0
	*/
	public String[] obtenerMensajeSwift(int parIn)
	throws ServicioDatosException,TtffException {
		List documento = null;
		HashMap salida = null;	
		String[] objeto = null;
	
		HashMap mapa = new HashMap();
		mapa.put("CHAINED_SUPPORT", "true");

		mapa.put("fld_hdr_sys_num", new Integer(parIn));
		
		String contexto = TablaValores.getValor("JNDIConfig.parametros", "cluster", "param");

		if (logger.isDebugEnabled()) {
			logger.debug("obtenerMensajeSwift");
			logger.debug("[ConectorServicioDePersistenciaDeDatos] - getConnection");
		}
	
		ConectorServicioDePersistenciaDeDatos conector = new
			ConectorServicioDePersistenciaDeDatos(contexto);
		documento = conector.consultar(sqlMapBaseDatosTtff, "obtenerMensajeSwift", mapa);
		if (documento != null) {
			if (logger.isDebugEnabled()) {
				logger.info("obtenerMensajeSwift::Cantidad de registros retornados: " 
						+ documento.size());
			}		
			if (documento.size() > 0) {
		
				objeto = new String[documento.size()];
				for (int c = 0; c < documento.size(); c++) {
					
					salida = (HashMap) documento.get(c);
			
					Integer resultSw = (Integer) salida.get("indicadorResultado");	
					
					String resultMsg = String.valueOf(salida.get("mensajeResultado"));
			
					procesaError(resultSw, resultMsg, "obtenerMensajeSwift");
					objeto[c] = String.valueOf(salida.get("textoLinea"));
				}
			}	
		
		}
		return objeto;
	}

	/**
	* Invoca al procedimiento almacenado FBPsrv_shw_liq_1a1.
	* <p>
	*
	* Registro de versiones:
	* <ul>
	*
	* <li>1.0 20/08/2013 14:05:52 BcxWSql (Bcx Solutions) : Versi�n inicial.</li>
	* </ul>
	* <p>
	*
	* @param parIn int Entrada para el SP FBPsrv_shw_liq_1a1.
	* @return String[] Salida del SP FBPsrv_shw_liq_1a1.
	* @throws ServicioDatosException En caso de ocurrir un error en el servicio de datos.
	* @throws TtffException En caso de ocurrir un error de negocio.
	* 
	* @since 1.0
	*/
	public String[] obtenerLiquidacion(int parIn)
	throws ServicioDatosException,TtffException {
		List documento = null;
		HashMap salida = null;	
		String[] objeto = null;
	
		HashMap mapa = new HashMap();
		mapa.put("CHAINED_SUPPORT", "true");

		mapa.put("fld_hdr_sys_num", new Integer(parIn));
		
		String contexto = TablaValores.getValor("JNDIConfig.parametros", "cluster", "param");

		if (logger.isDebugEnabled()) {
			logger.debug("obtenerLiquidacion");
			logger.debug("[ConectorServicioDePersistenciaDeDatos] - getConnection");
		}
	
		ConectorServicioDePersistenciaDeDatos conector = new
			ConectorServicioDePersistenciaDeDatos(contexto);
		documento = conector.consultar(sqlMapBaseDatosTtff, "obtenerLiquidacion", mapa);
		if (documento != null) {
			if (logger.isDebugEnabled()) {
				logger.info("obtenerLiquidacion::Cantidad de registros retornados: " 
						+ documento.size());
			}		
			if (documento.size() > 0) {
		
				objeto = new String[documento.size()];
				for (int c = 0; c < documento.size(); c++) {
					
					salida = (HashMap) documento.get(c);
			
					Integer resultSw = (Integer) salida.get("indicadorResultado");	
					
					String resultMsg = String.valueOf(salida.get("mensajeResultado"));
			
					procesaError(resultSw, resultMsg, "obtenerLiquidacion");		
					objeto[c] = String.valueOf(salida.get("textoLinea"));
				}
			}	
		
		}
		return objeto;
	}

	/**
	* Invoca al procedimiento almacenado FBPsrv_shh_ndp_1a1.
	* <p>
	*
	* Registro de versiones:
	* <ul>
	*
	* <li>1.0 20/08/2013 14:12:36 BcxWSql (Bcx Solutions) : Versi�n inicial.</li>
	* <li> 1.1 16/02/2017 Marcos Abraham Hernandez Bravo (ImageMaker IT) - Miguel Anza (BCI) : Se agrega y normaliza log.</li>
	* </ul>
	* <p>
	*
	* @param parIn ConsultaTransferenciaTO Entrada para el SP FBPsrv_shh_ndp_1a1.
	* @return Transferencia1a1TO[] Salida del SP FBPsrv_shh_ndp_1a1.
	* @throws ServicioDatosException En caso de ocurrir un error en el servicio de datos.
	* @throws TtffException En caso de ocurrir un error de negocio.
	* 
	* @since 1.0
	*/
	public Transferencia1a1TO[] obtenerTransferencias(ConsultaTransferenciaTO parIn) throws ServicioDatosException, TtffException {
		
		final String nombreMetodo = "obtenerTransferencias";
		
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "] [BCI_INI][" + StringUtil.contenidoDe(parIn) + "]");
		}
		
		List documento = null;
		HashMap salida = null;	
		Transferencia1a1TO[] objeto = null;
	
		HashMap parametros = new HashMap();
		parametros.put("CHAINED_SUPPORT", "true");
		parametros.put("fld_fbp_rut_ord", parIn.getRutEmpresa());
		parametros.put("fld_1a1_usr_typ", parIn.getTipoUsuario());
		parametros.put("fld_1a1_tip_acc", parIn.getServicioSolicitado());
		
		String contexto = TablaValores.getValor("JNDIConfig.parametros", "cluster", "param");

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[" + nombreMetodo + "] [parametros] [" + StringUtil.contenidoDe(parametros) + "]");
			getLogger().debug("[" + nombreMetodo + "] [contexto] [" + StringUtil.contenidoDe(contexto) + "]");
		}
		
		ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
		documento = conector.consultar(sqlMapBaseDatosTtff, "obtenerTransferencias", parametros);
		
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[" + nombreMetodo + "] [documento.size] [" + (documento != null ? documento.size() : 0) + "]");
			getLogger().debug("[" + nombreMetodo + "] [documento] [" + StringUtil.contenidoDe(documento) + "]");
		}
		
		if (documento != null && documento.size() > 0) {
			objeto = new Transferencia1a1TO[documento.size()];
			
			for (int c = 0; c < documento.size(); c++) {
				Transferencia1a1TO paso = new Transferencia1a1TO();
				salida = (HashMap) documento.get(c);
		
				Integer resultSw = (Integer) salida.get("indicadorResultado");
				String resultMsg = String.valueOf(salida.get("mensajeResultado"));

				procesaError(resultSw, resultMsg, "obtenerTransferencias");

                paso.setNombreBeneficiario(String.valueOf(salida.get("nombreBeneficiario")));
				paso.setPaisCuenta(String.valueOf(salida.get("paisCuenta")));
				paso.setNombrePais(String.valueOf(salida.get("nombrePais")));
                paso.setMonedaTransferencia(String.valueOf(salida.get("monedaTransferencia")));
				paso.setMontoTransferencia((BigDecimal)(salida.get("montoTransferencia")));
				paso.setFechaOperacion(String.valueOf(salida.get("fechaOperacion")));
                paso.setEstadoTransferencia(String.valueOf(salida.get("estadoTransferencia")));
				paso.setDescripcionEstado(String.valueOf(salida.get("descripcionEstado")));
				paso.setOrigenSolicitud(String.valueOf(salida.get("origenSolicitud")));
			
                paso.setCorrelativoInterno(Integer.parseInt(salida.get("correlativoInterno").toString()));
		
				objeto[c] = paso;
			}
		}
		
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "] [BCI_FINOK][" + StringUtil.contenidoDe(objeto) + "]");
		}
		return objeto;
	}

	/**
	* 
	* Invoca al procedimiento almacenado FBPsrv_shw_psg_rpp.
	* <p>
	*
	* Registro de versiones:
	* <ul>
	* <li>1.0 12/03/2019, Sergio Bustos B. (Everis) - Minheli Mejias (Ing. Soft. BCI): versi�n inicial</li>
	* </ul>
	* <p>
	* 
	* @param parIn ConsultaTransferenciaTO Entrada para el SP FBPsrv_shw_psg_rpp.
	* @return Transferencia1a1TO[] Salida del SP FBPsrv_shw_psg_rpp.
	* @throws TtffException En caso de ocurrir un error fatal.
	*
	* @since 1.4
	*/
	public Transferencia1a1TO[] obtenerTransferenciasPenRipple(ConsultaTransferenciaTO parIn) throws ServicioDatosException, TtffException {
		
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[obtenerTransferenciasPenRipple] [BCI_INI][" + StringUtil.contenidoDe(parIn) + "]");
		}
		
		List documento = null;
		HashMap salida = null;	
		Transferencia1a1TO[] objeto = null;
	
		HashMap parametros = new HashMap();
		parametros.put("CHAINED_SUPPORT", "true");
		parametros.put("fld_hdr_rut_ord", parIn.getRutEmpresa());
		
		String contexto = TablaValores.getValor("JNDIConfig.parametros", "cluster", "param");

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[obtenerTransferenciasPenRipple] [parametros] [" + StringUtil.contenidoDe(parametros) + "]");
			getLogger().debug("[obtenerTransferenciasPenRipple] [contexto] [" + StringUtil.contenidoDe(contexto) + "]");
		}
		
		ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
		documento = conector.consultar(sqlMapBaseDatosTtff, "obtenerTransferenciasRipple", parametros);
		
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[obtenerTransferenciasPenRipple] [documento.size] [" + (documento != null ? documento.size() : 0) + "]");
			getLogger().debug("[obtenerTransferenciasPenRipple] [documento] [" + StringUtil.contenidoDe(documento) + "]");
		}
		
		if (documento != null && documento.size() > 0) {
			objeto = new Transferencia1a1TO[documento.size()];
			
			for (int c = 0; c < documento.size(); c++) {
				Transferencia1a1TO paso = new Transferencia1a1TO();
				salida = (HashMap) documento.get(c);
		
				Integer resultSw = (Integer) salida.get("indicadorResultado");
				String resultMsg = String.valueOf(salida.get("mensajeResultado"));

				procesaError(resultSw, resultMsg, "obtenerTransferenciasRipple");

                paso.setNombreBeneficiario(String.valueOf(salida.get("nombreBeneficiario")));
				paso.setPaisCuenta(String.valueOf(salida.get("PaisBcoBeneficiario")));
				paso.setNombrePais(String.valueOf(salida.get("nombrePais")));
                paso.setMonedaTransferencia(String.valueOf(salida.get("monedaTransferencia")));
				paso.setMontoTransferencia((BigDecimal)(salida.get("montoTransferencia")));
				paso.setFechaOperacion(String.valueOf(salida.get("fechaOperacion")));
                paso.setEstadoTransferencia(String.valueOf(salida.get("estadoTransferencia")));
				paso.setDescripcionEstado(String.valueOf(salida.get("descripcionEstado")));
                paso.setCorrelativoInterno(Integer.parseInt(salida.get("correlativoFBP").toString()));
                paso.setSysTimeStamp(String.valueOf(salida.get("SysTimeStamp")));
                paso.setIndicadorResultado(Integer.parseInt(salida.get("indicadorResultado").toString())); 
                paso.setMensajeResultado(String.valueOf(salida.get("mensajeResultado")));
                paso.setNumeroOperacion(String.valueOf(salida.get("numeroOperacion")));
                paso.setCuentaOrigenFondos(String.valueOf(salida.get("CuentaOrigenFondos")));
                paso.setCuentaCorrienteBene(String.valueOf(salida.get("CuentaCorrienteBene")));
                paso.setCodigoMonedaITF(String.valueOf(salida.get("CodigoMonedaITF")));
                paso.setCodigoIdentificadorBene(String.valueOf(salida.get("CodigoIdentificadorBene")));
	
				objeto[c] = paso;
			}
		}
		
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[obtenerTransferenciasPenRipple] [BCI_FINOK][" + StringUtil.contenidoDe(objeto) + "]");
		}
		return objeto;
	}


	/**
	* Invoca al procedimiento almacenado FBPsrv_shd_ndp_1a1.
	* <p>
	*
	* Registro de versiones:
	* <ul>
	*
	* <li>1.0 20/08/2013 15:00:13 BcxWSql (Bcx Solutions) : Versi�n inicial.</li>
	* <li>1.1 1/10/2013 Benjamin Leon (Bcx Solutions) : Agregamos cinco propiedades al final.</li>
	* <li>1.2 05/11/2015 Oscar Nahuelpan (SEnTRA) - Heraldo Hernandez (Ing. Soft. BCI): Se obtienen nuevos 
    * atributos relacionados con el beneficiario.</li>
	* </ul>
	* <p>
	*
	* @param parIn ConsultaDetPagoTO Entrada para el SP FBPsrv_shd_ndp_1a1.
	* @return DetallePago1a1TO Salida del SP FBPsrv_shd_ndp_1a1.
	* @throws ServicioDatosException En caso de ocurrir un error en el servicio de datos.
	* @throws TtffException En caso de ocurrir un error de negocio.
	* 
	* @since 1.0
	*/
	public DetallePago1a1TO consultarDetallePago(ConsultaDetPagoTO parIn)
	throws ServicioDatosException,TtffException {
	    if(getLogger().isEnabledFor(Level.INFO)){
	        getLogger().info("[consultarDetallePago][BCI_INI] parIn["+parIn+"]");
	    }
		List documento = null;
		HashMap salida = null;	
		DetallePago1a1TO objeto = null;
	
		HashMap mapa = new HashMap();
		mapa.put("CHAINED_SUPPORT", "true");

		mapa.put("fld_hdr_sys_num", new Integer(parIn.getCorrelativoInterno()));
		mapa.put("fld_1a1_usr_typ", parIn.getTipoUsuario());
		mapa.put("fld_1a1_mod_ope", parIn.getModalidadOperacion());
		
		String contexto = TablaValores.getValor("JNDIConfig.parametros", "cluster", "param");

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[consultarDetallePago][ConectorServicioDePersistenciaDeDatos] - getConnection");
		}
	
		ConectorServicioDePersistenciaDeDatos conector = new
			ConectorServicioDePersistenciaDeDatos(contexto);
		documento = conector.consultar(sqlMapBaseDatosTtff, "consultarDetallePago", mapa);
		if (documento != null) {
			if (getLogger().isEnabledFor(Level.DEBUG)) {
			    getLogger().debug("[consultarDetallePago]::Cantidad de registros retornados: " 
						+ documento.size());
			}		
			if (documento.size() > 0) {
				
				objeto = new DetallePago1a1TO();
				
				salida = (HashMap) documento.get(0);	
			
				Integer resultSw = (Integer) salida.get("indicadorResultado");
				
				
				String resultMsg = String.valueOf(salida.get("mensajeResultado"));
				
				procesaError(resultSw, resultMsg, "consultarDetallePago");
				
				objeto.setRutEmpresa(String.valueOf(salida.get("rutEmpresa")));
				objeto.setMonedaCta(String.valueOf(salida.get("monedaCta")));
				objeto.setCodigoCuenta(String.valueOf(salida.get("codigoCuenta")));
				objeto.setCuentaDebito(String.valueOf(salida.get("cuentaDebito")));
				objeto.setMonedaNomina(String.valueOf(salida.get("monedaNomina")));
				objeto.setNumeroFolio(String.valueOf(salida.get("numeroFolio")));
				objeto.setFechaOperacion(String.valueOf(salida.get("fechaOperacion")));
				objeto.setNumeroOperacion(String.valueOf(salida.get("numeroOperacion")));
				objeto.setTipoOperacion(String.valueOf(salida.get("tipoOperacion")));
				objeto.setCodigoIdentificador(String.valueOf(salida.get("codigoIdentificador")));
				objeto.setPaisBco(String.valueOf(salida.get("paisBco")));
				objeto.setCuentaCorriente(String.valueOf(salida.get("cuentaCorriente")));
				objeto.setNombreBeneficiarioL1(String.valueOf(salida.get("nombreBeneficiarioL1")));
				objeto.setNombreBeneficiarioL2(String.valueOf(salida.get("nombreBeneficiarioL2")));
				objeto.setNombreBeneficiarioL3(String.valueOf(salida.get("nombreBeneficiarioL3")));
				objeto.setNombreBeneficiarioL4(String.valueOf(salida.get("nombreBeneficiarioL4")));
				objeto.setEmail1(String.valueOf(salida.get("email1")));
				objeto.setEmail2(String.valueOf(salida.get("email2")));
				objeto.setEmail3(String.valueOf(salida.get("email3")));
				objeto.setBicBanco(String.valueOf(salida.get("bicBanco")));
				objeto.setRutaPago(String.valueOf(salida.get("rutaPago")));
				objeto.setNomDirBancoL1(String.valueOf(salida.get("nomDirBancoL1")));
				objeto.setNomDirBancoL2(String.valueOf(salida.get("nomDirBancoL2")));
				objeto.setNomDirBancoL3(String.valueOf(salida.get("nomDirBancoL3")));
				objeto.setNomDirBancoL4(String.valueOf(salida.get("nomDirBancoL4")));
                objeto.setBicBancoIntermediario(String.valueOf(salida.get("bicBancoIntermediario")));
                objeto.setRutaPagoIntermediario(String.valueOf(salida.get("rutaPagoIntermediario")));
                objeto.setNomDirIntermediarioL1(String.valueOf(salida.get("nomDirIntermediarioL1")));
                objeto.setNomDirIntermediarioL2(String.valueOf(salida.get("nomDirIntermediarioL2")));
                objeto.setNomDirIntermediarioL3(String.valueOf(salida.get("nomDirIntermediarioL3")));
                objeto.setNomDirIntermediarioL4(String.valueOf(salida.get("nomDirIntermediarioL4")));
				objeto.setFechaPago(String.valueOf(salida.get("fechaPago")));
				objeto.setMontoPago((BigDecimal)(salida.get("montoPago")));
				objeto.setDetalleGastos(String.valueOf(salida.get("detalleGastos")));
				objeto.setInformacionRemesa(String.valueOf(salida.get("informacionRemesa")));
				objeto.setCodigoRechazo(String.valueOf(salida.get("codigoRechazo")));
				objeto.setDescripcionRechazo(String.valueOf(salida.get("descripcionRechazo")));
				objeto.setCodigoCambio(String.valueOf(salida.get("codigoCambio")));
				objeto.setForFurther(String.valueOf(salida.get("forFurther")));
				objeto.setCodigoValuta(String.valueOf(salida.get("codigoValuta")));
				objeto.setSystimestamp(String.valueOf(salida.get("systimestamp")));	
				objeto.setNombreMoneda(String.valueOf(salida.get("nombreMoneda")));
				objeto.setNombrePais(String.valueOf(salida.get("nombrePais")));
				objeto.setDescripcionDetalle(String.valueOf(salida.get("descripcionDetalle")));
				objeto.setNombreCuenta(String.valueOf(salida.get("nombreCuenta")));
				objeto.setNombreCodigo(String.valueOf(salida.get("nombreCodigo")));
				
			}
		}
		if(getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[consultarDetallePago][BCI_FINOK] objeto["+objeto+"]");
        }
		return objeto;
	}


	/**
	* Invoca al procedimiento almacenado FBPsrv_shd_ndp_rpp.
	* <p>
	* Registro de versiones:
	* <ul>
	*
	* <li>1.0 12/03/2019, Sergio Bustos B. (Everis) - Minheli Mejias (Ing. Soft. BCI): versi�n inicial</li>
	* </ul>
	* <p>
	* 
	* @param parIn
	* @return
	* @throws ServicioDatosException
	* @throws TtffException
	* 
	* @since 1.4
	*/
	public DetallePago1a1TO consultarDetallePagoRipple(ConsultaDetPagoTO parIn)	throws ServicioDatosException,TtffException {
	    if(getLogger().isEnabledFor(Level.INFO)){
	        getLogger().info("[consultarDetallePagoRipple][BCI_INI] parIn["+parIn+"]");
	    }
		List documento = null;
		HashMap salida = null;	
		DetallePago1a1TO objeto = null;
	
		HashMap mapa = new HashMap();
		mapa.put("CHAINED_SUPPORT", "true");

		mapa.put("fld_hdr_sys_num", new Integer(parIn.getCorrelativoInterno()));
		
		String contexto = TablaValores.getValor("JNDIConfig.parametros", "cluster", "param");

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[consultarDetallePago][ConectorServicioDePersistenciaDeDatos] - getConnection");
		}
	
		ConectorServicioDePersistenciaDeDatos conector = new
			ConectorServicioDePersistenciaDeDatos(contexto);
		documento = conector.consultar(sqlMapBaseDatosTtff, "consultarDetallePagoRipple", mapa);
		if (documento != null) {
			if (getLogger().isEnabledFor(Level.DEBUG)) {
			    getLogger().debug("[consultarDetallePago]::Cantidad de registros retornados: " + documento.size());
			}		
			if (documento.size() > 0) {
				
				objeto = new DetallePago1a1TO();
				
				salida = (HashMap) documento.get(0);	
			
				Integer resultSw = (Integer) salida.get("indicadorResultado");
				
				
				String resultMsg = String.valueOf(salida.get("mensajeResultado"));
				
				procesaError(resultSw, resultMsg, "consultarDetallePago");
				
				objeto.setMonedaCta(String.valueOf(salida.get("monedaCta")));
				objeto.setCodigoCuenta(String.valueOf(salida.get("codigoCuenta")));
				objeto.setCuentaDebito(String.valueOf(salida.get("cuentaDebito")));
				objeto.setMonedaNomina(String.valueOf(salida.get("monedaNomina")));
				objeto.setCodigoIdentificador(String.valueOf(salida.get("codigoIdentificador")));
				objeto.setPaisBco(String.valueOf(salida.get("paisBco")));
				objeto.setCuentaCorriente(String.valueOf(salida.get("cuentaCorriente")));
				objeto.setNombreBeneficiarioL1(String.valueOf(salida.get("nombreBeneficiarioL1")));
				objeto.setNombreBeneficiarioL2(String.valueOf(salida.get("nombreBeneficiarioL2")));
				objeto.setEmail1(String.valueOf(salida.get("email1")));
				objeto.setBicBanco(String.valueOf(salida.get("bicBanco")));
				objeto.setRutaPago(String.valueOf(salida.get("rutaPago")));
				objeto.setNomDirBancoL1(String.valueOf(salida.get("nomDirBancoL1")));
				objeto.setNomDirBancoL2(String.valueOf(salida.get("nomDirBancoL2")));
				objeto.setNomDirBancoL3(String.valueOf(salida.get("nomDirBancoL3")));
				objeto.setFechaPago(String.valueOf(salida.get("fechaPago")));
				objeto.setMontoPago((BigDecimal)(salida.get("montoPago")));
				objeto.setDetalleGastos(String.valueOf(salida.get("detalleGastos")));
				objeto.setInformacionRemesa(String.valueOf(salida.get("informacionRemesa")));
				objeto.setCodigoCambio(String.valueOf(salida.get("codigoCambio")));
				objeto.setNombreMoneda(String.valueOf(salida.get("nombreMoneda")));
				objeto.setNombrePais(String.valueOf(salida.get("nombrePais")));
				objeto.setDescripcionDetalle(String.valueOf(salida.get("descripcionDetalle")));
				objeto.setNombreCodigo(String.valueOf(salida.get("nombreCodigo")));
				
			}
		}
		if(getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[consultarDetallePago][BCI_FINOK] objeto["+objeto+"]");
        }
		return objeto;
	}


	/**
	* Invoca al procedimiento almacenado FBPsrv_shw_csb_1a1.
	* <p>
	*
	* Registro de versiones:
	* <ul>
	*
	* <li>1.0 20/08/2013 14:06:27 BcxWSql (Bcx Solutions) : Versi�n inicial.</li>
	* </ul>
	* <p>
	*
	* @param parIn ConsultaBeneficiariosTO Entrada para el SP FBPsrv_shw_csb_1a1.
	* @return Beneficiario1a1TO[] Salida del SP FBPsrv_shw_csb_1a1.
	* @throws ServicioDatosException En caso de ocurrir un error en el servicio de datos.
	* @throws TtffException En caso de ocurrir un error de negocio.
	* 
	* @since 1.0
	*/
	public Beneficiario1a1TO[] obtenerBeneficiariosAutorizados(ConsultaBeneficiariosTO parIn)
	throws ServicioDatosException,TtffException {
		List documento = null;
		HashMap salida = null;	
		Beneficiario1a1TO[] objeto = null;
	
		HashMap mapa = new HashMap();
		mapa.put("CHAINED_SUPPORT", "true");

		mapa.put("fld_cln_rut_emp", parIn.getRutEmpresa());
		mapa.put("fld_ben_mda_cta", parIn.getMndaCuenta());
		
		String contexto = TablaValores.getValor("JNDIConfig.parametros", "cluster", "param");

		if (logger.isDebugEnabled()) {
			logger.debug("obtenerBeneficiariosAutorizados");
			logger.debug("[ConectorServicioDePersistenciaDeDatos] - getConnection");
		}
	
		ConectorServicioDePersistenciaDeDatos conector = new
			ConectorServicioDePersistenciaDeDatos(contexto);
		documento = conector.consultar(sqlMapBaseDatosTtff, "obtenerBeneficiariosAutorizados", mapa);
		if (documento != null) {
			if (logger.isDebugEnabled()) {
				logger.info("obtenerBeneficiariosAutorizados::Cantidad de registros retornados: " 
						+ documento.size());
			}		
			if (documento.size() > 0) {
		
				objeto = new Beneficiario1a1TO[documento.size()];
				for (int c = 0; c < documento.size(); c++) {
					Beneficiario1a1TO paso = new Beneficiario1a1TO();
					salida = (HashMap) documento.get(c);
			
					Integer resultSw = (Integer) salida.get("indicadorResultado");	
					
					String resultMsg = String.valueOf(salida.get("mensajeResultado"));
			
					procesaError(resultSw, resultMsg, "obtenerBeneficiariosAutorizados");
			
                    paso.setIdentificacionBeneficiario(String.valueOf(salida.get("identificacionBeneficiario")));
					paso.setPaisBco(String.valueOf(salida.get("paisBco")));
					paso.setNombrePais(String.valueOf(salida.get("nombrePais")));
                    paso.setNombreBeneficiario(String.valueOf(salida.get("nombreBeneficiario")));
					paso.setCuentaCorriente(String.valueOf(salida.get("cuentaCorriente")));
					paso.setBicBanco(String.valueOf(salida.get("bicBanco")));
			
					objeto[c] = paso;
					
				}
			}	
		
		}
		return objeto;
	}

	/**
	* Invoca al procedimiento almacenado FBPsrv_shw_dbn_1a1.
	* <p>
	*
	* Registro de versiones:
	* <ul>
	*
	* <li>1.0 20/08/2013 15:09:54 BcxWSql (Bcx Solutions) : Versi�n inicial.</li>
	* <li>1.1 05/11/2015 Oscar Nahuelpan (SEnTRA) - Heraldo Hernandez (Ing. Soft. BCI): Se obtienen nuevos 
	* atributos relacionados con el beneficiario.</li>
	* <li> 1.2 16/02/2017 Marcos Abraham Hernandez Bravo (ImageMaker IT) - Miguel Anza (BCI) : Se agrega y normaliza log.</li>
	* </ul>
	* <p>
	*
	* @param parIn ConsultaDetBeneficiarioTO Entrada para el SP FBPsrv_shw_dbn_1a1.
	* @return DetalleBeneficiarioTO Salida del SP FBPsrv_shw_dbn_1a1.
	* @throws ServicioDatosException En caso de ocurrir un error en el servicio de datos.
	* @throws TtffException En caso de ocurrir un error de negocio.
	* 
	* @since 1.0
	*/
	public DetalleBeneficiarioTO obtenerDetalleDelBeneficiario(ConsultaDetBeneficiarioTO parIn) throws ServicioDatosException, TtffException {
	    
		final String nombreMetodo = "obtenerDetalleDelBeneficiario";
		
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "] [BCI_INI]");
			getLogger().info("[" + nombreMetodo + "] [Parametro][parIn][" + StringUtil.contenidoDe(parIn) + "]");
		}
		
		List documento = null;
		HashMap salida = null;	
		DetalleBeneficiarioTO objeto = null;
	
		HashMap parametros = new HashMap();
		parametros.put("CHAINED_SUPPORT", "true");

		parametros.put("fld_cln_rut_emp", parIn.getRutEmpresa());
		parametros.put("fld_ben_ide_ben", parIn.getIdentificadorBeneficiario());
		parametros.put("fld_ben_mda_cta", parIn.getMndaCuenta());
		parametros.put("fld_ben_ctr_ben", parIn.getPaisBco());
		
		ConsultaBeneficiariosTO consulta = new ConsultaBeneficiariosTO();
		consulta.setRutEmpresa(parIn.getRutEmpresa());
		consulta.setMndaCuenta(parIn.getMndaCuenta());
		
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[" + nombreMetodo + "] [consulta] [" + StringUtil.contenidoDe(consulta) + "]");
		}
		
		Beneficiario1a1TO[] beneficiario =  this. obtenerBeneficiariosAutorizados(consulta);
		
		String contexto = TablaValores.getValor("JNDIConfig.parametros", "cluster", "param");

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[" + nombreMetodo + "] [beneficiario] [" + StringUtil.contenidoDe(beneficiario) + "]");
			getLogger().debug("[" + nombreMetodo + "] [parametros] [" + StringUtil.contenidoDe(parametros) + "]");
			getLogger().debug("[" + nombreMetodo + "] [contexto] [" + StringUtil.contenidoDe(contexto) + "]");
		}
	
		ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
		documento = conector.consultar(sqlMapBaseDatosTtff, "obtenerDetalleDelBeneficiario", parametros);
		
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[" + nombreMetodo + "] [documento.size] [" + (documento != null ? documento.size() : 0) + "]");
			getLogger().debug("[" + nombreMetodo + "] [documento] [" + StringUtil.contenidoDe(documento) + "]");
		}
		
		if (documento != null && documento.size() > 0) {
			objeto = new DetalleBeneficiarioTO();
			
			salida = (HashMap) documento.get(0);
		
			Integer resultSw = (Integer) salida.get("indicadorResultado");
			String resultMsg = String.valueOf(salida.get("mensajeResultado"));
			
			procesaError(resultSw, resultMsg, "obtenerDetalleDelBeneficiario");
			
			objeto.setNombreBeneficiarioL1(String.valueOf(salida.get("nombreBeneficiarioL1")));
			objeto.setNombreBeneficiarioL2(String.valueOf(salida.get("nombreBeneficiarioL2")));
			objeto.setNombreBeneficiarioL3(String.valueOf(salida.get("nombreBeneficiarioL3")));
			objeto.setNombreBeneficiarioL4(String.valueOf(salida.get("nombreBeneficiarioL4")));
			objeto.setCuentaCorriente(String.valueOf(salida.get("cuentaCorriente")));
			objeto.setBancoBeneficiario(String.valueOf(salida.get("bancoBeneficiario")));
			objeto.setRutaPago(String.valueOf(salida.get("rutaPago")));
			objeto.setNomDirBancoL1(String.valueOf(salida.get("nomDirBancoL1")));
			objeto.setNomDirBancoL2(String.valueOf(salida.get("nomDirBancoL2")));
			objeto.setNomDirBancoL3(String.valueOf(salida.get("nomDirBancoL3")));
			objeto.setNomDirBancoL4(String.valueOf(salida.get("nomDirBancoL4")));
			objeto.setBicBanco(String.valueOf(salida.get("bicBanco")));
            objeto.setRutaPagoIntermediario(String.valueOf(salida.get("rutaPagoIntermediario")));
            objeto.setNomDirIntermediarioL1(String.valueOf(salida.get("nomDirIntermediarioL1")));
            objeto.setNomDirIntermediarioL2(String.valueOf(salida.get("nomDirIntermediarioL2")));
            objeto.setNomDirIntermediarioL3(String.valueOf(salida.get("nomDirIntermediarioL3")));
            objeto.setNomDirIntermediarioL4(String.valueOf(salida.get("nomDirIntermediarioL4")));
			objeto.setEmail1(String.valueOf(salida.get("email1")));
			objeto.setEmail2(String.valueOf(salida.get("email2")));
			objeto.setEmail3(String.valueOf(salida.get("email3")));
			objeto.setForFurther(String.valueOf(salida.get("forFurther")));	
			
		}
		
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "] [BCI_FINOK][" + StringUtil.contenidoDe(objeto) + "]");
		}
		return objeto;
	}

	/**
	* Invoca al procedimiento almacenado FBPsrv_shw_cyg_1a1.
	* <p>
	*
	* Registro de versiones:
	* <ul>
	*
	* <li>1.0 20/08/2013 14:06:45 BcxWSql (Bcx Solutions) : Versi�n inicial.</li>
	* <li>1.1 28/03/2018 Angelo Venegas (Everis)- Minheli Mejias (Ing. Soft. Bci) : Se modifica m�todo para obtener el tipo de tarifa.</li>
	* </ul>
	* <p>
	*
	* @param parIn int Entrada para el SP FBPsrv_shw_cyg_1a1.
	* @return ComisionTO Salida del SP FBPsrv_shw_cyg_1a1.
	* @throws ServicioDatosException En caso de ocurrir un error en el servicio de datos.
	* @throws TtffException En caso de ocurrir un error de negocio.
	* 
	* @since 1.0
	*/
	public ComisionTO obtenerComisiones(int parIn)
	throws ServicioDatosException,TtffException {
	   if (getLogger().isEnabledFor(Level.INFO)) {
            getLogger().info(
                    "[obtenerComisiones][" + parIn + "] [BCI_INI]");
        }
		List documento = null;
		HashMap salida = null;	
		ComisionTO objeto = null;
	
		HashMap mapa = new HashMap();
		mapa.put("CHAINED_SUPPORT", "true");

		mapa.put("fld_hdr_sys_num", new Integer( parIn));
		
		String contexto = TablaValores.getValor("JNDIConfig.parametros", "cluster", "param");

		if (logger.isDebugEnabled()) {
			logger.debug("obtenerComisiones");
			logger.debug("[ConectorServicioDePersistenciaDeDatos] - getConnection");
		}
	
		ConectorServicioDePersistenciaDeDatos conector = new
			ConectorServicioDePersistenciaDeDatos(contexto);
		documento = conector.consultar(sqlMapBaseDatosTtff, "obtenerComisiones", mapa);
		if (documento != null) {
			if (logger.isDebugEnabled()) {
				logger.debug("obtenerComisiones::Cantidad de registros retornados: " 
						+ documento.size());
			}		
			if (documento.size() > 0) {
				
				objeto = new ComisionTO();
				
				salida = (HashMap) documento.get(0);	
			
				Integer resultSw = (Integer) salida.get("indicadorResultado");
				
				
				String resultMsg = String.valueOf(salida.get("mensajeResultado"));
				
				procesaError(resultSw, resultMsg, "obtenerComisiones");
				
				objeto.setComisionEnvio((BigDecimal)(salida.get("comisionEnvio")));
				objeto.setMontoGatos((BigDecimal)(salida.get("montoGatos")));
				objeto.setMontoGatos1((BigDecimal)(salida.get("montoGatos1")));
                objeto.setTipoComision(Integer.parseInt(salida.get("tipoComision").toString()));	
				objeto.setDescripcionTipo(salida.get("descripcionTipo").toString());
				objeto.setTipoTarifa(salida.get("tipoTarifa").toString());				
			}
		}
		if (getLogger().isEnabledFor(Level.INFO)) {
            getLogger().info(
                    "[obtenerComisiones][" + objeto.getTipoTarifa() + "] [BCI_FINOK]");
        }
		return objeto;
	}
	
	/**
     * <p>M�todo getLogger.</p>
     * 
     * Registro de versiones:
     * <ul>
     * <li>1.0 05/11/2015 Oscar Nahuelpan (SEnTRA)- Heraldo Hernandez (Ing. Soft. BCI): Versi�n inicial.</li>
     * </ul>
     * @return log.
     * 
     * @since 1.1
     */
    public Logger getLogger() {
        if (logger == null) {
            logger = Logger.getLogger(this.getClass());
        }
        return logger;
    }
}