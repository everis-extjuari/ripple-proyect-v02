package wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.sessionfacade;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import cl.bci.middleware.aplicacion.persistencia.ejb.ServicioDatosException;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.bo.ApoderadoBO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.bo.BeneficiarioBO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.bo.CuentaFrecuenteBO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.bo.TransferenciaBO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.bo.TransferenciaOtrosBancosBO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.business.TransferenciaDeFondosAutenticacion;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.business.TransferenciaDeFondosBusiness;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.business.TransferenciaOtrosBancosBusiness;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.dao.TransferenciaDeFondosDAO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.dao.TransferenciaDeFondosDAOImpl;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.dao.jdbc.TransferenciasUnoAUnoDAO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.exception.TransferenciaDeFondosException;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.exception.TransferenciaDeFondosPersistenciaException;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.jms.TransferenciaMasivaJMSSender;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.parametros.TransferenciaDeFondosParametros;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.ApoderadoTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.BancoTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.Beneficiario1a1TO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.BeneficiarioTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.CambiaEstadoTttfMonexTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.CiudadTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.CodigoCambioTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.ComisionTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.ConsultaBeneficiariosTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.ConsultaCodigoCambioTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.ConsultaDetBeneficiarioTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.ConsultaDetPagoTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.ConsultaMonedaTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.ConsultaTransferenciaTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.CuentaComisionTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.CuentaFrecuenteTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.CuentaTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.DatosAutorizacionTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.DatosBeneficiarioTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.DatosTransferMonexTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.DetalleBeneficiarioTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.DetallePago1a1TO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.DetallePagoTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.GastosBancoExteriorTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.Moneda1a1TO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.MonedaTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.NominaDetalleTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.NominaPosicionCambioTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.NominaTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.PaisTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.RespuestaActualizaTtffTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.RespuestaHorarioTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.RutaPagoTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.Transferencia1a1TO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.TransferenciaMonexTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.TransferenciaTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.ValutaTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.dao.TransferenciasRippleDAO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.BeneficiarioRippleTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.ConsultaBeneficiariosRippleTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.ConsultaDetalleBeneficiarioRippleTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.DatosxCurrentTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.DetalleBeneficiarioRippleTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.FirmarEliminarTransferenciaRipple;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.MonedasPermitidasTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.NodosRippleTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.ObtenerComisionTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.RegistrarTransferenciaTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.RespuestaRegistroTransferenciaTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.RespuestaSpTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.util.HashUtil;
import wcorp.serv.bciexpress.AutorizaPagoCtaCte;
import wcorp.serv.bciexpress.BCIExpress;
import wcorp.serv.bciexpress.BCIExpressException;
import wcorp.serv.bciexpress.BCIExpressHome;
import wcorp.serv.bciexpress.ExtraeTipoProducto;
import wcorp.serv.bciexpress.ResultAutorizaPagoCuentaCorriente;
import wcorp.serv.bciexpress.ResultExtraeTipoProducto;
import wcorp.serv.bciexpress.ResultValidarUsuarioporTipoFirma;
import wcorp.serv.bciexpress.dao.BCIExpressDAO;
import wcorp.serv.comex.ServiciosComex;
import wcorp.serv.comex.ServiciosComexHome;
import wcorp.serv.misc.MiscelaneosException;
import wcorp.serv.misc.ServiciosMiscelaneos;
import wcorp.serv.misc.ServiciosMiscelaneosHome;
import wcorp.serv.pagos.PagosException;
import wcorp.serv.pagos.ServiciosPagos;
import wcorp.serv.pagos.ServiciosPagosHome;
import wcorp.serv.seguridad.SeguridadException;
import wcorp.serv.seguridad.ServiciosSeguridad;
import wcorp.serv.seguridad.ServiciosSeguridadHome;
import wcorp.util.EnhancedServiceLocator;
import wcorp.util.EnhancedServiceLocatorException;
import wcorp.util.ErroresUtil;
import wcorp.util.FechasUtil;
import wcorp.util.Feriados;
import wcorp.util.GeneralException;
import wcorp.util.StringUtil;
import wcorp.util.TablaValores;
import wcorp.util.com.TuxedoException;


/**
 * <b>EJB DE TRANSFERENCIAS FONDO MONEX</b>
 * <p>
 * Servicio que actua bajo el patr�n Session Facade, conectando los diferentes servicios del banco.
 * </p>
 *
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 30/05/2008, Pei Kang Fu (Neoris Chile), Edgardo Olivares(Neoris Chile), Juan Ramirez(Neoris Chile) -
 * versi�n inicial
 *
 * <li>1.1 15/05/2009, Pei Kang Fu (Neoris Chile), Cristi�n Briones(Neoris Chile) - se agregan los if
 * corespondientes antes de cada sentencia de logger de acuerdo a la norma del banco, se agregan m�todos
 * correspondientes al proyecto transferencia monex masivo que son los siguientes:
 * <ul>
 * <li><b>{@link #autorizarPagoOtrosBancos(DatosTransferMonexTO)}</b>
 * <li><b>{@link #advertirFondoInsuficiente(DatosTransferMonexTO, TransferenciaDeFondosException)}</b>
 * <li><b>{@link #determinarApoderado(DatosTransferMonexTO, String)}</b>
 * <li><b>{@link #ejecutarPagoNomina(DatosTransferMonexTO)}</b>
 * <li><b>{@link #enviarMensajeJMSNomina(byte[])}</b>
 * <li><b>{@link #extraeIndicadorSifp(String, String, long, char, int, long)}</b>
 * <li><b>{@link #obtenerDetalleNomina(Integer, String, String)}</b>
 * <li><b>{@link #obtenerDetallePagoNomina(int, int)}</b>
 * <li><b>{@link #obtenerDetallePosicionCambioNomina(int, int, String)}</b>
 * <li><b>{@link #obtenerListadoNominasPorFecha(long, char, Calendar, Calendar)} </b>
 * <li><b>{@link #obtenerListadoNominasPorRol(long, char, String)} </b>
 * <li><b>{@link #obtenerNominaPorEtiqueta(String, long, char)} </b>
 * <li><b>{@link #firmarNomina(DatosTransferMonexTO)} </b>
 * <li><b>{@link #rechazarNomina(DatosTransferMonexTO)} </b>
 * <li><b>{@link #autorizarNomina(DatosTransferMonexTO)} </b>
 * <li><b>{@link #validaApoderadoSifp(String, String, String, long, char, long)} </b>
 * <li><b>{@link #crearBeneficiario(BeneficiarioTO)} </b>
 * <li><b>{@link #actualizarBeneficiario(BeneficiarioTO)} </b>
 * <li><b>{@link #listarBeneficiarios(long, char)} </b>
 * <li><b>{@link #eliminarBeneficiario(DatosBeneficiarioTO)} </b>
 * <li><b>{@link #buscarBeneficiario(DatosBeneficiarioTO)} </b>
 * <li><b>{@link #listarPaises()} </b>
 * <li><b>{@link #listarCiudades(String)} </b>
 * <li><b>{@link #listarBancos(int, String)} </b>
 * <li><b>{@link #listarRutasDePago(String)} </b>
 * <li><b>{@link #buscarBancoPorSwift(String)} </b>
 * <li><b>{@link #listarMonedas()} </b>
 * <li><b>{@link #obtenerDetallePosicionCambioNomina(int, int, String)} </b>
 * </ul>
 * <li>1.2 27/10/2010, Jos� Verdugo B. (TINet): Modificaci�n para permitir continuar con el flujo normal cuando
 *         ocurre una excepci�n del tipo "FONDO_INSUFICIENTE" en los siguientes m�todos:
 *         <ul>
 *          <li>{@link #autorizarPagoOtrosBancos(DatosTransferMonexTO)}</li>
 *          <li>{@link #determinarApoderado(DatosTransferMonexTO, String)}</li>
 *         </ul>
 *         </li>
 *
 * <li>1.3 21/10/2010, Gustavo Espinoza Santiba�ez (SEnTRA): Se modifican los m�todos
 * obtenerListadoNominasPorFecha y obtenerNominaPorEtiqueta ya que no permitian consultar el rol
 * del usuario de forma parametrica.
 *
 *  <li>1.4 15/04/2011, Angelo Vel�squez Vel�squez (SEnTRA): Se agrega m�todo {@link # consultaLiquidacion()}, el cual cumple con la funci�n de obtener
 * los datos de las liquidaciones. Se agrega m�todo {@link #obtieneMensajeSwift()}, el cual cumple con la funci�n de obtener los datos del
 * mensaje Swift asociado a un pago realizado mediante Nominas de Pago (Moneda Extranjera).
 * <li>1.5 25/08/2014, Christian Saravia Jorquera (SEnTRA): se agrega m�todo 
 * {@link #autorizaPagoCuentaCorrienteMonto()}, el cual cumple la funci�n *  de validar el monto m�ximo de las 
 * transferencias Monex realizando el cambio de moneda. Adem�s se agregan las constantes {@link #MONEDA_PESOS}, 
 * {@link #FIRMADA}, {@link #PENDIENTE_FALTA_FIRMAS}, {@link #SUPERA_MONTO_MAXIMO}, {@link #PROBLEMAS_DE_CONEXION},
 * {@link #APODERADO_YA_FIRMO}, {@link #NO_ES_APODERADO} y {@link #LARGO_RUT}.
 * <li>1.6 13/11/2015 Oscar Nahuelpan (SEnTRA) - Heraldo Hernandez (Ing. Soft. BCI): Se crea metodo {@link #listarPaisesResidencia()}.</li>
 * <li>1.7 30/09/2015, Eric Mart�nez (TINet) - Oliver Hidalgo (Ing. Soft. BCI): Se cambia a p�blico el tipo de 
 * modificador del m�todo {@link #autorizaPagoCuentaCorrienteMonto(DatosTransferMonexTO)}.<li>
 *<li>1.8 16/02/2017 Marcos Abraham Hernandez Bravo (ImageMaker IT) - Miguel Anza (BCI) : Se agrega m�todo {@link #getLogger()}, y log a los siguientes m�todos:
 * 		- {@link #obtenerCuentaDeCargo(String)}
 * 		- {@link #consultarHorarioAprobacion(int)}
 * 		- {@link #obtenerTransferencias(long, String, String, String)}
 * 		- {@link #obtenerFechaValuta(String)}
 * 		- {@link #obtenerDetalleDelBeneficiario(ConsultaDetBeneficiarioTO)}
 * </li>
 * <li>1.9 25/02/2019 25/02/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft. BCI): Se agregan m�todos correspondientes
 * al proyecto Transferencias Internacionales:
 * 			- {@link #obtenerMonedasRipple(String)}
 *  		- {@link #obtenerNodosRippleDisp()} 
 * </li>
 * <li>2.0  28/02/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft. BCI): Se agregan m�todos correspondientes
 * al proyecto Transferencias Internacionales:
 *          - {@link #obtenerBeneficiariosAutorizadosRipple(ConsultaBeneficiariosRippleTO)}
 *          - {@link #obtenerDetalleBeneficiarioRipple(ConsultaDetalleBeneficiarioRippleTO)}  
 * </li>
 * <li>2.1  05/03/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft. BCI): Se agregan m�todos correspondientes
 * al proyecto Transferencias Internacionales:
 *          - {@link #obtenerComisionesRipple(ObtenerComisionTO)}
 *          - {@link #generarHashOperacion(RegistrarTransferenciaTO}
 *          - {@link #obtenerTransferenciasPenRipple(ConsultaTransferenciaTO)}  
 *          - {@link #consultarLimiteHorarioIngreso}
 *          - {@link #firmarOEliminarTransferenciaRipple}     
 *          - {@link #debitarCuentaCorrienteMX}
 *          - {@link #consultarDatosxCurrent}
 *          - {@link #registrarResulatadoxCurrent}
 * </li>
 * </ul>
 * </p>
 * <B>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</B>
 */
public class TransferenciaDeFondosFacadeBean implements TransferenciaDeFondosBusiness,
        TransferenciaDeFondosAutenticacion, TransferenciaOtrosBancosBusiness, SessionBean {

    /**
     * serialVersion.
     */
    private static final long serialVersionUID = 1L;

    /**
     * producto bci express.
     */
    private static final String TIPO_PRODUCTO_EXPRESS = "EXP";

    /**
     * codigo que representa no autorizado del status de firma poder.
     */
    private static final String NO_AUTORIZADO = "0";

    /**
     * valor que indica si es uan autotransaccion.
     */
    private static final char AUTOTRANSACCION = '2';

    /**
     * fecha de expiraci�n de la transferencia.
     */
    private static final String FECHAEXPIRACION = "01/01/1900";

    /**
     * variable estatica que indica apoderado virtual.
     */
    private static final String INDICADOR_APODERADO_VIRTUAL = "1.0";

    /**
     * formato de fecha que se utiliza en la tabla AUT.
     */
    private static final String FORMATO_FECHA_AUT = "yyyy/MM/dd";

    /**
     * constante que indica el apoderado es real.
     */
    private static final String INDICADOR_APODERADO_REAL = "S";

	/**
	 * Para serializar.
     */
	static final String  errorServicioDatos = "TTFF-027";

	/**
     * logger de la aplicacion TransferenciaDeFondosMonex.
     */
    private static Logger logger = (Logger) Logger.getLogger(TransferenciaDeFondosFacadeBean.class);

    /** JNDI del servicio pagos. */
    private static final String JNDI_SERV_PAGOS = "wcorp.serv.pagos.ServiciosPagos";

    /** JNDI de los servicios de BCI Express. */
    public static final String JNDI_BCI_EXPRESS = "wcorp.serv.bciexpress.BCIExpress";

    /** JNDI de los servicios de Seguridad. */
    public static final String JNDI_SEGURIDAD = "wcorp.serv.seguridad.ServiciosSeguridad";

    /** JNDI de los servicios miscelaneo. * */
    public static final String JNDI_MISCELANEO = "wcorp.serv.misc.ServiciosMiscelaneos";

    /** JNDI de los servicios comex. * */
    public static final String JNDI_COMEX = "wcorp.serv.comex.ServiciosComex";

    /**
     * Tipo moneda correspondiente a pesos chilenos.
     */
    private static final String MONEDA_PESOS = "CLP"; 
    
    /**
     * Indica que la transferencia se firm� correctamente.
     */
    private static final int FIRMADA = 1;
    
    /**
     * Indica que la transferencia est� pendiente de firmas.
     */
    private static final int PENDIENTE_FALTA_FIRMAS = 0;
    
    /**
     * Indica que la transferencia supera el monto m�ximo permitido.
     */
    private static final int SUPERA_MONTO_MAXIMO = -1;
    /**
     * Indica que hubieron problemas de conexi�n.
     */
    private static final int PROBLEMAS_DE_CONEXION = 102;
    
    /**
     * Indica que el apoderado ya firm� la transferencia.
     */
    private static final int APODERADO_YA_FIRMO = 101;
    
    /**
     * Indica que el usuario no es un apoderado v�lido.
     */
    private static final int NO_ES_APODERADO = 100;
    
    /**
	 * Tabla de par�metros de transfer monex.
	 */
	private static final String TABLA_PARAMETROS_TRANSFERRIPPLE = "pyme/transferRipple.parametros";
    
    /**
     * Largo para un rut.
     */
    private static final int LARGO_RUT = 8;
    
    /** Instancia del servicio de BCI Express. */
    private BCIExpressHome servicioBCIExpressHome = null;

    /** Instancia del servicio seguridad. */
    private ServiciosSeguridadHome serviciosSeguridadHome = null;

    /** Instancia del servicio de pagos. */
    private ServiciosPagosHome servicioPagosHome = null;

    /** Instancia del servicio miscelaneos. */
    private ServiciosMiscelaneosHome srvMiscHome = null;

    /** Instancia del servicio comex. * */
    private ServiciosComexHome serviciosComexHome = null;

    public void ejbActivate() throws EJBException, RemoteException {
    }

    public void ejbPassivate() throws EJBException, RemoteException {
    }

    public void ejbRemove() throws EJBException, RemoteException {
    }

    public void setSessionContext(SessionContext newContext) throws EJBException {
    }

    public void ejbCreate() throws CreateException {
        try {
            servicioBCIExpressHome = (BCIExpressHome) EnhancedServiceLocator.getInstance().getHome(
                    JNDI_BCI_EXPRESS, BCIExpressHome.class);
            serviciosSeguridadHome = (ServiciosSeguridadHome) EnhancedServiceLocator.getInstance().getHome(
                    JNDI_SEGURIDAD, ServiciosSeguridadHome.class);
            servicioPagosHome = (ServiciosPagosHome) EnhancedServiceLocator.getInstance().getHome(JNDI_SERV_PAGOS,
                    ServiciosPagosHome.class);
            srvMiscHome = (ServiciosMiscelaneosHome) EnhancedServiceLocator.getInstance().getHome(JNDI_MISCELANEO,
                    ServiciosMiscelaneosHome.class);
            serviciosComexHome = (ServiciosComexHome) EnhancedServiceLocator.getInstance().getHome(JNDI_COMEX,
                    ServiciosComexHome.class);

        }
        catch (EnhancedServiceLocatorException e) {
            throw new CreateException();
        }
    }

    /**
     * M�todo responsable de obtener las cuentas de origen del cliente de empresa con el monto m�ximo autorizado
     * para transferencia, monto transacci�n diaria , monto m�ximo disponible.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 03/06/2008, Pei Kang Fu.(Neoris Chile) - versi�n inicial.
     * <li>1.1 15/05/2009, Pei Kang Fu.(Neoris Chile) - se agregan los if corespondientes antes de cada sentencia
     * de logger de acuerdo a la norma del banco.
     * </ul>
     * </p>
     *
     * @param rutCliente el rut del cliente.
     * @param convenio convenio que posee el cliente.
     * @return CuentaTO[] arreglo de cuentas.
     * @throws TransferenciaDeFondosException
     * @since 1.0
     */
    public CuentaTO[] obtenerCuentasOrigen(long rutCliente, String convenio) throws TransferenciaDeFondosException {
        if (logger.isEnabledFor(Level.DEBUG)) {
            logger.debug("[obtenerCuentasOrigen] - rutCliente " + rutCliente + " convenio: " + convenio);
        }
        TransferenciaBO transferenciaBO = new TransferenciaBO();

        return transferenciaBO.obtenerCuentasOrigen(rutCliente, convenio);
    }

    /**
     * M�todo responsable de obtener detalle de una cuenta de moneda extranjera.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 03/06/2008, Pei Kang Fu.(Neoris Chile) - versi�n inicial.
     * <li>1.1 15/05/2009, Pei Kang Fu.(Neoris Chile) - se agregan los if corespondientes antes de cada sentencia
     * de logger de acuerdo a la norma del banco.
     * </ul>
     * </p>
     *
     * @param numeroCuenta numero de la cuenta.
     * @return CuentaTO objeto que representa una cuenta.
     * @throws TransferenciaDeFondosException.
     * @since 1.0
     */
    public CuentaTO obtenerDetalleCuenta(String numeroCuenta) throws TransferenciaDeFondosException {
        if (logger.isEnabledFor(Level.DEBUG)) {
            logger.debug("[obtenerDetalleCuenta] - numeroCuenta " + numeroCuenta);
        }
        TransferenciaBO transferenciaBO = new TransferenciaBO();
        CuentaTO detalleCuenta = new CuentaTO();
        if (numeroCuenta != null) {
            detalleCuenta = transferenciaBO.obtenerDetalleCuenta(numeroCuenta);
            return detalleCuenta;
        }
        else {
            return null;
        }
    }

    /**
     * M�todo encargado de registrar la transferencia hechas por el cliente.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 10/06/2008, Pei Kang Fu.(Neoris Chile) - versi�n inicial.
     * <li>1.1 03/02/2009, Pei Kang Fu.(Neoris Chile) - se agregan los if corespondientes antes de cada sentencia
     * de logger de acuerdo a la norma del banco.
     * </ul>
     * </p>
     *
     * @param transferenciaTO objeto que representa una transferencia monex.
     * @throws TransferenciaDeFondosException
     * @since 1.0
     */
    public String registrarTransferencia(TransferenciaTO transferenciaTO) throws TransferenciaDeFondosException {
        if (logger.isEnabledFor(Level.DEBUG)) {
            logger.debug("[registrarTransferencia] - transferenciaTO " + transferenciaTO.toString());
        }
        TransferenciaBO transferenciaBO = new TransferenciaBO();
        return transferenciaBO.registrarTransferencia(transferenciaTO);
    }

    /**
     * M�todo encargado de eliminar una transferencia de moneda extranjera.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 03/06/2008, Pei Kang Fu.(Neoris Chile) - versi�n inicial.
     * <li>1.1 03/02/2009, Pei Kang Fu.(Neoris Chile) - se agregan los if correspondientes antes de cada sentencia
     * de logger de acuerdo a la norma del banco.
     * </ul>
     * </p>
     *
     * @param rutCliente el rut del cliente.
     * @param convenio el convenio que posee el cliente.
     * @param idTransferencia identificador de la transferencia.
     * @param idLineaDetalle identificador de la linea detalle de la transferencia.
     * @throws TransferenciaDeFondosException
     * @since 1.0
     */
    public void eliminarTransferencia(long rutCliente, String convenio, String idTransferencia, int idLineaDetalle)
            throws TransferenciaDeFondosException {
        if (logger.isEnabledFor(Level.DEBUG)) {
            logger.debug("[eliminarTransferencia] - rutCliente " + rutCliente + " convenio: " + convenio
                    + "idTransferencia: " + idTransferencia + " idLinedaDetalle: " + idLineaDetalle);
        }
        TransferenciaBO transferenciaBO = new TransferenciaBO();
        if (idTransferencia != null) {
            transferenciaBO.eliminarTransferencia(rutCliente, convenio, idTransferencia, idLineaDetalle);
        }
    }

    /**
     * M�todo encargado de eliminar una transferencia de moneda extranjera, esto incluye tanto el encabezado como
     * los detalles.
     *
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 03/06/2008, Pei Kang Fu.(Neoris Chile) - versi�n inicial.
     * <li>1.1 03/02/2009, Pei Kang Fu.(Neoris Chile) - se agregan los if corespondientes antes de cada sentencia
     * de logger de acuerdo a la norma del banco.
     * </ul>
     * </p>
     *
     * @param rutCliente el rut del cliente.
     * @param convenio el convenio que posee el cliente.
     * @param idTransferencia identificador de la transferencia.
     * @throws TransferenciaDeFondosException
     * @since 1.0
     */
    public void eliminarTransferenciaCompleta(long rutCliente, String convenio, String idTransferencia)
            throws TransferenciaDeFondosException {
        if (logger.isEnabledFor(Level.DEBUG)) {
            logger.debug("[eliminarTransferenciaCompleta] - rutCliente " + rutCliente + " convenio: " + convenio
                    + "idTransferencia: " + idTransferencia);
        }
        TransferenciaBO transferenciaBO = new TransferenciaBO();
        if (idTransferencia != null) {
            transferenciaBO.eliminarTransferenciaCompleta(rutCliente, convenio, idTransferencia);
        }
    }

    /**
     * M�todo responsable de obtener todas las transferencias hechas por el cliente.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 11/06/2008, Pei Kang Fu.(Neoris Chile) - versi�n inicial.
     * <li>1.1 03/02/2009, Pei Kang Fu.(Neoris Chile) - se agregan los if corespondientes antes de cada sentencia
     * de logger de acuerdo a la norma del banco.
     * <li> 1.2 16/02/2017 Marcos Abraham Hernandez Bravo (ImageMaker IT) - Miguel Anza (BCI) : Se agrega y normaliza log.</li>
     * </ul>
     * </p>
     *
     * @param rutCliente el rut del cliente.
     * @param convenio el convenio que posee el cliente.
     * @param idTransferencia identificador de la transferencia.
     * @param tipoTransferencia tipo transferencia.
     * @throws TransferenciaDeFondosException excepci�n de la aplicaci�n.
     * @return TransferenciaTO[] arreglo de transferencias.
     * @since 1.0
     */
    public TransferenciaTO[] obtenerTransferencias(long rutCliente, String convenio, String idTransferencia, String tipoTransferencia) throws TransferenciaDeFondosException {
    	
    	final String nombreMetodo = "obtenerTransferencias";

    	if (getLogger().isEnabledFor(Level.INFO)) {
    		getLogger().info("[" + nombreMetodo + "] [BCI_INI]");
    		getLogger().info("[" + nombreMetodo + "] [Parametro][rutCliente][" + StringUtil.contenidoDe(new Long(rutCliente)) + "]");
    		getLogger().info("[" + nombreMetodo + "] [Parametro][convenio][" + StringUtil.contenidoDe(convenio) + "]");
    		getLogger().info("[" + nombreMetodo + "] [Parametro][idTransferencia][" + StringUtil.contenidoDe(idTransferencia) + "]");
    		getLogger().info("[" + nombreMetodo + "] [Parametro][tipoTransferencia][" + StringUtil.contenidoDe(tipoTransferencia) + "]");
    	}
    	
        TransferenciaBO transferenciaBO = new TransferenciaBO();
        TransferenciaTO[] retorno = transferenciaBO.obtenerTransferencias(rutCliente, convenio, idTransferencia, tipoTransferencia);
        
        if (getLogger().isEnabledFor(Level.INFO)) {
    		getLogger().info("[" + nombreMetodo + "] [BCI_FINOK][" + StringUtil.contenidoDe(retorno) + "]");
    	}
        return retorno;
    }

    /**
     * M�todo encargado de obtener las transferencias asociadas al rut Empresa.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 04/07/2008, Juan Ramirez (Neoris Chile) - versi�n inicial
     * <li>1.1 03/02/2009, Pei Kang Fu.(Neoris Chile) - se agregan los if corespondientes antes de cada sentencia
     * de logger de acuerdo a la norma del banco.
     * </ul>
     * </p>
     *
     * @param rutEmpresa Rut Empresa a consultar.
     * @param numConvenio el numero de convenio.
     * @param estado el estado de la transferencia.
     * @return arreglo de transferencias.
     * @throws TransferenciaDeFondosException
     * @since 1.0
     */
    public TransferenciaTO[] listarTransferencias(long rutEmpresa, String numConvenio, String estado)
            throws TransferenciaDeFondosException {
        if (logger.isEnabledFor(Level.DEBUG)) {
            logger.debug("[listarTransferencias]Rut Empresa: " + rutEmpresa + " N�mero Convenio: " + numConvenio
                    + "estado: " + estado);
        }
        TransferenciaBO transferenciaBO = new TransferenciaBO();
        return transferenciaBO.listarTransferencias(rutEmpresa, numConvenio, estado);
    }

    /**
     * M�todo encargado de obtener el detalle de la Transferencia asociada a un Rut Empresa, n�mero de convenio,
     * n�mero de Transferencia y tipo Transferencia.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 07/07/2008, Juan Ramirez (Neoris Chile) - versi�n inicial
     * <li>1.1 03/02/2009, Pei Kang Fu.(Neoris Chile) - se agregan los if corespondientes antes de cada sentencia
     * de logger de acuerdo a la norma del banco.
     * </ul>
     * </p>
     *
     * @param rutEmpresa Rut Empresa a consultar.
     * @param numConvenio N�mero convenio.
     * @param numTransferencia N�mero Transferencia.
     * @param tipoTransferencia Tipo Transferencia.
     * @return Arreglo de tipo TransferenciaTO con el detalle de la Transferencia.
     * @throws TransferenciaDeFondosException
     * @since 1.0
     */
    public TransferenciaTO[] detalleTransferencias(long rutEmpresa, String numConvenio, String numTransferencia,
            String tipoTransferencia) throws TransferenciaDeFondosException {
        if (logger.isEnabledFor(Level.DEBUG)) {
            logger.debug("[detalleTransferencias]Rut Empresa: " + rutEmpresa + " numConvenio: " + numConvenio
                    + " numTransferencia: " + numTransferencia + " tipoTransferencia: " + tipoTransferencia);
        }
        TransferenciaBO transferenciaBO = new TransferenciaBO();
        return transferenciaBO.detalleTransferencias(rutEmpresa, numConvenio, numTransferencia, tipoTransferencia);
    }

    /**
     * M�todo encargado de obtener todos los apoderados asociados al n�mero de Transferencia y tipo Transferencia.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 08/07/2008, Juan Ramirez (Neoris Chile) - versi�n inicial
     * <li>1.1 03/02/2009, Pei Kang Fu.(Neoris Chile) - se agregan los if corespondientes antes de cada sentencia
     * de logger de acuerdo a la norma del banco.
     * </ul>
     * </p>
     *
     * @param numTransferencia N�mero Transferencia.
     * @param tipoTransferencia Tipo Transferencia.
     * @return Arreglo de tipo ApoderadoTO con el apoderados asociados a la Transferencia.
     * @throws TransferenciaDeFondosException
     * @since 1.0
     */
    public ApoderadoTO[] obtenerApoderados(String numTransferencia, String tipoTransferencia)
            throws TransferenciaDeFondosException {
        if (logger.isEnabledFor(Level.DEBUG)) {
            logger.debug("[obtenerApoderados] numTransferencia: " + numTransferencia + " tipoTransferencia: "
                    + tipoTransferencia);
        }
        ApoderadoBO apoderadoBO = new ApoderadoBO();
        return apoderadoBO.obtieneApoderados(numTransferencia, tipoTransferencia);
    }

    /**
     * Metodo encargado de la autenticacion del token.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 28/07/2008, Pei Kang Fu (Neoris Chile) - versi�n inicial
     * <li>1.1 03/02/2009, Pei Kang Fu.(Neoris Chile) - se agregan los if corespondientes antes de cada sentencia
     * de logger de acuerdo a la norma del banco.
     * </ul>
     * </p>
     *
     * @param datosAutorizacionTO to que encapsula datos para la autenticaci�n de token.
     * @return valor booleano que indica si la validacion esta ok.
     * @throws TransferenciaDeFondosException
     * @since 1.0
     */
    public void autenticacionToken(DatosAutorizacionTO datosAutorizacionTO) throws TransferenciaDeFondosException {
        ServiciosSeguridad serviciosSeguridad = getRemoteSeguridad(serviciosSeguridadHome);
        if (datosAutorizacionTO == null) {
            logger.debug("[autenticacionToken]Error al autenticar token.");
            throw new TransferenciaDeFondosException("TFMX-0009");
        }

        try {
            long rutUsuario = datosAutorizacionTO.getRutUsuario();
            char dvUsuario = datosAutorizacionTO.getDvUsuario();
            String claveToken = datosAutorizacionTO.getClaveToken();
            if (!serviciosSeguridad.autenticaToken(rutUsuario, dvUsuario, claveToken)) {
                logger.debug("[autenticacionToken]Error al autenticar token.");
                throw new TransferenciaDeFondosException("TFMX-0009");
            }
        }
        catch (TuxedoException e) {
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[autenticacionToken]" + "Se produjo un error al invocar servicio tuxedo, causa: "
                        + e.getMessage());
            }
            throw new TransferenciaDeFondosException("TFMX-0021");
        }
        catch (SeguridadException e) {
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[autenticacionToken]" + "Se produjo un error al invocar servicio seguridad, causa: "
                        + e.getInfoAdic());
            }
            throw new TransferenciaDeFondosException("TFMX-0008");
        }
        catch (RemoteException e) {
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[autenticacionToken]" + "Se produjo un error al obtener la interfaz remota: "
                        + e.getMessage());
            }
            throw new TransferenciaDeFondosException("TFMX-0100" + " ServicioSeguridad");
        }
    }

    /**
     * Metodo responsable de traer los tipos de productos que sera usado para validar el tipo firma de los
     * apoderados.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 28/07/2008, Pei Kang Fu(Neoris Chile) - versi�n inicial
     * <li>1.1 03/02/2009, Pei Kang Fu.(Neoris Chile) - se agregan los if corespondientes antes de cada sentencia
     * de logger de acuerdo a la norma del banco.
     * </ul>
     * </p>
     *
     * @param datosAutorizacionTO objeto to que encapsule informaci�n necesaria para extraer el tipo de producto.
     * @return el tipo de producto.
     * @throws TransferenciaDeFondosException
     * @since 1.0
     */
    public String extraeTipoProducto(DatosAutorizacionTO datosAutorizacionTO)
            throws TransferenciaDeFondosException {

        BCIExpress express = getRemoteExpress(servicioBCIExpressHome);
        String codigoProducto = null;
        String convenio = null;
        String codServicio = null;
        try {
            if (datosAutorizacionTO == null) {
                logger.debug("[extraeTipoProducto]parametro de entrada es nulo.");
                throw new TransferenciaDeFondosException("TFMX-0029");
            }
            String rutUsuario = StringUtil.completaPorLaIzquierda(String.valueOf(
                    datosAutorizacionTO.getRutUsuario()).trim(), 8, '0');
            char dvUsuario = datosAutorizacionTO.getDvUsuario();
            String rutEmpresa = StringUtil.completaPorLaIzquierda(String.valueOf(
                    datosAutorizacionTO.getRutEmpresa()).trim(), 8, '0');
            char dvEmpresa = datosAutorizacionTO.getDvEmpresa();
            if (datosAutorizacionTO.getConvenio() != null) {
                convenio = datosAutorizacionTO.getConvenio();
                convenio = StringUtil.completaConEspacios(convenio, 10);
            }
            if (datosAutorizacionTO.getCodigoServicio() != null) {
                codServicio = datosAutorizacionTO.getCodigoServicio();
            }

            ResultExtraeTipoProducto tiposProductos = express.extraeTipoProducto(rutUsuario, dvUsuario,
                    rutEmpresa, dvEmpresa, convenio, codServicio);
            if (tiposProductos != null) {
                ExtraeTipoProducto[] etp = tiposProductos.getExtraeTipoProducto();
                if (etp != null && etp.length > 0) {
                    if (etp[0] != null) {
                        codigoProducto = etp[0].getIdProductoCli();
                    }
                }
            }
            return codigoProducto;
        }
        catch (TuxedoException e) {
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[extraeTipoProducto]" + "Se produjo un error al invocar servicio tuxedo, causa: "
                        + e.getMessage());
            }
            throw new TransferenciaDeFondosException("TFMX-0021");
        }
        catch (BCIExpressException e) {
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[extraeTipoProducto]"
                        + "Se produjo un error al invocar servicio BCIExpress, causa: " + e.getInfoAdic());
            }
            throw new TransferenciaDeFondosException("TFMX-0022");
        }
        catch (RemoteException e) {
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[extraeTipoProducto]" + "Se produjo un error al obtener la interfaz remota: "
                        + e.getMessage());
            }
            throw new TransferenciaDeFondosException("TFMX-0100" + "BCIExpress");
        }
        catch (GeneralException e) {
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[extraeTipoProducto]" + "Se produjo un GeneralException, causa: " + e.getInfoAdic());
            }
            throw new TransferenciaDeFondosException(e.getInfoAdic());
        }
    }

    /**
     *
     * M�todo encargado de autorizar la transferencia para el apoderado real.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 28/07/2008, Pei Kang Fu(Neoris Chile) - versi�n inicial
     * <li>1.1 03/02/2009, Pei Kang Fu.(Neoris Chile) - se agregan los if corespondientes antes de cada sentencia
     * de logger de acuerdo a la norma del banco.
     * </ul>
     * </p>
     *
     * @param datosAutorizacionTO objeto que encapsula datos para la autorizacion.
     * @throws TransferenciaDeFondosException
     * @since 1.0
     */
    private void autorizaPagoCuentaCorriente(DatosAutorizacionTO datosAutorizacionTO)
            throws TransferenciaDeFondosException {
        BCIExpress express = getRemoteExpress(servicioBCIExpressHome);
        String rutEmpresa = StringUtil.completaPorLaIzquierda(String.valueOf(datosAutorizacionTO.getRutEmpresa())
                .trim(), 8, '0');
        String rutUsuario = StringUtil.completaPorLaIzquierda(String.valueOf(datosAutorizacionTO.getRutUsuario())
                .trim(), 8, '0');
        char dvEmpresa = datosAutorizacionTO.getDvEmpresa();
        char dvUsuario = datosAutorizacionTO.getDvUsuario();
        /**
         * en vez de idTransferencia es idAutorizacion por requerimiento del banco que debe ser concadenado con
         * tipoTransferencia + idTransferencia
         */
        String idAutorizacion = datosAutorizacionTO.getIdAutorizacion();
        String convenio = datosAutorizacionTO.getConvenio();
        String autorizacion = "";
        ResultAutorizaPagoCuentaCorriente resultado = null;
        AutorizaPagoCtaCte[] autPagoCtaCte = null;
        try {
            resultado = express.autorizaPagoCuentaCorriente(rutUsuario, dvUsuario, convenio, rutEmpresa,
                    dvEmpresa, idAutorizacion, AUTOTRANSACCION, FECHAEXPIRACION);
        }
        catch (Exception e) {

            logger.debug("[autorizaPagoCuentaCorriente]No se puede validar firma.");
            try {
                logger.debug("[autorizaPagoCuentaCorriente]Borrar registro de la AUT.");

                express.borraTransferenciaAUT(rutUsuario, dvUsuario, convenio, rutEmpresa, dvEmpresa,
                        idAutorizacion);
                logger.debug("[autorizaPagoCuentaCorriente]Se borra registro de la AUT sin problemas.");
            }
            catch (Exception ex) {
                logger.debug("[autorizaPagoCuentaCorriente]No se puede validar firma, problemas al borrar AUT");
                throw new TransferenciaDeFondosException("TFMX-0038");
            }
            throw new TransferenciaDeFondosException("TFMX-0039");
        }
        if (resultado != null) {
            autPagoCtaCte = resultado.getAutorizaPagoCtaCte();
        }
        if (autPagoCtaCte != null) {
            for (int i = 0; i < autPagoCtaCte.length; i++) {

                if (autPagoCtaCte[i] == null) {
                    break;
                }
                if (logger.isEnabledFor(Level.DEBUG)) {
                    logger.debug("[autorizaPagoCuentaCorriente]autPagoCtaCte[i].getCodigo() = "
                            + autPagoCtaCte[i].getCodigo());
                }
                autorizacion = autPagoCtaCte[i].getCodigo();
            }
            int response = -1;
            try {
                response = Integer.parseInt(autorizacion);
            }
            catch (Exception e) {
                if (logger.isEnabledFor(Level.ERROR)) {
                    logger.error("[autorizaPagoCuentaCorriente]Error convirtiendo respuesta " + autorizacion);
                }
            }
            switch (response) {
                case 0:
                    // Pendiente por Falta de Firmas
                    throw new TransferenciaDeFondosException("TFMX-0068");
                case 1:
                    // Estado Firmado FIR
                    return;
                case 100:
                    // Usuario no es apoderado
                    throw new TransferenciaDeFondosException("TFMX-0069");
                case 101:
                    // El apoderado ya firm�
                    throw new TransferenciaDeFondosException("TFMX-0070");
                case 102:
                    // Problemas de Conexi�n
                    throw new TransferenciaDeFondosException("TFMX-0071");
                default:
                    // Problema al Autorizar Firma
                    throw new TransferenciaDeFondosException("TFMX-0039");
            }
        }
    }

    /**
    *
    * M�todo encargado de de validar el monto tope de un Poder del sistema de firmas contra el monto total 
    * de la N�mina Transfer Monex, realizando el cambio de moneda.
    *
    * <p>
    * Registro de versiones:
    * <ul>
    * <li>1.0 25/08/2014, Christian Saravia (SEnTRA) - versi�n inicial
    * <li>1.1 30/09/2015 Eric Mart�nez (TINet) - Oliver Hidalgo (Ing. Soft. BCI): Se modifica el tipo del 
	* modificador del m�todo a tipo p�blico.
    * </ul>
    * </p>
    *
    * @param datosTransferMonexTO objeto que encapsula datos para la validadci�n y conversi�n
    * @throws TransferenciaDeFondosException excepciones al realizar transferencias de fondos
    * @since 1.0
    */
    public void autorizaPagoCuentaCorrienteMonto(DatosTransferMonexTO datosTransferMonexTO)
            throws TransferenciaDeFondosException {
        BCIExpress express = getRemoteExpress(servicioBCIExpressHome);
        long rutEmp = datosTransferMonexTO.getRutEmpresa();
        long rutUsu = datosTransferMonexTO.getRutUsuario();
        char digitoVerifEmp = datosTransferMonexTO.getDvEmpresa();
        char digitoVerifUsu = datosTransferMonexTO.getDvUsuario();
        String rutEmpresa = StringUtil.completaPorLaIzquierda(String.valueOf(rutEmp).trim(), LARGO_RUT, '0');
        String rutUsuario = StringUtil.completaPorLaIzquierda(String.valueOf(rutUsu).trim(), LARGO_RUT, '0');
        String codigoConvenio = datosTransferMonexTO.getConvenio();
        String codigoAutorizacion = datosTransferMonexTO.getIdAutorizacion();
        String monedaCta = datosTransferMonexTO.getMonedaCuenta();
        double montoDetalleAprobado = datosTransferMonexTO.getMontoDetallesAprobados();
        BigDecimal montoTotal = new BigDecimal(montoDetalleAprobado);
        
        String codigoTipoMoneda = TablaValores.getValor("transferenciafondosmonex.parametros",
                "MONEDA", monedaCta);
        String codigoTipoMonedaPesos = TablaValores.getValor("transferenciafondosmonex.parametros",
                "MONEDA", MONEDA_PESOS);
        
        /**
         * en vez de idTransferencia es idAutorizacion por requerimiento del banco que debe ser concadenado con
         * tipoTransferencia + idTransferencia
         */
        if (logger.isEnabledFor(Level.DEBUG)) {
            logger.debug("[autorizaPagoCuentaCorrienteMonto]monedaCta:" + monedaCta);
            logger.debug("[autorizaPagoCuentaCorrienteMonto]codigoTipoMoneda:" + codigoTipoMoneda);
            logger.debug("[autorizaPagoCuentaCorrienteMonto]montoDetalleAprobado:" + montoTotal);
        }
        if (!monedaCta.equals(MONEDA_PESOS)) {
            try {
                Date fecha = new Date();
                BCIExpressDAO servicioDao = new BCIExpressDAO();
                double valorCambio = servicioDao.cambioMoneda(codigoTipoMoneda, codigoTipoMonedaPesos, fecha);
                BigDecimal bdValorCambio = new BigDecimal(valorCambio);
                montoTotal = montoTotal.multiply(bdValorCambio);
                if (logger.isEnabledFor(Level.DEBUG)) {
                    logger.debug("[autorizaPagoCuentaCorrienteMonto]valorCambio:" + bdValorCambio);
                    logger.debug("[autorizaPagoCuentaCorrienteMonto]montoTotal:" + montoTotal);
                }
                
            }
            catch (Exception e) {
                logger.error("[autorizaPagoCuentaCorrienteMonto] Error al convertir moneda a peso:"
                        + ErroresUtil.extraeStackTrace(e));
                throw new TransferenciaDeFondosException("TFMX-0089");
            }
        }
        
        String autorizacion = "";
        ResultAutorizaPagoCuentaCorriente resultado = null;
        AutorizaPagoCtaCte[] autPagoCtaCte = null;
        try {
            if (logger.isEnabledFor(Level.DEBUG)) {
                logger.debug("[autorizaPagoCuentaCorrienteMonto]montoTotal(double):" + montoTotal.doubleValue());
            }
            Date fecExpiracion = FechasUtil.convierteStringADate(FECHAEXPIRACION, 
                    new SimpleDateFormat("dd/MM/yyyy"));
            resultado = express.autorizaPagoCuentaCorriente(rutUsu, digitoVerifUsu, codigoConvenio, rutEmp, 
                    digitoVerifEmp, codigoAutorizacion, AUTOTRANSACCION, fecExpiracion, montoTotal.doubleValue());
        }
        catch (Exception e) {
            logger.error("[autorizaPagoCuentaCorrienteMonto]No se puede validar firma.");
            try {
                logger.debug("[autorizaPagoCuentaCorrienteMonto]Borrar registro de la AUT.");
                
                express.borraTransferenciaAUT(rutUsuario, digitoVerifUsu, codigoConvenio, rutEmpresa,
                        digitoVerifEmp, codigoAutorizacion);
                logger.debug("[autorizaPagoCuentaCorrienteMonto]Se borra registro de la AUT sin problemas.");
            }
            catch (Exception ex) {
                logger.error("[autorizaPagoCuentaCorrienteMonto]No se puede validar firma, "
                        + "problemas al borrar AUT");
                throw new TransferenciaDeFondosException("TFMX-0038");
            }
            throw new TransferenciaDeFondosException("TFMX-0039");
        }
        if (resultado != null) {
            autPagoCtaCte = resultado.getAutorizaPagoCtaCte();
        }
        if (autPagoCtaCte != null) {
            for (int i = 0; i < autPagoCtaCte.length; i++) {
                
                if (autPagoCtaCte[i] == null) {
                    break;
                }
                if (logger.isEnabledFor(Level.DEBUG)) {
                    logger.debug("[autorizaPagoCuentaCorrienteMonto]autPagoCtaCte[i].getCodigo() = "
                            + autPagoCtaCte[i].getCodigo());
                }
                autorizacion = autPagoCtaCte[i].getCodigo();
            }
            int response = PROBLEMAS_DE_CONEXION;
            try {
                response = Integer.parseInt(autorizacion);
            }
            catch (Exception e) {
                if (logger.isEnabledFor(Level.ERROR)) {
                    logger.error("[autorizaPagoCuentaCorrienteMonto]Error convirtiendo respuesta " + autorizacion);
                }
            }
            switch (response) {
                case SUPERA_MONTO_MAXIMO:
                     // Monto supera m�ximo permitido
                     throw new TransferenciaDeFondosException("TFMX-0088");
                case PENDIENTE_FALTA_FIRMAS:
                    // Pendiente por Falta de Firmas
                    throw new TransferenciaDeFondosException("TFMX-0068");
                case FIRMADA:
                    // Estado Firmado FIR
                    return;
                case NO_ES_APODERADO:
                    // Usuario no es apoderado
                    throw new TransferenciaDeFondosException("TFMX-0069");
                case APODERADO_YA_FIRMO:
                    // El apoderado ya firm�
                    throw new TransferenciaDeFondosException("TFMX-0070");
                case PROBLEMAS_DE_CONEXION:
                    // Problemas de Conexi�n
                    throw new TransferenciaDeFondosException("TFMX-0071");
                default:
                    // Problema al Autorizar Firma
                    throw new TransferenciaDeFondosException("TFMX-0039");
            }
        }
    }
    
    /**
     * Metodo responsable de insertar registro de autorizacion en la tabla AUT de la base de dato banele para
     * transferencia de fondos monex cuando es apoderado virtual.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 28/07/2008, Pei Kang Fu(Neoris Chile) - versi�n inicial
     * <li>1.1 03/02/2009, Pei Kang Fu.(Neoris Chile) - se agregan los if corespondientes antes de cada sentencia
     * de logger de acuerdo a la norma del banco.
     * </ul>
     * </p>
     *
     * @param datosAutorizacionTO objeto que encapsula datos para autorizacion.
     * @throws TransferenciaDeFondosException
     * @since 1.0
     */
    private void insertaTablaAutApoderado(DatosAutorizacionTO datosAutorizacionTO)
            throws TransferenciaDeFondosException {
        ServiciosPagos pagos = getRemotePagos(servicioPagosHome);

        char dvEmpresa = datosAutorizacionTO.getDvEmpresa();
        char dvUsuario = datosAutorizacionTO.getDvUsuario();
        /**
         * en vez de idTransferencia es idAutorizacion por requerimiento del banco que debe ser concadenado con
         * tipoTransferencia + idTransferencia
         */
        String idAutorizacion = datosAutorizacionTO.getIdAutorizacion();
        String convenio = datosAutorizacionTO.getConvenio();
        /**
         * valida si el apoderado ya habia firmado, validar si es necesario para otros bancos.
         */
        if (!validaAutorizacionApoderadoVirtual(datosAutorizacionTO)) {
            logger.debug("[insertaTablaAutApoderado] Este apoderado ya firm�. rut apoderado: "
                    + datosAutorizacionTO.getRutUsuario() + "-" + datosAutorizacionTO.getDvUsuario());
            throw new TransferenciaDeFondosException("TFMX-0070");
        }

        try {
            pagos.insertaAutApo(datosAutorizacionTO.getRutUsuario(), dvUsuario, convenio,
                    datosAutorizacionTO.getRutEmpresa(), dvEmpresa, idAutorizacion,
                    FechasUtil.convierteDateAString(new Date(), FORMATO_FECHA_AUT), INDICADOR_APODERADO_VIRTUAL);
        }
        catch (TuxedoException e) {
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[insertaTablaAutApoderado]"
                        + "Se produjo un error al invocar servicio tuxedo, causa: " + e.getMessage());
            }
            throw new TransferenciaDeFondosException("TFMX-0021");
        }
        catch (PagosException e) {
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[insertaTablaAutApoderado]"
                        + "Se produjo un error al invocar servicio pagos, causa: " + e.getInfoAdic());
            }
            throw new TransferenciaDeFondosException("TFMX-0024");
        }
        catch (RemoteException e) {
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[insertaTablaAutApoderado]" + "Se produjo un error al obtener la interfaz remota: "
                        + e.getMessage());
            }
            throw new TransferenciaDeFondosException("TFMX-0100" + " BCIExpress");
        }
    }

    /**
     *
     * M�todo responsable de validar si un apoderado ya ha firmado la transferencia en el caso de apoderado
     * virtual.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 22/08/2008, Pei Kang Fu (Neoris Chile) - versi�n inicial
     * <li>1.1 03/02/2009, Pei Kang Fu.(Neoris Chile) - se agregan los if corespondientes antes de cada sentencia
     * de logger de acuerdo a la norma del banco.
     * </ul>
     * </p>
     *
     * @param datosAutorizacionTO datos que encapsula los datos para la autorizacion.
     * @return valor booleano, true: ok, false: no ok.
     * @throws TransferenciaDeFondosException
     * @since 1.0
     */
    private boolean validaAutorizacionApoderadoVirtual(DatosAutorizacionTO datosAutorizacionTO)
            throws TransferenciaDeFondosException {

        ApoderadoBO apoderadoBO = new ApoderadoBO();
        if (datosAutorizacionTO == null) {
            logger.debug("[validaAutorizacionApoderadoVirtual]Datos autorizacion es nulo.");
            throw new TransferenciaDeFondosException("TFMX-0039");
        }
        if (datosAutorizacionTO.getIdTransferencia() == null
                || datosAutorizacionTO.getIdTransferencia().equals("")) {
            logger.debug("[validaAutorizacionApoderadoVirtual]id transferencia es nulo o blanco.");
            throw new TransferenciaDeFondosException("TFMX-0018");
        }
        if (datosAutorizacionTO.getTipoTransferencia() == null
                || datosAutorizacionTO.getTipoTransferencia().equals("")) {
            logger.debug("[validaAutorizacionApoderadoVirtual]Tipo transferencia nulo.");
            throw new TransferenciaDeFondosException("TFMX-0016");
        }
        if (datosAutorizacionTO.getRutUsuario() == 0) {
            logger.debug("[validaAutorizacionApoderadoVirtual]No existe el rutUsuario en datosAutorizacionTO.");
            throw new TransferenciaDeFondosException("TFMX-0039");
        }
        ApoderadoTO[] apoderados = apoderadoBO.obtieneApoderados(datosAutorizacionTO.getIdTransferencia(),
                datosAutorizacionTO.getTipoTransferencia());

        if (apoderados == null) {
            return true;
        }
        else {
            for (int i = 0; i < apoderados.length; i++) {
                ApoderadoTO apoderado = apoderados[i];
                if (datosAutorizacionTO.getRutUsuario() == apoderado.getRutApoderado()) {
                    if (logger.isEnabledFor(Level.DEBUG)) {
                        logger.debug("[validaAutorizacionApoderadoVirtual] Este apoderado ya firm�. rut apoderado: "
                                + apoderado.getRutApoderado() + "-" + apoderado.getDvApoderado());
                    }
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Metodo responsable de validar que el monto total de la transferencia no supere al monto maximo permitido.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 28/07/2008, Pei Kang Fu (Neoris Chile) - versi�n inicial
     * <li>1.1 03/02/2009, Pei Kang Fu.(Neoris Chile) - se agregan los if corespondientes antes de cada sentencia
     * de logger de acuerdo a la norma del banco.
     * </ul>
     * </p>
     *
     * @param datosAutorizacionTO datos que encapsula los datos para la validaci�n de monto m�ximo.
     * @throws TransferenciaDeFondosException
     * @since 1.0
     */
    public void validaMontoMaximoAutorizado(DatosAutorizacionTO datosAutorizacionTO)
            throws TransferenciaDeFondosException {
        if (logger.isEnabledFor(Level.DEBUG)) {
            logger.debug("[validaMontoMaximoAutorizado]datosAutorizacionTO: " + datosAutorizacionTO);
        }
        TransferenciaBO transferenciaBO = new TransferenciaBO();
        transferenciaBO.validaMontoMaximoAutorizado(datosAutorizacionTO);
    }

    /**
     * Metodo responsable autorizar una transferencia de moneda extranjera.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 30/07/2008, Pei Kang Fu (Neoris Chile) - versi�n inicial
     * </ul>
     * </p>
     *
     * @param datosAutorizacionTO objeto que encapsula datos necesarios para la autorizacion.
     * @throws TransferenciaDeFondosException
     * @since 1.0
     */
    public void autorizarTransferencia(DatosAutorizacionTO datosAutorizacionTO)
            throws TransferenciaDeFondosException {
        if (datosAutorizacionTO.getTipoApoderado() == DatosAutorizacionTO.APODERADO_REAL) {
            autorizaPagoCuentaCorriente(datosAutorizacionTO);
        }
        else {
            insertaTablaAutApoderado(datosAutorizacionTO);
        }
        registraPagoTransferencia(datosAutorizacionTO);
    }

    /**
     * Metodo responsable que trae el indicador de que si el usuario opera con firma poder.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 04/08/2008, Pei Kang Fu (Neoris Chile) - versi�n inicial
     * <li>1.1 03/02/2009, Pei Kang Fu.(Neoris Chile) - se agregan los if corespondientes antes de cada sentencia
     * de logger de acuerdo a la norma del banco.
     * </ul>
     * </p>
     *
     * @param rutEmpresa rut de la empresa sin digito verificador.
     * @param convenio convenio que posee la empresa.
     * @param codigoProducto el codigo de producto.
     * @param posicionFirma posici�n de la firma del apoderado.
     * @return posici�n del indicador
     * @throws TransferenciaDeFondosException
     * @since 1.0
     */
    public String extraePosicionCpearadat(long rutEmpresa, String convenio, String codigoProducto,
            int posicionFirma) throws TransferenciaDeFondosException {
        if (logger.isEnabledFor(Level.DEBUG)) {
            logger.debug("[extraePosicionCpearadat]rutEmpresa: " + rutEmpresa + " convenio: " + convenio
                    + "codigoProducto: " + codigoProducto + "posicionFirma: " + posicionFirma);
        }
        try {
            BCIExpress express = getRemoteExpress(servicioBCIExpressHome);
            String rutEmpresaStr = StringUtil.completaPorLaIzquierda(String.valueOf(rutEmpresa).trim(), 8, '0');
            return express.extraePosicionCpearadat(rutEmpresaStr, convenio, codigoProducto, posicionFirma);
        }
        catch (TuxedoException e) {
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[extraePosicionCpearadat]"
                        + "Se produjo un error al invocar servicio tuxedo, causa: " + e.getMessage());
            }
            throw new TransferenciaDeFondosException("TFMX-0021");
        }
        catch (BCIExpressException e) {
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[extraePosicionCpearadat]"
                        + "Se produjo un error al invocar servicio BCIExpress, causa: " + e.getInfoAdic());
            }
            throw new TransferenciaDeFondosException("TFMX-0022");
        }
        catch (RemoteException e) {
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[extraePosicionCpearadat]" + "Se produjo un error al obtener la interfaz remota: "
                        + e.getMessage());
            }
            throw new TransferenciaDeFondosException("TFMX-0100" + " BCIExpress");
        }
        catch (GeneralException e) {
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[extraePosicionCpearadat]" + "Se produjo un GeneralException, causa: "
                        + e.getInfoAdic());
            }
            throw new TransferenciaDeFondosException(e.getInfoAdic());
        }
    }

    /**
     * Valida los privilegios asociados al apoderado y retorna si este opera con firmas y poderes.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 28/07/2008, Pei Kang Fu (Neoris Chile) - versi�n inicial
     * <li>1.1 03/02/2009, Pei Kang Fu.(Neoris Chile) - se agregan los if corespondientes antes de cada sentencia
     * de logger de acuerdo a la norma del banco.
     * </ul>
     * </p>
     *
     * @param datosAutorizacionTO to que encapsule datos para validaci�n de firma poder.
     * @return char que indicar si el apoderado posee firma y poder.
     * @throws TransferenciaDeFondosException
     * @since 1.0
     */
    public char validaPermisosFirmaPoder(DatosAutorizacionTO datosAutorizacionTO)
            throws TransferenciaDeFondosException {

        BCIExpress express = getRemoteExpress(servicioBCIExpressHome);
        String operaFirmaPoder = null;
        String codigoProducto = null;
        char operaFirmaPoderChar;
        if (datosAutorizacionTO != null) {
            String rutEmpresa = StringUtil.completaPorLaIzquierda(String.valueOf(
                    datosAutorizacionTO.getRutEmpresa()).trim(), 8, '0');
            String rutUsuario = StringUtil.completaPorLaIzquierda(String.valueOf(
                    datosAutorizacionTO.getRutUsuario()).trim(), 8, '0');
            try {
                codigoProducto = extraeTipoProducto(datosAutorizacionTO);

                operaFirmaPoder = extraePosicionCpearadat(datosAutorizacionTO.getRutEmpresa(),
                        datosAutorizacionTO.getConvenio(), codigoProducto, datosAutorizacionTO.getPosicionFirma());

                if (operaFirmaPoder == null) {
                    if (logger.isEnabledFor(Level.ERROR)) {
                        logger.error("[validaPermisosFirmaPoder]Problemas al extraer posicion "
                                + datosAutorizacionTO.getPosicionFirma() + "de la CPE_ARA_DAT para el producto "
                                + codigoProducto + " empresa " + rutEmpresa + " convenio ["
                                + datosAutorizacionTO.getConvenio());
                    }
                    throw new TransferenciaDeFondosException("TFMX-0025");
                }
                else {
                    ResultValidarUsuarioporTipoFirma validaFirmaPoder = null;
                    String statusFirmaPoder = null;
                    operaFirmaPoderChar = operaFirmaPoder.charAt(0);
                    validaFirmaPoder = express.validarUsuarioporTipoFirma(rutUsuario, rutEmpresa,
                            datosAutorizacionTO.getConvenio(), operaFirmaPoderChar, TIPO_PRODUCTO_EXPRESS);
                    if (logger.isEnabledFor(Level.DEBUG)) {
                        logger.debug("[validaPermisosFirmaPoder][" + validaFirmaPoder + "]");
                    }
                    // Rescata indicador de apoderado autorizado
                    // (0) Error (usuario no aurotizado)
                    // (1) Ok (usuario autorizado - Val.Actual (usa SIFP))
                    // (2) Ok (usuario autorizado - Val.Nueva (NO usa SIFP))
                    if (validaFirmaPoder != null) {
                        statusFirmaPoder = validaFirmaPoder.getValUsuporTipoFirma()[0].getIndicadorStatus();
                        if (statusFirmaPoder.equals(NO_AUTORIZADO)) {
                            logger.debug("[validaPermisosFirmaPoder]Apoderado no autorizado. ");
                            throw new TransferenciaDeFondosException("TFMX-0028");
                        }
                        else {
                            return operaFirmaPoderChar;
                        }
                    }
                    else {
                        logger.debug("[validaPermisosFirmaPoder]validaFirmaPoder es null.");
                        throw new TransferenciaDeFondosException("TFMX-0028");
                    }
                }
            }
            catch (TuxedoException e) {
                if (logger.isEnabledFor(Level.ERROR)) {
                    logger.error("[extraeTipoProducto]"
                            + "Se produjo un error al invocar servicio tuxedo, causa: " + e.getMessage());
                }
                throw new TransferenciaDeFondosException("TFMX-0021");
            }
            catch (BCIExpressException e) {
                if (logger.isEnabledFor(Level.ERROR)) {
                    logger.error("[extraeTipoProducto]"
                            + "Se produjo un error al invocar servicio BCIExpress, causa: " + e.getInfoAdic());
                }
                throw new TransferenciaDeFondosException("TFMX-0022");
            }
            catch (RemoteException e) {
                if (logger.isEnabledFor(Level.ERROR)) {
                    logger.error("[extraeTipoProducto]" + "Se produjo un error al obtener la interfaz remota: "
                            + e.getMessage());
                }
                throw new TransferenciaDeFondosException("TFMX-0100" + " BCIExpress");
            }
            catch (GeneralException e) {
                if (logger.isEnabledFor(Level.ERROR)) {
                    logger.error("[extraeTipoProducto]" + "Se produjo un GeneralException, causa: "
                            + e.getInfoAdic());
                }
                throw new TransferenciaDeFondosException(e.getCodigo());
            }
        }
        logger.debug("[validaPermisosFirmaPoder]parametro de entrada es null.");
        throw new TransferenciaDeFondosException("TFMX-0030");
    }

    /**
     * M�todo encargado de obtener todas las cuentas frecuentes asociadas al Rut Empresa.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 29/07/2008, Juan Ramirez (Neoris Chile) - versi�n inicial
     * <li>1.1 03/02/2009, Pei Kang Fu.(Neoris Chile) - se agregan los if corespondientes antes de cada sentencia
     * de logger de acuerdo a la norma del banco.
     * </ul>
     * </p>
     *
     * @param rutEmpresa Rut Empresa a consultar.
     * @return Arreglo de Cuentas Frecuentes.
     * @throws TransferenciaDeFondosException
     * @since 1.0
     */
    public CuentaFrecuenteTO[] listarCtasFrecuentes(long rutEmpresa) throws TransferenciaDeFondosException {
        if (logger.isEnabledFor(Level.DEBUG)) {
            logger.debug("[listarCtasFrecuentes]Rut Empresa: " + rutEmpresa + " N�mero Convenio: ");
        }
        CuentaFrecuenteBO cuentaFrecuenteBO = new CuentaFrecuenteBO();

        return cuentaFrecuenteBO.listarCtasFrecuentes(rutEmpresa);
    }

    /**
     *
     * M�todo encargado de eliminar una Cuenta Frecuente.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 30/07/2008, Juan Ramirez (Neoris Chile) - versi�n inicial
     * <li>1.1 03/02/2009, Pei Kang Fu.(Neoris Chile) - se agregan los if corespondientes antes de cada sentencia
     * de logger de acuerdo a la norma del banco.
     * </ul>
     * </p>
     *
     * @param idCtaFrecuente n�mero identificador asociado a una cuenta frecuente.
     * @throws TransferenciaDeFondosException
     * @since 1.0
     */
    public void eliminaCtaFrecuente(long idCtaFrecuente) throws TransferenciaDeFondosException {
        if (logger.isEnabledFor(Level.DEBUG)) {
            logger.debug("[eliminaCtaFrecuente]idCtaFrecuente: " + idCtaFrecuente);
        }
        CuentaFrecuenteBO cuentaFrecuenteBO = new CuentaFrecuenteBO();
        cuentaFrecuenteBO.eliminaCtaFrecuente(idCtaFrecuente);
    }

    /**
     * M�todo encargado de Crear y/o Actualizar una Cuenta Frecuente.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 30/07/2008, Juan Ramirez (Neoris Chile) - versi�n inicial
     * <li>1.1 03/02/2009, Pei Kang Fu.(Neoris Chile) - se agregan los if corespondientes antes de cada sentencia
     * de logger de acuerdo a la norma del banco.
     * </ul>
     * </p>
     *
     * @param cuentaFrecuenteTO objeto que contiene los valores asociados a Cuenta Frecuente.
     * @throws TransferenciaDeFondosException
     * @since 1.0
     */
    public Long creaEditaCtaFrecuente(CuentaFrecuenteTO cuentaFrecuenteTO) throws TransferenciaDeFondosException {
        if (logger.isEnabledFor(Level.DEBUG)) {
            logger.debug("[creaEditaCtaFrecuente] - cuentaFrecuenteTO " + cuentaFrecuenteTO.toString());
        }
        CuentaFrecuenteBO cuentaFrecuenteBO = new CuentaFrecuenteBO();
        return cuentaFrecuenteBO.creaEditaCtaFrecuente(cuentaFrecuenteTO);
    }

    /**
     *
     * M�todo responsable de enviar el mensaje a la cola jms de la aplicaci�n.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 11/11/2008, Pei Kang Fu (Neoris Chile) - versi�n inicial
     * </ul>
     * </p>
     *
     * @param mensaje mensaje cargado por usuario para enviar a la cola JMS.
     * @throws TransferenciaDeFondosException
     * @since 1.1
     */
    public void enviarMensajeJMSNomina(byte[] mensaje) throws TransferenciaDeFondosException {
        logger.debug("[enviarMensaje] Enviando mensaje.");
        TransferenciaMasivaJMSSender transferenciaMasivaJMSSender = new TransferenciaMasivaJMSSender();
        transferenciaMasivaJMSSender.enviarMensajeJMS(mensaje);
    }

    /**
     * M�todo responsable de autorizar una n�mina de pago para luego realizar el pago de la n�mina.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 15/05/2009, Pei Kang Fu (Neoris Chile) - versi�n inicial
     * <li>1.1 27/10/2010, Jos� Verdugo B. (TINet): Modificaci�n para continuar con el flujo normal cuando
     *         ocurre una excepci�n por fondo insuficiente (FONDO_INSUFICIENTE).
     *
     * </ul>
     * </p>
     *
     * @param datosTransferMonexTO objeto que encapsula datos necesarios para la firma.
     * @throws TransferenciaDeFondosException
     * @since 1.1
     */
    public void autorizarPagoOtrosBancos(DatosTransferMonexTO datosTransferMonexTO)
        throws TransferenciaDeFondosException {
        String indicadorSifp = extraeIndicadorSifp(datosTransferMonexTO.getConvenio(),
                TransferenciaDeFondosParametros.CODIGO_PRODUCTO, datosTransferMonexTO.getRutEmpresa(),
                datosTransferMonexTO.getDvEmpresa(), TransferenciaDeFondosParametros.POSICION_INDICADOR_SIFP,
                datosTransferMonexTO.getRutUsuario());
        datosTransferMonexTO.setTipoTransferencia(TransferenciaDeFondosParametros.TIPO_TFMX_MASIVA);
        String prefijo = TransferenciaDeFondosParametros.TIPO_TFMX_MASIVA.substring(1);
        String idAutorizacion = prefijo + datosTransferMonexTO.getIdTransferencia();
        datosTransferMonexTO.setIdAutorizacion(idAutorizacion);

        TransferenciaDeFondosException exceptionGuardada = null;
        try {
            // determinar que tipo de apoderado es para firmar la n�mina.
            determinarApoderado(datosTransferMonexTO, indicadorSifp);
        }
        catch (TransferenciaDeFondosException e) {
            // Si la excepcion es distinta de saldo insuficiente se continua con el flujo.
            if (!e.getCodigo().equals(TransferenciaDeFondosParametros.FONDO_INSUFICIENTE)) {
                throw e;
            }
            exceptionGuardada = e;
            logger.debug("[autorizarPagoOtrosBancos] Saldo insuficiente, se continua con el flujo.");
        }

        try {
            ejecutarPagoNomina(datosTransferMonexTO);
        }
        catch (TransferenciaDeFondosException e) {

            String textoSaldoInsuficiente = TablaValores.getValor("transferenciafondosmonex.parametros",
                "MENSAJE_SERVICIO_SALDO_INSUFICIENTE", "Desc");
            if (e.getInfoAdic() != null && textoSaldoInsuficiente.trim().equalsIgnoreCase(e.getInfoAdic().trim())) {
                if (logger.isEnabledFor(Level.ERROR)) {
                    logger.error("[autorizarPagoOtrosBancos]saldo insuficiente en la cuenta corriente, causa: "
                        + e.getMessage());
                }
                String mensajeRespuesta = TransferenciaDeFondosParametros.MENSAJE_SALDO_INSUFICIENTE_1
                    + datosTransferMonexTO.getEtiqueta().trim()
                    + TransferenciaDeFondosParametros.getMensajeFondoInsuficiente();

                throw new TransferenciaDeFondosException(TransferenciaDeFondosParametros.FONDO_INSUFICIENTE,
                    mensajeRespuesta);
            }
            else {
                throw e;
            }

        }

        if (exceptionGuardada != null) {
            throw exceptionGuardada;
        }
    }


    /**
     * M�todo responsable de determinar que tipo de apoderado pertencece el usuario para luegar firmar la n�mina.
     * los tipos de apdorados son:
     * <ul>
     * <li>Apoderado real.
     * <li>Apoderado virtual.
     * </ul>
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 15/05/2009, Pei Kang Fu (Neoris Chile) - versi�n inicial
     * <li>1.1 27/10/2010, Jos� Verdugo B. (TINet): Modificaci�n para ejecutar el m�todo
     *         "autorizaPagoCuentaCorriente" cuando la excepci�n es del tipo "FONDO_INSUFICIENTE".
     * </ul>
     * </p>
     *
     * @param datosTransferMonexTO objeto que encapsula datos necesarios para determinar el tipo de apoderado.
     * @param indicadorSifp indicador de firma/poder.
     * @throws TransferenciaDeFondosException
     * @since 1.1
     */
    private void determinarApoderado(DatosTransferMonexTO datosTransferMonexTO, String indicadorSifp)
        throws TransferenciaDeFondosException {
        if (indicadorSifp.equals(INDICADOR_APODERADO_REAL)) {
            logger.debug("[determinarApoderado]El indicador es 'S', apoderado es real.");
            try {
                firmarNomina(datosTransferMonexTO);
                autorizaPagoCuentaCorrienteMonto(datosTransferMonexTO);
            }
            catch (TransferenciaDeFondosException e) {
                // si es pendiente de firma se debe firmar igual
                logger.debug("[determinarApoderado] Pendiente de firma.");
                if (e.getCodigo().equals(TransferenciaDeFondosParametros.PENDIENTE_DE_FIRMA)) {
                    firmarNomina(datosTransferMonexTO);
                    throw new TransferenciaDeFondosException("TFMX-0068");
                }
                else if (e.getCodigo().equals(TransferenciaDeFondosParametros.FONDO_INSUFICIENTE)){
                    logger.debug("[determinarApoderado] Saldo insuficiente, se debe autorizar pago igual.");
                    try {
                        autorizaPagoCuentaCorrienteMonto(datosTransferMonexTO);
                    }
                    catch (TransferenciaDeFondosException ex) {
                        logger.error("[determinarApoderado] error al autorizar el pago.", ex);
                        throw new TransferenciaDeFondosException(ex.getCodigo(), ex.getInfoAdic());
                    }
                    throw new TransferenciaDeFondosException(e.getCodigo(), e.getInfoAdic());
                }
                else {
                    throw new TransferenciaDeFondosException(e.getCodigo(), e.getInfoAdic());
                }
            }
        }
        else {
            logger.debug("[determinarApoderado]El indicador es 'N', apoderado es virtual.");
            insertaTablaAutApoderado(datosTransferMonexTO);
            firmarNomina(datosTransferMonexTO);
        }
    }

    /**
     * M�todo encargado de obtener todos el detalle de una n�mina de pago.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 15/05/2009, Pei Kang Fu (Neoris Chile) - versi�n inicial
     * </ul>
     * </p>
     *
     * @param numSysNomina N�mero sistema de la n�mina.
     * @param rol el rol de usuario.
     * @param idAutorizacion id autorizacon de la n�mina.
     * @return NominaTO objeto que contiene informaci�n de una n�mina de pago.
     * @throws TransferenciaDeFondosException
     * @since 1.1
     */
    public NominaTO obtenerDetalleNomina(Integer numSysNomina, String rol, String idAutorizacion)
        throws TransferenciaDeFondosException {
        TransferenciaOtrosBancosBO transferenciaBO = new TransferenciaOtrosBancosBO();
        return transferenciaBO.obtenerDetalleNomina(numSysNomina, rol, idAutorizacion);
    }

    /**
     * M�todo encargado de obtener el listado de nominas de acuerdo al rango de la fecha.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 15/05/2009, Pei Kang Fu (Neoris Chile) - versi�n inicial
	 * <li>1.1 21/10/2010, Gustavo Espinoza Santiba�ez (SEnTRA): Se agrega parametro rol para
	 * realizar las consultas.
     * </ul>
     * </p>
     *
     * @param rutEmpresa rut de la Empresa.
     * @param dv d�gito verificador del rut del cliente.
     * @param fechaInicio fecha inicio.
     * @param fechaFin fecha fin.
	 * @param rol parametro que representa el rol del usuario que consulta.
     * @return Arreglo de tipo que contiene las nominas de pago.
     * @throws TransferenciaDeFondosException
     * @since 1.1
     *
     */
    public NominaTO[] obtenerListadoNominasPorFecha(long rutEmpresa, char dv, Calendar fechaInicio,
			Calendar fechaFin, String rol) throws TransferenciaDeFondosException {
        TransferenciaOtrosBancosBO transferenciaBO = new TransferenciaOtrosBancosBO();
		return transferenciaBO.obtenerListadoNominasPorFecha(rutEmpresa, dv, fechaInicio, fechaFin, rol);
    }

    /**
     * M�todo encargado de obtener el listado de las nominas de acuerdo al rol del usuario.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 15/05/2009, Pei Kang Fu (Neoris Chile) - versi�n inicial
     * </ul>
     * </p>
     *
     * @param rutEmpresa rut empresa.
     * @param dv d�gito verificador del rut del cliente.
     * @param rol el identificador de la n�mina.
     * @return Arreglo que contiene las n�mina de pago.
     * @throws TransferenciaDeFondosException
     * @since 1.1
     */
    public NominaTO[] obtenerListadoNominasPorRol(long rutEmpresa, char dv, String rol)
        throws TransferenciaDeFondosException {
        TransferenciaOtrosBancosBO transferenciaBO = new TransferenciaOtrosBancosBO();
        return transferenciaBO.obtenerListadoNominasPorRol(rutEmpresa, dv, rol);
    }

    /**
     *
     * M�todo responsable de obtener la n�mina de pago por la etiqueta.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 15/05/2009, Pei Kang Fu (Neoris Chile) - versi�n inicial.
	 * <li>1.1 21/10/2010, Gustavo Espinoza Santiba�ez (SEnTRA): Se modifica el m�todo
	 * para recibir el rol como parametro para consultar.
     * </ul>
     * </p>
     *
     * @param numeroNomina n�mero identificador de la n�mina, equivalente a la etiqueta.
     * @param rutEmpresa rut de la empresa.
     * @param dvEmpresa d�gito verificador de la empresa.
	 * @param rol parametro que representa el rol del usuario que consulta.
     * @return nominaTO objeto que encapsule datos de una n�mina.
     * @throws TransferenciaDeFondosException
     * @since 1.1
     *
     */
	public NominaTO obtenerNominaPorEtiqueta(String numeroNomina, long rutEmpresa, char dvEmpresa, String rol)
        throws TransferenciaDeFondosException {
        TransferenciaOtrosBancosBO transferenciaBO = new TransferenciaOtrosBancosBO();
		return transferenciaBO.obtenerNominaPorEtiqueta(numeroNomina, rutEmpresa, dvEmpresa, rol);
    }

    /**
     * M�todo encargado de obtener el detalle de pago de una n�mina.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 15/05/2009, Pei Kang Fu (Neoris Chile) - versi�n inicial
     * </ul>
     * </p>
     *
     * @param numSysNomina identificador de la n�mina.
     * @param numCorrelativoDetallePago n�mero correlativo del listado.
     * @return NominaDetalleTO que contiene la informaci�n del detalle de pago de una n�mina.
     * @throws TransferenciaDeFondosException
     * @since 1.1
     */
    public NominaDetalleTO obtenerDetallePagoNomina(int numSysNomina, int numCorrelativoDetallePago)
        throws TransferenciaDeFondosException {
        TransferenciaOtrosBancosBO transferenciaBO = new TransferenciaOtrosBancosBO();
        return transferenciaBO.obtenerDetallePagoNomina(numSysNomina, numCorrelativoDetallePago);
    }

    /**
     *
     * M�todo responsable de obtener la informaci�n de posici�n de cambio de la n�mina de pago.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>20/06/2009, Pei Kang Fu (Neoris Chile) - versi�n inicial
     * </ul>
     * </p>
     *
     * @param numSysNomina n�mero sistema de la n�mina
     * @param numCorrelativoDetallePago n�mero correlativo del detalle de pago
     * @param codigoCambio c�digo de cambio
     * @return objeto to que contiene informaci�n de posici�n de cambio de la n�mina
     * @throws TransferenciaDeFondosException excepci�n de la aplicaci�n
     * @since 1.1
     */
    public NominaPosicionCambioTO obtenerDetallePosicionCambioNomina(int numSysNomina,
            int numCorrelativoDetallePago, String codigoCambio) throws TransferenciaDeFondosException {
        TransferenciaOtrosBancosBO transferenciaBO = new TransferenciaOtrosBancosBO();
        return transferenciaBO.obtenerDetallePosicionCambioNomina(numSysNomina, numCorrelativoDetallePago,
                codigoCambio);
    }

    /**
     * M�todo encargado de autorizar(dar el visto bueno) a una n�mina.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 15/05/2009, Pei Kang Fu (Neoris Chile) - versi�n inicial
     * </ul>
     * </p>
     *
     * @param datosTransferMonexTO objeto to que encapsule los datos para dar visto bueno a una n�mina.
     * @throws TransferenciaDeFondosException
     * @since 1.1
     */
    public void autorizarNomina(DatosTransferMonexTO datosTransferMonexTO) throws TransferenciaDeFondosException {
        TransferenciaOtrosBancosBO transferenciaBO = new TransferenciaOtrosBancosBO();
        if (datosTransferMonexTO != null) {
            datosTransferMonexTO.setTipoAccion(TransferenciaDeFondosParametros.ESTADO_TRANSFER_AUTORIZA);
            datosTransferMonexTO.setRolUsuario(TransferenciaDeFondosParametros.ROL_CONTRALOR);
        }
        transferenciaBO.cambiarEstadoNomina(datosTransferMonexTO);
    }

    /**
     * M�todo encargado de firmar una n�mina. Si el error es por falta de fondo, env�a un mensaje de advertencia y
     * sigue el flujo.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 15/05/2009, Pei Kang Fu (Neoris Chile) - versi�n inicial
     * </ul>
     * </p>
     *
     * @param datosTransferMonexTO objeto to que encapsule los datos para dar visto bueno a una n�mina.
     * @throws TransferenciaDeFondosException
     * @since 1.1
     */
    public void firmarNomina(DatosTransferMonexTO datosTransferMonexTO) throws TransferenciaDeFondosException {
        TransferenciaOtrosBancosBO transferenciaBO = new TransferenciaOtrosBancosBO();
        if (datosTransferMonexTO != null) {
            datosTransferMonexTO.setTipoAccion(TransferenciaDeFondosParametros.ESTADO_TRANSFER_FIRMA);
            datosTransferMonexTO.setRolUsuario(TransferenciaDeFondosParametros.ROL_APODERADO);
        }
        // antes de firmar la n�mina debe validar la disponibilidad de la cuenta corriente.
        try {
            transferenciaBO.validarDisponibilidadCuenta(datosTransferMonexTO);
            datosTransferMonexTO.setTimeStampSistema(transferenciaBO.cambiarEstadoNomina(datosTransferMonexTO));
        }
        catch (TransferenciaDeFondosException e) {
            advertirFondoInsuficiente(datosTransferMonexTO, e);
        }
    }

    /**
     * M�todo responsable de manejar la l�gica de negocio "fondo insuficiente", esto es: Cuando el usuario no posee
     * fondo suficiente en la cuenta, se autoriza de todas formas, y env�a un mensaje de advertencia al usuario
     * indicando que no posee fondo en la cuenta.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 15/05/2009, Pei Kang Fu (Neoris Chile) - versi�n inicial
     * </ul>
     * </p>
     *
     * @param datosTransferMonexTO objeto to que encapsule los datos para dar visto bueno a una n�mina.
     * @param e TransferenciaDeFondosException
     * @throws TransferenciaDeFondosException
     * @since 1.1
     */
    private void advertirFondoInsuficiente(DatosTransferMonexTO datosTransferMonexTO,
            TransferenciaDeFondosException e) throws TransferenciaDeFondosException {
        TransferenciaOtrosBancosBO transferenciaBO = new TransferenciaOtrosBancosBO();
        if (e.getCodigo().equals(TransferenciaDeFondosParametros.FONDO_INSUFICIENTE)) {
            logger.error("[advertirFondoInsuficiente]saldo insuficiente en la cuenta corriente.");

            String mensajeRespuesta = TransferenciaDeFondosParametros.MENSAJE_SALDO_INSUFICIENTE_1
                    + datosTransferMonexTO.getEtiqueta().trim()
                    + TransferenciaDeFondosParametros.getMensajeFondoInsuficiente();
            try {
                datosTransferMonexTO.setTimeStampSistema(transferenciaBO.cambiarEstadoNomina(datosTransferMonexTO));
            }
            catch (TransferenciaDeFondosException e1) {
                logger.error("[advertirFondoInsuficiente]Se produjo un problema al firmar la n�mina. \n Advertencia: saldo insuficiente en la cuenta corriente.");
                throw new TransferenciaDeFondosException("TFMX", e1.getInfoAdic() + "\n" + mensajeRespuesta);
            }
            throw new TransferenciaDeFondosException("TFMX-0130", mensajeRespuesta);
        }
        else {
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[advertirFondoInsuficiente]se produjo un problema bloqueante al validar la disponibilidad de la cuenta corriente, causa: "
                        + e.getInfoAdic());
            }
            throw new TransferenciaDeFondosException("TFMX", e.getInfoAdic());
        }
    }

    /**
     * M�todo encargado de rechazar una n�mina.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 15/05/2009, Pei Kang Fu (Neoris Chile) - versi�n inicial
     * </ul>
     * </p>
     *
     * @param datosTransferMonexTO objeto to que encapsule los datos para rechazar la n�mina.
     * @throws TransferenciaDeFondosException
     * @since 1.1
     */
    public void rechazarNomina(DatosTransferMonexTO datosTransferMonexTO) throws TransferenciaDeFondosException {
        TransferenciaOtrosBancosBO transferenciaBO = new TransferenciaOtrosBancosBO();
        if (datosTransferMonexTO != null) {
            // si el rol es contralor, la acci�n es eliminar
            if (datosTransferMonexTO.getRolUsuario() == null) {
                logger.error("[rechazarNomina] El rol de usuario es null.");
                throw new TransferenciaDeFondosException("TFMX-0123");
            }
            if (datosTransferMonexTO.getRolUsuario().equals(TransferenciaDeFondosParametros.ROL_CONTRALOR)) {
                datosTransferMonexTO.setTipoAccion(TransferenciaDeFondosParametros.ESTADO_TRANSFER_ELIMINA);
            }
            else {
                // si el rol es apoderado, la accion es rechazar
                datosTransferMonexTO.setTipoAccion(TransferenciaDeFondosParametros.ESTADO_TRANSFER_RECHAZA);
            }
        }
        transferenciaBO.cambiarEstadoNomina(datosTransferMonexTO);
    }

    /**
     * M�todo encargado de ejecutar pago de una n�mina.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 15/05/2009, Pei Kang Fu (Neoris Chile) - versi�n inicial
     * </ul>
     * </p>
     *
     * @param datosTransferMonexTO objeto to que encapsule los datos para ejecutar pago de la n�mina.
     * @throws TransferenciaDeFondosException
     * @since 1.1
     */
    public void ejecutarPagoNomina(DatosTransferMonexTO datosTransferMonexTO)
        throws TransferenciaDeFondosException {
        TransferenciaOtrosBancosBO transferenciaBO = new TransferenciaOtrosBancosBO();
        if (datosTransferMonexTO != null) {
            datosTransferMonexTO.setTipoAccion(TransferenciaDeFondosParametros.ESTADO_TRANSFER_EJECUTA_PAGO);
            datosTransferMonexTO.setRolUsuario(TransferenciaDeFondosParametros.ROL_APODERADO);
        }
        transferenciaBO.cambiarEstadoNomina(datosTransferMonexTO);
    }

    /**
     * M�todo que obtiene el n�mero secuencial para el n�mero de colilla.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 12/08/2008, Edgardo Olivares (Neoris Chile) - versi�n inicial
     * <li>1.1 15/05/2009, Pei Kang Fu.(Neoris Chile) - se agregan los if corespondientes antes de cada sentencia
     * de logger de acuerdo a la norma del banco.
     * </ul>
     * </p>
     *
     * @return n�mero de secuencia.
     * @throws TransferenciaDeFondosException
     * @since 1.0
     */
    private int getNumeroSecuencia() throws TransferenciaDeFondosException {
        int x = -1;
        try {
            ServiciosMiscelaneos miscbean = (ServiciosMiscelaneos) srvMiscHome.create();
            miscbean = (ServiciosMiscelaneos) srvMiscHome.create();
            x = miscbean.getSecuencia(TransferenciaDeFondosParametros.getCodigoSemilla());
        }
        catch (TuxedoException e) {
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[extraeTipoProducto]" + "Se produjo un error al invocar servicio tuxedo, causa: "
                        + e.getMessage());
            }
            throw new TransferenciaDeFondosException("TFMX-0115");
        }
        catch (MiscelaneosException e) {
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[extraeTipoProducto]" + "Se produjo un MiscelaneosException, causa: "
                        + e.getInfoAdic());
            }
            throw new TransferenciaDeFondosException("TFMX-0116");
        }
        catch (GeneralException e) {
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[extraeTipoProducto]" + "Se produjo un GeneralException, causa: " + e.getInfoAdic());
            }
            throw new TransferenciaDeFondosException("TFMX-0116");
        }
        catch (Exception e) {
            ErroresUtil.extraeStackTrace(e);
            throw new TransferenciaDeFondosException("TFMX-0116");
        }
        return x;
    }

    /**
     * Obtiene el objeto remoto que representa el servicio de seguridad y maneja los errores asociados.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 28/07/2008, Pei Kang Fu(Neoris Chile) - versi�n inicial
     * </ul>
     * </p>
     *
     * @param home Instancia que representa la interfaz home que va a crear el objeto remoto.
     * @return Objeto remoto de ServiciosSeguridad.
     * @throws TransferenciaDeFondosException
     * @since 1.0
     */
    private ServiciosSeguridad getRemoteSeguridad(ServiciosSeguridadHome home)
            throws TransferenciaDeFondosException {
        ServiciosSeguridad remote;
        try {
            remote = home.create();
        }
        catch (Exception e) {
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("Se produjo un error al crear ejb, " + home.getClass() + "causa: " + e.getMessage());
            }
            throw new TransferenciaDeFondosException("TFMX-0101" + " de ServiciosSeguridad.");
        }
        return remote;
    }

    /**
     * Obtiene el objeto remoto que representa el servicio de BCIExpress y maneja los errores asociados.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 28/07/2008, Pei Kang Fu(Neoris Chile) - versi�n inicial
     * </ul>
     * </p>
     *
     * @param home Instancia que representa la interfaz home que va a crear el objeto remoto.
     * @return Objeto remoto de BCIExpress.
     * @throws TransferenciaDeFondosException
     * @since 1.0
     */
    private BCIExpress getRemoteExpress(BCIExpressHome home) throws TransferenciaDeFondosException {
        BCIExpress remote;
        try {
            remote = home.create();
        }
        catch (Exception e) {
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("Se produjo un error al crear ejb, " + home.getClass() + "causa: " + e.getMessage());
            }
            throw new TransferenciaDeFondosException("TFMX-0101" + " de BCIExpress.");
        }
        return remote;
    }

    /**
     * Obtiene el objeto remoto que representa el servicio de pagos y maneja los errores asociados.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 28/07/2008, Pei Kang Fu(Neoris Chile) - versi�n inicial
     * </ul>
     * </p>
     *
     * @param home Instancia que representa la interfaz home que va a crear el objeto remoto.
     * @return Objeto remoto de ServiciosPagos.
     * @throws TransferenciaDeFondosException
     * @since 1.0
     */
    private ServiciosPagos getRemotePagos(ServiciosPagosHome home) throws TransferenciaDeFondosException {
        ServiciosPagos remote;
        try {
            remote = home.create();
        }
        catch (Exception e) {
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("Se produjo un error al crear ejb, " + home.getClass() + "causa: " + e.getMessage());
            }
            throw new TransferenciaDeFondosException("TFMX-0101" + " de ServiciosPagos.");
        }
        return remote;
    }

    /**
     * Metodo con la misi�n de registrar el pago de la transferencia, generando el cargo y el abono en las cuentas
     * correspondientes ademas de registrar la contabilidad para los movimientos entre cuentas.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 28/07/2008, Edgardo Olivares (Neoris Chile) - versi�n inicial
     * <li>1.1 15/05/2009, Pei Kang Fu.(Neoris Chile) - se agregan los if corespondientes antes de cada sentencia
     * de logger de acuerdo a la norma del banco.
     * </ul>
     * </p>
     *
     * @param datosAutorizacionTO TO que contiene datos para autorizaci�n.
     * @return String con el id de la transaccion.
     * @throws TransferenciaDeFondosException
     * @since 1.0
     */
    private Date registraPagoTransferencia(DatosAutorizacionTO datosAutorizacionTO)
            throws TransferenciaDeFondosException {
        TransferenciaTO[] transferenciaTOs = detalleTransferencias(datosAutorizacionTO.getRutEmpresa(),
                datosAutorizacionTO.getConvenio(), datosAutorizacionTO.getIdTransferencia(),
                datosAutorizacionTO.getTipoTransferencia());
        if (logger.isEnabledFor(Level.DEBUG)) {
            logger.debug("[registraPagoTransferencia] - registraPagoTransferencia,datosAutorizacionTO: "
                    + datosAutorizacionTO.toString().concat(" transferenciaTO:").concat(
                            transferenciaTOs.toString()));
        }
        TransferenciaBO transfer = new TransferenciaBO();
        Integer estadoCambiaMonto = null;
        datosAutorizacionTO = transfer.obtenerMaximosCuenta(datosAutorizacionTO);
        datosAutorizacionTO.setCodigoCargo(TransferenciaDeFondosParametros.getCodigoCargo());
        datosAutorizacionTO.setCodigoAbono(TransferenciaDeFondosParametros.getCodigoAbono());
        datosAutorizacionTO.setCuentaContable(TransferenciaDeFondosParametros.getCuentaContable());
        datosAutorizacionTO.setSucursal(TransferenciaDeFondosParametros.getSucursal());
        datosAutorizacionTO.setUsuario(TransferenciaDeFondosParametros.getUsuario());
        datosAutorizacionTO.setSupervisor(TransferenciaDeFondosParametros.getSupervisor());
        // Establece la fecha contable para la operacion.
        Date fechaContable = new Date();
        if (Feriados.esHabil(fechaContable)) {
            datosAutorizacionTO.setFechaContableSiguiente(FechasUtil.diaHabilSiguienteAl(fechaContable));
        }
        else {
            fechaContable = FechasUtil.diaHabilSiguienteAl(fechaContable);
            datosAutorizacionTO.setFechaContableSiguiente(FechasUtil.diaHabilSiguienteAl(fechaContable));
        }
        datosAutorizacionTO.setFechaContable(fechaContable);
        Date fechaTransferencia = null;
        BigDecimal montoTotal = new BigDecimal(0);
        TransferenciaDeFondosDAO servicioDao = new TransferenciaDeFondosDAOImpl();
        TransferenciaBO transferenciaBO = new TransferenciaBO();
        for (int i = 0; i < transferenciaTOs.length; i++) {
            try {
                transferenciaTOs[i].setNumeroColilla(TransferenciaDeFondosParametros.getNumeroColilla()
                        + getNumeroSecuencia());
                fechaTransferencia = servicioDao.transferenciaMonex(datosAutorizacionTO, transferenciaTOs[i]);
                transferenciaTOs[i].setFechaTransferencia(fechaTransferencia);
                transferenciaTOs[i].setEstado(TransferenciaDeFondosParametros.ESTADO_TRF_PAGADO);
                montoTotal = montoTotal.add(transferenciaTOs[i].getMonto());
                if (logger.isEnabledFor(Level.DEBUG)) {
                    logger.debug("[registraPagoTransferencia] Pago exitoso. Numero transferencia: "
                            + transferenciaTOs[i].getIdTransferencia() + " y linea detalle: "
                            + transferenciaTOs[i].getIdLineaDetalle());
                }
                // Realiza contabilizacion de la operacion en cuentasmx
                transfer.contabilizaTransferencia(datosAutorizacionTO, transferenciaTOs[i]);
            }
            catch (TransferenciaDeFondosPersistenciaException e) {
                logger.error(ErroresUtil.extraeStackTrace(e));
                transferenciaTOs[i].setEstado(TransferenciaDeFondosParametros.ESTADO_TRF_RECHAZADO);
                if (logger.isEnabledFor(Level.ERROR)) {
                    logger.error("[registraPagoTransferencia] Pago rechazado. Numero transferencia: "
                            + transferenciaTOs[i].getIdTransferencia() + " y linea detalle: "
                            + transferenciaTOs[i].getIdLineaDetalle());
                }
            }
            // Actualiza estado transferencia.
            // Cambia estado transferencia en banele
            transfer.actualizaEstadoTransferencia(transferenciaTOs[i]);
        }
        logger.debug("[registraPagoTransferencia] se actualiza montos dias transferidos.");
        try {
            datosAutorizacionTO.setMontoTotal(montoTotal);
            datosAutorizacionTO.setCuentaOrigen(StringUtil.completaPorLaIzquierda(
                    datosAutorizacionTO.getCuentaOrigen().trim(), 12, '0'));
            estadoCambiaMonto = transferenciaBO.actualizaMontoDiarioTransferido(datosAutorizacionTO);
        }
        catch (TransferenciaDeFondosException e) {
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[registraPagoTransferencia]Ocurrio un error al actualizar el monto diario transferido. estado: "
                        + estadoCambiaMonto + " causa: " + ErroresUtil.extraeStackTrace(e));
            }
        }
        return fechaTransferencia;
    }

    /**
     * M�todo responsable de obtener el indicador sifp del apoderado.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 15/05/2009, Pei Kang Fu (Neoris Chile) - versi�n inicial
     * </ul>
     * </p>
     *
     * @param convenio convenio que posee la empresa.
     * @param codigoProducto el c�digo del producto.
     * @param rutEmpresa rut de la empresa.
     * @param dv d�gito verificador del rut del cliente.
     * @param posicionIndicadorSifp posici�n del indicador SIFP.
     * @param rutUsuario el rut del apoderado.
     * @return indicador Sifp valor que indica si el apoderado opera con firma y poder.
     * @throws TransferenciaDeFondosException
     * @since 1.1
     */
    private String extraeIndicadorSifp(String convenio, String codigoProducto, long rutEmpresa, char dv,
            int posicionIndicadorSifp, long rutUsuario) throws TransferenciaDeFondosException {
        if (logger.isEnabledFor(Level.DEBUG)) {
            logger.debug("[extraePosicionCpearadat]rutEmpresa: " + rutEmpresa + " convenio: " + convenio
                    + "codigoProducto: " + codigoProducto + "posicionFirma: " + posicionIndicadorSifp);
        }

        String indicadorSifp = extraePosicionCpearadat(rutEmpresa, convenio, codigoProducto, posicionIndicadorSifp);
        String resultValidacion = validaApoderadoSifp(indicadorSifp, convenio,
                TransferenciaDeFondosParametros.PRODUCTO, rutEmpresa, dv, rutUsuario);

        if ((resultValidacion == null) || resultValidacion.equals("") || resultValidacion.equals("0")) {
            logger.debug("[extraeIndicadorSifp]validaApoderadoSifp, el apoderado no tiene privilegio.");
            throw new TransferenciaDeFondosException("TFMX-0030");
        }
        return indicadorSifp;

    }

    /**
     *
     * M�todo responsable de validar si un apoderado opera con firma y poder.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 15/05/2009, Pei Kang Fu (Neoris Chile) - versi�n inicial
     * </ul>
     * </p>
     *
     * @param indicadorSifp indicador sifp.
     * @param convenio convenio que posee la empresa.
     * @param codigoProducto c�digo de producto.
     * @param rutEmpresa rut de la empresa.
     * @param dv d�gito verificador del rut del cliente.
     * @param rutUsuario rut del apoderado.
     * @return indicadorSifp indica si el apoderado opera firma poder.
     * @throws TransferenciaDeFondosException
     * @since 1.1
     */
    private String validaApoderadoSifp(String indicadorSifp, String convenio, String codigoProducto,
            long rutEmpresa, char dv, long rutUsuario) throws TransferenciaDeFondosException {
        if (logger.isEnabledFor(Level.DEBUG)) {
            logger.debug("[validaApoderadoSifp]rutEmpresa: " + rutEmpresa + " convenio: " + convenio
                    + "codigoProducto: " + codigoProducto + "indicadorSifp: " + indicadorSifp);
        }
        try {
            ServiciosComex serviciosComex = serviciosComexHome.create();
            String rutEmpresaStr = StringUtil.completaPorLaIzquierda(String.valueOf(rutEmpresa).trim(), 8, '0');
            String rutUsuarioStr = StringUtil.completaPorLaIzquierda(String.valueOf(rutUsuario).trim(), 8, '0');
            return serviciosComex.ValidaApoderadoSifp(indicadorSifp, convenio, codigoProducto, rutEmpresaStr,
                    rutUsuarioStr);
        }
        catch (TuxedoException e) {
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[validaApoderadoSifp]Se produjo un error al invocar servicio tuxedo, causa: "
                        + e.getMessage());
            }
            throw new TransferenciaDeFondosException("TFMX-0021");
        }
        catch (GeneralException e) {
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[validaApoderadoSifp]Se produjo un GeneralException, causa: " + e.getInfoAdic());
            }
            throw new TransferenciaDeFondosException(e.getCodigo());
        }
        catch (Exception e) {
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[validaApoderadoSifp]Se produjo una exception " + e.getMessage());
            }
            throw new TransferenciaDeFondosException("TFMX-0100");
        }
    }

    /**
     *
     * M�todo responsable de crear un beneficiario.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 15/05/2009, Cristi�n Briones(Neoris Chile) - versi�n inicial
     * </ul>
     * </p>
     *
     * @param beneficiarioTO Objeto to que contiene los datos del beneficiario
     * @throws TransferenciaDeFondosException excepci�n de la aplicaci�n.
     * @since 1.1
     */
    public void crearBeneficiario(BeneficiarioTO beneficiarioTO) throws TransferenciaDeFondosException {
        BeneficiarioBO beneficiarioBO = new BeneficiarioBO();
        beneficiarioBO.crearActualizarBeneficiario(beneficiarioTO);
    }

    /**
     *
     * M�todo responsable de actualizar los datos de un beneficiario.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 15/05/2009, Cristi�n Briones(Neoris Chile) - versi�n inicial
     * </ul>
     * </p>
     *
     * @param beneficiarioTO objeto que contiene informaciones de un beneficiario
     * @throws TransferenciaDeFondosException excepci�n de la aplicaci�n.
     * @since 1.1
     */
    public void actualizarBeneficiario(BeneficiarioTO beneficiarioTO) throws TransferenciaDeFondosException {
        BeneficiarioBO beneficiarioBO = new BeneficiarioBO();
        beneficiarioBO.crearActualizarBeneficiario(beneficiarioTO);
    }

    /**
     *
     * M�todo responsable de obtener datos de un beneficiario.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 15/05/2009, Cristi�n Briones(Neoris Chile) - versi�n inicial
     * </ul>
     * </p>
     *
     * @param datosBeneficiarioTO objeto TO que contiene datos para obtener un beneficiario, estos son:
     *        <ul>
     *        <li>rut empresa
     *        <li>identificador del beneficiario.
     *        <li>c�digo iso de la moneda de la cuenta
     *        <li>c�digo iso del pa�s del banco beneficiario
     *        </ul>
     * @return to que contiene informaciones del beneficiario
     * @throws TransferenciaDeFondosException excepci�n de la aplicaci�n.
     * @since 1.1
     */
    public BeneficiarioTO buscarBeneficiario(DatosBeneficiarioTO datosBeneficiarioTO)
        throws TransferenciaDeFondosException {
        BeneficiarioBO beneficiarioBO = new BeneficiarioBO();
        return beneficiarioBO.obtenerDetalleBeneficiario(datosBeneficiarioTO);
    }

    /**
     *
     * M�todo responsable de eliminar un beneficiario.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 15/05/2009, Cristi�n Briones(Neoris Chile) - versi�n inicial
     * </ul>
     * </p>
     *
     * @param datosBeneficiarioTO objeto TO que contiene datos para eliminar un beneficiario, estos son:
     *        <ul>
     *        <li>rut empresa
     *        <li>identificador del beneficiario.
     *        <li>c�digo iso de la moneda de la cuenta
     *        <li>c�digo iso del pa�s del banco beneficiario
     *        </ul>
     * @throws TransferenciaDeFondosException excepci�n de la aplicaci�n.
     * @since 1.1
     */
    public void eliminarBeneficiario(DatosBeneficiarioTO datosBeneficiarioTO)
        throws TransferenciaDeFondosException {
        BeneficiarioBO beneficiarioBO = new BeneficiarioBO();
        beneficiarioBO.eliminarBeneficiario(datosBeneficiarioTO);
    }

    /**
     *
     * M�todo responsable de obtener el listado de los beneficiarios de una empresa en particular.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 15/05/2009, Cristi�n Briones(Neoris Chile) - versi�n inicial
     * </ul>
     * </p>
     *
     * @param rutEmpresa el rut de la empresa
     * @param dvEmpresa d�gito verificador de la empresa
     * @return arreglo que contiene informaciones de beneficiario
     * @throws TransferenciaDeFondosException excepci�n de la aplicaci�n.
     * @since 1.1
     */
    public BeneficiarioTO[] listarBeneficiarios(long rutEmpresa, char dvEmpresa)
        throws TransferenciaDeFondosException {
        BeneficiarioBO beneficiarioBO = new BeneficiarioBO();
        return beneficiarioBO.obtenerListadoBeneficiarios(rutEmpresa, dvEmpresa);
    }

    /**
     *
     * M�todo responsable de obtener el listado de bancos disponibles de una ciudad.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 15/05/2009, Cristi�n Briones(Neoris Chile) - versi�n inicial
     * </ul>
     * </p>
     *
     * @param idCiudad identificador de la ciudad
     * @param codigoPais el c�digo ISO del pa�s
     * @return arreglo que contiene informaciones de los bancos de una ciudad
     * @throws TransferenciaDeFondosException excepci�n de la aplicaci�n.
     * @since 1.1
     */
    public BancoTO[] listarBancos(int idCiudad, String codigoPais) throws TransferenciaDeFondosException {
        BeneficiarioBO beneficiarioBO = new BeneficiarioBO();
        return beneficiarioBO.obtenerListadoBancos(idCiudad, codigoPais);
    }

    /**
     *
     * M�todo encargado de obtener el listado de ciudades de un pa�s.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 15/05/2009, Cristi�n Briones(Neoris Chile) - versi�n inicial
     * </ul>
     * </p>
     *
     * @param codigoPais c�digo ISO del pa�s
     * @return arreglo que contiene informaciones de las cuidades de un pa�s
     * @throws TransferenciaDeFondosException excepci�n de la aplicaci�n.
     * @since 1.1
     */
    public CiudadTO[] listarCiudades(String codigoPais) throws TransferenciaDeFondosException {
        BeneficiarioBO beneficiarioBO = new BeneficiarioBO();
        return beneficiarioBO.obtenerListadoCiudades(codigoPais);
    }

    /**
     *
     * M�todo responsable de obtener el listado de paises.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 15/05/2009, Cristi�n Briones(Neoris Chile) - versi�n inicial
     * </ul>
     * </p>
     *
     * @return arreglo que contiene informacion de los paises disponibles en el sistema
     * @throws TransferenciaDeFondosException excepci�n de la aplicaci�n.
     * @since 1.1
     */
    public PaisTO[] listarPaises() throws TransferenciaDeFondosException {
        BeneficiarioBO beneficiarioBO = new BeneficiarioBO();
        return beneficiarioBO.obtenerListadoPaises();
    }

    /**
    *
    * M�todo responsable de obtener el listado de paises de residencia para los beneficiarios.
    *
    * <p>
    * Registro de versiones:
    * <ul>
    * <li>1.0 13/11/2015 Oscar Nahuelpan (SEnTRA) - Heraldo Hernandez (Ing. Soft. BCI): version inicial.</li>
    * </ul>
    * </p>
    *
    * @return arreglo que contiene informacion de los paises de residencia disponibles en el sistema.
    * @throws TransferenciaDeFondosException excepci�n de la aplicaci�n.
    * @since 1.6
    */
   public PaisTO[] listarPaisesResidencia() throws TransferenciaDeFondosException {
       BeneficiarioBO beneficiarioBO = new BeneficiarioBO();
       return beneficiarioBO.obtenerListadoPaisesResidencia();
   }

    /**
     *
     * M�todo responsable de obtener el listado de rutas de pago de un pa�s.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 15/05/2009, Cristi�n Briones(Neoris Chile) - versi�n inicial
     * </ul>
     * </p>
     *
     * @param codigoPais c�digo iso del pa�s
     * @return arreglo que contiene informaci�n de las rutas de pago de un pa�s
     * @throws TransferenciaDeFondosException excepci�n de la aplicaci�n.
     * @since 1.1
     */
    public RutaPagoTO[] listarRutasDePago(String codigoPais) throws TransferenciaDeFondosException {
        BeneficiarioBO beneficiarioBO = new BeneficiarioBO();
        return beneficiarioBO.obtenerListadoRutaPago(codigoPais);
    }

    /**
     *
     * M�todo responsable de obtener las informaciones de un banco solicitado por el usuario.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 15/05/2009, Cristi�n Briones(Neoris Chile) - versi�n inicial
     * </ul>
     * </p>
     *
     * @param swift c�digo swift del banco
     * @return to que contiene informaciones del banco
     * @throws TransferenciaDeFondosException excepci�n de la aplicaci�n.
     * @since 1.1
     */
    public BancoTO buscarBancoPorSwift(String swift) throws TransferenciaDeFondosException {
        BeneficiarioBO beneficiarioBO = new BeneficiarioBO();
        return beneficiarioBO.obtenerBancoPorSwift(swift);
    }

    /**
     *
     * M�todo responsable de obtener una lista de monedas.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 15/05/2009, Cristi�n Briones(Neoris Chile) - versi�n inicial
     * </ul>
     * </p>
     *
     * @return arreglo que contiene informaciones de las monedas disponibles del sistema
     * @throws TransferenciaDeFondosException excepci�n de la aplicaci�n.
     * @since 1.1
     */
    public MonedaTO[] listarMonedas() throws TransferenciaDeFondosException {
        BeneficiarioBO beneficiarioBO = new BeneficiarioBO();
        return beneficiarioBO.obtenerListadoMonedas();
    }



    /**
     * M�todo encargado de obtener el listado de nominas.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 21/10/2010, Gustavo Espinoza Santiba�ez (SEnTRA)- Versi�n inicial.
     * </ul>
     * </p>
     *
     * @param rutEmpresa rut de la Empresa.
     * @param dv d�gito verificador del rut de la Empresa.
     * @param rol de la Empresa.
     * @return Arreglo de tipo que contiene las nominas de pago.
     * @throws TransferenciaDeFondosException
     * @since 1.2
     *
     */
    public NominaTO[] consultaNominas(long rutEmpresa, char dv, String rol) throws TransferenciaDeFondosException {
        TransferenciaOtrosBancosBO transferenciaBO = new TransferenciaOtrosBancosBO();
        return transferenciaBO.consultaNominas(rutEmpresa, dv, rol);
    }

    /**
     * M�todo encargado de obtener el listado de liquidaciones.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 15/04/2011, Angelo Vel�squez Vel�squez (SEnTRA)- Versi�n inicial.
     * </ul>
     * </p>
     *
     * @param numeroNomina Numero de nomina.
     * @return Arreglo que contiene los datos de las liquidaciones.
     * @throws TransferenciaDeFondosException
     * @since 1.4
     *
     */
    public DetallePagoTO[] consultaLiquidacion(int numeroNomina)  throws TransferenciaDeFondosException {
        TransferenciaOtrosBancosBO transferenciaBO = new TransferenciaOtrosBancosBO();
        return transferenciaBO.consultaLiquidacion(numeroNomina);
    }

    /**
     * M�todo encargado de obtener el mensaje swift asociado a un pago realizado mediante Nominas de Pago (Moneda Extranjera).
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 15/04/2011, Angelo Vel�squez Vel�squez (SEnTRA)- Versi�n inicial.
     * </ul>
     * </p>
     *
     * @param NumeroOperacionNDP.
	 * @param lineaDetallePago.
     * @return Arreglo que contiene los datos de las liquidaciones.
     * @throws TransferenciaDeFondosException
     * @since 1.4
     *
     */
    public DetallePagoTO[] obtieneMensajeSwift(int numeroOperacionNDP,int lineaDetallePago)  throws TransferenciaDeFondosException {
        TransferenciaOtrosBancosBO transferenciaBO = new TransferenciaOtrosBancosBO();
        return transferenciaBO.obtieneMensajeSwift(numeroOperacionNDP,lineaDetallePago);
    }

	/*******************************************************************/
	/************ TRANSFERENCIA MONEDA EXTRANJERA UNO A UNO ************/
	/*******************************************************************/
    /**
	* Invoca al procedimiento almacenado CVDsrv_shw_cur_vps.
	* <p>
	*
	* Registro de versiones:
	* <ul>
	*
	* <li>1.0 20/08/2013 11:43:01 BcxWSql (Bcx Solutions) : Versi�n inicial.</li>
	* </ul>
	* <p>
	*
	* @param parIn ConsultaMonedaTO Entrada para el SP CVDsrv_shw_cur_vps.
	* @return Moneda1a1TO[] Salida del SP CVDsrv_shw_cur_vps.
	* @throws TtffException En caso de ocurrir un error fatal.
	*
	* @since 1.0
	*/
	public Moneda1a1TO[] obtenerMonedas(ConsultaMonedaTO parIn)
	throws TtffException {
		TransferenciasUnoAUnoDAO objDAO = new TransferenciasUnoAUnoDAO();
		try {
			return objDAO.obtenerMonedas(parIn);
		}
		catch (ServicioDatosException e) {
            logger.error("[obtenerMonedas]:Error [" + ErroresUtil.extraeStackTrace(e) + "]");
			throw (new TtffException(errorServicioDatos));
		}
	}

	/**
	* Invoca al procedimiento almacenado FBPsrv_shw_acc_cyg.
	* <p>
	*
	* Registro de versiones:
	* <ul>
	*
	* <li>1.0 20/08/2013 11:00:47 BcxWSql (Bcx Solutions) : Versi�n inicial.</li>
	* <li> 1.1 16/02/2017 Marcos Abraham Hernandez Bravo (ImageMaker IT) - Miguel Anza (BCI) : Se agrega y normaliza log.</li>
	* </ul>
	* <p>
	*
	* @param parIn String�Entrada para el SP FBPsrv_shw_acc_cyg.
	* @return CuentaComisionTO�Salida del SP FBPsrv_shw_acc_cyg.
	* @throws TtffException En caso de ocurrir un error fatal.
	*
	* @since 1.0
	*/
	public CuentaComisionTO obtenerCuentaDeCargo(String parIn) throws TtffException {
		
		final String nombreMetodo = "obtenerCuentaDeCargo";
		
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "] [BCI_INI]");
			getLogger().info("[" + nombreMetodo + "] [Parametro][parIn][" + StringUtil.contenidoDe(parIn) + "]");
		}
		
		TransferenciasUnoAUnoDAO objDAO = new TransferenciasUnoAUnoDAO();
		
		try {
			CuentaComisionTO retorno = objDAO.obtenerCuentaDeCargo(parIn);
			
			if (getLogger().isEnabledFor(Level.INFO)) {
				getLogger().info("[" + nombreMetodo + "] [BCI_FINOK][" + StringUtil.contenidoDe(retorno) + "]");
			}
			return retorno;
		}
		catch (ServicioDatosException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[" + nombreMetodo + "] [BCI_FINEX][ServicioDatosException][" + e.getMessage() + "]", e);
			}
			throw (new TtffException(errorServicioDatos));
		}
	}

	/**
	* Invoca al procedimiento almacenado FBPsrv_shw_bex_cyg.
	* <p>
	*
	* Registro de versiones:
	* <ul>
	*
	* <li>1.0 20/08/2013 14:05:16 BcxWSql (Bcx Solutions) : Versi�n inicial.</li>
	* </ul>
	* <p>
	*
	* @return GastosBancoExteriorTO[] Salida del SP FBPsrv_shw_bex_cyg.
	* @throws TtffException En caso de ocurrir un error fatal.
	*
	* @since 1.0
	*/
	public GastosBancoExteriorTO[] obtenerGastosBancoExterior()
	throws TtffException {
		TransferenciasUnoAUnoDAO objDAO = new TransferenciasUnoAUnoDAO();
		try {
			return objDAO.obtenerGastosBancoExterior();
		}
		catch (ServicioDatosException e) {
            logger.error("[obtenerGastosBancoExterior]:Error [" + ErroresUtil.extraeStackTrace(e) + "]");
			throw (new TtffException(errorServicioDatos));
		}
	}

	/**
	* Invoca al procedimiento almacenado RTMsrv_shw_cmb_exi.
	* <p>
	*
	* Registro de versiones:
	* <ul>
	*
	* <li>1.0 20/08/2013 12:58:22 BcxWSql (Bcx Solutions) : Versi�n inicial.</li>
	* </ul>
	* <p>
	*
	* @param parIn ConsultaCodigoCambioTO Entrada para el SP RTMsrv_shw_cmb_exi.
	* @return CodigoCambioTO Salida del SP RTMsrv_shw_cmb_exi.
	* @throws TtffException En caso de ocurrir un error fatal.
	*
	* @since 1.0
	*/
	public CodigoCambioTO[] obtenerCodigosDeCambio(ConsultaCodigoCambioTO parIn)
	throws TtffException {
		TransferenciasUnoAUnoDAO objDAO = new TransferenciasUnoAUnoDAO();
		try {
			return objDAO.obtenerCodigosDeCambio(parIn);
		}
		catch (ServicioDatosException e) {
            logger.error("[obtenerCodigosDeCambio]:Error [" + ErroresUtil.extraeStackTrace(e) + "]");
			throw (new TtffException(errorServicioDatos));
		}
	}


	/**
	* Invoca al procedimiento almacenado FBPsrv_shw_fvl_sts.
	* <p>
	*
	* Registro de versiones:
	* <ul>
	*
	* <li>1.0 20/08/2013 13:05:59 BcxWSql (Bcx Solutions) : Versi�n inicial.</li>
	* <li> 1.1 16/02/2017 Marcos Abraham Hernandez Bravo (ImageMaker IT) - Miguel Anza (BCI) : Se agrega y normaliza log.</li>
	* </ul>
	* <p>
	*
	* @param parIn String Entrada para el SP FBPsrv_shw_fvl_sts.
	* @return ValutaTO[] Salida del SP FBPsrv_shw_fvl_sts.
	* @throws TtffException En caso de ocurrir un error fatal.
	*
	* @since 1.0
	*/
	public ValutaTO[] obtenerFechaValuta(String parIn) throws TtffException {
		
		final String nombreMetodo = "obtenerFechaValuta";

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "] [BCI_INI]");
			getLogger().info("[" + nombreMetodo + "] [Parametro][parIn][" + StringUtil.contenidoDe(parIn) + "]");
		}
		
		TransferenciasUnoAUnoDAO objDAO = new TransferenciasUnoAUnoDAO();
		
		try {
			ValutaTO[] retorno = objDAO.obtenerFechaValuta(parIn);
			
			if (getLogger().isEnabledFor(Level.INFO)) {
				getLogger().info("[" + nombreMetodo + "] [BCI_FINOK][" + StringUtil.contenidoDe(retorno) + "]");
			}
			return retorno;
		}
		catch (ServicioDatosException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[" + nombreMetodo + "] [BCI_FINEX][ServicioDatosException][" + e.getMessage() + "]", e);
			}
			throw (new TtffException(errorServicioDatos));
		}
	}

	/**
	* Invoca al procedimiento almacenado FBPsrv_cap_tim_lim.
	* <p>
	*
	* Registro de versiones:
	* <ul>
	*
	* <li>1.0 20/08/2013 13:13:16 BcxWSql (Bcx Solutions) : Versi�n inicial.</li>
	* <li> 1.1 16/02/2017 Marcos Abraham Hernandez Bravo (ImageMaker IT) - Miguel Anza (BCI) : Se agrega y normaliza log.</li>
	* </ul>
	* <p>
	*
	* @param parIn int Entrada para el SP FBPsrv_cap_tim_lim.
	* @return RespuestaHorarioTO Salida del SP FBPsrv_cap_tim_lim.
	* @throws TtffException En caso de ocurrir un error fatal.
	*
	* @since 1.0
	*/
	public RespuestaHorarioTO consultarHorarioAprobacion(int parIn) throws TtffException {
		
		final String nombreMetodo = "consultarHorarioAprobacion";

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "] [BCI_INI]");
			getLogger().info("[" + nombreMetodo + "] [Parametro][parIn][" + StringUtil.contenidoDe(new Integer(parIn)) + "]");
		}

		TransferenciasUnoAUnoDAO objDAO = new TransferenciasUnoAUnoDAO();
		try {
			RespuestaHorarioTO retorno = objDAO.consultarHorarioAprobacion(parIn);
			
			if (getLogger().isEnabledFor(Level.INFO)) {
				getLogger().info("[" + nombreMetodo + "] [BCI_FINOK][" + StringUtil.contenidoDe(retorno) + "]");
			}
			return retorno;
		}
		catch (ServicioDatosException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[" + nombreMetodo + "] [BCI_FINEX][ServicioDatosException][" + e.getMessage() + "]", e);
			}
			throw (new TtffException(errorServicioDatos));
		}
	}

	/**
	* Invoca al procedimiento almacenado FBPsrv_jct_tim_lim.
	* <p>
	*
	* Registro de versiones:
	* <ul>
	*
	* <li>1.0 20/08/2013 14:05:27 BcxWSql (Bcx Solutions) : Versi�n inicial.</li>
	* </ul>
	* <p>
	*
	* @throws TtffException En caso de ocurrir un error fatal.
	*
	* @since 1.0
	*/
	public void consultarHorarioIngreso()
	throws TtffException {
		TransferenciasUnoAUnoDAO objDAO = new TransferenciasUnoAUnoDAO();
		try {
			objDAO.consultarHorarioIngreso();
		}
		catch (ServicioDatosException e) {
            logger.error("[consultarHorarioIngreso]:Error [" + ErroresUtil.extraeStackTrace(e) + "]");
			throw (new TtffException(errorServicioDatos));
		}
	}

	/**
	* Invoca al procedimiento almacenado FBPsrv_upd_itf_dta.
	* <p>
	*
	* Registro de versiones:
	* <ul>
	*
	* <li>1.0 20/08/2013 13:18:36 BcxWSql (Bcx Solutions) : Versi�n inicial.</li>
	* </ul>
	* <p>
	*
	* @param parIn TransferenciaMonexTO Entrada para el SP FBPsrv_upd_itf_dta.
	* @return RespuestaActualizaTtffTO Salida del SP FBPsrv_upd_itf_dta.
	* @throws TtffException En caso de ocurrir un error fatal.
	*
	* @since 1.0
	*/
	public RespuestaActualizaTtffTO actualizarTransferenciaMonex(TransferenciaMonexTO parIn)
	throws TtffException {
		TransferenciasUnoAUnoDAO objDAO = new TransferenciasUnoAUnoDAO();
		try {
			return objDAO.actualizarTransferenciaMonex(parIn);
		}
		catch (ServicioDatosException e) {
            logger.error("[actualizarTransferenciaMonex]:Error [" + ErroresUtil.extraeStackTrace(e) + "]");
			throw (new TtffException(errorServicioDatos));
		}
	}

	/**
	* Invoca al procedimiento almacenado FBPsrv_chg_sts_itf.
	* <p>
	*
	* Registro de versiones:
	* <ul>
	*
	* <li>1.0 20/08/2013 14:05:40 BcxWSql (Bcx Solutions) : Versi�n inicial.</li>
	* </ul>
	* <p>
	*
	* @param parIn CambiaEstadoTttfMonexTO Entrada para el SP FBPsrv_chg_sts_itf.
	* @return String Salida del SP FBPsrv_chg_sts_itf.
	* @throws TtffException En caso de ocurrir un error fatal.
	*
	* @since 1.0
	*/
	public String cambiarEstadoTtffMonex(CambiaEstadoTttfMonexTO parIn)
	throws TtffException{
		TransferenciasUnoAUnoDAO objDAO = new TransferenciasUnoAUnoDAO();
		try {
			return objDAO.cambiarEstadoTtffMonex(parIn);
		}
		catch (ServicioDatosException e) {
            logger.error("[cambiarEstadoTtffMonex]:Error [" + ErroresUtil.extraeStackTrace(e) + "]");
			throw (new TtffException(errorServicioDatos));
		}
	}

	/**
	* Invoca al procedimiento almacenado FBPsrv_shw_swf_1a1.
	* <p>
	*
	* Registro de versiones:
	* <ul>
	*
	* <li>1.0 20/08/2013 18:08:52 BcxWSql (Bcx Solutions) : Versi�n inicial.</li>
	* </ul>
	* <p>
	*
	* @param parIn int Entrada para el SP FBPsrv_shw_swf_1a1.
	* @return String[] Salida del SP FBPsrv_shw_swf_1a1.
	* @throws TtffException En caso de ocurrir un error fatal.
	*
	* @since 1.0
	*/
	public String[] obtenerMensajeSwift(int parIn)
	throws TtffException {
		TransferenciasUnoAUnoDAO objDAO = new TransferenciasUnoAUnoDAO();
		try {
			return objDAO.obtenerMensajeSwift(parIn);
		}
		catch (ServicioDatosException e) {
            logger.error("[obtenerMensajeSwift]:Error [" + ErroresUtil.extraeStackTrace(e) + "]");
			throw (new TtffException(errorServicioDatos));
		}
	}

	/**
	* Invoca al procedimiento almacenado FBPsrv_shw_liq_1a1.
	* <p>
	*
	* Registro de versiones:
	* <ul>
	*
	* <li>1.0 20/08/2013 14:05:52 BcxWSql (Bcx Solutions) : Versi�n inicial.</li>
	* </ul>
	* <p>
	*
	* @param parIn int Entrada para el SP FBPsrv_shw_liq_1a1.
	* @return String[] Salida del SP FBPsrv_shw_liq_1a1.
	* @throws TtffException En caso de ocurrir un error fatal.
	*
	* @since 1.0
	*/
	public String[] obtenerLiquidacion(int parIn)
	throws TtffException {
		TransferenciasUnoAUnoDAO objDAO = new TransferenciasUnoAUnoDAO();
		try {
			return objDAO.obtenerLiquidacion(parIn);
		}
		catch (ServicioDatosException e) {
            logger.error("[obtenerLiquidacion]:Error [" + ErroresUtil.extraeStackTrace(e) + "]");
			throw (new TtffException(errorServicioDatos));
		}
	}

	/**
	* Invoca al procedimiento almacenado FBPsrv_shh_ndp_1a1.
	* <p>
	*
	* Registro de versiones:
	* <ul>
	*
	* <li>1.0 20/08/2013 14:12:36 BcxWSql (Bcx Solutions) : Versi�n inicial.</li>
	* </ul>
	* <p>
	*
	* @param parIn ConsultaTransferenciaTO Entrada para el SP FBPsrv_shh_ndp_1a1.
	* @return Transferencia1a1TO[] Salida del SP FBPsrv_shh_ndp_1a1.
	* @throws TtffException En caso de ocurrir un error fatal.
	*
	* @since 1.0
	*/
	public Transferencia1a1TO[] obtenerTransferencias(ConsultaTransferenciaTO parIn)
	throws TtffException {
		TransferenciasUnoAUnoDAO objDAO = new TransferenciasUnoAUnoDAO();
		try {
			return objDAO.obtenerTransferencias(parIn);
		}
		catch (ServicioDatosException e) {
            logger.error("[obtenerTransferencias]:Error [" + ErroresUtil.extraeStackTrace(e) + "]");
			throw (new TtffException(errorServicioDatos));
		}
	}
	
	/**
    * 
    * Invoca al procedimiento almacenado FBPsrv_shw_psg_rpp.
    * <p>
    *
    * Registro de versiones:
    * <ul>
    *
    * <li>1.0 12/03/2019, Sergio Bustos B. (Everis) - Minheli Mejias (Ing. Soft. BCI): versi�n inicial</li>
    * </ul>
    * <p>
    * 
    * @param parIn ConsultaTransferenciaTO Entrada para el SP FBPsrv_shw_psg_rpp.
    * @return Transferencia1a1TO[] Salida del SP FBPsrv_shw_psg_rpp.
    * @throws TtffException En caso de ocurrir un error fatal.
    *
    * @since 2.1
    */
	public Transferencia1a1TO[] obtenerTransferenciasPenRipple(ConsultaTransferenciaTO parIn)
	throws TtffException {
		TransferenciasUnoAUnoDAO objDAO = new TransferenciasUnoAUnoDAO();
		try {
			return objDAO.obtenerTransferenciasPenRipple(parIn);
		}
		catch (ServicioDatosException e) {
            logger.error("[obtenerTransferencias]:Error [" + ErroresUtil.extraeStackTrace(e) + "]");
			throw (new TtffException(errorServicioDatos));
		}
	}
	
	/**
	* Invoca al procedimiento almacenado FBPsrv_shd_ndp_1a1.
	* <p>
	*
	* Registro de versiones:
	* <ul>
	*
	* <li>1.0 20/08/2013 15:00:13 BcxWSql (Bcx Solutions) : Versi�n inicial.</li>
	* </ul>
	* <p>
	*
	* @param parIn ConsultaDetPagoTO Entrada para el SP FBPsrv_shd_ndp_1a1.
	* @return DetallePago1a1TO Salida del SP FBPsrv_shd_ndp_1a1.
	* @throws TtffException En caso de ocurrir un error fatal.
	*
	* @since 1.0
	*/
	public DetallePago1a1TO consultarDetallePago(ConsultaDetPagoTO parIn)
	throws TtffException {
		TransferenciasUnoAUnoDAO objDAO = new TransferenciasUnoAUnoDAO();
		try {
			return objDAO.consultarDetallePago(parIn);
		}
		catch (ServicioDatosException e) {
            logger.error("[consultarDetallePago]:Error [" + ErroresUtil.extraeStackTrace(e) + "]");
			throw (new TtffException(errorServicioDatos));
		}
	}

	
	/**
	* Invoca al procedimiento almacenado FBPsrv_shd_ndp_rpp.
	* <p>
	*
	* Registro de versiones:
	* <ul>
	*
	* <li>1.0 25/03/2019, Sergio Bustos B. (Everis) - Minheli Mejias (Ing. Soft. BCI): versi�n inicial</li>
	* </ul>
	* <p>
	*
	* @param parIn ConsultaDetPagoTO Entrada para el SP FBPsrv_shd_ndp_1a1.
	* @return DetallePago1a1TO Salida del SP FBPsrv_shd_ndp_1a1.
	* @throws TtffException En caso de ocurrir un error fatal.
	*
	* @since 1.0
	*/
	public DetallePago1a1TO consultarDetallePagoRipple(ConsultaDetPagoTO parIn)
	throws TtffException {
		TransferenciasUnoAUnoDAO objDAO = new TransferenciasUnoAUnoDAO();
		try {
			return objDAO.consultarDetallePagoRipple(parIn);
		}
		catch (ServicioDatosException e) {
            logger.error("[consultarDetallePago]:Error [" + ErroresUtil.extraeStackTrace(e) + "]");
			throw (new TtffException(errorServicioDatos));
		}
	}

	/**
	* Invoca al procedimiento almacenado FBPsrv_shw_csb_1a1.
	* <p>
	*
	* Registro de versiones:
	* <ul>
	*
	* <li>1.0 20/08/2013 14:06:27 BcxWSql (Bcx Solutions) : Versi�n inicial.</li>
	* </ul>
	* <p>
	*
	* @param parIn ConsultaBeneficiariosTO Entrada para el SP FBPsrv_shw_csb_1a1.
	* @return Beneficiario1a1TO[] Salida del SP FBPsrv_shw_csb_1a1.
	* @throws TtffException En caso de ocurrir un error fatal.
	*
	* @since 1.0
	*/
	public Beneficiario1a1TO[] obtenerBeneficiariosAutorizados(ConsultaBeneficiariosTO parIn)
	throws TtffException {
		TransferenciasUnoAUnoDAO objDAO = new TransferenciasUnoAUnoDAO();
		try {
			return objDAO.obtenerBeneficiariosAutorizados(parIn);
		}
		catch (ServicioDatosException e) {
            logger.error("[obtenerBeneficiariosAutorizados]:Error [" + ErroresUtil.extraeStackTrace(e) + "]");
			throw (new TtffException(errorServicioDatos));
		}
	}

	/**
	* Invoca al procedimiento almacenado FBPsrv_shw_dbn_1a1.
	* <p>
	*
	* Registro de versiones:
	* <ul>
	*
	* <li>1.0 20/08/2013 15:09:54 BcxWSql (Bcx Solutions) : Versi�n inicial.</li>
	* <li> 1.1 16/02/2017 Marcos Abraham Hernandez Bravo (ImageMaker IT) - Miguel Anza (BCI) : Se agrega y normaliza log.</li>
	* </ul>
	* <p>
	*
	* @param parIn ConsultaDetBeneficiarioTO Entrada para el SP FBPsrv_shw_dbn_1a1.
	* @return DetalleBeneficiarioTO Salida del SP FBPsrv_shw_dbn_1a1.
	* @throws TtffException En caso de ocurrir un error fatal.
	*
	* @since 1.0
	*/
	public DetalleBeneficiarioTO obtenerDetalleDelBeneficiario(ConsultaDetBeneficiarioTO parIn) throws TtffException {
		
		final String nombreMetodo = "obtenerDetalleDelBeneficiario";

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "] [BCI_INI]");
			getLogger().info("[" + nombreMetodo + "] [Parametro][parIn][" + StringUtil.contenidoDe(parIn) + "]");
		}
		
		TransferenciasUnoAUnoDAO objDAO = new TransferenciasUnoAUnoDAO();
		try {
			DetalleBeneficiarioTO retorno = objDAO.obtenerDetalleDelBeneficiario(parIn);
			
			if (getLogger().isEnabledFor(Level.INFO)) {
				getLogger().info("[" + nombreMetodo + "] [BCI_FINOK][" + StringUtil.contenidoDe(retorno) + "]");
			}
			return retorno;
		}
		catch (ServicioDatosException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[" + nombreMetodo + "] [BCI_FINEX][ServicioDatosException][" + e.getMessage() + "]", e);
			}
			throw (new TtffException(errorServicioDatos));
		}
	}

	/**
	* Invoca al procedimiento almacenado FBPsrv_shw_cyg_1a1.
	* <p>
	*
	* Registro de versiones:
	* <ul>
	*
	* <li>1.0 20/08/2013 14:06:45 BcxWSql (Bcx Solutions) : Versi�n inicial.</li>
	* </ul>
	* <p>
	*
	* @param parIn int Entrada para el SP FBPsrv_shw_cyg_1a1.
	* @return ComisionTO Salida del SP FBPsrv_shw_cyg_1a1.
	* @throws TtffException En caso de ocurrir un error fatal.
	*
	* @since 1.0
	*/
	public ComisionTO obtenerComisiones(int parIn)
	throws TtffException {
		TransferenciasUnoAUnoDAO objDAO = new TransferenciasUnoAUnoDAO();
		try {
			return objDAO.obtenerComisiones(parIn);
		}
		catch (ServicioDatosException e) {
            logger.error("[obtenerComisiones]:Error [" + ErroresUtil.extraeStackTrace(e) + "]");
			throw (new TtffException(errorServicioDatos));
		}
	}
	
	/**
	* Invoca al procedimiento almacenado FBPsrv_shw_csb_web_pen.
	* <p>
	* Registro de versiones:
	* <ul>
	* <li>1.0 20/12/2013 Alfonso Ibarra C. (Bcx Solutions) : Versi�n inicial.</li>
	* </ul>
	* <p>
	* @param beneficiario BeneficiarioTO Entrada para el SP FBPsrv_shw_csb_web_pen.
	* @return BeneficiarioTO[] Salida del SP FBPsrv_shw_csb_web_pen.
	* @throws TtffException En caso de ocurrir un error fatal.
	* @since 1.0
	*/
	public BeneficiarioTO[] listarBeneficiariosPendientes(BeneficiarioTO beneficiario)
	throws TtffException {		
		BeneficiarioBO objBO = new BeneficiarioBO();
		try {
			return objBO.obtenerListadoBeneficiariosPendientes(beneficiario);
		}
		catch (ServicioDatosException e) {
            logger.error("[TransferenciaDeFondosFacadeBean:listarBeneficiariosPendientes]:Error [" 
            	+ ErroresUtil.extraeStackTrace(e) + "]");
			throw (new TtffException(errorServicioDatos));
		}
	}
	
	/**
	* Invoca al procedimiento almacenado FBPsrv_upd_ben_web_pen.
	* <p>
	* Registro de versiones:
	* <ul>
	* <li>1.0 20/12/2013 Alfonso Ibarra C. (Bcx Solutions) : Versi�n inicial.</li>
	* </ul>
	* <p>
	* @param beneficiario BeneficiarioTO Entrada para el SP FBPsrv_upd_ben_web_pen.
	* @return BeneficiarioTO Salida del SP FBPsrv_upd_ben_web_pen.
	* @throws TtffException En caso de ocurrir un error fatal.
	* @since 1.0
	*/
	public BeneficiarioTO crearBeneficiarioPendiente(BeneficiarioTO beneficiario)
	throws TtffException {		
		BeneficiarioBO objBO = new BeneficiarioBO();
		try {
			return objBO.crearBeneficiarioPendiente(beneficiario);
		}
		catch (ServicioDatosException e) {
            logger.error("[TransferenciaDeFondosFacadeBean:crearBeneficiarioPendiente]:Error [" 
            	+ ErroresUtil.extraeStackTrace(e) + "]");
			throw (new TtffException(errorServicioDatos));
		}
	}
	
	/**
	* Invoca al procedimiento almacenado FBPsrv_del_dbn_web_pen.
	* <p>
	* Registro de versiones:
	* <ul>
	* <li>1.0 20/12/2013 Alfonso Ibarra C. (Bcx Solutions) : Versi�n inicial.</li>
	* </ul>
	* <p>
	* @param datos DatosBeneficiarioTO Entrada para el SP FBPsrv_del_dbn_web_pen.
	* @return DatosBeneficiarioTO Salida del SP FBPsrv_del_dbn_web_pen.
	* @throws TtffException En caso de ocurrir un error fatal.
	* @since 1.0
	*/
	public DatosBeneficiarioTO eliminarBeneficiarioPendiente(DatosBeneficiarioTO datos)
	throws TtffException {		
		BeneficiarioBO objBO = new BeneficiarioBO();
		try {
			return objBO.eliminarBeneficiarioPendiente(datos);
		}
		catch (ServicioDatosException e) {
            logger.error("[TransferenciaDeFondosFacadeBean:eliminarBeneficiarioPendiente]:Error [" 
            	+ ErroresUtil.extraeStackTrace(e) + "]");
			throw (new TtffException(errorServicioDatos));
		}
	}
	
	/**
	* Invoca al procedimiento almacenado FBPsrv_shw_dbn_web_1a1.
	* <p>
	* Registro de versiones:
	* <ul>
	* <li>1.0 20/12/2013 Alfonso Ibarra C. (Bcx Solutions) : Versi�n inicial.</li>
	* <li>1.1 07/01/2014 Alfonso Ibarra C. (Bcx Solutions) : cambio nombre de metodo 
	* buscarBeneficiarioInscrito a consultarDatosBeneficiario.</li>
	* </ul>
	* <p>
	* @param datos DatosBeneficiarioTO Entrada para el SP FBPsrv_shw_dbn_web_1a1.
	* @return BeneficiarioTO Salida del SP FBPsrv_shw_dbn_web_1a1.
	* @throws TtffException En caso de ocurrir un error fatal.
	* @since 1.0
	*/
	public BeneficiarioTO consultarDatosBeneficiario(DatosBeneficiarioTO datos)
	throws TtffException {		
		BeneficiarioBO objBO = new BeneficiarioBO();
		try {
			return objBO.obtenerDatosBeneficiario(datos);
		}
		catch (ServicioDatosException e) {
            logger.error("[TransferenciaDeFondosFacadeBean:consultarDatosBeneficiario]:Error [" 
            	+ ErroresUtil.extraeStackTrace(e) + "]");
			throw (new TtffException(errorServicioDatos));
		}
	}
	
	/**
	* Invoca al procedimiento almacenado FBPsrv_upd_ben_web_pen.
	* <p>
	* Registro de versiones:
	* <ul>
	* <li>1.0 20/12/2013 Alfonso Ibarra C. (Bcx Solutions) : Versi�n inicial.</li>
	* </ul>
	* <p>
	* @param beneficiario BeneficiarioTO Entrada para el SP FBPsrv_upd_ben_web_pen.
	* @return BeneficiarioTO Salida del SP FBPsrv_upd_ben_web_pen.
	* @throws TtffException En caso de ocurrir un error fatal.
	* @since 1.0
	*/
	public BeneficiarioTO actualizarBeneficiarioPendiente(BeneficiarioTO beneficiario)
	throws TtffException {		
		BeneficiarioBO objBO = new BeneficiarioBO();
		try {
			return objBO.actualizarBeneficiarioPendiente(beneficiario);
		}
		catch (ServicioDatosException e) {
            logger.error("[TransferenciaDeFondosFacadeBean:actualizarBeneficiarioPendiente]:Error [" 
            	+ ErroresUtil.extraeStackTrace(e) + "]");
			throw (new TtffException(errorServicioDatos));
		}
	}
	
	/**
	* Invoca al procedimiento almacenado FBPsrv_aut_dbn_web.
	* <p>
	* Registro de versiones:
	* <ul>
	* <li>1.0 20/12/2013 Alfonso Ibarra C. (Bcx Solutions) : Versi�n inicial.</li>
	* </ul>
	* <p>
	* @param datos DatosBeneficiarioTO Entrada para el SP FBPsrv_aut_dbn_web.
	* @return BeneficiarioTO Salida del SP FBPsrv_aut_dbn_web.
	* @throws TtffException En caso de ocurrir un error fatal.
	* @since 1.0
	*/
	public DatosBeneficiarioTO autorizarBeneficiario(DatosBeneficiarioTO datos)
	throws TtffException {		
		BeneficiarioBO objBO = new BeneficiarioBO();
		try {
			return objBO.autorizarBeneficiario(datos);
		}
		catch (ServicioDatosException e) {
            logger.error("[TransferenciaDeFondosFacadeBean:autorizarBeneficiario]:Error [" 
            	+ ErroresUtil.extraeStackTrace(e) + "]");
			throw (new TtffException(errorServicioDatos));
		}
	}
	
	/**
     * M�todo encargado de entregar la instancia del Logger.
     * Registro de versiones:
     * <UL>
     * <li> 1.0 16/02/2017 Marcos Abraham Hernandez Bravo (ImageMaker IT) - Miguel Anza (BCI) : Versi�n Inicial.</li>
     * </UL>
     * 
     * @return Logger
     * @since 1.7
     * 
     */
    private Logger getLogger() {
        if (logger == null) {
        	logger = Logger.getLogger(this.getClass());
        }
        return logger;
    }
    
    
    /**
	* Invoca al procedimiento almacenado FBPsrv_shw_cur_rpp.
	* <p>
	*
	* Registro de versiones:
	* <ul>
	* <li>1.0 20/02/2019 Danilo Dominguez (Everis) - Minheli Mejias (Ing. Soft. BCI) : Versi�n inicial.</li>
	* </ul>
	* <p>
	*
	* @param bic String Entrada para el SP FBPsrv_shw_cur_rpp.
	* @return MonedasPermitidasTO[] Salida del SP FBPsrv_shw_cur_rpp.
	* @throws TtffException En caso de ocurrir un error fatal.
	*
	* @since 1.9
	*/
	public MonedasPermitidasTO[] obtenerMonedasRipple(String bic) throws TtffException {
		
		String nombreMetodo = "obtenerMonedasRipple";
		
		if (logger.isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "] [BCI_INI]");
			getLogger().info("[" + nombreMetodo + "] [Parametro][bic][" + bic + "]");
		}
		
		TransferenciasRippleDAO objDao = new TransferenciasRippleDAO();
		try {
			
			MonedasPermitidasTO[] monedaPermitidas = objDao.obtenerMonedasPermitidas(bic);
			 
			if (getLogger().isEnabledFor(Level.INFO)) {
				getLogger().info("[" + nombreMetodo + "] [BCI_FINOK][" + StringUtil.contenidoDe(monedaPermitidas) + "]");
			}
			
			 return monedaPermitidas;
		}
		catch (ServicioDatosException e) {
            if(logger.isEnabledFor(Level.ERROR)){
            	logger.error("[obtenerMonedasRipple]["+bic+"][BCI_FINEX][ServicioDatosException] error con mensaje: " + e.getMessage(), e);
            }
			throw (new TtffException(errorServicioDatos));
		}
	}
	
	/**
	* Invoca al procedimiento almacenado FBPsrv_shw_avl_nod.
	* <p>
	*
	* Registro de versiones:
	* <ul>
	* <li>1.0 20/02/2019 Danilo Dominguez (Everis) - Minheli Mejias (Ing. Soft. BCI) : Versi�n inicial.</li>
	* </ul>
	* <p>
	*
	* @return NodosRippleTO[] Salida del SP FBPsrv_shw_avl_nod.
	* @throws TtffException En caso de ocurrir un error fatal.
	*
	* @since 1.9
	*/
	public NodosRippleTO[] obtenerNodosRippleDisp() throws TtffException{
		
		String nombreMetodo = "obtenerNodosRippleDisp";
		
		if (logger.isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "] [BCI_INI]");
		}
		
		TransferenciasRippleDAO objDao = new TransferenciasRippleDAO();
		try {
			
			NodosRippleTO[] nodosRipple = objDao.obtenerNodosRippleDisp();
			
			if (getLogger().isEnabledFor(Level.INFO)) {
				getLogger().info("[" + nombreMetodo + "] [BCI_FINOK][" + StringUtil.contenidoDe(nodosRipple) + "]");
			}
			
			return nodosRipple;
		}
		catch (ServicioDatosException e) {
		    if(logger.isEnabledFor(Level.ERROR)){
		       getLogger().error("[" + nombreMetodo + "] [BCI_FINEX][ServicioDatosException][" + e.getMessage() + "]", e);
            }
			throw (new TtffException(errorServicioDatos));
		}
	}
	
	/**
	* Invoca al procedimiento almacenado FBPsrv_shw_csb_rpp.
	* <p>
	*
	* Registro de versiones:
	* <ul>
	*
	* <li>1.0 28/02/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft. BCI): Versi�n inicial.</li>
	* </ul>
	* <p>
	*
	* @param parIn ConsultaBeneficiariosRippleTO Entrada para el SP FBPsrv_shw_csb_rpp.
	* @return BeneficiarioRippleTO[] Salida del SP FBPsrv_shw_csb_rpp.
	* @throws TtffException En caso de ocurrir un error fatal.
	*
	* @since 2.0
	*/
	public BeneficiarioRippleTO[] obtenerBeneficiariosAutorizadosRipple(ConsultaBeneficiariosRippleTO parIn) throws TtffException {
		
		String nombreMetodo = "obtenerBeneficiariosAutorizadosRipple";
		
		if(logger.isEnabledFor(Level.INFO)){
			getLogger().info("[" + nombreMetodo + "] [BCI_INI]");
			getLogger().info("[" + nombreMetodo + "] [Parametro][parIn][" + StringUtil.contenidoDe(parIn) + "]");
		}	
		TransferenciasRippleDAO objDao = new TransferenciasRippleDAO();
		try {
			BeneficiarioRippleTO[] beneficiarioRippleTo = objDao.obtenerBeneficiariosAutorizadosRipple(parIn);
			
			if(logger.isEnabledFor(Level.INFO)){
				logger.info ("[obtenerBeneficiariosAutorizadosRipple] [BCI_FINOK] retornando BeneficiarioRippleTO ["+ StringUtil.contenidoDe(beneficiarioRippleTo) +"]");
			}	
			return beneficiarioRippleTo;
		}
		catch (ServicioDatosException e) {
            logger.error("[obtenerBeneficiariosAutorizadosRipple]:Error [" + ErroresUtil.extraeStackTrace(e) + "]");
            if(logger.isEnabledFor(Level.ERROR)){
            	getLogger().error("[" + nombreMetodo + "] [BCI_FINEX][ServicioDatosException][" + e.getMessage() + "]", e);
            }
			throw (new TtffException(errorServicioDatos));
		}
	}
	
	/**
	* Invoca al procedimiento almacenado FBPsrv_shw_dbn_rpp.
	* <p>
	* Registro de versiones:
	* <ul>
	* <li>1.0 28/02/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft. BCI): Versi�n inicial.</li>
	* </ul>
	* <p>
	* @param datos ConsultaDetalleBeneficiarioRippleTO Entrada para el SP FBPsrv_shw_dbn_rpp.
	* @return DetalleBeneficiarioRippleTO Salida del SP FBPsrv_shw_dbn_rpp.
	* @throws TtffException En caso de ocurrir un error fatal.
	* @since 2.0
	*/
	public DetalleBeneficiarioRippleTO obtenerDetalleBeneficiarioRipple(ConsultaDetalleBeneficiarioRippleTO datos) throws TtffException {
		
		String nombreMetodo = "obtenerBeneficiariosAutorizadosRipple";
		
		if(logger.isEnabledFor(Level.INFO)){
			getLogger().info("[" + nombreMetodo + "] [BCI_INI]");
			getLogger().info("[" + nombreMetodo + "] [Parametro][datos][" + StringUtil.contenidoDe(datos) + "]");
		}	
		TransferenciasRippleDAO objDao = new TransferenciasRippleDAO();
		try {
			
			DetalleBeneficiarioRippleTO detalleBeneficiarioRipple = objDao.obtenerDetalleBeneficiarioRipple(datos);
			
			if(logger.isEnabledFor(Level.INFO)){
				getLogger().info("[" + nombreMetodo + "] [BCI_FINOK][" + StringUtil.contenidoDe(detalleBeneficiarioRipple) + "]");
			}	
			return detalleBeneficiarioRipple;
		}
		catch (ServicioDatosException e) {
            if(logger.isEnabledFor(Level.ERROR)){
            	getLogger().error("[" + nombreMetodo + "] [BCI_FINEX][ServicioDatosException][" + e.getMessage() + "]", e);
            }
			throw (new TtffException(errorServicioDatos));
		}
	}
	
	/**
	* Invoca al procedimiento almacenado FBPsrv_shw_cyg_rpp.
	* <p>
	*
	* Registro de versiones:
	* <ul>
	*
	* <li>1.0 05/03/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft. BCI): Versi�n inicial.</li>
	* </ul>
	* <p>
	*
	* @param obtenerComision int Entrada para el SP FBPsrv_shw_cyg_rpp.
	* @return ComisionTO Salida del SP FBPsrv_shw_cyg_rpp.
	* @throws TtffException En caso de ocurrir un error fatal.
	*
	* @since 2.1
	*/
	public wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.ComisionTO obtenerComisionesRipple(ObtenerComisionTO obtenerComision)
	throws TtffException {
		
		String nombreMetodo = "obtenerComisionesRipple";
		
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "] [BCI_INI]");
			getLogger().info("[" + nombreMetodo + "] [Parametro][obtenerComision][" + StringUtil.contenidoDe(obtenerComision) + "]");
		}
		
		TransferenciasRippleDAO objDao = new TransferenciasRippleDAO();
		try {
			
			wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.ComisionTO comisionTo = objDao.obtenerComisiones(obtenerComision);
			
			if (getLogger().isEnabledFor(Level.INFO)) {
				getLogger().info("[" + nombreMetodo + "] [BCI_FINOK][" + StringUtil.contenidoDe(comisionTo) + "]");
			}
			return comisionTo;
		}
		catch (ServicioDatosException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[" + nombreMetodo + "] [BCI_FINEX][ServicioDatosException][" + e.getMessage() + "]", e);
			}
			throw (new TtffException(errorServicioDatos));
		}
	}
	
	/**
	* Invoca en primera isntancia al generado del Hash de la operacion y 
	* posteriormente al procedimiento almacenado FBPsrv_reg_itf_rpp.
	* <p>
	*
	* Registro de versiones:
	* <ul>
	*
	* <li>1.0 05/03/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft. BCI): Versi�n inicial.</li>
	* </ul>
	* <p>
	*
	* @param regTransferencia int Entrada para el SP FBPsrv_reg_itf_rpp.
	* @return RespuestaRegistroTransferenciaTO Salida del SP FBPsrv_reg_itf_rpp.
	* @throws TtffException En caso de ocurrir un error fatal.
	*
	* @since 2.1
	*/
	
	public RespuestaRegistroTransferenciaTO registrarTransferenciaRipple(RegistrarTransferenciaTO regTransferencia) 
		    throws TtffException{
		        
				String nombreMetodo = "obtenerComisionesRipple";
				
				if (getLogger().isEnabledFor(Level.INFO)) {
					getLogger().info("[" + nombreMetodo + "] [BCI_INI]");
					getLogger().info("[" + nombreMetodo + "] [Parametro][regTransferencia][" + StringUtil.contenidoDe(regTransferencia) + "]");
				}
		        
				TransferenciasRippleDAO objDao = new TransferenciasRippleDAO();
				try {
					regTransferencia.setHashOperacion(generarHashOperacion(regTransferencia.getCuentaDebito(), String.valueOf(regTransferencia.getMontoPago()),
							regTransferencia.getMoneda(),regTransferencia.getCodIdentifBenef(),regTransferencia.getCuentaDestino()));
					
					RespuestaRegistroTransferenciaTO respuestaRegistroTransferenciaTO = objDao.registrarTransferencia(regTransferencia);
					
					if (getLogger().isEnabledFor(Level.INFO)) {
						getLogger().info("[" + nombreMetodo + "] [BCI_FINOK][" + StringUtil.contenidoDe(respuestaRegistroTransferenciaTO) + "]");
					}
					
					return respuestaRegistroTransferenciaTO;
				}
				catch (ServicioDatosException e) {
					if (getLogger().isEnabledFor(Level.ERROR)) {
						getLogger().error("[" + nombreMetodo + "] [BCI_FINEX][ServicioDatosException][" + e.getMessage() + "]", e);
					}
					throw (new TtffException(errorServicioDatos));
				}
			}
	
	/**
	* Generacion del hash de la operacion
	* <p>
	*
	* Registro de versiones:
	* <ul>
	*
	* <li>1.0 05/03/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft. BCI): Versi�n inicial.</li>
	* </ul>
	* <p>
	*
	* @param regTransferencia int Entrada para el SP FBPsrv_reg_itf_rpp.
	* @throws TtffException En caso de ocurrir un error fatal.
	*
	* @since 2.1
	*/
	private String generarHashOperacion(String cuentaDebito, String montoPago, String moneda, String identifBenef, String cuentaDestino) 
	throws TtffException {
		
		String nombreMetodo = "generarHashOperacion";
		
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "] [BCI_INI]");
		}
		
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().info("[" + nombreMetodo + "] [Parametro][parIn][cuentaDebito: "+cuentaDebito+"]"
					+ ", [montoPago: "+montoPago+"]"
					+ ", [moneda: "+moneda+"]"
					+ ", [identifBenef: "+identifBenef+"]"
					+ ", [cuentaDestino: "+cuentaDestino+"]");
		}
		
		String hash = cuentaDebito 
				+montoPago
				+moneda
				+identifBenef
				+cuentaDestino;	
		
		String hashOperacion = HashUtil.sha256Hex(hash);
		
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "] [BCI_FINOK][" + hashOperacion + "]");
		}
		return hashOperacion;
	}
	
	/**
	* Invoca al procedimiento almacenado FBPsrv_jct_trp_lim.
	* <p>
	*
	* Registro de versiones:
	* <ul>
	* <li>1.0 19/03/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft. BCI): Versi�n inicial.</li>
	</ul>
	* <p>
	*
	* @return RespuestaHorarioTO Salida del SP FBPsrv_jct_trp_lim.
	* @throws TtffException En caso de ocurrir un error de negocio.
	* 
	* @since 1.0
	*/
	public RespuestaHorarioTO consultarLimiteHorarioIngreso() throws TtffException {
		String nombreMetodo = "consultarLimiteHorarioIngreso";
		
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "] [BCI_INI]");
		}
		
		TransferenciasRippleDAO objDao = new TransferenciasRippleDAO();
		try {
			
			RespuestaHorarioTO horario = objDao.consultarLimiteHorarioIngreso();
			
			if (getLogger().isEnabledFor(Level.INFO)) {
				getLogger().info("[" + nombreMetodo + "] [BCI_FINOK][" + StringUtil.contenidoDe(horario) + "]");
			}
			return horario;
		}
		catch (ServicioDatosException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[" + nombreMetodo + "] [BCI_FINEX][ServicioDatosException][" + e.getMessage() + "]", e);
			}
			throw (new TtffException(errorServicioDatos));
		}
	}
	
	/**
	* Invoca al procedimiento almacenado FBPsrv_apr_itf_rpp.
	* <p>
	*
	* Registro de versiones:
	* <ul>
	* <li>1.0 19/03/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft. BCI): Versi�n inicial.</li>
	</ul>
	* <p>
	* @param transferencia Entrada para el SP FBPsrv_apr_itf_rpp.
	* @return RespuestaSpTO Salida del SP FBPsrv_apr_itf_rpp.
	* @throws TtffException En caso de ocurrir un error de negocio.
	* 
	* @since 1.0
	*/
	public RespuestaSpTO firmarOEliminarTransferenciaRipple(FirmarEliminarTransferenciaRipple transferencia) throws TtffException{
		String nombreMetodo = "firmarOEliminarTransferenciaRipple";
		
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "] [BCI_INI]");
			getLogger().info("[" + nombreMetodo + "] [Parametro][firmarOEliminarTransferenciaRipple][" + StringUtil.contenidoDe(transferencia) + "]");
		}
        
		TransferenciasRippleDAO objDao = new TransferenciasRippleDAO();
		try {
			if(transferencia.getAccion().equalsIgnoreCase(TablaValores.getValor(TABLA_PARAMETROS_TRANSFERRIPPLE,"accionSolicitada","aprobar"))){
				transferencia.setHash(generarHashOperacion(transferencia.getDatosHash().getCuentaOrigen(), 
						transferencia.getDatosHash().getMonto(),
						transferencia.getDatosHash().getMoneda(),
						transferencia.getDatosHash().getIdBeneficiario(),
						transferencia.getDatosHash().getCuentaDestino()));
			}
			else{
				transferencia.setHash("");
			}
			
			RespuestaSpTO respuesta = objDao.firmarOEliminarTransferenciaRipple(transferencia);
			
			if (getLogger().isEnabledFor(Level.INFO)) {
				getLogger().info("[" + nombreMetodo + "] [BCI_FINOK][" + StringUtil.contenidoDe(respuesta) + "]");
			}
			
			return respuesta;
		}
		catch (ServicioDatosException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[" + nombreMetodo + "] [BCI_FINEX][ServicioDatosException][" + e.getMessage() + "]", e);
			}
			throw (new TtffException(errorServicioDatos));
		}
	}
	
	/**
	* Invoca al procedimiento almacenado FBPsrv_edb_cct_mmx.
	* <p>
	*
	* Registro de versiones:
	* <ul>
	* <li>1.0 21/03/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft. BCI): Versi�n inicial.</li>
	</ul>
	* <p>
	* @param correlativoFBP Entrada para el SP FBPsrv_edb_cct_mmx.
	* @return RespuestaSpTO Salida del SP FBPsrv_edb_cct_mmx.
	* @throws TtffException En caso de ocurrir un error de negocio.
	* 
	* @since 1.0
	*/
	public RespuestaSpTO debitarCuentaCorrienteMX(int correlativoFBP) throws TtffException{
		String nombreMetodo = "debitarCuentaCorrienteMX";
		
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "] [BCI_INI]");
			getLogger().info("[" + nombreMetodo + "] [Parametro][debitarCuentaCorrienteMX][" + correlativoFBP + "]");
		}
		
		TransferenciasRippleDAO objDao = new TransferenciasRippleDAO();
		try {
						
			RespuestaSpTO respuesta = objDao.debitarCuentaCorrienteMX(correlativoFBP);
			
			if (getLogger().isEnabledFor(Level.INFO)) {
				getLogger().info("[" + nombreMetodo + "] [BCI_FINOK][" + StringUtil.contenidoDe(respuesta) + "]");
			}
			
			return respuesta;
		}
		catch (ServicioDatosException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[" + nombreMetodo + "] [BCI_FINEX][ServicioDatosException][" + e.getMessage() + "]", e);
			}
			throw (new TtffException(errorServicioDatos));
		}
	}
	
	
	/**
	* Invoca al procedimiento almacenado FBPsrv_sdt_snd_xcr.
	* <p>
	*
	* Registro de versiones:
	* <ul>
	* <li>1.0 22/03/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft. BCI): Versi�n inicial.</li>
	</ul>
	* <p>
	* @param correlativoFBP Entrada para el SP FBPsrv_sdt_snd_xcr.
	* @return DatosxCurrentTO Salida del SP FBPsrv_sdt_snd_xcr.
	* @throws TtffException En caso de ocurrir un error de negocio.
	* 
	* @since 1.0
	*/
	public DatosxCurrentTO consultarDatosxCurrent (int correlativoFBP) throws TtffException{
		String nombreMetodo = "consultarDatosxCurrent";
		
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "] [BCI_INI]");
			getLogger().info("[" + nombreMetodo + "] [Parametro][debitarCuentaCorrienteMX][" + correlativoFBP + "]");
		}
		
		TransferenciasRippleDAO objDao = new TransferenciasRippleDAO();
		try {
						
			DatosxCurrentTO respuesta = objDao.consultarDatosxCurrent(correlativoFBP);
			
			if (getLogger().isEnabledFor(Level.INFO)) {
				getLogger().info("[" + nombreMetodo + "] [BCI_FINOK][" + StringUtil.contenidoDe(respuesta) + "]");
			}
			
			return respuesta;
		}
		catch (ServicioDatosException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[" + nombreMetodo + "] [BCI_FINEX][ServicioDatosException][" + e.getMessage() + "]", e);
			}
			throw (new TtffException(errorServicioDatos));
		}
	}
	
	/**
	* Invoca al procedimiento almacenado FBPsrv_rcd_rpo_xcr.
	* <p>
	*
	* Registro de versiones:
	* <ul>
	* <li>1.0 22/03/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft. BCI): Versi�n inicial.</li>
	</ul>
	* <p>
	* @param correlativoFBP numero de correlativo de la transaccion.
	* @param resultado resultado del proceso.
	* @param paymentId id del pago.
	* @return RespuestaSpTO Salida del SP FBPsrv_rcd_rpo_xcr.
	* @throws TtffException En caso de ocurrir un error de negocio.
	* 
	* @since 1.0
	*/
	public RespuestaSpTO registrarResulatadoxCurrent (String correlativoFBP, String resultado, String paymentId) throws TtffException{
		String nombreMetodo = "registrarResulatadoxCurrent";
		
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "] [BCI_INI]");
			getLogger().info("[" + nombreMetodo + "] [Parametro][correlativoFBP][" + correlativoFBP + "], [resultado][" + resultado + "], [paymentId][" + paymentId + "] ");
		}
		
		TransferenciasRippleDAO objDao = new TransferenciasRippleDAO();
		try {
						
			RespuestaSpTO respuesta = objDao.registrarResulatadoxCurrent(correlativoFBP, resultado, paymentId);
			
			if (getLogger().isEnabledFor(Level.INFO)) {
				getLogger().info("[" + nombreMetodo + "] [BCI_FINOK][" + StringUtil.contenidoDe(respuesta) + "]");
			}
			
			return respuesta;
		}
		catch (ServicioDatosException e) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[" + nombreMetodo + "] [BCI_FINEX][ServicioDatosException][" + e.getMessage() + "]", e);
			}
			throw (new TtffException(errorServicioDatos));
		}
	}
}