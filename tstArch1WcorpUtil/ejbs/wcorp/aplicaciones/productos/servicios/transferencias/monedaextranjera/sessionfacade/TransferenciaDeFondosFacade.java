package wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.sessionfacade;

import java.rmi.RemoteException;
import java.util.Calendar;

import javax.ejb.EJBObject;

import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.exception.TransferenciaDeFondosException;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.ApoderadoTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.BancoTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.Beneficiario1a1TO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.BeneficiarioTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.CambiaEstadoTttfMonexTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.CiudadTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.CodigoCambioTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.ComisionTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.ConsultaBeneficiariosTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.ConsultaCodigoCambioTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.ConsultaDetBeneficiarioTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.ConsultaDetPagoTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.ConsultaMonedaTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.ConsultaTransferenciaTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.CuentaComisionTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.CuentaFrecuenteTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.CuentaTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.DatosAutorizacionTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.DatosBeneficiarioTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.DatosTransferMonexTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.DetalleBeneficiarioTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.DetallePago1a1TO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.DetallePagoTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.GastosBancoExteriorTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.Moneda1a1TO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.MonedaTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.NominaDetalleTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.NominaPosicionCambioTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.NominaTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.PaisTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.RespuestaActualizaTtffTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.RespuestaHorarioTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.RutaPagoTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.Transferencia1a1TO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.TransferenciaMonexTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.TransferenciaPendienteRippleTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.TransferenciaTO;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.ValutaTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.BeneficiarioRippleTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.ConsultaBeneficiariosRippleTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.ConsultaDetalleBeneficiarioRippleTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.DatosxCurrentTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.DetalleBeneficiarioRippleTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.FirmarEliminarTransferenciaRipple;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.MonedasPermitidasTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.NodosRippleTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.ObtenerComisionTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.RegistrarTransferenciaTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.RespuestaRegistroTransferenciaTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.RespuestaSpTO;

/**
 * Interfaz encargada de conectar los m�todos con el EJB TrasnferenciasDeFondosFacadeBean.
 * 
 * Registro de versiones:
 * <ul>
 * <li>1.0 ??/??/????, desconocido : Versi?n Inicial.</li>
 * <li>1.1 30/09/2015, Eric Mart�nez (TINet) - Oliver Hidalgo (Ing. Soft. BCI): Se cambia a p�blico el tipo de 
 * modificador del m�todo {@link #autorizaPagoCuentaCorrienteMonto(DatosTransferMonexTO)}.<li>
 * <li>1.2 20/02/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft. BCI): Se agregan m�todos correspondientes
 * al proyecto Transferencias Internacionales:
 * 			- obtenerMonedasRipple
 *  		- obtenerNodosRippleDisp
 * </li>
 * </ul>
 * <B>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</B>
 * <P>
 * 
 * @see TransferenciaDeFondosFacadeBean
 * 
 */
public interface TransferenciaDeFondosFacade extends EJBObject {

    /**
     * @see TransferenciaDeFondosFacadeBean#obtenerCuentaOrigen(long, String)
     */
    public CuentaTO[] obtenerCuentasOrigen(long rutCliente, String convenio)
            throws TransferenciaDeFondosException, RemoteException;

    /**
     * @see TransferenciaDeFondosFacadeBean#obtenerDetalleCuenta(String)
     */
    public CuentaTO obtenerDetalleCuenta(String numeroCuenta) throws TransferenciaDeFondosException,
            RemoteException;

    /**
     * @see TransferenciaDeFondosFacadeBean#registrarTransferencia(TransferenciaTO)
     */
    public String registrarTransferencia(TransferenciaTO transferenciaTO) throws TransferenciaDeFondosException,
            RemoteException;

    /**
     * @see TransferenciaDeFondosFacadeBean#eliminarTransferencia(long, String, String, int)
     */
    public void eliminarTransferencia(long rutCliente, String convenio, String idTransferencia, int idLineaDetalle)
            throws TransferenciaDeFondosException, RemoteException;

    /**
     * @see TransferenciaDeFondosFacadeBean#eliminarTransferenciaCompleta(long, String, String)
     */
    public void eliminarTransferenciaCompleta(long rutCliente, String convenio, String idTransferencia)
            throws TransferenciaDeFondosException, RemoteException;

    /**
     * @see TransferenciaDeFondosFacadeBean#obtenerTransferencias(long, String, String, String)
     */
    public TransferenciaTO[] obtenerTransferencias(long rutCliente, String convenio, String idTransferencia,
            String tipoTransferencia) throws TransferenciaDeFondosException, RemoteException;

    /**
     * @see TransferenciaDeFondosFacadeBean#listarTransferencias(long, String, String)
     */
    public TransferenciaTO[] listarTransferencias(long rutEmpresa, String numConvenio, String estado)
            throws TransferenciaDeFondosException, RemoteException;

    /**
     * @see TransferenciaDeFondosFacadeBean#detalleTransferencias(long, String, String, String)
     */
    public TransferenciaTO[] detalleTransferencias(long rutEmpresa, String numConvenio, String numTransferencia,
            String tipoTransferencia) throws TransferenciaDeFondosException, RemoteException;

    /**
     * @see TransferenciaDeFondosFacadeBean#obtenerApoderados(String, String)
     */
    public ApoderadoTO[] obtenerApoderados(String numTransferencia, String tipoTransferencia)
            throws TransferenciaDeFondosException, RemoteException;

    /**
     * @see TransferenciaDeFondosFacadeBean#autenticacionToken(DatosAutorizacionTO)
     */
    public void autenticacionToken(DatosAutorizacionTO datosAutorizacionTO) throws TransferenciaDeFondosException,
            RemoteException;

    /**
     * @see TransferenciaDeFondosFacadeBean#validaPermisosFirmaPoder(DatosAutorizacionTO)
     */
    public char validaPermisosFirmaPoder(DatosAutorizacionTO datosAutorizacionTO)
            throws TransferenciaDeFondosException, RemoteException;

    /**
     * @see TransferenciaDeFondosFacadeBean#validaMontoMaximoAutorizado(DatosAutorizacionTO)
     */
    public void validaMontoMaximoAutorizado(DatosAutorizacionTO datosAutorizacionTO)
            throws TransferenciaDeFondosException, RemoteException;

    /**
     * @see TransferenciaDeFondosFacadeBean#autorizarTransferencia(DatosAutorizacionTO)
     */
    public void autorizarTransferencia(DatosAutorizacionTO datosAutorizacionTO)
            throws TransferenciaDeFondosException, RemoteException;

    /**
     * @see TransferenciaDeFondosFacadeBean#extraeTipoProducto(DatosAutorizacionTO)
     */
    public String extraeTipoProducto(DatosAutorizacionTO datosAutorizacionTO)
            throws TransferenciaDeFondosException, RemoteException;
    
    /**
     * @see TransferenciaDeFondosFacadeBean#autorizaPagoCuentaCorrienteMonto(DatosTransferMonexTO)
     */
    public void autorizaPagoCuentaCorrienteMonto(DatosTransferMonexTO datosTransferMonexTO)
        throws TransferenciaDeFondosException, RemoteException;

    /**
     * @see TransferenciaDeFondosFacadeBean#extraePosicionCpearadat(long, String, String, String)
     */
    public String extraePosicionCpearadat(long rutEmpresa, String convenio, String codigoProducto,
            int posicionFirma) throws TransferenciaDeFondosException, RemoteException;

    /**
     * @see TransferenciaDeFondosFacadeBean#listarCtasFrecuentes(long)
     */
    public CuentaFrecuenteTO[] listarCtasFrecuentes(long rutEmpresa) throws TransferenciaDeFondosException,
            RemoteException;

    /**
     * @see TransferenciaDeFondosFacadeBean#eliminaCtaFrecuente(long)
     */
    public void eliminaCtaFrecuente(long idCtaFrecuente) throws TransferenciaDeFondosException, RemoteException;

    /**
     * @see TransferenciaDeFondosFacadeBean#creaEditaCtaFrecuente(CuentaFrecuenteTO)
     */
    public Long creaEditaCtaFrecuente(CuentaFrecuenteTO cuentaFrecuenteTO) throws TransferenciaDeFondosException,
            RemoteException;

    /**
     * @see TransferenciaDeFondosFacadeBean#enviarMensajeJMSNomina(byte[])
     */
    public void enviarMensajeJMSNomina(byte[] mensaje) throws TransferenciaDeFondosException, RemoteException;

    /**
     * @see TransferenciaDeFondosFacadeBean#autorizarPagoOtrosBancos(DatosTransferMonexTO)
     */
    public void autorizarPagoOtrosBancos(DatosTransferMonexTO DatosTransferMonexTO)
            throws TransferenciaDeFondosException, RemoteException;

    /**
     * @see TransferenciaDeFondosFacadeBean#obtenerDetalleNomina(Integer, String, String)
     */
    public NominaTO obtenerDetalleNomina(Integer numSysNomina, String rol, String idAutorizacion)
            throws TransferenciaDeFondosException, RemoteException;

    /**
     * @see TransferenciaDeFondosFacadeBean#obtenerListadoNominasPorFecha(long, char, Calendar, Calendar, String)
     */
    public NominaTO[] obtenerListadoNominasPorFecha(long rutEmpresa, char dv, Calendar fechaInicio,
            Calendar fechaFin, String rol) throws TransferenciaDeFondosException, RemoteException;

    /**
     * @see TransferenciaDeFondosFacadeBean#obtenerListadoNominasPorRol(long, char, String)
     */
    public NominaTO[] obtenerListadoNominasPorRol(long rutEmpresa, char dv, String rol)
            throws TransferenciaDeFondosException, RemoteException;

    /**
     * @see TransferenciaDeFondosFacadeBean#obtenerDetallePagoNomina(int, int)
     */
    public NominaDetalleTO obtenerDetallePagoNomina(int numSysNomina, int numCorrelativoDetallePago)
            throws TransferenciaDeFondosException, RemoteException;

    /**
     * @see TransferenciaDeFondosFacadeBean#obtenerDetallePosicionCambioNomina(int, int, String)
     */
    public NominaPosicionCambioTO obtenerDetallePosicionCambioNomina(int numSysNomina, int numCorrelativoDetallePago,
            String codigoCambio) throws TransferenciaDeFondosException, RemoteException;

    /**
     * @see TransferenciaDeFondosFacadeBean#autorizarNomina(DatosTransferMonexTO)
     */
    public void autorizarNomina(DatosTransferMonexTO datosTransferMonexTO) throws TransferenciaDeFondosException,
            RemoteException;

    /**
     * @see TransferenciaDeFondosFacadeBean#firmarNomina(DatosTransferMonexTO)
     */
    public void firmarNomina(DatosTransferMonexTO datosTransferMonexTO) throws TransferenciaDeFondosException,
            RemoteException;

    /**
     * @see TransferenciaDeFondosFacadeBean#rechazarNomina(DatosTransferMonexTO)
     */
    public void rechazarNomina(DatosTransferMonexTO datosTransferMonexTO) throws TransferenciaDeFondosException,
            RemoteException;

    /**
     * @see TransferenciaDeFondosFacadeBean#obtenerNominaPorEtiqueta(String, long, char, String)
     */
    public NominaTO obtenerNominaPorEtiqueta(String numeroNomina, long rutEmpresa, char dvEmpresa, String rol)
            throws TransferenciaDeFondosException, RemoteException;

    /**
     * @see TransferenciaDeFondosFacadeBean#crearBeneficiario(BeneficiarioTO)
     */
    public void crearBeneficiario(BeneficiarioTO beneficiarioTO) throws TransferenciaDeFondosException,
            RemoteException;

    /**
     * @see TransferenciaDeFondosFacadeBean#listarBeneficiarios(long, char)
     */
    public BeneficiarioTO[] listarBeneficiarios(long rutEmpresa, char dvEmpresa)
            throws TransferenciaDeFondosException, RemoteException;

    /**
     * @see TransferenciaDeFondosFacadeBean#actualizarBeneficiario(BeneficiarioTO)
     */
    public void actualizarBeneficiario(BeneficiarioTO beneficiarioTO) throws TransferenciaDeFondosException,
            RemoteException;

    /**
     * @see TransferenciaDeFondosFacadeBean#eliminarBeneficiario(DatosBeneficiarioTO)
     */
    public void eliminarBeneficiario(DatosBeneficiarioTO datosBeneficiarioTO)
            throws TransferenciaDeFondosException, RemoteException;

    /**
     * @see TransferenciaDeFondosFacadeBean#buscarBeneficiario(DatosBeneficiarioTO)
     */
    public BeneficiarioTO buscarBeneficiario(DatosBeneficiarioTO datosBeneficiarioTO)
            throws TransferenciaDeFondosException, RemoteException;

    /**
     * @see TransferenciaDeFondosFacadeBean#listarPaises()
     */
    public PaisTO[] listarPaises() throws TransferenciaDeFondosException, RemoteException;

    /**
     * @see TransferenciaDeFondosFacadeBean#listarPaisesResidencia()
     */
    public PaisTO[] listarPaisesResidencia() throws TransferenciaDeFondosException, RemoteException;

    /**
     * @see TransferenciaDeFondosFacadeBean#listarCiudades(String)
     */
    public CiudadTO[] listarCiudades(String codigoPais) throws TransferenciaDeFondosException, RemoteException;

    /**
     * @see TransferenciaDeFondosFacadeBean#listarBancos(int, String)
     */
    public BancoTO[] listarBancos(int idCiudad, String codigoPais) throws TransferenciaDeFondosException,
            RemoteException;

    /**
     * @see TransferenciaDeFondosFacadeBean#listarRutasDePago(String)
     */
    public RutaPagoTO[] listarRutasDePago(String codigoPais) throws TransferenciaDeFondosException,
            RemoteException;

    /**
     * @see TransferenciaDeFondosFacadeBean#buscarBancoPorSwift(String)
     */
    public BancoTO buscarBancoPorSwift(String swift) throws TransferenciaDeFondosException, RemoteException;

    /**
     * @see TransferenciaDeFondosFacadeBean#listarMonedas()
     */
    public MonedaTO[] listarMonedas() throws TransferenciaDeFondosException, RemoteException;
    
    /**
     * @see TransferenciaDeFondosFacadeBean#consultaNominas(long, char, String)
     */
    public NominaTO[] consultaNominas(long rutEmpresa, char dv, String rol) 
    		throws TransferenciaDeFondosException, RemoteException;
    
    /**
     * @see TransferenciaDeFondosFacadeBean#consultaLiquidacion(int numeroNomina)
     */
    public DetallePagoTO[] consultaLiquidacion(int numeroNomina) 
    		throws TransferenciaDeFondosException, RemoteException;
    
    /**
     * @see TransferenciaDeFondosFacadeBean#obtieneMensajeSwift(int numeroOperacionNDP,int lineaDetallePago) 
     */
    public DetallePagoTO[] obtieneMensajeSwift(int numeroOperacionNDP,int lineaDetallePago) 
    		throws TransferenciaDeFondosException, RemoteException;
			
	/*******************************************************************/    
	/************ TRANSFERENCIA MONEDA EXTRANJERA UNO A UNO ************/    
	/*******************************************************************/
	/**
     * @see TransferenciaDeFondosFacadeBean#obtenerMonedas(ConsultaMonedaTO parIn) 
     */
	public Moneda1a1TO[] obtenerMonedas(ConsultaMonedaTO parIn)
	throws TtffException, RemoteException;

	/**
     * @see TransferenciaDeFondosFacadeBean#obtenerCuentaDeCargo(String parIn) 
     */
	public CuentaComisionTO obtenerCuentaDeCargo(String parIn)
	throws TtffException, RemoteException;

	/**
     * @see TransferenciaDeFondosFacadeBean#obtenerGastosBancoExterior() 
     */
	public GastosBancoExteriorTO[] obtenerGastosBancoExterior()
	throws TtffException, RemoteException;
	
	/**
     * @see TransferenciaDeFondosFacadeBean#obtenerCodigosDeCambio(ConsultaCodigoCambioTO parIn) 
     */
	public CodigoCambioTO[] obtenerCodigosDeCambio(ConsultaCodigoCambioTO parIn)
	throws TtffException, RemoteException;

	/**
     * @see TransferenciaDeFondosFacadeBean#obtenerFechaValuta(String parIn) 
     */
	public ValutaTO[] obtenerFechaValuta(String parIn)
	throws TtffException, RemoteException;

	/**
     * @see TransferenciaDeFondosFacadeBean#consultarHorarioAprobacion(int parIn) 
     */
	public RespuestaHorarioTO consultarHorarioAprobacion(int parIn)
	throws TtffException, RemoteException;

	/**
     * @see TransferenciaDeFondosFacadeBean#consultarHorarioIngreso() 
     */
	public void consultarHorarioIngreso()
	throws TtffException, RemoteException;

	/**
     * @see TransferenciaDeFondosFacadeBean#actualizarTransferenciaMonex(TransferenciaMonexTO parIn) 
     */
	public RespuestaActualizaTtffTO actualizarTransferenciaMonex(TransferenciaMonexTO parIn)
	throws TtffException, RemoteException;

	/**
     * @see TransferenciaDeFondosFacadeBean#cambiarEstadoTtffMonex(CambiaEstadoTttfMonexTO parIn) 
     */
	public String cambiarEstadoTtffMonex(CambiaEstadoTttfMonexTO parIn)
	throws TtffException, RemoteException;

	/**
     * @see TransferenciaDeFondosFacadeBean#obtenerMensajeSwift(int parIn) 
     */
	public String[] obtenerMensajeSwift(int parIn)
	throws TtffException , RemoteException;

	/**
     * @see TransferenciaDeFondosFacadeBean#obtenerLiquidacion(int parIn) 
     */
	public String[] obtenerLiquidacion(int parIn)
	throws TtffException, RemoteException;

	/**
     * @see TransferenciaDeFondosFacadeBean#obtenerTransferencias(ConsultaTransferenciaTO parIn) 
     */
	public Transferencia1a1TO[] obtenerTransferencias(ConsultaTransferenciaTO parIn)
	throws TtffException, RemoteException;
	
	/**
     * @see TransferenciaDeFondosFacadeBean#obtenerTransferenciasPenRipple(ConsultaTransferenciaTO parIn) 
     */
	public Transferencia1a1TO[] obtenerTransferenciasPenRipple(ConsultaTransferenciaTO parIn)
	throws TtffException, RemoteException;
	
	/**
     * @see TransferenciaDeFondosFacadeBean#consultarDetallePago(ConsultaDetPagoTO parIn) 
     */
	public DetallePago1a1TO consultarDetallePago(ConsultaDetPagoTO parIn)
	throws TtffException, RemoteException;

	/**
     * @see TransferenciaDeFondosFacadeBean#consultarDetallePagoRipple(ConsultaDetPagoTO parIn) 
     */
	public DetallePago1a1TO consultarDetallePagoRipple(ConsultaDetPagoTO parIn)
	throws TtffException, RemoteException;

	/**
     * @see TransferenciaDeFondosFacadeBean#obtenerBeneficiariosAutorizados(ConsultaBeneficiariosTO parIn) 
     */
	public Beneficiario1a1TO[] obtenerBeneficiariosAutorizados(ConsultaBeneficiariosTO parIn)
	throws TtffException, RemoteException;

	/**
     * @see TransferenciaDeFondosFacadeBean#obtenerDetalleDelBeneficiario(ConsultaBeneficiariosTO parIn) 
     */
	public DetalleBeneficiarioTO obtenerDetalleDelBeneficiario(ConsultaDetBeneficiarioTO parIn)
	throws TtffException, RemoteException;

	/**
     * @see TransferenciaDeFondosFacadeBean#obtenerComisiones(int parIn) 
     */
	public ComisionTO obtenerComisiones(int parIn)
	throws TtffException, RemoteException;
	
	/**
     * @see TransferenciaDeFondosFacadeBean#crearBeneficiarioPendiente(BeneficiarioTO beneficiario) 
     */
	public BeneficiarioTO crearBeneficiarioPendiente(BeneficiarioTO beneficiario)
	throws TtffException, RemoteException;
	
	/**
     * @see TransferenciaDeFondosFacadeBean#eliminarBeneficiarioPendiente(DatosBeneficiarioTO datos) 
     */
	public DatosBeneficiarioTO eliminarBeneficiarioPendiente(DatosBeneficiarioTO datos)
	throws TtffException, RemoteException;
	
	/**
     * @see TransferenciaDeFondosFacadeBean#consultarDatosBeneficiario(DatosBeneficiarioTO datos) 
     */
	public BeneficiarioTO consultarDatosBeneficiario(DatosBeneficiarioTO datos)
	throws TtffException, RemoteException;
	
	/**
     * @see TransferenciaDeFondosFacadeBean#listarBeneficiariosPendientes(BeneficiarioTO beneficiario) 
     */
	public BeneficiarioTO[] listarBeneficiariosPendientes(BeneficiarioTO beneficiario)
	throws TtffException, RemoteException;
	
	/**
     * @see TransferenciaDeFondosFacadeBean#actualizarBeneficiarioPendiente(BeneficiarioTO beneficiario) 
     */
	public BeneficiarioTO actualizarBeneficiarioPendiente(BeneficiarioTO beneficiario)
	throws TtffException, RemoteException;
	
	/**
     * @see TransferenciaDeFondosFacadeBean#autorizarBeneficiario(DatosBeneficiarioTO datos)
     */
	public DatosBeneficiarioTO autorizarBeneficiario(DatosBeneficiarioTO datos)
	throws TtffException, RemoteException;
	
	/**
     * @see TransferenciaDeFondosFacadeBean#obtenerMonedasRipple(ConsultaMonedaTO parIn) 
     */
	public MonedasPermitidasTO[] obtenerMonedasRipple(String bic)
	throws TtffException, RemoteException;
    
	/**
     * @see TransferenciaDeFondosFacadeBean#obtenerNodosRippleDisp() 
     */
	public NodosRippleTO[] obtenerNodosRippleDisp()
	throws TtffException, RemoteException;
	
	/**
     * @see TransferenciaDeFondosFacadeBean#obtenerBeneficiariosAutorizadosRipple(ConsultaBeneficiariosRippleTO parIn) 
     */
	public BeneficiarioRippleTO[] obtenerBeneficiariosAutorizadosRipple(ConsultaBeneficiariosRippleTO parIn)
	throws TtffException, RemoteException;
	
	/**
     * @see TransferenciaDeFondosFacadeBean#obtenerDetalleBeneficiarioRipple(ConsultaDetalleBeneficiarioRippleTO datos) 
     */
	public DetalleBeneficiarioRippleTO obtenerDetalleBeneficiarioRipple(ConsultaDetalleBeneficiarioRippleTO datos)
	throws TtffException, RemoteException;
	
	/**
     * @see TransferenciaDeFondosFacadeBean#obtenerComisionesRipple(ObtenerComisionTO obtenerComision) 
     */
	public wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.ComisionTO obtenerComisionesRipple(ObtenerComisionTO obtenerComision)
	throws TtffException, RemoteException;
	
	/**
     * @see TransferenciaDeFondosFacadeBean#registrarTransferenciaRipple(RegistrarTransferenciaTO regTransferencia) 
     */
	public RespuestaRegistroTransferenciaTO registrarTransferenciaRipple(RegistrarTransferenciaTO regTransferencia) 
    throws TtffException, RemoteException;
	
	/**
     * @see TransferenciaDeFondosFacadeBean#consultarLimiteHorarioIngreso() 
     */
	public RespuestaHorarioTO consultarLimiteHorarioIngreso() 
	throws TtffException, RemoteException;
	
	/**
     * @see TransferenciaDeFondosFacadeBean#firmarOEliminarTransferenciaRipple(FirmarEliminarTransferenciaRipple transferencia) 
     */
	public RespuestaSpTO firmarOEliminarTransferenciaRipple(FirmarEliminarTransferenciaRipple transferencia)
	throws TtffException, RemoteException;
	
	/**
     * @see TransferenciaDeFondosFacadeBean#debitarCuentaCorrienteMX(int correlativoFBP) 
     */
	public RespuestaSpTO debitarCuentaCorrienteMX(int correlativoFBP)
	throws TtffException, RemoteException;
	
	/**
     * @see TransferenciaDeFondosFacadeBean#consultarDatosAXCurrent(int correlativoFBP) 
     */
	public DatosxCurrentTO consultarDatosxCurrent (int correlativoFBP)
	throws TtffException, RemoteException;
	
	/**
     * @see TransferenciaDeFondosFacadeBean#registrarResulatadoxCurrent(String correlativoFBP, String resultado, UUID paymentId) 
     */
	public RespuestaSpTO registrarResulatadoxCurrent (String correlativoFBP, String resultado, String paymentId)
	throws TtffException, RemoteException;
}
