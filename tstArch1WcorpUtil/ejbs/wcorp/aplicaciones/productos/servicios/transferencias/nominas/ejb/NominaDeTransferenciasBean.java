package wcorp.aplicaciones.productos.servicios.transferencias.nominas.ejb;

import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import wcorp.aplicaciones.bancaempresas.administracion.supervisor.delegate.SupervisorDelegate;
import wcorp.aplicaciones.bancaempresas.administracion.supervisor.to.EmpresasDelConvenioTO;
import wcorp.aplicaciones.bancaempresas.administracion.supervisor.to.UsuariosDelConvenioEmpresasTO;
import wcorp.aplicaciones.bancaempresas.diccionariodecuentas.vo.CuentasInscritasVO;
import wcorp.aplicaciones.productos.servicios.pagos.to.FiltroTO;
import wcorp.aplicaciones.productos.servicios.transferencias.delegate.TransferenciasDeFondosDelegate;
import wcorp.aplicaciones.productos.servicios.transferencias.implementacion.TransferenciasDeFondos;
import wcorp.aplicaciones.productos.servicios.transferencias.implementacion.TransferenciasDeFondosHome;
import wcorp.aplicaciones.productos.servicios.transferencias.nominas.dao.NominaDeTransferenciasDAO;
import wcorp.aplicaciones.productos.servicios.transferencias.nominas.delegate.NominaDeTransferenciasDelegate;
import wcorp.aplicaciones.productos.servicios.transferencias.nominas.to.InstruccionTransferenciaTO;
import wcorp.aplicaciones.productos.servicios.transferencias.nominas.vo.DetalleTransferenciaVO;
import wcorp.aplicaciones.productos.servicios.transferencias.nominas.vo.EncabezadoTransferenciaVO;
import wcorp.aplicaciones.productos.servicios.transferencias.nominas.vo.EstadoFirmaVO;
import wcorp.aplicaciones.productos.servicios.transferencias.nominas.vo.FiltroConsultarTransferenciasVO;
import wcorp.aplicaciones.productos.servicios.transferencias.nominas.vo.InformacionCodigoActivacionVO;
import wcorp.aplicaciones.productos.servicios.transferencias.nominas.vo.TotalPorCuentaVO;
import wcorp.aplicaciones.productos.servicios.transferencias.nominas.vo.TransferenciasEnLineaVO;
import wcorp.aplicaciones.productos.servicios.transferencias.nominas.vo.TransferenciasVO;
import wcorp.aplicaciones.productos.servicios.transferencias.nominas.vo.UsuarioTransferenciaVO;
import wcorp.aplicaciones.productos.servicios.transferencias.to.CabeceraFirmaNominaPnolTO;
import wcorp.aplicaciones.productos.servicios.transferencias.to.NominaPnolTO;
import wcorp.aplicaciones.productos.servicios.transferencias.to.TransferenciaATercerosTO;
import wcorp.aplicaciones.productos.servicios.transferencias.util.GeneradorComprobanteTransferenciaDeFondos;
import wcorp.bprocess.autorizaryfirmar.delegate.AutorizarYFirmarDelegate;
import wcorp.bprocess.autorizaryfirmar.to.RespuestaCapacidadDeAutorizacionTO;
import wcorp.bprocess.bancaempresas.transferenciaslbtr.cargamasiva.vo.FiltroConsultaTO;
import wcorp.bprocess.bancaempresas.transferenciaslbtr.cargamasiva.vo.InfoDetalleNomina;
import wcorp.bprocess.bancaempresas.transferenciaslbtr.cargamasiva.vo.InformacionCargaMasiva;
import wcorp.bprocess.bancaempresas.transferenciaslbtr.cargamasiva.vo.TotalTO;
import wcorp.bprocess.cuentas.TransferenciaException;
import wcorp.bprocess.cuentas.vo.CCADirectaVO;
import wcorp.bprocess.cuentas.vo.TransferenciasRecibidasVO;
import wcorp.model.actores.Cliente;
import wcorp.model.seguridad.Canal;
import wcorp.serv.bciexpress.AutorizaPagoCtaCte;
import wcorp.serv.bciexpress.BCIExpress;
import wcorp.serv.bciexpress.BCIExpressException;
import wcorp.serv.bciexpress.BCIExpressHome;
import wcorp.serv.bciexpress.ConApoquehanFirmado;
import wcorp.serv.bciexpress.ListaDeteTransf;
import wcorp.serv.bciexpress.MontoMaximoYAutor;
import wcorp.serv.bciexpress.ObtenCtasCtes;
import wcorp.serv.bciexpress.ResultAutorizaPagoCuentaCorriente;
import wcorp.serv.bciexpress.ResultConsultaPerfilesUCEP;
import wcorp.serv.bciexpress.ResultConsultarApoderadosquehanFirmado;
import wcorp.serv.bciexpress.ResultMontoMaximoYAutorizado;
import wcorp.serv.bciexpress.ResultObtieneCtasCtes;
import wcorp.serv.bciexpress.ResultTransferenciaFondoCuentaCorriente;
import wcorp.serv.bciexpress.ResultValidarUsuarioporTipoFirma;
import wcorp.serv.bciexpress.TransfFondoCtaCte;
import wcorp.serv.bciexpress.delegate.BCIExpressDelegate;
import wcorp.serv.cuentas.SaldoCtaCte;
import wcorp.serv.pagos.ServiciosPagosDelegate;
import wcorp.util.EnhancedServiceLocator;
import wcorp.util.EnvioDeCorreo;
import wcorp.util.ErroresUtil;
import wcorp.util.FechasUtil;
import wcorp.util.GeneralException;
import wcorp.util.IntUtl;
import wcorp.util.StringUtil;
import wcorp.util.TablaValores;
import wcorp.util.excepciones.ExcepcionGeneral;
import wcorp.util.journal.Eventos;
import wcorp.util.journal.Journal;
import wcorp.util.mime.GeneradorDeComponentesMIME;

import cl.bci.mensajeria.sdp.conector.ConectorServicioEnvioSDP;

/**
 * <b>NominaDeTransferenciasBean</b>
 * <p>
 * Servicio EJB que contiene los m�todos necesarios para el manejo de las N�minas
 * de Transferencia de Fondos.
 *
 * Clase utilizada en el servicio Transferencia de Fondos del m�dulo de Pagos de
 * Banca Empresas/Empresarios.
 * </p>
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 25/04/2009 Alejandro Barra (SEnTRA) : versi�n inicial.</li>
 * <li>1.1 04/03/2010 Marcelo Fuentes H.(SEnTRA): Se modifica los m�todos:
 *                     {@link # extraeTransferenciasRealizadas()}, {@link # consultarEventosParaTransferencias()} </li>
 * <li> 1.2 08/06/2010, Denisse Vel�squez S. (SEnTRA): Se agrega el m�todo: {@link # registraEvento()}</li>
 * <li> 1.3 05/08/2010, H�ctor Hern�ndez Orrego. (SEnTRA): Se agrega el m�todo: {@link # cambiaGlosaTransferencia()}</li>
 * <li> 1.4 09/09/2010, Marcelo Fuentes H. (SEnTRA): Se agrega el m�todo {@link #extraePagosRealizadosViaCargaMasiva()}.</li>
 * <li> 1.5 28/06/2011, Denisse Vel�squez S. (SEnTRA): Se agrega el m�todo {@link #extraeTransfRecibidasEmpBCIEmpBCI()}.</li>
 * 
 * <li> 2.0 02/08/2011, Pedro Carmona Escobar (SEnTRA) : Se agregan los m�todos:<ul>
 *                      <li>{@link #extraeTransferenciasPendientesPorNumero()}</li>
 *                      <li>{@link #extraeCodigodDeActivacion()}</li>
 *                      <li>{@link #extraeCuentasNoInscritas()}</li>
 *                      <li>{@link #validarCodigoActivacion()}</li>
 *                      <li>{@link #enviarCodigoActivacion()}</li></ul>
 *                      Adem�s, se agregan las constantes ARCHIVO_PARAMETROS y  CONSULTA_SIN_FILTRO.</li>
 * <li> 2.1 23/08/2011, Freddy Painenao - Freddy Monsalve (Os.One): Se agrega el m�todo {@link #extraeTransferenciasPendientesPorResolver()}.</li>
 * <li> 2.2 14/06/2012, Jaime Pezoa N��ez (BCI): Se elimina activaci�n de SMS del m�todo {@link #enviarCodigoActivacion()}.</li>
 * <li> 2.3 16/01/2012, Iv�n L�pez B. (ADA Ltda.): Se agregan m�todos extraeTransferenciasPendientesATerceros,
 *                                                 extraeTransferenciasPendientesEPC y obtenerDetalleTransferencia.</li>
 * <li> 2.4 08/03/2013, Sergio Cuevas D. (SEnTRA): Se agregan los m�todos:<ul>
 *                      <li>{@link #eliminaDetalleCuentasNoInscritas()}</li></ul>
 * <li> 2.5 22/04/2013 Rodrigo Arce Campos (SEnTRA) : Se agregan los m�todos
 *          <li>{@link #obtenerInstruccionesTransferenciaSupervisor()}</li>
 *          <li>{@link #enviarInstruccionSupervisor(InstruccionTransferenciaTO)}</li>
 *          <li>{@link #eliminarInstruccionTransferenciaSupervisor(long, int, String)}</li>
 *          <li>{@link #insertarAccionBitacoraInstruccionesTransferencia(InstruccionTransferenciaTO)}</li>
 *          <li>{@link #extraeTransferenciasPendientesCTALBT(FiltroConsultarTransferenciasVO)}</li>
 * <li> 2.6 04/02/2013 Venancio Ar�nguiz P. (SEnTRA): Se agrega el m�todo 
 *                      {@link #consultarExisteTransferencia()} </li>
 * <li> 2.5 22/01/2014 Venancio Ar�nguiz P. (SEnTRA): Se agrega el m�todo {@link #obtenerTiempoEsperaNominaTTFF()},
 *          que obtiene los tiempos de espera y posici�n en cola de procesamiento de las n�minas que se 
 *          encuentran con estado firmada.</li>
 * <br><br>                
 * <li> 2.7 11/02/2014 Jaime Suazo D. (Experimento Social. SPA.): Debido al proyecto de firmas de n�minas PNOL, y de acuerdo
 *          a lo solicitado por Arquitectura aplicativa, es que se migran desde 
 *          {@link wcorp.aplicaciones.productos.servicios.transferencias.nominas.struts.helper.TransferenciasHelper}}, 
 *          los siguientes m�todos<br>
 *          <br>
 *          {@link #validaMontosYTopesEnLinea(String, double, TransferenciasVO)}, <br>
 *          {@link #firmarTransferenciasPNOL(String, String, String, String, String, String[], int, String)}, <br>
 *          {@link #creaFiltro(String, String, String, String)}, <br>
 *          {@link #autorizaPagoFyP(String, String, char, String, char, String)}, <br>
 *          {@link #validaMontosYTopesEnLinea(String, double, TransferenciasVO)}, <br>
 *          {@link #montoMaximoYAutorizado(String, String, char, String, char, String, int, String, double)}, <br>
 *          {@link #rescatarProducto(String, String, String)}, <br>
 *          {@link #consultaTipoApoderado(String, String, String, String, String)}, <br>
 *          {@link #obtieneCuentasySaldo(ResultObtieneCtasCtes)} <br>
 *          <br><br>
 *          Adem�s, tambi�n se migra l�gica desde,
 *          {@link wcorp.aplicaciones.productos.servicios.transferencias.nominas.struts.actions.FirmarTransferenciaAction.firmaTransferencia(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)}}<br><br> 
 *          a los siguientes m�todos<br><br>
 *          {@link #firmarNominasPNOL(NominaPnolTO[], CabeceraFirmaNominaPnolTO)}<br>
 *          {@link #obtenerModoFirmaEnFyP(String, String, String, String, String, String, String, String)}<br>
 *          {@link #esApoderado(String)}<br>
 *          {@link #esApoderadoAutorizado(String, String)}<br>
 *          {@link #validarDisponibleDeCtaCargoNominaConMontoNomina(String, EncabezadoTransferenciaVO[], 
 *          String[], CabeceraFirmaNominaPnolTO)}<br>
 * <li> 2.8 04/11/2014 Angel Crisostomo (ImageMaker): Se agregan los m�todos 
 *                      {@link #obtenerEncabezadoDeNomina(InformacionCargaMasiva)} <br>
 *                      {@link #actualizarEstadoNominaEnProceso(InformacionCargaMasiva)} <br>
 *                      {@link #obtenerPagosNomina(InformacionCargaMasiva)} <br>
 *                      {@link #obtenerTotalesNomina(InformacionCargaMasiva)} <br>
 *                      {@link #obtieneNominasEnLineaPendientesDeFirma(long, String, Date, Date)} <br>
 *                      {@link #obtenerEncabezadoDeNominaEnProceso(InformacionCargaMasiva)} <br>
 * <li> 2.9 13/11/2014 Angel Crisostomo (ImageMaker): Se agregan los m�todos      
 *                      {@link #obtenerEncabezadoPagosRealizados(FiltroConsultarTransferenciasVO, String, String)}
 *                      <br>
 *                      {@link #obtenerDetallePagosRealizados(FiltroConsultarTransferenciasVO, String, String)
 * <li> 3.0 30/10/2014 Sergio Cuevas Diaz (SEnTRA): Se agregan los m�todos
 *                      {@link #obtenerResumenNominas(FiltroConsultaTO)}<br>
 *                      {@link #consultarNominasConProblemasPorFiltro(FiltroConsultaTO)}<br>
 *                      {@link #consultarNominasEnProcesoPorFiltro(FiltroConsultaTO)}<br>
 * </li>
 * <li> 3.1 27/11/2014 Angel Crisostomo (ImageMaker): Se agrega el m�todo
 *                      {@link #insertarEncabezadoTransferencia(TransferenciaATercerosTO transferencia)}<br>
 * </li>
 * <li> 3.2 09/12/2014 Sergio Cuevas Diaz (SEnTRA): Se agrega m�todo
 *                      {@link #obtieneTransferenciasDePagoRecibidas(FiltroTO)}<br>
 * <li> 3.3 18/12/2014 Pablo Romero C�ceres (SEnTRA): Se agregan los m�todos 
 *                      {@link #realizaTransferenciaDeFondos(TransferenciaATercerosTO, Cliente)}<br>
 *                      {@link #crearEJBTransferenciasDeFondos()}<br>
 *                      {@link #crearEJBBCIExpress()}<br>
 *                      {@link #enviarCorreoTransferencia(TransferenciaATercerosTO, Cliente)}<br>
 * <li>3.4 26/01/2015 Sergio Cuevas D�az (Sentra): Se encapsulan parametros de la firma del m�todo
 *                      {@link #obtieneNominasEnLineaPendientesDeFirma(FiltroConsultaTO)}.</li>
 * <li>3.5 09/02/2015 Jorge G�mez O. (Sentra): Se modifica el m�todo
 *                      {@link #realizaTransferenciaDeFondos(TransferenciaATercerosTO, Cliente)}.</li>
 *        
 * <li> 3.6 29/12/2014 Rafael Pizarro (TINet): se normaliza log para ajustarse a Normativa Log4j de BCI, para esto
 *              se modifican los siguientes m�todos:
 *              {@link #validaMontosYTopesEnLinea(String, double, TransferenciasVO)},
 *              {@link #creaFiltro(String, String, String, String)},
 *              {@link #montoMaximoYAutorizado(String, String, char, String, char, String, int, String, double)},
 *              {@link #obtieneCuentasySaldo(ResultObtieneCtasCtes)}
 *<li> 3.7 01/04/2015 Christian Saravia J. (SEnTRA) - Lucy Velasquez Cea (IS BCI): Se normaliza logueo en toda la clase.</li>
 * <li>3.8 02/12/2015 Gonzalo Cofre Guzman (Sentra) - Daniel Araya (Ing de Soft BCI): Se modifica el m�todo {@link #realizaTransferenciaDeFondos(TransferenciaATercerosTO, Cliente)}.
 * con la finalidad de realizar el envio de correo a otros bancos. 
 * <li>3.9 28/12/2015 Jaime Gaete (SEnTRA) - Jorge Lara D�az (ing. Soft. BCI): Se agrega m�todo 
 *      {@link #caducarNominaDeTransferencia(FiltroTO)} y 
 * {@link #revisarCaducidadDeTransferencia(TransferenciaATercerosTO, String, String)}</li>
 * <li>3.10 11/02/2016 Pablo Romero C�ceres (SEnTRA) - Jorge Lara D�az (Ing. Soft. BCI): Se modifican los m�todos {@link #realizaTransferenciaDeFondos(TransferenciaATercerosTO, Cliente)} 
 * y {@link #enviarCorreoTransferencia(TransferenciaATercerosTO)}</li>
 * <li>3.11 26/02/2016 Gonzalo Cofre Guzman (Sentra) - Jorge Lara (Ing de Soft BCI): Se modifica el m�todo {@link #realizaTransferenciaDeFondos(TransferenciaATercerosTO, Cliente)}.
 * <li>3.12 08/03/2016 Pablo Romero C�ceres (SEnTRA) - Iv�n S�nchez (Ing. Soft. BCI): Se modifica el m�todo {@link #realizaTransferenciaDeFondos(TransferenciaATercerosTO, Cliente)}.
 * <li>3.13 12/04/2016 Jaime Gaete L. (SEnTRA) - Jorge Lara D�az (Ing. Soft. BCI): Se agrega m�todo 
 * {@link #consultarPosiblesTransferenciasDuplicadas(TransferenciaATercerosTO, FiltroTO, String, String)}. Se corrige documentaci�n de atributos
 * de la clase y su orden, para correcci�n de Checkstyle BCI.</li>
 * <li>3.14 17/05/2016 Jaime Gaete L. (SEnTRA) - Paola Avalos (Ing. Soft. BCI): Se agrega m�todo 
 * {@link #consultarPosiblesNominasDuplicadas(InformacionCargaMasiva, FiltroTO, String, String)}.</li>
 * <li>3.15 23/06/2016 Sergio Bustos B. (IMIT) - Marcelo Avenda�o (BCI): Se modifica m�todo 
 * {@link #realizaTransferenciaDeFondos(TransferenciaATercerosTO, Cliente)}
 * para controlar exception timeOut de CCA.</li>
 * <li>3.16 05/06/2016 Jaime Gaete L. (SEnTRA) - Daniel Araya (Ing. Soft. BCI): Se agrega m�todo 
 * {@link #realizarTransferenciaDeFondosDetalleNomina(InfoDetalleNomina, Cliente)} para realizar transferencia de fondos para un detalle de n�mina.
 * </li>
 * <li>3.17 15/11/2016 Jaime Gaete L. (SEnTRA) - Daniel Araya (Ing. Soft. BCI): Se modifica m�todo 
 * {@link #realizarTransferenciaDeFondosDetalleNomina(InfoDetalleNomina, Cliente)} para agregar correo de destinatario.
 * </li>
 *  <li>3.18 05/04/2017 Mauricio Retamal C. (SEnTRA) - Daniel Araya (Ing. Soft. BCI): Se agregan los m�todos {@link #consultarOperacionesCVD(FiltroConsultaTO)} 
 *                                                      y {@link #cambiaEstadoProceso(String)}.</li>
 * <li>3.19 26/10/2018 Bastian Nelson G. (Sermaluc) - Gonzalo Mu�oz (Ing. Soft. BCI): se agrega el siguiente metodo:{@link #eliminarNumeroDeTransferencia}
 *           
 * </ul>
 * <p>
 * <b>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</b>
 * <p>
 */
public class NominaDeTransferenciasBean implements SessionBean {

    /**
     * Cantidad de meses de consulta hacia atras.
     */
    public static final int MESES_ANTERIORES_CONSULTA = -6;

    /**
     * Posici�n de comienzo de la cuenta.
     */
    public static final int POSICION_COMIENZO_CUENTA = 4;

    /**
     * Largo del c�digo de la CCA.
     */
    private static final int LARGO_CODIGO_CCA = 2;
    
    /**
     * Archivo de Par�metros para Transferencia de Fondos.
     */
    private static String ARCHIVO_PARAMETROS = "TransferenciaDeFondosEmpresa.parametros";   

    /**
     * C�digo que indica que no se debe realizar filtro para la consulta de alertas contradas de un cliente.
     */

    private static final String CONSULTA_SIN_FILTRO = "-1";

    /**
     * Estado Pendiente.
     */
    private static final String PENDIENTE       = "PEN";

    /**
     * Estado Firmada.
     */
    private static final String FIRMADA         = "FIR";

    /**
     * Estado Error.
     */
    private static final String ERROR           = "ERR";

    /**
     * Estado Pago por Firmar.
     */
    private static final String PAGO_POR_FIRMAR = "PPF";    

    /**
     * Archivo de Par�metros para Transferencia de Fondos Terceros.
     */
    private static String ARCH_PARAM_TRF_TER    = "TransferenciaTerceros.parametros";

    /**
     * Archivo de Par�metros para Empresas.
     */
    private static String ARCH_PARAM_EMP        = "bciexpress.parametros";

    /**
     * Tabla de par�metros de Error.
     */
    private static String TABLA_PARAMETROS_ERROR = "errores.codigos";    

    /**
     * Codigo producto Express.
     */
    private static String PRODUCTO_EXPRESS           = "Prod01";

    /**
     * Codigo producto EPyme.
     */
    private static String PRODUCTO_EPYME             = "Prod21";

    /**
     * Valor a indicar, para obtener la totalidad de registros.
     */
    private static final int TODAS_TRANSFERENCIAS = 0;

    /**
     * Valor a indicar, para no considerar datos paginados.
     */
    private static final int SIN_PAGINACION = 0;

    /**
     * Largo de la cuenta.
     */
    private static final int LARGO_CUENTA = 12;

    /**
     * Variable que contiene la tabla parametros.
     */
    private static final String ARCHIVO_ADMIN = "firmaPNOL.parametros";

    /**
     * Caracter de relleno para el RUT de empresas.
     */
    private static final char RELLENO_RUT_EMPRESAS = '0';

    /**
     * Largo RUT de empresas.
     */
    private static final int LARGO_RUT = 8;

    /**
     * Caracter de relleno para el convenio de empresas.
     */
    private static final char RELLENO_CONVENIO_EMPRESAS = ' ';

    /**
     * Largo convenio empresas.
     */
    private static final int LARGO_CONVENIO = 10;   

    /**
     * Delimitador de correo.
     */
    private static final int    DELIMITADOR_CORRREO       = 4;

    /**
     * Largo de cuenta de ocho digitos.
     */
    private static final int LARGO_CUENTA_OCHO = 8;    

    /**
     * Identificador del comprobante que ser� enviado al destinatario banco Bci.
     */
    private static final String ID_COMP_DEST_BCI = "COMPROBANTE_TTFF_DESTINATARIO_BANCO";

    /**
     * Identificador del comprobante que ser� enviado al destinatario otros bancos.
     */
    private static final String ID_COMP_DEST_OTRO_BANCO = "COMPROBANTE_TTFF_DESTINATARIO_OTRO_BANCO";

    /**
     *  N�mero que identifica al cana bci express empresa.
     */
    private static final Object CANAL_BCIEXPRESS_NEW = "230";

    /**
     * N�mero que identifica al canal BCINET para el env�o de correo.
     */
    private static final String CANAL_CORREO_BCI = "110";
    
    /**
     * Logueo de la Clase.
     */
    private transient Logger log = Logger.getLogger(NominaDeTransferenciasBean.class);

    /**
     * @see SessionBean#ejbActivate().
     */
    public void ejbActivate()                          throws EJBException,    RemoteException {}

    /**
     * @see SessionBean#ejbPassivate().
     */
    public void ejbPassivate()                         throws EJBException,    RemoteException {}

    /**
     * @see SessionBean#ejbRemove().
     */
    public void ejbRemove()                            throws EJBException,    RemoteException {}

    /**
     * @see SessionBean#setSessionContext().
     */
    public void setSessionContext(SessionContext arg0) throws EJBException,    RemoteException {}

    /**
     * @see SessionBean#ejbCreate().
     */
    public void ejbCreate()                            throws CreateException, RemoteException {}

    /**
     * <p>M�todo que permite agregar una Transferencia para su posterior aprobaci�n y firma</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 19/10/2009 Alejandro Barra (SEnTRA): Versi�n inicial.
     *
     * </ul>
     *
     * @param rutEmpresa rut de la empresa
     * @param dvEmpresa d�gito verificador de la empresa
     * @param convenio n�mero de convenio
     * @param rutUsu rut del usuario que realiza la operaci�n
     * @param dvUsu digito verificador del usuario que realiza la operaci�n
     * @param transferencia con los datos de la transferencia a ingresar
     * @throws Exception
     * @return Listado de transferencias en linea
     * @since 1.0
     */
    public TransferenciasEnLineaVO[] agregarTransferencia (String rutEmpresa, String dvEmpresa, String convenio, String rutUsu, String dvUsu, TransferenciasEnLineaVO transferencia) throws Exception{

        try{
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[agregarTransferencia][" + rutEmpresa + "]["+ convenio +"][BCI_INI]" );
                getLogger().info("[agregarTransferencia][" + rutEmpresa + "]["+ convenio +"] rutUsu [" + rutUsu + "] - ["+ dvUsu +"], transferencia [" + transferencia + "]");
            }
            NominaDeTransferenciasDAO dao = new NominaDeTransferenciasDAO();
            if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[agregarTransferencia][" + rutEmpresa + "]["+ convenio +"][BCI_FINOK]");}
            return dao.agregarTransferencia(rutEmpresa, dvEmpresa, convenio, rutUsu, dvUsu, transferencia);
        }catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) { getLogger().error("[agregarTransferencia][" + rutEmpresa + "]["+ convenio +"][BCI_FINEX][Exception e]" + e.getMessage());}
            throw e;
        }
    }

    /**
     * <p>M�todo que obtiene un listado de Transferencias frecuentes pertenecientes
     * a un grupo dado</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 19/10/2009 Alejandro Barra (SEnTRA): Versi�n inicial.
     *
     * </ul>
     *
     * @param convenio n�mero de convenio
     * @param rutEmpresa rut de la empresa
     * @param nombreGrupo nombre del grupo de transferencia
     * @throws Exception
     * @return Listado con las transferencias pertenecientes al grupo.
     * @since 1.0
     */
    public TransferenciasEnLineaVO[] obtieneGruposFrecuentes(String convenio, String rutEmpresa, String nombreGrupo) throws Exception{

        try{
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[obtieneGruposFrecuentes][" + rutEmpresa + "]["+ convenio +"][BCI_INI]" );
                getLogger().info("[obtieneGruposFrecuentes][" + rutEmpresa + "]["+ convenio +"] nombreGrupo ["+ nombreGrupo +"]");
            }
            NominaDeTransferenciasDAO dao = new NominaDeTransferenciasDAO();
            if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[obtieneGruposFrecuentes][" + rutEmpresa + "]["+ convenio +"][BCI_FINOK]");}
            return dao.obtieneGruposFrecuentes(convenio, rutEmpresa, nombreGrupo);
        }catch (Exception e) {
            if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[obtieneGruposFrecuentes][" + rutEmpresa + "]["+ convenio +"][BCI_FINEX][(Exception e]" + e.getMessage());}
            throw e;
        }

    }

    /**
     * <p>M�todo que permite eliminar el detalle de una Transferencia</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 19/10/2009 Alejandro Barra (SEnTRA): Versi�n inicial.
     *
     * </ul>
     *
     * @param rutEmpresa rut de la empresa
     * @param convenio n�mero de convenio
     * @param numTransferencia n�mero de la transferencia
     * @param idDetalle identificador del detalle de la transferencia
     * @throws Exception
     * @return Respuesta de la eliminaci�n
     * @since 1.0cambioaboolean
     */
    public boolean eliminarTransferencia(String rutEmpresa, String convenio, String numTransferencia,int  idDetalle) throws Exception{
        try{
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[eliminarTransferencia][" + numTransferencia + "][BCI_INI]" );
                getLogger().info("[eliminarTransferencia][" + numTransferencia + "] rutEmpresa ["+ rutEmpresa +"], convenio ["+ convenio +"], idDetalle ["+ idDetalle +"]");
            }
            NominaDeTransferenciasDAO dao = new NominaDeTransferenciasDAO();
            if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[eliminarTransferencia][" + numTransferencia + "][BCI_FINOK]");}
            return dao.eliminarTransferencia(rutEmpresa, convenio, numTransferencia, idDetalle);
        }catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) { getLogger().error("[eliminarTransferencia][" + numTransferencia + "][BCI_FINEX][Exception e]" + e.getMessage());}
            throw e;
        }

    }

    /**
     * <p>M�todo que permite crear un Grupo de Transferencias Frecuentes</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 19/10/2009 Alejandro Barra (SEnTRA): Versi�n inicial.
     *
     * </ul>
     *
     * @param convenio n�mero de convenio
     * @param rutEmpresa rut de la empresa
     * @param nombreGrupo nombre del grupo de transferencias
     * @param numTransferencia n�mero de la transferencia
     * @param rutUsu rut del usuario que realiza la creaci�n del grupo
     * @param ipUsuario ip del usuario que realiza la creaci�n del grupo
     * @throws Exception
     * @return Respuesta de la creaci�n.
     * @since 1.0
     */
    public boolean crearGrupo(String convenio, String rutEmpresa, String nombreGrupo, String numTransferencia, String rutUsuario, String ipUsuario) throws Exception{

        try{
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[crearGrupo]c[BCI_INI]" );
                getLogger().info("[crearGrupo]["+ numTransferencia +"] nombreGrupo ["+ nombreGrupo +"], rutEmpresa ["+ rutEmpresa +"], convenio ["+ convenio +"] rutUsuario ["+ rutUsuario +"], ipUsuario ["+ ipUsuario +"]");
            }
            NominaDeTransferenciasDAO dao = new NominaDeTransferenciasDAO();
            if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[crearGrupo]["+ numTransferencia +"][BCI_FINOK]");}
            return dao.crearGrupo(convenio, rutEmpresa, nombreGrupo, numTransferencia, rutUsuario, ipUsuario);
        }catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) { getLogger().error("[crearGrupo]["+ numTransferencia +"][BCI_FINEX][Exception e]" + e.getMessage());}
            throw e;
        }

    }

    /**
     * <p>M�todo que cambia el estado de un detalle de una Transferencia</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 19/10/2009 Alejandro Barra (SEnTRA): Versi�n inicial.
     * <li>1.1 24/03/2010 Marcelo Fuentes (SEnTRA): Se agrega nuevo par�metro codigoRechazo que ser� utilizado para guardar el motivo por el 
     *                                              cual no se pudo realizar una transferencia o "0000" en el resto de los casos.
     *
     * </ul>
     *
     * @param rutUsuario rut del usuario que realiza la modificaci�n
     * @param dvUsuario d�gito verificador del usuario que realiza la modificaci�n
     * @param convenio n�mero de convenio
     * @param rutEmpresa rut de la empresa
     * @param dvEmpresa d�gito verificador del rut de la empresa
     * @param numTransferencia n�mero de la transferencia
     * @param detalleTransferencia n�mero del detalle de la transferencia
     * @param estado el estado en el que se quiere dejar la cuenta inscrita
     * @param codigoRechazo el motivo por el cual se rechaz� la transferencia
     * @param encabezadoODetalle indicador de si el cambio de estado es a un detalle o a un encabezado de una transferencia
     * @throws Exception
     * @return Respuesta de la modificaci�n.
     * @since 1.0
     */
    public boolean cambiaEstadoTransferencia(String rutUsuario, String dvUsuario, String convenio, String rutEmpresa, String dvEmpresa, String numTransferencia, int detalleTransferencia, String estado, String codigoRechazo, String encabezadoODetalle) throws Exception{
        try{
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[cambiaEstadoTransferencia][" + numTransferencia + "][BCI_INI]" );
                getLogger().info("[cambiaEstadoTransferencia][" + numTransferencia + "] rutUsuario ["+ rutUsuario +"] - ["+ dvUsuario +"], convenio ["+ convenio +"], rutEmpresa ["+ rutEmpresa +"] - ["+ dvEmpresa +"], detalleTransferencia ["+ detalleTransferencia +"]");
                getLogger().info("[cambiaEstadoTransferencia][" + numTransferencia + "] estado ["+ estado +"], codigoRechazo ["+ codigoRechazo +"], encabezadoODetalle ["+ encabezadoODetalle +"]");
            }
            NominaDeTransferenciasDAO dao = new NominaDeTransferenciasDAO();
            if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[cambiaEstadoTransferencia][" + numTransferencia + "][BCI_FINOK]");}
            return dao.cambiaEstadoTransferencia(rutUsuario, dvUsuario, convenio, rutEmpresa, dvEmpresa, numTransferencia, detalleTransferencia, estado, codigoRechazo, encabezadoODetalle);
        }catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) { getLogger().error("[cambiaEstadoTransferencia][" + numTransferencia + "][BCI_FINEX][Exception e]" + e.getMessage());}
            throw e;
        }
    }

    /**
     * <p>M�todo que elimina un grupo de Transferencias</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 19/10/2009 Alejandro Barra (SEnTRA): Versi�n inicial.
     *
     * </ul>
     *
     * @param convenio n�mero de convenio
     * @param rutEmpresa rut de la empresa
     * @param nombreGrupo nombre del grupo de transferencias
     * @throws Exception
     * @return Respuesta de la eliminaci�n.
     * @since 1.0
     */
    public boolean eliminarGrupoDeLaTransferencia(String convenio, String rutEmpresa, String nombreGrupo) throws Exception{
        try{
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[eliminarGrupoDeLaTransferencia][" + rutEmpresa + "]["+ convenio +"][BCI_INI]" );
                getLogger().info("[eliminarGrupoDeLaTransferencia][" + rutEmpresa + "]["+ convenio +"] nombreGrupo ["+ nombreGrupo +"]");
            }
            NominaDeTransferenciasDAO dao = new NominaDeTransferenciasDAO();
            if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[eliminarGrupoDeLaTransferencia][" + rutEmpresa + "]["+ convenio +"]][BCI_FINOK]");}
            return dao.eliminarGrupoDeLaTransferencia(convenio, rutEmpresa, nombreGrupo);
        }catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) { getLogger().error("[eliminarGrupoDeLaTransferencia][" + rutEmpresa + "]["+ convenio +"][BCI_FINEX][Exception e]" + e.getMessage());}
            throw e;
        }
    }

    /**
     * <p>M�todo que modifica los datos asociados a un grupo de Transferencias</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 19/10/2009 Alejandro Barra (SEnTRA): Versi�n inicial.
     *
     * </ul>
     *
     * @param convenio n�mero de convenio
     * @param rutEmpresa rut de la empresa
     * @param nombreGrupo nombre del grupo de transferencias
     * @param numTransferencia n�mero de la transferencia
     * @param rutUsu rut del usuario que realiza la modificaci�n
     * @param ipUsuario ip del usuario que realiza la modificaci�n
     * @throws Exception
     * @return Respuesta de la modificaci�n.
     * @since 1.0
     */
    public boolean modificaGrupoDeLaTransferencia(String convenio, String rutEmpresa, String nombreGrupo, String numTransferencia, String rutUsu, String ipUsuario) throws Exception{
        try{
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("modificaGrupoDeLaTransferencia][" + rutEmpresa + "][" + convenio + "][BCI_INI]" );
                getLogger().info("[modificaGrupoDeLaTransferencia][" + rutEmpresa + "][" + convenio + "] nombreGrupo ["+ nombreGrupo +"], numTransferencia ["+ numTransferencia +"], rutUsu ["+ rutUsu +"], ipUsuario ["+ ipUsuario +"]");
            }
            NominaDeTransferenciasDAO dao = new NominaDeTransferenciasDAO();
            if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[modificaGrupoDeLaTransferencia][" + rutEmpresa + "][" + convenio + "][BCI_FINOK]");}
            return dao.modificaGrupoDeLaTransferencia(convenio, rutEmpresa, nombreGrupo, numTransferencia, rutUsu, ipUsuario);
        }catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) { getLogger().error("[modificaGrupoDeLaTransferencia][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][Exception e]" + e.getMessage());}
            throw e;
        }
    }

    /**
     * <p>M�todo que modifica un detalle de Transferencia.</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 19/10/2009 Alejandro Barra (SEnTRA): Versi�n inicial.
     *
     * </ul>
     *
     * @param convenio n�mero de convenio
     * @param rutEmpresa rut de la empresa
     * @param detalleVO los datos del detalle de la transferencia a modificar
     * @throws Exception
     * @return Respuesta de la modificaci�n.
     * @since 1.0
     */
    public boolean modificaDetalleTransferencia(String convenio, String rutEmpresa, DetalleTransferenciaVO detalleVO) throws Exception{
    	String numTransferencia = detalleVO.getNumeroTransferencia();
        try{
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[modificaDetalleTransferencia][" + numTransferencia +"][BCI_INI]" );
                getLogger().info("[modificaDetalleTransferencia][" + numTransferencia +"] convenio ["+ convenio +"], rutEmpresa ["+ rutEmpresa +"], detalleVO ["+ detalleVO.toString() +"]");
            }
            NominaDeTransferenciasDAO dao = new NominaDeTransferenciasDAO();
            if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[modificaDetalleTransferencia][" + numTransferencia +"][BCI_FINOK]");}
            return dao.modificaDetalleTransferencia(convenio, rutEmpresa, detalleVO);
        }catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) { getLogger().error("[modificaDetalleTransferencia][" + numTransferencia +"][BCI_FINEX][Exception e]" + e.getMessage());}
            throw e;
        }

    }

    /**
     * <p>M�todo que obtiene un listado de transferencias realizadas, consultando seg�n
     * ciertos criterios.</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 19/10/2009 Alejandro Barra (SEnTRA): Versi�n inicial.
     *
     * </ul>
     *
     * @param filtro los filtros a aplicar a la consulta
     * @param bloque la p�gina a consultar
     * @param cantidadRegistros la cantidad de registros por cada p�gina
     * @throws Exception
     * @return Listado con las transferencias realizadas.
     * @since 1.0
     */
    public DetalleTransferenciaVO[] extraeTransferenciasRealizadas(FiltroConsultarTransferenciasVO filtro, int bloque, int cantidadRegistros) throws Exception{
        
    	 String rutEmpresa = filtro.getRutOrigen();
         String convenio = filtro.getConvenio();
        try{
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[extraeTransferenciasRealizadas][" + rutEmpresa + "][" + convenio + "][BCI_INI]" );
                getLogger().info("[extraeTransferenciasRealizadas][" + rutEmpresa + "][" + convenio + "] bloque ["+ bloque +"], cantidadRegistros ["+ cantidadRegistros +"], filtro ["+ filtro.toString() +"]");
            }
            NominaDeTransferenciasDAO dao = new NominaDeTransferenciasDAO();
            if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[extraeTransferenciasRealizadas][" + rutEmpresa + "][" + convenio + "][BCI_FINOK]");}
            return dao.extraeTransferenciasRealizadas(filtro, bloque, cantidadRegistros);
        }catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) { getLogger().error("[extraeTransferenciasRealizadas][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][Exception e]" + e.getMessage());}
            throw e;
        }
    }

    /**
     * <p>M�todo que obtiene los montos totales agrupados por cuenta para un listado de
     * transferencias realizadas, obtenidas seg�n ciertos criterios.</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 19/10/2009 Alejandro Barra (SEnTRA): Versi�n inicial.
     *
     * </ul>
     *
     * @param filtro los filtros a aplicar a la consulta
     * @throws Exception
     * @return Listado con los montos totales de una trasferencia agrupados por cuenta.
     * @since 1.0
     */
    public TotalPorCuentaVO[] extraeTotalesPorCuentaDeTransferenciasRealizadas(FiltroConsultarTransferenciasVO filtro) throws Exception{
        
    	String rutEmpresa = filtro.getRutOrigen();
        String convenio = filtro.getConvenio();
        try{
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[extraeTotalesPorCuentaDeTransferenciasRealizadas][" + rutEmpresa + "][" + convenio + "][BCI_INI]" );
                getLogger().info("[extraeTotalesPorCuentaDeTransferenciasRealizadas][" + rutEmpresa + "][" + convenio + "] filtro ["+ filtro.toString() +"]");
            }
            NominaDeTransferenciasDAO dao = new NominaDeTransferenciasDAO();
            if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[extraeTotalesPorCuentaDeTransferenciasRealizadas][" + rutEmpresa + "][" + convenio + "][BCI_FINOK]");}
            return dao.extraeTotalesPorCuentaDeTransferenciasRealizadas(filtro);
        }catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) { getLogger().error("[extraeTotalesPorCuentaDeTransferenciasRealizadas][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][Exception e]" + e.getMessage());}
            throw e;
        }
    }

    /**
     * <p>M�todo que obtiene un listado de los detalles asociados a una transferencia
     * pendiente.</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 19/10/2009 Alejandro Barra (SEnTRA): Versi�n inicial.
     *
     * </ul>
     *
     * @param rutEmpresa rut de la empresa
     * @param convenio n�mero de convenio
     * @param numeroTransferencia n�mero de transferencia
     * @param bloque la p�gina a consultar
     * @param cantidadRegistros la cantidad de registros por cada p�gina
     * @throws Exception
     * @return Listado con los detalles de la transferencia a consultar.
     * @since 1.0
     */
    public TransferenciasVO[] extraeDetallesTransferenciaPendiente(String rutEmpresa, String convenio, String  numeroTransferencia, int bloque, int cantidadRegistros) throws Exception{
        try{
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[extraeDetallesTransferenciaPendiente][" + numeroTransferencia + "][BCI_INI]" );
                getLogger().info("[extraeDetallesTransferenciaPendiente][" + numeroTransferencia + "] rutEmpresa ["+ rutEmpresa +"], convenio ["+ convenio +"], bloque ["+ bloque +"], cantidadRegistros ["+ cantidadRegistros +"]");
            }
            NominaDeTransferenciasDAO dao = new NominaDeTransferenciasDAO();
            if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[extraeDetallesTransferenciaPendiente][" + numeroTransferencia + "][BCI_FINOK]");}
            return dao.extraeDetallesTransferenciaPendiente(rutEmpresa, convenio, numeroTransferencia, bloque, cantidadRegistros);
        }catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) { getLogger().error("[extraeDetallesTransferenciaPendiente][" + numeroTransferencia + "][BCI_FINEX][Exception e]" + e.getMessage());}
            throw e;
        }
    }

    /**
     * <p>M�todo que obtiene un listado de transferencias pendientes, consultando seg�n ciertos
     * criterios.</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 19/10/2009 Alejandro Barra (SEnTRA): Versi�n inicial.
     *
     * </ul>
     *
     * @param filtro los filtros a aplicar a la consulta
     * @param bloque la p�gina a consultar
     * @param cantidadRegistros la cantidad de registros por cada p�gina
     * @throws Exception
     * @return Listado con las transferencias pendientes.
     * @since 1.0
     */
    public EncabezadoTransferenciaVO[] extraeTransferenciasPendientes(FiltroConsultarTransferenciasVO filtro, int bloque, int cantidadRegistros) throws Exception{
        
    	 String rutEmpresa = filtro.getRutOrigen();
         String convenio = filtro.getConvenio();
        try{
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[extraeTransferenciasPendientes][" + rutEmpresa + "][" + convenio + "][BCI_INI]" );
                getLogger().info("[extraeTransferenciasPendientes][" + rutEmpresa + "][" + convenio + "] bloque ["+ bloque +"], cantidadRegistros ["+ cantidadRegistros +"], filtro ["+ filtro.toString()+"]");
            }
            NominaDeTransferenciasDAO dao = new NominaDeTransferenciasDAO();
            if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[extraeTransferenciasPendientes][" + rutEmpresa + "][" + convenio + "][BCI_FINOK]");}
            return dao.extraeTransferenciasPendientes(filtro, bloque, cantidadRegistros);
        }catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) { getLogger().error("[extraeTransferenciasPendientes][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][Exception e]" + e.getMessage());}
            throw e;
        }
    }

    /**
     * <p>M�todo que obtiene un listado de las transferencias pendientes asociadas a la carga de
     * un archivo,
     * y consultando seg�n ciertos filtros.</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 19/10/2009 Alejandro Barra (SEnTRA): Versi�n inicial.
     *
     * </ul>
     *
     * @param filtro los filtros a aplicar a la consulta
     * @param bloque la p�gina a consultar
     * @param cantidadRegistros la cantidad de registros por cada p�gina
     * @throws Exception
     * @return Listado con las transferencias pendientes asociadas a la carga de un archivo.
     * @since 1.0
     */
    public EncabezadoTransferenciaVO[] extraeTransferenciasPendientesPorArchivo(FiltroConsultarTransferenciasVO filtro, int bloque, int cantidadRegistros) throws Exception{
        
    	String rutEmpresa = filtro.getRutOrigen();
        String convenio = filtro.getConvenio();
        try{
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[extraeTransferenciasPendientesPorArchivo][" + rutEmpresa + "][" + convenio + "][BCI_INI]" );
                getLogger().info("[extraeTransferenciasPendientesPorArchivo][" + rutEmpresa + "][" + convenio + "] bloque ["+ bloque +"], cantidadRegistros ["+ cantidadRegistros +"], filtro ["+ filtro.toString() +"]");
            }
            NominaDeTransferenciasDAO dao = new NominaDeTransferenciasDAO();
            if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[extraeTransferenciasPendientesPorArchivo][" + rutEmpresa + "][" + convenio + "][BCI_FINOK]");}
            return dao.extraeTransferenciasPendientesPorArchivo(filtro, bloque, cantidadRegistros);
        }catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) { getLogger().error("[extraeTransferenciasPendientesPorArchivo][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][Exception e]" + e.getMessage());}
            throw e;
        }
    }

    /**
     * <p>M�todo que obtiene los montos totales agrupados por cuenta para un grupo de transferencias
     * obtenidas seg�n ciertos criterios.</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 19/10/2009 Alejandro Barra (SEnTRA): Versi�n inicial.
     *
     * </ul>
     *
     * @param filtro los filtros a aplicar a la consulta
     * @throws Exception
     * @return Listado con los montos totales de ciertas trasferencias agrupados por cuenta.
     * @since 1.0
     */
    public TotalPorCuentaVO[] extraeTotalesPorCuentaDeTransferenciasPendientes(FiltroConsultarTransferenciasVO filtro) throws Exception{
        
    	String rutEmpresa = filtro.getRutOrigen();
        String convenio = filtro.getConvenio();
        try{
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[extraeTotalesPorCuentaDeTransferenciasPendientes][" + rutEmpresa + "][" + convenio + "][BCI_INI]" );
                getLogger().info("[extraeTotalesPorCuentaDeTransferenciasPendientes][" + rutEmpresa + "][" + convenio + "] filtro ["+ filtro.toString() +"]");
            }
            NominaDeTransferenciasDAO dao = new NominaDeTransferenciasDAO();
            if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[extraeTotalesPorCuentaDeTransferenciasPendientes][" + rutEmpresa + "][" + convenio + "][BCI_FINOK]");}
            return dao.extraeTotalesPorCuentaDeTransferenciasPendientes(filtro);
        }catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) { getLogger().error("[extraeTotalesPorCuentaDeTransferenciasPendientes][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][Exception e]" + e.getMessage());}
            throw e;
        }
    }

    /**
     * <p>M�todo que obtiene un listado de detalles de transferencias que en su carga han tenido reparos</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 19/10/2009 Alejandro Barra (SEnTRA): Versi�n inicial.
     *
     * </ul>
     *
     * @param filtro los filtros a aplicar a la consulta
     * @param bloque la p�gina a consultar
     * @param cantidadRegistros la cantidad de registros por cada p�gina
     * @throws Exception
     * @return Listado con los detalles de las transferencias con reparos.
     * @since 1.0
     */
    public TransferenciasVO[] extraeDetallesTransferenciasPendientesConReparos(FiltroConsultarTransferenciasVO filtro, int bloque, int cantidadRegistros) throws Exception{
    	String rutEmpresa = filtro.getRutOrigen();
        String convenio = filtro.getConvenio();
        try{
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[extraeDetallesTransferenciasPendientesConReparos][" + rutEmpresa + "][" + convenio + "][BCI_INI]" );
                getLogger().info("[extraeDetallesTransferenciasPendientesConReparos][" + rutEmpresa + "][" + convenio + "] bloque ["+ bloque +"], cantidadRegistros ["+ cantidadRegistros +"], filtro ["+ filtro.toString() +"]");
            }
            NominaDeTransferenciasDAO dao = new NominaDeTransferenciasDAO();
            if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[extraeDetallesTransferenciasPendientesConReparos][" + rutEmpresa + "][" + convenio + "][BCI_FINOK]");}
            return dao.extraeDetallesTransferenciasPendientesConReparos(filtro, bloque, cantidadRegistros);
        }catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) { getLogger().error("[extraeDetallesTransferenciasPendientesConReparos][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][Exception e]" + e.getMessage());}
            throw e;
        }
    }

    /**
     * <p>M�todo que obtiene un listado de las transferencias recibidas, consultando seg�n determinados
     * criterios.</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 19/10/2009 Alejandro Barra (SEnTRA): Versi�n inicial.
     *
     * </ul>
     *
     * @param filtro los filtros a aplicar a la consulta
     * @param bloque la p�gina a consultar
     * @param cantidadRegistros la cantidad de registros por cada p�gina
     * @throws Exception
     * @return Listado con las transferencias recibidas.
     * @since 1.0
     */
    public TransferenciasVO[] extraeTransferenciasRecibidas(FiltroConsultarTransferenciasVO filtro, int bloque, int cantidadRegistros) throws Exception{
    	String rutEmpresa = filtro.getRutOrigen();
        String convenio = filtro.getConvenio();
        try{
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[extraeTransferenciasRecibidas][" + rutEmpresa + "][" + convenio + "][BCI_INI]" );
                getLogger().info("[extraeTransferenciasRecibidas][" + rutEmpresa + "][" + convenio + "] bloque ["+ bloque +"], cantidadRegistros ["+ cantidadRegistros +"], filtro ["+ filtro.toString() +"]");
            }
            NominaDeTransferenciasDAO dao = new NominaDeTransferenciasDAO();
            if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[extraeTransferenciasRecibidas][" + rutEmpresa + "][" + convenio + "][BCI_FINOK]");}
            return dao.extraeTransferenciasRecibidas(filtro, bloque, cantidadRegistros);
        }catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) { getLogger().error("[extraeTransferenciasRecibidas][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][Exception e]" + e.getMessage());}
            throw e;
        }
    }

    /**
     * <p>M�todo que obtiene un listado de las transferencias recibidas enviadas desde Empresa BCI
     * a Empresa BCI.</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 28/06/2011, Denisse Vel�squez S. (SEnTRA): Versi�n inicial.
     *
     * </ul>
     *
     * @param filtro los filtros a aplicar a la consulta
     * @param bloque la p�gina a consultar
     * @param cantidadRegistros la cantidad de registros por cada p�gina
     * @throws Exception
     * @return Listado con las transferencias recibidas.
     * @since 1.5
     */
    public TransferenciasRecibidasVO[] extraeTransfRecibidasEmpBCIEmpBCI(long rutEmpresa, char dvEmpresa, Date fechaProceso, String cuentaDestino, int bloque, int cantidadRegistros) throws Exception{
        try{
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[extraeTransfRecibidasEmpBCIEmpBCI][" + rutEmpresa + "][BCI_INI]" );
                getLogger().info("[extraeTransfRecibidasEmpBCIEmpBCI][" + rutEmpresa + "] fechaProceso ["+ fechaProceso +"], cuentaDestino ["+ cuentaDestino +"],bloque ["+ bloque +"], cantidadRegistros ["+ cantidadRegistros +"] ");
            }
            NominaDeTransferenciasDAO dao = new NominaDeTransferenciasDAO();
            if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[extraeTransfRecibidasEmpBCIEmpBCI][" + rutEmpresa + "][BCI_FINOK]");}
            return dao.extraeTransfRecibidasEmpBCIEmpBCI(rutEmpresa, dvEmpresa, fechaProceso, cuentaDestino, bloque, cantidadRegistros);
        }catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) { getLogger().error("[extraeTransfRecibidasEmpBCIEmpBCI][" + rutEmpresa + "][BCI_FINEX][Exception e]" + e.getMessage());}
            throw e;
        }
    }

    /**
     * <p>M�todo que obtiene los datos de un detalle de transferencia.</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 19/10/2009 Alejandro Barra (SEnTRA): Versi�n inicial.
     *
     * </ul>
     *
     * @param rutEmpresa rut de la empresa
     * @param convenio n�mero de convenio
     * @param numeroTransferencia n�mero de la transferencia a consultar
     * @param numeroDetalle n�mero de detalle de la transferencia a consultar
     * @throws Exception
     * @return VO con los datos del detalle de la transferecia.
     * @since 1.0
     */
    public DetalleTransferenciaVO extraeDetalleTransferencia(String rutEmpresa, String convenio, String numeroTransferencia, int numeroDetalle) throws Exception{
        try{
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[extraeDetalleTransferencia][" + numeroTransferencia + "][BCI_INI]" );
                getLogger().info("[extraeDetalleTransferencia][" + numeroTransferencia + "] convenio ["+ convenio +"], numeroDetalle ["+ numeroDetalle +"]");
            }
            NominaDeTransferenciasDAO dao = new NominaDeTransferenciasDAO();
            if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[extraeDetalleTransferencia][" + numeroTransferencia + "][BCI_FINOK]");}
            return dao.extraeDetalleTransferencia(rutEmpresa, convenio, numeroTransferencia, numeroDetalle);
        }catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) { getLogger().error("[extraeDetalleTransferencia][" + numeroTransferencia + "][BCI_FINEX][Exception e]" + e.getMessage());}
            throw e;
        }
    }

    /**
     * <p>M�todo que consulta los eventos ocurridos sobre un conjunto de transferencias obtenidas 
     * seg�n ciertos criterios de b�squeda.</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 19/10/2009 Alejandro Barra (SEnTRA): Versi�n inicial.
     *
     * </ul>
     *
     * @param filtro los filtros a aplicar a la consulta
     * @throws Exception
     * @return Listado con los eventos realizados sobre las transferencias.
     * @since 1.0
     */
    public UsuarioTransferenciaVO[] consultarEventosParaTransferencias(FiltroConsultarTransferenciasVO filtro) throws Exception{
        
    	String rutEmpresa = filtro.getRutOrigen();
        String convenio = filtro.getConvenio();
        try{
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[consultarEventosParaTransferencias][" + rutEmpresa + "][" + convenio + "][BCI_INI]" );
                getLogger().info("[consultarEventosParaTransferencias][" + rutEmpresa + "][" + convenio + "] filtro ["+ filtro.toString() +"]");
            }
            NominaDeTransferenciasDAO dao = new NominaDeTransferenciasDAO();
            if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[consultarEventosParaTransferencias][" + rutEmpresa + "][" + convenio + "][BCI_FINOK]");}
            return dao.consultarEventosParaTransferencias(filtro);
        }catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) { getLogger().error("[consultarEventosParaTransferencias][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][Exception e]" + e.getMessage());}
            throw e;
        }

    }

    /**
     * <p>M�todo registra los eventos de la Transferencia.</p>
     *
     * Registro de versiones:<ul>
     *
     * <li> 1.0 08/06/2010, Denisse Vel�squez S. (SEnTRA): versi�n inicial.</li>
     *
     * </ul>
     *
     * @param rutUsuario rut del usuario que realiza el evento 
     * @param dvUsuario d�gito verificador del usuario que realiza el evento 
     * @param convenio c�digo del convenio 
     * @param rutEmpresa rut de la empresa 
     * @param dvEmpresa d�gito verificador de la empresa 
     * @param numOperacion n�mero de la operaci�n 
     * @param fechaAccion fecha en que se realiza el evento 
     * @param tipoAccion c�digo del tipo de la operaci�n a registrar
     * @throws Exception
     * @since 1.1
     */
    public void registraEvento(String rutUsuario, String dvUsuario, String convenio, String rutEmpresa, String dvEmpresa,
        String numOperacion, Date fechaAccion, String tipoAccion) throws Exception{
        try{
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[registraEvento][" + rutEmpresa + "]["+ convenio +"][BCI_INI]" );
                getLogger().info("[registraEvento][" + rutEmpresa + "]["+ convenio +"] rutUsuario ["+ rutUsuario +"] - ["+ dvUsuario +"], numOperacion ["+ numOperacion +"], fechaAccion ["+ fechaAccion +"], tipoAccion ["+ tipoAccion +"]");
            }
            NominaDeTransferenciasDAO dao = new NominaDeTransferenciasDAO();
            if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[registraEvento][" + rutEmpresa + "]["+ convenio +"][BCI_FINOK]");}
            dao.registraEvento(rutUsuario, dvUsuario, convenio, rutEmpresa, dvEmpresa, numOperacion, fechaAccion, tipoAccion);
            return;
        }catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) { getLogger().error("[registraEvento][" + rutEmpresa + "]["+ convenio +"][BCI_FINEX][Exception e]" + e.getMessage());}
            throw e;
        }
    }

    /**
     * <p>M�todo gen�rico para cambiar el valor, de la glosa elegida, en un detalle de transferencia</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 05/08/2010 H�ctor Hern�ndez Orrego (SEnTRA): Versi�n inicial.
     *
     * </ul>
     *
     * @param convenio n�mero de convenio
     * @param rutEmpresa rut de la empresa
     * @param numTransferencia n�mero de la transferencia
     * @param detalleTransferencia n�mero del detalle de la transferencia
     * @param identificadorGlosa numero identificador de la glosa que se desea modificar
     * @param detalleGlosa detalle a incluir en la glosa definida
     * @throws Exception
     * @return Respuesta de la modificaci�n.
     * @since 1.3
     */
    public boolean cambiaGlosaTransferencia(String convenio, String rutEmpresa, String numTransferencia, int detalleTransferencia,int identificadorGlosa, String detalleGlosa) throws Exception{
        try{
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[cambiaGlosaTransferencia][" + numTransferencia + "][BCI_INI]" );
                getLogger().info("[cambiaGlosaTransferencia][" + numTransferencia + "] convenio ["+ convenio +"], rutEmpresa ["+ rutEmpresa +"], detalleTransferencia ["+ detalleTransferencia +"], identificadorGlosa ["+ identificadorGlosa +"], detalleGlosa ["+ detalleGlosa +"]");
            }
            NominaDeTransferenciasDAO dao = new NominaDeTransferenciasDAO();
            if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[cambiaGlosaTransferencia][" + numTransferencia + "][BCI_FINOK]");}
            return dao.cambiaGlosaTransferencia(convenio, rutEmpresa, numTransferencia, detalleTransferencia,identificadorGlosa, detalleGlosa);
        }catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) { getLogger().error("[cambiaGlosaTransferencia][" + numTransferencia + "][BCI_FINEX][Exception e]" + e.getMessage());}
            throw e;
        }
    }

    /**
     * <p>M�todo que obtiene un listado de los pagos realizados a trav�s de carga masiva
     * y consultando seg�n ciertos filtros.</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 09/09/2010 Marcelo Fuentes H. (SEnTRA): Versi�n inicial.
     *
     * </ul>
     *
     * @param filtro los filtros a aplicar a la consulta
     * @param bloque la p�gina a consultar
     * @param cantidadRegistros la cantidad de registros por cada p�gina
     * @throws Exception
     * @return Listado con los pagos realizados a trav�s de carga masiva.
     * @since 1.4
     */
    public EncabezadoTransferenciaVO[] extraePagosRealizadosViaCargaMasiva(FiltroConsultarTransferenciasVO filtro, int bloque, int cantidadRegistros) throws Exception{
        
    	String rutEmpresa = filtro.getRutOrigen();
        String convenio = filtro.getConvenio();
        try{
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[extraePagosRealizadosViaCargaMasiva][" + rutEmpresa + "][" + convenio + "][BCI_INI]" );
                getLogger().info("[extraePagosRealizadosViaCargaMasiva][" + rutEmpresa + "][" + convenio + "] bloque ["+ bloque +"], cantidadRegistros ["+ cantidadRegistros +"], filtro ["+ filtro.toString() +"]");
            }
            NominaDeTransferenciasDAO dao = new NominaDeTransferenciasDAO();
            if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[extraePagosRealizadosViaCargaMasiva][" + rutEmpresa + "][" + convenio + "][BCI_FINOK]");}
            return dao.extraePagosRealizadosViaCargaMasiva(filtro, bloque, cantidadRegistros);
        }catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) { getLogger().error("[extraePagosRealizadosViaCargaMasiva][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][Exception e]" + e.getMessage());}
            throw e;
        }
    }


    /**
     * <p>M�todo que obtiene un listado de las transferencias pendientes asociadas a un n�mero de transferencia.</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 02/08/2011 Pedro Carmona Escobar (SEnTRA): Versi�n inicial.
     *
     * </ul>
     *
     * @param filtro los filtros a aplicar a la consulta, este caso el n�mero de transferencia
     * @param bloque la p�gina a consultar
     * @param cantidadRegistros la cantidad de registros por cada p�gina
     * @throws Exception
     * @return Listado con las transferencias pendientes asociadas a la carga de un archivo.
     * @since 2.0
     */
    public EncabezadoTransferenciaVO[] extraeTransferenciasPendientesPorNumero(FiltroConsultarTransferenciasVO filtro, int bloque, int cantidadRegistros) throws Exception{
    	String rutEmpresa = filtro.getRutOrigen();
        String convenio = filtro.getConvenio();
        try{
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[extraeTransferenciasPendientesPorNumero][" + rutEmpresa + "][" + convenio + "][BCI_INI]" );
                getLogger().info("[extraeTransferenciasPendientesPorNumero][" + rutEmpresa + "][" + convenio + "] bloque ["+ bloque +"], cantidadRegistros ["+ cantidadRegistros +"], filtro ["+ filtro.toString() +"]");
            }
            NominaDeTransferenciasDAO dao = new NominaDeTransferenciasDAO();
            if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[extraeTransferenciasPendientesPorNumero][" + rutEmpresa + "][" + convenio + "][BCI_FINOK]");}
            return dao.extraeTransferenciasPendientesPorNumero(filtro, bloque, cantidadRegistros);
        }catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) { getLogger().error("[extraeTransferenciasPendientesPorNumero][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][Exception e]" + e.getMessage());}
            throw e;
        }
    }




    /**
     * <p>M�todo que obtiene un listado con las transferencias que poseen cuentas no inscritas. Esta transferencias 
     * ser�n retornadas con el correspondiente c�digo de activaci�n.</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 02/08/2011 Pedro Carmona Escobar (SEnTRA): Versi�n inicial.
     *
     * </ul>
     *
     * @param rut con la parte num�rica del rut de la empresa
     * @param bloque la p�gina a consultar
     * @param cantidadRegistros la cantidad de registros por cada p�gina
     * @throws Exception
     * @return Listado con las transferencias.
     * @since 2.0
     */
    public InformacionCodigoActivacionVO[] extraeCodigodDeActivacion(long rut, int bloque, int cantidadRegistros) throws Exception{
        try{
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[extraeCodigodDeActivacion][" + rut + "][BCI_INI]" );
                getLogger().info("[extraeCodigodDeActivacion][" + rut + "] bloque ["+ bloque +"], cantidadRegistros ["+ cantidadRegistros +"]");
            }
            NominaDeTransferenciasDAO dao = new NominaDeTransferenciasDAO();
            if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[extraeCodigodDeActivacion][" + rut + "][BCI_FINOK]");}
            return dao.extraeCodigodDeActivacion(rut, bloque, cantidadRegistros);
        }catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) { getLogger().error("[extraeCodigodDeActivacion][" + rut + "][BCI_FINEX][Exception e]" + e.getMessage());}
            throw e;
        }
    }




    /**
     * <p>M�todo que permite validar el c�digo de las cuentas no inscritas de una transferencia.</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 02/08/2011 Pedro Carmona Escobar (SEnTRA): Versi�n inicial.
     *
     * </ul>
     *
     * @param idTransferencia con el id de la transferencia
     * @param convenio con el id del convenio de la empresa
     * @param rut con la parte num�rica del rut de la empresa
     * @param codigo de activaci�n de cuentas
     * @throws Exception
     * @return Listado con las cuentas.
     * @since 2.0
     */
    public boolean validarCodigoActivacion(String idTransferencia, String convenio, long rutEmpresa, String codigo) throws Exception{
        try{
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[validarCodigoActivacion][" + idTransferencia + "][BCI_INI]" );
                getLogger().info("[validarCodigoActivacion][" + idTransferencia + "] rutEmpresa ["+ rutEmpresa +"], convenio ["+ convenio +"], codigo ["+ codigo +"]");
            }
            NominaDeTransferenciasDAO dao = new NominaDeTransferenciasDAO();
            if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[validarCodigoActivacion][" + idTransferencia + "][BCI_FINOK]");}
            return dao.validarCodigoActivacion(idTransferencia, convenio, rutEmpresa, codigo);
        }catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) { getLogger().error("[validarCodigoActivacion][" + idTransferencia + "][BCI_FINEX][Exception e]" + e.getMessage());}
            throw e;
        }
    }



    /**
     * <p>M�todo que permite enviar al mail el c�digo de activaci�n de las cuentas no inscritas de una transferencia.
     * El mail es obtenido desde la base de datos Banele (tabla UCE).</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 02/08/2011 Pedro Carmona Escobar (SEnTRA): Versi�n inicial.</li>
     * <li>1.1 14/06/2012, Jaime Pezoa N��ez (BCI): Se elimina activaci�n de SMS.</li>
     *
     * </ul>
     *
     * @param rutUsuario con la parte num�rica del rut del usuario
     * @param dvUsuario con el d�gito verificador del rut del usuario
     * @param rutEmpresa con la parte num�rica del rut de la empresa
     * @param dvEmpresa con el d�gito verificador del rut de la empresa
     * @param convenio con el id del convenio de la empresa
     * @param numTrf con el id de la transferencia
     * @param evento con un objeto evento ya construido a trav�s de la sessionBci
     * @throws Exception
     * @return booleano con el resultado dela operacion.
     * @since 2.0
     */
    public boolean enviarCodigoActivacion(long rutUsuario, char dvUsuario, long rutEmpresa, char dvEmpresa, String convenio, String numTrf, Eventos evento) throws Exception{
        
        String codigo = "";
        try{
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[enviarCodigoActivacion][" + numTrf + "][BCI_INI]" );
                getLogger().info("[enviarCodigoActivacion][" + numTrf + "] rutUsuario ["+ rutUsuario +"] - ["+ dvUsuario +"], rutEmpresa ["+ rutEmpresa +"] - ["+ dvEmpresa +"], convenio ["+ convenio +"], evento ["+ evento +"]");
            }
            NominaDeTransferenciasDAO dao = new NominaDeTransferenciasDAO();
            codigo = dao.obtenerCodigoActivacion(rutEmpresa, convenio, numTrf);
            if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[enviarCodigoActivacion][" + numTrf + "] codigo: " + StringUtil.censura(codigo));}
            if (codigo==null){
                if (getLogger().isEnabledFor(Level.INFO)){getLogger().info("[enviarCodigoActivacion][" + numTrf + "][BCI_FINEX] No hara envio al no encontrar el c�digo de activaci�n.");}
                return false;
            }
        }catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) { getLogger().error("[enviarCodigoActivacion][" + numTrf + "][BCI_FINEX][Exception e]" + e.getMessage());}
            throw e;
        }
        boolean envioEmial = false; 
        try{
            SupervisorDelegate supDelegate = SupervisorDelegate.getInstance();          
            UsuariosDelConvenioEmpresasTO usuarioParametro = new UsuariosDelConvenioEmpresasTO();
            usuarioParametro.setRutUsuario(rutUsuario);
            usuarioParametro.setDvUsuario(dvUsuario);           
            EmpresasDelConvenioTO[] empresa = new EmpresasDelConvenioTO[1];
            empresa[0] = new EmpresasDelConvenioTO();
            empresa[0].setRutEmpresa(rutEmpresa);
            empresa[0].setDvEmpresa(dvEmpresa);
            empresa[0].setConvenio(convenio);
            usuarioParametro.setEmpresas(empresa);
            UsuariosDelConvenioEmpresasTO datosMail = supDelegate.datosDetalleUCE(usuarioParametro);
            UsuariosDelConvenioEmpresasTO datosNombres = supDelegate.obtenerDatosDeUsuario(rutUsuario, dvUsuario);
            String asunto = TablaValores.getValor(ARCHIVO_PARAMETROS, "msjEmailActivacion", "asunto");
            String cuerpo = TablaValores.getValor(ARCHIVO_PARAMETROS, "msjEmailActivacion", "cuerpo");
            String from = TablaValores.getValor(ARCHIVO_PARAMETROS, "msjEmailActivacion", "from");
            String nombreFrom = TablaValores.getValor(ARCHIVO_PARAMETROS, "msjEmailActivacion", "nombreFrom");
            asunto = StringUtil.reemplazaTodo(asunto, "[N_TRANF]", numTrf);
            cuerpo = StringUtil.reemplazaTodo(cuerpo, "[NOMBRE]", (datosNombres.getNombres() + " "  +datosNombres.getApellidoPaterno() + " "  + datosNombres.getApellidoMaterno()));
            cuerpo = StringUtil.reemplazaTodo(cuerpo, "[N_TRANF]", numTrf);
            cuerpo = StringUtil.reemplazaTodo(cuerpo, "[RUT_E]", (String.valueOf(rutEmpresa) + "-" + dvEmpresa));
            cuerpo = StringUtil.reemplazaTodo(cuerpo, "[CNV]", convenio);
            cuerpo = StringUtil.reemplazaTodo(cuerpo, "[CODIGO]", codigo);
            
            if (getLogger().isEnabledFor(Level.INFO)){
                getLogger().info("[enviarCodigoActivacion][" + numTrf + "][BCI_FINOK] enviar correo");
                getLogger().info("[enviarCodigoActivacion][" + numTrf + "][BCI_FINOK] datosNombres [ " + datosNombres + " ], datosMail [ " + datosMail + " ], asunto [ " + asunto + " ], from [ " + from + " ]");
                getLogger().info("[enviarCodigoActivacion][" + numTrf + "][BCI_FINOK] cuerpo[ " + cuerpo + " ], nombreFrom[ " + nombreFrom + " ]");
            }
            envioEmial = EnvioDeCorreo.simple(nombreFrom, from, datosMail.getEmail().trim(), asunto, cuerpo, "") == EnvioDeCorreo.EXITO ; 
        }catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) { getLogger().error("[enviarCodigoActivacion][" + numTrf + "][BCI_FINEX][Exception e]" + e.getMessage());}
            envioEmial = false;
        }
        return (envioEmial);
    }


    /**
     * <p>M�todo que obtiene un listado de las cuentas no inscritas que pertenencen a una
     * transferencia de una empresa.</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 02/08/2011 Pedro Carmona Escobar (SEnTRA): Versi�n inicial.
     *
     * </ul>
     *
     * @param idTransferencia con el id de la transferencia
     * @param convenio con el id del convenio de la empresa
     * @param rut con la parte num�rica del rut de la empresa
     * @throws Exception
     * @return Listado con las cuentas.
     * @since 2.0
     */
    public CuentasInscritasVO[] extraeCuentasNoInscritas(String idTransferencia, String convenio, long rutEmpresa) throws Exception{
        try{
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[extraeCuentasNoInscritas][" + idTransferencia + "][BCI_INI]" );
                getLogger().info("[extraeCuentasNoInscritas][" + idTransferencia + "] rutEmpresa ["+ rutEmpresa +"], convenio ["+ convenio +"]");
            }
            NominaDeTransferenciasDAO dao = new NominaDeTransferenciasDAO();
            if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[extraeCuentasNoInscritas][" + idTransferencia + "][BCI_FINOK]");}
            return dao.extraeCuentasNoInscritas(idTransferencia, convenio, rutEmpresa);
        }catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) { getLogger().error("[extraeCuentasNoInscritas][" + idTransferencia + "][BCI_FINEX][Exception e]" + e.getMessage());}
            throw e;
        }
    }

    /**
     * <p>M�todo que obtiene las transferencias pendientes por resolver.</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 23/08/2011 Freddy Monsalve - Freddy Painenao. (Os.One): Versi�n inicial.
     *
     * </ul>
     *
     * @param filtro los filtros a aplicar a la consulta
     * @throws Exception
     * @return Listado con las transferencias pendientes por resolver.
     * @since 2.1
     */
    public TransferenciasVO[] extraeTransferenciasPendientesPorResolver(FiltroConsultarTransferenciasVO filtro) throws Exception{
        
    	String rutEmpresa = filtro.getRutOrigen();
        String convenio = filtro.getConvenio();
        try{
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[extraeTransferenciasPendientesPorResolver][" + rutEmpresa + "][" + convenio + "][BCI_INI]" );
                getLogger().info("[extraeTransferenciasPendientesPorResolver][" + rutEmpresa + "][" + convenio + "] filtro ["+ filtro.toString() +"]");
            }
            NominaDeTransferenciasDAO dao = new NominaDeTransferenciasDAO();
            if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[extraeTransferenciasPendientesPorResolver][" + rutEmpresa + "][" + convenio + "][BCI_FINOK]");}
            return dao.extraeTransferenciasPendientesPorResolver(filtro);

        }catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) { getLogger().error("[extraeTransferenciasPendientesPorResolver][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][Exception e]" + e.getMessage());}
            throw e;
        }
    }

    /**
     * <p>M�todo que obtiene un listado de las transferencias a terceros que se
     * encuentran en estado de pendientes por resolver.</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 16/01/2012 Iv�n L�pez B. (ADA Ltda.): Versi�n inicial.
     * </ul>
     *
     * @param rut con el rut de la empresa.
     * @param convenio con el c�digo del convenio a consultar.
     * @param bloque con la p�gina a desplegar.
     * @param cantidadRegistros la cantidad de registros a desplegar por p�gina.
     * @return Objeto con el listado de transferencia pendientes correspondiente.
     * @throws ExcepcionGeneral Cuando existe un error se lanza esta excepci�n.
     * @since 2.3
     */
    public EncabezadoTransferenciaVO[]
        extraeTransferenciasPendientesAterceros(long rut, String convenio,
            int bloque,int cantidadRegistros)
                throws ExcepcionGeneral {
        
        if (getLogger().isEnabledFor(Level.INFO)) { 
            getLogger().info("[extraeTransferenciasPendientesAterceros][" + rut + "]["+ convenio +"][BCI_INI]" );
            getLogger().info("[extraeTransferenciasPendientesAterceros][" + rut + "]["+ convenio +"] bloque ["+ bloque +"], cantidadRegistros ["+ cantidadRegistros +"]");
        }
        EncabezadoTransferenciaVO[] listadoTransferenciasPendientes;
        NominaDeTransferenciasDAO nominaDeTransferenciasDao =
            new NominaDeTransferenciasDAO();
        listadoTransferenciasPendientes =
            nominaDeTransferenciasDao.extraeTransferenciasPendientesATerceros(
                rut, convenio, bloque,
                cantidadRegistros);
        if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[extraeTransferenciasPendientesAterceros][" + rut + "]["+ convenio +"][BCI_FINOK] transferencias pendientes ["+ listadoTransferenciasPendientes +"]");}
        return listadoTransferenciasPendientes;
    }

    /**
     * <p>M�todo que obtiene un listado de las transferencias entre productos
     * contratados que se encuentran en estado de pendientes por resolver.</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 16/01/2012 Iv�n L�pez B. (ADA Ltda.): Versi�n inicial.
     * </ul>
     *
     * @param rut con el rut de la empresa.
     * @param convenio con el c�digo del convenio a consultar.
     * @param bloque con la p�gina a desplegar.
     * @param cantidadRegistros la cantidad de registros a desplegar por p�gina.
     * @return Objeto con el listado de transferencia pendientes correspondiente.
     * @throws ExcepcionGeneral Cuando existe un error se lanza esta excepci�n.
     * @since 2.3
     */
    public EncabezadoTransferenciaVO[]
        extraeTransferenciasPendientesEPC(long rut, String convenio,
            int bloque,int cantidadRegistros)
                throws ExcepcionGeneral {
        if (getLogger().isEnabledFor(Level.INFO)) { 
            getLogger().info("[extraeTransferenciasPendientesEPC][" + rut + "]["+ convenio +"][BCI_INI]" );
            getLogger().info("[extraeTransferenciasPendientesEPC][" + rut + "]["+ convenio +"] bloque ["+ bloque +"], cantidadRegistros ["+ cantidadRegistros +"]");
        }
        EncabezadoTransferenciaVO[] listadoTransferenciasPendientes;
        NominaDeTransferenciasDAO nominaDeTransferenciasDao = new NominaDeTransferenciasDAO();
        listadoTransferenciasPendientes =
            nominaDeTransferenciasDao.extraeTransferenciasPendientesEPC(
                rut, convenio, bloque,
                cantidadRegistros);
        if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[extraeTransferenciasPendientesEPC][" + rut + "]["+ convenio +"][BCI_FINOK] transferencias pendientes ["+ listadoTransferenciasPendientes +"]");}
        return listadoTransferenciasPendientes;
    }

    /**
     * <p>M�todo que obtiene el detalle de una transferencia seg�n su n�mero de
     * transferencia.</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 16/01/2012 Iv�n L�pez B. (ADA Ltda.): Versi�n inicial.</li>
     * </ul>
     *
     * @param numeroTransferencia con el n�mero de la transferencia a consultar.
     * @param rutEmpresa con el rut de la empresa.
     * @param convenio con el c�digo del convenio.
     * @return Objeto con el detalle de transferencia pendientes correspondiente.
     * @throws ExcepcionGeneral Cuando existe un error se lanza esta excepci�n.
     * @since 2.3
     */
    public DetalleTransferenciaVO[]
        obtenerDetalleTransferencia(String numeroTransferencia,
            long rutEmpresa, String convenio)
                throws ExcepcionGeneral {
        if (getLogger().isEnabledFor(Level.INFO)) { 
            getLogger().info("[obtenerDetalleTransferencia][" + numeroTransferencia + "][BCI_INI]" );
            getLogger().info("[obtenerDetalleTransferencia][" + numeroTransferencia + "] rutEmpresa ["+ rutEmpresa +"], convenio ["+ convenio +"]");
        }
        DetalleTransferenciaVO[] detalles;
        NominaDeTransferenciasDAO nominaDeTransferenciasDao = new NominaDeTransferenciasDAO();
        detalles =
            nominaDeTransferenciasDao.obtenerDetalleTransferencia(
                numeroTransferencia,
                rutEmpresa, convenio);
        if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[obtenerDetalleTransferencia][" + numeroTransferencia + "]["+ convenio +"][BCI_FINOK] transferencias pendientes ["+ detalles +"]");}
        return detalles;
    }

    /**
     * <p>M�todo que elimina las cuentas Por Inscribir de una transferencia.</p>
     *
     * Registro de versiones:<ul>
     * 
     * <li>1.0 08/03/2013 Sergio Cuevas Diaz (SEnTRA): Versi�n inicial.</li>
     * 
     * </ul>
     *
     * @param numConvenio n�mero de convenio
     * @param rutEmpresa rut de la empresa
     * @param dvEmpresa dv de la empresa
     * @param numTransferencia N�mero de Transferencia
     * @throws Exception Cuando existe un error se lanza esta excepci�n.
     * @return Respuesta de la modificaci�n (true, false).
     * @since 2.4
     */
    public boolean eliminaDetalleCuentasNoInscritas(String numConvenio, long rutEmpresa, char dvEmpresa, 
        String numTransferencia) throws Exception{

        try{
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[eliminaDetalleCuentasNoInscritas][" + numTransferencia + "][BCI_INI]" );
                getLogger().info("[eliminaDetalleCuentasNoInscritas][" + numTransferencia + "] rutEmpresa ["+ rutEmpresa +"] - ["+ dvEmpresa +"], numConvenio ["+ numConvenio +"]");
            }
            NominaDeTransferenciasDAO dao = new NominaDeTransferenciasDAO();
            
            if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[eliminaDetalleCuentasNoInscritas][" + numTransferencia + "][BCI_FINOK]");}
            return dao.eliminaDetalleCuentasNoInscritas(numConvenio, rutEmpresa, dvEmpresa, numTransferencia);
        }
        catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) { getLogger().error("[eliminaDetalleCuentasNoInscritas][" + numTransferencia + "][BCI_FINEX][Exception e]" + e.getMessage()); }
            throw e;
        }
    }   

    /**
     * <p>Metodo Obtiene instrucciones de transferencia supervisor</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 22/01/2013 Rodrigo Arce Campos  SEnTRA: Versi�n inicial.</li>
     * </ul>
     *
     * @return retorna un arreglo de las instrucciones del supervisor
     * @throws Exception En caso de haber error durante la ejecuci�n.
     * @since 2.4
     */
    public InstruccionTransferenciaTO[] obtenerInstruccionesTransferenciaSupervisor()
        throws Exception {

        if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[InstruccionTransferenciaTO][BCI_INI]" ); }
        NominaDeTransferenciasDAO dao = new NominaDeTransferenciasDAO();
        InstruccionTransferenciaTO[] transferencia = dao
            .obtenerInstruccionesTransferenciaSupervisor();
        if (getLogger().isEnabledFor(Level.ERROR)) { getLogger().error("[InstruccionTransferenciaTO][BCI_FINOK] transferencia ["+ transferencia +"]");}
        return transferencia;
    }


    /**
     * <p>
     * M�todo que envia una instruccion a un supervisor.
     * </p>
     * 
     * Registro de versiones:
     * <ul>
     * 
     * <li>1.0 22/01/2013 Rodrigo Arce Campos. (SEnTRA): Versi�n inicial.
     * 
     * </ul>
     * 
     * @param instruccion
     *            Instruccion a enviar al supervisor.
     * @throws Exception en caso de error.
     * @since 2.4
     */
    public void enviarInstruccionSupervisor(
        InstruccionTransferenciaTO instruccion) throws Exception {

        String numTransferencia = instruccion.getNumeroTransferencia();
        if (getLogger().isEnabledFor(Level.INFO)) { 
            getLogger().info("[enviarInstruccionSupervisor][" + numTransferencia + "][BCI_INI]" );
            getLogger().info("[enviarInstruccionSupervisor][" + numTransferencia + "] instruccion ["+ instruccion +"]");
        }
        NominaDeTransferenciasDAO dao = new NominaDeTransferenciasDAO();
        dao.enviarInstruccionSupervisor(instruccion);
        if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[enviarInstruccionSupervisor][BCI_FINOK]");}
    }

    /**
     * <p>
     * M�todo que elimina una instruccion a un supervisor mediante un rut.
     * </p>
     * 
     * Registro de versiones:
     * <ul>
     * 
     * <li>1.0 22/01/2013 Rodrigo Arce Campos. (SEnTRA): Versi�n inicial.
     * 
     * </ul>
     * 
     * @param rutEmpresa
     *            Rut Empresa
     * @param convenio
     *            Numero convenio
     * @param numeroTransferencia
     *            Numero transferencia
     * @throws Exception en caso de error.
     * @since 2.4
     */

    public void eliminarInstruccionTransferenciaSupervisor(long rutEmpresa,
        int convenio, String numeroTransferencia) throws Exception {

        if (getLogger().isEnabledFor(Level.INFO)) { 
            getLogger().info("[eliminarInstruccionTransferenciaSupervisor][" + numeroTransferencia + "][BCI_INI]" );
            getLogger().info("[eliminarInstruccionTransferenciaSupervisor][" + numeroTransferencia + "] rutEmpresa ["+ rutEmpresa +"], convenio ["+ convenio +"]");
        }
        NominaDeTransferenciasDAO dao = new NominaDeTransferenciasDAO();
        dao.eliminarInstruccionTransferenciaSupervisor(rutEmpresa, convenio,
            numeroTransferencia);
        if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[eliminarInstruccionTransferenciaSupervisor][BCI_FINOK]");}
    }

    /**
     * <p>
     * M�todo que inserta acciones de trasferencia en tabla bitacora.
     * </p>
     * 
     * Registro de versiones:
     * <ul>
     * 
     * <li>1.0 22/01/2013 Rodrigo Arce Campos. (SEnTRA): Versi�n inicial.
     * 
     * </ul>
     * 
     * @param datosAccion
     *            parametro de tipo clase que contiene los atributos a insetar
     *            en Base datos
     * @throws Exception en caso de error.
     * @since 2.4
     */

    public void insertarAccionBitacoraInstruccionesTransferencia(
        InstruccionTransferenciaTO datosAccion) throws Exception {
        String numTransferencia = datosAccion.getNumeroTransferencia();
        if (getLogger().isEnabledFor(Level.INFO)) { 
            getLogger().info("[insertarAccionBitacoraInstruccionesTransferencia][" + numTransferencia + "][BCI_INI]" );
            getLogger().info("[insertarAccionBitacoraInstruccionesTransferencia][" + numTransferencia + "] datosAccion ["+ datosAccion.toString() +"]");
        }
        NominaDeTransferenciasDAO dao = new NominaDeTransferenciasDAO();
        dao.insertarAccionBitacoraInstruccionesTransferencia(datosAccion);
        if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[eliminarInstruccionTransferenciaSupervisor][" + numTransferencia + "][BCI_FINOK]");}
    }

    /**
     * <p>
     * M�todo que obtiene un listado de transferencias pendientes del tipo CTA o
     * LBT, consultando seg�n criterios enviados.
     * </p>
     * 
     * Registro de versiones:
     * <ul>
     * 
     * <li>1.0 22/01/2013 Rodrigo Arce Campos. (SEnTRA): Versi�n inicial.
     * </ul>
     * 
     * @param filtro
     *            los filtros a aplicar a la consulta
     *
     * @throws Exception en caso de error.
     * @return Listado con las transferencias pendientes.
     * @since 2.4
     */
    public EncabezadoTransferenciaVO[] extraeTransferenciasPendientesCTALBT(
        FiltroConsultarTransferenciasVO filtro) throws Exception {
        
    	String rutEmpresa = filtro.getRutOrigen();
        String convenio = filtro.getConvenio();
        if (getLogger().isEnabledFor(Level.INFO)) { 
            getLogger().info("[extraeTransferenciasPendientesCTALBT][" + rutEmpresa + "][" + convenio + "][BCI_INI]" );
            getLogger().info("[extraeTransferenciasPendientesCTALBT][" + rutEmpresa + "][" + convenio + "] filtro ["+ filtro.toString() +"]");
        }
        NominaDeTransferenciasDAO dao = new NominaDeTransferenciasDAO();
        if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[extraeTransferenciasPendientesCTALBT][" + rutEmpresa + "][" + convenio + "][BCI_FINOK]");}
        return dao.extraeTransferenciasPendientesCTALBT(filtro);
    }

    /**
     * <p>M�todo que permite consultar si se ha realizado una Transferencia con los mismos valores en las �ltimas
     *    24 horas.</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 04/02/2013 Venancio Ar�nguiz (SEnTRA): Versi�n inicial.
     * </ul>
     *
     * @param rutEmpresa Rut de la empresa.
     * @param convenio N�mero de convenio.
     * @param numTTFF N�mero de transferencia.
     * @param cuentaOrigen Cuenta de origen.
     * @param detalle Objeto con el detalle de la transferencia.
     * @throws ExcepcionGeneral Alg�n error al ejecutar la consulta.
     * @return Registros de transferencias con los mismos datos.
     * @since 2.4
     */
    public EncabezadoTransferenciaVO[] consultarExisteTransferencia(String rutEmpresa, String convenio, String numTTFF, 
        String cuentaOrigen, ListaDeteTransf detalle) throws ExcepcionGeneral {
            
        if (getLogger().isEnabledFor(Level.INFO)) { 
            getLogger().info("[consultarExisteTransferencia][" + numTTFF + "][BCI_INI]" );
            getLogger().info("[consultarExisteTransferencia][" + numTTFF + "] rutEmpresa ["+ rutEmpresa +"], convenio ["+ convenio +"], cuentaOrigen ["+ cuentaOrigen +"], detalle ["+ detalle +"]");
        }
            NominaDeTransferenciasDAO dao = new NominaDeTransferenciasDAO();
            if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[consultarExisteTransferencia][" + numTTFF + "][BCI_FINOK]"); }
        return dao.consultarExisteTransferencia(rutEmpresa, convenio, numTTFF, cuentaOrigen, detalle);
    }

    /**
     * M�todo que actualiza los tiempos de espera y posici�n en cola de procesamiento de las n�minas que se
     * encuentran con estado firmada.
     * <p>
     * Registro de versiones:<ul>
     * <li> 1.0 22/01/2014, Venancio Ar�nguiz (SEnTRA): Versi�n inicial.</li>
     * </ul>
     * <p>
     *
     * @param numTTFF N�mero de la transferencia.
     * @return Un objeto que contiene el estado, el tiempo de espera y la posici�n en cola.
     * @throws Exception
     * 
     * @since 2.5
     */
    public EncabezadoTransferenciaVO obtenerTiempoEsperaNominaTTFF(String numTTFF) throws ExcepcionGeneral {
        if (getLogger().isEnabledFor(Level.INFO)) { 
            getLogger().info("[obtenerTiempoEsperaNominaTTFF][" + numTTFF + "][BCI_INI]" );
        }
        NominaDeTransferenciasDAO dao = new NominaDeTransferenciasDAO();
        if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[obtenerTiempoEsperaNominaTTFF][" + numTTFF + "][BCI_FINOK]");}
        
        return dao.obtenerTiempoEsperaNominaTTFF(numTTFF);
    }
    /**
     * <p>
     * M�todo que permite firmar n�minas PNOL desde Pyme Web o desde ambiente Pyme m�vil.
     * <br><br>
     * Este m�todo es extraido desde {@link 
     * wcorp.aplicaciones.productos.servicios.transferencias.nominas.struts.actions.FirmarTransferenciaAction.
     * firmaTransferencia(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)}}
     * 
     * Es importante se�alar que este m�todo no realiza validaci�n de firma del apoderado de un convenio, ya que 
     * dicha tarease realiza en capa 1 con los componentes de segunda clave. Por tanto, primero se debe validar 
     * la firma, y si �sta es OK se puede llamar a este m�todo
     * <p> 
     * 
     * Registro de versiones:<ul>
     * <li> 1.0 10/02/2014, Jaime Suazo D. (Experimento Social. SPA.): versi�n inicial.</li> 
     * </ul>
     * @param nominasAFirmar arreglo con las n�minas PNOL a firmar.
     * @param cabecera TO con los atributos necesarios para la realizaci�n de las validaciones previas a firma de
     *  n�minas.
     * @return arreglo con los estados de firma de cada una de las n�minas.
     * @throws GeneralException en caso de error de negocio.
     * @throws Exception en caso de error de sistema.
     * @since 2.7
     */
    public EstadoFirmaVO[] firmarNominasPNOL(NominaPnolTO[] nominasAFirmar, 
        CabeceraFirmaNominaPnolTO cabecera) throws GeneralException, Exception {


        EstadoFirmaVO[] resultadoFirma = null;

        if (nominasAFirmar != null && nominasAFirmar.length > 0 && nominasAFirmar[0] != null) {

            String rutEmpresa = nominasAFirmar[0].getRutEmpresa();
            rutEmpresa = StringUtil.completaPorLaIzquierda(rutEmpresa, LARGO_RUT, RELLENO_RUT_EMPRESAS);

            String dvEmpresa = String.valueOf(nominasAFirmar[0].getDvEmpresa());
            String convenio = nominasAFirmar[0].getConvenioEmpresa();

            convenio = StringUtil.completaPorLaIzquierda(convenio, LARGO_CONVENIO, RELLENO_CONVENIO_EMPRESAS);

            String rutUsuario = String.valueOf(nominasAFirmar[0].getRutUsuario());
            rutUsuario = StringUtil.completaPorLaIzquierda(rutUsuario, LARGO_RUT, RELLENO_RUT_EMPRESAS);

            String dvUsuario = String.valueOf(nominasAFirmar[0].getDvUsuario());

            String tipoUsuario = cabecera.getTipoUsuario();
            String perfil = cabecera.getPerfilUsuario();

            if (getLogger().isEnabledFor(Level.INFO)){
                getLogger().info("[firmarNominasPNOL][" + rutEmpresa + "]["+ convenio +"][BCI_INI]" );
                getLogger().info("[firmarNominasPNOL][" + rutEmpresa + "]["+ convenio +"] rutUsuario : [" + rutUsuario +"], perfil : [" + perfil + "]");
            }

            if(!esApoderadoAutorizado(tipoUsuario, perfil)){
                if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[firmarNominasPNOL][" + rutEmpresa + "]["+ convenio +"][BCI_FINEX] Apoderado [" + rutUsuario + "] No est� autorizado"); }
                throw new GeneralException("TRF043");
            }           
            if (getLogger().isEnabledFor(Level.INFO)){getLogger().info("[firmarNominasPNOL][" + rutEmpresa + "]["+ convenio +"] antes de llamar a convertirAArregloString"); }

            String[] arregloFolios = convertirAArregloString(nominasAFirmar);
            if (getLogger().isEnabledFor(Level.INFO)){getLogger().info("[firmarNominasPNOL][" + rutEmpresa + "]["+ convenio +"] antes de llamar a  creaFiltro");}

            FiltroConsultarTransferenciasVO filtro = this.creaFiltro(rutEmpresa, convenio, "", 
                PAGO_POR_FIRMAR);
            if (getLogger().isEnabledFor(Level.INFO)){getLogger().info("[firmarNominasPNOL][" + rutEmpresa + "]["+ convenio +"] antes de llamar a  extraeTransferenciasPendientes");}

            EncabezadoTransferenciaVO[] transferenciasPendientes = 
                this.extraeTransferenciasPendientes(filtro, SIN_PAGINACION , TODAS_TRANSFERENCIAS);
            if (getLogger().isEnabledFor(Level.INFO)){getLogger().info("[firmarNominasPNOL][" + rutEmpresa + "]["+ convenio +"] antes de llamar a  validarDisponibleDeCtaCargoNominaConMontoNomina");}

            this.validarDisponibleDeCtaCargoNominaConMontoNomina(rutEmpresa, transferenciasPendientes, 
                arregloFolios, cabecera);

            if (getLogger().isEnabledFor(Level.INFO)){getLogger().info("[firmarNominasPNOL][" + rutEmpresa + "]["+ convenio +"] voy a llamar a obtenerModoFirmaFyP"); }

            String canal = cabecera.getCanal();
            String resultFyPMono = "";

            try {
                resultFyPMono = obtenerModoFirmaEnFyP(rutEmpresa, dvEmpresa, convenio, rutUsuario, dvUsuario, 
                    canal, tipoUsuario, perfil);

                if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[firmarNominasPNOL][" + rutEmpresa + "]["+ convenio +"] obtenerModoFirmaEnFyP resultFyPMono : " + resultFyPMono);}                
            
            } catch (GeneralException e) {
                if (getLogger().isEnabledFor(Level.ERROR)) { getLogger().error("[firmarNominasPNOL][" + rutEmpresa + "]["+ convenio +"][BCI_FINEX][GeneralException e]"+ e.getMessage() +"]", e);    }
                throw e;
            } catch (Exception e) {
                if (getLogger().isEnabledFor(Level.ERROR)) { getLogger().error("[firmarNominasPNOL][" + rutEmpresa + "]["+ convenio +"][BCI_FINEX][Exception e]" + e.getMessage() + "]", e);}
                throw e;
            }

            int topePorEvento = 0;
            String producto = rescatarProducto(rutUsuario, convenio, rutEmpresa);

        
            if (PRODUCTO_EXPRESS.equals(producto)){
                topePorEvento = Integer.parseInt(TablaValores.getValor(ARCHIVO_PARAMETROS, "topesIndustria", 
                    "topePorEventoEmpresa"));
            }else{
                topePorEvento = Integer.parseInt(TablaValores.getValor(ARCHIVO_PARAMETROS, "topesIndustria", 
                    "topePorEventoEmpresario"));
            }

            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[firmarNominasPNOL][" + rutEmpresa + "]["+ convenio +"] producto [" + producto + "]");
                getLogger().info("[firmarNominasPNOL][" + rutEmpresa + "]["+ convenio +"] topePorEvento [" + topePorEvento + "]");
            }

            try {
                cabecera.setProductoBel(producto);
                resultadoFirma = this.firmarTransferenciasPNOL(rutEmpresa, dvEmpresa, convenio, rutUsuario, 
                    dvUsuario, arregloFolios, topePorEvento, cabecera);

                if(getLogger().isEnabledFor(Level.INFO)){ getLogger().info("[firmarNominasPNOL][" + rutEmpresa + "]["+ convenio +"][BCI_FINOK] resultado firma ["+ resultadoFirma +"]");}
                return resultadoFirma;
            } catch (GeneralException e) {
                if (getLogger().isEnabledFor(Level.ERROR)) { getLogger().error("[firmarNominasPNOL][" + rutEmpresa + "]["+ convenio +"][BCI_FINEX][GeneralException e][" + e.getMessage() +"]", e);}
                throw e;
            } catch (Exception e) {
                if (getLogger().isEnabledFor(Level.ERROR)) { getLogger().error("[firmarNominasPNOL][" + rutEmpresa + "]["+ convenio +"][BCI_FINEX][Exception e][" + e.getMessage() +"]", e);}
                throw e;
            }
        }

        return resultadoFirma;
    }

    /**
     * M�todo que consulta si el convenio opera con firmas y poderes o es Monousuario Pyme.
     * M�todo migrado desde {@link wcorp.aplicaciones.productos.servicios.transferencias.nominas.struts.helper.TransferenciasHelper}
     * <p>
     * Registro de versiones:<ul>
     * <li> 1.0 10/02/2014, Jaime Suazo D. (Experimento Social): versi�n inicial.</li>
     * </ul>
     * <p>
     *
     * @param rutEmpresa Rut Empresa.
     * @param digitoVerifEmp D�gito Verificador Empresa.
     * @param convenioEmpresa N�mero de Convenio.
     * @param rutUsu Rut Usuario.
     * @param digitoVerifUsu D�gito Verificador Usuario.
     * @return String confirmaci�n si opera con FyP o es monousuario.
     * @throws Exception cuando existe un error de sistema.
     *
     * @since 2.7
     */
    public String validaFyPoMono(String rutEmpresa, String digitoVerifEmp, String convenioEmpresa, String rutUsu,
        String digitoVerifUsu) throws Exception {

        String continua = "N";
        String apoMonoUsuario  = "";
        int    rutInt   = 0;
        int    apoReal  = 0;
        long   rutTopeFyPEPyme = 0;

        try{
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[validaFyPoMono][" + rutEmpresa + "]["+ convenioEmpresa +"][BCI_INI]" );
                getLogger().info("[validaFyPoMono][" + rutEmpresa + "]["+ convenioEmpresa +"] rutUsuario ["+ rutUsu +"] - ["+ digitoVerifUsu +"]");
            }

            rutInt = IntUtl.getNumero(rutEmpresa);

            apoReal = Integer.parseInt(TablaValores.getValor(ARCH_PARAM_EMP, "PosTipoApoderado", "Desc"));
            rutTopeFyPEPyme = Long.parseLong(TablaValores.getValor(ARCH_PARAM_EMP, "UsuarioEpyme", "rutTope"));
            apoMonoUsuario  = TablaValores.getValor(ARCH_PARAM_EMP, "UsuarioEpyme", "apoMonoUsuario");

            BCIExpressDelegate delegate  = new BCIExpressDelegate();

            String tipoProducto  = rescatarProducto(rutUsu, convenioEmpresa, rutEmpresa);
            String posicion      = delegate.posicionCpearadat(rutEmpresa, convenioEmpresa, tipoProducto, apoReal);
            

            if (posicion.trim().toUpperCase().equalsIgnoreCase("S")) {
                continua = "S";
            }

            if (PRODUCTO_EPYME.equals(tipoProducto) && rutInt < rutTopeFyPEPyme ) {
                continua = apoMonoUsuario;
            }

            if (getLogger().isEnabledFor(Level.INFO)){getLogger().info("[validaFyPoMono][" + rutEmpresa + "]["+ convenioEmpresa +"][BCI_FINOK] continua [" + continua + "]");}
            return continua;

        }catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) { getLogger().error("[validaFyPoMono][" + rutEmpresa + "]["+ convenioEmpresa +"][BCI_FINEX][Exception e][" + e.getMessage() + "]", e);}
            throw new Exception("Error al validar si opera con FyP");
        }

    }

    /**
     * M�todo que convierte un arreglo de {@link NominaPnolTO} en arreglo de String
     * 
     * Registro de versiones:<ul>
     * <li> 1.0 31/05/2014, Jaime Suazo D. (Experimento Social): versi�n inicial.</li>
     * </ul>
     * <p> 
     * @param nominasAFirmar a firmar
     * @return folios de n�minas a firmar
     * 
     * @since 2.7
     */
    private String[] convertirAArregloString(NominaPnolTO[] nominasAFirmar) {

        String[] arreglo = new String[nominasAFirmar.length];
        getLogger().info("[convertirAArregloString][BCI_INI]");
        for (int indice = 0; indice < arreglo.length; indice++) {
            arreglo[indice] = nominasAFirmar[indice].getTransfAFirmar();
        }
        getLogger().info("[convertirAArregloString][BCI_FINOK]");
        return arreglo;
    }


    /**
     * <p>M�todo realiza la Firma de las transferencias Empresa.</p>
     * Se deja marcada en la BD las transferencias que est�n firmadas, para que sean tomadas por el proceso
     * autom�tico que realiza el pago de estas.
     * <br><br>
     * M�todo migrado desde {@link M�todo migrado desde 
     * {@link wcorp.aplicaciones.productos.servicios.transferencias.nominas.struts.helper.TransferenciasHelper}}
     * 
     * Es importante se�alar que este m�todo no realiza validaci�n de firma del apoderado de un convenio, ya que 
     * dicha tarea se realiza en capa 1 con los componentes de segunda clave. Por tanto, primero se debe validar
     la firma, y si �sta es OK se puede llamar a este m�todo
     * 
     * Registro de versiones:<ul>
     * <li> 1.0 10/02/2014, Jaime Suazo D. (Experimento Social): versi�n inicial.</li>
     * </ul>
     * <p>  
     * @param rutEmpresa Rut Empresa
     * @param digitoVerifEmp D�gito Verificador Empresa
     * @param convenioEmpresa N�mero de Convenio
     * @param rutUsu Rut Usuario
     * @param digitoVerifUsu D�gito Verificador Usuario
     * @param transfAFirmar Transferencias a Firmar
     * @param topeIndustria tope transferencias Industria
     * @param cabeceraFirma Cabecera firma.
     * @return EstadoFirmaVO[] estado de transferencias firmadas.
     * @throws Exception se lanza cuando hay error de sistema.
     * @since 2.7
     */
    public EstadoFirmaVO[] firmarTransferenciasPNOL(String rutEmpresa, String digitoVerifEmp, String convenioEmpresa,
        String rutUsu, String digitoVerifUsu, String[] transfAFirmar,
        int topeIndustria, CabeceraFirmaNominaPnolTO cabeceraFirma) throws Exception {

        try{
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[firmarTransferenciasPNOL][" + rutEmpresa + "]["+ convenioEmpresa +"][BCI_INI]" );
                getLogger().info("[firmarTransferenciasPNOL][" + rutEmpresa + "]["+ convenioEmpresa +"] rutUsu ["+ rutUsu +"] - ["+ digitoVerifUsu +"], transfAFirmar ["+ transfAFirmar +"], topeIndustria ["+ topeIndustria +"], cabeceraFirma ["+ cabeceraFirma +"]");
            }
            int bloque   = 1;
            int maxReg   = Integer.parseInt(TablaValores.getValor(ARCHIVO_PARAMETROS, "maxPaginacion", 
                "Desc").trim());
            BCIExpressDelegate delegateEXP = new BCIExpressDelegate();

            if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[firmarTransferenciasPNOL][" + rutEmpresa + "]["+ convenioEmpresa +"]BCIExpressDelegate [" + delegateEXP + "]"); }

            boolean continua = true;
            String tipotrans  = "";
            String cuentaTrans = "";
            int numtransfAFirmar = transfAFirmar.length;
            EstadoFirmaVO[] respuestaFirma = new EstadoFirmaVO[transfAFirmar.length];

            for (int indice=0; indice < numtransfAFirmar; indice++){
                try {
                    continua = true;
                    respuestaFirma[indice] = new EstadoFirmaVO();
                    respuestaFirma[indice].setEstado("");
                    respuestaFirma[indice].setGlosaEstado("");
                    respuestaFirma[indice].setNumeroTransferencia(transfAFirmar[indice]);

                    try {
                        long rutLong = Long.parseLong(rutEmpresa);
                        char dvChar = digitoVerifEmp.charAt(0);
                        this.eliminaDetalleCuentasNoInscritas(convenioEmpresa, rutLong, dvChar,
                            transfAFirmar[indice]);
                    } catch (Exception e){
                        if (getLogger().isEnabledFor(Level.ERROR)) { getLogger().error("[firmarTransferenciasPNOL][" + rutEmpresa + "]["+ convenioEmpresa +"][BCI_FINEX][Exception e][" + e.getMessage() + "]");}
                    }

                    if(getLogger().isEnabledFor(Level.INFO)){
                        getLogger().info("[firmarTransferenciasP][" + rutEmpresa + "][" 
                            + rutUsu + "-" + digitoVerifUsu + " num:" + transfAFirmar[indice] 
                                + "] -> Consulta Estado TRF");
                    }

                    FiltroConsultarTransferenciasVO filtro = creaFiltro(rutEmpresa, convenioEmpresa, 
                        transfAFirmar[indice], PENDIENTE);

                    EncabezadoTransferenciaVO[] transfEnca = this.extraeTransferenciasPendientes(filtro, bloque, 
                        maxReg);

                    if (transfEnca == null){
                        if (getLogger().isEnabledFor(Level.ERROR)){
                            getLogger().error("[firmarTransferenciasPNOL][" + rutEmpresa + "]["+ convenioEmpresa +"] transferencia ["+ transfAFirmar[indice] + "]TODOS LOS DETALLES RECHAZADOS");
                            getLogger().error("[firmarTransferenciasPNOL][" + rutEmpresa + "]["+ convenioEmpresa +"] no existen transferencias pendientes ");
                        }

                        respuestaFirma[indice].setEstado(ERROR);
                        respuestaFirma[indice].setGlosaEstado(TablaValores.getValor(TABLA_PARAMETROS_ERROR, 
                            "TRF034", "Desc"));
                        respuestaFirma[indice].setCantidadDetalles(0);
                        continue;
                    }

                    for(int indice1 = 0; indice1 < transfEnca.length; indice1++){
                        respuestaFirma[indice].setNombreTransferencia(transfEnca[indice1].getNombreTransferencia());
                        respuestaFirma[indice].setCantidadDetalles(transfEnca[indice1].getCantidadDetalles());
                        respuestaFirma[indice].setMonto(transfEnca[indice1].getMonto());
                        if ( !PENDIENTE.equals(transfEnca[indice1].getEstado()) ) {
                            if (getLogger().isEnabledFor(Level.ERROR)) { 
                                getLogger().error("[firmarTransferenciasPNOL][" + rutEmpresa + "]["+ convenioEmpresa +"] Estado de TRF ["+ transfAFirmar[indice] +"]es distinto de PEN");
                            }
                            respuestaFirma[indice].setEstado(ERROR);
                            respuestaFirma[indice].setGlosaEstado(TablaValores.getValor(TABLA_PARAMETROS_ERROR,
                                "TRF041", "Desc"));
                            respuestaFirma[indice].setCantidadDetalles(0);
                            continua = false;
                            break;
                        }
                        tipotrans = transfEnca[indice1].getTipo();
                        cuentaTrans = transfEnca[indice1].getCuentaOrigen();
                    }
                    if (!continua) continue;

                    ResultConsultarApoderadosquehanFirmado apoderados = 
                        delegateEXP.consultarApoderadosquehanFirmado(rutEmpresa, convenioEmpresa, 
                            transfAFirmar[indice]);

                    ConApoquehanFirmado[] apoFir = apoderados.getConApoquehanFirmado();

                    if (getLogger().isEnabledFor(Level.INFO)) { 
                        getLogger().info("[firmarTransferenciasPNOL][" + rutEmpresa + "]["+ convenioEmpresa +"] Apoderados que han firmado ["+ (apoFir != null ? apoFir.length: -1)+"]");    
                    }

                    for( int indice2 = 0; indice2 < apoFir.length; indice2++ ){
                        if ( rutUsu.equals(apoFir[indice2].getRutUsuario())) {
                            if (getLogger().isEnabledFor(Level.ERROR)) { 
                                getLogger().error("[firmarTransferenciasPNOL][" + rutEmpresa + "]["+ convenioEmpresa +"] Apoderado ya Firm� la transferencia ["+ transfAFirmar[indice] +"] ");
                            }
                            respuestaFirma[indice].setEstado(ERROR);
                            respuestaFirma[indice].setGlosaEstado(TablaValores.getValor(TABLA_PARAMETROS_ERROR, 
                                "EXP-015", "Desc"));
                            respuestaFirma[indice].setCantidadDetalles(0);
                            continua = false;
                            break;
                        }
                    }
                    if (!continua) continue;

                    TransferenciasVO[] arrTransferenciasPendientes =
                        this.extraeDetallesTransferenciaPendiente(rutEmpresa, convenioEmpresa, 
                            transfAFirmar[indice], bloque, maxReg);

                    double montoTotalOTB = 0;
                    double montoTotalBCI = 0;

                    if(arrTransferenciasPendientes != null && arrTransferenciasPendientes.length > 0){
                        String nominasPnol = TablaValores.getValor(ARCHIVO_ADMIN, "tiposNominasPNOL", "tipos");
                        String[] arrgloTipo = StringUtil.divide(nominasPnol, ",");
                        List lista = Arrays.asList(arrgloTipo);
                        if (getLogger().isEnabledFor(Level.INFO)) { 
                            getLogger().info("[firmarTransferenciasPNOL][" + rutEmpresa + "]["+ convenioEmpresa +"] tipotrans : [" + tipotrans + "], nominasPnol : [" + nominasPnol +"]");
                        }

                        for (int i=0; i < arrTransferenciasPendientes.length; i++){
                            if (!arrTransferenciasPendientes[i].getBancoDestino().equals("016")) {
                                if (lista.contains(tipotrans)) {
                                    FiltroConsultarTransferenciasVO respMonto = 
                                        validaMontosYTopesEnLinea(cuentaTrans, topeIndustria, 
                                            arrTransferenciasPendientes[i]);
                                    if (respMonto.getEstado().equals(ERROR)) {
                                        if (getLogger().isEnabledFor(Level.ERROR)) { 
                                            getLogger().error("[firmarTransferenciasPNOL][" + rutEmpresa + "]["+ convenioEmpresa +"] Transferencia ["+ transfAFirmar[indice] +"] ha superado Monto Industria ");
                                        }
                                        respuestaFirma[indice].setEstado(respMonto.getEstado());
                                        respuestaFirma[indice].setGlosaEstado(respMonto.getGlosa());
                                        respuestaFirma[indice].setCantidadDetalles(0);
                                        continua = false;
                                        break;
                                    }
                                }
                                if (PENDIENTE.equals(arrTransferenciasPendientes[i].getEstado())){ 
                                    montoTotalOTB = montoTotalOTB + arrTransferenciasPendientes[i].getMonto();
                                }
                            }
                            else
                                montoTotalBCI = montoTotalBCI + arrTransferenciasPendientes[i].getMonto();
                        }
                    }

                    if (!continua) continue;
                    String servicioConsMonto      = TablaValores.getValor(ARCH_PARAM_EMP, "IngresoTerceros", 
                        "Desc");
                    FiltroConsultarTransferenciasVO montosAut = montoMaximoYAutorizado(cuentaTrans, rutUsu, 
                        digitoVerifUsu.charAt(0), rutEmpresa, digitoVerifEmp.charAt(0), convenioEmpresa, 
                        topeIndustria, servicioConsMonto, montoTotalOTB);
                    if (montosAut.getEstado().equals(ERROR)) {
                        if (getLogger().isEnabledFor(Level.ERROR)) { 
                            getLogger().error("[firmarTransferenciasPNOL][" + rutEmpresa + "]["+ convenioEmpresa +"] Transferencia ["+ transfAFirmar[indice] +"] ha superado Monto convenio ");
                        }
                        respuestaFirma[indice].setEstado(montosAut.getEstado());
                        respuestaFirma[indice].setGlosaEstado(montosAut.getGlosa());
                        respuestaFirma[indice].setCantidadDetalles(0);
                        continue;
                    }
                    if ("M".equals(cabeceraFirma.getResultadoFyPMono())) {
                        respuestaFirma[indice].setEstado("OK");
                        String numeroEventoJournal = this.journalizacionFirmaPnol(rutEmpresa, 
                            digitoVerifEmp, convenioEmpresa, rutUsu, digitoVerifUsu, 
                            respuestaFirma[indice].getNumeroTransferencia(), cabeceraFirma);
                        this.registraEvento(rutUsu, digitoVerifUsu, convenioEmpresa, rutEmpresa, 
                            digitoVerifEmp, transfAFirmar[indice], new Date(), FIRMADA );
                        String autoApo = TablaValores.getValor(ARCH_PARAM_EMP, "UsuarioEpyme", "autoApo");
                        ServiciosPagosDelegate delegatePagos = ServiciosPagosDelegate.getInstance();
                        delegatePagos.insertaAutApo(Long.parseLong(rutUsu),
                            digitoVerifUsu.charAt(0),
                            convenioEmpresa,
                            Long.parseLong(rutEmpresa),
                            digitoVerifEmp.charAt(0),
                            transfAFirmar[indice],
                            FechasUtil.convierteDateAString(new Date(),"yyyy/MM/dd"),
                            autoApo);
                    } else {
                        FiltroConsultarTransferenciasVO respAutFyP = autorizaPagoFyP(transfAFirmar[indice], 
                            rutUsu, digitoVerifUsu.charAt(0), rutEmpresa, digitoVerifEmp.charAt(0), 
                            convenioEmpresa);
                        respuestaFirma[indice].setEstado(respAutFyP.getEstado());
                        respuestaFirma[indice].setGlosaEstado(respAutFyP.getGlosa());
                        if (respuestaFirma[indice].getEstado().equals("OK") 
                            || respuestaFirma[indice].getEstado().equals(PENDIENTE)) {

                            String numeroEventoJournal = this.journalizacionFirmaPnol(rutEmpresa, 
                                digitoVerifEmp, convenioEmpresa, rutUsu, digitoVerifUsu, 
                                respuestaFirma[indice].getNumeroTransferencia(), cabeceraFirma);

                            this.registraEvento(rutUsu, digitoVerifUsu, convenioEmpresa, rutEmpresa, 
                                digitoVerifEmp, transfAFirmar[indice], new Date(), FIRMADA );
                        }

                        if (!respuestaFirma[indice].getEstado().equals("OK")){
                            respuestaFirma[indice].setCantidadDetalles(0);
                            continue;
                        }
                    }

                    NominaDeTransferenciasDelegate delegateNom = NominaDeTransferenciasDelegate.getInstance();
                    boolean cambEstado = delegateNom.cambiaEstadoTransferencia(rutUsu, digitoVerifUsu, 
                        convenioEmpresa, rutEmpresa, digitoVerifEmp, transfAFirmar[indice], 0, "FIR", "ENCA");


                    if (!cambEstado) {
                        respuestaFirma[indice].setEstado(ERROR);
                        respuestaFirma[indice].setGlosaEstado(TablaValores.getValor(TABLA_PARAMETROS_ERROR, 
                            "TRF020", "Desc"));
                        respuestaFirma[indice].setCantidadDetalles(0);

                        if (getLogger().isEnabledFor(Level.ERROR)) { 
                            getLogger().error("[firmarTransferenciasPNOL][" + rutEmpresa + "]["+ convenioEmpresa +"] Voy a borrar la Autorzacion de la tabla AUT correspondiente a la nomina [" + transfAFirmar[indice] +"]"); 
                        }
                        delegateEXP.borraTransferenciaAUT(rutUsu, digitoVerifUsu.charAt(0), convenioEmpresa, 
                            rutEmpresa, digitoVerifEmp.charAt(0), transfAFirmar[indice]);
                    }
                } catch (Exception ex) {
                    if (getLogger().isEnabledFor(Level.ERROR)) { 
                        getLogger().error("[firmarTransferenciasPNOL][" + rutEmpresa + "]["+ convenioEmpresa +"][BCI_FINEX] Transferencia" + transfAFirmar[indice] + "][Exception ex][" + ex.getMessage() + "]");
                    }
                    respuestaFirma[indice].setEstado(ERROR);
                    respuestaFirma[indice].setGlosaEstado(TablaValores.getValor(TABLA_PARAMETROS_ERROR, 
                        "TRANSFERENCIAS", "Desc"));
                    respuestaFirma[indice].setCantidadDetalles(0);
                }
            }
            if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[firmarTransferenciasPNOL][" + rutEmpresa + "]["+ convenioEmpresa +"][BCI_FINOK] Retorno firma ["+ respuestaFirma +"]");}
            return respuestaFirma;

        } catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) { 
                getLogger().error("[firmarTransferenciasPNOL][" + rutEmpresa + "]["+ convenioEmpresa +"][BCI_FINEX] convenioEmpresa: [" + convenioEmpresa + "][Exception e][" + e.getMessage() + "]", e);
            }
            throw new Exception("Error al realizar la Transferencia");
        }
    }

    /**
     * <p>M�todo crea el filtro para la firma de transferencias.
     * <br><br>
     *   M�todo migrado desde {@link M�todo migrado desde 
     *   {@link wcorp.aplicaciones.productos.servicios.transferencias.nominas.struts.helper.TransferenciasHelper}}
     * </p>
     * <p>
     * Registro de versiones:<ul>
     * <li> 1.0 10/02/2014, Jaime Suazo D. (Experimento Social. SPA.): versi�n inicial.</li>
     * <li> 1.1 29/12/2014 Rafael Pizarro (TINet): Se normaliza log de metodo
     * para ajustarse a Normativa Log4j de BCI.</li>
     * </ul>
     * <p>
     *
     * @param rutEmpresa rut de la empresa
     * @param convenioEmpresa convenio de la empresa
     * @param transfAFirmar n�mero de la transferencia a firmar
     * @param estado estado de la transferencias por la cual se desea filtrar
     * @return Objeto que corresponde a un filtro utilizado para realizar consultas
     *
     * @since 2.7
     */    
    public FiltroConsultarTransferenciasVO creaFiltro(String rutEmpresa, String convenioEmpresa, 
        String transfAFirmar, String estado){

        if (getLogger().isEnabledFor(Level.INFO)) { 
            getLogger().info("[creaFiltro][" + transfAFirmar +"][BCI_INI]" );
            getLogger().info("[creaFiltro][" + transfAFirmar +"] rutEmpresa ["+ rutEmpresa +"], convenioEmpresa ["+ convenioEmpresa +"], estado ["+ estado +"]");
        }
        
        FiltroConsultarTransferenciasVO filtro = new FiltroConsultarTransferenciasVO();
        Calendar calFechaDesde = Calendar.getInstance();
        calFechaDesde.setTime(new Date());
        calFechaDesde.add(Calendar.MONTH, MESES_ANTERIORES_CONSULTA);
        Date fechaDesde = calFechaDesde.getTime();
        Calendar calFechaHasta = Calendar.getInstance();
        calFechaHasta.setTime(new Date());
        calFechaHasta.add(Calendar.DAY_OF_MONTH, 1);
        Date fechaHasta = calFechaHasta.getTime();

        filtro.setRutOrigen(rutEmpresa);
        filtro.setConvenio(convenioEmpresa);
        filtro.setNumeroTransferencia(transfAFirmar);
        filtro.setEstado(estado);
        filtro.setFechaDesde(fechaDesde);
        filtro.setFechaHasta(fechaHasta);
        filtro.setCuentaCorriente("");
        filtro.setRutDestino("");
        filtro.setGlosa("");

        if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[creaFiltro][" + transfAFirmar +"][BCI_FINOK] filtro[" + StringUtil.contenidoDe(filtro) + "]");}

        return filtro;
    }    

    /**
     * <p>M�todo realiza la l�gica para la autorizaci�n del pago por Firmas y Poderes. </p>
     * <br><br>
     *   M�todo migrado desde {@link M�todo migrado desde 
     *   {@link wcorp.aplicaciones.productos.servicios.transferencias.nominas.struts.helper.TransferenciasHelper}} 
     * <p>
     * Registro de versiones:<ul>
     * <li> 1.0 10/02/2014, Jaime Suazo D. (Experimento Social. SPA.): versi�n inicial.</li>
     * </ul>
     * <p>
     *
     * @param numTransferencia n�mero de la transferencia
     * @param rutUsu rut del usuario que realiza la autorizaci�n de pago
     * @param digitoVerifUsu d�gito verificador del usuario
     * @param rutEmpresa rut de la empresa
     * @param digitoVerifEmp d�gito verificador de la empresa
     * @param convenioEmpresa convenio de la empresa
     * @return el resultado de la autorizaci�n de pago
     *
     * @since 2.7
     */    
    public FiltroConsultarTransferenciasVO autorizaPagoFyP(String numTransferencia, String rutUsu, 
        char digitoVerifUsu, String rutEmpresa, char digitoVerifEmp, String convenioEmpresa) {

        FiltroConsultarTransferenciasVO   respuestaFirma = null;
        ResultAutorizaPagoCuentaCorriente resAut         = null;
        String  autorizacion                             = "";

        if (getLogger().isEnabledFor(Level.INFO)) { 
            getLogger().info("[autorizaPagoFyP]["+ numTransferencia +"]["+ convenioEmpresa +"][BCI_INI]" );
            getLogger().info("[autorizaPagoFyP]["+ numTransferencia +"]["+ convenioEmpresa +"] rutUsu ["+ rutUsu +"] - ["+ digitoVerifUsu +"], rutEmpresa ["+ rutEmpresa +"] - ["+ digitoVerifEmp +"]");
        }

        try {
            String autorTransac = TablaValores.getValor(ARCH_PARAM_EMP, "AutoTransac",     "Desc");
            String fecExpiracion  = TablaValores.getValor(ARCH_PARAM_EMP, "FechaExpiracion", "Desc");
            BCIExpressDelegate delegateEXP = new BCIExpressDelegate();
            resAut = delegateEXP.autorizaPagoCuentaCorriente(rutUsu, digitoVerifUsu, convenioEmpresa, rutEmpresa,
                digitoVerifEmp, numTransferencia, autorTransac.charAt(0), fecExpiracion);
            AutorizaPagoCtaCte[] autPagoCtaCte = resAut.getAutorizaPagoCtaCte();
            for (int i = 0; i < autPagoCtaCte.length; i++) {
                if (autPagoCtaCte[i] == null) {
                    break;
                }
                autorizacion = autPagoCtaCte[i].getCodigo();
            }
            if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[autorizaPagoFyP]["+ numTransferencia +"]["+ convenioEmpresa +"] Autorizacion Firmada ["+ autorizacion +"]");}
            respuestaFirma = new FiltroConsultarTransferenciasVO();
            if (autorizacion.equals("0")) {
                respuestaFirma.setEstado(PENDIENTE);
                respuestaFirma.setGlosa(TablaValores.getValor(TABLA_PARAMETROS_ERROR, "EXP-013", "Desc"));
            } else if (autorizacion.equals("100")) {
                respuestaFirma.setEstado(ERROR);
                respuestaFirma.setGlosa(TablaValores.getValor(TABLA_PARAMETROS_ERROR, "EXP-014", "Desc"));
            } else if (autorizacion.equals("101")) {
                respuestaFirma.setEstado(ERROR);
                respuestaFirma.setGlosa(TablaValores.getValor(TABLA_PARAMETROS_ERROR, "EXP-015", "Desc"));
            } else if (autorizacion.equals("102")) {
                respuestaFirma.setEstado(ERROR);
                respuestaFirma.setGlosa(TablaValores.getValor(TABLA_PARAMETROS_ERROR, "EXP-016", "Desc"));
            } else if (autorizacion.equals("1")) {
                respuestaFirma.setEstado("OK");
                respuestaFirma.setGlosa(TablaValores.getValor(ARCHIVO_PARAMETROS, "PRP", "DescExt"));
            } else {
                respuestaFirma.setEstado(ERROR);
                respuestaFirma.setGlosa(TablaValores.getValor(TABLA_PARAMETROS_ERROR, "EXP-017", "Desc"));
            }
        } catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) { getLogger().error("[autorizaPagoFyP]["+ numTransferencia +"]["+ convenioEmpresa +"][BCI_FINEX][Exception e][" + e.getMessage() + "]", e); }
            respuestaFirma.setEstado(ERROR);
            respuestaFirma.setGlosa(TablaValores.getValor(TABLA_PARAMETROS_ERROR, "EXP-017", "Desc"));
        }

        if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[autorizaPagoFyP]["+ numTransferencia +"]["+ convenioEmpresa +"][BCI_FINOK] Autorizacion Mensaje " + respuestaFirma.getGlosa()); }
        return respuestaFirma;
    }

    /**
     * <p>M�todo valida los montos y topes a transferir por detalle. </p>
     * <br><br>
     *   M�todo migrado desde {@link M�todo migrado desde 
     *   {@link wcorp.aplicaciones.productos.servicios.transferencias.nominas.struts.helper.TransferenciasHelper}}
     * <p>
     * Registro de versiones:<ul>
     * <li> 1.0 10/02/2014, Jaime Suazo D. (Experimento Social. SPA): versi�n inicial.</li>
     * <li> 1.1 29/12/2014 Rafael Pizarro (TINet): Se normaliza log de metodo
     * para ajustarse a Normativa Log4j de BCI</li>
     * </ul>
     * <p>
     *
     * @param cuentaTrans n�mero de la transferencia
     * @param topeIndustria tope m�ximo por convenio industria
     * @param detalle con los datos de un detalle de una transferencia
     * @return el resultado de la validaci�n de montos por detalle
     *
     * @since 2.7
     */    
    public FiltroConsultarTransferenciasVO validaMontosYTopesEnLinea(String cuentaTrans, double topeIndustria,
        TransferenciasVO detalle){
        FiltroConsultarTransferenciasVO respuesta = new FiltroConsultarTransferenciasVO();

        if (getLogger().isEnabledFor(Level.INFO)) { 
            getLogger().info("[validaMontosYTopesEnLinea][" + cuentaTrans + "][BCI_INI]" );
            getLogger().info("[validaMontosYTopesEnLinea][" + cuentaTrans + "] topeIndustria ["+ topeIndustria +"], detalle ["+ detalle +"]");
        }

        try {
            TransferenciasDeFondosDelegate  delegateTrans  = TransferenciasDeFondosDelegate.getInstance();
            int bloque = 1;
            double monto = 0;

            int maxReg = Integer.parseInt(TablaValores.getValor(ARCHIVO_PARAMETROS, "maxPaginacion",
                "Desc").trim());
            double topePorRutBeneficiario = Double.parseDouble(TablaValores.getValor(ARCHIVO_PARAMETROS, 
                "topesIndustria", "topePorRutBeneficiario"));

            respuesta.setEstado("OK");
            respuesta.setGlosa("");

            TransferenciasRecibidasVO[] transEnvBenef = 
                delegateTrans.consultaTransferenciasEnviadasCliente(detalle.getRutDestino(), detalle.getDvDestino(),
                    new Date(), detalle.getCuentaDestino(), bloque, maxReg);

            if(transEnvBenef != null){
                for(int j=0; j<transEnvBenef.length; j++){
                    if (cuentaTrans.equals(transEnvBenef[j].getCuentaOrigen())) {
                        monto = monto + transEnvBenef[j].getMonto();
                    }
                }
            }
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[validaMontosYTopesEnLinea][" + cuentaTrans + "] Monto del detalle() [" + detalle.getMonto() +"],tope monto Industria [" + topeIndustria + "],monto [" + monto + "],tope Por Rut Beneficiario [" + topePorRutBeneficiario +"]"); 
            }

            if ((detalle.getMonto() + monto) > topeIndustria || (detalle.getMonto() 
                + monto) > topePorRutBeneficiario) {
                respuesta.setEstado(ERROR);
                respuesta.setGlosa(TablaValores.getValor(TABLA_PARAMETROS_ERROR, "TRF042", "Desc"));
            }

            if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[validaMontosYTopesEnLinea][" + cuentaTrans + "][BCI_FINOK] respuesta ["+ respuesta +"]");}

        } catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[validaMontosYTopesEnLinea][" + cuentaTrans + "][BCI_FINEX] detalle [" + detalle + "][Exception e][" + e.getMessage() + "]", e);
            }
            respuesta.setEstado(ERROR);
            respuesta.setGlosa(TablaValores.getValor(TABLA_PARAMETROS_ERROR, "TRF042", "Desc"));
        }
        return respuesta;
    }

    /**
     * <p>M�todo valida los montos y topes a transferir por encabezado. </p>
     * <br><br>
     *   M�todo migrado desde {@link M�todo migrado desde 
     *   {@link wcorp.aplicaciones.productos.servicios.transferencias.nominas.struts.helper.TransferenciasHelper}}
     * <p>
     * Registro de versiones:<ul>
     * <li> 1.0 10/02/2014, Jaime Suazo D. (Experimento Social. SPA): versi�n inicial.</li>
     * <li> 1.1 29/12/2014 Rafael Pizarro (TINet): Se normaliza log de metodo
     * para ajustarse a Normativa Log4j de BCI</li>
     * </ul>
     * <p>
     *
     * @param cuentaTrans n�mero de la transferencia
     * @param rutUsu rut del usuario que realiza la autorizaci�n de pago
     * @param digitoVerifUsu d�gito verificador del usuario
     * @param rutEmpresa rut de la empresa
     * @param digitoVerifEmp d�gito verificador de la empresa
     * @param convenioEmpresa convenio de la empresa
     * @param topeIndustria tope m�ximo por convenio industria
     * @param servicioConsMonto c�digo de servicio para consultar monto m�ximo autorizado
     * @param montoTotalOTB monto total transferido a otros bancos durante el d�a
     * @return el resultado de la validaci�n de montos por encabezado
     *
     * @since 2.7
     */    
    public FiltroConsultarTransferenciasVO montoMaximoYAutorizado(String cuentaTrans, String rutUsu, 
        char digitoVerifUsu, String rutEmpresa, char digitoVerifEmp, String convenioEmpresa, 
        int topeIndustria, String servicioConsMonto, double montoTotalOTB){

        FiltroConsultarTransferenciasVO respuesta = new FiltroConsultarTransferenciasVO();

        String maximo    = "";
        String utilizado = "";


        if (getLogger().isEnabledFor(Level.INFO)) { 
            getLogger().info("[montoMaximoYAutorizado][" + cuentaTrans + "][BCI_INI]" );
            getLogger().info("[montoMaximoYAutorizado][" + cuentaTrans + "] rutUsu ["+ rutUsu +"] - ["+ digitoVerifUsu +"], rutEmpresa ["+ rutEmpresa +"] - ["+ digitoVerifEmp +"], convenioEmpresa ["+ convenioEmpresa +"],");
            getLogger().info("[montoMaximoYAutorizado][" + cuentaTrans + "] topeIndustria ["+ topeIndustria +"], servicioConsMonto ["+ servicioConsMonto +"], montoTotalOTB ["+ montoTotalOTB +"]");
        }
        try {
            BCIExpressDelegate delegateEXP = new BCIExpressDelegate();

            ResultMontoMaximoYAutorizado monto = delegateEXP.montoMaximoYAutorizado(cuentaTrans, 
                rutUsu, digitoVerifUsu, rutEmpresa, digitoVerifEmp, convenioEmpresa,
                topeIndustria, servicioConsMonto);

            MontoMaximoYAutor[] montoMaxYAutor = monto.getMontoMaximoYAutor();

            respuesta.setEstado("OK");
            respuesta.setGlosa("");

            if (montoMaxYAutor[0] != null) {
                maximo    = montoMaxYAutor[0].getMontoMaxTransfer();
                utilizado = montoMaxYAutor[0].getMontoUtilizado();
            } else {
                if (getLogger().isEnabledFor(Level.INFO)) { 
                    getLogger().info("[montoMaximoYAutorizado][" + cuentaTrans + "]Problema al encontrar monto maximo permitido CEC");
                }
                throw new Exception();
            }

            if (maximo.indexOf(',') != -1) {
                maximo = maximo.substring(0, maximo.indexOf('.'));
            }
            if (utilizado.indexOf('.') != -1) {
                utilizado = utilizado.substring(0, utilizado.indexOf('.'));
            }

            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[montoMaximoYAutorizado][" + cuentaTrans + "] obtuvimos datos CEC.-, convenioEmpresa [" + convenioEmpresa + "], maximo [" + maximo + "], convenioEmpresa [" + convenioEmpresa + "], utilizado [" + utilizado + "]");
            }

            if (montoTotalOTB > (Double.parseDouble(maximo) - Double.parseDouble(utilizado))){
                if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[montoMaximoYAutorizado][" + cuentaTrans + "] Se ha superado el monto m�ximo para transf de la Cta");}
                throw new Exception();
            }
        } catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) { getLogger().error("[montoMaximoYAutorizado][" + cuentaTrans + "][BCI_FINEX][Exception e][" + e.getMessage() + "]", e); }
            respuesta.setEstado(ERROR);
            respuesta.setGlosa(TablaValores.getValor(TABLA_PARAMETROS_ERROR, "TRF042", "Desc"));
        }
        if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[montoMaximoYAutorizado][" + cuentaTrans + "][BCI_FINOK] respuesta ["+ respuesta +"]");}
        
        return respuesta;
    } 

    /**
     * <p>M�todo que rescata si el usuario utiliza Express o EPyme
     * </p>
     * <br><br>
     *   M�todo migrado desde {@link M�todo migrado desde 
     *   {@link wcorp.aplicaciones.productos.servicios.transferencias.nominas.struts.helper.TransferenciasHelper}}
     * <p>
     * Registro de versiones:<ul>
     * <li> 1.0 10/02/2014, Jaime Suazo D. (Experimento Social. SPA): versi�n inicial.</li>
     * </ul>
     * <p>
     *
     * @param rutUsuario rut del usuario que realiza el rescate del producto
     * @param convenioEmpresa convenio de la empresa
     * @param rutEmpresa rut de la empresa
     * @return el c�digo del producto obtenido
     *
     * @since 2.7
     */    
    public String rescatarProducto(String rutUsuario, String convenioEmpresa, String rutEmpresa){

        String tipoProducto = "";
        if (getLogger().isEnabledFor(Level.INFO)) { 
            getLogger().info("[rescatarProducto][" + rutEmpresa + "]["+ convenioEmpresa +"][BCI_INI]" );
            getLogger().info("[rescatarProducto][" + rutEmpresa + "]["+ convenioEmpresa +"] rutUsuario ["+ rutUsuario +"]");
        }

        try {
            BCIExpressDelegate delegate = new BCIExpressDelegate();
            if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[rescatarProducto][" + rutEmpresa + "]["+ convenioEmpresa +"] delegate [" + delegate + "]");}

            ResultConsultaPerfilesUCEP respuestaProducto21 = ((ResultConsultaPerfilesUCEP) 
                delegate.consultaPerfilesUCEP(rutUsuario, convenioEmpresa, rutEmpresa, PRODUCTO_EPYME));

            tipoProducto = (respuestaProducto21.getPerfilesUCEP()).length > 0 
                ? PRODUCTO_EPYME : PRODUCTO_EXPRESS;

            if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[rescatarProducto][" + rutEmpresa + "]["+ convenioEmpresa +"][BCI_FINOK] tipoProducto[" + tipoProducto + "]");}
           
        } catch (Exception ex) {
            if (getLogger().isEnabledFor(Level.ERROR)) { getLogger().error("[rescatarProducto][" + rutEmpresa + "]["+ convenioEmpresa +"][BCI_FINEX][Exception ex][" + ex.getMessage() + "]", ex); }
        }
        return tipoProducto;
    }

    /**
     * M�todo que Consulta el Apoderado posee todas las validaciones para poder Autorizar.
     * <br><br>
     *   M�todo migrado desde {@link M�todo migrado desde 
     *   {@link wcorp.aplicaciones.productos.servicios.transferencias.nominas.struts.helper.TransferenciasHelper}}
     * <p>
     * Registro de versiones:<ul>
     * <li> 1.0 10/02/2014, Jaime Suazo D. (Experimento Social. SPA.): versi�n inicial.</li>
     * </ul>
     * <p>
     *
     * @param rutEmpresa Rut Empresa
     * @param digitoVerifEmp D�gito Verificador Empresa
     * @param convenioEmpresa N�mero de Convenio
     * @param rutUsu Rut Usuario
     * @param digitoVerifUsu D�gito Verificador Usuario
     * @return boolean confirmaci�n
     * @throws Exception se lanza cuando hay error de sistema.
     *
     * @since 2.7
     */    
    public boolean consultaTipoApoderado(String rutEmpresa, String digitoVerifEmp, String convenioEmpresa, 
        String rutUsu, String digitoVerifUsu) throws Exception {

        boolean continua = false;
        String posicion = "S";
        try{
            BCIExpressDelegate delegate = new BCIExpressDelegate();
            ResultValidarUsuarioporTipoFirma consultaSBIF =delegate.consultaIndicadorSBIF(rutUsu, rutEmpresa, 
                convenioEmpresa, posicion.charAt(0), "EXP");
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[consultaTipoApoderado][" + rutEmpresa + "]["+ convenioEmpresa +"][BCI_INI]" );
                getLogger().info("[consultaTipoApoderado][" + rutEmpresa + "]["+ convenioEmpresa +"] rutUsu ["+ rutUsu +"] - ["+ digitoVerifUsu +"], posicion [" + posicion + "], consultaSBIF [" + consultaSBIF + "]");
            }
            
            String validacion = consultaSBIF.getValUsuporTipoFirma()[0].getIndicadorStatus();
            if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[consultaTipoApoderado][" + rutEmpresa + "]["+ convenioEmpresa +"] validacion [" + validacion + "]"); }

            if(validacion != null){
                if (validacion.equals("0")) {
                    if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[consultaTipoApoderado][" + rutEmpresa + "]["+ convenioEmpresa +"] Usuario no autorizado....");
                    continua = false;
                    }
                } else if (validacion.equals("1")) {
                    continua = true;
                } else if (validacion.equals("2")) {
                    continua = true;
                } else {
                    if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[consultaTipoApoderado][" + rutEmpresa + "]["+ convenioEmpresa +"] Validacion de usuario erronea");}
                    continua = false;
                }
            }
            if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[consultaTipoApoderado][" + rutEmpresa + "]["+ convenioEmpresa +"][BCI_FINOK] continua [" + continua + "]"); }
            return continua;

        }catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) { getLogger().error("[consultaTipoApoderado][" + rutEmpresa + "]["+ convenioEmpresa +"][BCI_FINEX][Exception e][" + e.getMessage() + "]" , e); }
            throw new Exception("Error al consultar por el Tipo de Apoderado");
        }

    }

    /**
     * M�todo encargado de obtener el modo de firma de usuario de un convenio empresas.
     * 
     * Este m�todo consulta contra firmas y poderes.
     * 
     * Este m�todo es extraido desde {@link 
     * wcorp.aplicaciones.productos.servicios.transferencias.nominas.struts.actions.
     * FirmarTransferenciaAction.firmaTransferencia(ActionMapping, ActionForm, HttpServletRequest, 
     * HttpServletResponse)}}
     * 
     * <ul>
     * <li> 07/02/2014 Jaime Suazo. (Experimento Social. SPA.): Versi�n Inicial</li>
     * </ul>
     *  
     * @param rutEmpresa Rut de empresa.
     * @param digitoVerifEmp D�gito verificador de RUT de empresa.
     * @param convenioEmpresa Convenio Empresa.
     * @param rutUsu Rut de usuario.
     * @param digitoVerifUsu D�gito verificador de usuario.
     * @param canal canal por donde se realiza la transacci�n de firma de n�mina.
     * @param tipoUsuario Tipo de usuario
     * @param perfil perfil de usuario
     * @return retorna el String asociado al tipo de firma del convenio
     * @throws GeneralException Se lanza cuando hay un error de negocio
     * @throws Exception  Se lanza cuando hay error de sistema.
     * 
     * @since 2.7
     */
    private String obtenerModoFirmaEnFyP(String rutEmpresa, String digitoVerifEmp,
        String convenioEmpresa, String rutUsu, String digitoVerifUsu,
        String canal, String tipoUsuario, String perfil) throws GeneralException, Exception {

        if (getLogger().isEnabledFor(Level.INFO)) { 
            getLogger().info("[obtenerModoFirmaEnFyP][" + rutEmpresa + "]["+ convenioEmpresa +"][BCI_INI]" );
            getLogger().info("[obtenerModoFirmaEnFyP][" + rutEmpresa + "]["+ convenioEmpresa +"] rutUsu ["+ rutUsu +"] - ["+ digitoVerifUsu +"], canal ["+ canal +"], tipoUsuario ["+ tipoUsuario +"],perfil ["+ perfil +"]");
        }

        String okFAC = "OKFAC"; 
        String noFyP = "NOFYP";
        String noFAC = "NOFAC";
        String noAPO = "NOAPO";     
        String resultFyPMono = "";

        boolean respuestaConsultaTipoApoderado = false;
        int facultad = Integer.parseInt(TablaValores.getValor(ARCH_PARAM_EMP, "AutoTransac", "Desc"));
        String canelesPermitidos = TablaValores.getValor(ARCHIVO_ADMIN, "canales", "pymeFirma");
        String[] arrgloCanalesPermitidos = StringUtil.divide(canelesPermitidos, ",");
        List listaCanales = Arrays.asList(arrgloCanalesPermitidos);

        if (listaCanales.contains(canal)) {
            long rutTopeFyPEPyme = Long.parseLong(TablaValores.getValor(ARCH_PARAM_EMP,
                "UsuarioEpyme", "rutTope"));

            if (Long.parseLong(rutEmpresa) < rutTopeFyPEPyme) {
                resultFyPMono = "M";
                respuestaConsultaTipoApoderado = true;
            } 
            else {
                AutorizarYFirmarDelegate autorizarYfirmarDelegate = new AutorizarYFirmarDelegate();
                RespuestaCapacidadDeAutorizacionTO respuesta = autorizarYfirmarDelegate.obtenerCapacidadDeGiro(
                    Long.parseLong(rutUsu ), convenioEmpresa, Long.parseLong(rutEmpresa),
                    facultad, tipoUsuario, perfil, canal);
                if (respuesta.isRespuesta() && (respuesta.getCodigoRespuesta().equalsIgnoreCase(okFAC))) {
                    if (getLogger().isEnabledFor(Level.INFO)){getLogger().info("[obtenerModoFirmaEnFyP][" + rutEmpresa + "]["+ convenioEmpresa +"] Respuesta true codigoRespuesta es OKFAC"); }
                    resultFyPMono = "S";
                    respuestaConsultaTipoApoderado = true;
                }
                else if (respuesta.isRespuesta() && (
                    respuesta.getCodigoRespuesta().equalsIgnoreCase(noFyP))) {
                    if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[obtenerModoFirmaEnFyP][" + rutEmpresa + "]["+ convenioEmpresa +"] Respuesta true codigoRespuesta es NOFYP"); }
                    resultFyPMono = "M";
                    respuestaConsultaTipoApoderado = true;
                }
                else if (!respuesta.isRespuesta()
                    && (respuesta.getCodigoRespuesta().equalsIgnoreCase(noFAC) || respuesta
                        .getCodigoRespuesta().equalsIgnoreCase(noAPO))) {
                    if (getLogger().isEnabledFor(Level.ERROR)) { getLogger().error("[obtenerModoFirmaEnFyP][" + rutEmpresa + "]["+ convenioEmpresa +"] espuesta false codigoRespuesta es ["+ respuesta +"]");}                      
                    resultFyPMono = "N";
                    throw new GeneralException("TRF043");

                }
            }
        } 
        else {
            resultFyPMono = validaFyPoMono(rutEmpresa, 
                digitoVerifEmp, convenioEmpresa, rutUsu, digitoVerifUsu); 

            if("M".equals(resultFyPMono))
                respuestaConsultaTipoApoderado  = true;
            else
                respuestaConsultaTipoApoderado  = consultaTipoApoderado(rutEmpresa, 
                    digitoVerifEmp, convenioEmpresa, rutUsu, digitoVerifUsu); 
        }

        if("N".equals(resultFyPMono)){
            if (getLogger().isEnabledFor(Level.ERROR)) { 
                getLogger().error("[obtenerModoFirmaEnFyP][" + rutEmpresa + "]["+ convenioEmpresa +"] este convenio no opera con FyP, por ende no podra hacer uso de este servicio");
            }
            throw new GeneralException("TRF044");
        }

        if (!respuestaConsultaTipoApoderado) {   
            if (getLogger().isEnabledFor(Level.ERROR)) { 
                getLogger().error("[obtenerModoFirmaEnFyP][" + rutEmpresa + "]["+ convenioEmpresa +"] Apoderado [" + rutUsu + "] No es un Apoderado Real");
            }
            throw new GeneralException("TRF045");
        }
        if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[obtenerModoFirmaEnFyP][" + rutEmpresa + "]["+ convenioEmpresa +"][BCI_FINOK]");}
        return resultFyPMono;
    }


    /**
     * M�todo encargado validar el monto disponible de cuenta de cargo asociada a las n�minas de un convenio 
     * empresas.
     * 
     * Este m�todo es extraido desde {@link 
     * wcorp.aplicaciones.productos.servicios.transferencias.nominas.struts.actions.FirmarTransferenciaAction.
     * firmaTransferencia(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)}}
     * 
     * <ul>
     * <li> 07/02/2014 Jaime Suazo. (Experimento Social. SPA.): Versi�n Inicial</li>
     * </ul>
     *  
     * @param rutEmpresa Rut de empresa
     * @param transferenciasPendientes Transferencias pendientes del convenio.
     * @param transferenciasParaFirmar Transferencias a firmar del convenio.
     * @param cabecera TO que transporta los datos necesarios para realizar las validaciones previasa para la 
     * firma de n�minas.
     * @throws GeneralException Se lanza cuando hay error de negocio.
     */
    private void validarDisponibleDeCtaCargoNominaConMontoNomina(
        String rutEmpresa, EncabezadoTransferenciaVO[] transferenciasPendientes,
        String[] transferenciasParaFirmar, CabeceraFirmaNominaPnolTO cabecera) throws GeneralException {

        if (getLogger().isEnabledFor(Level.INFO)) { 
            getLogger().info("[validarDisponibleDeCtaCargoNominaConMontoNomina][" + rutEmpresa + "][BCI_INI]" );
            getLogger().info("[validarDisponibleDeCtaCargoNominaConMontoNomina][" + rutEmpresa + "] transferenciasPendientes ["+ transferenciasPendientes.toString() +"], transferenciasParaFirmar ["+ transferenciasParaFirmar.toString() +"], cabecera ["+ cabecera.toString() +"]");
        }

        HashMap totalesPorCuenta = new HashMap();
        if (transferenciasParaFirmar != null && transferenciasParaFirmar.length > 0 ){
            for (int j=0; j < transferenciasParaFirmar.length;j++){

                String nroTrfAFirmar = transferenciasParaFirmar[j];

                if (transferenciasPendientes !=null && transferenciasPendientes.length > 0){
                    for (int i=0 ; i < transferenciasPendientes.length;i++){

                        if (nroTrfAFirmar.equals(transferenciasPendientes[i].getNumeroTransferencia())){
                            String cuentaOrigen = transferenciasPendientes[i].getCuentaOrigen();
                            double montoTransferencia = transferenciasPendientes[i].getMonto();
                            if (totalesPorCuenta.containsKey(cuentaOrigen)){
                                double montoAcumulado = 
                                    Double.parseDouble(String.valueOf(totalesPorCuenta.get(cuentaOrigen)));
                                totalesPorCuenta.put(cuentaOrigen, new Double(montoAcumulado 
                                    + montoTransferencia));
                            }
                            else{
                                totalesPorCuenta.put(cuentaOrigen, new Double(montoTransferencia));
                            }
                            continue;
                        }
                    }
                }
            }
        }
        else {
            if (getLogger().isEnabledFor(Level.ERROR)) { 
                getLogger().error("[validarDisponibleDeCtaCargoNominaConMontoNomina][" + rutEmpresa + "] No se pudo obtener listado de transferencias a firmar. Reintente la operacion");
            }
            throw new GeneralException("TRF072");
        }
        ResultObtieneCtasCtes cuentas = 
            (ResultObtieneCtasCtes) cabecera.getCuentasCorrientesConvenio();
        
        if (getLogger().isEnabledFor(Level.INFO)) { 
            getLogger().info("[validarDisponibleDeCtaCargoNominaConMontoNomina][" + rutEmpresa + "] totalesPorCuenta [" + StringUtil.contenidoDe(totalesPorCuenta) + "]");
            getLogger().info("[validarDisponibleDeCtaCargoNominaConMontoNomina][" + rutEmpresa + "]: cuentas(" + cuentas + ")");
        }
        
        HashMap cuentaCorrienteConSaldo=null;
        try{
            cuentaCorrienteConSaldo = this.obtieneCuentasySaldo(cuentas);
        }
        catch (Exception e) {
            getLogger().error("[validarDisponibleDeCtaCargoNominaConMontoNomina][" + rutEmpresa + "][BCI_FINEX][Exception e][" + e + "]");
            throw new GeneralException("TRF071");   
        }

        if (getLogger().isEnabledFor(Level.INFO)) { 
            getLogger().info("[validarDisponibleDeCtaCargoNominaConMontoNomina][" + rutEmpresa + "] cuentaCorrienteConSaldo (" + StringUtil.contenidoDe(cuentaCorrienteConSaldo ) + ")");
        }

        Iterator it = totalesPorCuenta.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry e = (Map.Entry) it.next();
            double montoDisponibleCuenta = Double.parseDouble(String.valueOf(
                cuentaCorrienteConSaldo.get(e.getKey())));
            double montoTransferenciaCuenta = Double.parseDouble(String.valueOf(e.getValue()));
            if ( montoDisponibleCuenta <  montoTransferenciaCuenta ){
                if (getLogger().isEnabledFor(Level.ERROR)) { 
                    getLogger().error("[validarDisponibleDeCtaCargoNominaConMontoNomina][" + rutEmpresa + "] Saldo insuficiente en cuenta " + e.getKey());
                }
                throw new GeneralException("0020");
            }
        }
    }

    /**
     * M�todo que genera Filtro de la Consulta de Transferencias.
     * <br><br>
     *   M�todo migrado desde {@link M�todo migrado desde 
     *   {@link wcorp.aplicaciones.productos.servicios.transferencias.nominas.struts.helper.TransferenciasHelper}} 
     *
     * <p>
     * Registro de versiones:<ul>
     * <li> 1.0 10/02/2014, Jaime Suazo D. (Experimento Social. SPA.): versi�n inicial.</li>
     * <li> 1.1 29/12/2014 Rafael Pizarro (TINet): Se normaliza log de metodo
     * para ajustarse a Normativa Log4j de BCI.</li>
     * </ul>
     * <p>
     *
     * @param resultadoCuentas Cuentas de la Empresa
     * @return HashMap obtenci�n de los saldos por cuenta
     * @throws Exception Se lanza cuando hay un error de sistema.
     * @since 2.7
     */
    public HashMap obtieneCuentasySaldo(ResultObtieneCtasCtes resultadoCuentas)  throws Exception {

        try{
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[obtieneCuentasySaldo][BCI_INI]" );
                getLogger().info("[obtieneCuentasySaldo] resultadoCuentas ["+ resultadoCuentas +"]");
            }
            ObtenCtasCtes[] obtCtasCtes    = resultadoCuentas == null ? null : resultadoCuentas.getObtenCtasCtes();
            if(obtCtasCtes == null || obtCtasCtes.length == 0){
                return null;
            }
            HashMap ctaConSaldo = new HashMap();
            if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[obtieneCuentasySaldo] Cuentas obtenidas [" + obtCtasCtes.length + "]"); }
            NominaDeTransferenciasDelegate delegate = NominaDeTransferenciasDelegate.getInstance();

            for(int i = 0; i < obtCtasCtes.length; i++) {
                String cuenta = obtCtasCtes[i].getNumCuentaCorriente().trim();
                if (cuenta.length() == LARGO_CUENTA){
                    cuenta = cuenta.substring(POSICION_COMIENZO_CUENTA, cuenta.length());
                    }
                SaldoCtaCte saldoCtaCte = delegate.saldoCtaCte(cuenta);
                double saldoTotal = saldoCtaCte.saldoDisponible + saldoCtaCte.sobregiroDisp;
                ctaConSaldo.put(cuenta, new Double(saldoTotal));
            }
            if (getLogger().isEnabledFor(Level.INFO)){getLogger().info("[obtieneCuentasySaldo][BCI_FINOK] saldo total ["+ ctaConSaldo +"]");}

            return ctaConSaldo;
        } catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[obtieneCuentasySaldo][BCI_FINEX][Exception e][" + e.getMessage() + "]", e);}
            throw e;
        }
    }   

    /**
     * M�todo que obtiene el log.
     * Registro de Versiones:
     * <ul>
     * <li> 07/02/2014 Jaime Suazo. (Experimento Social SPA): Versi�n Inicial</li>
     * </ul>
     * @return log
     */
    public Logger getLogger(){
        if(log == null){
            log = Logger.getLogger(this.getClass());
        }
        return log;
    }

    /**
     * Metodo utilitario para identificar si el usuario es Apoderado 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 10/02/2014 Jaime Suazo D. (Experimento Social. SPA.): Versi�n inicial.</li>
     * </ul>
     * <p>
     * 
     * @param tipoUsuario del convenio empresa.
     * @return boolean de respuesta
     *  
     * @since 2.7
     */     
    private boolean esApoderado(String tipoUsuario) {
        tipoUsuario = StringUtil.eliminaCaracteres(tipoUsuario, " ");
        if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[esApoderado][BCI_FINOK]");}
        return ("APO".equalsIgnoreCase(tipoUsuario) || "Apoderado".equalsIgnoreCase(tipoUsuario));
    }

    /**
     * Metodo utilitario para identificar si el usuario es Apoderado Autorizado
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 10/02/2014 Jaime Suazo D. (Experimento Social. SPA.): Versi�n inicial.</li>
     * </ul>
     * </p>
     * 
     * @param tipoUsuario del convenio empresa.
     * @param perfil del usuario del convenio empresa.
     * @return boolean de respuesta
     *  
     * @since 2.7
     */    
    private boolean esApoderadoAutorizado(String tipoUsuario, String perfil) {
        String perfilLimpio = StringUtil.eliminaCaracteres(perfil, " ");
        if (esApoderado(tipoUsuario) && perfilLimpio.equalsIgnoreCase("Autorizado")) {
            if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[esApoderadoAutorizado][BCI_FINOK]");}
            return true;
        }
        if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[esApoderadoAutorizado][BCI_FINEX]");}
        return false;
    }

    /**
     *  M�todo que permite journalizar las firmas de las n�minas PNOL. 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 10/02/2014 Jaime Suazo D. (Experimento Social. SPA.): Versi�n inicial.</li>
     * </ul>
     * </p>
     * 
     * @param rutEmpresa Rut de empresa.
     * @param digitoVerifEmp D�giyo verificador de empresa.
     * @param convenioEmpresa n�mero de convenio.
     * @param rutUsu rut de usuario.
     * @param digitoVerifUsu D�gito verificador de usuario.
     * @param folioNomina folio de la n�mina.
     * @param cabecera cabecera con datos relevantes para la firma.
     * @return String del c�digo jen de la journalizaci�n.
     * @since 2.7
     */
    private String journalizacionFirmaPnol(String rutEmpresa, String digitoVerifEmp, String convenioEmpresa, 
        String rutUsu, String digitoVerifUsu, String folioNomina, CabeceraFirmaNominaPnolTO cabecera) {

        try {
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[journalizacionFirmaPnol][" + rutEmpresa + "]["+ convenioEmpresa +"][BCI_INI]" );
                getLogger().info("[journalizacionFirmaPnol][" + rutEmpresa + "]["+ convenioEmpresa +"] rutUsu ["+ rutUsu +"] - ["+ digitoVerifUsu +"], folioNomina ["+ folioNomina +"], cabecera ["+ cabecera +"]");
            }

            String codigoEvento = TablaValores.getValor(ARCHIVO_ADMIN, "codigosJournal",
                "codEventoNegocio");
            String codigoSubEvento = TablaValores.getValor(ARCHIVO_ADMIN, "codigosJournal",
                "subCodEventoNegocio");
            String productoJen = TablaValores.getValor(ARCHIVO_ADMIN, "codigosJournal", "idProducto");

            String glosaLog = TablaValores.getValor(ARCHIVO_ADMIN, "codigosJournal", "glosaLog");
            glosaLog = glosaLog + folioNomina + " canal : " + cabecera.getCanal();

            Journal journalEmpresa = new Journal();
            Eventos evento = new Eventos();
            evento.setRutCliente(rutEmpresa);
            evento.setDvCliente(digitoVerifEmp);
            evento.setRutOperadorCliente(rutUsu);
            evento.setDvOperadorCliente(digitoVerifUsu);
            evento.setCodEventoNegocio(codigoEvento);
            evento.setEstadoEventoNegocio("P");
            evento.setSubCodEventoNegocio(codigoSubEvento);
            evento.setCodsubtipoEventoNegocio(codigoEvento + " " + codigoSubEvento);
            evento.setFueraLinea("N");
            evento.setModo("N");       
            evento.setIdProducto(productoJen);
            evento.setAtributo("convenio", convenioEmpresa);
            evento.setAtributo("codProducto", cabecera.getProductoBel());
            evento.setAtributo("codServicio", " ");
            evento.setAtributo("tipoLog", "C");
            evento.setAtributo("glosaLog", glosaLog);
            Canal canal = new Canal(cabecera.getCanal());

            evento.setIdCanal(canal.getNombre());
            evento.setClavePrincipal(convenioEmpresa);
            evento.setIdMedio(cabecera.getIdMedio());

            journalEmpresa.journalizar(evento);
            String numeroEventoJournal = evento.getNroOperacionEvento();
            if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[journalizacionFirmaPnol][" + rutEmpresa + "]["+ convenioEmpresa +"][BCI_FINOK]");}
            return numeroEventoJournal;
        } 
        catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[journalizacionFirmaPnol][" + rutEmpresa + "]["+ convenioEmpresa +"][BCI_FINEX][Exception e][" + e.getMessage() + "] ", e);
            }
            return "";
        }
    }   

    /**
     * 
     * M�todo que permite obtener encabezado de nomina.
     * <p>
     * <b>Registro de versiones:</b>
     * <ul>
     * <li> 1.0 24-10-2014, H�ctor Beas D. (Imagemaker IT): versi�n inicial</li>
     * </ul>
     * </p> 
     * 
     * @param info Entidad de negocio.
     * @return InformacionCargaMasiva
     * @throws Exception Exception.
     * @since 2.8
     */
    public InformacionCargaMasiva obtenerEncabezadoDeNomina(InformacionCargaMasiva info) throws Exception{
        
        String numTransferencia = info.getNumTransferencia();
        try{
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[obtenerEncabezadoDeNomina][" + numTransferencia + "][BCI_INI]" );
                getLogger().info("[obtenerEncabezadoDeNomina][" + numTransferencia + "] informaci�n ["+ info.toString() +"]");
            }
            NominaDeTransferenciasDAO dao = new NominaDeTransferenciasDAO();
            if (getLogger().isEnabledFor(Level.INFO)){getLogger().info("[obtenerEncabezadoDeNomina][" + numTransferencia + "][BCI_FINOK]");}
            return dao.obtenerEncabezadoDeNomina(info);
        }
        catch(Exception e){
            if(getLogger().isEnabledFor(Level.ERROR)){
                getLogger().error("[obtenerEncabezadoDeNomina][" + numTransferencia + "][BCI_FINEX][Exception e]["+ ErroresUtil.extraeStackTrace(e) + "]");
            }
            throw new Exception(ErroresUtil.extraeStackTrace(e));
        }
    }
    /**
     * 
     * M�todo que permite actualizar el estado de una n�mina en proceso.
     * <p>
     * <b>Registro de versiones:</b>
     * <ul>
     * <li> 1.0 24-10-2014, H�ctor Beas D. (Imagemaker IT): versi�n inicial</li>
     * </ul>
     * </p> 
     * 
     * @param info informacion de la n�mina.
     * @return boolean.
     * @throws Exception en caso de error.
     * @since 1.0
     */
    public boolean actualizarEstadoNominaEnProceso(InformacionCargaMasiva info) throws Exception{
        boolean respuesta;

        String numTransferencia = info.getNumTransferencia();
        try{
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[actualizarEstadoNominaEnProceso][" + numTransferencia + "][BCI_INI]" );
                getLogger().info("[actualizarEstadoNominaEnProceso][" + numTransferencia + "] informaci�n ["+ info.toString() +"]");
            }
            NominaDeTransferenciasDAO dao = new NominaDeTransferenciasDAO();
            respuesta = dao.actualizarEstadoNominaEnProceso(info);
            if (getLogger().isEnabledFor(Level.INFO)){getLogger().info("[actualizarEstadoNominaEnProceso][" + numTransferencia + "][BCI_FINOK] respuesta ["+ respuesta +"]");}
            return respuesta;
        }
        catch(Exception e){
            if (getLogger().isEnabledFor(Level.ERROR)) {getLogger().error("[actualizarEstadoNominaEnProceso][" + numTransferencia + "][BCI_FINEX][Exception e]["+ ErroresUtil.extraeStackTrace(e) + "]");}
            throw new Exception(ErroresUtil.extraeStackTrace(e));
        }
    }

    /**
     * M�todo utilizado para obtener los pagos de una n�mina.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 28/10/2014 Pablo Romero C�ceres (SEnTRA) : versi�n incial.
     * </ul>
     * @param nomina N�mina de la cual se consultar�n los pagos.
     * @return Pagos obtenidos.
     * @throws GeneralException en caso de error.
     * @since 2.8
     */
    public InfoDetalleNomina[] obtenerPagosNomina(InformacionCargaMasiva nomina) 
        throws GeneralException{
        String numTransferencia = nomina.getNumTransferencia();
        try{
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[obtenerPagosNomina][" + numTransferencia + "][BCI_INI]" );
                getLogger().info("[obtenerPagosNomina][" + numTransferencia + "] nomina ["+ nomina.toString() +"]");
            }
            InfoDetalleNomina[] pagos = (new NominaDeTransferenciasDAO()).obtenerPagosNomina(nomina);
            if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[obtenerPagosNomina][" + numTransferencia + "][BCI_FINOK] retornando pagos : ["+ pagos +"]");}
            return pagos;
        }
        catch(GeneralException e){
            if (getLogger().isEnabledFor(Level.ERROR)) {getLogger().error("[obtenerPagosNomina][" + numTransferencia + "][BCI_FINEX][GeneralException e][" + e.getMessage(),e);}
            throw e;
        }
    }

    /**
     * M�todo utilizado para obtener los totales de una n�mina en proceso.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 28/10/2014 Pablo Romero C�ceres (SEnTRA) : versi�n incial.
     * </ul>
     * @param nomina Nomina que se consultar�.
     * @return Totales de la n�mina
     * @throws GeneralException en caso de error.
     * @since 2.8
     */
    public TotalTO[] obtenerTotalesNomina(InformacionCargaMasiva nomina) 
        throws GeneralException{
        String numTransferencia = nomina.getNumTransferencia();
        try{
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[obtenerTotalesNomina][" + numTransferencia + "][BCI_INI]" );
                getLogger().info("[obtenerTotalesNomina][" + numTransferencia + "] nomina ["+ nomina.toString() +"]");
            }
            TotalTO[] totales = (new NominaDeTransferenciasDAO()).obtenerTotalesNomina(nomina);
            if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[obtenerPagosNomina][" + numTransferencia + "][BCI_FINOK] retornando totales : " + totales);}
            return totales;
        }
        catch(GeneralException e){
            if (getLogger().isEnabledFor(Level.ERROR)) { getLogger().error("[obtenerPagosNomina][" + numTransferencia + "][BCI_FINEX][GeneralException e]["+ e.getMessage(), e);}
            throw e;
        }
    }

    /**
     * Obtiene nominas pendinetes de firma.
     * <p>
     * <b>Registro de versiones:</b>
     * <ul>
     * <li> 1.0 04-11-2014, H�ctor Hern�ndez Orrego. (Sentra): versi�n inicial</li>
     * <li> 1.1 26/01/2015 Sergio Cuevas D�az (Sentra): Se encapsulan parametros de la firma del m�todo.</li>
     * </ul>
     * </p> 
     * @param FiltroConsultaTO contenedor de datos para consulta.
     * @return InformacionCargaMasiva[]
     * @throws Exception Exception.
     * @since 2.8
     */
    public InformacionCargaMasiva[] obtieneNominasEnLineaPendientesDeFirma(
            FiltroConsultaTO filtroConsulta) throws Exception {
        
    	String rutEmpresa = filtroConsulta.getCuentaOrigen();
        String convenioEmp = filtroConsulta.getConvenio();
        
        try {
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[obtieneNominasEnLineaPendientesDeFirma][" + rutEmpresa + "][" + convenioEmp + "][BCI_INI]" );
                getLogger().info("[obtieneNominasEnLineaPendientesDeFirma][" + rutEmpresa + "][" + convenioEmp + "] filtroConsulta ["+ filtroConsulta.toString() +"]");
            }
            NominaDeTransferenciasDAO dao = new NominaDeTransferenciasDAO();
            if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[obtieneNominasEnLineaPendientesDeFirma][" + rutEmpresa + "][" + convenioEmp + "][BCI_FINOK]");}
            return dao.obtieneNominasEnLineaPendientesDeFirma(filtroConsulta);
        }
        catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) { getLogger().error("[obtieneNominasEnLineaPendientesDeFirma][" + rutEmpresa + "][" + convenioEmp + "][BCI_FINEX][Exception e]["+ e.getMessage()+"]");}
            throw e;
        }
    }

    
    /**
     * M�todo utilizado para obtener el encabezado de una n�mina en proceso.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 28/10/2014 Pablo Romero C�ceres (SEnTRA) : versi�n incial.
     * </ul>
     * @param nomina Nomina que se consultar�.
     * @return encabezado de la n�mina.
     * @throws GeneralException en caso de error.
     * @since 2.8
     */
    public InformacionCargaMasiva obtenerEncabezadoDeNominaEnProceso(InformacionCargaMasiva nomina) 
        throws GeneralException{
        
        String numTransferencia = nomina.getNumTransferencia();
        try{
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[obtenerEncabezadoDeNominaEnProceso][" + numTransferencia + "][BCI_INI]" );
                getLogger().info("[obtenerEncabezadoDeNominaEnProceso][" + numTransferencia + "] nomina ["+ nomina.toString() +"]");
            }
            InformacionCargaMasiva encabezadoNomina = (new 
                NominaDeTransferenciasDAO()).obtenerEncabezadoDeNominaEnProceso(nomina);
            if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[obtenerEncabezadoDeNominaEnProceso][" + numTransferencia + "][BCI_FINOK] retornando encabezado : " + encabezadoNomina);}
            return encabezadoNomina;
        }
        catch(GeneralException e){
            if (getLogger().isEnabledFor(Level.ERROR)) { getLogger().error("[obtenerEncabezadoDeNominaEnProceso][" + numTransferencia + "][BCI_FINEX][GeneralException e][" + e.getMessage(), e);
            }
            throw e;
        }
    }

    /**
     * M�todo consulta el monto transferido a un benefeciario (rut-cuenta) desde una cuenta origen en el d�a
     * consultado.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 27/10/2014 H�ctor Hern�ndez Orrego (Sentra): versi�n inicial.</li>
     * </ul>
     * <p>
     * 
     * @since 2.8
     */
    public double consultaMontoPagadoBeneficiario(long rutBeneficiario, String cuentaDestino, String cuentaOrigen,
        Date fechaPago) throws GeneralException {
        try {
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[consultaMontoPagadoBeneficiario][" + rutBeneficiario + "][BCI_INI]" );
                getLogger().info("[consultaMontoPagadoBeneficiario][" + rutBeneficiario + "] cuentaDestino ["+ cuentaDestino +"], cuentaOrigen ["+ cuentaOrigen +"], fechaPago ["+ fechaPago +"]");
            }

            NominaDeTransferenciasDAO dao = new NominaDeTransferenciasDAO();
            if (getLogger().isEnabledFor(Level.INFO)){getLogger().info("[consultaMontoPagadoBeneficiario][" + rutBeneficiario + "][BCI_FINOK]");}
            return dao.consultaMontoPagadoBeneficiario(rutBeneficiario, cuentaDestino, cuentaOrigen, fechaPago);
        }
        catch (GeneralException e) {
            if (getLogger().isEnabledFor(Level.ERROR)) { getLogger().error("[consultaMontoPagadoBeneficiario][" + rutBeneficiario + "][BCI_FINEX][GeneralException e]" + e.getMessage(), e);}
            throw e;
        }
    }

    /**
     * M�todo que actualiza el estado de una pago
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 06/11/2014 Pablo Romero C�ceres (SEnTRA) : versi�n incial.
     * </ul>
     * @param pago Pago con estado a actualizar.
     * @return <b>true</b> si se pudo actualizar.
     * @throws GeneralException en caso de error.
     * @since 2.8
     */
    public boolean actualizarEstadoPago(InfoDetalleNomina pago) throws GeneralException{
        
        String numTransferencia = pago.getNumTransferencia();
        try{
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[actualizarEstadoPago][" + numTransferencia + "][BCI_INI]" );
                getLogger().info("[actualizarEstadoPago][" + numTransferencia + "] pago ["+ pago.toString() +"]");
            }
            boolean respuesta = (new NominaDeTransferenciasDAO()).actualizarEstadoPago(pago);
            if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[actualizarEstadoPago][" + numTransferencia + "][BCI_FINOK] retornando estado : " + respuesta);}
            return respuesta;
        }
        catch(GeneralException e){
            if (getLogger().isEnabledFor(Level.ERROR)) { getLogger().error("[actualizarEstadoPago][" + numTransferencia + "][BCI_FINEX][GeneralException e]" + e.getMessage(), e); }
            throw e;
        }
    }

    /**
     * M�todo que actualiza los totales de una n�mina.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 06-11-2014 Pablo Romero C�ceres (SEnTRA) : versi�n incial.
     * </ul>
     * @param numeroTransferencia N�mero de la Transferencia.
     * @return <b>true</b> si se pudo actualizar.
     * @throws GeneralException en caso de error.
     * @since 2.8
     */
    public boolean actualizarTotalesNomina(String numeroTransferencia) throws GeneralException{
        try{
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[actualizarTotalesNomina][" + numeroTransferencia + "][BCI_INI]" );
            }
            boolean respuesta = (new NominaDeTransferenciasDAO()).actualizarTotalesNomina(numeroTransferencia);
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[actualizarTotalesNomina][" + numeroTransferencia + "] retornando totales : " + respuesta);
            }
            return respuesta;
        }
        catch(GeneralException e){
            if (getLogger().isEnabledFor(Level.ERROR)) { getLogger().error("[actualizarTotalesNomina][" + numeroTransferencia + "][BCI_FINEX][GeneralException e]" + e.getMessage(), e);}
            throw e;
        }
    }

    /**
     * Metodo para obtener el resumen de nominas.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li> 1.0 30/10/2014 Sergio Cuevas Diaz (SEnTRA) : versi�n inicial.
     * </ul>
     * </p>
     * 
     * @param fecha dia actual para realizar la consulta del resumen.
     * @return boolean respuesta de la actualizacion de totales.
     * @throws Exception en caso de error.
     * @since 3.0
     */
    public TotalTO[] obtenerResumenNominas(FiltroConsultaTO filtro) throws Exception{
        TotalTO[] respuesta;
        
        long rutEmpresa = filtro.getRutEmpresa();
        String convenio = filtro.getConvenio();
        try{
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[obtenerResumenNominas][" + rutEmpresa + "][" + convenio + "][BCI_INI]" );
                getLogger().info("[obtenerResumenNominas][" + rutEmpresa + "][" + convenio + "] filtro ["+ filtro.toString() +"]");
            }
            NominaDeTransferenciasDAO dao = new NominaDeTransferenciasDAO();
            respuesta = dao.obtenerResumenNominas(filtro);
            if (getLogger().isEnabledFor(Level.INFO)){getLogger().info("[obtenerResumenNominas][" + rutEmpresa + "][" + convenio + "][BCI_FINOK] retornando resumen ["+ respuesta +"]");}
            return respuesta;
        }
        catch(Exception e){
            if (getLogger().isEnabledFor(Level.ERROR)) { getLogger().error("[obtenerResumenNominas][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][Exception e][" + ErroresUtil.extraeStackTrace(e) + "]");}
            throw new Exception(ErroresUtil.extraeStackTrace(e));
        }
    }

    /**
     * M�todo utilizado para consultar nominas por numero de transferencia o fecha.
     *
     * <p>
     * Registro de versiones:<ul>
     * <li> 1.0 30/10/2014 Sergio Cuevas Diaz (SEnTRA) : versi�n inicial.
     * </ul>
     * </p>
     * 
     * @param filtro objeto con los datos a consultar.
     * @return arreglo con las n�minas obtenidas.
     * @throws Exception en caso de error.
     * @since 3.0
     */
    public InformacionCargaMasiva[] consultarNominasConProblemasPorFiltro(FiltroConsultaTO filtro)throws Exception{
        InformacionCargaMasiva[] respuesta;
        long rutEmpresa = filtro.getRutEmpresa();
        String convenio = filtro.getConvenio();
        try{
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[consultarNominasConProblemasPorFiltro][" + rutEmpresa + "][" + convenio + "][BCI_INI]" );
                getLogger().info("[consultarNominasConProblemasPorFiltro][" + rutEmpresa + "][" + convenio + "] filtro ["+ filtro.toString() +"]");
            }
            NominaDeTransferenciasDAO dao = new NominaDeTransferenciasDAO();
            respuesta = dao.consultarNominasConProblemasPorFiltro(filtro);
            if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[consultarNominasConProblemasPorFiltro][" + rutEmpresa + "][" + convenio + "][BCI_FINOK] retornando nominas ["+ respuesta +"]");}
            return respuesta;
        }
        catch(Exception e){
            if (getLogger().isEnabledFor(Level.ERROR)) { getLogger().error("[consultarNominasConProblemasPorFiltro][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][Exception e]["+ ErroresUtil.extraeStackTrace(e) + "]");}
            throw new Exception(ErroresUtil.extraeStackTrace(e));
        }
    }

    /**
     * M�todo utilizado para consultar nominas por numero de transferencia o fecha.
     *
     * <p>
     * Registro de versiones:<ul>
     * <li> 1.0 30/10/2014 Sergio Cuevas Diaz (SEnTRA) : versi�n inicial.
     * </ul>
     * </p>
     * 
     * @param filtro objeto con los datos a consultar.
     * @return arreglo con las n�minas obtenidas.
     * @throws Exception en caso de error.
     * @since 3.0
     */
    public InformacionCargaMasiva[] consultarNominasEnProcesoPorFiltro(FiltroConsultaTO filtro)throws Exception{
        InformacionCargaMasiva[] respuesta;
        long rutEmpresa = filtro.getRutEmpresa();
        String convenio = filtro.getConvenio();
        try{
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[consultarNominasEnProcesoPorFiltro][" + rutEmpresa + "][" + convenio + "][BCI_INI]" );
                getLogger().info("[consultarNominasEnProcesoPorFiltro][" + rutEmpresa + "][" + convenio + "] filtro ["+ filtro.toString() +"]");
            }
            NominaDeTransferenciasDAO dao = new NominaDeTransferenciasDAO();
            respuesta = dao.consultarNominasEnProcesoPorFiltro(filtro);
            if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[consultarNominasEnProcesoPorFiltro][" + rutEmpresa + "][" + convenio + "][BCI_FINOK] retornando nominas ["+ respuesta +"]");}
            return respuesta;
        }
        catch(Exception e){
            if(getLogger().isEnabledFor(Level.ERROR)){
                getLogger().error("[consultarNominasEnProcesoPorFiltro][Exception e]["+ ErroresUtil.extraeStackTrace(e) + "]");
            }
            throw new Exception(ErroresUtil.extraeStackTrace(e));
        }
    }


    /**
     * M�todo que obtiene un listado de los pagos realizados a trav�s de carga masiva y consultando seg�n ciertos
     * fitros.
     * <p>
     * <b>Registro de versiones:</b>
     * <ul>
     * <li> 1.0 13-11-2014, Angel Cris�stomo (Imagemaker IT): versi�n inicial</li>
     * </ul>
     * </p> 
     * 
     * @param filtro los filtros a aplicar a la consulta.
     * @param tipoTransferencia tipo transferencia.
     * @param canal canal.
     * @throws Exception Exception.
     * @return Listado con los pagos realizados a trav�s de carga masiva.
     * @since 2.9
     */
    public EncabezadoTransferenciaVO[] obtenerEncabezadoPagosRealizados(FiltroConsultarTransferenciasVO filtro,
        String tipoTransferencia, String canal) throws Exception {
    	String rutEmpresa = filtro.getRutOrigen();
        String convenio = filtro.getConvenio();
        try {
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[obtenerEncabezadoPagosRealizados][" + rutEmpresa + "][" + convenio + "][BCI_INI]" );
                getLogger().info("[obtenerEncabezadoPagosRealizados][" + rutEmpresa + "][" + convenio + "] canal ["+ canal +"], tipoTransferencia ["+ tipoTransferencia +"], filtro ["+ filtro.toString() +"]");
            }
            NominaDeTransferenciasDAO dao = new NominaDeTransferenciasDAO();
            if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[obtenerEncabezadoPagosRealizados][" + rutEmpresa + "][" + convenio + "][BCI_FINOK]");}
            return dao.obtenerEncabezadoPagosRealizados(filtro, tipoTransferencia, canal);
        }
        catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) {
            	getLogger().error("[obtenerEncabezadoPagosRealizados][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][Exception e]" + e.getMessage());
            }
            throw e;
        }
    }

    /**
     *  M�todo que obtiene un listado de transferencias realizadas, consultando seg�n ciertos criterios.
     * <p>
     * <b>Registro de versiones:</b>
     * <ul>
     * <li> 1.0 13-11-2014, Angel Cris�stomo (Imagemaker IT): versi�n inicial</li>
     * </ul>
     * </p> 
     * 
     * @param filtro los filtros a aplicar a la consulta.
     * @param tipoTransferencia tipo transferencia.
     * @param canal canal.
     * @param origen origen.
     * @throws Exception exception.
     * @return Listado con las transferencias realizadas.
     * @since 2.9
     */
    public DetalleTransferenciaVO[] obtenerDetallePagosRealizados(FiltroConsultarTransferenciasVO filtro,
        String tipoTransferencia, String canal, String origen) throws Exception {
    	String rutEmpresa = filtro.getRutOrigen();
        String convenio = filtro.getConvenio();
        try {
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[obtenerDetallePagosRealizados][" + rutEmpresa + "][" + convenio + "][BCI_INI]" );
                getLogger().info("[obtenerDetallePagosRealizados][" + rutEmpresa + "][" + convenio + "] canal ["+ canal +"], tipoTransferencia ["+ tipoTransferencia +"], origen ["+ origen +"], filtro ["+ filtro.toString() +"]");
            }
            NominaDeTransferenciasDAO dao = new NominaDeTransferenciasDAO();
            if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[obtenerDetallePagosRealizados][" + rutEmpresa + "][" + convenio + "][BCI_FINOK]");}
            return dao.obtenerDetallePagosRealizados(filtro, tipoTransferencia, canal, origen);
        }
        catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) { getLogger().error("[obtenerDetallePagosRealizados][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][Exception e]" + e.getMessage());}
            throw e;
        }
    }

    /**
     *  M�todo que obtiene un listado de transferencias realizadas, consultando seg�n ciertos criterios.
     * <p>
     * <b>Registro de versiones:</b>
     * <ul>
     * <li>1.0 27/10/2014, Darlyn Delgado Perez (SEnTRA): versi�n inicial</li>
     * </ul>
     * </p> 
     * 
     * @param transferenciaIngreso con datos a ingresar el detalle.
     * @throws Exception exception.
     * @return valor del ingreso del detalle.
     * @since 2.9
     */
    public boolean insertarDetalleTransferencia(TransferenciaATercerosTO transferenciaIngreso) 
        throws Exception {
        
        String numTransferencia = transferenciaIngreso.getIdentificador();
        try {

            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[insertarDetalleTransferencia][" + numTransferencia + "][BCI_INI]" );
                getLogger().info("[insertarDetalleTransferencia][" + numTransferencia + "] transferenciaIngreso ["+ transferenciaIngreso.toString() +"]");
            }
            NominaDeTransferenciasDAO dao = new NominaDeTransferenciasDAO();
            if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[insertarDetalleTransferencia][" + numTransferencia + "][BCI_FINOK]");}
            return dao.insertarDetalleTransferencia(transferenciaIngreso);
        }
        catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) { getLogger().error("[insertarDetalleTransferencia][" + numTransferencia + "][BCI_FINEX][Exception e]" + e.getMessage(), e);
            }
            throw e;
        }
    }

    /**
     * Inserta encabezado de la transferencia.
     * <p>
     * <b>Registro de versiones:</b>
     * <ul>
     * <li> 1.0 27-11-2014, Angel Cris�stomo (Imagemaker IT): versi�n inicial</li>
     * </ul>
     * </p> 
     *  
     * @param transferencia Entidad de negocio.
     * @return String
     * @throws GeneralException Exception.
     * @since 3.1
     */
    public String insertarEncabezadoTransferencia(TransferenciaATercerosTO transferencia) throws GeneralException {
        String numeroTransferencia=null;
        String numTransferencia = transferencia.getIdentificador();
        try{
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[insertarEncabezadoTransferencia][" + numTransferencia + "][BCI_INI]" );
                getLogger().info("[insertarEncabezadoTransferencia][" + numTransferencia + "] transferencia ["+ transferencia.toString() +"]");
            }
            NominaDeTransferenciasDAO dao = new NominaDeTransferenciasDAO();
            numeroTransferencia = dao.insertarEncabezadoTransferencia(transferencia);
            if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[insertarEncabezadoTransferencia][" + numTransferencia + "][BCI_FINOK]");}
            return numeroTransferencia;
        }
        catch (GeneralException e) {
            if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[insertarEncabezadoTransferencia][" + numTransferencia + "][BCI_FINEX][GeneralException e]" + e.getMessage()); 
            }
            throw e;
        }


    }

    /**
     * Elimina operacion realizada sobre transferencia.
     * <p>
     * <b>Registro de versiones:</b>
     * <ul>
     * <li> 1.0 28-11-2014, Angel Cris�stomo (Imagemaker IT): versi�n inicial</li>
     * </ul>
     * </p> 
     * 
     * @param numeroTransferencia Numero de transferencia.
     * @return boolean
     * @throws GeneralException Exception de negocio.
     * @since 1.0
     */
    public boolean eliminarOperacionTransferencia(String numeroTransferencia) throws GeneralException {
        boolean respuesta =false;
        try{
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[eliminarOperacionTransferencia][" + numeroTransferencia + "][BCI_INI]" );
                getLogger().info("[eliminarOperacionTransferencia][" + numeroTransferencia + "] numeroTransferencia ["+ numeroTransferencia +"]");
            }
            NominaDeTransferenciasDAO dao = new NominaDeTransferenciasDAO();
            respuesta = dao.eliminarOperacionTransferencia(numeroTransferencia);
            if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[eliminarOperacionTransferencia][" + numeroTransferencia + "][BCI_FINOK]");}
            return respuesta;
        }
        catch (GeneralException e) {
            if (getLogger().isEnabledFor(Level.ERROR)) { getLogger().error("[eliminarOperacionTransferencia][" + numeroTransferencia + "][BCI_FINEX][GeneralException e]" + e.getMessage());}
            throw e;
        }
    }

    /**
     * Inserta totales de una transferencia.
     * <p>
     * <b>Registro de versiones:</b>
     * <ul>
     * <li> 1.0 28-11-2014, Angel Cris�stomo (Imagemaker IT): versi�n inicial</li>
     * </ul>
     * </p> 
     * 
     * @param numeroTransferencia Numero de transferencia.
     * @return boolean
     * @throws GeneralException Exception de negocio.
     * @since 1.0
     */
    public boolean insertarTotalesTransferencia(String numeroTransferencia) throws GeneralException {
        boolean respuesta =false;
        try{
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[insertarTotalesTransferencia][" + numeroTransferencia + "][BCI_INI]" );
                getLogger().info("[insertarTotalesTransferencia][" + numeroTransferencia + "] numeroTransferencia ["+ numeroTransferencia +"]");
            }
            NominaDeTransferenciasDAO dao = new NominaDeTransferenciasDAO();
            respuesta = dao.insertarTotalesTransferencia(numeroTransferencia);
            if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[insertarTotalesTransferencia][" + numeroTransferencia + "][BCI_FINOK] respuesta ["+ respuesta +"]");}
            return respuesta;
        }
        catch (GeneralException e) {
            if (getLogger().isEnabledFor(Level.ERROR)) { getLogger().error("[insertarTotalesTransferencia][" + numeroTransferencia + "][BCI_FINEX][GeneralException e]" + e.getMessage()); }
            throw e;
        }
    }

    /**
     * M�todo que consulta las transferencias de acuerdo al filtro ingresado.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 28/11/2014 Pablo Romero C�ceres (SEnTRA) : versi�n incial.
     * </ul>
     * @param transferenciasAConsultar Filtro por el cual consultar� las transferencias.
     * @return Transferencias encontradas.
     * @throws GeneralException en caso de error.
     * @since 3.1
     */
    public TransferenciaATercerosTO[] obtieneTransferenciasDePago(FiltroTO transferenciasAConsultar) 
        throws GeneralException{
    	long rutEmp = transferenciasAConsultar.getRutEmpresa();
        String conv = transferenciasAConsultar.getNumeroConvenio();
        try{
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[obtieneTransferenciasDePago][" + rutEmp + "][" + conv + "][BCI_INI]" );
                getLogger().info("[obtieneTransferenciasDePago][" + rutEmp + "][" + conv + "] transferenciasAConsultar ["+ transferenciasAConsultar.toString() +"]");
            }
            NominaDeTransferenciasDAO nominaDeTransferencias = new NominaDeTransferenciasDAO();
            TransferenciaATercerosTO[] transferencias = nominaDeTransferencias.obtieneTransferenciasDePago(
                transferenciasAConsultar);
            if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[obtieneTransferenciasDePago][" + rutEmp + "][" + conv + "][BCI_FINOK] retornando transferencias [" + transferencias +"]");}
            return transferencias;
        }
        catch(GeneralException e){
            if (getLogger().isEnabledFor(Level.ERROR)) { getLogger().error("[obtieneTransferenciasDePago][" + rutEmp + "][" + conv + "][BCI_FINEX][GeneralException e]" + e.getMessage(), e); } 
            throw e;
        }
    }

    /**
     * M�todo que consulta las transferencias de acuerdo al filtro ingresado.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 09/12/2014 Sergio Cuevas Diaz (SEnTRA) : versi�n incial.
     * </ul>
     * @param transferenciasAConsultar Filtro por el cual consultar� las transferencias.
     * @return Transferencias encontradas.
     * @throws GeneralException en caso de error.
     * @since 3.2
     */
    public TransferenciaATercerosTO[] obtieneTransferenciasDePagoRecibidas(FiltroTO transferenciasAConsultar) 
        throws GeneralException{
    	long rutEmp = transferenciasAConsultar.getRutEmpresa();
        String conv = transferenciasAConsultar.getNumeroConvenio();
        try{
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[obtieneTransferenciasDePagoRecibidas][" + rutEmp + "][" + conv + "][BCI_INI]" );
                getLogger().info("[obtieneTransferenciasDePagoRecibidas][" + rutEmp + "][" + conv + "] transferenciasAConsultar ["+ transferenciasAConsultar.toString() +"]");
            }
            NominaDeTransferenciasDAO nominaDeTransferencias = new NominaDeTransferenciasDAO();
            TransferenciaATercerosTO[] transferencias = nominaDeTransferencias.
                    obtieneTransferenciasDePagoRecibidas(transferenciasAConsultar);
            if (transferencias != null){
                if (getLogger().isEnabledFor(Level.INFO)) { getLogger().info("[obtieneTransferenciasDePagoRecibidas][" + rutEmp + "][" + conv + "][BCI_FINOK] retornando transferencias ["+ transferencias.toString() +"]");}
            }
            return transferencias;
        }
        catch(GeneralException e){
            if (getLogger().isEnabledFor(Level.ERROR)) { getLogger().error("[obtieneTransferenciasDePagoRecibidas][" + rutEmp + "][" + conv + "][BCI_FINEX][GeneralException e]" + e.getMessage(), e); } 
            throw e;
        }
    }

    /**
     * M�todo que realiza una transferencia de fondos a cuentas propias y a terceros inscritos.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 18/12/2014 Pablo Romero C�ceres (SEnTRA) : versi�n incial.
     * <li>1.1 09/02/2015 Jorge G�mez O (SEnTRA) : Se eliminan los cambios de estado en el caso de transferencias a otros bancos.
     * <li>1.2 02/12/2015 Gonzalo Cofre Guzman (Sentra) - Daniel Araya (Ing de Soft BCI): Se agrega envio de correo a otros bancos.
     * <li>1.3 11/02/2016 Pablo Romero C�ceres (Sentra) - Jorge Lara D�az (Ing. Soft. BCI): Se modifica la llamada al m�todo
     * {@link #enviarCorreoTransferencia(TransferenciaATercerosTO)}
     * <li>1.3 26/02/2016 Gonzalo Cofre Guzman (Sentra) - Jorge Lara (Ing de Soft BCI): Se modifica c�digo de error de la excepcion.
     * <li>1.4 08/03/2016 Pablo Romero C�ceres (SEnTRA) - Iv�n S�nchez (Ing. Soft. BCI): Se modifica el canal usado para el comprobante. 
     * <li>1.5 23/06/2016 Sergio Bustos B. (IMIT) - Marcelo Avenda�o (BCI): Se agrega control de exception cuando 
     *                    CCA pierde conexion por timeout . 
     * </ul>
     * @param transferencia Objeto con los datos de la transferencia.
     * @param cliente Objeto con los datos del cliente.
     * @return boolean con la respuesta de la transferencia de fondos.
     * @throws GeneralException en caso de error.
     * @since 3.3
     */
    public boolean realizaTransferenciaDeFondos(TransferenciaATercerosTO transferencia, Cliente cliente)
        throws GeneralException {
        boolean resultadoTransf = false;
        String numTransferencia = transferencia.getIdentificador();
        String estadoTransaccion =
            TablaValores.getValor(ARCH_PARAM_EMP, "codigosEstadosTransferencia", "noRealizada");
        String codigoEstado = TablaValores.getValor(ARCH_PARAM_TRF_TER, "codigoError", "errorComunicacion");
        try {
            String ctaOrigen = StringUtil.sacaCeros(transferencia.getCtaOrigen());
            ctaOrigen = StringUtil.rellenaConCeros(ctaOrigen, LARGO_CUENTA_OCHO);

            String cuentaAbono = StringUtil.sacaCeros(transferencia.getCtaDestino());
            cuentaAbono = StringUtil.rellenaConCeros(cuentaAbono, LARGO_CUENTA);
            String tipoTransferencia = transferencia.getTipoTrf();

            if (getLogger().isEnabledFor(Level.INFO)){
                getLogger().info("[realizaTransferenciaDeFondos][" + numTransferencia + "][BCI_INI]");
                getLogger().info("[realizaTransferenciaDeFondos][" + numTransferencia + "] cliente: [" + cliente.toString()+"], cuentaAbono [" + cuentaAbono +"], tipoTransferencia [" + tipoTransferencia +"]");
                getLogger().info("[realizaTransferenciaDeFondos][" + numTransferencia + "] Monto [" + transferencia.getMontoTransfer()+"], BancoDestino [" + transferencia.getCodBanco1()+"], transferencia: [" + transferencia.toString()+"]");
            }

            if (tipoTransferencia.equals(TablaValores.getValor(ARCH_PARAM_TRF_TER, "tipoTransferenciaTTFF",
                "CTAPROPIAS"))
                && transferencia.getCodBanco1().equals(
                    TablaValores.getValor(ARCH_PARAM_TRF_TER, "codBancoBCITTFF", "Desc"))) {
                if (getLogger().isEnabledFor(Level.INFO)){
                    getLogger().info("[realizaTransferenciaDeFondos][" + numTransferencia + "] Transferencia es a cuenta propia");
                }
                BCIExpress bciExpress = crearEJBBCIExpress();
                try {
                    String servicioFirmaCuentaPropia =
                        TablaValores.getValor(ARCH_PARAM_EMP, "FirmaCuentasPropias", "Desc");
                    ResultTransferenciaFondoCuentaCorriente respuesta =
                        (ResultTransferenciaFondoCuentaCorriente) bciExpress
                            .transferenciaFondoEntreBciModoManual(ctaOrigen, String.valueOf(transferencia
                                .getMontoTransfer()), cuentaAbono, servicioFirmaCuentaPropia);

                    TransfFondoCtaCte[] tranFonCtaCte = respuesta.getTransfFondoCtaCte();

                    if (getLogger().isEnabledFor(Level.INFO)){
                        getLogger().info("[realizaTransferenciaDeFondos][" + numTransferencia + "] TFF a Cta Cte: " + tranFonCtaCte);
                    }
                    if (tranFonCtaCte != null) {
                        if (tranFonCtaCte[0] == null) {
                            if (getLogger().isEnabledFor(Level.ERROR)) {
                                getLogger().error("[realizaTransferenciaDeFondos][" + numTransferencia + "] Error en transferencia");
                            }
                            throw new GeneralException("PAGOS027");
                        }
                    }
                    else {
                        if (getLogger().isEnabledFor(Level.ERROR)) {
                            getLogger().error("[realizaTransferenciaDeFondos][" + numTransferencia + "] Error en transferencia");
                        }
                        throw new GeneralException("PAGOS027");
                    }
                    resultadoTransf = true;
                    if (getLogger().isEnabledFor(Level.INFO)){
                        getLogger().info("[realizaTransferenciaDeFondos][" + numTransferencia + "] resultado " + resultadoTransf);
                    }

                    codigoEstado = TablaValores.getValor(ARCH_PARAM_TRF_TER, "codRechazoTTFF", "Desc");
                    estadoTransaccion =
                        TablaValores.getValor(ARCH_PARAM_TRF_TER, "codigosEstadosTransferencia", "realizada");

                }
                catch (Exception e) {
                    if (getLogger().isEnabledFor(Level.ERROR)) {
                        getLogger().error("[realizaTransferenciaDeFondos][" + numTransferencia + "][BCI_FINEX][Exception e]" + e.getMessage());
                    }
                    if (e instanceof BCIExpressException) {
                        BCIExpressException bex = (BCIExpressException) e;
                        codigoEstado = bex.getCodigo();
                        
                        if (getLogger().isEnabledFor(Level.ERROR)) {
                            getLogger().error("[realizaTransferenciaDeFondos][" + numTransferencia + "] ErrorBCIExpressException, codigoEstado ["+ codigoEstado +"]");
                        }
                    }
                    resultadoTransf = false;
                    estadoTransaccion =
                        TablaValores.getValor(ARCH_PARAM_TRF_TER, "codigosEstadosTransferencia", "noRealizada");
                }

                if (getLogger().isEnabledFor(Level.INFO)){
                    getLogger().info("[realizaTransferenciaDeFondos][" + numTransferencia + "] codigoEstado: [" + codigoEstado + "], estadoTransaccion ["+ estadoTransaccion +"]");
                }
                cambiaEstadoTransferenciaPago(transferencia.getIdentificador(), 1, estadoTransaccion,
                    codigoEstado, "");
                if (!resultadoTransf) {
                    throw new GeneralException("PAGOS027");
                }

            }
            else if (tipoTransferencia.equals(TablaValores.getValor(ARCH_PARAM_TRF_TER, "tipoTransferenciaTTFF",
                "BCI"))
                && transferencia.getCodBanco1().equals(
                    TablaValores.getValor(ARCH_PARAM_TRF_TER, "codBancoBCITTFF", "Desc"))) {
                if (getLogger().isEnabledFor(Level.INFO)){
                    getLogger().info("[realizaTransferenciaDeFondos][" + numTransferencia + "] Transferencia es a tercero BCI");
                }
                try {
                    BCIExpress bciExpress = crearEJBBCIExpress();
                    String trFnVrcodCajero = TablaValores.getValor(ARCH_PARAM_EMP, "TrFnVrcodCajero", "Desc");
                    String trFnVridfCajero = TablaValores.getValor(ARCH_PARAM_EMP, "TrFnVridfCajero", "Desc");
                    String trFnVrsupervisor = TablaValores.getValor(ARCH_PARAM_EMP, "TrFnVrsupervisor", "Desc");
                    String trFnVrmodEntren = TablaValores.getValor(ARCH_PARAM_EMP, "TrFnVrmodEntren", "Desc");
                    String trFnVrmodEntrada = TablaValores.getValor(ARCH_PARAM_EMP, "TrFnVrmodEntrada", "Desc");
                    cuentaAbono = StringUtil.sacaCeros(cuentaAbono);
                    cuentaAbono = StringUtil.rellenaConCeros(cuentaAbono, LARGO_CUENTA_OCHO);
                    String servicioFirmaTerceroBCI =
                        TablaValores.getValor(ARCH_PARAM_EMP, "FirmaTerceros", "Desc");
                    ResultTransferenciaFondoCuentaCorriente respuesta =
                        bciExpress.transferenciaFondoEntreBciModoManualTranTer(ctaOrigen, String
                            .valueOf(transferencia.getMontoTransfer()), cuentaAbono, trFnVrcodCajero,
                            trFnVridfCajero, trFnVrsupervisor, trFnVrmodEntren, trFnVrmodEntrada,
                            servicioFirmaTerceroBCI);

                    TransfFondoCtaCte[] tranFonCtaCte = respuesta.getTransfFondoCtaCte();

                    if (getLogger().isEnabledFor(Level.INFO)){
                        getLogger().info("[realizaTransferenciaDeFondos][" + numTransferencia + "] tranFonCtaCte: " + tranFonCtaCte);
                    }
                    if (tranFonCtaCte != null) {
                        if (tranFonCtaCte[0] == null) {
                            if (getLogger().isEnabledFor(Level.ERROR)) {
                                getLogger().error("[realizaTransferenciaDeFondos][" + numTransferencia + "] Error en transferencia");
                            }
                            throw new GeneralException("PAGOS027");
                        }
                    }
                    else {
                        if (getLogger().isEnabledFor(Level.ERROR)) {
                            getLogger().error("[realizaTransferenciaDeFondos][" + numTransferencia + "] Error en transferencia");
                        }
                        throw new GeneralException("PAGOS027");
                    }

                    resultadoTransf = true;
                    if (getLogger().isEnabledFor(Level.INFO)){
                        getLogger().info("[realizaTransferenciaDeFondos][" + numTransferencia + "] resultado " + resultadoTransf);
                    }

                    codigoEstado = TablaValores.getValor(ARCH_PARAM_TRF_TER, "codRechazoTTFF", "Desc");
                    estadoTransaccion =
                        TablaValores.getValor(ARCH_PARAM_TRF_TER, "codigosEstadosTransferencia", "realizada");

                }
                catch (Exception e) {
                    if (getLogger().isEnabledFor(Level.ERROR)) {
                        getLogger().error("[realizaTransferenciaDeFondos][" + numTransferencia + "][Exception e]" + e.getMessage());
                    }
                    if (e instanceof BCIExpressException) {
                        BCIExpressException bex = (BCIExpressException) e;
                        codigoEstado = bex.getCodigo();
                        if (getLogger().isEnabledFor(Level.ERROR)) {
                            getLogger().error("[realizaTransferenciaDeFondos][" + numTransferencia + "] ErrorBCIExpressException, codigoEstado: ["+ codigoEstado +"]");
                        }
                    }
                    resultadoTransf = false;
                    estadoTransaccion =
                        TablaValores.getValor(ARCH_PARAM_TRF_TER, "codigosEstadosTransferencia", "noRealizada");
                }

                if (getLogger().isEnabledFor(Level.INFO)){
                    getLogger().info("[realizaTransferenciaDeFondos][" + numTransferencia + "] codigoEstado: [" + codigoEstado +"], estadoTransaccion: ["+ estadoTransaccion +"]");
                }
                cambiaEstadoTransferenciaPago(transferencia.getIdentificador(), 1, estadoTransaccion,
                    codigoEstado, "");
                if (!resultadoTransf) {
                    throw new GeneralException(codigoEstado);
                }

            }
            else if (tipoTransferencia.equals(TablaValores.getValor(ARCH_PARAM_TRF_TER, "tipoTransferenciaTTFF",
                "OTR"))
                && !transferencia.getCodBanco1().equals(
                    TablaValores.getValor(ARCH_PARAM_TRF_TER, "codBancoBCITTFF", "Desc"))) {
                if (getLogger().isEnabledFor(Level.INFO)){
                    getLogger().info("[realizaTransferenciaDeFondos][" + numTransferencia + "] Transferencia es a tercero OTB");
                }
                String codigoCCA = "";

                try {
                    TransferenciasDeFondos ttff = crearEJBTransferenciasDeFondos();
                    String tipoCuentaOrigen =
                        TablaValores.getValor(ARCH_PARAM_TRF_TER, "tiposCuenta", "corriente");
                    String tipoCuentaDestino =
                        TablaValores.getValor(ARCH_PARAM_TRF_TER, "tiposCuenta", "corriente");
                    String servicioFirmaTerceros = TablaValores.getValor(ARCH_PARAM_EMP, "FirmaTerceros", "Desc");
                    CCADirectaVO resultadoCCA =
                        ttff.transferenciaCtaCteOtroBancoEnLineaEmpresa(tipoCuentaOrigen, transferencia
                            .getCtaOrigen(), cuentaAbono, tipoCuentaDestino, String.valueOf(transferencia
                            .getMontoTransfer()), transferencia.getRutDestin(), transferencia
                            .getDigitoVerificador(), transferencia.getCodBanco1(), transferencia
                            .getIdentificador(), transferencia.getMensajeDestinatario(), transferencia
                            .getNombreDestin(), String.valueOf(transferencia.getRutUsuario()), transferencia
                            .getNumConvenio(), servicioFirmaTerceros, cliente, transferencia.getCanal());

                    codigoCCA = resultadoCCA.getTipoRespuesta();

                    if (getLogger().isEnabledFor(Level.INFO)){
                        getLogger().info("[realizaTransferenciaDeFondos][" + numTransferencia + "] codigoCCA: " + codigoCCA);
                    }
                }
                catch (Exception e) {
                    String msgErr = "";
                    if (getLogger().isEnabledFor(Level.ERROR)) {
                        getLogger().error("[realizaTransferenciaDeFondos][" + numTransferencia + "][BCI_FINEX][Exception e]" + e.getMessage());
                    }

                    if (e instanceof TransferenciaException) {
                        TransferenciaException te = (TransferenciaException) e;
                        if (getLogger().isEnabledFor(Level.ERROR)) {
                            getLogger().error("[realizaTransferenciaDeFondos][" + numTransferencia + "] TransferenciaException: [" + te.getCodigo() + te.getMessage());
                        }

                        String glosaBancoDestino = "";
                        glosaBancoDestino =
                            TablaValores.getValor("bancos.codigos", transferencia.getCodBanco1(), "Desc");

                        if (getLogger().isEnabledFor(Level.ERROR)) {
                            getLogger().error("[realizaTransferenciaDeFondos][" + numTransferencia + "] TransferenciaException: glosaBancoDestino [" + glosaBancoDestino);
                        }

                        if (te.getCodigo().equals("TRF057")) {
                            msgErr = TablaValores.getValor("errores.codigos", "TRF057", "Desc");
                        }

                        if (te.getCodigo().equals("TRF100")) {
                            msgErr = TablaValores.getValor("errores.codigos", "TRF100", "Desc");
                            msgErr = StringUtil.reemplazaUnaVez(msgErr, "%CAMPO%", glosaBancoDestino);
                        }

                        if (getLogger().isEnabledFor(Level.ERROR)) {
                            getLogger().error("[realizaTransferenciaDeFondos][" + numTransferencia + "] msgErr: ["+ msgErr +"]");
                            getLogger().error("[realizaTransferenciaDeFondos][" + numTransferencia + "] Se borrara autorizacion de tabla AUT");
                        }

                        BCIExpress bciExpress = crearEJBBCIExpress();
                        bciExpress.borraTransferenciaAUT(String.valueOf(transferencia.getRutUsuario()),
                            transferencia.getDigitoVerifUsu(), transferencia.getNumConvenio(), String
                                .valueOf(transferencia.getRutEmpresa()), transferencia.getDigitoVerifEmp(),
                            transferencia.getIdentificador());
                        if (getLogger().isEnabledFor(Level.INFO)){
                            getLogger().info("[realizaTransferenciaDeFondos][" + numTransferencia + "] Se borra registro de AUT sin Problemas");
                        }

                        if (!msgErr.equals("")) {
                            throw new GeneralException(te.getCodigo(), msgErr);
                        }
                    }

                    resultadoTransf = false;

                    throw new GeneralException("", msgErr);
                }
                if (codigoCCA.equals(TablaValores.getValor(ARCH_PARAM_TRF_TER, "codigoCCA", "exito"))) {
                    codigoEstado = TablaValores.getValor(ARCH_PARAM_TRF_TER, "codRechazoTTFF", "Desc");
                    resultadoTransf = true;
                    if (getLogger().isEnabledFor(Level.INFO)){
                        getLogger().info("[realizaTransferenciaDeFondos][" + numTransferencia + "] codigoEstado: " + codigoEstado);
                    }
                }
                else {
                    resultadoTransf = false;
                    String msgErr = "";
                    if (getLogger().isEnabledFor(Level.INFO)){
                        getLogger().info("[realizaTransferenciaDeFondos][" + numTransferencia + "] codigoCCA: " + codigoCCA);
                    }

                    if (TablaValores.getValor("TransferenciaTerceros.parametros", "0" + codigoCCA, "tipo") != null
                        && TablaValores.getValor("TransferenciaTerceros.parametros", "0" + codigoCCA, "tipo")
                            .equalsIgnoreCase("NOKSISTEMA")) {
                        if (getLogger().isEnabledFor(Level.ERROR)) {
                            getLogger().error("[realizaTransferenciaDeFondos][" + numTransferencia + "] ERROR DE SISTEMA:" + codigoCCA);
                        }

                        if (getLogger().isEnabledFor(Level.INFO)){
                            getLogger().info("[realizaTransferenciaDeFondos][" + numTransferencia + "] Se borrara la Autorizacion de la tabla AUT");
                        }
                        BCIExpress bciExpress = crearEJBBCIExpress();
                        bciExpress.borraTransferenciaAUT(String.valueOf(transferencia.getRutUsuario()),
                            transferencia.getDigitoVerifUsu(), transferencia.getNumConvenio(), String
                                .valueOf(transferencia.getRutEmpresa()), transferencia.getDigitoVerifEmp(),
                            transferencia.getIdentificador());

                        if (getLogger().isEnabledFor(Level.INFO)){
                            getLogger().info("[realizaTransferenciaDeFondos][" + numTransferencia + "]Se borra registro de la AUT sin Problemas");
                        }

                        String mensajeCCA =
                            TablaValores.getValor("TransferenciaTerceros.parametros", "0" + codigoCCA, "mensaje");
                        if (getLogger().isEnabledFor(Level.INFO)){
                            getLogger().info("[realizaTransferenciaDeFondos][" + numTransferencia + "] mensajeCCA: " + mensajeCCA);
                        }
                        String bancoDestino =
                            TablaValores.getValor("bancos.codigos", transferencia.getCodBanco1(), "Desc");

                        if (mensajeCCA != null) {
                            msgErr =
                                TablaValores.getValor("TransferenciaTerceros.parametros", "BCO_NO_DISPONIBLE",
                                    "mensaje");
                            msgErr = StringUtil.reemplazaTodo(msgErr, "<bancoDestino>", bancoDestino);

                            if (getLogger().isEnabledFor(Level.ERROR)) {
                                getLogger().error("[realizaTransferenciaDeFondos][" + numTransferencia + "] Error : " + msgErr);
                            }
                        }

                    }
                    else {
                        if (getLogger().isEnabledFor(Level.ERROR)) {
                            getLogger().error("[realizaTransferenciaDeFondos][" + numTransferencia + "] ERROR DE CLIENTE:" + codigoCCA);
                        }
                        if(!codigoCCA.equals("")){
                        codigoEstado =
                            TablaValores.getValor(ARCH_PARAM_TRF_TER, "prefijoEstadoCCA", "desc")
                                + codigoCCA.substring(codigoCCA.length() - LARGO_CODIGO_CCA);
                        }
						else {
                            throw new GeneralException("PAGOS060");
                        }
                        if (getLogger().isEnabledFor(Level.INFO)){
                            getLogger().info("[realizaTransferenciaDeFondos][" + numTransferencia + "] codigoEstado: " + codigoEstado);
                        }

                        if (TablaValores.getValor(ARCH_PARAM_TRF_TER, "0" + codigoCCA, "mensaje") != null) {
                            if (getLogger().isEnabledFor(Level.ERROR)){
                                getLogger().error("[realizaTransferenciaDeFondos][" + numTransferencia + "] mensaje Error A:" + msgErr);
                            }
                            msgErr =
                                TablaValores.getValor("TransferenciaTerceros.parametros", "0" + codigoCCA,
                                    "mensaje");
                            if (getLogger().isEnabledFor(Level.ERROR)){
                                getLogger().error("[realizaTransferenciaDeFondos][" + numTransferencia + "] mensaje Error B:" + msgErr);
                            }
                        }
                    }
                    if (!msgErr.equals("")) {
                        throw new GeneralException("TRF057", msgErr);
                    }
                    else {
                        throw new GeneralException("", msgErr);
                    }
                }

            }
            else {
                throw new Exception("Tipo de transferencia no implementado");
            }

            if (resultadoTransf) {
                try {
                    String mailDestinatario = transferencia.getEmailDestinatario();
                    if (getLogger().isEnabledFor(Level.INFO)){
                        getLogger().info("[realizaTransferenciaDeFondos][" + numTransferencia + "] Se realiza envio de mail");
                        getLogger().info("[realizaTransferenciaDeFondos][" + numTransferencia + "] Mail: " + mailDestinatario);
                    }
                    if (mailDestinatario != null && !mailDestinatario.trim().equals("")) {
                        enviarCorreoTransferencia(transferencia);
                    }
                }
                catch (Exception e) {
                    if (getLogger().isEnabledFor(Level.ERROR)) {
                        getLogger().error("[realizaTransferenciaDeFondos][" + numTransferencia + "][BCI_FINEX][Exception e]" + e.getMessage());
                    }
                }
            }

        }
        catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[realizaTransferenciaDeFondos][" + numTransferencia + "][BCI_FINEX][Exception e]" + e.getMessage(), e);
            }
            throw (e instanceof GeneralException) ? (GeneralException) e : new GeneralException("PAGOS027");
        }
        if (getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[realizaTransferenciaDeFondos][" + numTransferencia + "][BCI_FINOK] resultado transferencia ["+ resultadoTransf +"]");
        }
        return resultadoTransf;
    }

    /**
     * M�todo que realiza el envio de correo de una transferencia.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 18/12/2014 Pablo Romero C�ceres (SEnTRA) : versi�n incial.
     * <li>2.0 11/02/2016 Pablo Romero C�ceres (SEnTRA) - Jorge Lara D�az (Ing. Soft. BCI): Se elimina el par�metro de entrada <code>Cliente</code> que era utilizado
     * para rescatar el nombre del cliente, en su lugar se usa el atributo <code>nombreCliente</code> del {@link #TransferenciaATercerosTO} y adem�s
     * se elimina diferencia de comprobante seg�n banco.
     * </ul>
     * @param transferencia Datos de la transferencia.
     * @since 3.3
     */
    private void enviarCorreoTransferencia(TransferenciaATercerosTO transferencia) {
        String numTransferencia = transferencia.getIdentificador();
        
        if (getLogger().isEnabledFor(Level.INFO)) { 
            getLogger().info("[enviarCorreoTransferencia][" + numTransferencia + "][BCI_INI]" );
            getLogger().info("[enviarCorreoTransferencia][" + numTransferencia + "] transferencia [" + transferencia + "]");
        }
        GeneradorComprobanteTransferenciaDeFondos componente = new GeneradorComprobanteTransferenciaDeFondos();
        componente.setNombreCliente(transferencia.getNombreCliente());
        componente.setBancoOrigen(
            TablaValores.getValor(ARCH_PARAM_TRF_TER, "codigoBCI", "desc_" + transferencia.getCanal()));

        componente.setNombreDestinatario(transferencia.getNombreDestin());
        componente.setBancoDestino(transferencia.getDescripcionBanco());
        componente.setCuentaDestino(transferencia.getCtaDestino());

        componente.setNumeroOperacion(transferencia.getIdentificador());
        componente.setFechaTransferencia(transferencia.getFechaPago());
        componente.setFechaAbono(transferencia.getFechaPago());
        componente.setMonto(transferencia.getMontoTransfer());
        componente.setComentario(transferencia.getMensajeDestinatario());
        componente.setTipoComprobante(ID_COMP_DEST_OTRO_BANCO);

        if (getLogger().isEnabledFor(Level.INFO)) { 
            getLogger().info("[enviarCorreoTransferencia][" + numTransferencia + "] componente : " + componente);
        }
        Map parametros = new HashMap();
        parametros.put("canal", CANAL_CORREO_BCI);
        parametros.put("otroBanco", "S");
        
        GeneradorDeComponentesMIME generador = new GeneradorDeComponentesMIME();
        generador.setComponente(componente);
        generador.agregarFormatoHtml(ID_COMP_DEST_OTRO_BANCO, CANAL_CORREO_BCI);
        Map correos = generador.generarComponentes(parametros);
        String correoTexto = (String) correos.get("XSLT_" + ID_COMP_DEST_OTRO_BANCO + "_TXT_" + CANAL_CORREO_BCI);
        if (correoTexto != null) {
            String correoHtml = (String) correos.get("XSLT_" + ID_COMP_DEST_OTRO_BANCO + "_HTML_" + CANAL_CORREO_BCI);
            if (correoHtml != null) {
                correoTexto = "MIME" + correoTexto + "|T&H|" + correoHtml;
            }
        }
        if (getLogger().isEnabledFor(Level.INFO)) { 
            getLogger().info("[enviarCorreoTransferencia][" + numTransferencia + "] : " + correoTexto);
        }

        String asunto = TablaValores.getValor(ARCH_PARAM_TRF_TER, "asuntoEmail", "valor");
        String contenidoPlano = "";
        String contenidoHTML = "";

        String from = TablaValores.getValor(ARCH_PARAM_TRF_TER, "correo", "mailFrom");
        String tipoCorreo = TablaValores.getValor(ARCH_PARAM_TRF_TER, "tipoCorreo", "latinia");
        boolean latinia = (new Boolean(tipoCorreo)).booleanValue();
        if (!latinia) {
            try{
                int resultado =
                    EnvioDeCorreo.simple(null, from, transferencia.getEmailDestinatario(), asunto, correoTexto,
                        null);
                if (getLogger().isEnabledFor(Level.INFO)) { 
                    getLogger().info("[enviarCorreoTransferencia][" + numTransferencia + "][BCI_FINOK] resultado EnvioDeCorreo.simple " + resultado);
                }
            }
            catch (Exception e){
                if (getLogger().isEnabledFor(Level.ERROR)) {
                    getLogger().error("[enviarCorreoTransferencia][" + numTransferencia + "][BCI_FINEX][Exception e]" + e.getMessage(), e);
                }
            }
        }
        else{
            try{
                String loginEnterprise =
                    TablaValores.getValor(ARCH_PARAM_TRF_TER, "loginEnterprise" + CANAL_CORREO_BCI, "banco");
                String refContract = TablaValores.getValor(ARCH_PARAM_TRF_TER, "contract" + CANAL_CORREO_BCI, "refContract");
                if(correoTexto != null && correoTexto.length() > DELIMITADOR_CORRREO){
                    correoTexto = correoTexto.substring(DELIMITADOR_CORRREO, correoTexto.length());
                    String[] contendioMail = StringUtil.divideConString(correoTexto, "|T&H|");
                    if(contendioMail != null && contendioMail.length >= 2){
                        contenidoPlano = contendioMail[0];
                        contenidoHTML = contendioMail[1];
                    }
                    else{
                        contenidoPlano = correoTexto;
                        contenidoHTML = correoTexto;
                    }
                }
                else{
                    contenidoPlano = correoTexto;
                    contenidoHTML = correoTexto;
                }
                Map paramsMail = new HashMap();
                paramsMail.put("to", transferencia.getEmailDestinatario());
                paramsMail.put("subject", asunto);
                paramsMail.put("body", contenidoPlano);
                paramsMail.put("bodyHtml", contenidoHTML);

                String contexto = TablaValores.getValor("JNDIConfig.parametros",  "cluster", "param");

                ConectorServicioEnvioSDP instance = new ConectorServicioEnvioSDP(contexto);

                int result = instance.sendMail( loginEnterprise, refContract,     paramsMail);
                if (getLogger().isEnabledFor(Level.INFO)){
                    getLogger().info("[enviarCorreoTransferencia][" + numTransferencia + "][BCI_FINOK] resultado latinia : " + result);
                }

            }
            catch (Exception e){
                if (getLogger().isEnabledFor(Level.ERROR)) {
                    getLogger().error("[enviarCorreoTransferencia][" + numTransferencia + "][BCI_FINEX][Exception e]" + e.getMessage(), e);
                }
            }
        }
    }

    /**
     * M�todo encargado de crear una instacia del EJB TransferenciasDeFondos.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 18/12/2014 Pablo Romero C�ceres (SEnTRA) : versi�n incial.
     * </ul>
     * <p>
     * 
     * @return Instancia del EJB TransferenciasDeFondos.
     * @throws GeneralException en caso de error.
     * @since 3.3
     */
    private TransferenciasDeFondos crearEJBTransferenciasDeFondos() throws GeneralException {
        try {
            if (getLogger().isEnabledFor(Level.INFO)){
                getLogger().info("[crearEJBTransferenciasDeFondos][BCI_INI]");
            }
            EnhancedServiceLocator locator = EnhancedServiceLocator.getInstance();
            TransferenciasDeFondosHome transferenciasDeFondosHome =
                (TransferenciasDeFondosHome) locator
                .getGenericService(
                    "wcorp.aplicaciones.productos.servicios.transferencias.implementacion.TransferenciasDeFondos",
                    TransferenciasDeFondosHome.class);
            if (getLogger().isEnabledFor(Level.INFO)){getLogger().info("[crearEJBTransferenciasDeFondos][BCI_FINOK]");}
            return transferenciasDeFondosHome.create();
        }
        catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[crearEJBTransferenciasDeFondos][BCI_FINEX][Exception e]" + ErroresUtil.extraeStackTrace(e));
            }
            throw new GeneralException("ESPECIAL", ErroresUtil.extraeStackTrace(e));
        }
    }

    /**
     * M�todo encargado de crear una instacia del EJB BCIExpress.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 18/12/2014 Pablo Romero C�ceres (SEnTRA) : versi�n incial.
     * </ul>
     * <p>
     * 
     * @return Instancia del EJB BCIExpress.
     * @throws GeneralException en caso de error.
     * @since 3.3
     */
    private BCIExpress crearEJBBCIExpress() throws GeneralException {
        try {
            if (getLogger().isEnabledFor(Level.INFO)){
                getLogger().info("[crearEJBBCIExpress][Inicio]");
            }
            EnhancedServiceLocator locator = EnhancedServiceLocator.getInstance();
            BCIExpressHome bciExpressHome =
                (BCIExpressHome) locator.getGenericService("wcorp.serv.bciexpress.BCIExpress",
                    BCIExpressHome.class);
            if (getLogger().isEnabledFor(Level.INFO)){getLogger().info("[crearEJBBCIExpress][BCI_FINOK]");}
            return bciExpressHome.create();
        }
        catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[crearEJBBCIExpress][BCI_FINEX][Exception e]" + ErroresUtil.extraeStackTrace(e));
            }
            throw new GeneralException("ESPECIAL", ErroresUtil.extraeStackTrace(e));
        }
    }

    /**
     * M�todo que cambia el estado de una transferencia.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 22/12/2014 Pablo Romero C�ceres (SEnTRA) : versi�n incial.
     * </ul>
     * @param numeroTransferencia N�mero de la transferencia.
     * @param numeroLinea N�mero de l�nea.
     * @param codigoEstadoTransaccion C�digo Estado Transacci�n.
     * @param codigoEstado C�digo Estado.
     * @param glosaDetalle Glosa Detalle.
     * @return <b>true</b> si esta correcto.
     * @throws GeneralException en caso de error.
     * @since 3.3
     */
    public boolean cambiaEstadoTransferenciaPago(String numeroTransferencia, int numeroLinea, 
        String codigoEstadoTransaccion, String codigoEstado, String glosaDetalle) throws GeneralException{
        try {
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[cambiaEstadoTransferenciaPago][" + numeroTransferencia + "][BCI_INI]" );
                getLogger().info("[cambiaEstadoTransferenciaPago][" + numeroTransferencia + "] numeroLinea ["+ numeroLinea +"], codigoEstadoTransaccion ["+ codigoEstadoTransaccion +"], codigoEstado ["+ codigoEstado +"], glosaDetalle ["+ glosaDetalle +"]");
            }
            NominaDeTransferenciasDAO nominaDeTransferenciaDao = new NominaDeTransferenciasDAO();
            boolean respuesta = nominaDeTransferenciaDao.cambiaEstadoTransferenciaPago(
                numeroTransferencia, numeroLinea, codigoEstadoTransaccion, codigoEstado, glosaDetalle);
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[cambiaEstadoTransferenciaPago][" + numeroTransferencia + "][BCI_FINOK] respuesta: " + respuesta);
            }
            return respuesta;
        }
        catch(GeneralException e) {
            if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[cambiaEstadoTransferenciaPago][" + numeroTransferencia + "][BCI_FINEX][GeneralException e]" + e.getMessage(), e);
            }
            throw e;
        }
    }
    
    
    /**
     * M�todo que revisa si nomina de transferencia es necesaria caducarla debido a restricciones de fechas
     * para una acci�n y origen espec�fico.
     * La regla general que se revisa es si la transferencias tiene firmas con fecha anterior cuya diferencia
     * con el d�a actual es mayor al rango definido param�tricamente.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 28/12/2015 Jaime Gaete L. (SEnTRA) - Jorge Lara D�az (ing. Soft. BCI) : versi�n incial.
     * </ul>
     * @param transferencia Objeto con transferencia a revisar.
     * @param origen Origen de la petici�n.
     * @param accion Accion a revisar. 
     * @return <b>true</b> si corresponde caducaci�n.
     * @throws GeneralException en caso de error.
     * @since 3.8
     */   
    public boolean revisarCaducidadDeTransferencia(TransferenciaATercerosTO transferencia, String origen, 
    String accion) throws GeneralException {

        boolean respuesta = false; 	
        if(transferencia != null){
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                String strFirmaAlmacenada= TablaValores.getValor(
                    ARCH_PARAM_TRF_TER, "firmaAlmacenada", "habilitado");
                boolean esHabilitadoRangoFirma = strFirmaAlmacenada!= null
                    ?new Boolean(strFirmaAlmacenada).booleanValue():false;  
                if (getLogger().isDebugEnabled()) {
                    getLogger().debug("[revisarCaducidadNomina]strFirmaAlmacenada:"+strFirmaAlmacenada);
                    getLogger().debug("[revisarCaducidadNomina]esHabilitadoRangoFirma:"+esHabilitadoRangoFirma);
                }
                if(esHabilitadoRangoFirma){
                    int rango = Integer.parseInt(TablaValores.getValor(ARCH_PARAM_TRF_TER, 
                        "rangoFirmaAlmacenada"+origen, accion));
                    if (getLogger().isDebugEnabled()) {
                        getLogger().debug("[revisarCaducidadNomina] rango:"+ rango);
                    }

                    ResultConsultarApoderadosquehanFirmado firmas=crearEJBBCIExpress()
                        .consultarApoderadosquehanFirmado(String.valueOf(transferencia.getRutEmpresa()),
                        transferencia.getNumConvenio(), transferencia.getIdentificador());
                    if(firmas!= null && firmas.getConApoquehanFirmado()!= null
                        &&firmas.getConApoquehanFirmado().length>0){
                        ConApoquehanFirmado[] apoFir = firmas.getConApoquehanFirmado();
                        for (int i = 0; i < apoFir.length; i++) {
                            Date fechaFirma = sdf.parse(apoFir[i].getFechaCreacion());
                            if (getLogger().isDebugEnabled()) {
                                getLogger().debug("[revisarCaducidadNomina]firmas:" + apoFir[i].toString());
                                getLogger().debug("[revisarCaducidadNomina]dif:"
                                        + FechasUtil.diferenciaDeDias(fechaFirma, new Date()));
                            }
                            if (FechasUtil.diferenciaDeDias(fechaFirma, new Date()) > rango) {
                                respuesta = true;
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception e) {
                if (getLogger().isEnabledFor(Level.ERROR)) {
                    getLogger().error("[revisarCaducidadNomina][Exception] error al revisar: "
                            + e.getMessage(), e);
                }
                throw new GeneralException("PAGOS018");
            }
        } 
        return respuesta;
    }
    
    /**
     * M�todo que caduca una transferencia espec�fica.
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 28/12/2015 Jaime Gaete L. (SEnTRA) - Jorge Lara D�az (ing. Soft. BCI) : versi�n incial.
     * </ul>
     * @param transferenciaACaducar Objeto con transferencia a caducar.
     * @return <b>true</b> si esta correcta la caducaci�n.
     * @throws GeneralException en caso de error.
     * @since 3.8
     */   
    public boolean caducarNominaDeTransferencia(FiltroTO transferenciaACaducar)
    		throws GeneralException{
        try {
            if (getLogger().isInfoEnabled()) {
                getLogger().debug("[caducarNominaDeTransferencia] Inicio transferenciaACaducar[" 
                    + transferenciaACaducar + "]");
            }
            NominaDeTransferenciasDAO nominaDeTransferenciaDao = new NominaDeTransferenciasDAO();
            boolean respuesta = nominaDeTransferenciaDao.caducarNominaDeTransferencia(
            		transferenciaACaducar);
            if (getLogger().isDebugEnabled()) {
                getLogger().debug("[caducarNominaDeTransferencia] respuesta: "
                    + respuesta);
            }
            return respuesta;
        }
        catch(GeneralException e) {
            if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[cambiaEstadoTransferenciaPago]"
                   +"[GeneralException] error con mensaje " + e.getMessage(), e);
            }
            throw e;
        }
    }
    
    /**
     * M�todo encargado de consultar transferencias que puedan ser posibles duplicados.
     * <p>
     * <b>Registro de versiones:</b>
     * <ul>
     * <li> 1.0 12/04/2016 Jaime Gaete L. (SEnTRA) - Jorge Lara D�az (Ing. Soft. BCI): Versi�n inicial.</li>
     * </ul>
     * <p>
     * 
     * @param transferencia Transferencia original.
     * @param filtroBusqueda Objeto que detalla filtros espec�ficos para la b�squeda (tipo transferencia, origen, producto de pago)
     * @param agpEstadosPen C�digo que representa agrupaci�n de estados pendientes.
     * @param agpEstadosFin C�digo que representa agrupaci�n de estados finales.
     * @return Arreglo con posibles transferecias duplicadas.
     * @throws GeneralException en caso de error
     * @since 3.13
     */
    public TransferenciaATercerosTO[]  consultarPosiblesTransferenciasDuplicadas(TransferenciaATercerosTO transferencia, FiltroTO filtroBusqueda, 
            String agpEstadosPen, String agpEstadosFin)
            throws GeneralException{
        TransferenciaATercerosTO[] transferencias = null;
        String numeroTransferencia = "";
        try {
            numeroTransferencia = transferencia.getIdentificador() == null ? "":transferencia.getIdentificador();
            if (getLogger().isInfoEnabled()) {
                getLogger().info("[consultarPosiblesTransferenciasDuplicadas]["+numeroTransferencia+"][BCI_INI]  Inicio transferencia[" + transferencia 
                        + "], filtroBusqueda["+filtroBusqueda+"], agpEstadosPen["+agpEstadosPen+"], agpEstadosFin["+agpEstadosFin+"]");
            }
            NominaDeTransferenciasDAO nominaDeTransferenciaDao = new NominaDeTransferenciasDAO();
            transferencias = nominaDeTransferenciaDao.consultarPosiblesTransferenciasDuplicadas(transferencia, filtroBusqueda, agpEstadosPen, agpEstadosFin);
            if (getLogger().isInfoEnabled()) {
                getLogger().info("[consultarPosiblesTransferenciasDuplicadas]["+numeroTransferencia+"][BCI_FINOK] respuesta: "+ StringUtil.contenidoDe(transferencias));
            }
            return transferencias;
        }
        catch(GeneralException e) {
            if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[consultarPosiblesTransferenciasDuplicadas]["+numeroTransferencia+"][BCI_FINEX][Exception] error con mensaje=<" + e.getMessage()+">", e);
            }
            throw e;
        }
    }    
    
    /**
     * M�todo encargado de consultar n�minas que puedan ser posibles duplicados.
     * <p>
     * <b>Registro de versiones:</b>
     * <ul>
     * <li> 1.0 17/05/2016 Jaime Gaete L. (SEnTRA) - Paola Avalos(Ing. Soft. BCI): Versi�n inicial.</li>
     * </ul>
     * <p>
     * 
     * @param nomina N�mina original.
     * @param filtroBusqueda Objeto que detalla filtros espec�ficos para la b�squeda  (tipo transferencia, origen, producto de pago).
     * @param agpEstadosPen C�digo que representa agrupaci�n de estados pendientes.
     * @param agpEstadosFin C�digo que representa agrupaci�n de estados finales.
     * @return Arreglo con posibles n�minas duplicadas.
     * @throws GeneralException en caso de error.
     * @since 3.14
     */
    public InformacionCargaMasiva[]  consultarPosiblesNominasDuplicadas(InformacionCargaMasiva nomina, FiltroTO filtroBusqueda, 
            String agpEstadosPen, String agpEstadosFin)
            throws GeneralException{
        InformacionCargaMasiva[] nominasDuplicadas = null;
        String numeroTransferencia = "";
        try {
            numeroTransferencia = nomina.getNumTransferencia() == null ? "":nomina.getNumTransferencia();
            if (getLogger().isInfoEnabled()) {
                getLogger().info("[consultarPosiblesNominasDuplicadas]["+numeroTransferencia+"][BCI_INI]  Inicio nomina[" + nomina 
                        + "], filtroBusqueda["+filtroBusqueda+"], agpEstadosPen["+agpEstadosPen+"], agpEstadosFin["+agpEstadosFin+"]");
            }
            NominaDeTransferenciasDAO nominaDeTransferenciaDao = new NominaDeTransferenciasDAO();
            nominasDuplicadas = nominaDeTransferenciaDao.consultarPosiblesNominasDuplicadas(nomina, filtroBusqueda, agpEstadosPen, agpEstadosFin);
            if (getLogger().isInfoEnabled()) {
                getLogger().info("[consultarPosiblesNominasDuplicadas]["+numeroTransferencia+"][BCI_FINOK] respuesta: "+ StringUtil.contenidoDe(nominasDuplicadas));
            }
            return nominasDuplicadas;
        }
        catch(GeneralException e) {
            if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[consultarPosiblesNominasDuplicadas]["+numeroTransferencia+"][BCI_FINEX][Exception] error con mensaje=<" + e.getMessage()+">", e);
            }
            throw e;
        }
    }
    
    
    /**
     * M�todo que realiza una transferencia de fondos a partir de una detalle de n�mina.
     * Este m�todo se basa en {@link #realizaTransferenciaDeFondos(TransferenciaATercerosTO, Cliente)}
     * Inicialmente se habilita para las transferencias entre cuentas propias.
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 05/07/2016 Jaime Gaete L. (SEnTRA) - Daniel Araya (Ing. Soft. BCI):: versi�n incial.</li>
     * <li>1.1 15/11/2016 Jaime Gaete L. (SEnTRA) - Daniel Araya (Ing. Soft. BCI): Se agrega carga de correo de destinatario
     * para el env�o de correo.</li>
     * </ul>
     * @param detalleNomina Detalle de n�mina para la cual se realizar� una transferencia de fondos.
     * @param cliente Objeto con los datos del cliente.
     * @return boolean con la respuesta de la transferencia de fondos.
     * @throws GeneralException en caso de error.
     * @since 3.16
     */
    public boolean realizarTransferenciaDeFondosDetalleNomina(InfoDetalleNomina detalleNomina, Cliente cliente) throws GeneralException {
        
            boolean resultadoTransf = false;
            String numTransferencia = detalleNomina!=null?detalleNomina.getNumTransferencia():"";
            try {
                if (getLogger().isEnabledFor(Level.INFO)){
                    getLogger().info("[realizarTransferenciaDeFondosDetalleNomina]["+numTransferencia+"][BCI_INI] Inicio cliente:["+cliente+"], detalleNomina ["+detalleNomina+"]");
                }
                if(detalleNomina!= null){
                    String estadoTransaccion = TablaValores.getValor(ARCH_PARAM_TRF_TER, "codigosEstadosTransferencia", "noRealizada");
                    String codigoEstado = TablaValores.getValor(ARCH_PARAM_TRF_TER, "codigoError", "errorComunicacion");
                    
                    String ctaOrigen = StringUtil.sacaCeros(detalleNomina.getCuentaOrigen());
                    ctaOrigen = StringUtil.rellenaConCeros(ctaOrigen, LARGO_CUENTA_OCHO);

                    String cuentaAbono = StringUtil.sacaCeros(detalleNomina.getCtaDestino());
                    cuentaAbono = StringUtil.rellenaConCeros(cuentaAbono, LARGO_CUENTA);
                    String tipoTransferencia = detalleNomina.getTipoTransferencia();

                    if (getLogger().isDebugEnabled()){
                        getLogger().debug("[realizarTransferenciaDeFondosDetalleNomina]["+numTransferencia+"] estadoTransaccion:["+estadoTransaccion+"], codigoEstado ["
                                +codigoEstado+"], ctaOrigen:["+ctaOrigen+"], cuentaAbono:["+cuentaAbono+"], tipoTransferencia:["+tipoTransferencia+"], bcoDestino:["+detalleNomina.getBcoDestino()+"]");
                    }
                    if (tipoTransferencia.equals(TablaValores.getValor(ARCH_PARAM_TRF_TER, "tipoTransferenciaTTFF", "CTAPROPIAS"))
                            && detalleNomina.getBcoDestino().equals(TablaValores.getValor(ARCH_PARAM_TRF_TER, "codBancoBCITTFF", "Desc"))) {
                        if (getLogger().isDebugEnabled()){
                            getLogger().debug("[realizarTransferenciaDeFondosDetalleNomina]["+numTransferencia+"] Transferencia es a cuenta propia.");
                        }
                        BCIExpress bciExpress = crearEJBBCIExpress();
                        try {
                            String servicioFirmaCuentaPropia = TablaValores.getValor(ARCH_PARAM_EMP, "FirmaCuentasPropias", "Desc");
                            if (getLogger().isDebugEnabled()){
                                getLogger().debug("[realizarTransferenciaDeFondosDetalleNomina]["+numTransferencia+"] servicioFirmaCuentaPropia["+servicioFirmaCuentaPropia+"]");
                            }
                            ResultTransferenciaFondoCuentaCorriente respuesta = (ResultTransferenciaFondoCuentaCorriente) bciExpress
                                    .transferenciaFondoEntreBciModoManual(ctaOrigen, String.valueOf(detalleNomina.getMontoDetalle()), cuentaAbono, servicioFirmaCuentaPropia);

                            TransfFondoCtaCte[] tranFonCtaCte = respuesta.getTransfFondoCtaCte();

                            if (getLogger().isDebugEnabled()){
                                getLogger().debug("[realizarTransferenciaDeFondosDetalleNomina]["+numTransferencia+"] TFF a Cta Cte: " + StringUtil.contenidoDe(tranFonCtaCte));
                            }
                            if (tranFonCtaCte != null) {
                                if (tranFonCtaCte[0] == null) {
                                    if (getLogger().isDebugEnabled()) {
                                        getLogger().debug("[realizarTransferenciaDeFondosDetalleNomina]["+numTransferencia+"] Error en respuesta de transferencia");
                                    }
                                    throw new GeneralException("PAGOS027");
                                }
                            }
                            else {
                                if (getLogger().isDebugEnabled()) {
                                    getLogger().debug("[realizarTransferenciaDeFondosDetalleNomina]["+numTransferencia+"] Error en transferencia");
                                }
                                throw new GeneralException("PAGOS027");
                            }
                            resultadoTransf = true;
                            codigoEstado = TablaValores.getValor(ARCH_PARAM_TRF_TER, "codRechazoTTFF", "Desc");
                            estadoTransaccion = TablaValores.getValor(ARCH_PARAM_TRF_TER, "codigosEstadosTransferencia", "realizada");

                        }
                        catch (Exception e) {
                            if (getLogger().isEnabledFor(Level.WARN)) {
                                getLogger().warn("[realizarTransferenciaDeFondosDetalleNomina]["+numTransferencia+"][Exception] al transferir=<"+ e.getMessage()+">", e);
                            }
                            if (e instanceof BCIExpressException) {
                                BCIExpressException bex = (BCIExpressException) e;
                                codigoEstado = bex.getCodigo();

                                if (getLogger().isDebugEnabled()) {
                                    getLogger().debug("[realizarTransferenciaDeFondosDetalleNomina]["+numTransferencia+"] Error BCIExpressException, codigoEstado ["+ codigoEstado +"]");
                                }
                            }
                            resultadoTransf = false;
                            estadoTransaccion = TablaValores.getValor(ARCH_PARAM_TRF_TER, "codigosEstadosTransferencia", "noRealizada");
                        }

                        if (getLogger().isDebugEnabled()){
                            getLogger().debug("[realizarTransferenciaDeFondosDetalleNomina]["+numTransferencia+"] codigoEstado: ["+codigoEstado+"], estadoTransaccion ["+estadoTransaccion+"]");
                        }

                        cambiaEstadoTransferenciaPago(numTransferencia, (int)detalleNomina.getIndicador(), estadoTransaccion, codigoEstado, "");
                        if (getLogger().isDebugEnabled()){
                            getLogger().debug("[realizarTransferenciaDeFondosDetalleNomina]["+numTransferencia+"] resultadoTransf: ["+resultadoTransf+"]");
                        }
                        if (!resultadoTransf) {
                            throw new GeneralException("PAGOS027");
                        }
                    }
                    else {
                        throw new Exception("Tipo de transferencia no implementado");
                    }

                    if (getLogger().isDebugEnabled()){
                        getLogger().debug("[realizarTransferenciaDeFondosDetalleNomina]["+numTransferencia+"] resultadoTransf para enviar correo: ["+resultadoTransf+"]");
                    }
                    if (resultadoTransf) {
                        try {
                            String mailDestinatario = detalleNomina.getCorreoDestinatario();
                            if (getLogger().isDebugEnabled()){
                                getLogger().debug("[realizarTransferenciaDeFondosDetalleNomina]["+numTransferencia+ "] correo destinatario:"+mailDestinatario);
                            }
                            if (mailDestinatario != null && !mailDestinatario.trim().equals("")) {
                                TransferenciaATercerosTO transferencia = new TransferenciaATercerosTO();
                                transferencia.setIdentificador(numTransferencia);
                                transferencia.setNombreCliente(detalleNomina.getNombreEmpresa());
                                transferencia.setCanal(detalleNomina.getCodigoCanal());
                                transferencia.setNombreDestin(detalleNomina.getNombreDestinatario());
                                transferencia.setDescripcionBanco(detalleNomina.getDescripcionBancoDestino());
                                transferencia.setCtaDestino(detalleNomina.getCtaDestino());
                                transferencia.setFechaPago(detalleNomina.getFechaPago());
                                transferencia.setMontoTransfer((long)detalleNomina.getMontoDetalle());
                                transferencia.setMensajeDestinatario(detalleNomina.getMensajeCorreoBenef());
                                transferencia.setEmailDestinatario(detalleNomina.getCorreoDestinatario());
                                if (getLogger().isDebugEnabled()){
                                    getLogger().debug("[realizarTransferenciaDeFondosDetalleNomina]["+numTransferencia+ "] transferencia:"+transferencia.toString());
                                }
                                enviarCorreoTransferencia(transferencia);
                            }
                        }
                        catch (Exception e) {
                            if (getLogger().isEnabledFor(Level.WARN)) {
                                getLogger().warn("[realizarTransferenciaDeFondosDetalleNomina]["+numTransferencia+"][Exception] al enviar correo=<"+ e.getMessage()+">", e);
                            }
                        }
                    }
                }
            }
            catch (Exception e) {
                if (getLogger().isEnabledFor(Level.ERROR)) {
                    getLogger().error("[realizarTransferenciaDeFondosDetalleNomina]["+numTransferencia+"][BCI_FINEX][Exception] al procesar transferencia=<"+ e.getMessage()+">", e);
                }
                throw (e instanceof GeneralException) ? (GeneralException) e : new GeneralException("PAGOS027");
            }
            if (getLogger().isEnabledFor(Level.INFO)){
                getLogger().info("[realizarTransferenciaDeFondosDetalleNomina]["+numTransferencia+"][BCI_FINOK] Fin resultado transferencia ["+resultadoTransf+"]");
            }
            return resultadoTransf;
        }
    
    /**
     * M�todo que permite consultar operaciones de compra y venta de divisas con transferencia. 
     * <p>
     * <b>Registro de versiones:</b>
     * <ul>
     * <li>1.0 05/04/2017 Mauricio Retamal C. (SEnTRA) - Daniel Araya (Ing. Soft. BCI): Versi�n Inicial.
     * </ul>
     * <p>
     * 
     * @param filtroBusqueda objeto que contiene los filtros para busqueda.
     * @return arreglo con datos obtenidos de consulta.
     * @throws Exception en caso de error.
     * @since 3.18
     */
    public InformacionCargaMasiva[]  consultarOperacionesCVD(FiltroConsultaTO filtroBusqueda) throws Exception{
        InformacionCargaMasiva[] respuesta;
        if (getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[consultarOperacionesCVD][BCI_INI] filtroBusqueda["+filtroBusqueda+"]");
        }
        try{
            NominaDeTransferenciasDAO dao = new NominaDeTransferenciasDAO();
            respuesta = dao.consultarOperacionesCVD(filtroBusqueda);
            if (getLogger().isEnabledFor(Level.INFO)){
                getLogger().info("[consultarOperacionesCVD][BCI_FINOK] resultado de la operaci�n["+respuesta+"]");
            }
            return respuesta;
        }
        catch(Exception e){
            if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[consultarOperacionesCVD][BCI_FINEX][Exception] al consultar las operaciones=<"+ e.getMessage()+">", e);
            }
            throw new Exception(ErroresUtil.extraeStackTrace(e));
        }
    }
    
    /**
     * <p>M�todo que cambia el estado de un detalle de una Transferencia en la tabla de procesos</p>
     * <p>
     * <b>Registro de versiones:</b>
     * <ul>
     * <li>1.0 05/04/2017 Mauricio Retamal C. (SEnTRA) - Daniel Araya (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <p>
     *
     * @param numTransferencia n�mero de la transferencia
     * @throws Exception en caso de error.
     * @return Respuesta de la modificaci�n.
     * @since 3.18
     */
    public boolean cambiaEstadoProceso(String numTransferencia) throws Exception{
        try{
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[cambiaEstadoProceso][BCI_INI] Inicio metodo. numTransferencia[" + numTransferencia + "]" );
                
            }
            NominaDeTransferenciasDAO dao = new NominaDeTransferenciasDAO();
            boolean resultado = dao.cambiaEstadoProceso(numTransferencia);
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[cambiaEstadoProceso][" + numTransferencia + "][BCI_FINOK] Fin metodo. resultado: "+resultado);
            }
            return resultado;
        }
        catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) { 
                getLogger().error("[cambiaEstadoProceso][" + numTransferencia + "][BCI_FINEX]["+e.getClass()+"] Error: " + e.getMessage(), e);
            }
            throw e;
        }
    }
    
    /**
     * <p>M�todo que elimina todas las transferencias segun su identificador</p>
     * <p>
     * <b>Registro de versiones:</b>
     * <ul>
     * <li>1.0 22/10/2018 Bastian Nelson G. (Sermaluc) - Gonzalo Mu�oz (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <p>
     *
     * @param numeroDeTransferencia n�mero de la transferencia
     * @throws Exception en caso de error.
     * @throws RemoteException en caso de error.
     * @since 3.19
     */
    public void eliminarNumeroDeTransferencia(String numeroDeTransferencia) throws Exception,RemoteException{
        try{
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[cambiaEstadoProceso][BCI_INI] Inicio metodo. numTransferencia[" + numeroDeTransferencia + "]" );
                
            }
            NominaDeTransferenciasDAO dao = new NominaDeTransferenciasDAO();
             dao.eliminarNumeroDeTransferencia(numeroDeTransferencia);
            if (getLogger().isEnabledFor(Level.INFO)) { 
                getLogger().info("[cambiaEstadoProceso][" + numeroDeTransferencia + "][BCI_FINOK] Fin metodo)");
            }
        
        }
        catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) { 
                getLogger().error("[cambiaEstadoProceso][" + numeroDeTransferencia + "][BCI_FINEX]["+e.getClass()+"] Error: " + e.getMessage(), e);
            }
            throw e;
        }
    }
    
  
    
}
