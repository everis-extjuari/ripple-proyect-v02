package wcorp.aplicaciones.productos.servicios.transferencias.nominas.dao;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import wcorp.aplicaciones.bancaempresas.diccionariodecuentas.vo.CuentasInscritasVO;
import wcorp.aplicaciones.productos.servicios.pagos.to.FiltroTO;
import wcorp.aplicaciones.productos.servicios.transferencias.nominas.to.InstruccionTransferenciaTO;
import wcorp.aplicaciones.productos.servicios.transferencias.nominas.vo.DetalleTransferenciaVO;
import wcorp.aplicaciones.productos.servicios.transferencias.nominas.vo.EncabezadoTransferenciaVO;
import wcorp.aplicaciones.productos.servicios.transferencias.nominas.vo.FiltroConsultarTransferenciasVO;
import wcorp.aplicaciones.productos.servicios.transferencias.nominas.vo.InformacionCodigoActivacionVO;
import wcorp.aplicaciones.productos.servicios.transferencias.nominas.vo.TotalPorCuentaVO;
import wcorp.aplicaciones.productos.servicios.transferencias.nominas.vo.TransferenciasEnLineaVO;
import wcorp.aplicaciones.productos.servicios.transferencias.nominas.vo.TransferenciasVO;
import wcorp.aplicaciones.productos.servicios.transferencias.nominas.vo.UsuarioTransferenciaVO;
import wcorp.aplicaciones.productos.servicios.transferencias.to.TransferenciaATercerosTO;
import wcorp.bprocess.bancaempresas.transferenciaslbtr.cargamasiva.vo.FiltroConsultaTO;
import wcorp.bprocess.bancaempresas.transferenciaslbtr.cargamasiva.vo.InfoDetalleNomina;
import wcorp.bprocess.bancaempresas.transferenciaslbtr.cargamasiva.vo.InformacionCargaMasiva;
import wcorp.bprocess.bancaempresas.transferenciaslbtr.cargamasiva.vo.TotalTO;
import wcorp.bprocess.cuentas.vo.TransferenciasRecibidasVO;
import wcorp.serv.bciexpress.ListaDeteTransf;
import wcorp.util.ErroresUtil;
import wcorp.util.FechasUtil;
import wcorp.util.GeneralException;
import wcorp.util.NumerosUtil;
import wcorp.util.RUTUtil;
import wcorp.util.StringUtil;
import wcorp.util.TablaValores;
import wcorp.util.excepciones.ExcepcionGeneral;

import cl.bci.middleware.aplicacion.persistencia.ConectorServicioDePersistenciaDeDatos;
import cl.bci.middleware.aplicacion.persistencia.ejb.ConfiguracionException;
import cl.bci.middleware.aplicacion.persistencia.ejb.EjecutarException;
import cl.bci.middleware.aplicacion.persistencia.ejb.ServicioDatosException;


/**
 * <b>NominaDeTransferenciasDAO</b>
 * <p>
 * DAO de la aplicaci�n Transferencias de Fondos utilizado para el acceso a la capa de
 * datos mediante el uso de Persistencia de Datos.
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 25/04/2009 Alejandro Barra (SEnTRA) : versi�n inicial.
 * <li>1.1 05/01/2010 Marcelo Fuentes H. (SEnTRA) : Se eliminan referencias a tabla bancos.codigos
 *                     ya que los nombres de los bancos se obtienen de una consulta a la CCA.</li>
 * <li>1.2 15/01/2010 Marcelo Fuentes H. (SEnTRA) : Se agrega validaci�n al obtener el c�digo de Banco para evitar
 *                     errores cuando dicho c�digo se obtenga con un largo menor al necesitado.</li>
 * <li>1.3 01/02/2010 Marcelo Fuentes H. (SEnTRA) : Se modifica m�todo {@link #extraeDetalleTransferencia(String, String, String, int)}
 *                     para obtener glosas de motivos de error de transferencias que no se pudieron realizar.</li>
 * <li>1.4 28/05/2010 Denisse Vel�squez S. (SEnTRA) : Se modifica m�todo {@link #extraeDetalleTransferencia(String, String, String, int)}</li>
 * <li>1.5 14/07/2010 Ximena Medel P. (SEnTRA) : Se organiza import por migraci�n de weblogic 8.1 a weblogic 10.3</li>
 * <li>1.6 05/08/2010 Marcelo Fuentes H. (SEnTRA) : Se agregan los m�todos {@link #registraEvento()} y {@link #cambiaGlosaTransferencia()}.
 *                    Adem�s se modifican los siguientes m�todos:<ul>
 *                    <li>{@link #agregarTransferencia()}</li>
 *                    <li>{@link #eliminarTransferencia()}</li>
 *                    <li>{@link #crearGrupo()}</li>
 *                    <li>{@link #eliminarGrupoDeLaTransferencia()}</li>
 *                    <li>{@link #modificaGrupoDeLaTransferencia()}</li>
 *                    <li>{@link #cambiaEstadoTransferencia()}</li>
 *                    <li>{@link #obtieneGruposFrecuentes()}</li>
 *                    <li>{@link #modificaDetalleTransferencia()}</li>
 *                    <li>{@link #extraeTransferenciasPendientesPorArchivo()}</li>
 *                    <li>{@link #extraeDetallesTransferenciasPendientesConReparos()}</li>
 *                    <li>{@link #extraeTransferenciasRecibidas()}</li>
 *                    <li>{@link #extraeDetalleTransferencia()}</li></ul>
 *                    Tambi�n se elimina la variable "BD_EXT" la cual ya no es utilizada.</li>
 * <li>1.7 07/12/2010 Marcelo Fuentes H. (SEnTRA): Se modifica el m�todo {@link #obtieneGruposFrecuentes()}.</li>
 * <li>1.8 27/01/2011 Marcelo Fuentes H. (SEnTRA): Se modifica el m�todo {@link #extraeTransferenciasRecibidas()},
 *                      {@link #extraeDetallesTransferenciasPendientesConReparos()} y {@link #extraeDetallesTransferenciaPendiente()}.</li>
 * <li>1.9 07/02/2011 Marcelo Fuentes H. (SEnTRA): Se modifica el m�todo {@link #extraeDetalleTransferencia()}.</li>
 * <li>1.10 21/02/2011 Marcelo Fuentes H. (SEnTRA): Se agrega el m�todo {@link #extraePagosRealizadosViaCargaMasiva()}.</li>
 * <li>1.11 28/06/2011 Denisse Vel�squez S. (SEnTRA): Se agrega el m�todo {@link #extraeTransfRecibidasEmpBCIEmpBCI()}.</li>
 *
 * <li>2.0 02/08/2011 Pedro Carmona Escobar (SEnTRA) : Se agregan los m�todos:<ul>
 *                      <li>{@link #extraeTransferenciasPendientesPorNumero()}</li>
 *                      <li>{@link #extraeCodigodDeActivacion()}</li>
 *                      <li>{@link #extraeCuentasNoInscritas()}</li>
 *                      <li>{@link #validarCodigoActivacion()}</li>
 *                      <li>{@link #obtenerCodigoActivacion()}</li></ul>
 *                      Adem�s, se modifica el m�todo {@link #extraeTransferenciasPendientes()}. Por �ltimo, se agregan
 *                      las constante HAY_CUENTAS_NO_INSCRITA, BD_BANELE y TTFF_PENDIENTES_NUMERO,
 *                      y se retiran l�neas comentadas de toda la clase.
 * <li>2.1 23/08/2011 Freddy Monsalve - Freddy Painenao (Os.One): Se agrega el m�todo {@link #extraeTransferenciasPendientesPorResolver()}.</li>
 * <li>2.3 23/11/2011 Denisse Vel�squez S. (SEnTRA): Se modifican los m�todos
 *                    {@link #extraeTransferenciasRealizadas()}, {@link #extraeDetalleTransferencia()}.</li>
 *
 * <li>2.4 16/01/2012 Iv�n L�pez B. (ADA Ltda.): Se agregan m�todos extraeTransferenciasPendientesATerceros,
 *                                               extraeTransferenciasPendientesEPC y obtenerDetalleTransferencia.</li>
 * <li> 2.5 08/03/2013, Sergio Cuevas D. (SEnTRA): Se agregan los m�todos:<ul>
 *                      <li>{@link #eliminaDetalleCuentasNoInscritas()}</li></ul>
 * <li> 2.5 25/03/2013, Sergio Cuevas D. (SEnTRA): Se modifican los m�todos:
 *                          {@link #extraeTransferenciasRealizadas()} {@link #extraeDetalleTransferencia()}
 *                          en donde se agrego el seteo del campo "vuelveIntentar" para saber si se 
 *                          coloca en el excels si debe intentar de nuevo</li>
 * <li>2.6 22/04/2013 Rodrigo Arce Campos (SEnTRA) : Se agregan los m�todos
 *          <li>{@link #obtenerInstruccionesTransferenciaSupervisor()}</li>
 *          <li>{@link #enviarInstruccionSupervisor(InstruccionTransferenciaTO)}</li>
 *          <li>{@link #eliminarInstruccionTransferenciaSupervisor(long, int, String)}</li>
 *          <li>{@link #insertarAccionBitacoraInstruccionesTransferencia(InstruccionTransferenciaTO)}</li>
 *          <li>{@link #extraeTransferenciasPendientesCTALBT(FiltroConsultarTransferenciasVO)}</li>
 * <li> 2.8 25/04/2013, Sergio Cuevas D. (SEnTRA): se mofica  {@link #cambiaEstadoTransferencia()}   
 *                          se agrega atributo inicioCierreCCA 
 * <li>2.7 04/02/2013 Venancio Ar�nguiz P. (SEnTRA): Se agrega el m�todo 
 *                    {@link #consultarExisteTransferencia()}.</li>                    
 * <li>2.10 15/07/2013, Sergio Cuevas D. (SEnTRA): se mofica  {@link #extraePagosRealizadosViaCargaMasiva()}   
 *                          se agrega atributo estado para realizar la busqueda
 * <li> 2.11 30/01/2014, Marcelo Fuentes. (SEnTRA): se mofica el metodo {@link #eliminarTransferencia()}   
 * <li>2.12 02/04/2014 Venancio Ar�nguiz P. (SEnTRA): Se mejora el logueo del m�todo 
 *                    {@link #consultarExisteTransferencia()}.</li>
 * <li>3.0 22/01/2014 Venancio Ar�nguiz P. (SEnTRA): Se agrega el m�todo {@link #obtenerTiempoEsperaNominaTTFF()}, 
 *         que obtiene los tiempos de espera y posici�n en cola de procesamiento de las n�minas que se encuentran
 *         con estado firmada.</li>
 * <li>3.1 27/10/2014 H�ctor Hern�ndez Orrego. (SEnTRA): Se agrega el m�todo 
 *         {@link #obtieneNominasEnLineaPendientesDeFirma()}, que obtiene las n�minas pendientes de 
 *         firma para un convenio empresa, en un rango de fecha.<br>
 *         {@link #obtenerEncabezadoDeNomina(InformacionCargaMasiva))}<br>
 *         {@link #actualizarEstadoNominaEnProceso(InformacionCargaMasiva))}<br>
 *         {@link #obtenerPagosNomina(InformacionCargaMasiva))}<br>
 *         {@link #obtenerTotalesNomina(InformacionCargaMasiva))}<br>
 *         {@link #obtenerEncabezadoDeNominaEnProceso(InformacionCargaMasiva))}<br>
 *         
 *         </li>
 * <li> 3.2 13/11/2014 Angel Crisostomo (ImageMaker): Se agregan los m�todos      
 *                      {@link #obtenerEncabezadoPagosRealizados(FiltroConsultarTransferenciasVO, String, String)}
 *                      <br>
 *                      {@link #obtenerDetallePagosRealizados(FiltroConsultarTransferenciasVO, String, String)
 * <li> 3.3 30/10/2014 Sergio Cuevas Diaz (SEnTRA): Se agregan los m�todos
 *                      {@link #obtenerResumenNominas(FiltroConsultaTO)}<br>
 *                      {@link #consultarNominasConProblemasPorFiltro(FiltroConsultaTO)}<br>
 *                      {@link #consultarNominasEnProcesoPorFiltro(FiltroConsultaTO)}<br>
 * <li>
 * <li> 3.4 27/11/2014 Angel Crisostomo (ImageMaker): Se agrega el m�todo
 *                      {@link #insertarEncabezadoTransferencia(TransferenciaATercerosTO)}<br>
 *                      {@link #eliminarOperacionTransferencia(String)}<br>
 *                      {@link #insertarTotalesTransferencia(String)}<br>
 * <li> 3.5 09/12/2014 Sergio Cuevas Diaz (SEnTRA): Se agrega m�todo
 *                      {@link #obtieneTransferenciasDePagoRecibidas(FiltroTO)}<br>
 * <li>3.6 26/01/2015 Sergio Cuevas D�az (Sentra): Se encapsulan parametros de la firma del m�todo
 *                      {@link #obtieneNominasEnLineaPendientesDeFirma(FiltroConsultaTO)}.</li>
 * <li>3.7 26/01/2015 Pablo Romero C�ceres (Sentra): Se modifica el m�todo
 *                      {@link #obtieneTransferenciasDePago(FiltroTO)}.</li>
 * <li>3.8 16/02/2015 Gonzalo Cofre Guzman (Sentra): Se modifica el m�todo
 *                      {@link #obtenerEncabezadoDeNominaEnProceso(InformacionCargaMasiva)} se agrega el canal 
 *                      al objeto de salida</li> 
 * <li> 3.4 29/12/2014 Rafael Pizarro (TINet): Se normaliza log para ajustarse a Normativa Log4j de BCI, para esto
 *              se modifican los siguientes m�todos:
 *              {@link #obtenerEncabezadoDeNomina(InformacionCargaMasiva)},
 *              {@link #obtenerPagosNomina(InformacionCargaMasiva)},
 *              {@link #obtenerTotalesNomina(InformacionCargaMasiva)},
 *              {@link #obtenerEncabezadoDeNominaEnProceso(InformacionCargaMasiva)},
 *              {@link #obtenerEncabezadoPagosRealizados(FiltroConsultarTransferenciasVO, String, String)},
 *              {@link #obtenerDetallePagosRealizados(FiltroConsultarTransferenciasVO, String, String)     
 * </li>
 * <li> 3.4 26/01/2015 Marcelo Fuentes (SEnTRA): Se modifica el m�todo 
 *                      {@link #extraeDetalleTransferencia(String, String, String, int)} </li>
 * <li> 3.5 26/05/2015 Pablo Romero C�ceres (SEnTRA): Se modifica el m�todo 
 *                      {@link #obtieneTransferenciasDePago(FiltroTO)} </li>
 * <li> 4.0 01/04/2015 Christian Saravia J. (SEnTRA) - Lucy Velasquez Cea (IS BCI): Se normaliza logueo en toda la clase.</li>
 * <li> 4.1 28/12/2015 Jaime Gaete (SEnTRA) - Jorge Lara D�az (ing. Soft. BCI): Se agrega m�todo 
 * 		{@link #caducarNominaDeTransferencia(FiltroTO)} </li>
 * <li> 4.2 12/04/2016 Jaime Gaete L. (SEnTRA) - Jorge Lara D�az (Ing. Soft. BCI): Se agrega m�todo 
 * {@link #consultarPosiblesTransferenciasDuplicadas(TransferenciaATercerosTO, FiltroTO, String, String)}. Se corrige documentaci�n de atributos
 * de la clase y su orden, para correcci�n de Checkstyle BCI.</li>
 * <li> 4.3 09/12/2015 Darlyn Delgado Perez (SEnTRA)  - Maria Jose Romero (ing Soft. BCI) :se agregaran par�metros de salida.
 *          {@link #obtenerEncabezadoDeNominaEnProceso(InformacionCargaMasiva)} </li>
 * <li> 4.4 02/06/2016 Gonzalo Cofre G. (SEnTRA) - Daniel Araya (Ing. Soft. BCI): Se modifica el m�todo {@link #obtieneTransferenciasDePagoRecibidas(FiltroTO)}.</li>
 * <li> 4.5 17/05/2016 Jaime Gaete L. (SEnTRA) - Paola Avalos(Ing. Soft. BCI): Se agrega m�todo 
 * {@link #consultarPosiblesNominasDuplicadas(InformacionCargaMasiva, FiltroTO, String, String)} y atributo {@link #CODIGO_TOTALIZACION_NOMINA}.</li>
 * <li> 4.6 19/05/2017 Carlep Lucena (Everis) - Kay Vera (Ing De Soft BCI ): Se incorpora glosa del codigo de rechazo al metodo {@link #obtenerPagosNomina(nomina)}.</li>
 * <li> 4.7 06/09/2017 Rodrigo Seguel (Everis) - Maribel Saavedra (Ing De Soft BCI ): Se agrega validaci�n para rescatar las glosas de motivos de rechazo 
 *      en transferencias en el mismo banco BCI, al metodo {@link #obtenerPagosNomina(nomina)}.</li>
 * <li> 4.7 05/04/2017 Mauricio Retamal C. (SEnTRA) - Daniel Araya (Ing. Soft. BCI): Se agrega el m�todo {@link #consultarOperacionesCVD(FiltroConsultaTO)} 
 *                                                      y {@link #cambiaEstadoProceso(String)}.</li>
 * <li> 4.8 26/10/2018 Bastian Nelson G. (Sermaluc) - Bastian Nelson (Ing. Soft. BCI): Se agrega el m�todo {@link #eliminarNumeroDeTransferencia}.</li>
 *                                                      
 *                                                      
 * </ul>
 * <p>
 * <b>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</b>
 * <p>
 */
public class NominaDeTransferenciasDAO {

    /**
     * Archivo de Par�metros para Transferencia de Fondos.
     */
    private static final String TABLA_TRANSFERENCIAS = "TransferenciaDeFondosEmpresa.parametros";

    /**
     * Archivo de Par�metros para Transferencia de Fondos a Terceros.
     */
    private static final String TABLA_TTFF_TERCEROS = "TransferenciaTerceros.parametros";

    /**
     * Archivo de Par�metros para Informaci�n de Productos.
     */
    private static final String ARCHIVO_PARAMETROS = "informacionDeProductos.parametros";

    /**
     * Archivo de Errores para Transferencia de Fondos.
     */
    private static final String TABLA_ERROR_TRF = "erroresTrf.parametros";


    /**
     * Archivo de Par�metros para pago en linea.
     */
    private static final String TABLA_ESTADO_OPF = "bcipay/estadosOPF.parametros";

    /**
     * Archivo de Par�metros de Errores.
     */
    private static final String ARCH_PARAM_ERR     = "errores.codigos";

    /**
     * Error Timeout al realizar transferencia.
     */
    private static final String ERROR_TIMEOUT     = "W200";

    /**
     * Bancos Origen.
     */
    private static final String BANCOS_DE_ORIGEN = "BCI/TBANC/NOVA";

    /**
     * C�digo que representa la existencia de cuentas no inscritas dentro de una transferencia cargada mediante archivo.
     */
    private static final String HAY_CUENTAS_NO_INSCRITA     = "1";

    /**
     * C�digo que representa la base de datos Sybase Banele.
     */
    private static final String BD_BANELE     = "banele";

    /**
     * C�digo que representa el procedimiento almacenado que botiene las Transferencias de Fondos filtradas por el n�mero de transferencia.
     */
    private static final String TTFF_PENDIENTES_NUMERO     = "extraeTransferenciasPendientesPorNumero";

    /**
     * Largo de un rut.
     */
    private static final int LARGO_RUT     = 8;

    /**
     * Largo del convenio.
     */
    private static final int LARGO_CONVENIO = 10;

    /**
     * C�digo que identifica registro de totalizaci�n de una n�mina.
     */
    private static final String CODIGO_TOTALIZACION_NOMINA = "TOP";

    /**
     * Logueo de la Clase.
     */
    private transient Logger log = Logger.getLogger(NominaDeTransferenciasDAO.class);

    /**
     * <p>M�todo que permite agregar una Transferencia para su posterior aprobaci�n y firma</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 19/10/2009 Alejandro Barra (SEnTRA): Versi�n inicial.
     * <li>1.1 05/01/2010 Marcelo Fuentes (SEnTRA): Se elimina la obtenci�n del nombre de banco desde la tabla
     *                    bancos.codigos ya que los nombres de los bancos se obtienen de una consulta a la CCA.</li>
     * <li>1.2 09/07/2010 Marcelo Fuentes (SEnTRA): Se agrega par�metro que permite ejecutar operaciones en "CHAINED_MODE" para Sybase.</li>
     *
     * </ul>
     *
     * @param rutEmpresa rut de la empresa
     * @param dvEmpresa d�gito verificador de la empresa
     * @param convenio n�mero de convenio
     * @param rutUsu rut del usuario que realiza la operaci�n
     * @param dvUsu digito verificador del usuario que realiza la operaci�n
     * @param transferencia con los datos de la transferencia a ingresar
     * @throws Exception
     * @return Listado de transferencias en linea
     * @since 1.0
     */
    public TransferenciasEnLineaVO[] agregarTransferencia(String rutEmpresa, String dvEmpresa, String convenio, String rutUsu, String dvUsu, TransferenciasEnLineaVO transferencia) throws Exception{

        try{
            if (getLogger().isEnabledFor(Level.INFO)) {
            	getLogger().info("[agregarTransferencia][" + rutEmpresa + "][" + convenio + "] [BCI_INI]"); 
                getLogger().info("[agregarTransferencia][" + rutEmpresa + "][" + convenio + "] rutUsu:[" + rutUsu + "] - [" + dvUsu + "], datos Transferencia:[" + transferencia.toString() + "]");
            }

            List documento = null;
            HashMap salida = null;
            TransferenciasEnLineaVO[] transferenciasObtenidas = null;
            String contexto = TablaValores.getValor("JNDIConfig.parametros","cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
            HashMap parametrosAgregarTTFF = new HashMap();
            parametrosAgregarTTFF.put("CHAINED_SUPPORT","true");
            parametrosAgregarTTFF.put("rutUsuario",      rutUsu);
            parametrosAgregarTTFF.put("dvUsuario",    dvUsu);
            parametrosAgregarTTFF.put("rutEmpresa",    rutEmpresa);
            parametrosAgregarTTFF.put("dvEmpresa", dvEmpresa);
            parametrosAgregarTTFF.put("convenio",  convenio);
            parametrosAgregarTTFF.put("numeroTransferencia",   transferencia.getNumeroDeTransferencia());
            parametrosAgregarTTFF.put("nombreTransferencia",     transferencia.getNombreDeLaTransferencia());
            parametrosAgregarTTFF.put("monto", new Double(transferencia.getMonto()));
            parametrosAgregarTTFF.put("cuentaOrigen",  transferencia.getCuentaOrigen());
            parametrosAgregarTTFF.put("tipoTransferencia",   transferencia.getTipoDeTransferencia());
            parametrosAgregarTTFF.put("tipoPago",   transferencia.getTipoDePago());
            parametrosAgregarTTFF.put("cuentaDestino",  transferencia.getCuentaDestino());
            parametrosAgregarTTFF.put("bancoDestino",  transferencia.getCodigoBancoDestino());
            parametrosAgregarTTFF.put("nombreCuenta",  transferencia.getNombreCuenta());
            parametrosAgregarTTFF.put("numeroFactura",  transferencia.getNumeroFactura());
            parametrosAgregarTTFF.put("numeroOrden",  transferencia.getNumeroDeOrden());
            parametrosAgregarTTFF.put("rutDestino",  String.valueOf(transferencia.getRutBeneficiario()));
            parametrosAgregarTTFF.put("dvDestino",  String.valueOf(transferencia.getDvBeneficiario()));
            parametrosAgregarTTFF.put("nombreDestino",  transferencia.getNombreDelBeneficiario());
            parametrosAgregarTTFF.put("emailDestino",  transferencia.getEmailDelBeneficiario());
            parametrosAgregarTTFF.put("mensajeDestino",  transferencia.getMensajeParaElBeneficiario());
            documento = conector.consultar("banele", "agregarTransferencia",parametrosAgregarTTFF);
            if (documento != null){
                if(documento.size() > 0){
                    transferenciasObtenidas = new TransferenciasEnLineaVO[documento.size()];
                    if (getLogger().isEnabledFor(Level.INFO)) {getLogger().info("[agregarTransferencia][" + rutEmpresa + "][" + convenio + "] cuentasObtenidas [" + documento.size() + "]");}
                    for(int i = 0 ; i < documento.size() ; i++){
                        salida = (HashMap) documento.get(i);
                        transferenciasObtenidas[i] = new TransferenciasEnLineaVO();
                        transferenciasObtenidas[i].setNumeroDeTransferencia((((String)salida.get("numeroTransferencia")).trim()));
                        transferenciasObtenidas[i].setCuentaOrigen(((String)salida.get("cuentaOrigen")).trim());
                        transferenciasObtenidas[i].setNombreCuenta(((String)salida.get("nombreCuenta")).trim());
                        transferenciasObtenidas[i].setCuentaDestino(((String)salida.get("cuentaDestino")).trim());
                        transferenciasObtenidas[i].setCodigoBancoDestino(((String)salida.get("bancoDestino")).trim());
                        transferenciasObtenidas[i].setRutBeneficiario(Long.parseLong(((String)salida.get("rutBeneficiario")).trim()));
                        transferenciasObtenidas[i].setDvBeneficiario(((String)salida.get("dvBeneficiario")).trim().charAt(0));
                        transferenciasObtenidas[i].setMonto((((Double)salida.get("monto")).doubleValue()));
                        transferenciasObtenidas[i].setIdDetalle((((Integer)salida.get("idDetalle")).intValue()));
                    }
                }
            }
            if (getLogger().isEnabledFor(Level.INFO)){getLogger().info("[agregarTransferencia][" + rutEmpresa + "][" + convenio + "][BCI_FINOK] transferenciasObtenidas [" + (transferenciasObtenidas != null ? transferenciasObtenidas.length : 0) + "]"); }
            return transferenciasObtenidas;

        } catch (EjecutarException eE) {
            if (getLogger().isEnabledFor(Level.ERROR)) {getLogger().error("[agregarTransferencia][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][EjecutarException eE][" + eE.getDetalleException() + "]");}
            throw new Exception(eE.getDetalleException());
        } catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) {getLogger().error("[agregarTransferencia][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][Exception e][" + e.toString() + "]");}
            throw new Exception(e.getMessage());
        }
    }

    /**
     * <p>M�todo que permite eliminar el detalle de una Transferencia</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 19/10/2009 Alejandro Barra (SEnTRA): Versi�n inicial.
     * <li>1.1 09/07/2010 Marcelo Fuentes (SEnTRA): Se agrega par�metro que permite ejecutar operaciones en "CHAINED_MODE" para Sybase.</li>
     * <li>1.2 30/01/2014 Marcelo Fuentes (SEnTRA): Se agrega logica para la identificacion de la respuesta recibida de la eliminacion.</li>
     * </ul>
     *
     * @param rutEmpresa rut de la empresa
     * @param convenio n�mero de convenio
     * @param numTransferencia n�mero de la transferencia
     * @param idDetalle identificador del detalle de la transferencia
     * @throws Exception
     * @return Respuesta de la eliminaci�n
     * @since 1.0cambioaboolean
     */
    public boolean eliminarTransferencia(String rutEmpresa, String convenio, String numTransferencia, int idDetalle) throws Exception{

        try{
            if(getLogger().isEnabledFor(Level.INFO)){
               getLogger().info("[eliminarTransferencia][" + numTransferencia + "][BCI_INI]");    
               getLogger().info("[eliminarTransferencia][" + numTransferencia + "] rutEmpresa [" + rutEmpresa + "], convenio [" + convenio + "], idDetalle [" + idDetalle + "]");
            }
            int respuesta = 1;
            String contexto = TablaValores.getValor("JNDIConfig.parametros","cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
            HashMap parametrosEliminarTTFF = new HashMap();
            parametrosEliminarTTFF.put("CHAINED_SUPPORT","true");
            parametrosEliminarTTFF.put("rutEmpresa",      rutEmpresa);
            parametrosEliminarTTFF.put("convenio",    convenio);
            parametrosEliminarTTFF.put("numTransferencia",    numTransferencia);
            parametrosEliminarTTFF.put("idDetalle", new Integer(idDetalle));

            respuesta = Integer.parseInt(String.valueOf( conector.ejecutar("banele","eliminarTransferencia", parametrosEliminarTTFF)).trim());

            if (respuesta == 1) { 
                if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[eliminarTransferencia][" + numTransferencia + "][BCI_FINOK] respuesta  [" + respuesta + "]");}
                return true; 
            }
            else {
                if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[eliminarTransferencia][" + numTransferencia + "][BCI_FINEX] respuesta  [" + respuesta + "]");}
                return false;
            }

        } catch (EjecutarException eE) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[eliminarTransferencia][" + numTransferencia + "][BCI_FINEX][EjecutarException eE][" + eE.getDetalleException() + "]");}
            throw new Exception(eE.getDetalleException());
        } catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[eliminarTransferencia][" + numTransferencia + "][BCI_FINEX][Exception e][" + e.toString() + "]");}
            throw new Exception(e.getMessage());
        }
    }

    /**
     * <p>M�todo que permite crear un Grupo de Transferencias Frecuentes</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 19/10/2009 Alejandro Barra (SEnTRA): Versi�n inicial.
     * <li>1.1 09/07/2010 Marcelo Fuentes (SEnTRA): Se agrega par�metro que permite ejecutar operaciones en "CHAINED_MODE" para Sybase.</li>
     *
     * </ul>
     *
     * @param convenio n�mero de convenio
     * @param rutEmpresa rut de la empresa
     * @param nombreGrupo nombre del grupo de transferencias
     * @param numTransferencia n�mero de la transferencia
     * @param rutUsu rut del usuario que realiza la creaci�n del grupo
     * @param ipUsuario ip del usuario que realiza la creaci�n del grupo
     * @throws Exception
     * @return Respuesta de la creaci�n.
     * @since 1.0
     */
    public boolean crearGrupo(String convenio, String rutEmpresa, String nombreGrupo, String numTransferencia, String rutUsuario, String ipUsuario) throws Exception{

        try{
            if(getLogger().isEnabledFor(Level.INFO)){
                getLogger().info("[crearGrupo][" + rutEmpresa + "][" + convenio + "][BCI_INI]");
                getLogger().info("[crearGrupo][" + rutEmpresa + "][" + convenio + "] nombreGrupo [" + nombreGrupo + "], numTransferencia [" + numTransferencia + "], rutUsuario [" + rutUsuario + "], ipUsuario [" + ipUsuario + "]");
            }
            int respuesta = 1;
            String contexto = TablaValores.getValor("JNDIConfig.parametros","cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
            HashMap parametrosCuenta = new HashMap();
            parametrosCuenta.put("CHAINED_SUPPORT","true");
            parametrosCuenta.put("convenio",      convenio);
            parametrosCuenta.put("rutEmpresa",    rutEmpresa);
            parametrosCuenta.put("nombreGrupo",     nombreGrupo);
            parametrosCuenta.put("numTransferencia",     numTransferencia);
            parametrosCuenta.put("rutUsuario",     rutUsuario);
            parametrosCuenta.put("ipUsuario",     ipUsuario);

            respuesta = Integer.parseInt(String.valueOf( conector.ejecutar("banele","crearGrupo", parametrosCuenta)).trim());

            if (respuesta == 0) {
                if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[crearGrupo][" + numTransferencia + "][BCI_FINOK] respuesta  [" + respuesta + "]");}
                return true; 
            }
            else {
                if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[crearGrupo][" + numTransferencia + "][BCI_FINEX] respuesta  [" + respuesta + "]");}
                return false;
            }

        } catch (EjecutarException eE) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[crearGrupo][" + numTransferencia + "][BCI_FINEX][EjecutarException eE][" + eE.getDetalleException() + "]");}
            throw new Exception(eE.getDetalleException());
        } catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[crearGrupo][" + numTransferencia + "][BCI_FINEX][Exception e][" + e.toString() + "]");}
            throw new Exception(e.getMessage());
        }
    }

    /**
     * <p>M�todo que elimina un grupo de Transferencias</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 19/10/2009 Alejandro Barra (SEnTRA): Versi�n inicial.
     * <li>1.1 09/07/2010 Marcelo Fuentes (SEnTRA): Se agrega par�metro que permite ejecutar operaciones en "CHAINED_MODE" para Sybase.</li>
     *
     * </ul>
     *
     * @param convenio n�mero de convenio
     * @param rutEmpresa rut de la empresa
     * @param nombreGrupo nombre del grupo de transferencias
     * @throws Exception
     * @return Respuesta de la eliminaci�n.
     * @since 1.0
     */
    public boolean eliminarGrupoDeLaTransferencia(String convenio, String rutEmpresa, String nombreGrupo) throws Exception{

        try{
            if(getLogger().isEnabledFor(Level.INFO)){
               getLogger().info("[eliminarGrupoDeLaTransferencia][" + rutEmpresa + "][" + convenio + "][BCI_INI]");
               getLogger().info("[eliminarGrupoDeLaTransferencia][" + rutEmpresa + "][" + convenio + "] nombreGrupo [" + nombreGrupo + "]");
            }
            int respuesta = 1;
            String contexto = TablaValores.getValor("JNDIConfig.parametros","cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
            HashMap parametrosCuenta = new HashMap();
            parametrosCuenta.put("CHAINED_SUPPORT","true");
            parametrosCuenta.put("convenio",      convenio);
            parametrosCuenta.put("rutEmpresa",    rutEmpresa);
            parametrosCuenta.put("nombreGrupo",     nombreGrupo);

            respuesta = Integer.parseInt(String.valueOf( conector.ejecutar("banele","eliminarGrupoDeLaTransferencia", parametrosCuenta)).trim());

            if (respuesta == 0) {
                if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[eliminarGrupoDeLaTransferencia][" + rutEmpresa + "][" + convenio + "][BCI_FINOK] respuesta [" + respuesta + "]");}
                return true; 
            }
            else {
                if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[eliminarGrupoDeLaTransferencia][" + rutEmpresa + "][" + convenio + "][BCI_FINEX] respuesta [" + respuesta + "]");}
                return false;
            }
        } catch (EjecutarException eE) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[eliminarGrupoDeLaTransferencia][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][EjecutarException eE][" + eE.getDetalleException() + "]");}

            throw new Exception(eE.getDetalleException());
        } catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[eliminarGrupoDeLaTransferencia][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][Exception e][" + e.toString() + "]");}
            throw new Exception(e.getMessage());
        }
    }

    /**
     * <p>M�todo que modifica los datos asociados a un grupo de Transferencias</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 19/10/2009 Alejandro Barra (SEnTRA): Versi�n inicial.
     * <li>1.1 09/07/2010 Marcelo Fuentes (SEnTRA): Se agrega par�metro que permite ejecutar operaciones en "CHAINED_MODE" para Sybase.</li>
     *
     * </ul>
     *
     * @param convenio n�mero de convenio
     * @param rutEmpresa rut de la empresa
     * @param nombreGrupo nombre del grupo de transferencias
     * @param numTransferencia n�mero de la transferencia
     * @param rutUsu rut del usuario que realiza la modificaci�n
     * @param ipUsuario ip del usuario que realiza la modificaci�n
     * @throws Exception
     * @return Respuesta de la modificaci�n.
     * @since 1.0
     */
    public boolean modificaGrupoDeLaTransferencia(String convenio, String rutEmpresa, String nombreGrupo, String numTransferencia, String rutUsu, String ipUsuario) throws Exception{

        try{
            if(getLogger().isEnabledFor(Level.INFO)){
               getLogger().info("[modificaGrupoDeLaTransferencia][" + rutEmpresa + "][" + convenio + "]BCI_INI]");
               getLogger().info("[modificaGrupoDeLaTransferencia][" + rutEmpresa + "][" + convenio + "] nombreGrupo  [" + nombreGrupo + "], numTransferencia [" + numTransferencia + "], rutUsu [" + rutUsu + "], ipUsuario [" + ipUsuario + "] ");
            }

            int respuesta = 1;
            String contexto = TablaValores.getValor("JNDIConfig.parametros","cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
            HashMap parametrosCuenta = new HashMap();
            parametrosCuenta.put("CHAINED_SUPPORT","true");
            parametrosCuenta.put("convenio",      convenio);
            parametrosCuenta.put("rutEmpresa",    rutEmpresa);
            parametrosCuenta.put("nombreGrupo",     nombreGrupo);
            parametrosCuenta.put("numTransferencia",     numTransferencia);
            parametrosCuenta.put("rutUsu",     rutUsu);
            parametrosCuenta.put("ipUsuario",     ipUsuario);

            respuesta = Integer.parseInt(String.valueOf( conector.ejecutar("banele","modificaGrupoDeLaTransferencia", parametrosCuenta)).trim());

            if (respuesta == 0) {
                if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[modificaGrupoDeLaTransferencia][" + numTransferencia + "][BCI_FINOK] respuesta  [" + respuesta + "]");}
                return true; 
            }
            else {
                if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[modificaGrupoDeLaTransferencia][" + numTransferencia + "][BCI_FINEX] respuesta  [" + respuesta + "]");}
                return false;
            }
        } catch (EjecutarException eE) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[modificaGrupoDeLaTransferencia][" + numTransferencia + "][BCI_FINEX][EjecutarException eE][" + eE.getDetalleException() + "]");}
            throw new Exception(eE.getDetalleException());
        } catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[modificaGrupoDeLaTransferencia][" + numTransferencia + "][BCI_FINEX][Exception e][" + e.toString() + "]");}
            throw new Exception(e.getMessage());
        }

    }

    /**
     * <p>M�todo que cambia el estado de un detalle de una Transferencia</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 19/10/2009 Alejandro Barra (SEnTRA): Versi�n inicial.
     * <li>1.1 24/03/2010 Marcelo Fuentes (SEnTRA): Se agrega nuevo par�metro codigoRechazo que ser� utilizado para guardar el motivo por el
     *                                              cual no se pudo realizar una transferencia o "0000" en el resto de los casos.
     *                                              Adem�s se agrega par�metro que permite ejecutar operaciones en "CHAINED_MODE" para Sybase.</li>
     *
     * </ul>
     *
     * @param rutUsuario rut del usuario que realiza la modificaci�n
     * @param dvUsuario d�gito verificador del usuario que realiza la modificaci�n
     * @param convenio n�mero de convenio
     * @param rutEmpresa rut de la empresa
     * @param dvEmpresa d�gito verificador del rut de la empresa
     * @param numTransferencia n�mero de la transferencia
     * @param detalleTransferencia n�mero del detalle de la transferencia
     * @param estado el estado en el que se quiere dejar la cuenta inscrita
     * @param codigoRechazo el motivo por el cual se rechaz� la transferencia
     * @param encabezadoODetalle indicador de si el cambio de estado es a un detalle o a un encabezado de una transferencia
     * @throws Exception
     * @return Respuesta de la modificaci�n.
     * @since 1.0
     */
    public boolean cambiaEstadoTransferencia(String rutUsuario, String dvUsuario, String convenio, String rutEmpresa, String dvEmpresa, String numTransferencia, int detalleTransferencia, String estado, String codigoRechazo, String encabezadoODetalle) throws Exception{

        try{
            if(getLogger().isEnabledFor(Level.INFO)){
               getLogger().info("[cambiaEstadoTransferencia][" + numTransferencia + "][BCI_INI]");
               getLogger().info("[cambiaEstadoTransferencia][" + numTransferencia + "] rutUsu [" + rutUsuario + "] - [" + dvUsuario + "],rutEmpresa [" + rutEmpresa + "] - [" + dvEmpresa + "],  convenio [" + convenio + "], numTransferencia [" + numTransferencia + "]");
               getLogger().info("[cambiaEstadoTransferencia][" + numTransferencia + "] detalleTransferencia [" + detalleTransferencia + "], estado [" + estado + "], codigoRechazo [" + codigoRechazo + "], encabezadoODetalle [" + encabezadoODetalle + "]");
            }
            int respuesta = 0;
            String contexto = TablaValores.getValor("JNDIConfig.parametros","cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
            HashMap parametrosCuenta = new HashMap();
            parametrosCuenta.put("CHAINED_SUPPORT","true");
            String inicioCierreCCA = TablaValores.getValor(TABLA_TRANSFERENCIAS, "FECHASCIERRECCA", "InicioCierreCCA");           
            parametrosCuenta.put("rutUsuario",      rutUsuario);
            parametrosCuenta.put("dvUsuario",    dvUsuario);
            parametrosCuenta.put("convenio",      convenio);
            parametrosCuenta.put("rutEmpresa",    rutEmpresa);
            parametrosCuenta.put("dvEmpresa",    dvEmpresa);
            parametrosCuenta.put("numTransferencia",     numTransferencia);
            parametrosCuenta.put("detalleTransferencia",     new Integer(detalleTransferencia));
            parametrosCuenta.put("estado",     estado);
            parametrosCuenta.put("codigoRechazo",     codigoRechazo);
            parametrosCuenta.put("encabezadoODetalle",     encabezadoODetalle);

            if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[cambiaEstadoTransferencia][" + numTransferencia + "] InicioCierreCCA [" + inicioCierreCCA + "]");}
            parametrosCuenta.put("inicioCierreCCA",     inicioCierreCCA);

            respuesta = Integer.parseInt(String.valueOf( conector.ejecutar("banele","cambiaEstadoTransferencia", parametrosCuenta)).trim());
            
            if (respuesta == 0) {
                if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[cambiaEstadoTransferencia][" + numTransferencia + "][BCI_FINOK] respuesta  [" + respuesta + "]");}
                return true; 
            }
            else {
                if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[cambiaEstadoTransferencia][" + numTransferencia + "][BCI_FINEX] respuesta  [" + respuesta + "]");}
                return false;
            }

        } catch (EjecutarException eE) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[cambiaEstadoTransferencia][" + numTransferencia + "][BCI_FINEX][EjecutarException eE][" + eE.getDetalleException() + "]");}
            throw new Exception(eE.getDetalleException());
        } catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[cambiaEstadoTransferencia][" + numTransferencia + "][BCI_FINEX][Exception e][" + e.toString() + "]");}
            throw new Exception(e.getMessage());
        }

    }

    /**
     * <p>M�todo que obtiene un listado de Transferencias frecuentes pertenecientes a un grupo dado</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 19/10/2009 Alejandro Barra (SEnTRA): Versi�n inicial.
     * <li>1.1 05/01/2010 Marcelo Fuentes (SEnTRA): Se elimina la obtenci�n del nombre de banco desde la tabla
     *                    bancos.codigos ya que los nombres de los bancos se obtienen de una consulta a la CCA.</li>
     * <li>1.2 09/07/2010 Marcelo Fuentes (SEnTRA): Se agrega par�metro que permite ejecutar operaciones en "CHAINED_MODE" para Sybase.</li>
     * <li>1.3 07/12/2010 Marcelo Fuentes (SEnTRA): Se agrega la obtenci�n del tipoDePago, nombreDelBeneficiario y emailDelBeneficiario
     *                    en la consulta a la Base de Datos.</li>
     *
     * </ul>
     *
     * @param convenio n�mero de convenio
     * @param rutEmpresa rut de la empresa
     * @param nombreGrupo nombre del grupo de transferencia
     * @throws Exception
     * @return Listado con las transferencias pertenecientes al grupo.
     * @since 1.0
     */
    public TransferenciasEnLineaVO[] obtieneGruposFrecuentes(String convenio, String rutEmpresa, String nombreGrupo) throws Exception{

        try{
            if(getLogger().isEnabledFor(Level.INFO)){
               getLogger().info("[obtieneGruposFrecuentes][" + rutEmpresa + "][" + convenio + "][BCI_INI]");
               getLogger().info("[obtieneGruposFrecuentes][" + rutEmpresa + "][" + convenio + "] nombreGrupo [" + nombreGrupo + "]");
            }
            List documento = null;
            HashMap salida = null;
            TransferenciasEnLineaVO[] gruposObtenidas = null;
            String contexto = TablaValores.getValor("JNDIConfig.parametros","cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
            HashMap parametrosGrupo = new HashMap();
            parametrosGrupo.put("CHAINED_SUPPORT","true");
            parametrosGrupo.put("convenio",      convenio);
            parametrosGrupo.put("rutEmpresa",    rutEmpresa);
            parametrosGrupo.put("nombreGrupo",     nombreGrupo);
            documento = conector.consultar("banele", "obtieneGruposFrecuentes",parametrosGrupo);
            
            if (documento != null){
                if(documento.size() > 0){
                    gruposObtenidas = new TransferenciasEnLineaVO[documento.size()];
                    if(getLogger().isEnabledFor(Level.INFO)){
                       getLogger().info("[obtieneGruposFrecuentes][" + rutEmpresa + "][" + convenio + "] cuentasObtenidas [" + documento.size() + "]");
                    }
                    for(int i = 0 ; i < documento.size() ; i++){
                        salida = (HashMap) documento.get(i);
                        gruposObtenidas[i] = new TransferenciasEnLineaVO();
                        gruposObtenidas[i].setNombreDelGrupo((((String)salida.get("nombreDelGrupo")).trim()));
                        gruposObtenidas[i].setNumeroDeTransferencia((((String)salida.get("numeroTransferencia")).trim()));
                        if(((String)salida.get("rutDestinatario")).trim().equals("")){
                            gruposObtenidas[i].setRutBeneficiario(0);
                        }else{
                            gruposObtenidas[i].setRutBeneficiario(Long.parseLong(((String)salida.get("rutDestinatario")).trim()));
                        }
                        if(((String)salida.get("dvDestinatario")).trim().equals("")){
                            gruposObtenidas[i].setDvBeneficiario(' ');
                        }else{
                            gruposObtenidas[i].setDvBeneficiario(((String)salida.get("dvDestinatario")).trim().charAt(0));
                        }
                        gruposObtenidas[i].setNombreCuenta(((String)salida.get("nombreCuenta")).trim());
                        gruposObtenidas[i].setMonto((((Double)salida.get("monto")).doubleValue()));
                        gruposObtenidas[i].setCuentaDestino(((String)salida.get("cuentaDestino")).trim());
                        gruposObtenidas[i].setCodigoBancoDestino(((String)salida.get("bancoDestino")).trim());
                        gruposObtenidas[i].setTipoDePago(((String)salida.get("tipoDePago")).trim());
                        gruposObtenidas[i].setNombreDelBeneficiario(((String)salida.get("nombreDelBeneficiario")).trim());
                        gruposObtenidas[i].setEmailDelBeneficiario(((String)salida.get("emailDelBeneficiario")).trim());
                    }
                }
            }
            if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[obtieneGruposFrecuentes][" + rutEmpresa + "][" + convenio + "][BCI_FINOK] gruposObtenidas [" + (gruposObtenidas != null ? gruposObtenidas.length : 0) + "]");}
            return gruposObtenidas;
            
        } catch (EjecutarException eE) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[obtieneGruposFrecuentes][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][EjecutarException eE][" + eE.getDetalleException() + "]");}
            throw new Exception(eE.getDetalleException());
        } catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[obtieneGruposFrecuentes][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][Exception e][" + e.toString() + "]");}
            throw new Exception(e.getMessage());
        }

    }

    /**
     * <p>M�todo que modifica un detalle de Transferencia.</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 19/10/2009 Alejandro Barra (SEnTRA): Versi�n inicial.
     * <li>1.1 09/07/2010 Marcelo Fuentes (SEnTRA): Se agrega par�metro que permite ejecutar operaciones en "CHAINED_MODE" para Sybase.</li>
     *
     * </ul>
     *
     * @param convenio n�mero de convenio
     * @param rutEmpresa rut de la empresa
     * @param detalleVO los datos del detalle de la transferencia a modificar
     * @throws Exception
     * @return Respuesta de la modificaci�n.
     * @since 1.0
     */
    public boolean modificaDetalleTransferencia(String convenio, String rutEmpresa, DetalleTransferenciaVO detalleVO) throws Exception{

    String numTransferencia = detalleVO.getNumeroTransferencia();
        try{
             if(getLogger().isEnabledFor(Level.INFO)){
                getLogger().info("[modificaDetalleTransferencia][" + numTransferencia + "][BCI_INI]");
                getLogger().info("[modificaDetalleTransferencia][" + numTransferencia + "] convenio [" + convenio + "], rutEmpresa [" + rutEmpresa + "], detalleTransferencia [" + detalleVO.getNumeroDetalle() + "], monto [" + detalleVO.getMonto() + "]");
                getLogger().info("[modificaDetalleTransferencia][" + numTransferencia + "] mensajeEmail [" + detalleVO.getMensajeEmailDestino() + "], ordenCompra [" + detalleVO.getOrdenCompra() + "], numeroFactura [" + detalleVO.getNumeroFactura() + "]");
            }
            int respuesta = 1;
            String contexto = TablaValores.getValor("JNDIConfig.parametros","cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
            HashMap parametrosCuenta = new HashMap();
            parametrosCuenta.put("CHAINED_SUPPORT","true");
            parametrosCuenta.put("convenio",      convenio);
            parametrosCuenta.put("rutEmpresa",    rutEmpresa);
            parametrosCuenta.put("numTransferencia",     detalleVO.getNumeroTransferencia());
            parametrosCuenta.put("numDetalle",     new Integer(detalleVO.getNumeroDetalle()));
            parametrosCuenta.put("monto",     new Long(detalleVO.getMonto()));
            parametrosCuenta.put("msjEmailDestino",     detalleVO.getMensajeEmailDestino());
            parametrosCuenta.put("ordenCompra",     detalleVO.getOrdenCompra());
            parametrosCuenta.put("numeroFactura",     detalleVO.getNumeroFactura());

            respuesta = Integer.parseInt(String.valueOf( conector.ejecutar("banele","modificaDetalleTransferencia", parametrosCuenta)).trim());
            
            if (respuesta == 0) {
                if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[modificaDetalleTransferencia][" + numTransferencia + "][BCI_FINOK] respuesta  [" + respuesta + "]");}
                return true; 
            }
            else {
                if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[modificaDetalleTransferencia][" + numTransferencia + "][BCI_FINEX] respuesta  [" + respuesta + "]");}
                return false;
            }

        } catch (EjecutarException eE) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[modificaDetalleTransferencia][" + numTransferencia + "][BCI_FINEX][EjecutarException eE][" + eE.getDetalleException() + "]");}
            throw new Exception(eE.getDetalleException());
        } catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[modificaDetalleTransferencia][" + numTransferencia + "][BCI_FINEX][Exception e][" + e.toString() + "]");}
            throw new Exception(e.getMessage());
        }

    }

    /**
     * <p>M�todo que obtiene un listado de transferencias realizadas, consultando seg�n ciertos criterios.</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 19/10/2009 Alejandro Barra (SEnTRA): Versi�n inicial.
     * <li>1.1 05/01/2010 Marcelo Fuentes (SEnTRA): Se elimina la obtenci�n del nombre de banco desde la tabla
     *                                              bancos.codigos ya que los nombres de los bancos se obtienen
     *                                              de una consulta a la CCA.</li>
     * <li>1.2 23/11/2011 Denisse Vel�squez (SEnTRA): Se rescatan los c�digos de error de la CCA, enviados por
     *                                              Pago de N�minas a OTB.</li>
     * <li> 1.3 25/03/2013, Sergio Cuevas D. (SEnTRA): Se agrego el seteo del campo "vuelveIntentar" para saber si 
     *                          se coloca en el excel si debe intentar de nuevo</li>
     * </ul>
     *
     * @param filtro los filtros a aplicar a la consulta
     * @param bloque la p�gina a consultar
     * @param cantidadRegistros la cantidad de registros por cada p�gina
     * @throws Exception
     * @return Listado con las transferencias realizadas.
     * @since 1.0
     */
    public DetalleTransferenciaVO[] extraeTransferenciasRealizadas(FiltroConsultarTransferenciasVO filtro,
        int bloque, int cantidadRegistros) throws Exception{

        String rutEmpresa = filtro.getRutOrigen();
        String convenio = filtro.getConvenio();
        try{
            if(getLogger().isEnabledFor(Level.INFO)){
               getLogger().info("[extraeTransferenciasRealizadas][" + rutEmpresa + "][" + convenio + "][BCI_INI]");
               getLogger().info("[extraeTransferenciasRealizadas][" + rutEmpresa + "][" + convenio + "] filtro ["+ filtro.toString() +"],bloque ["+ bloque +"],cantidadRegistros ["+ cantidadRegistros +"]");
            }
            List documento = null;
            HashMap salida = null;
            HashMap parametros = new HashMap();
            DetalleTransferenciaVO[] transferenciasRealizadasVO = null;
            parametros.put("CHAINED_SUPPORT","true");
            parametros.put("convenio", filtro.getConvenio());
            parametros.put("rutEmpresa", filtro.getRutOrigen());
            parametros.put("nroCuenta",  filtro.getCuentaCorriente());
            parametros.put("rutDestino", filtro.getRutDestino());
            parametros.put("fechaDesde", filtro.getFechaDesde());
            parametros.put("fechaHasta", filtro.getFechaHasta());
            parametros.put("glosa", filtro.getGlosa());
            parametros.put("estado", filtro.getEstado());
            parametros.put("numTransferencia", filtro.getNumeroTransferencia());
            parametros.put("bloque", Integer.valueOf(String.valueOf(bloque)));
            parametros.put("cantidadRegistros", Integer.valueOf(String.valueOf(cantidadRegistros)));
            String contexto = TablaValores.getValor("JNDIConfig.parametros","cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector =
                new ConectorServicioDePersistenciaDeDatos(contexto);
            documento = conector.consultar("banele", "extraeTransferenciasRealizadas", parametros);
            if (documento != null){
                if(documento.size() > 0){
                    String codigoRechazo = "";
                    String glosaRechazo = "";
                    String vuelveIntentar = "";
                    transferenciasRealizadasVO = new DetalleTransferenciaVO[documento.size()];
                    if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[extraeTransferenciasRealizadas][" + rutEmpresa + "][" + convenio + "] transferenciasRealizadas[" + documento.size() + "]");}
                    for(int i = 0 ; i < documento.size() ; i++){
                        codigoRechazo = "";
                        glosaRechazo = "";
                        vuelveIntentar = "";
                        salida = (HashMap) documento.get(i);
                        transferenciasRealizadasVO[i] = new DetalleTransferenciaVO();
                        transferenciasRealizadasVO[i].setNumeroTransferencia(
                            String.valueOf(salida.get("numeroOperacion")).trim());
                        transferenciasRealizadasVO[i].setNumeroDetalle(
                            String.valueOf(salida.get("numeroDetalle")).trim());
                        transferenciasRealizadasVO[i].setCuentaOrigen(
                            StringUtil.sacaCeros(String.valueOf(salida.get("cuentaOrigen")).trim()));
                        transferenciasRealizadasVO[i].setMonto(Long.parseLong(
                            String.valueOf(salida.get("montoTransferido")).trim()));
                        transferenciasRealizadasVO[i].setCuentaDestino(
                            StringUtil.sacaCeros(String.valueOf(salida.get("cuentaDestino")).trim()));
                        transferenciasRealizadasVO[i].setNombreDestinatario(
                            String.valueOf( salida.get("nombreDestinatario")).trim());
                        transferenciasRealizadasVO[i].setRutDestino(
                            Long.parseLong(String.valueOf(salida.get("rutDestino")).trim()));
                        transferenciasRealizadasVO[i].setDvDestino(
                            String.valueOf(salida.get("dVDestino")).trim().charAt(0));
                        String codigoBancoDestino =
                            String.valueOf(salida.get("bancoDestino")).trim();

                        if(codigoBancoDestino.length()>3){
                            codigoBancoDestino = codigoBancoDestino.substring(
                                codigoBancoDestino.length()-3, codigoBancoDestino.length());
                        }

                        transferenciasRealizadasVO[i].setBancoDestino(codigoBancoDestino);
                        String codigoEstado = String.valueOf(salida.get("estado")).trim();
                        transferenciasRealizadasVO[i].setEstado(codigoEstado);
                        transferenciasRealizadasVO[i].setGlosaEstado(
                            TablaValores.getValor(TABLA_TRANSFERENCIAS, codigoEstado, "Desc"));
                        transferenciasRealizadasVO[i].setFechaCreacion((Date)salida.get("fechaCreacion"));
                        transferenciasRealizadasVO[i].setFechaPago((Date)salida.get("fechaPago"));
                        transferenciasRealizadasVO[i].setNombreArchivo(String.valueOf(
                            salida.get("nomTransferencia")).trim());
                        String modalidad = String.valueOf(salida.get("modalidad")).trim();
                        transferenciasRealizadasVO[i].setModalidad(modalidad);
                        transferenciasRealizadasVO[i].setGlosaModalidad(TablaValores.getValor(
                            TABLA_TRANSFERENCIAS, "tipoModalidad", modalidad));
                        transferenciasRealizadasVO[i].setTipoIngreso(String.valueOf(
                            salida.get("modalidad")).trim());
                        String codigoTipo = String.valueOf(salida.get("tipo")).trim();
                        transferenciasRealizadasVO[i].setTipo(codigoTipo);
                        String glosaTipo = TablaValores.getValor(
                            TABLA_TRANSFERENCIAS, "tipoPagoTransferencia", codigoTipo);
                        if(glosaTipo != null){
                            transferenciasRealizadasVO[i].setGlosaTipo(glosaTipo);
                        }else{
                            transferenciasRealizadasVO[i].setGlosaTipo("");
                        }

                        transferenciasRealizadasVO[i].setEmailDestino(String.valueOf(
                            salida.get("emailDestino")).trim());
                        transferenciasRealizadasVO[i].setMensajeEmailDestino(
                            String.valueOf(salida.get("mensajeEmailDestino")).trim());
                        transferenciasRealizadasVO[i].setOrdenCompra(String.valueOf(
                            salida.get("ordenCompra")).trim());
                        transferenciasRealizadasVO[i].setNumeroFactura(String.valueOf(
                            salida.get("numeroFactura")).trim());
                        transferenciasRealizadasVO[i].setNombreUsuario(String.valueOf(
                            salida.get("nombreUsuario")).trim());
                        codigoRechazo = String.valueOf(salida.get("rechazo")).trim();
                        transferenciasRealizadasVO[i].setRechazo(codigoRechazo);
                        transferenciasRealizadasVO[i].setMotivoRechazo(String.valueOf(
                            salida.get("motivoRechazo")).trim());
                        if(!codigoRechazo.equals("")){
                            if(codigoRechazo.equals(ERROR_TIMEOUT)){
                                glosaRechazo = TablaValores.getValor(
                                    ARCH_PARAM_ERR, codigoRechazo, "Desc");
                            }
                            else{
                                if ("C".equals(String.valueOf(codigoRechazo.charAt(0)))){
                                    codigoRechazo = "0" + codigoRechazo.substring(
                                        codigoRechazo.length()-3, codigoRechazo.length()).trim();
                                    glosaRechazo = TablaValores.getValor(
                                        TABLA_TTFF_TERCEROS, codigoRechazo, "mensaje");
                                    vuelveIntentar  = TablaValores.getValor(TABLA_TTFF_TERCEROS, 
                                        codigoRechazo, "vuelveIntentar");
                                } 
                                else
                                    glosaRechazo = TablaValores.getValor(
                                        TABLA_ERROR_TRF, codigoRechazo, "Desc");
                            }
                            if(glosaRechazo != null){
                                transferenciasRealizadasVO[i].setMotivoRechazo(glosaRechazo);
                            }
                            if(vuelveIntentar != null){
                                transferenciasRealizadasVO[i].setVuelveIntentar(vuelveIntentar);
                            }

                        }
                        transferenciasRealizadasVO[i].setCantidadBloques(Integer.parseInt(
                            String.valueOf(salida.get("numerosPagina")).trim()));
                        transferenciasRealizadasVO[i].setCantidadTotaltransferencias(Integer.parseInt(
                            String.valueOf(salida.get("cantidadTransferencias")).trim()));
                    }
                }
            }
            if (getLogger().isEnabledFor(Level.INFO)){ 
                getLogger().info("[extraeTransferenciasRealizadas][" + rutEmpresa + "][" + convenio + "][BCI_FINOK] transferenciasRealizadasVO [" + (transferenciasRealizadasVO != null ? transferenciasRealizadasVO.length : 0) + "]");
            }
            return transferenciasRealizadasVO;

        } catch (EjecutarException eE) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[extraeTransferenciasRealizadas][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][EjecutarException eE][" + eE.getDetalleException() + "]");}
            throw new Exception(eE.getDetalleException());
        } catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[extraeTransferenciasRealizadas][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][Exception e][" + ErroresUtil.extraeStackTrace(e) + "]");}
            throw new Exception(e.getMessage());
        }

    }


    /**
     * <p>M�todo que obtiene los montos totales agrupados por cuenta para un listado de transferencias realizadas,
     * obtenidas seg�n ciertos criterios.</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 19/10/2009 Alejandro Barra (SEnTRA): Versi�n inicial.
     *
     * </ul>
     *
     * @param filtro los filtros a aplicar a la consulta
     * @throws Exception
     * @return Listado con los montos totales de una trasferencia agrupados por cuenta.
     * @since 1.0
     */
    public TotalPorCuentaVO[] extraeTotalesPorCuentaDeTransferenciasRealizadas(FiltroConsultarTransferenciasVO filtro) throws Exception{

        String rutEmpresa = filtro.getRutOrigen();
        String convenio = filtro.getConvenio();
        try{
            if(getLogger().isEnabledFor(Level.INFO)){
               getLogger().info("[extraeTotalesPorCuentaDeTransferenciasRealizadas][" + rutEmpresa + "][" + convenio + "][BCI_INI]");
               getLogger().info("[extraeTotalesPorCuentaDeTransferenciasRealizadas][" + rutEmpresa + "][" + convenio + "]filtro :[" + filtro.toString() + "]");
            }

            List documento = null;
            HashMap salida = null;
            HashMap parametros = new HashMap();
            TotalPorCuentaVO[] totalesPorCuentaVO = null;
            parametros.put("CHAINED_SUPPORT","true");
            parametros.put("convenio", filtro.getConvenio());
            parametros.put("rutEmpresa", filtro.getRutOrigen());
            parametros.put("nroCuenta",  filtro.getCuentaCorriente());
            parametros.put("rutDestino", filtro.getRutDestino());
            parametros.put("fechaDesde", filtro.getFechaDesde());
            parametros.put("fechaHasta", filtro.getFechaHasta());
            parametros.put("glosa", filtro.getGlosa());
            parametros.put("estado", filtro.getEstado());
            parametros.put("numTransferencia", filtro.getNumeroTransferencia());

            String contexto = TablaValores.getValor("JNDIConfig.parametros","cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
            documento = conector.consultar("banele", "extraeTotalesPorCuentaDeTransferenciasRealizadas", parametros);

            if (documento != null){
                if(documento.size() > 0){
                    totalesPorCuentaVO = new TotalPorCuentaVO[documento.size()];
                    if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[extraeTotalesPorCuentaDeTransferenciasRealizadas][" + rutEmpresa + "][" + convenio + "] totalesPorCuenta [" + documento.size() + "]");}
                    for(int i = 0 ; i < documento.size() ; i++){
                        salida = (HashMap) documento.get(i);
                        totalesPorCuentaVO[i] = new TotalPorCuentaVO();
                        totalesPorCuentaVO[i].setNumeroCuenta(StringUtil.sacaCeros(String.valueOf(salida.get("numeroCuenta")).trim()));
                        totalesPorCuentaVO[i].setCantidadTransferencias(Integer.parseInt(String.valueOf(salida.get("cantidadTransferencias")).trim()));
                        totalesPorCuentaVO[i].setMonto(Long.parseLong(String.valueOf(salida.get("monto")).trim()));
                    }
                }
            }
            if (getLogger().isEnabledFor(Level.INFO)){ 
                getLogger().info("[extraeTotalesPorCuentaDeTransferenciasRealizadas][" + rutEmpresa + "][" + convenio + "][BCI_FINOK] totalesPorCuentaVO [" + (totalesPorCuentaVO != null ? totalesPorCuentaVO.length : 0) + "]");
            }
            return totalesPorCuentaVO;

        } catch (EjecutarException eE) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[extraeTotalesPorCuentaDeTransferenciasRealizadas][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][EjecutarException eE][" + eE.getDetalleException() + "]");}
            throw new Exception(eE.getDetalleException());
        } catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[extraeTotalesPorCuentaDeTransferenciasRealizadas][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][Exception e][" + ErroresUtil.extraeStackTrace(e) + "]");}
            throw new Exception(e.getMessage());
        }

    }


    /**
     * <p>M�todo que obtiene un listado de los detalles asociados a una transferencia pendiente.</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 19/10/2009 Alejandro Barra (SEnTRA): Versi�n inicial.
     * <li>1.1 05/01/2010 Marcelo Fuentes (SEnTRA): Se elimina la obtenci�n del nombre de banco desde la tabla
     *                    bancos.codigos ya que los nombres de los bancos se obtienen de una consulta a la CCA.</li>
     * <li>1.2 27/01/2011 Marcelo Fuentes (SEnTRA): Debido a cambio del tipo de dato de atributo numeroOperacion se cambia parseo de int a long</li>
     *
     * </ul>
     *
     * @param rutEmpresa rut de la empresa
     * @param convenio n�mero de convenio
     * @param numeroTransferencia n�mero de transferencia
     * @param bloque la p�gina a consultar
     * @param cantidadRegistros la cantidad de registros por cada p�gina
     * @throws Exception
     * @return Listado con los detalles de la transferencia a consultar.
     * @since 1.0
     */
    public TransferenciasVO[] extraeDetallesTransferenciaPendiente(String rutEmpresa, String convenio, String  numeroTransferencia, int bloque, int cantidadRegistros) throws Exception{

        try{
            if(getLogger().isEnabledFor(Level.INFO)){
               getLogger().info("[extraeDetallesTransferenciaPendiente][" + numeroTransferencia + "][BCI_INI]");
               getLogger().info("[extraeDetallesTransferenciaPendiente][" + numeroTransferencia + "] bloque [" + bloque + "], cantidad de registros [" + cantidadRegistros + "]");
            }
            
            List documento = null;
            HashMap salida = null;
            HashMap parametros = new HashMap();
            TransferenciasVO[] transferenciasPendientesVO = null;
            Date fechaConsulta = null ;
            parametros.put("CHAINED_SUPPORT",     "true");
            parametros.put("rutEmpresa",          rutEmpresa);
            parametros.put("convenio",            convenio);
            parametros.put("numeroTransferencia", numeroTransferencia);
            parametros.put("bloque",              Integer.valueOf(String.valueOf(bloque)));
            parametros.put("cantidadRegistros",   Integer.valueOf(String.valueOf(cantidadRegistros)));

            String contexto = TablaValores.getValor("JNDIConfig.parametros","cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
            documento = conector.consultar("banele", "extraeDetallesTransferenciaPendiente", parametros);

            if (documento != null){
                if(documento.size() > 0){
                    transferenciasPendientesVO = new TransferenciasVO[documento.size()];
                    if(getLogger().isEnabledFor(Level.INFO)){
                    	getLogger().info("[extraeDetallesTransferenciaPendiente] detallesTransferenciaPendiente [" + documento.size() + "]");
                    }
                    for(int i = 0 ; i < documento.size() ; i++){
                        salida = (HashMap) documento.get(i);
                        transferenciasPendientesVO[i] = new TransferenciasVO();
                        transferenciasPendientesVO[i].setNumeroOperacion(Long.parseLong(String.valueOf(salida.get("numeroOperacion")).trim()));
                        transferenciasPendientesVO[i].setNumeroDetalle(Integer.parseInt(String.valueOf(salida.get("numeroDetalle")).trim()));
                        transferenciasPendientesVO[i].setCuentaOrigen(StringUtil.sacaCeros(String.valueOf(salida.get("cuentaOrigen")).trim()));

                        String modalidad = String.valueOf(salida.get("modalidad")).trim();
                        transferenciasPendientesVO[i].setModalidad(modalidad);
                        transferenciasPendientesVO[i].setGlosaModalidad(TablaValores.getValor(TABLA_TRANSFERENCIAS, "tipoModalidad", modalidad));
                        transferenciasPendientesVO[i].setTipoIngreso(String.valueOf(salida.get("modalidad")).trim());
                        transferenciasPendientesVO[i].setMensajeEmailDestino(String.valueOf(salida.get("mensajeEmailDestino")).trim());
                        String codigoTipo = String.valueOf(salida.get("tipo")).trim();
                        transferenciasPendientesVO[i].setTipo(codigoTipo);
                        String glosaTipo = TablaValores.getValor(TABLA_TRANSFERENCIAS, "tipoPagoTransferencia", codigoTipo);
                        if(glosaTipo != null){
                            transferenciasPendientesVO[i].setGlosaTipo(glosaTipo);
                        }else{
                            transferenciasPendientesVO[i].setGlosaTipo("");
                        }

                        transferenciasPendientesVO[i].setMonto(Long.parseLong(String.valueOf(salida.get("montoTransferido")).trim()));
                        String nombreDestinatario = String.valueOf(salida.get("nombreDestinatario")).trim();
                        transferenciasPendientesVO[i].setNombreDestinatario(nombreDestinatario);
                        transferenciasPendientesVO[i].setCuentaDestino(StringUtil.sacaCeros(String.valueOf(salida.get("cuentaDestino")).trim()));
                        String nombreCuentaDestino = String.valueOf(salida.get("nombreCuentaDestino")).trim();
                        if(nombreCuentaDestino!=null && !nombreCuentaDestino.equals("")){
                            transferenciasPendientesVO[i].setNombreCuentaDestino(nombreCuentaDestino);
                        }else{
                            transferenciasPendientesVO[i].setNombreCuentaDestino(nombreDestinatario);
                        }
                        transferenciasPendientesVO[i].setRutDestino(Long.parseLong(String.valueOf(salida.get("rutDestino")).trim()));
                        transferenciasPendientesVO[i].setDvDestino(String.valueOf(salida.get("dVDestino")).trim().charAt(0));
                        String codigoBancoDestino = String.valueOf(salida.get("bancoDestino")).trim();
                        if(codigoBancoDestino.length()>3){
                            codigoBancoDestino = codigoBancoDestino.substring(codigoBancoDestino.length()-3, codigoBancoDestino.length());
                        }
                        transferenciasPendientesVO[i].setBancoDestino(codigoBancoDestino);
                        String codigoEstado = String.valueOf(salida.get("estado")).trim();
                        transferenciasPendientesVO[i].setEstado(codigoEstado);
                        transferenciasPendientesVO[i].setGlosaEstado(TablaValores.getValor(TABLA_TRANSFERENCIAS, codigoEstado, "Desc"));
                        fechaConsulta = (Date)salida.get("fechaCreacion");
                        transferenciasPendientesVO[i].setFechaCreacion(fechaConsulta);
                        fechaConsulta = (Date)salida.get("fechaPago");
                        transferenciasPendientesVO[i].setFechaPago(fechaConsulta);
                        transferenciasPendientesVO[i].setComentario(String.valueOf(salida.get("nomTransferencia")).trim());
                        transferenciasPendientesVO[i].setCantidadBloques(Integer.parseInt(String.valueOf(salida.get("numerosPagina")).trim()));
                        transferenciasPendientesVO[i].setCantidadTotaltransferencias(Integer.parseInt(String.valueOf(salida.get("cantidadTransferencias")).trim()));
                    }
                }
            }
            if (getLogger().isEnabledFor(Level.INFO)){ 
                getLogger().info("[extraeDetallesTransferenciaPendiente][" + numeroTransferencia + "][BCI_FINOK] transferenciasPendientesVO [" + (transferenciasPendientesVO != null ? transferenciasPendientesVO.length : 0) + "]");
            }

            return transferenciasPendientesVO;

        } catch (EjecutarException eE) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[extraeDetallesTransferenciaPendiente][" + numeroTransferencia + "][BCI_FINEX][EjecutarException eE][" + eE.getDetalleException() + "]");}
            throw new Exception(eE.getDetalleException());
        } catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[extraeDetallesTransferenciaPendiente][" + numeroTransferencia + "][BCI_FINEX][Exception e][" + ErroresUtil.extraeStackTrace(e) + "]");}
            throw new Exception(e.getMessage());
        }

    }


    /**
     * <p>M�todo que obtiene un listado de transferencias pendientes, consultando seg�n ciertos criterios.</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 19/10/2009 Alejandro Barra (SEnTRA): Versi�n inicial.
     * <li>1.1 05/08/2011 Pedro Carmona Escobar (SEnTRA): Se agrega seteo del atributo 'tieneCuentasNoInscritas' con el valor 'true' si el
     *                                                      objeto 'hayCuentasNoInscritas' dentro del HashMap es retornado con el valor '1'.
     *                                                      En caso contrario, se setear� el valor 'false'.
     *
     * </ul>
     *
     * @param filtro los filtros a aplicar a la consulta
     * @param bloque la p�gina a consultar
     * @param cantidadRegistros la cantidad de registros por cada p�gina
     * @throws Exception
     * @return Listado con las transferencias pendientes.
     * @since 1.0
     */
    public EncabezadoTransferenciaVO[] extraeTransferenciasPendientes(FiltroConsultarTransferenciasVO filtro, int bloque, int cantidadRegistros) throws Exception{

        String rutEmpresa = filtro.getRutOrigen();
        String convenio = filtro.getConvenio();
        
        try{
            if(getLogger().isEnabledFor(Level.INFO)){
               getLogger().info("[extraeTransferenciasPendientes][" + rutEmpresa + "][" + convenio + "][BCI_INI]");
               getLogger().info("[extraeTransferenciasPendientes][" + rutEmpresa + "][" + convenio + "] bloque [" + bloque + "] , cantidad de registros [" + cantidadRegistros + "], filtro [" + filtro.toString() + "]");
            }
            
            List documento = null;
            HashMap salida = null;
            HashMap parametros = new HashMap();
            EncabezadoTransferenciaVO[] transferenciasPendientesVO = null;
            parametros.put("CHAINED_SUPPORT","true");
            parametros.put("convenio", filtro.getConvenio());
            parametros.put("rutEmpresa", filtro.getRutOrigen());
            parametros.put("nroCuenta",  filtro.getCuentaCorriente());
            parametros.put("rutDestino", filtro.getRutDestino());
            parametros.put("fechaDesde", filtro.getFechaDesde());
            parametros.put("fechaHasta", filtro.getFechaHasta());
            parametros.put("estado", filtro.getEstado());
            parametros.put("numTransferencia", filtro.getNumeroTransferencia());
            parametros.put("bloque", Integer.valueOf(String.valueOf(bloque)));
            parametros.put("cantidadRegistros", Integer.valueOf(String.valueOf(cantidadRegistros)));

            String contexto = TablaValores.getValor("JNDIConfig.parametros","cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
            documento = conector.consultar("banele", "extraeTransferenciasPendientes", parametros);
            if(getLogger().isEnabledFor(Level.INFO)){
                getLogger().info("[extraeTransferenciasPendientes][" + rutEmpresa + "][" + convenio + "] despues de la ejecucion del sp");
                getLogger().info("[extraeTransferenciasPendientes][" + rutEmpresa + "][" + convenio + "] Cantidad de registros: " + documento.size());
            }
            if (documento != null){
                if(documento.size() > 0){
                    transferenciasPendientesVO = new EncabezadoTransferenciaVO[documento.size()];
                    if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[extraeTransferenciasPendientes][" + rutEmpresa + "][" + convenio + "] transferenciasPendientes [" + documento.size() + "]");}
                    for(int i = 0 ; i < documento.size() ; i++){
                        salida = (HashMap) documento.get(i);
                        transferenciasPendientesVO[i] = new EncabezadoTransferenciaVO();
                        transferenciasPendientesVO[i].setNumeroTransferencia(String.valueOf(salida.get("numeroOperacion")).trim());
                        transferenciasPendientesVO[i].setCantidadDetalles(Integer.parseInt(String.valueOf(salida.get("cantidadDetalles")).trim()));
                        transferenciasPendientesVO[i].setNombreTransferencia(String.valueOf(salida.get("nomTransferencia")).trim());
                        transferenciasPendientesVO[i].setCuentaOrigen(StringUtil.sacaCeros(String.valueOf(salida.get("cuentaOrigen")).trim()));
                        transferenciasPendientesVO[i].setMonto(Long.parseLong(String.valueOf(salida.get("montoTransferido")).trim()));
                        String codigoTipo = String.valueOf(salida.get("tipo")).trim();
                        transferenciasPendientesVO[i].setTipo(codigoTipo);
                        transferenciasPendientesVO[i].setGlosaTipo(TablaValores.getValor(TABLA_TRANSFERENCIAS, codigoTipo, "Desc"));
                        String codigoEstado = String.valueOf(salida.get("estado")).trim();
                        transferenciasPendientesVO[i].setEstado(codigoEstado);
                        transferenciasPendientesVO[i].setGlosaEstado(TablaValores.getValor(TABLA_TRANSFERENCIAS, codigoEstado, "Desc"));
                        transferenciasPendientesVO[i].setFechaCreacion((Date)salida.get("fechaCreacion"));
                        transferenciasPendientesVO[i].setNumeroFirmas(Integer.parseInt(String.valueOf(salida.get("hayFirmas")).trim()));
                        transferenciasPendientesVO[i].setCantidadBloques(Integer.parseInt(String.valueOf(salida.get("numerosPagina")).trim()));
                        String hayCuentasNoInscritas = String.valueOf(salida.get("hayCuentasNoInscritas")).trim();
                        transferenciasPendientesVO[i].setTieneCuentasNoInscritas(hayCuentasNoInscritas.equalsIgnoreCase(HAY_CUENTAS_NO_INSCRITA));
                        }
                    }
                }
            if (getLogger().isEnabledFor(Level.INFO)){ 
                getLogger().info("[extraeTransferenciasPendientes][" + rutEmpresa + "][" + convenio + "][BCI_FINOK] transferenciasPendientesVO [" + (transferenciasPendientesVO != null ? transferenciasPendientesVO.length : 0) + "]");
            }
            return transferenciasPendientesVO;

        } catch (EjecutarException eE) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[extraeTransferenciasPendientes][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][EjecutarException eE][" + eE.getDetalleException() + "]");}
            throw new Exception(eE.getDetalleException());
        } catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[extraeTransferenciasPendientes][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][Exception e][" + ErroresUtil.extraeStackTrace(e) + "]");}
            throw new Exception(e.getMessage());
        }

    }


    /**
     * <p>M�todo que obtiene un listado de las transferencias pendientes asociadas a la carga de un archivo,
     * y consultando seg�n ciertos filtros.</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 19/10/2009 Alejandro Barra (SEnTRA): Versi�n inicial.
     * <li>1.1 23/06/2010 H�ctor Hern�ndez O. (SEnTRA): Se incorpora la obtenci�n de propiedades montoReparos y nombreArchivo desde la Base de Datos.
     * <li>1.2 05/08/2011 Pedro Carmona Escobar (SEnTRA): Se agrega seteo del atributo 'tieneCuentasNoInscritas' con el valor 'true' si el
     *                                                      objeto 'hayCuentasNoInscritas' dentro del HashMap es retornado con el valor '1'.
     *                                                      En caso contrario, se setear� el valor 'false'.
     *
     * </ul>
     *
     * @param filtro los filtros a aplicar a la consulta
     * @param bloque la p�gina a consultar
     * @param cantidadRegistros la cantidad de registros por cada p�gina
     * @throws Exception
     * @return Listado con las transferencias pendientes asociadas a la carga de un archivo.
     * @since 1.0
     */
    public EncabezadoTransferenciaVO[] extraeTransferenciasPendientesPorArchivo(FiltroConsultarTransferenciasVO filtro, int bloque, int cantidadRegistros) throws Exception{

        String rutEmpresa = filtro.getRutOrigen();
        String convenio = filtro.getConvenio();
        
        try{
            if(getLogger().isEnabledFor(Level.INFO)){
               getLogger().info("[extraeTransferenciasPendientesPorArchivo][" + rutEmpresa + "][" + convenio + "][BCI_INI]");
               getLogger().info("[extraeTransferenciasPendientesPorArchivo][" + rutEmpresa + "][" + convenio + "] bloque [" + bloque + "], cantidad de registros [" + cantidadRegistros + "], filtro [" + filtro.toString() + "]");
            }
            List documento = null;
            HashMap salida = null;
            HashMap parametros = new HashMap();
            EncabezadoTransferenciaVO[] transferenciasPendientesVO = null;
            parametros.put("CHAINED_SUPPORT","true");
            parametros.put("convenio", filtro.getConvenio());
            parametros.put("rutEmpresa", filtro.getRutOrigen());
            parametros.put("glosa", filtro.getGlosa());
            parametros.put("bloque", Integer.valueOf(String.valueOf(bloque)));
            parametros.put("cantidadRegistros", Integer.valueOf(String.valueOf(cantidadRegistros)));

            String contexto = TablaValores.getValor("JNDIConfig.parametros","cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
            documento = conector.consultar("banele", "extraeTransferenciasPendientesPorArchivo", parametros);

            if (documento != null){
                if(documento.size() > 0){
                    transferenciasPendientesVO = new EncabezadoTransferenciaVO[documento.size()];
                    if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[extraeTransferenciasPendientesPorArchivo][" + rutEmpresa + "][" + convenio + "] transferenciasPendientes [" + documento.size() + "]");}
                    for(int i = 0 ; i < documento.size() ; i++){
                        salida = (HashMap) documento.get(i);
                        transferenciasPendientesVO[i] = new EncabezadoTransferenciaVO();
                        transferenciasPendientesVO[i].setNumeroTransferencia(String.valueOf(salida.get("numeroOperacion")).trim());
                        transferenciasPendientesVO[i].setCantidadDetalles(Integer.parseInt(String.valueOf(salida.get("cantidadDetalles")).trim()));
                        transferenciasPendientesVO[i].setNombreTransferencia(String.valueOf(salida.get("nomTransferencia")).trim());
                        transferenciasPendientesVO[i].setNombreArchivo(String.valueOf(salida.get("nomArchivo")).trim());
                        transferenciasPendientesVO[i].setCuentaOrigen(StringUtil.sacaCeros(String.valueOf(salida.get("cuentaOrigen")).trim()));
                        transferenciasPendientesVO[i].setMonto(Long.parseLong(String.valueOf(salida.get("montoTransferido")).trim()));
                        String codigoTipo = String.valueOf(salida.get("tipo")).trim();
                        transferenciasPendientesVO[i].setTipo(codigoTipo);
                        transferenciasPendientesVO[i].setGlosaTipo(TablaValores.getValor(TABLA_TRANSFERENCIAS, codigoTipo, "Desc"));
                        String codigoEstado = String.valueOf(salida.get("estado")).trim();
                        transferenciasPendientesVO[i].setEstado(codigoEstado);
                        transferenciasPendientesVO[i].setGlosaEstado(TablaValores.getValor(TABLA_TRANSFERENCIAS, codigoEstado, "Desc"));
                        transferenciasPendientesVO[i].setFechaCreacion((Date)salida.get("fechaCreacion"));
                        transferenciasPendientesVO[i].setNumeroFirmas(Integer.parseInt(String.valueOf(salida.get("hayFirmas")).trim()));
                        transferenciasPendientesVO[i].setCantidadReparos(Integer.parseInt(String.valueOf(salida.get("cantidadReparos")).trim()));
                        transferenciasPendientesVO[i].setMontoReparos(Long.parseLong(String.valueOf(salida.get("montoReparos")).trim()));
                        transferenciasPendientesVO[i].setCantidadBloques(Integer.parseInt(String.valueOf(salida.get("numerosPagina")).trim()));
                        String hayCuentasNoInscritas = String.valueOf(salida.get("hayCuentasNoInscritas")).trim();
                        transferenciasPendientesVO[i].setTieneCuentasNoInscritas(hayCuentasNoInscritas.equalsIgnoreCase(HAY_CUENTAS_NO_INSCRITA));
                    }
                }
            }
            if (getLogger().isEnabledFor(Level.INFO)){ 
                getLogger().info("[extraeTransferenciasPendientesPorArchivo][" + rutEmpresa + "][" + convenio + "][BCI_FINOK] transferenciasPendientesVO [" + (transferenciasPendientesVO != null ? transferenciasPendientesVO.length : 0) + "]");
            }
            return transferenciasPendientesVO;

        } catch (EjecutarException eE) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[extraeTransferenciasPendientesPorArchivo][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][EjecutarException eE][" + eE.getDetalleException() + "]");}
            throw new Exception(eE.getDetalleException());
        } catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[extraeTransferenciasPendientesPorArchivo][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][Exception e][" + ErroresUtil.extraeStackTrace(e) + "]");}
            throw new Exception(e.getMessage());
        }

    }





    /**
     * <p>M�todo que obtiene un listado de las transferencias pendientes asociadas a un n�mero de transferencia.</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>2.0 02/08/2011 Pedro Carmona Escobar (SEnTRA): Versi�n inicial.
     *
     * </ul>
     *
     * @param filtro los filtros a aplicar a la consulta, este caso el n�mero de transferencia
     * @param bloque la p�gina a consultar
     * @param cantidadRegistros la cantidad de registros por cada p�gina
     * @throws Exception
     * @return Listado con las transferencias pendientes asociadas a la carga de un archivo.
     * @since 2.0
     */
    public EncabezadoTransferenciaVO[] extraeTransferenciasPendientesPorNumero(FiltroConsultarTransferenciasVO filtro, int bloque, int cantidadRegistros) throws Exception{

    	String rutEmpresa = filtro.getRutOrigen();
        String convenio = filtro.getConvenio();
        try{
            if(getLogger().isEnabledFor(Level.INFO)){
               getLogger().info("[extraeTransferenciasPendientesPorNumero][" + rutEmpresa + "][" + convenio + "][BCI_INI]");
               getLogger().info("[extraeTransferenciasPendientesPorNumero][" + rutEmpresa + "][" + convenio + "] bloque ["+ bloque +"], cantidad de Registros ["+ cantidadRegistros +"], filtro ["+ filtro.toString() +"]");
            }
            List documento = null;
            HashMap salida = null;
            HashMap parametros = new HashMap();
            EncabezadoTransferenciaVO[] transferenciasPendientesVO = null;
            parametros.put("CHAINED_SUPPORT","true");
            parametros.put("convenio", filtro.getConvenio());
            parametros.put("rutEmpresa", filtro.getRutOrigen());
            parametros.put("numTransferencia", filtro.getNumeroTransferencia());

            String contexto = TablaValores.getValor("JNDIConfig.parametros","cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
            documento = conector.consultar(BD_BANELE, TTFF_PENDIENTES_NUMERO, parametros);

            if (documento != null){
                if(documento.size() > 0){
                    transferenciasPendientesVO = new EncabezadoTransferenciaVO[documento.size()];
                    if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[extraeTransferenciasPendientesPorNumero][" + rutEmpresa + "][" + convenio + "] transferenciasPendientes [" + documento.size() + "]");}
                    for(int i = 0 ; i < documento.size() ; i++){
                        salida = (HashMap) documento.get(i);
                        transferenciasPendientesVO[i] = new EncabezadoTransferenciaVO();
                        transferenciasPendientesVO[i].setNumeroTransferencia(String.valueOf(salida.get("numeroOperacion")).trim());
                        transferenciasPendientesVO[i].setCantidadDetalles(Integer.parseInt(String.valueOf(salida.get("cantidadDetalles")).trim()));
                        transferenciasPendientesVO[i].setNombreTransferencia(String.valueOf(salida.get("nomTransferencia")).trim());
                        transferenciasPendientesVO[i].setNombreArchivo(String.valueOf(salida.get("nomArchivo")).trim());
                        transferenciasPendientesVO[i].setCuentaOrigen(StringUtil.sacaCeros(String.valueOf(salida.get("cuentaOrigen")).trim()));
                        transferenciasPendientesVO[i].setMonto(Long.parseLong(String.valueOf(salida.get("montoTransferido")).trim()));
                        String codigoTipo = String.valueOf(salida.get("tipo")).trim();
                        transferenciasPendientesVO[i].setTipo(codigoTipo);
                        transferenciasPendientesVO[i].setGlosaTipo(TablaValores.getValor(TABLA_TRANSFERENCIAS, codigoTipo, "Desc"));
                        String codigoEstado = String.valueOf(salida.get("estado")).trim();
                        transferenciasPendientesVO[i].setEstado(codigoEstado);
                        transferenciasPendientesVO[i].setGlosaEstado(TablaValores.getValor(TABLA_TRANSFERENCIAS, codigoEstado, "Desc"));
                        transferenciasPendientesVO[i].setFechaCreacion((Date)salida.get("fechaCreacion"));
                        transferenciasPendientesVO[i].setNumeroFirmas(Integer.parseInt(String.valueOf(salida.get("hayFirmas")).trim()));
                        transferenciasPendientesVO[i].setCantidadReparos(Integer.parseInt(String.valueOf(salida.get("cantidadReparos")).trim()));
                        transferenciasPendientesVO[i].setMontoReparos(Long.parseLong(String.valueOf(salida.get("montoReparos")).trim()));
                        transferenciasPendientesVO[i].setCantidadBloques(Integer.parseInt(String.valueOf(salida.get("numerosPagina")).trim()));
                        String hayCuentasNoInscritas = String.valueOf(salida.get("hayCuentasNoInscritas")).trim();
                        transferenciasPendientesVO[i].setTieneCuentasNoInscritas(hayCuentasNoInscritas.equalsIgnoreCase(HAY_CUENTAS_NO_INSCRITA));
                    }
                }
            }
            if (getLogger().isEnabledFor(Level.INFO)){ 
                getLogger().info("[extraeTransferenciasPendientesPorNumero][" + rutEmpresa + "][" + convenio + "][BCI_FINOK] transferenciasPendientesVO [" + (transferenciasPendientesVO != null ? transferenciasPendientesVO.length : 0) + "]");
            }
            return transferenciasPendientesVO;

        } catch (EjecutarException eE) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[extraeTransferenciasPendientesPorNumero][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][EjecutarException eE][" + eE.getDetalleException() + "]");}
            throw new Exception(eE.getDetalleException());
        } catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[extraeTransferenciasPendientesPorNumero][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][Exception e][" + ErroresUtil.extraeStackTrace(e) + "]");}
            throw new Exception(e.getMessage());
        }

    }


    /**
     * <p>M�todo que obtiene un listado con las transferencias que poseen cuentas no inscritas. Esta transferencias
     * ser�n retornadas con el correspondiente c�digo de activaci�n.</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 02/08/2011 Pedro Carmona Escobar (SEnTRA): Versi�n inicial.
     *
     * </ul>
     *
     * @param rut con la parte num�rica del rut de la empresa
     * @param bloque la p�gina a consultar
     * @param cantidadRegistros la cantidad de registros por cada p�gina
     * @throws Exception
     * @return Listado con las transferencias.
     * @since 2.0
     */
    public InformacionCodigoActivacionVO[] extraeCodigodDeActivacion(long rut, int bloque, int cantidadRegistros) throws Exception{

        try{
            if(getLogger().isEnabledFor(Level.INFO)){
               getLogger().info("[extraeCodigodDeActivacion][" + rut + "][BCI_INI]");
               getLogger().info("[extraeCodigodDeActivacion][" + rut + "] bloque [" + bloque + "], cantidad de Registros [" + cantidadRegistros + "]");
            }
            List documento = null;
            HashMap salida = null;
            HashMap parametros = new HashMap();
            InformacionCodigoActivacionVO[] informacionCodigoActivacionVO = null;
            parametros.put("CHAINED_SUPPORT","true");
            parametros.put("rutEmpresa", String.valueOf(rut));
            parametros.put("bloque", Integer.valueOf(String.valueOf(bloque)));
            parametros.put("cantidadRegistros", Integer.valueOf(String.valueOf(cantidadRegistros)));

            String contexto = TablaValores.getValor("JNDIConfig.parametros","cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
            documento = conector.consultar("banele", "extraeCodigosActivacion", parametros);

            if (documento != null){
                if(documento.size() > 0){
                    informacionCodigoActivacionVO = new InformacionCodigoActivacionVO[documento.size()];
                    if(getLogger().isEnabledFor(Level.INFO)){
                        getLogger().info("[extraeCodigodDeActivacion][" + rut + "] tamano documento [" + documento.size() + "], contenido documento [" + StringUtil.contenidoDe(documento) + "]");
                    }
                    for(int i = 0 ; i < documento.size() ; i++){
                        salida = (HashMap) documento.get(i);
                        informacionCodigoActivacionVO[i] = new InformacionCodigoActivacionVO();
                        informacionCodigoActivacionVO[i].setFechaCreacion((Date)salida.get("fechaCreacion"));
                        informacionCodigoActivacionVO[i].setConvenio(String.valueOf(salida.get("convenio")).trim());
                        informacionCodigoActivacionVO[i].setRut(Long.parseLong(String.valueOf(salida.get("rutUsuario")).trim()));
                        informacionCodigoActivacionVO[i].setDv(String.valueOf(salida.get("dvUsuario")).trim().charAt(0));
                        informacionCodigoActivacionVO[i].setNombreTransferencia(String.valueOf(salida.get("nomTransferencia")).trim());
                        informacionCodigoActivacionVO[i].setNombreArchivo(String.valueOf(salida.get("nomArchivo")).trim());
                        informacionCodigoActivacionVO[i].setNumTransferencia(String.valueOf(salida.get("numeroOperacion")).trim());
                        informacionCodigoActivacionVO[i].setMonto((((Double)salida.get("montoTransferido")).doubleValue()));
                        informacionCodigoActivacionVO[i].setCodActivacion(String.valueOf(salida.get("codigoAct")).trim());
                    }
                }
            }

            if (getLogger().isEnabledFor(Level.INFO)){ getLogger().info("[extraeCodigodDeActivacion][" + rut + "][BCI_FINOK]");}
            return informacionCodigoActivacionVO;

        } catch (EjecutarException eE) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[extraeCodigodDeActivacion][" + rut + "][BCI_FINEX][EjecutarException eE][" + eE.getDetalleException() + "]");}
            throw new Exception(eE.getDetalleException());
        } catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[extraeCodigodDeActivacion][" + rut + "][BCI_FINEX][Exception e][" + ErroresUtil.extraeStackTrace(e) + "]");}
            throw new Exception(e.getMessage());
        }

    }




    /**
     * <p>M�todo que obtiene un listado de las cuentas no inscritas que pertenencen a una
     * transferencia de una empresa.</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 02/08/2011 Pedro Carmona Escobar (SEnTRA): Versi�n inicial.
     *
     * </ul>
     *
     * @param idTransferencia con el id de la transferencia
     * @param convenio con el id del convenio de la empresa
     * @param rut con la parte num�rica del rut de la empresa
     * @throws Exception
     * @return Listado con las cuentas.
     * @since 2.0
     */
    public CuentasInscritasVO[] extraeCuentasNoInscritas(String idTransferencia, String convenio, long rutEmpresa) throws Exception{

        try{
            if(getLogger().isEnabledFor(Level.INFO)){
               getLogger().info("[extraeCuentasNoInscritas][" + idTransferencia + "][BCI_INI]");
               getLogger().info("[extraeCuentasNoInscritas][" + idTransferencia + "] rutEmpresa [" + rutEmpresa + "], convenio [" + convenio + "]");
            }
            List documento = null;
            HashMap salida = null;
            HashMap parametros = new HashMap();
            CuentasInscritasVO[] cuentasInscritasVO = null;
            parametros.put("CHAINED_SUPPORT","true");
            parametros.put("rutEmpresa", String.valueOf(rutEmpresa));
            parametros.put("convenio", convenio);
            parametros.put("numTransferencia", idTransferencia);

            String contexto = TablaValores.getValor("JNDIConfig.parametros","cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
            documento = conector.consultar("banele", "extraeCuentasNoInscritas", parametros);

            if (documento != null){
                if(documento.size() > 0){
                    cuentasInscritasVO = new CuentasInscritasVO[documento.size()];
                    if(getLogger().isEnabledFor(Level.INFO)){
                        getLogger().info("[extraeCuentasNoInscritas][" + idTransferencia + "] tamano documento [" + documento.size() + "], contenido documento [" + StringUtil.contenidoDe(documento) + "]");
                    }
                    for(int i = 0 ; i < documento.size() ; i++){
                        salida = (HashMap) documento.get(i);
                        cuentasInscritasVO[i] = new CuentasInscritasVO();

                        cuentasInscritasVO[i].setCuentaTercero(String.valueOf(salida.get("cuenta")).trim());
                        cuentasInscritasVO[i].setNombreBeneficiario(String.valueOf(salida.get("nombreBeneficiario")).trim());
                        cuentasInscritasVO[i].setRutBeneficiario(Long.parseLong(String.valueOf(salida.get("rutBeneficiario")).trim()));
                        cuentasInscritasVO[i].setDvBeneficiario(String.valueOf(salida.get("dvBeneficiario")).trim().charAt(0));
                        cuentasInscritasVO[i].setCodigoDelBanco(String.valueOf(salida.get("codBanco")).trim());
                        cuentasInscritasVO[i].setNombreDelBanco(String.valueOf(salida.get("banco")).trim());
                    }
                }
            }
            if (getLogger().isEnabledFor(Level.INFO)){ 
                getLogger().info("[extraeCuentasNoInscritas][" + idTransferencia + "][BCI_FINOK] cuentasInscritasVO [" + (cuentasInscritasVO != null ? cuentasInscritasVO.length : 0) + "]");
            }
            return cuentasInscritasVO;

        } catch (EjecutarException eE) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[extraeCuentasNoInscritas][" + idTransferencia + "][BCI_FINEX][EjecutarException eE][" + eE.getDetalleException() + "]");}
            throw new Exception(eE.getDetalleException());
        } catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[extraeCuentasNoInscritas][" + idTransferencia + "][BCI_FINEX][Exception e][" + ErroresUtil.extraeStackTrace(e) + "]");}
            throw new Exception(e.getMessage());
        }

    }




    /**
     * <p>M�todo que permite validar el c�digo de las cuentas no inscritas de una transferencia.</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 02/08/2011 Pedro Carmona Escobar (SEnTRA): Versi�n inicial.
     *
     * </ul>
     *
     * @param idTransferencia con el id de la transferencia
     * @param convenio con el id del convenio de la empresa
     * @param rut con la parte num�rica del rut de la empresa
     * @param codigo de activaci�n de cuentas
     * @throws Exception
     * @return Listado con las cuentas.
     * @since 2.0
     */
    public boolean validarCodigoActivacion(String idTransferencia, String convenio, long rutEmpresa, String codigo) throws Exception{

        try{
            if(getLogger().isEnabledFor(Level.INFO)){
                getLogger().info("[validarCodigoActivacion][" + idTransferencia + "][BCI_INI]");
                getLogger().info("[validarCodigoActivacion][" + idTransferencia + "] convenio [" + convenio + "], rutEmpresa [" + rutEmpresa + "], codigo [" + codigo + "]");
            }
            List documento = null;
            HashMap salida = null;
            HashMap parametros = new HashMap();
            CuentasInscritasVO[] cuentasInscritasVO = null;
            parametros.put("CHAINED_SUPPORT","true");
            parametros.put("rutEmpresa", String.valueOf(rutEmpresa));
            parametros.put("convenio", convenio);
            parametros.put("numTransferencia", idTransferencia);
            parametros.put("codigo", codigo);

            String contexto = TablaValores.getValor("JNDIConfig.parametros","cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
            documento = conector.consultar("banele", "validaCodigoActivacion", parametros);

            if (documento != null){
                if(documento.size() > 0){
                    salida = (HashMap) documento.get(0);
                    if(getLogger().isEnabledFor(Level.INFO)){
                       getLogger().info("[validarCodigoActivacion][" + idTransferencia + "][BCI_FINOK]");
                    }
                    return String.valueOf(salida.get("respuesta")).equalsIgnoreCase("1");
                }
            }
            if(getLogger().isEnabledFor(Level.ERROR)){
               getLogger().info("[validarCodigoActivacion][" + idTransferencia + "][BCI_FINEX]");
            }
            return false;

        } catch (EjecutarException eE) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[validarCodigoActivacion][" + idTransferencia + "][BCI_FINEX][EjecutarException eE][" + eE.getDetalleException() + "]");}
            throw new Exception(eE.getDetalleException());
        } catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[validarCodigoActivacion][" + idTransferencia + "][BCI_FINEX][Exception e][" + ErroresUtil.extraeStackTrace(e) + "]");}
            throw new Exception(e.getMessage());
        }

    }



    /**
     * <p>M�todo que permite validar el c�digo de las cuentas no inscritas de una transferencia.</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 02/08/2011 Pedro Carmona Escobar (SEnTRA): Versi�n inicial.
     *
     * </ul>
     *
     * @param rutEmpresa con la parte num�rica del rut de la empresa
     * @param numTrf con el id de la transferencia
     * @param convenio con el id del convenio de la empresa
     * @throws Exception
     * @return String con el c�digo de activacion para las cuentas no inscritas de la transferencia.
     * @since 2.0
     */
    public String obtenerCodigoActivacion(long rutEmpresa, String convenio, String numTrf) throws Exception{

        try{
            if(getLogger().isEnabledFor(Level.INFO)){
               getLogger().info("[obtenerCodigoActivacion][" + numTrf + "][BCI_INI]");
               getLogger().info("[obtenerCodigoActivacion][" + numTrf + "] rutEmpresa ["+ rutEmpresa +"], convenio ["+ convenio +"]");
            }
            List documento = null;
            HashMap salida = null;
            HashMap parametros = new HashMap();
            CuentasInscritasVO[] cuentasInscritasVO = null;
            parametros.put("CHAINED_SUPPORT","true");
            parametros.put("rutEmpresa", String.valueOf(rutEmpresa));
            parametros.put("convenio", convenio);
            parametros.put("numTransferencia", numTrf);

            String contexto = TablaValores.getValor("JNDIConfig.parametros","cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
            documento = conector.consultar("banele", "obtenerCodigoActivacion", parametros);

            if (documento != null){
                if(documento.size() > 0){
                    salida = (HashMap) documento.get(0);
                    if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[obtenerCodigoActivacion][" + numTrf + "][BCI_FINOK]");}
                    return String.valueOf(salida.get("codigo"));
                }
            }
            if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[obtenerCodigoActivacion][" + numTrf + "][BCI_FINEX]");}
            return null;
        } catch (EjecutarException eE) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[obtenerCodigoActivacion][" + numTrf + "][BCI_FINEX][EjecutarException eE][" + eE.getDetalleException() + "]");}
            throw new Exception(eE.getDetalleException());
        } catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[obtenerCodigoActivacion][" + numTrf + "][BCI_FINEX][Exception e][" + ErroresUtil.extraeStackTrace(e) + "]");}
            throw new Exception(e.getMessage());
        }

    }


    /**
     * <p>M�todo que obtiene los montos totales agrupados por cuenta para un grupo de transferencias
     * obtenidas seg�n ciertos criterios.</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 19/10/2009 Alejandro Barra (SEnTRA): Versi�n inicial.
     *
     * </ul>
     *
     * @param filtro los filtros a aplicar a la consulta
     * @throws Exception
     * @return Listado con los montos totales de ciertas trasferencias agrupados por cuenta.
     * @since 1.0
     */
    public TotalPorCuentaVO[] extraeTotalesPorCuentaDeTransferenciasPendientes(FiltroConsultarTransferenciasVO filtro) throws Exception{

    	String rutEmpresa = filtro.getRutOrigen();
        String convenio = filtro.getConvenio();
        
        try{
            if(getLogger().isEnabledFor(Level.INFO)){
               getLogger().info("[extraeTotalesPorCuentaDeTransferenciasPendientes][" + rutEmpresa + "][" + convenio + "][BCI_INI]");
               getLogger().info("[extraeTotalesPorCuentaDeTransferenciasPendientes][" + rutEmpresa + "][" + convenio + "] filtro ["+ filtro.toString() +"]");
            }
            List documento = null;
            HashMap salida = null;
            HashMap parametros = new HashMap();
            TotalPorCuentaVO[] totalesPorCuentaVO = null;
            parametros.put("CHAINED_SUPPORT","true");
            parametros.put("convenio", filtro.getConvenio());
            parametros.put("rutEmpresa", filtro.getRutOrigen());
            parametros.put("nroCuenta",  filtro.getCuentaCorriente());
            parametros.put("rutDestino", filtro.getRutDestino());
            parametros.put("fechaDesde", filtro.getFechaDesde());
            parametros.put("fechaHasta", filtro.getFechaHasta());
            parametros.put("glosa", filtro.getGlosa());
            parametros.put("estado", filtro.getEstado());
            parametros.put("numTransferencia", filtro.getNumeroTransferencia());

            String contexto = TablaValores.getValor("JNDIConfig.parametros","cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
            documento = conector.consultar("banele", "extraeTotalesPorCuentaDeTransferenciasPendientes", parametros);

            if (documento != null){
                if(documento.size() > 0){
                    totalesPorCuentaVO = new TotalPorCuentaVO[documento.size()];
                    if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[extraeTotalesPorCuentaDeTransferenciasPendientes][" + rutEmpresa + "][" + convenio + "] totalesPorCuenta [" + documento.size() + "]");}
                    for(int i = 0 ; i < documento.size() ; i++){
                        salida = (HashMap) documento.get(i);
                        totalesPorCuentaVO[i] = new TotalPorCuentaVO();
                        totalesPorCuentaVO[i].setNumeroCuenta(StringUtil.sacaCeros(String.valueOf(salida.get("numeroCuenta")).trim()));
                        totalesPorCuentaVO[i].setCantidadTransferencias(Integer.parseInt(String.valueOf(salida.get("cantidadTransferencias")).trim()));
                        totalesPorCuentaVO[i].setMonto(Long.parseLong(String.valueOf(salida.get("monto")).trim()));
                    }
                }
            }
            if (getLogger().isEnabledFor(Level.INFO)){ getLogger().info("[extraeTotalesPorCuentaDeTransferenciasPendientes][" + rutEmpresa + "][" + convenio + "][BCI_FINOK]");}
            return totalesPorCuentaVO;

        } catch (EjecutarException eE) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[extraeTotalesPorCuentaDeTransferenciasPendientes][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][EjecutarException eE][" + eE.getDetalleException() + "]");}
            throw new Exception(eE.getDetalleException());
        } catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[extraeTotalesPorCuentaDeTransferenciasPendientes][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][Exception e][" + ErroresUtil.extraeStackTrace(e) + "]");}
            throw new Exception(e.getMessage());
        }

    }

    /**
     * <p>M�todo que obtiene un listado de detalles de transferencias que en su carga han tenido reparos</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 19/10/2009 Alejandro Barra (SEnTRA): Versi�n inicial.
     * <li>1.1 05/01/2010 Marcelo Fuentes (SEnTRA): Se elimina la obtenci�n del nombre de banco desde la tabla
     *                    bancos.codigos ya que los nombres de los bancos se obtienen de una consulta a la CCA.</li>
     * <li>1.2 03/03/2010 Marcelo Fuentes (SEnTRA): Se agrega a la informaci�n obtenida para cada detalle los datos del
     *                    destinatario de la transferencia. Tambi�n se valida que el rut recuperado sea un n�mero para evitar
     *                    que se lance una excepci�n.
     *                    Adem�s se agrega la obtenci�n del motivo de rechazo de una transferencia</li>
     * <li>1.3 27/01/2011 Marcelo Fuentes (SEnTRA): Debido a cambio del tipo de dato de atributo numeroOperacion se cambia parseo de int a long</li>
     *
     * </ul>
     *
     * @param filtro los filtros a aplicar a la consulta
     * @param bloque la p�gina a consultar
     * @param cantidadRegistros la cantidad de registros por cada p�gina
     * @throws Exception
     * @return Listado con los detalles de las transferencias con reparos.
     * @since 1.0
     */
    public TransferenciasVO[] extraeDetallesTransferenciasPendientesConReparos(FiltroConsultarTransferenciasVO filtro, int bloque, int cantidadRegistros) throws Exception{

    	String rutEmpresa = filtro.getRutOrigen();
        String convenio = filtro.getConvenio();
        try{
            if(getLogger().isEnabledFor(Level.INFO)){
               getLogger().info("[extraeDetallesTransferenciasPendientesConReparos][" + rutEmpresa + "][" + convenio + "][BCI_INI]");
               getLogger().info("[extraeDetallesTransferenciasPendientesConReparos][" + rutEmpresa + "][" + convenio + "] bloque ["+ bloque +"], cantidad de Registros ["+ cantidadRegistros +"], filtro ["+ filtro.toString() +"]");
            }   
            List documento = null;
            HashMap salida = null;
            HashMap parametros = new HashMap();
            TransferenciasVO[] transferenciasPendientesVO = null;
            parametros.put("CHAINED_SUPPORT","true");
            parametros.put("convenio", filtro.getConvenio());
            parametros.put("rutEmpresa", filtro.getRutOrigen());
            parametros.put("glosa", filtro.getGlosa());
            parametros.put("bloque", Integer.valueOf(String.valueOf(bloque)));
            parametros.put("cantidadRegistros", Integer.valueOf(String.valueOf(cantidadRegistros)));

            String contexto = TablaValores.getValor("JNDIConfig.parametros","cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
            documento = conector.consultar("banele", "extraeDetallesTransferenciasPendientesConReparos", parametros);

            if (documento != null){
                if(documento.size() > 0){
                    transferenciasPendientesVO = new TransferenciasVO[documento.size()];
                    if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[extraeDetallesTransferenciasPendientesConReparos][" + rutEmpresa + "][" + convenio + "] transferenciasPendientes [" + documento.size() + "]");}
                    for(int i = 0 ; i < documento.size() ; i++){
                        salida = (HashMap) documento.get(i);
                        transferenciasPendientesVO[i] = new TransferenciasVO();
                        transferenciasPendientesVO[i].setNumeroOperacion(Long.parseLong(String.valueOf(salida.get("numeroOperacion")).trim()));
                        transferenciasPendientesVO[i].setNumeroDetalle(Integer.parseInt(String.valueOf(salida.get("numeroDetalle")).trim()));
                        transferenciasPendientesVO[i].setCuentaOrigen(StringUtil.sacaCeros(String.valueOf(salida.get("cuentaOrigen")).trim()));
                        transferenciasPendientesVO[i].setMonto(Long.parseLong(String.valueOf(salida.get("montoTransferido")).trim()));
                        transferenciasPendientesVO[i].setNombreDestinatario(String.valueOf(salida.get("nombreDestinatario")).trim());

                        String nombreDestinatario = String.valueOf(salida.get("nombreDestinatario")).trim();
                        transferenciasPendientesVO[i].setNombreDestinatario(nombreDestinatario);
                        transferenciasPendientesVO[i].setCuentaDestino(StringUtil.sacaCeros(String.valueOf(salida.get("cuentaDestino")).trim()));
                        String nombreCuentaDestino = String.valueOf(salida.get("nombreCuentaDestino")).trim();
                        if(nombreCuentaDestino!=null && !nombreCuentaDestino.equals("")){
                            transferenciasPendientesVO[i].setNombreCuentaDestino(nombreCuentaDestino);
                        }else{
                            transferenciasPendientesVO[i].setNombreCuentaDestino(nombreDestinatario);
                        }

                        String rut = String.valueOf(salida.get("rutDestino")).trim();

                        if(rut!=null && !rut.equals("")){
                            if(NumerosUtil.esNumero(rut)){
                                transferenciasPendientesVO[i].setRutDestino(Long.parseLong(rut));
                            }
                        }

                        String dv = String.valueOf(salida.get("dVDestino")).trim();
                        if(dv!=null && !dv.equals("")) transferenciasPendientesVO[i].setDvDestino(dv.charAt(0));

                        String codigoBancoDestino = String.valueOf(salida.get("bancoDestino")).trim();
                        if(codigoBancoDestino.length()>3){
                            codigoBancoDestino = codigoBancoDestino.substring(codigoBancoDestino.length()-3, codigoBancoDestino.length());
                        }
                        transferenciasPendientesVO[i].setBancoDestino(codigoBancoDestino);

                        String codigoEstado = String.valueOf(salida.get("estado")).trim();
                        transferenciasPendientesVO[i].setEstado(codigoEstado);
                        transferenciasPendientesVO[i].setGlosaEstado(String.valueOf(salida.get("motivoRechazo")).trim());

                        transferenciasPendientesVO[i].setCodigoEstadoError(String.valueOf(salida.get("codEstado")).trim());

                        transferenciasPendientesVO[i].setFechaCreacion((Date)salida.get("fechaCreacion"));
                        transferenciasPendientesVO[i].setFechaPago((Date)salida.get("fechaPago"));
                        transferenciasPendientesVO[i].setComentario(String.valueOf(salida.get("nomTransferencia")).trim());
                        transferenciasPendientesVO[i].setCantidadBloques(Integer.parseInt(String.valueOf(salida.get("numerosPagina")).trim()));
                        transferenciasPendientesVO[i].setCantidadTotaltransferencias(Integer.parseInt(String.valueOf(salida.get("cantidadTransferencias")).trim()));
                    }
                }
            }
            if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[extraeDetallesTransferenciasPendientesConReparos][" + rutEmpresa + "][" + convenio + "][BCI_FINOK]");}
            return transferenciasPendientesVO;

        } catch (EjecutarException eE) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[extraeDetallesTransferenciasPendientesConReparos][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][EjecutarException eE][" + eE.getDetalleException() + "]");}
            throw new Exception(eE.getDetalleException());
        } catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[extraeDetallesTransferenciasPendientesConReparos][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][Exception e][" + ErroresUtil.extraeStackTrace(e) + "]");}
            throw new Exception(e.getMessage());
        }

    }


    /**
     * <p>M�todo que obtiene un listado de las transferencias recibidas, consultando seg�n determinados criterios.</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 19/10/2009 Alejandro Barra (SEnTRA): Versi�n inicial.
     * <li>1.1 05/01/2010 Marcelo Fuentes (SEnTRA): Se elimina la obtenci�n del nombre de banco desde la tabla
     *                    bancos.codigos ya que los nombres de los bancos se obtienen de una consulta a la CCA.</li>
     * <li>1.2 21/04/2010 Marcelo Fuentes (SEnTRA): Se modifica el dato cuenta corriente destino en la consulta, agreg�ndole
     *                    un prefijo obtenido desde una tabla de par�metros al realizar dicha consulta y quit�ndole este
     *                    prefijo en el resultado. Esto es necesario ya que en la Base de Datos, que contiene una r�plica de la
     *                    informaci�n almacenada en una tabla en TANDEM, se guarda la cuenta con este prefijo el cual no debe
     *                    ser mostrado al cliente.</li>
     * <li>1.3 11/05/2010 Marcelo Fuentes (SEnTRA): Se modifica el m�todo cambiando la base de datos desde la cual se obtiene la
     *                    informaci�n. Esto se hace debido a que se cre� un proceso batch que obtiene la informaci�n de todas las
     *                    transferencias recibidas, tanto de otros bancos como bci, desde distintos repositorios y las reune en una
     *                    tabla en la base de datos banele. Adem�s se quita la extracci�n de datos que no son utilizados y que no ser�n
     *                    obtenidos desde el nuevo repositorio de la informaci�n.</li>
     * <li>1.4 27/01/2011 Marcelo Fuentes (SEnTRA): Debido a cambio del tipo de dato de atributo numeroOperacion se cambia parseo de int a long</li>
     *
     * </ul>
     *
     * @param filtro los filtros a aplicar a la consulta
     * @param bloque la p�gina a consultar
     * @param cantidadRegistros la cantidad de registros por cada p�gina
     * @throws Exception
     * @return Listado con las transferencias recibidas.
     * @since 1.0
     */
    public TransferenciasVO[] extraeTransferenciasRecibidas(FiltroConsultarTransferenciasVO filtro, int bloque, int cantidadRegistros) throws Exception{

        String rutEmpresa = filtro.getRutOrigen();
        String convenio = filtro.getConvenio();
        
        try{
            if(getLogger().isEnabledFor(Level.INFO)){
               getLogger().info("[extraeTransferenciasRecibidas][" + rutEmpresa + "][" + convenio + "][BCI_INI]");
               getLogger().info("[extraeTransferenciasRecibidas][" + rutEmpresa + "][" + convenio + "] bloque ["+ bloque +"], cantidad de Registros ["+ cantidadRegistros +"], filtro ["+ filtro +"]");
            }
            List documento = null;
            HashMap salida = null;
            HashMap parametros = new HashMap();
            TransferenciasVO[] transferenciasRecibidasVO = null;
            String inicioFijoCCT = TablaValores.getValor(TABLA_TTFF_TERCEROS, "producto", "CCT");
            String cuentaCorriente = "";

            if(filtro.getCuentaCorriente()!=null && !filtro.getCuentaCorriente().equals("")){
                cuentaCorriente = inicioFijoCCT + (StringUtil.rellenaPorLaIzquierda(filtro.getCuentaCorriente(), 8, '0'));
            }

            parametros.put("CHAINED_SUPPORT","true");
            parametros.put("rutCliente", filtro.getRutOrigen());
            parametros.put("nroCuenta",  cuentaCorriente);
            parametros.put("fechaDesde", filtro.getFechaDesde());
            parametros.put("fechaHasta", filtro.getFechaHasta());
            parametros.put("bloque", Integer.valueOf(String.valueOf(bloque)));
            parametros.put("cantidadRegistros", Integer.valueOf(String.valueOf(cantidadRegistros)));

            String contexto = TablaValores.getValor("JNDIConfig.parametros","cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
            documento = conector.consultar("banele", "extraeTransferenciasRecibidas", parametros);

            if (documento != null){
                if(documento.size() > 0){
                    transferenciasRecibidasVO = new TransferenciasVO[documento.size()];
                    if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[extraeTransferenciasRecibidas][" + rutEmpresa + "][" + convenio + "] transferenciasRecibidas [" + documento.size() + "]");}
                    for(int i = 0 ; i < documento.size() ; i++){
                        salida = (HashMap) documento.get(i);
                        transferenciasRecibidasVO[i] = new TransferenciasVO();
                        transferenciasRecibidasVO[i].setCuentaOrigen(StringUtil.sacaCeros(String.valueOf(salida.get("cuentaOrigen")).trim()));
                        transferenciasRecibidasVO[i].setTipoCuentaOrigen(String.valueOf(salida.get("tipoCuentaOrigen")).trim());
                        transferenciasRecibidasVO[i].setNombreOriginadorTransferencia(String.valueOf(salida.get("nombreOrigen")).trim());
                        transferenciasRecibidasVO[i].setRutOriginadorTransferencia(String.valueOf(salida.get("rutOrigen")).trim());
                        transferenciasRecibidasVO[i].setDvOriginadorTransferencia(String.valueOf(salida.get("dVOrigen")).trim());
                        transferenciasRecibidasVO[i].setMonto(Long.parseLong(String.valueOf(salida.get("montoTransferido")).trim()));
                        transferenciasRecibidasVO[i].setCuentaDestino(StringUtil.reemplazaUnaVez(String.valueOf(salida.get("cuentaDestino")).trim(),inicioFijoCCT,""));
                        String codigoBancoOrigen = String.valueOf(salida.get("bancoOrigen")).trim();
                        if(codigoBancoOrigen.length()>3){
                            codigoBancoOrigen = codigoBancoOrigen.substring(codigoBancoOrigen.length()-3, codigoBancoOrigen.length());
                        }
                        transferenciasRecibidasVO[i].setCodigoBancoOrigen(codigoBancoOrigen);
                        transferenciasRecibidasVO[i].setNumeroOperacion(Long.parseLong(String.valueOf(salida.get("numeroOperacion")).trim()));
                        transferenciasRecibidasVO[i].setFechaPago((Date)salida.get("fechaMovimiento"));
                        transferenciasRecibidasVO[i].setOperacion(String.valueOf(salida.get("operacion")).trim());
                        transferenciasRecibidasVO[i].setCantidadBloques(Integer.parseInt(String.valueOf(salida.get("numerosPagina")).trim()));
                        transferenciasRecibidasVO[i].setCantidadTotaltransferencias(Integer.parseInt(String.valueOf(salida.get("cantidadTransferencias")).trim()));
                    }
                }
            }
            if (getLogger().isEnabledFor(Level.INFO)){ getLogger().info("[extraeTransferenciasRecibidas][" + rutEmpresa + "][" + convenio + "][BCI_FINOK] transferenciasRecibidasVO [" + (transferenciasRecibidasVO != null ? transferenciasRecibidasVO.length : 0) + "]");}
            return transferenciasRecibidasVO;

        } catch (EjecutarException eE) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[extraeTransferenciasRecibidas][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][EjecutarException eE][" + eE.getDetalleException() + "]");}
            throw new Exception(eE.getDetalleException());
        } catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[extraeTransferenciasRecibidas][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][Exception e][" + ErroresUtil.extraeStackTrace(e) + "]");}

            throw new Exception(e.getMessage());
        }

    }

    /**
     * <p>M�todo que obtiene un listado de las transferencias recibidas desde Empresa BCI a Empresa BCI.</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.5 28/06/2011 Denisse Vel�squez Salazar (SEnTRA): Versi�n inicial.</li>
     *
     * </ul>
     *
     * @param rutEmpresa Rut Empresa
     * @param dvEmpresa D�gito Verificador Empresa
     * @param fechaProceso Fecha Proceso a consultar
     * @param cuentaDestino Cuenta destino de la Transferencia
     * @param bloque para paginaci�n, que p�gina se consulta
     * @param cantidadRegistros para paginaci�n, cuantos registros por p�gina
     * @return
     * @throws Exception
     *
     * @since 1.11
     */
    public TransferenciasRecibidasVO[] extraeTransfRecibidasEmpBCIEmpBCI(long rutEmpresa, char dvEmpresa, Date fechaProceso,
        String cuentaDestino, int bloque, int cantidadRegistros) throws Exception{
        try{
            if(getLogger().isEnabledFor(Level.INFO)){
               getLogger().info("[extraeTransfRecibidasEmpBCIEmpBCI][" + rutEmpresa + "][BCI_INI]");
               getLogger().info("[extraeTransfRecibidasEmpBCIEmpBCI][" + rutEmpresa + "] fechaProceso ["+ fechaProceso +"], cuentaDestino ["+ cuentaDestino +"], bloque ["+ bloque +"], cantidadRegistros ["+ cantidadRegistros +"]");
            }
            
            List documento = null;
            HashMap salida = null;
            HashMap parametros = new HashMap();
            Calendar fechaConsulta = Calendar.getInstance();
            TransferenciasRecibidasVO[] transferenciasRecibidasVO = null;
            String rutFormateado = StringUtil.rellenaPorLaIzquierda(String.valueOf(rutEmpresa), Integer.parseInt(TablaValores.getValor(ARCHIVO_PARAMETROS,"largoTransferenciasRecibidasBCI", "Desc")), '0');
            if(!(cuentaDestino==null || cuentaDestino.equals("") || cuentaDestino.equals("T"))){
                cuentaDestino = StringUtil.rellenaPorLaIzquierda(cuentaDestino, 8, '0');
            }
            fechaConsulta.setTime(fechaProceso);
            parametros.put("CHAINED_SUPPORT","true");
            parametros.put( "rutCliente",        rutFormateado);
            parametros.put( "dvCliente",         String.valueOf(dvEmpresa));
            parametros.put( "nroCuenta",         cuentaDestino);
            parametros.put( "fecha",             fechaConsulta);
            parametros.put("bloque", Integer.valueOf(String.valueOf(bloque)));
            parametros.put("cantidadRegistros", Integer.valueOf(String.valueOf(cantidadRegistros)));

            String contexto = TablaValores.getValor("JNDIConfig.parametros","cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
            documento = conector.consultar("banele", "extraeTransfRecibidasEmpBCI", parametros);

            if (documento != null){
                if(documento.size() > 0){
                    transferenciasRecibidasVO = new TransferenciasRecibidasVO[documento.size()];
                    if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[extraeTransfRecibidasEmpBCIEmpBCI][" + rutEmpresa + "] transferenciasRecibidas [" + documento.size() + "]");}
                    for(int i = 0 ; i < documento.size() ; i++){
                        transferenciasRecibidasVO[i] = new TransferenciasRecibidasVO();
                        salida = (HashMap) documento.get(i);
                        transferenciasRecibidasVO[i].setCuentaOrigen(StringUtil.sacaCeros(String.valueOf(salida.get("cuentaOrigen")).trim()));
                        transferenciasRecibidasVO[i].setTipoCuentaOrigen(String.valueOf(salida.get("tipoCuentaOrigen")).trim());
                        transferenciasRecibidasVO[i].setMonto(Long.parseLong(String.valueOf(salida.get("montoTransferido")).trim()));
                        transferenciasRecibidasVO[i].setCuentaDestino(String.valueOf(salida.get("cuentaDestino")).trim());
                        transferenciasRecibidasVO[i].setTipo(String.valueOf(salida.get("tipoCuentaDestino")).trim());
                        transferenciasRecibidasVO[i].setNombreDestinatario(String.valueOf(salida.get("nombreDestinatario")).trim());
                        transferenciasRecibidasVO[i].setRutOriginadorTransferencia(String.valueOf(salida.get("rutOriginador")).trim());
                        transferenciasRecibidasVO[i].setRutDestino(Long.parseLong(String.valueOf(salida.get("rutDestino")).trim()));
                        transferenciasRecibidasVO[i].setDvDestino(RUTUtil.calculaDigitoVerificador(String.valueOf(salida.get("rutDestino")).trim()));
                        transferenciasRecibidasVO[i].setBancoOrigen(BANCOS_DE_ORIGEN);
                        transferenciasRecibidasVO[i].setNumeroOperacion(Integer.parseInt(String.valueOf(salida.get("numeroOperacion")).trim()));
                        fechaConsulta = (Calendar)salida.get("fechaAbono");
                        transferenciasRecibidasVO[i].setFechaMovimiento(fechaConsulta.getTime());
                        transferenciasRecibidasVO[i].setComentario(String.valueOf(salida.get("comentarioTransferencia")).trim());
                        transferenciasRecibidasVO[i].setOperacion(String.valueOf(salida.get("operacion")).trim());
                        transferenciasRecibidasVO[i].setCantidadBloques(Integer.parseInt(String.valueOf(salida.get("numerosPagina")).trim()));
                    }
                }
            }
            if (getLogger().isEnabledFor(Level.INFO)){ getLogger().info("[extraeTransfRecibidasEmpBCIEmpBCI][" + rutEmpresa + "][BCI_FINOK] transferenciasRecibidasVO [" + (transferenciasRecibidasVO != null ? transferenciasRecibidasVO.length : 0) + "]");}
            return transferenciasRecibidasVO;

        } catch (EjecutarException eE) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[extraeTransfRecibidasEmpBCIEmpBCI][" + rutEmpresa + "][BCI_FINEX][EjecutarException eE][" + eE.getDetalleException() + "]");}

            throw new Exception(eE.getDetalleException());
        } catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[extraeTransfRecibidasEmpBCIEmpBCI][" + rutEmpresa + "][BCI_FINEX][Exception e][" + ErroresUtil.extraeStackTrace(e) + "]");}
            throw new Exception(e.getMessage());
        }

    }


    /**
     * <p>M�todo que obtiene los datos de un detalle de transferencia.</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 19/10/2009 Alejandro Barra (SEnTRA): Versi�n inicial.
     * <li>1.1 05/01/2010 Marcelo Fuentes (SEnTRA): Se elimina la obtenci�n del nombre de banco desde la tabla
     *                    bancos.codigos ya que los nombres de los bancos se obtienen de una consulta a la CCA.</li>
     * <li>1.2 01/02/2010 Marcelo Fuentes (SEnTRA): Se agrega obtenci�n de glosas de motivos de error para
     *                    transferencias que no se pudieron realizar.</li>
     * <li>1.3 28/05/2010 Denisse Vel�squez S. (SEnTRA): Se completan a 4 los c�digos de rechazo dada
     *                    inconsistencia de nomenclatura en producci�n.</li>
     * <li>1.4 08/06/2010 Denisse Vel�squez (SEnTRA): Se agrega seteo del n�mero de detalle en el objeto retornado.</li>
     * <li>1.5 07/02/2011 Marcelo Fuentes (SEnTRA): Se agrega seteo del traceNumber en el objeto retornado.</li>
     * <li>1.6 23/11/2011 Denisse Vel�squez (SEnTRA): Se rescatan los c�digos de error de la CCA, enviados por
     *                                                Pago de N�minas a OTB.</li>
     * <li> 1.7 25/03/2013, Sergio Cuevas D. (SEnTRA): Se agrego el seteo del campo "vuelveIntentar" para saber si 
     *                          se coloca en el excel si debe intentar de nuevo</li>
     * <li> 1.8 26/01/2015, Marcelo Fuentes H. (SEnTRA): Se agrega obtenci�n de campo "tipoCuentaDestino".</li>
     *
     * </ul>
     *
     * @param rutEmpresa rut de la empresa
     * @param convenio n�mero de convenio
     * @param numeroTransferencia n�mero de la transferencia a consultar
     * @param numeroDetalle n�mero de detalle de la transferencia a consultar
     * @throws Exception
     * @return VO con los datos del detalle de la transferecia.
     * @since 1.0
     */
    public DetalleTransferenciaVO extraeDetalleTransferencia(String rutEmpresa, String convenio,
        String numeroTransferencia, int numeroDetalle) throws Exception{

        try{
            if(getLogger().isEnabledFor(Level.INFO)){
               getLogger().info("[extraeDetalleTransferencia][" + numeroTransferencia + "][BCI_INI]");
               getLogger().info("[extraeDetalleTransferencia][" + numeroTransferencia + "] rutEmpresa ["+ rutEmpresa +"], convenio ["+ convenio +"], numeroDetalle ["+ numeroDetalle +"]");
            }
            List documento = null;
            HashMap salida = null;
            HashMap parametros = new HashMap();
            DetalleTransferenciaVO detalleTransferenciaVO = null;
            parametros.put("CHAINED_SUPPORT",     "true");
            parametros.put("rutEmpresa",          rutEmpresa);
            parametros.put("convenio",            convenio);
            parametros.put("numeroTransferencia", numeroTransferencia);
            parametros.put("numeroDetalleTrf",    Integer.valueOf(String.valueOf(numeroDetalle)));

            String contexto = TablaValores.getValor("JNDIConfig.parametros","cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
            documento = conector.consultar("banele", "extraeDetalleTransferencia", parametros);

            if (documento != null){
                String codigoRechazo = "";
                String glosaRechazo = "";
                String vuelveIntentar = "";

                if(documento.size() > 0){
                    detalleTransferenciaVO = new DetalleTransferenciaVO();
                    if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[extraeDetalleTransferencia][" + numeroTransferencia + "] detalle Transferencia [" + documento.size() + "]");} 
                    salida = (HashMap) documento.get(0);
                    detalleTransferenciaVO.setNumeroTransferencia(String.valueOf(
                        salida.get("numeroTransferencia")).trim());
                    detalleTransferenciaVO.setNumeroDetalle(String.valueOf(numeroDetalle));
                    String codigoEstado = String.valueOf(salida.get("estado")).trim();
                    detalleTransferenciaVO.setEstado(codigoEstado);
                    detalleTransferenciaVO.setGlosaEstado(TablaValores.getValor(TABLA_TRANSFERENCIAS, codigoEstado,
                        "Desc"));
                    String modalidad = String.valueOf(salida.get("modalidad")).trim();
                    detalleTransferenciaVO.setModalidad(modalidad);
                    detalleTransferenciaVO.setGlosaModalidad(TablaValores.getValor(TABLA_TRANSFERENCIAS,
                        "tipoModalidad", modalidad));
                    detalleTransferenciaVO.setTipoIngreso(String.valueOf(salida.get("modalidad")).trim());
                    detalleTransferenciaVO.setNombreArchivo(String.valueOf(salida.get("nombreArchivo")).trim());
                    detalleTransferenciaVO.setCuentaOrigen(StringUtil.sacaCeros(String.valueOf(
                        salida.get("cuentaOrigen")).trim()));
                    detalleTransferenciaVO.setMonto(Long.parseLong(String.valueOf(
                        salida.get("monto")).trim()));
                    String codigoTipo = String.valueOf(salida.get("tipo")).trim();
                    detalleTransferenciaVO.setTipo(codigoTipo);
                    String glosaTipo = TablaValores.getValor(TABLA_TRANSFERENCIAS, "tipoPagoTransferencia",
                        codigoTipo);
                    if(glosaTipo != null){
                        detalleTransferenciaVO.setGlosaTipo(glosaTipo);
                    }else{
                        detalleTransferenciaVO.setGlosaTipo("");
                    }

                    detalleTransferenciaVO.setCuentaDestino(StringUtil.sacaCeros(String.valueOf(
                        salida.get("cuentaDestino")).trim()));
                    detalleTransferenciaVO.setTipoCuentaDestino(String.valueOf(
                            salida.get("tipoCuentaDestino")).trim());

                    String codigoBancoDestino = String.valueOf(salida.get("bancoDestino")).trim();
                    if(codigoBancoDestino.length()>3){
                        codigoBancoDestino = codigoBancoDestino.substring(codigoBancoDestino.length()-3,
                            codigoBancoDestino.length());
                    }
                    detalleTransferenciaVO.setBancoDestino(codigoBancoDestino);
                    detalleTransferenciaVO.setNombreDestinatario(String.valueOf(
                        salida.get("nombreDestinatario")).trim());
                    detalleTransferenciaVO.setRutDestino(Long.parseLong(String.valueOf(
                        salida.get("rutDestino")).trim()));
                    detalleTransferenciaVO.setDvDestino(String.valueOf(salida.get("dVDestino")).trim().charAt(0));
                    detalleTransferenciaVO.setEmailDestino(String.valueOf(salida.get("emailDestino")).trim());
                    detalleTransferenciaVO.setMensajeEmailDestino(String.valueOf(
                        salida.get("mensajeEmailDestino")).trim());
                    detalleTransferenciaVO.setOrdenCompra(String.valueOf(salida.get("ordenCompra")).trim());
                    detalleTransferenciaVO.setNumeroFactura(String.valueOf(salida.get("numeroFactura")).trim());
                    detalleTransferenciaVO.setFechaCreacion((Date)salida.get("fechaCreacion"));
                    detalleTransferenciaVO.setFechaPago((Date)salida.get("fechaPago"));
                    detalleTransferenciaVO.setNombreUsuario(String.valueOf(salida.get("nombreUsuario")).trim());
                    codigoRechazo = String.valueOf(salida.get("rechazo")).trim();
                    detalleTransferenciaVO.setRechazo(codigoRechazo);
                    detalleTransferenciaVO.setMotivoRechazo(String.valueOf(salida.get("motivoRechazo")).trim());
                    if(!codigoRechazo.equals("")){
                        if(codigoRechazo.equals(ERROR_TIMEOUT)){
                            glosaRechazo = TablaValores.getValor(ARCH_PARAM_ERR, codigoRechazo, "Desc");
                        }
                        else{
                            if ("C".equals(String.valueOf(codigoRechazo.charAt(0)))){
                                codigoRechazo = codigoRechazo.substring(codigoRechazo.length()-3,
                                    codigoRechazo.length()).trim();
                                codigoRechazo = StringUtil.rellenaPorLaIzquierda(codigoRechazo, 4, '0');
                                glosaRechazo  = TablaValores.getValor(TABLA_TTFF_TERCEROS, codigoRechazo, "mensaje");
                                vuelveIntentar  = TablaValores.getValor(TABLA_TTFF_TERCEROS, 
                                    codigoRechazo, "vuelveIntentar");
                            } 
                            else {
                                codigoRechazo = StringUtil.rellenaPorLaIzquierda(codigoRechazo, 4, '0');
                                glosaRechazo  = TablaValores.getValor(TABLA_ERROR_TRF, codigoRechazo, "Desc");
                            }
                        }
                        if(getLogger().isEnabledFor(Level.INFO)){
                            getLogger().info("[extraeDetalleTransferencia][" + numeroTransferencia + "] codigoRechazo [" + codigoRechazo +"] , glosaRechazo [" + glosaRechazo +"]");
                        }
                        if(glosaRechazo != null){
                            detalleTransferenciaVO.setMotivoRechazo(glosaRechazo);
                        }
                        if(vuelveIntentar != null){
                            detalleTransferenciaVO.setVuelveIntentar(vuelveIntentar);
                        }
                    }
                    detalleTransferenciaVO.setTraceNumber(String.valueOf(salida.get("traceNumber")).trim());
                    if(getLogger().isEnabledFor(Level.INFO)){ getLogger().info("[extraeDetalleTransferencia][" + numeroTransferencia + "][BCI_FINOK]");}
                }
            }

            if(detalleTransferenciaVO == null){
                if(getLogger().isEnabledFor(Level.INFO)){ getLogger().info("[extraeDetalleTransferencia][" + numeroTransferencia + "][BCI_FINEX] detalleTransferencia es null");}
            }
            return detalleTransferenciaVO;

        } catch (EjecutarException eE) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[extraeDetalleTransferencia][" + numeroTransferencia + "][BCI_FINEX][EjecutarException eE][" + eE.getDetalleException() + "]");}
            throw new Exception(eE.getDetalleException());
        } catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[extraeDetalleTransferencia][" + numeroTransferencia + "][BCI_FINEX][Exception e][" + ErroresUtil.extraeStackTrace(e) + "]");}
            throw new Exception(e.getMessage());
        }

    }

    /**
     * <p>M�todo que consulta los eventos ocurridos sobre un conjunto de transferencias obtenidas
     * seg�n ciertos criterios de b�squeda.</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 19/10/2009 Alejandro Barra   (SEnTRA): Versi�n inicial.
     *
     * </ul>
     *
     * @param filtro los filtros a aplicar a la consulta
     * @throws Exception
     * @return Listado con los eventos realizados sobre las transferencias.
     * @since 1.0
     */
    public UsuarioTransferenciaVO[] consultarEventosParaTransferencias(FiltroConsultarTransferenciasVO filtro) throws Exception{

    	String rutEmpresa = filtro.getRutOrigen();
        String convenio = filtro.getConvenio();
        try{
            if(getLogger().isEnabledFor(Level.INFO)){
               getLogger().info("[consultarEventosParaTransferencias][" + rutEmpresa + "][" + convenio + "][BCI_INI]");
               getLogger().info("[consultarEventosParaTransferencias][" + rutEmpresa + "][" + convenio + "] filtro ["+ filtro.toString() +"]");
            }
            List documento = null;
            HashMap salida = null;
            HashMap parametros = new HashMap();
            UsuarioTransferenciaVO[] apoFirmadosVO = null;
            parametros.put("CHAINED_SUPPORT",  "true");
            parametros.put("convenio", filtro.getConvenio());
            parametros.put("rutEmpresa", filtro.getRutOrigen());
            parametros.put("nroCuenta",  filtro.getCuentaCorriente());
            parametros.put("rutDestino", filtro.getRutDestino());
            parametros.put("fechaDesde", filtro.getFechaDesde());
            parametros.put("fechaHasta", filtro.getFechaHasta());
            parametros.put("glosa", filtro.getGlosa());
            parametros.put("estado", filtro.getEstado());
            parametros.put("numTransferencia", filtro.getNumeroTransferencia());

            String contexto = TablaValores.getValor("JNDIConfig.parametros", "cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
            documento = conector.consultar("banele", "consultarEventosParaTransferencias", parametros);

            if (documento != null){
                if(documento.size() > 0){
                    apoFirmadosVO = new UsuarioTransferenciaVO[documento.size()];
                    if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[consultarEventosParaTransferencias][" + rutEmpresa + "][" + convenio + "] apoFirmadosVO [" + documento.size() + "]");}
                    for(int i = 0 ; i < documento.size() ; i++){
                        salida = (HashMap) documento.get(i);
                        apoFirmadosVO[i] = new UsuarioTransferenciaVO();
                        apoFirmadosVO[i].setNumeroTransferencia(String.valueOf(salida.get("numeroTransferencia")).trim());
                        apoFirmadosVO[i].setNombreUsuario(String.valueOf(salida.get("nombreUsuario")).trim());
                        String codEstado = String.valueOf(salida.get("estado")).trim();
                        apoFirmadosVO[i].setEstado(codEstado);
                        apoFirmadosVO[i].setGlosaEstado(TablaValores.getValor(TABLA_TRANSFERENCIAS, "estadoFirmaTransferencia", codEstado));
                        apoFirmadosVO[i].setFechaAccion((Date)salida.get("fechaAccion"));
                    }
                }
            }
            if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[consultarEventosParaTransferencias][" + rutEmpresa + "][" + convenio + "][BCI_FINOK]");}
            return apoFirmadosVO;

        } catch (EjecutarException eE) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[consultarEventosParaTransferencias][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][EjecutarException eE][" + eE.getDetalleException() + "]");}
            throw new Exception(eE.getDetalleException());
        } catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[consultarEventosParaTransferencias][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][Exception e][" + ErroresUtil.extraeStackTrace(e) + "]");}
            throw new Exception(e.getMessage());
        }

    }



    /**
     * <p>M�todo registra los eventos de la Transferencia.</p>
     *
     * Registro de versiones:<ul>
     *
     * <li> 1.0 05/08/2010, Denisse Vel�squez S. (SEnTRA): versi�n inicial.</li>
     *
     * </ul>
     *
     * @param rutUsuario rut del usuario que realiza el evento
     * @param dvUsuario d�gito verificador del usuario que realiza el evento
     * @param convenio c�digo del convenio
     * @param rutEmpresa rut de la empresa
     * @param dvEmpresa d�gito verificador de la empresa
     * @param numOperacion n�mero de la operaci�n
     * @param fechaAccion fecha en que se realiza el evento
     * @param tipoAccion c�digo del tipo de la operaci�n a registrar
     * @throws Exception
     * @since 1.6
     */
    public void registraEvento(String rutUsuario, String dvUsuario, String convenio, String rutEmpresa, String dvEmpresa,
        String numOperacion, Date fechaAccion, String tipoAccion) throws Exception{

        try{
            if(getLogger().isEnabledFor(Level.INFO)){
               getLogger().info("[registraEvento][" + rutEmpresa + "][" + convenio + "][BCI_INI]");
               getLogger().info("[registraEvento][" + rutEmpresa + "][" + convenio + "] rutUsuario ["+ rutUsuario +"] - ["+ dvUsuario +"], convenio ["+ convenio +"], numOperacion ["+ numOperacion +"], fechaAccion ["+ fechaAccion +"],tipoAccion ["+ tipoAccion +"]");
            }
            HashMap parametros = new HashMap();
            parametros.put("CHAINED_SUPPORT",  "true");
            parametros.put("rutUsuario",   rutUsuario);
            parametros.put("dvUsuario",    dvUsuario);
            parametros.put("convenio",     convenio);
            parametros.put("rutEmpresa",   rutEmpresa);
            parametros.put("dvEmpresa",    dvEmpresa);
            parametros.put("numOperacion", numOperacion);
            parametros.put("fechaAccion",  fechaAccion);
            parametros.put("tipoAccion",   tipoAccion);

            String contexto = TablaValores.getValor("JNDIConfig.parametros", "cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
            conector.consultar("banele", "insertaEventosParaTransferencias", parametros);
            if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[registraEvento][" + rutEmpresa + "][" + convenio + "][BCI_FINOK]");}
            return;

        } catch (EjecutarException eE) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[registraEvento][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][EjecutarException eE][" + eE.getDetalleException() + "]");}
            throw new Exception(eE.getDetalleException());
        } catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[registraEvento][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][Exception e][" + ErroresUtil.extraeStackTrace(e) + "]");}
            throw new Exception(e.getMessage());
        }

    }


    /**
     * <p>M�todo gen�rico para cambiar el valor, de la glosa elegida, en un detalle de transferencia</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 05/08/2010 H�ctor Hern�ndez Orrego (SEnTRA): Versi�n inicial.
     *
     * </ul>
     *
     * @param convenio n�mero de convenio
     * @param rutEmpresa rut de la empresa
     * @param numTransferencia n�mero de la transferencia
     * @param detalleTransferencia n�mero del detalle de la transferencia
     * @param identificadorGlosa numero identificador de la glosa que se desea modificar
     * @param detalleGlosa detalle a incluir en la glosa definida
     * @throws Exception
     * @return Respuesta de la modificaci�n.
     * @since 1.6
     */
    public boolean cambiaGlosaTransferencia(String convenio, String rutEmpresa, String numTransferencia, int detalleTransferencia,int identificadorGlosa, String detalleGlosa) throws Exception{

        try{
            if(getLogger().isEnabledFor(Level.INFO)){
               getLogger().info("[cambiaGlosaTransferencia][" + numTransferencia + "][BCI_INI]");
               getLogger().info("[cambiaGlosaTransferencia][" + numTransferencia + "] convenio [" + convenio + "], rutEmpresa [" + rutEmpresa + "], detalleTransferencia [" + detalleTransferencia + "],identificadorGlosa [" + identificadorGlosa + "], detalleGlosa [" + detalleGlosa + "]");
            }

            int respuesta = 0;
            String contexto = TablaValores.getValor("JNDIConfig.parametros","cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
            HashMap parametrosCuenta = new HashMap();
            parametrosCuenta.put("CHAINED_SUPPORT","true");
            parametrosCuenta.put("convenio",      convenio);
            parametrosCuenta.put("rutEmpresa",    rutEmpresa);
            parametrosCuenta.put("numTransferencia",     numTransferencia);
            parametrosCuenta.put("detalleTransferencia",     new Integer(detalleTransferencia));
            parametrosCuenta.put("identificadorGlosa",     new Integer(identificadorGlosa));
            parametrosCuenta.put("detalleGlosa",     detalleGlosa);

            respuesta = Integer.parseInt(String.valueOf( conector.ejecutar("banele","cambiaGlosaTransferencia", parametrosCuenta)).trim());
            if (respuesta==0) {
                if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[cambiaGlosaTransferencia][" + numTransferencia + "][BCI_FINOK] respuesta  [" + respuesta + "]");}
                return true;
            }else{
                if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[cambiaGlosaTransferencia][" + numTransferencia + "][BCI_FINEX] respuesta  [" + respuesta + "]");}
                return false;
            }

        } catch (EjecutarException eE) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[cambiaGlosaTransferencia][" + numTransferencia + "][BCI_FINEX][EjecutarException eE][" + eE.getDetalleException() + "]");}
            throw new Exception(eE.getDetalleException());
        } catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[cambiaGlosaTransferencia][" + numTransferencia + "][BCI_FINEX][Exception e][" + e.toString() + "]");}
            throw new Exception(e.getMessage());
        }

    }


    /**
     * <p>M�todo que obtiene un listado de los pagos realizados a trav�s de carga masiva,
     * y consultando seg�n ciertos filtros.</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 09/09/2010 Marcelo Fuentes H. (SEnTRA): Versi�n inicial.
     * <li>1.1 15/07/2013 Sergio Cuevas D (SEnTRA): se agrega parametro Estado a la consulta al SP
     *
     * </ul>
     *
     * @param filtro los filtros a aplicar a la consulta
     * @param bloque la p�gina a consultar
     * @param cantidadRegistros la cantidad de registros por cada p�gina
     * @throws Exception
     * @return Listado con los pagos realizados a trav�s de carga masiva.
     * @since 1.7
     */
    public EncabezadoTransferenciaVO[] extraePagosRealizadosViaCargaMasiva(FiltroConsultarTransferenciasVO filtro, int bloque, int cantidadRegistros) throws Exception{

    	String rutEmpresa = filtro.getRutOrigen();
        String convenio = filtro.getConvenio();
        
        try{
            if(getLogger().isEnabledFor(Level.INFO)){
                getLogger().info("[extraePagosRealizadosViaCargaMasiva][" + rutEmpresa + "][" + convenio + "][BCI_INI]");
                getLogger().info("[extraePagosRealizadosViaCargaMasiva][" + rutEmpresa + "][" + convenio + "] bloque [" + bloque + "], cantidadRegistros [" + cantidadRegistros + "], filtro [" + filtro.toString() + "]");
            }
            List documento = null;
            HashMap salida = null;
            HashMap parametros = new HashMap();
            EncabezadoTransferenciaVO[] transferenciasPendientesVO = null;
            parametros.put("CHAINED_SUPPORT","true");
            parametros.put("rutEmpresa", filtro.getRutOrigen());
            parametros.put("convenio", filtro.getConvenio());
            parametros.put("numTransferencia", filtro.getNumeroTransferencia());
            parametros.put("rutDestino", filtro.getRutDestino());
            parametros.put("fechaDesde", filtro.getFechaDesde());
            parametros.put("fechaHasta", filtro.getFechaHasta());
            parametros.put("bloque", Integer.valueOf(String.valueOf(bloque)));
            parametros.put("cantidadRegistros", Integer.valueOf(String.valueOf(cantidadRegistros)));
            parametros.put("estado", filtro.getEstado());

            String contexto = TablaValores.getValor("JNDIConfig.parametros","cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
            documento = conector.consultar("banele", "extraePagosRealizadosViaCargaMasiva", parametros);

            if (documento != null){
                if(documento.size() > 0){
                    transferenciasPendientesVO = new EncabezadoTransferenciaVO[documento.size()];
                    if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[extraePagosRealizadosViaCargaMasiva][" + rutEmpresa + "][" + convenio + "] pagosPendientes [" + documento.size() + "]");}
                    for(int i = 0 ; i < documento.size() ; i++){
                        salida = (HashMap) documento.get(i);
                        transferenciasPendientesVO[i] = new EncabezadoTransferenciaVO();
                        transferenciasPendientesVO[i].setNumeroTransferencia(String.valueOf(salida.get("numeroOperacion")).trim());
                        transferenciasPendientesVO[i].setCantidadDetalles(Integer.parseInt(String.valueOf(salida.get("cantidadDetalles")).trim()));
                        transferenciasPendientesVO[i].setNombreTransferencia(String.valueOf(salida.get("nomTransferencia")).trim());
                        transferenciasPendientesVO[i].setNombreArchivo(String.valueOf(salida.get("nomArchivo")).trim());
                        transferenciasPendientesVO[i].setCuentaOrigen(StringUtil.sacaCeros(String.valueOf(salida.get("cuentaOrigen")).trim()));
                        transferenciasPendientesVO[i].setMonto(Long.parseLong(String.valueOf(salida.get("montoTransferido")).trim()));
                        String codigoTipo = String.valueOf(salida.get("tipo")).trim();
                        transferenciasPendientesVO[i].setTipo(codigoTipo);
                        transferenciasPendientesVO[i].setGlosaTipo(TablaValores.getValor(TABLA_TRANSFERENCIAS, codigoTipo, "Desc"));
                        String codigoEstado = String.valueOf(salida.get("estado")).trim();
                        transferenciasPendientesVO[i].setEstado(codigoEstado);
                        transferenciasPendientesVO[i].setGlosaEstado(TablaValores.getValor(TABLA_TRANSFERENCIAS, codigoEstado, "Desc"));
                        transferenciasPendientesVO[i].setFechaCreacion((Date)salida.get("fechaPago"));
                        transferenciasPendientesVO[i].setNumeroFirmas(Integer.parseInt(String.valueOf(salida.get("hayFirmas")).trim()));
                        transferenciasPendientesVO[i].setCantidadBloques(Integer.parseInt(String.valueOf(salida.get("numerosPagina")).trim()));
                    }
                }
            }
            if (getLogger().isEnabledFor(Level.INFO)){ getLogger().info("[extraePagosRealizadosViaCargaMasiva][" + rutEmpresa + "][" + convenio + "][BCI_FINOK] transferenciasPendientesVO [" + (transferenciasPendientesVO != null ? transferenciasPendientesVO.length : 0) + "]");}
            return transferenciasPendientesVO;

        } catch (EjecutarException eE) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[extraePagosRealizadosViaCargaMasiva][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][EjecutarException eE][" + eE.getDetalleException() + "]");}
            throw new Exception(eE.getDetalleException());
        } catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[extraePagosRealizadosViaCargaMasiva][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][Exception e][" + ErroresUtil.extraeStackTrace(e) + "]");}
            throw new Exception(e.getMessage());
        }

    }

    /**
     * <p>M�todo que obtiene un listado de las transferencias pendientes por resolver.</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 23/08/2011 Freddy Monsalve - Freddy Painenao (Os.One): Versi�n inicial.
     * </ul>
     *
     * @param rutEmpresa rut de la empresa
     * @param filtro: VO con los parametros a filtrar
     * @throws Exception
     * @return Listado con los detalles de la transferencia pendientes por resolver.
     * @since 2.1
     */
    public TransferenciasVO[] extraeTransferenciasPendientesPorResolver(FiltroConsultarTransferenciasVO filtro) throws Exception{

    	String rutEmpresa = filtro.getRutOrigen();
        String convenio = filtro.getConvenio();
        
        try{
            if(getLogger().isEnabledFor(Level.INFO)){
               getLogger().info("[extraeTransferenciasPendientesPorResolver][" + rutEmpresa + "][" + convenio + "][BCI_INI]");
               getLogger().info("[extraeTransferenciasPendientesPorResolver][" + rutEmpresa + "][" + convenio + "] filtro ["+ filtro.toString() +"]");
            }
            List documento = null;
            HashMap salida = null;
            HashMap parametros = new HashMap();
            TransferenciasVO[] transferenciasPendientesVO = null;
            Date fechaConsulta = null ;
            parametros.put("CHAINED_SUPPORT",     "true");
            parametros.put("rutEmpresa",          filtro.getRutOrigen());
            parametros.put("convenio",            filtro.getConvenio());

            String contexto = TablaValores.getValor("JNDIConfig.parametros","cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
            documento = conector.consultar("banele", "extraeTransferenciasPendientesPorResolver", parametros);

            if (documento != null){
                if(documento.size() > 0){
                    transferenciasPendientesVO = new TransferenciasVO[documento.size()];
                    if(getLogger().isEnabledFor(Level.INFO)){ getLogger().info("[extraeTransferenciasPendientesPorResolver][" + rutEmpresa + "][" + convenio + "] detallesTransferenciaPendiente [" + documento.size() + "]");}
                    for(int i = 0 ; i < documento.size() ; i++){
                        salida = (HashMap) documento.get(i);
                        transferenciasPendientesVO[i] = new TransferenciasVO();
                        transferenciasPendientesVO[i].setNumeroOperacion(Long.parseLong(String.valueOf(salida.get("numeroOperacion")).trim()));
                        transferenciasPendientesVO[i].setCuentaOrigen(StringUtil.sacaCeros(String.valueOf(salida.get("cuentaOrigen")).trim()));

                        String modalidad = String.valueOf(salida.get("modalidad")).trim();
                        transferenciasPendientesVO[i].setModalidad(modalidad);
                        transferenciasPendientesVO[i].setGlosaModalidad(TablaValores.getValor(TABLA_TRANSFERENCIAS, "tipoModalidad", modalidad));
                        transferenciasPendientesVO[i].setTipoIngreso(String.valueOf(salida.get("modalidad")).trim());

                        transferenciasPendientesVO[i].setMonto(Long.parseLong(String.valueOf(salida.get("montoTransferido")).trim()));
                        fechaConsulta = (Date)salida.get("fechaCreacion");
                        transferenciasPendientesVO[i].setFechaCreacion(fechaConsulta);
                        transferenciasPendientesVO[i].setComentario(String.valueOf(salida.get("nomTransferencia")).trim());

                    }
                }
            }
            if (getLogger().isEnabledFor(Level.INFO)){getLogger().info("[extraeTransferenciasPendientesPorResolver][" + rutEmpresa + "][" + convenio + "][BCI_FINOK] transferenciasPendientesVO [" + (transferenciasPendientesVO != null ? transferenciasPendientesVO.length : 0) + "]");}
            return transferenciasPendientesVO;

        } catch (EjecutarException eE) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[extraeTransferenciasPendientesPorResolver][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][EjecutarException eE][" + eE.getDetalleException() + "]");}
            throw new Exception(eE.getDetalleException());
        } catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[extraeTransferenciasPendientesPorResolver][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][Exception e][" + ErroresUtil.extraeStackTrace(e) + "]");}
            throw new Exception(e.getMessage());
        }    
    }   

    /**
     * <p>M�todo para obtener el tracenumber para una transferencia</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 17/10/2011 Mauricio Retamal (SEnTRA): Versi�n inicial.
     *
     * </ul>
     * 
     * @param codigoTabla codigo de la tabla a obtener tracenumber
     * @throws Exception
     * @return traceNumber indentificador unico de una transferencia.
     * @since 2.2
     */
    public String obtenerTraceNumber(String codigoTabla) throws Exception{

        try{
            if(getLogger().isEnabledFor(Level.INFO)){
               getLogger().info("[obtenerTraceNumber][" + codigoTabla + "][BCI_INI]");
            }
            HashMap parametros = new HashMap();
            String traceNumber = null;
            parametros.put("CHAINED_SUPPORT","true");
            parametros.put("codigo",codigoTabla);
            String contexto = TablaValores.getValor("JNDIConfig.parametros","cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);

            traceNumber = conector.ejecutar("ctaster", "obtenerTraceNumber", parametros).toString();
            if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[obtenerTraceNumber][" + codigoTabla + "][BCI_FINOK] respuesta[" + traceNumber + "]");}
            return traceNumber;

        } catch (EjecutarException eE) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[obtenerTraceNumber][" + codigoTabla + "][BCI_FINEX][EjecutarException eE][" + eE.getDetalleException() + "]");}
            throw new Exception(eE.getDetalleException());
        } catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[obtenerTraceNumber][" + codigoTabla + "][BCI_FINEX][Exception e][" + ErroresUtil.extraeStackTrace(e) + "]");}
            throw new Exception(e.getMessage());
        }
    }   

    /**
     * <p>M�todo que obtiene un listado de las transferencias a terceros que se
     * encuentran en estado de pendientes por resolver.</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 16/01/2012 Iv�n L�pez B. (ADA Ltda.): Versi�n inicial.
     * </ul>
     *
     * @param rut con el rut de la empresa.
     * @param convenio con el c�digo del convenio a consultar.
     * @param bloque con la p�gina a desplegar.
     * @param cantidadRegistros la cantidad de registros a mostrar por p�gina.
     * @return objeto con el listado correspondiente.
     * @throws ExcepcionGeneral Cuando existe un error se lanza esta excepci�n.
     * @since 2.4
     */
    public EncabezadoTransferenciaVO[]
        extraeTransferenciasPendientesATerceros(long rut, String convenio,
            int bloque, int cantidadRegistros)
                throws ExcepcionGeneral {

        if(getLogger().isEnabledFor(Level.INFO)){
           getLogger().info("[extraeTransferenciasPendientesATerceros][" + rut + "][" + convenio + "][BCI_INI]");
           getLogger().info("[extraeTransferenciasPendientesATerceros][" + rut + "][" + convenio + "] bloque ["+ bloque +"], cantidadRegistros ["+ cantidadRegistros +"]");
        }
        EncabezadoTransferenciaVO[] listadoTransferenciasPendientes;
        HashMap parametros = new HashMap();
        String rutEmpresa = StringUtil.rellenaConCeros(String.valueOf(rut), LARGO_RUT);
        parametros.put("rutEmpresa", rutEmpresa);
        parametros.put("convenio", convenio);
        parametros.put("bloque", new Integer(bloque));
        parametros.put("cantidadRegistros", new Integer(cantidadRegistros));
        parametros.put("CHAINED_SUPPORT", "true");

        try {
            String contexto = TablaValores.getValor("JNDIConfig.parametros", "cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
            List lista = conector.consultar("banele", "obtenerTransferenciasPendientesATerceros", parametros);

            if (lista != null && lista.size() > 0) {
                HashMap fila = new HashMap();
                listadoTransferenciasPendientes = new EncabezadoTransferenciaVO[lista.size()];
                for (int i = 0; i < lista.size(); i++) {
                    fila = (HashMap) lista.get(i);
                    listadoTransferenciasPendientes[i] = new EncabezadoTransferenciaVO();
                    if (fila.get("numeroTransferencia") != null) {
                        listadoTransferenciasPendientes[i].setNumeroTransferencia(String.valueOf(
                            fila.get("numeroTransferencia")));
                    }
                    else {
                        listadoTransferenciasPendientes[i].setNumeroTransferencia("-");
                    }
                    if (fila.get("nombreTransferencia") != null) {
                        listadoTransferenciasPendientes[i].setNombreTransferencia(String.valueOf(
                            fila.get("nombreTransferencia")));
                    }
                    else {
                        listadoTransferenciasPendientes[i].setNombreTransferencia("-");
                    }
                    if (fila.get("cuentaOrigen") != null) {
                        listadoTransferenciasPendientes[i].setCuentaOrigen(String.valueOf(
                            fila.get("cuentaOrigen")));
                    }
                    else {
                        listadoTransferenciasPendientes[i].setCuentaOrigen("-");
                    }
                    if (fila.get("cuentaDestino") != null) {
                        listadoTransferenciasPendientes[i].setCuentaDestino(String.valueOf(
                            fila.get("cuentaDestino")));
                    }
                    else {
                        listadoTransferenciasPendientes[i].setCuentaDestino("-");
                    }
                    if (fila.get("nombreDestinatario") != null) {
                        listadoTransferenciasPendientes[i].setNombreDestinatario(String.valueOf(
                            fila.get("nombreDestinatario")).trim());
                    }
                    else {
                        listadoTransferenciasPendientes[i].setNombreDestinatario("");
                    }
                    if (fila.get("monto") != null) {
                        listadoTransferenciasPendientes[i].setMonto(Double.parseDouble(String.valueOf(
                            fila.get("monto"))));
                    }
                    else {
                        listadoTransferenciasPendientes[i].setMonto(0.0);
                    }
                    if (fila.get("paginaActual") != null) {
                        listadoTransferenciasPendientes[i].setPaginaActual(Integer.parseInt(String.valueOf(
                            fila.get("paginaActual"))));
                    }
                    else {
                        listadoTransferenciasPendientes[i].setPaginaActual(0);
                    }
                    if (fila.get("totalPaginas") != null) {
                        listadoTransferenciasPendientes[i].setCantidadBloques(Integer.parseInt(String.valueOf(
                            fila.get("totalPaginas"))));
                    }
                    else {
                        listadoTransferenciasPendientes[i].setCantidadBloques(0);
                    }
                    if (fila.get("registroInicial") != null) {
                        listadoTransferenciasPendientes[i].setRegistroInicial(Integer.parseInt(String.valueOf(
                            fila.get("registroInicial"))));
                    }
                    else {
                        listadoTransferenciasPendientes[i].setRegistroInicial(0);
                    }
                    if (fila.get("registroFinal") != null) {
                        listadoTransferenciasPendientes[i].setRegistroFinal(Integer.parseInt(String.valueOf(
                            fila.get("registroFinal"))));
                    }
                    else {
                        listadoTransferenciasPendientes[i].setRegistroFinal(0);
                    }
                    if (fila.get("totalRegistros") != null) {
                        listadoTransferenciasPendientes[i].setTotalRegistros(Integer.parseInt(String.valueOf(
                            fila.get("totalRegistros"))));
                    }
                    else {
                        listadoTransferenciasPendientes[i].setTotalRegistros(0);
                    }
                    if (fila.get("tipoTransferencia") != null) {
                        listadoTransferenciasPendientes[i].setTipo(String.valueOf(
                            fila.get("tipoTransferencia")));
                    }
                    else {
                        listadoTransferenciasPendientes[i].setTipo("-");
                    }
                }
            }
            else {
                if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[extraeTransferenciasPendientesATerceros][" + rut + "][" + convenio + "][BCI_FINEX] listadoTransferenciasPendientes es null");}
                listadoTransferenciasPendientes = null;
            }
        }
        catch(EjecutarException ee) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[extraeTransferenciasPendientesATerceros][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][EjecutarException ee]" + ErroresUtil.extraeStackTrace(ee));}
            throw new ExcepcionGeneral("Ha ocurrido una EjecutarException: ", ErroresUtil.extraeStackTrace(ee) );
        }
        catch(ConfiguracionException ce) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[extraeTransferenciasPendientesATerceros][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][ConfiguracionException ce]"+ ErroresUtil.extraeStackTrace(ce));}
            throw new ExcepcionGeneral("Ha ocurrido una ConfiguracionException: ", ErroresUtil.extraeStackTrace(ce) );
        }
        catch(ServicioDatosException sde) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[extraeTransferenciasPendientesATerceros][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][ServicioDatosException sde]"+ ErroresUtil.extraeStackTrace(sde));}
            throw new ExcepcionGeneral("Ha ocurrido una ServicioDatosException: ", ErroresUtil.extraeStackTrace(sde) );
        }
        if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[extraeTransferenciasPendientesATerceros][" + rut + "][" + convenio + "][BCI_FINOK]");}
        return listadoTransferenciasPendientes;
    }

    /**
     * <p>M�todo que obtiene un listado de las transferencias entre productos
     * contratados que se encuentran en estado de pendientes por resolver.</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 16/01/2012 Iv�n L�pez B. (ADA Ltda.): Versi�n inicial.
     * </ul>
     *
     * @param rut con el rut de la empresa.
     * @param convenio con el c�digo del convenio a consultar.
     * @param bloque con la p�gina a desplegar.
     * @param cantidadRegistros la cantidad de registros a mostrar por p�gina.
     * @return objeto EPC con el listado correspondiente.
     * @throws ExcepcionGeneral Cuando existe un error se lanza esta excepci�n.
     * @since 2.4
     */
    public EncabezadoTransferenciaVO[]
        extraeTransferenciasPendientesEPC(long rut, String convenio,
            int bloque, int cantidadRegistros)
                throws ExcepcionGeneral {

         if(getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[extraeTransferenciasPendientesEPC][" + rut + "][" + convenio + "][BCI_INI]");
            getLogger().info("[extraeTransferenciasPendientesEPC][" + rut + "][" + convenio + "] bloque ["+ bloque +"] , cantidadRegistros ["+ cantidadRegistros +"]");
         }
        EncabezadoTransferenciaVO[] listadoTransferenciasPendientes;
        HashMap parametros = new HashMap();
        String rutEmpresa = StringUtil.rellenaConCeros(String.valueOf(rut), LARGO_RUT);
        parametros.put("rutEmpresa", rutEmpresa);
        parametros.put("convenio", convenio);
        parametros.put("bloque", new Integer(bloque));
        parametros.put("cantidadRegistros", new Integer(cantidadRegistros));
        parametros.put("CHAINED_SUPPORT", "true");

        try {
            String contexto = TablaValores.getValor("JNDIConfig.parametros", "cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
            List lista = conector.consultar("banele", "obtenerTransferenciasPendientesEPC", parametros);

            if (lista != null && lista.size() > 0) {
                HashMap fila = new HashMap();
                listadoTransferenciasPendientes = new EncabezadoTransferenciaVO[lista.size()];
                for (int i = 0; i < lista.size(); i++) {
                    fila = (HashMap) lista.get(i);
                    listadoTransferenciasPendientes[i] = new EncabezadoTransferenciaVO();
                    if (fila.get("numeroTransferencia") != null) {
                        listadoTransferenciasPendientes[i].setNumeroTransferencia(
                            String.valueOf(
                                fila.get("numeroTransferencia")));
                    }
                    else {
                        listadoTransferenciasPendientes[i].setNumeroTransferencia("-");
                    }
                    if (fila.get("nombreTransferencia") != null) {
                        listadoTransferenciasPendientes[i].setNombreTransferencia(
                            String.valueOf(
                                fila.get("nombreTransferencia")));
                    }
                    else {
                        listadoTransferenciasPendientes[i].setNombreTransferencia("-");
                    }
                    if (fila.get("cuentaOrigen") != null) {
                        listadoTransferenciasPendientes[i].setCuentaOrigen(
                            String.valueOf(
                                fila.get("cuentaOrigen")));
                    }
                    else {
                        listadoTransferenciasPendientes[i].setCuentaOrigen("-");
                    }
                    if (fila.get("cuentaDestino") != null) {
                        listadoTransferenciasPendientes[i].setCuentaDestino(
                            String.valueOf(
                                fila.get("cuentaDestino")));
                    }
                    else {
                        listadoTransferenciasPendientes[i].setCuentaDestino("-");
                    }
                    if (fila.get("nombreDestinatario") != null) {
                        listadoTransferenciasPendientes[i].setNombreDestinatario(
                            String.valueOf(
                                fila.get("nombreDestinatario")).trim());
                    }
                    else {
                        listadoTransferenciasPendientes[i].setNombreDestinatario("");
                    }
                    if (fila.get("monto") != null) {
                        listadoTransferenciasPendientes[i].setMonto(
                            Double.parseDouble(
                                String.valueOf(
                                    fila.get("monto"))));
                    }
                    else {
                        listadoTransferenciasPendientes[i].setMonto(0.0);
                    }
                    if (fila.get("paginaActual") != null) {
                        listadoTransferenciasPendientes[i].setPaginaActual(
                            Integer.parseInt(
                                String.valueOf(
                                    fila.get("paginaActual"))));
                    }
                    else {
                        listadoTransferenciasPendientes[i].setPaginaActual(0);
                    }
                    if (fila.get("totalPaginas") != null) {
                        listadoTransferenciasPendientes[i].setCantidadBloques(
                            Integer.parseInt(
                                String.valueOf(
                                    fila.get("totalPaginas"))));
                    }
                    else {
                        listadoTransferenciasPendientes[i].setCantidadBloques(0);
                    }
                    if (fila.get("registroInicial") != null) {
                        listadoTransferenciasPendientes[i].setRegistroInicial(
                            Integer.parseInt(
                                String.valueOf(
                                    fila.get("registroInicial"))));
                    }
                    else {
                        listadoTransferenciasPendientes[i].setRegistroInicial(0);
                    }
                    if (fila.get("registroFinal") != null) {
                        listadoTransferenciasPendientes[i].setRegistroFinal(
                            Integer.parseInt(
                                String.valueOf(
                                    fila.get("registroFinal"))));
                    }
                    else {
                        listadoTransferenciasPendientes[i].setRegistroFinal(0);
                    }
                    if (fila.get("totalRegistros") != null) {
                        listadoTransferenciasPendientes[i].setTotalRegistros(
                            Integer.parseInt(
                                String.valueOf(
                                    fila.get("totalRegistros"))));
                    }
                    else {
                        listadoTransferenciasPendientes[i].setTotalRegistros(0);
                    }

                    if (fila.get("idDetalle") != null) {
                        listadoTransferenciasPendientes[i].setIdDetalle(
                            Integer.parseInt(
                                String.valueOf(
                                    fila.get("idDetalle"))));
                    }
                    else {
                        listadoTransferenciasPendientes[i].setIdDetalle(0);
                    }
                    if (fila.get("tipoTransferencia") != null) {
                        listadoTransferenciasPendientes[i].setTipo(
                            String.valueOf(
                                fila.get("tipoTransferencia")));
                    }
                    else {
                        listadoTransferenciasPendientes[i].setTipo("-");
                    }
                }
            }
            else {
                if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[extraeTransferenciasPendientesEPC][" + rut + "][" + convenio + "][BCI_FINEX] listadoTransferenciasPendientes es null");}
                listadoTransferenciasPendientes = null;
            }
        }
        catch(EjecutarException ee) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[extraeTransferenciasPendientesEPC][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][EjecutarException ee]" + ErroresUtil.extraeStackTrace(ee));}
            throw new ExcepcionGeneral("Ha ocurrido una EjecutarException: ",ErroresUtil.extraeStackTrace(ee) );
        }
        catch(ConfiguracionException ce) {
          if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[extraeTransferenciasPendientesEPC][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][ConfiguracionException ce]"+ ErroresUtil.extraeStackTrace(ce));}
            throw new ExcepcionGeneral("Ha ocurrido una ConfiguracionException: ",ErroresUtil.extraeStackTrace(ce) );
        }
        catch(ServicioDatosException sde) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[extraeTransferenciasPendientesEPC][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][ServicioDatosException sde]" + ErroresUtil.extraeStackTrace(sde));}
            throw new ExcepcionGeneral("Ha ocurrido una ServicioDatosException: ",ErroresUtil.extraeStackTrace(sde) );
        }
        if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[extraeTransferenciasPendientesEPC][" + rut + "][" + convenio + "][BCI_FINOK]");}
        return listadoTransferenciasPendientes;
    }

    /**
     * <p>M�todo que obtiene el detalle de una transferencia seg�n su n�mero de
     * transferencia.</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 16/01/2012 Iv�n L�pez B. (ADA Ltda.): Versi�n inicial.</li>
     * </ul>
     *
     * @param numeroTransferencia n�mero de transferencia a consultar.
     * @param rutEmpresa con el rut de la empresa.
     * @param convenio con el c�digo del convenio.
     * @return arreglo con el detalle de la transferencia.
     * @throws ExcepcionGeneral Cuando existe un error se lanza esta excepci�n.
     * @since 2.4
     */
    public DetalleTransferenciaVO[]
        obtenerDetalleTransferencia(
            String numeroTransferencia, long rutEmpresa, String convenio)
                throws ExcepcionGeneral {

        if(getLogger().isEnabledFor(Level.INFO)){
             getLogger().info("[obtenerDetalleTransferencia][" + numeroTransferencia + "][BCI_INI]");
             getLogger().info("[obtenerDetalleTransferencia][" + numeroTransferencia + "] rut ["+ rutEmpresa +"], convenio ["+ convenio +"]");
        }
        DetalleTransferenciaVO[] detalles;
        HashMap parametros = new HashMap();
        parametros.put("numeroTransferencia", numeroTransferencia);
        parametros.put("rutEmpresa", StringUtil.rellenaConCeros(String.valueOf(rutEmpresa), LARGO_RUT));
        parametros.put("convenio", StringUtil.rellenaPorLaIzquierda(convenio, LARGO_CONVENIO, ' '));
        parametros.put("CHAINED_SUPPORT", "true");

        try {
            String contexto = TablaValores.getValor("JNDIConfig.parametros", "cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector =new ConectorServicioDePersistenciaDeDatos(contexto);
            List lista = conector.consultar("banele", "obtenerDetalleTransferencia", parametros);

            if (lista != null && lista.size() > 0) {

                HashMap fila = new HashMap();

                detalles = new DetalleTransferenciaVO[lista.size()];

                for (int i = 0; i < lista.size(); i++) {

                    fila = (HashMap) lista.get(i);
                    detalles[i] = new DetalleTransferenciaVO();

                    if (fila.get("numeroTransferencia") != null) {
                        detalles[i].setNumeroTransferencia(
                            String.valueOf(
                                fila.get("numeroTransferencia")));
                    }
                    else {
                        detalles[i].setNumeroTransferencia("-");
                    }
                    if (fila.get("nombreTransferencia") != null) {
                        detalles[i].setNombreArchivo(
                            String.valueOf(
                                fila.get("nombreTransferencia")));
                        detalles[i].setNombreTransferencia(
                            String.valueOf(
                                fila.get("nombreTransferencia")));
                    }
                    else {
                        detalles[i].setNombreArchivo("-");
                        detalles[i].setNombreTransferencia("-");
                    }

                    if (fila.get("cuentaOrigen") != null) {
                        detalles[i].setCuentaOrigen(
                            String.valueOf(
                                fila.get("cuentaOrigen")));
                    }
                    else {
                        detalles[i].setCuentaOrigen("-");
                    }
                    if (fila.get("cuentaDestino") != null) {
                        detalles[i].setCuentaDestino(
                            String.valueOf(
                                fila.get("cuentaDestino")));
                    }
                    else {
                        detalles[i].setCuentaDestino("-");
                    }
                    if (fila.get("nombreDestinatario") != null) {
                        detalles[i].setNombreDestinatario(
                            (String.valueOf(
                                fila.get("nombreDestinatario"))).trim());
                    }
                    else {
                        detalles[i].setNombreDestinatario("");
                    }
                    if (fila.get("monto") != null) {
                        detalles[i].setMonto(
                            Long.parseLong(
                                String.valueOf(
                                    fila.get("monto"))));
                    }
                    else {
                        detalles[i].setMonto(0);
                    }
                    if (fila.get("idDetalle") != null) {
                        detalles[i].setNumeroDetalle(
                            String.valueOf(
                                fila.get("idDetalle")));
                    }
                    else {
                        detalles[i].setNumeroDetalle("0");
                    }
                    if (fila.get("bancoDestino") != null) {
                        detalles[i].setBancoDestino(
                            String.valueOf(
                                fila.get("bancoDestino")));
                    }
                    else {
                        detalles[i].setBancoDestino("-");
                    }
                    if (fila.get("rutDestinatario") != null) {
                        detalles[i].setRutDestino(
                            Long.parseLong(
                                String.valueOf(
                                    fila.get("rutDestinatario")).trim()));
                    }
                    else {
                        detalles[i].setRutDestino(1);
                    }
                    if (fila.get("digitoVerificadorDestinatario") != null) {
                        detalles[i].setDvDestino(
                            String.valueOf(
                                fila.get("digitoVerificadorDestinatario")).charAt(0));
                    }
                    else {
                        detalles[i].setDvDestino('-');
                    }
                    if (fila.get("tipoTransferencia") != null) {
                        detalles[i].setTipo(String.valueOf(fila.get("tipoTransferencia")));
                        detalles[i].setModalidad(String.valueOf(fila.get("tipoTransferencia")).trim());
                        detalles[i].setGlosaModalidad(
                            TablaValores.getValor(TABLA_TRANSFERENCIAS, "tipoModalidad",
                                detalles[i].getModalidad()));

                    }
                    else {
                        detalles[i].setTipo(null);
                        detalles[i].setModalidad(null);
                        detalles[i].setGlosaModalidad(null);
                    }
                    if (fila.get("correoDestinatario") != null) {
                        detalles[i].setEmailDestino(
                            String.valueOf(
                                fila.get("correoDestinatario")));
                    }
                    else {
                        detalles[i].setEmailDestino(null);
                    }
                    if (fila.get("tipoCuentaOrigen") != null) {
                        detalles[i].setTipoCuentaOrigen(String.valueOf(fila.get("tipoCuentaOrigen")));
                    }
                    else {
                        detalles[i].setTipoCuentaOrigen(null);
                    }
                    if (fila.get("tipoCuentaDestino") != null) {
                        detalles[i].setTipoCuentaDestino(String.valueOf(fila.get("tipoCuentaDestino")));
                    }
                    else {
                        detalles[i].setTipoCuentaDestino(null);
                    }
                    if (fila.get("mensajeEmailDestino") != null) {
                        detalles[i].setMensajeEmailDestino(String.valueOf(fila.get("mensajeEmailDestino")).trim());
                    }
                    else {
                        detalles[i].setMensajeEmailDestino(null);
                    }
                }
            }
            else {
                if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[obtenerDetalleTransferencia][" + numeroTransferencia + "][BCI_FINEX] Detalles es null ");}
                detalles = null;
            }
        }
        catch(EjecutarException ee) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[obtenerDetalleTransferencia][" + numeroTransferencia + "][BCI_FINEX][EjecutarException ee]" + ErroresUtil.extraeStackTrace(ee));}
            throw new ExcepcionGeneral("Ha ocurrido una EjecutarException: ",ErroresUtil.extraeStackTrace(ee) );
        }
        catch(ConfiguracionException ce) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[obtenerDetalleTransferencia][" + numeroTransferencia + "][BCI_FINEX][ConfiguracionException ce]" + ErroresUtil.extraeStackTrace(ce));}
            throw new ExcepcionGeneral("Ha ocurrido una ConfiguracionException: ",ErroresUtil.extraeStackTrace(ce) );
        }
        catch(ServicioDatosException sde) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[obtenerDetalleTransferencia][" + numeroTransferencia + "][BCI_FINEX][ServicioDatosException sde]" + ErroresUtil.extraeStackTrace(sde));}
            throw new ExcepcionGeneral("Ha ocurrido una ServicioDatosException: ",ErroresUtil.extraeStackTrace(sde) );
        }
        if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[obtenerDetalleTransferencia][" + numeroTransferencia + "][BCI_FINOK] se entregan datelles ");}
        return detalles;
    }


    /**
     * <p>M�todo que elimina las cuentas Por Inscribir de una transferencia.</p>
     *
     * Registro de versiones:<ul>
     * 
     * <li>1.0 08/03/2013 Sergio Cuevas Diaz (SEnTRA): Versi�n inicial.</li>
     * 
     * </ul>
     *
     * @param numConvenio n�mero de convenio
     * @param rutEmpresa rut de la empresa
     * @param dvEmpresa dv de la empresa
     * @param numTransferencia N�mero de Transferencia
     * @throws Exception Cuando existe un error se lanza esta excepci�n.
     * @return Respuesta de la modificaci�n (true, false).
     * @since 2.5
     */
    public boolean eliminaDetalleCuentasNoInscritas(String numConvenio, long rutEmpresaLong, char dvEmpresaChar, 
        String numTransferencia) throws Exception{
        try{
            String rutEmpresa = String.valueOf(rutEmpresaLong);
            String dvEmpresa = String.valueOf(dvEmpresaChar);

            if(getLogger().isEnabledFor(Level.INFO)){
               getLogger().info("[eliminaDetalleCuentasNoInscritas][" + numTransferencia + "][BCI_INI]");
               getLogger().info("[eliminaDetalleCuentasNoInscritas][" + numTransferencia + "] convenio ["+ numConvenio +"], rutEmpresa ["+ rutEmpresaLong +"] - ["+ dvEmpresaChar +"]");
            }
            String respuesta = "";
            String contexto = TablaValores.getValor("JNDIConfig.parametros","cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
            HashMap parametrosCuenta = new HashMap();
            parametrosCuenta.put("CHAINED_SUPPORT","true");
            parametrosCuenta.put("convenio",         numConvenio);
            parametrosCuenta.put("rutEmpresa",       rutEmpresa);
            parametrosCuenta.put("dvEmpresa",        dvEmpresa);
            parametrosCuenta.put("numTransferencia", numTransferencia);

            respuesta = ((String) conector.ejecutar("banele", "eliminaDetalleCuentasNoInscritas", 
                parametrosCuenta)).toString();
            if(respuesta == null){
                respuesta = "NOK";
                if(getLogger().isEnabledFor(Level.INFO)){ getLogger().info("[eliminaDetalleCuentasNoInscritas][" + numTransferencia + "][BCI_FINEX] respuesta ["+ respuesta +"]"); }
            }

            if(log.isDebugEnabled()){
                if(getLogger().isEnabledFor(Level.INFO)){ getLogger().info("[eliminaDetalleCuentasNoInscritas][" + numTransferencia + "][BCI_FINOK] respuesta ["+ respuesta +"]"); }
            }
            return respuesta.equalsIgnoreCase("OK");

        } 
        catch (EjecutarException eE) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[eliminaDetalleCuentasNoInscritas][" + numTransferencia + "][BCI_FINEX][EjecutarException eE]["+ eE.getDetalleException()+"]");
            }
            throw new Exception(eE.getDetalleException());
        }
        catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[eliminaDetalleCuentasNoInscritas][" + numTransferencia + "][BCI_FINEX][Exception e]["  + e.toString() + "]");}
            throw new Exception(e.getMessage());
        }
    }


    /**
     * <p>
     * M�todo que obtiene un listado de instrucciones de transferencias de un
     * supervisor, y consultando seg�n ciertos filtros.
     * </p>
     * 
     * Registro de versiones:
     * <ul>
     * 
     * <li>1.0 22/01/2013 Rodrigo Arce Campos. (SEnTRA): Versi�n inicial.
     * 
     * </ul>
     * 
     * @throws Exception en caso de error.
     * @return Listado de transferencias.
     * @since 2.5
     */

    public InstruccionTransferenciaTO[] obtenerInstruccionesTransferenciaSupervisor()
        throws Exception {

        try {
            getLogger().info("[obtenerInstruccionesTransferenciaSupervisor][BCI_INI]");
            List documento = null;
            HashMap salida = null;
            HashMap parametros = new HashMap();
            InstruccionTransferenciaTO[] instruccionTransferenciaTO = null;
            parametros.put("CHAINED_SUPPORT", "true");
            String contexto = TablaValores.getValor("JNDIConfig.parametros",
                "cluster", "param");
            
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(
                contexto);
            documento = conector.consultar("banele",
                "obtenerInstruccionesTransferenciaSupervisor", parametros);

            if (documento != null) {
                if (documento.size() > 0) {
                    instruccionTransferenciaTO = new InstruccionTransferenciaTO[documento
                                                                                .size()];
                    if(getLogger().isEnabledFor(Level.INFO)){ getLogger().info("[obtenerInstruccionesTransferenciaSupervisor] transferencias [" + documento.size() + "]"); }
                    for (int i = 0; i < documento.size(); i++) {
                        salida = (HashMap) documento.get(i);
                        instruccionTransferenciaTO[i] = new InstruccionTransferenciaTO();
                        instruccionTransferenciaTO[i].setRutEmpresa(Long
                            .parseLong(((String) salida.get("emp_num_rut"))
                                .trim()));
                        instruccionTransferenciaTO[i].setConvenio(Integer
                            .parseInt(((String) salida.get("cnv_num"))
                                .trim()));
                        instruccionTransferenciaTO[i]
                            .setNumeroTransferencia(((String) salida
                                .get("trf_num")).trim());
                    }
                }
            }
            if(getLogger().isEnabledFor(Level.INFO)){ getLogger().info("[obtenerInstruccionesTransferenciaSupervisor][FINOK] instruccionTransferenciaTO [" + instruccionTransferenciaTO + "]"); }
            return instruccionTransferenciaTO;

        } 
        catch (EjecutarException eE) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[obtenerInstruccionesTransferenciaSupervisor][BCI_FINEX][EjecutarException eE][" + eE.getDetalleException() + "]"); }
            throw new Exception(eE.getDetalleException());
        } 
        catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[obtenerInstruccionesTransferenciaSupervisor][BCI_FINEX][Exception e]" + ErroresUtil.extraeStackTrace(e));}
            throw new Exception(e.getMessage());
        }
    }

    /**
     * <p>
     * M�todo que envia una instruccion a un supervisor.
     * </p>
     * 
     * Registro de versiones:
     * <ul>
     * 
     * <li>1.0 22/01/2013 Rodrigo Arce Campos. (SEnTRA): Versi�n inicial.
     * 
     * </ul>
     * 
     * @param instruccion
     *            Instruccion
     * @throws Exception en caso de error.
     * @since 2.5
     */

    public void enviarInstruccionSupervisor(
        InstruccionTransferenciaTO instruccion) throws Exception {

        String numTransferencia = instruccion.getNumeroTransferencia();
        try {
            if(getLogger().isEnabledFor(Level.INFO)){
               getLogger().info("[enviarInstruccionSupervisor][" + numTransferencia + "][BCI_INI]");
               getLogger().info("[enviarInstruccionSupervisor][" + numTransferencia + "] instruccion ["+ instruccion.toString() +"]");
            }
            HashMap parametros = new HashMap();
            parametros.put("CHAINED_SUPPORT", "true");
            parametros.put(
                "emp_num_rut",
                StringUtil.rellenaConCeros(
                    String.valueOf(instruccion.getRutEmpresa()), LARGO_RUT));
            parametros.put(
                "cnv_num",
                StringUtil.rellenaConEspacios(
                    String.valueOf(instruccion.getConvenio()), LARGO_CONVENIO));
            parametros.put("trf_num", instruccion.getNumeroTransferencia());

            String contexto = TablaValores.getValor("JNDIConfig.parametros",
                "cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(
                contexto);
            conector.ejecutar("banele", "enviarInstruccionSupervisor",
                parametros);
            if(getLogger().isEnabledFor(Level.INFO)){
                getLogger().info("[enviarInstruccionSupervisor][" + numTransferencia + "][BCI_FINOK]");
        } 
            }
        catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[enviarInstruccionSupervisor][" + numTransferencia + "][BCI_FINEX][Exception e][" + e.toString() + "]");}
            throw new Exception(e.getMessage());
        }

    }

    /**
     * <p>
     * M�todo que elimina uns instruccion de un supervisor mediante un rut.
     * </p>
     * 
     * Registro de versiones:
     * <ul>
     * 
     * <li>1.0 22/01/2013 Rodrigo Arce Campos. (SEnTRA): Versi�n inicial.
     * 
     * </ul>
     * 
     * @param rutEmpresa
     *            Rut Empresa
     * @param convenio
     *            Numero convenio
     * @param numeroTransferencia
     *            Numero transferencia
     * @throws Exception en caso de error.
     * @since 2.5
     */

    public void eliminarInstruccionTransferenciaSupervisor(long rutEmpresa,
        int convenio, String numeroTransferencia) throws Exception {

        try {
            if(getLogger().isEnabledFor(Level.INFO)){
               getLogger().info("[eliminarInstruccionTransferenciaSupervisor][" + numeroTransferencia + "][BCI_INI]");
               getLogger().info("[eliminarInstruccionTransferenciaSupervisor][" + numeroTransferencia + "] convenio ["+ convenio +"], rutEmpresa ["+ rutEmpresa +"]");
            }
            String contexto = TablaValores.getValor("JNDIConfig.parametros",
                "cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(
                contexto);
            HashMap parametrosEliminar = new HashMap();
            parametrosEliminar.put("CHAINED_SUPPORT", "true");
            parametrosEliminar.put(
                "emp_num_rut",
                StringUtil.rellenaConCeros(
                    String.valueOf(rutEmpresa), LARGO_RUT));
            parametrosEliminar.put(
                "cnv_num",
                StringUtil.rellenaConEspacios(
                    String.valueOf(convenio), LARGO_CONVENIO));
            parametrosEliminar.put("trf_num", numeroTransferencia);

            conector.ejecutar("banele",
                "eliminarInstruccionTransferenciaSupervisor",
                parametrosEliminar);
            if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[eliminarInstruccionTransferenciaSupervisor][" + numeroTransferencia + "][BCI_FINOK]");}
        } 
        catch (EjecutarException eE) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[eliminarInstruccionTransferenciaSupervisor][" + numeroTransferencia + "][BCI_FINEX][EjecutarException eE][" + eE.getDetalleException() + "]"); }
            throw new Exception(eE.getDetalleException());
        } 
        catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[eliminarInstruccionTransferenciaSupervisor][" + numeroTransferencia + "][BCI_FINEX][Exception e][" + e.toString() + "]");}
            throw new Exception(e.getMessage());
        }
    }

    /**
     * <p>
     * M�todo que inserta acciones de trasferencia en tabla bitacora.
     * </p>
     * 
     * Registro de versiones:
     * <ul>
     * 
     * <li>1.0 22/01/2013 Rodrigo Arce Campos. (SEnTRA): Versi�n inicial.
     * 
     * </ul>
     * 
     * @param datosAccion
     *            parametro de tipo clase que contiene los atributos a insetar
     *            en Base datos
     * @throws Exception en caso de error.
     * @since 2.5
     */

    public void insertarAccionBitacoraInstruccionesTransferencia(
        InstruccionTransferenciaTO datosAccion) throws Exception {

        String numTransferencia = datosAccion.getNumeroTransferencia();
        try {
            if(getLogger().isEnabledFor(Level.INFO)){
               getLogger().info("[insertarAccionBitacoraInstruccionesTransferencia][" + numTransferencia + "][BCI_INI]");
               getLogger().info("[insertarAccionBitacoraInstruccionesTransferencia][" + numTransferencia + "] Datos Accion ["+ datosAccion.toString() +"]");
            }
            HashMap parametros = new HashMap();
            parametros.put("CHAINED_SUPPORT", "true");
            parametros.put(
                "emp_num_rut",
                StringUtil.rellenaConCeros(
                    String.valueOf(datosAccion.getRutEmpresa()), LARGO_RUT));
            parametros.put(
                "cnv_num",
                StringUtil.rellenaConEspacios(
                    String.valueOf(datosAccion.getConvenio()), LARGO_CONVENIO));
            parametros.put("trf_num", datosAccion.getNumeroTransferencia());
            parametros.put("fecha_acc", datosAccion.getFechaHoraCreacion());
            parametros.put("cod_acc", datosAccion.getCodigoAccion());
            parametros.put("trf_usu", datosAccion.getAsistente());

            String contexto = TablaValores.getValor("JNDIConfig.parametros",
                "cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(
                contexto);
            conector.ejecutar("banele",
                "insertarAccionBitacoraInstruccionesTransferencia",
                parametros);
            if(getLogger().isEnabledFor(Level.INFO)){ getLogger().info("[insertarAccionBitacoraInstruccionesTransferencia][" + numTransferencia + "][BCI_FINOK]");} 
        } 
        catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[insertarAccionBitacoraInstruccionesTransferencia][" + numTransferencia + "][BCI_FINEX][Exception e][" + e.toString() + "]");}
            throw new Exception(e.getMessage());
        }
    }

    /**
     * <p>
     * M�todo que obtiene un listado de transferencias pendientes del tipo CTA o
     * LBT, consultando seg�n criterios enviados.
     * </p>
     * 
     * Registro de versiones:
     * <ul>
     * 
     * <li>1.0 22/01/2013 Rodrigo Arce Campos. (SEnTRA): Versi�n inicial.
     * </ul>
     * 
     * @param filtro
     *            los filtros a aplicar a la consulta
     * @throws Exception en caso de error.
     * @return Listado con las transferencias pendientes.
     * @since 2.5
     */
    public EncabezadoTransferenciaVO[] extraeTransferenciasPendientesCTALBT(
        FiltroConsultarTransferenciasVO filtro) throws Exception {

    	String rutEmpresa = filtro.getRutOrigen();
        String convenio = filtro.getConvenio();
        try {
            if(getLogger().isEnabledFor(Level.INFO)){
               getLogger().info("[extraeTransferenciasPendientesCTALBT][" + rutEmpresa + "][" + convenio + "][BCI_INI]");
               getLogger().info("[extraeTransferenciasPendientesCTALBT][" + rutEmpresa + "][" + convenio + "] filtro ["+ filtro.toString() +"]");
            }
            List documento = null;
            HashMap salida = null;
            HashMap parametros = new HashMap();
            EncabezadoTransferenciaVO[] transferenciasPendientesVO = null;
            parametros.put("CHAINED_SUPPORT", "true");
            parametros.put("convenio", filtro.getConvenio());
            parametros.put("rutEmpresa", filtro.getRutOrigen());
            parametros.put("fechaDesde", filtro.getFechaDesde());
            parametros.put("fechaHasta", filtro.getFechaHasta());
            parametros.put("numTransferencia", filtro.getNumeroTransferencia());

            String contexto = TablaValores.getValor("JNDIConfig.parametros",
                "cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(
                contexto);
            documento = conector.consultar("banele",
                "extraeTransferenciasPendientesCTALBT", parametros);

            if (documento != null) {
                if (documento.size() > 0) {
                    transferenciasPendientesVO = new EncabezadoTransferenciaVO[documento
                                                                               .size()];
                    if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[extraeTransferenciasPendientesCTALBT][" + rutEmpresa + "][" + convenio + "] transferenciasPendientes [" + documento.size() + "]");}
                    for (int i = 0; i < documento.size(); i++) {
                        salida = (HashMap) documento.get(i);
                        transferenciasPendientesVO[i] = new EncabezadoTransferenciaVO();
                        transferenciasPendientesVO[i]
                            .setNumeroTransferencia(String.valueOf(
                                salida.get("numeroOperacion")).trim());
                        transferenciasPendientesVO[i]
                            .setCantidadDetalles(Integer.parseInt(String
                                .valueOf(salida.get("cantidadDetalles"))
                                .trim()));
                        transferenciasPendientesVO[i]
                            .setNombreTransferencia(String.valueOf(
                                salida.get("nomTransferencia")).trim());
                        transferenciasPendientesVO[i]
                            .setCuentaOrigen(StringUtil.sacaCeros(String
                                .valueOf(salida.get("cuentaOrigen"))
                                .trim()));
                        transferenciasPendientesVO[i]
                            .setMonto(Long.parseLong(String.valueOf(
                                salida.get("montoTransferido")).trim()));
                        String codigoTipo = String.valueOf(salida.get("tipo"))
                            .trim();
                        transferenciasPendientesVO[i].setTipo(codigoTipo);
                        transferenciasPendientesVO[i].setGlosaTipo(TablaValores
                            .getValor(TABLA_TRANSFERENCIAS, codigoTipo,
                                "Desc"));
                        String codigoEstado = String.valueOf(
                            salida.get("estado")).trim();
                        transferenciasPendientesVO[i].setEstado(codigoEstado);
                        transferenciasPendientesVO[i]
                            .setGlosaEstado(TablaValores.getValor(
                                TABLA_TRANSFERENCIAS, codigoEstado,
                                "Desc"));
                        transferenciasPendientesVO[i]
                            .setFechaCreacion((Date) salida
                                .get("fechaCreacion"));
                        transferenciasPendientesVO[i].setNumeroFirmas(Integer
                            .parseInt(String.valueOf(
                                salida.get("hayFirmas")).trim()));
                        transferenciasPendientesVO[i]
                            .setCantidadBloques(Integer.parseInt(String
                                .valueOf(salida.get("numerosPagina"))
                                .trim()));
                        String hayCuentasNoInscritas = String.valueOf(
                            salida.get("hayCuentasNoInscritas")).trim();
                        transferenciasPendientesVO[i]
                            .setTieneCuentasNoInscritas(hayCuentasNoInscritas
                                .equalsIgnoreCase(HAY_CUENTAS_NO_INSCRITA));
                        transferenciasPendientesVO[i].setNombreArchivo(String
                            .valueOf(salida.get("nombreEmpresa")).trim());
                    }
                }
            }
            if (getLogger().isEnabledFor(Level.INFO)){ getLogger().info("[extraeTransferenciasPendientesCTALBT][" + rutEmpresa + "][" + convenio + "][BCI_FINOK] transferenciasPendientesVO [" + (transferenciasPendientesVO != null ? transferenciasPendientesVO.length : 0) + "]");}
            return transferenciasPendientesVO;

        } 
        catch (EjecutarException eE) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[extraeTransferenciasPendientesCTALBT][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][EjecutarException eE][" + eE.getDetalleException() + "]");}
            throw new Exception(eE.getDetalleException());
        } 
        catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[extraeTransferenciasPendientesCTALBT][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][Exception e]" + ErroresUtil.extraeStackTrace(e));}
            throw new Exception(e.getMessage());
        }

    } 

    /**
     * <p>M�todo que permite consultar si se ha realizado una Transferencia con los mismos valores en las ltimas
     *    24 horas.</p>
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 04/02/2013 Venancio Ar�nguiz (SEnTRA): Versi�n inicial.
     * <li>1.1 02/04/2014 Venancio Ar�nguiz (SEnTRA): Se mejora el logueo del metodo.
     * </ul>
     *
     * @param rutEmpresa Rut de la empresa.
     * @param convenio N�mero de convenio.
     * @param numTTFF N�mero de transferencia.
     * @param cuentaOrigen Cuenta de origen.
     * @param detalle Objeto con el detalle de la transferencia.
     * @throws ExcepcionGeneral Algn error al ejecutar la consulta.
     * @return Transferencias realizadas con los mismos datos.
     * @since 2.5
     */
    public EncabezadoTransferenciaVO[] consultarExisteTransferencia(String rutEmpresa, String convenio, String numTTFF, 
        String cuentaOrigen, ListaDeteTransf detalle) throws ExcepcionGeneral {
        int cantidad = 0;

        try {
            if(getLogger().isEnabledFor(Level.INFO)){
               getLogger().info("[consultarExisteTransferencia][" + numTTFF + "][BCI_INI]");
               getLogger().info("[consultarExisteTransferencia][" + numTTFF + "] rutEmpresa ["+ rutEmpresa +"], convenio ["+ convenio +"], cuentaOrigen ["+ cuentaOrigen +"], rutDestino ["+ detalle.getRutDestin() +"]");
               getLogger().info("[consultarExisteTransferencia][" + numTTFF + "] codBanco ["+ detalle.getCodBanco() +"], cuentaDestino ["+ detalle.getCtaDestino() +"], montoTransfer ["+ detalle.getMontoTransfer() +"]");
            }
            HashMap parametros = new HashMap();
            List documento = null;
            HashMap salida = null;
            EncabezadoTransferenciaVO[] transferenciasRepetidasVO = null;
            String contexto = TablaValores.getValor("JNDIConfig.parametros","cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
            parametros.put("CHAINED_SUPPORT", "true");

            parametros.put("rutEmpresa", rutEmpresa);
            parametros.put("numConvenio", convenio);
            parametros.put("numTransferencia", numTTFF);
            parametros.put("cuentaOrigen", cuentaOrigen);
            parametros.put("rutDestino", detalle.getRutDestin());
            parametros.put("codBanco", detalle.getCodBanco());
            parametros.put("cuentaDestino", detalle.getCtaDestino());
            parametros.put("montoTransfer", new Double(detalle.getMontoTransfer()));

            documento = conector.consultar("banele", "consultarExisteTransferencia", parametros);

            if (documento != null && documento.size() >= 0) {
                transferenciasRepetidasVO = new EncabezadoTransferenciaVO[documento.size()];
                if(getLogger().isEnabledFor(Level.INFO)){
                    getLogger().info("[consultarExisteTransferencia][" + numTTFF + "] transferencias repetidas [" + documento.size() + "]");
                }
                for (int i = 0; i < documento.size(); i++) {
                    salida = (HashMap) documento.get(i);
                    transferenciasRepetidasVO[i] = new EncabezadoTransferenciaVO();
                    transferenciasRepetidasVO[i]
                        .setNumeroTransferencia(String.valueOf(
                            salida.get("numeroTransferencia")).trim());
                    transferenciasRepetidasVO[i]
                        .setNombreTransferencia(String.valueOf(
                            salida.get("nombreTransferencia")).trim());
                    transferenciasRepetidasVO[i]
                        .setFechaCreacion((Date) salida /*Cambiar FechaCreacion por un setFechaPago*/
                            .get("fechaPago"));
                    transferenciasRepetidasVO[i]
                        .setEstado(String.valueOf(
                            salida.get("estado")).trim());
                }
            }
            if (getLogger().isEnabledFor(Level.INFO)){ getLogger().info("[consultarExisteTransferencia][" + numTTFF + "][BCI_FINOK] transferenciasRepetidasVO [" + (transferenciasRepetidasVO != null ? transferenciasRepetidasVO.length : 0) + "]");}
            return transferenciasRepetidasVO;

        } 
        catch (EjecutarException eE) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[consultarExisteTransferencia][" + numTTFF + "][BCI_FINEX][EjecutarException eE][" + ErroresUtil.extraeStackTrace(eE) + "]");}
            throw new ExcepcionGeneral("Ha ocurrido una EjecutarException: ",
                ErroresUtil.extraeStackTrace(eE));
        } 
        catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[consultarExisteTransferencia][" + numTTFF + "][BCI_FINEX][Exception e][" + ErroresUtil.extraeStackTrace(e) + "]"); }
            throw new ExcepcionGeneral("Ha ocurrido una Exception: ",
                ErroresUtil.extraeStackTrace(e));
        }
    }

    /**
     * M�todo que obtiene los tiempos de espera y posici�n en cola de procesamiento de las nominas que se
     * encuentran con estado firmada.
     * <p>
     * Registro de versiones:<ul>
     * <li> 1.0 22/01/2014, Venancio Ar�nguiz (SEnTRA): Versi�n inicial.</li>
     * </ul>
     * <p>
     *
     * @param numTTFF N�mero de la transferencia.
     * @return Un objeto que contiene el estado, el tiempo de espera y la posici�n en cola.
     * @throws Exception
     * 
     * @since 3.0
     */
    public EncabezadoTransferenciaVO obtenerTiempoEsperaNominaTTFF (String numTTFF) throws ExcepcionGeneral {
        try {
            HashMap parametros = new HashMap();
            List documento = null;
            HashMap salida = null;
            String codigoEstado = "";

            if(getLogger().isEnabledFor(Level.INFO)){
               getLogger().info("[obtenerTiempoEsperaNominaTTFF][" + numTTFF + "][BCI_INI]");
            }
            String contexto = TablaValores.getValor("JNDIConfig.parametros","cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
            parametros.put("CHAINED_SUPPORT", "true");
            parametros.put("numTTFF", numTTFF);

            EncabezadoTransferenciaVO ttffVO = new EncabezadoTransferenciaVO();
            documento = conector.consultar("banele", "obtieneTiempoEspera", parametros);

            if (documento != null && !documento.isEmpty()) {
                salida = (HashMap) documento.get(0);
                ttffVO = new EncabezadoTransferenciaVO();
                codigoEstado = String.valueOf(salida.get("estado")).trim();
                ttffVO.setEstado(codigoEstado);
                ttffVO.setGlosaEstado(TablaValores.getValor(TABLA_TRANSFERENCIAS, codigoEstado, "Desc"));
                ttffVO.setTiempoEspera(Integer.parseInt(String.valueOf(salida.get("tiempoEspera"))));
                ttffVO.setPosicion(Integer.parseInt(String.valueOf(salida.get("turno"))));
            }
            if(getLogger().isEnabledFor(Level.INFO)){ getLogger().info("[obtenerTiempoEsperaNominaTTFF][" + numTTFF + "][BCI_FINOK]");}
            return ttffVO;

        } 
        catch (EjecutarException eE) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[obtenerTiempoEsperaNominaTTFF][" + numTTFF + "][BCI_FINEX][EjecutarException eE][" + ErroresUtil.extraeStackTrace(eE) + "]"); }
            throw new ExcepcionGeneral("Ha ocurrido una EjecutarException: ", ErroresUtil.extraeStackTrace(eE));
        } 
        catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[obtenerTiempoEsperaNominaTTFF][" + numTTFF + "][BCI_FINEX][Exception e][" + ErroresUtil.extraeStackTrace(e) + "]"); }
            throw new ExcepcionGeneral("Ha ocurrido una Exception: ", ErroresUtil.extraeStackTrace(e));
        }
    }

    /**
     * M�todo utilizado para obtener el encabezado de una n�mina.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 28/10/2014 Pablo Romero C�ceres (SEnTRA) : versi�n incial.
     * <li>1.1 29/12/2014 Rafael Pizarro (TINet): Se normaliza log de metodo
     * para ajustarse a Normativa Log4j de BCI.</li>
     * </ul>
     * @param nomina Nomina que se consultar�.
     * @return Totales de la n�mina
     * @throws GeneralException en caso de error.
     * @since 3.1
     */
    public InformacionCargaMasiva obtenerEncabezadoDeNomina(InformacionCargaMasiva nomina) throws GeneralException{
        
        String numTransferencia = nomina.getNumTransferencia();
        try{
            if(getLogger().isEnabledFor(Level.INFO)){
                getLogger().info("[obtenerEncabezadoDeNomina][" + numTransferencia + "][BCI_INI]");
                getLogger().info("[obtenerEncabezadoDeNomina][" + numTransferencia + "] Inicio nomina [" + nomina.toString() + "]");
            }
            HashMap parametros = new HashMap();
            parametros.put("CHAINED_SUPPORT", "true");
            parametros.put("trf_num", nomina.getNumTransferencia() == null ? "" : nomina.getNumTransferencia());
            parametros.put("trf_tip", nomina.getTipoTransferencia() == null ? "" : nomina.getTipoTransferencia());
            parametros.put("canal", nomina.getCanal() == null ? "" : nomina.getCanal());
            parametros.put("origen", nomina.getOrigen() == null ? "" : nomina.getOrigen());
            parametros.put("producto", nomina.getProducto() == null ? "" : nomina.getProducto());

            String contexto = TablaValores.getValor("JNDIConfig.parametros", "cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
            if(getLogger().isEnabledFor(Level.INFO)){
               getLogger().info("[obtenerEncabezadoDeNomina][" + numTransferencia + "] llama a sp obtenerEncabezadoDeNomina");
            }
            List resultado = conector.consultar("banele", "obtenerEncabezadoDeNomina", parametros);

            if (resultado != null && resultado.size() > 0) {
                HashMap datosGenerales = (HashMap) resultado.get(0);              

                nomina.setNumTransferencia(datosGenerales.get("numeroTransferencia").toString().trim());
                nomina.setRutEmpresa(Long.parseLong(datosGenerales.get("rutEmpresa").toString().trim()));
                nomina.setDvEmpresa(((String) datosGenerales.get("dvEmpresa")).charAt(0));
                nomina.setRutUsuario(Long.parseLong(datosGenerales.get("rutUsuario").toString().trim()));
                nomina.setDvUsuario(((String) datosGenerales.get("dvUsuario")).charAt(0));
                nomina.setTipoTransferencia(datosGenerales.get("tipoTransferencia").toString().trim());
                nomina.setDescripcionTipoTransferencia(datosGenerales.get("descripcionTipoTransferencia").toString().trim());
                nomina.setCtaOrigen(datosGenerales.get("cuentaCargo").toString().trim());
                nomina.setEtiqueta(datosGenerales.get("glosa").toString().trim());
                nomina.setEstado(datosGenerales.get("codigoEstado").toString().trim());
                nomina.setFechaEnvio((Date) datosGenerales.get("fechaCreacion"));
                nomina.setFechaPago((Date) datosGenerales.get("fechaPago"));
                nomina.setModalidadDeCargo(Integer.parseInt(datosGenerales.get("indicadorDiferida").toString()
                    .trim()));
                nomina.setDescripcionEstado(datosGenerales.get("glosaEstado").toString().trim());
                nomina.setDescripcionIndicadorModulo(
                    datosGenerales.get("glosaModalidadCargo").toString().trim());
                nomina.setNombreEmpresa(datosGenerales.get("razonSocial").toString().trim());
                nomina.setCnvEmpresa(datosGenerales.get("convenio").toString().trim());
                nomina.setMontoTotalNominas(
                    ((Double) datosGenerales.get("montoTotalAceptado")).doubleValue());
                nomina.setCantidadPagosAceptados(
                    ((Integer) datosGenerales.get("totalPagosAceptados")).intValue());
            }
            else{
                nomina = null;
                if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[obtenerEncabezadoDeNomina][" + numTransferencia + "][BCI_FINEX] nomina es null"); }
            }
            
            if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[obtenerEncabezadoDeNomina][" + numTransferencia + "][BCI_FINOK] retornando objeto : " + StringUtil.contenidoDe(nomina) + ")"); }
            return nomina;
        }
        catch(Exception e){
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[obtenerEncabezadoDeNomina][" + numTransferencia + "][BCI_FINEX][Exception e]"+ e.getMessage(), e);}
            throw e instanceof GeneralException ? (GeneralException) e 
                : new GeneralException("ESPECIAL", ErroresUtil.extraeStackTrace(e));
        }

    }
    /**
     * 
     * M�todo que permite actualizar el estado de una n�mina en proceso.
     * <p>
     * <b>Registro de versiones:</b>
     * <ul>
     * <li> 1.0 08-10-2014, H�ctor Beas D. (Imagemaker IT): versi�n inicial</li>
     * </ul>
     * </p> 
     * 
     * @param info objeto de n�mina para consultar.
     * @return boolean con respuesta.
     * @throws Exception en caso de error.
     * @since 1.0
     */
    public boolean actualizarEstadoNominaEnProceso(InformacionCargaMasiva info) throws Exception{
        
        String numTransferencia = info.getNumTransferencia();
        try{
            if(getLogger().isEnabledFor(Level.INFO)){
                getLogger().info("[actualizarEstadoNominaEnProceso][" + numTransferencia + "][BCI_INI]");
                getLogger().info("[actualizarEstadoNominaEnProceso][" + numTransferencia + "] info ["+ info.toString() +"]");
            }
            boolean salida = false;
            HashMap parametros = new HashMap();
            parametros.put("CHAINED_SUPPORT", "true");
            parametros.put("trf_num", info.getNumTransferencia() == null ? "" : info.getNumTransferencia());
            parametros.put("cod_est", info.getEstado() == null ? "" : info.getEstado());
            parametros.put("cod_est_prc", info.getEstadoProceso() == null ? "" : info.getEstadoProceso());
            parametros.put("cod_est_opf", new Integer(info.getEstadoProcesoOPF()));

            String contexto = TablaValores.getValor("JNDIConfig.parametros", "cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
            String resultado = String.valueOf(conector.ejecutar("banele", "actualizarEstadoNominaEnProceso",
                parametros));

            if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[actualizarEstadoNominaEnProceso][" + numTransferencia + "] Respuesta ejecuci�n sp actualizarEstadoNominaEnProceso " + resultado);}

            if (resultado != null) {
                salida = "1".equals(resultado) ? true : false;
            }
            if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[actualizarEstadoNominaEnProceso][" + numTransferencia + "][BCI_INI]");}
            return salida;
        }
        catch(Exception e){
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[actualizarEstadoNominaEnProceso][" + numTransferencia + "][BCI_FINEX][Exception e]" + e.getMessage(), e);}
            throw e instanceof GeneralException ? (GeneralException) e : new GeneralException("ESPECIAL",
                ErroresUtil.extraeStackTrace(e));
        }

    }

    /**
     * M�todo utilizado para obtener los pagos de una n�mina.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 28/10/2014 Pablo Romero C�ceres (SEnTRA) : versi�n incial.</li>
     * <li>1.1 29/12/2014 Rafael Pizarro (TINet): Se normaliza log de metodo</li>
	 * <li>1.3 15/05/2017 Carlep Lucena (Everis) - Kay Vera (Ing De Soft BCI ): Se incorpora manejo de estado para nominas rechazadas, se adiciona glosaCodigoRechazo
     * para ajustarse a Normativa Log4j de BCI.</li>
     * <li>1.4 06/09/2017 Rodrigo Seguel (Everis) - Maribel Saavedra (Ing De Soft BCI ): Correctivo 48299 PNOL. Se agrega validacion para obtener glosas de rechazos en transferencias BCI</li>
     * </ul>
     * @param nomina N�mina de la cual se consultar�n los pagos.
     * @return Pagos obtenidos.
     * @throws GeneralException en caso de error.
     * @since 3.1
     */
    public InfoDetalleNomina[] obtenerPagosNomina(InformacionCargaMasiva nomina) 
        throws GeneralException{
        
        String numTransferencia = nomina.getNumTransferencia();
        try{
            if(getLogger().isEnabledFor(Level.INFO)){
                getLogger().info("[obtenerPagosNomina][" + numTransferencia + "][BCI_INI]");
                getLogger().info("[obtenerPagosNomina][" + numTransferencia + "] nomina ["+ nomina.toString() +"]");
            }
            InfoDetalleNomina[] pagos = null;
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(
                TablaValores.getValor("JNDIConfig.parametros","cluster", "param"));
            HashMap parametros = new HashMap();
            parametros.put("numeroTransferencia", nomina.getNumTransferencia());
            parametros.put("tipoTransferencia", nomina.getTipoTransferencia());
            parametros.put("canal", nomina.getCanal());
            parametros.put("origen", nomina.getOrigen());
            parametros.put("productoPago", nomina.getProducto() == null ? "" 
                : nomina.getProducto());

            if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[obtenerPagosNomina][" + numTransferencia + "]llamando sp obtenerPagosNomina"); }
            List resultado = conector.consultar("banele", "obtenerPagosNomina", parametros);

            if (resultado != null ){
                pagos = new InfoDetalleNomina[resultado.size()];
                for (int i = 0; i < resultado.size(); i++){
                    HashMap pago = (HashMap) resultado.get(i);
                    pagos[i] = new InfoDetalleNomina();
                    pagos[i].setIndicador(((Double) pago.get("indicadorRegistro")).doubleValue());
                    pagos[i].setNumTransferencia(((String) (pago.get("numeroTransferencia") != null 
                        ? pago.get("numeroTransferencia") : "")).trim());
                    pagos[i].setNumConvenio(((String) (pago.get("numeroConvenio") != null 
                        ? pago.get("numeroConvenio") : "")).trim());
                    pagos[i].setRutEmpresa((((String) (pago.get("rutEmpresa") != null 
                        ? pago.get("rutEmpresa") : "")).trim()));
                    pagos[i].setDvEmpresa(((String) (pago.get("digitoVerificadorEmpresa") != null 
                        ? pago.get("digitoVerificadorEmpresa") : "")).trim());
                    pagos[i].setCtaDestino(((String) (pago.get("cuentaDestino") != null 
                        ? pago.get("cuentaDestino") : "")).trim());
                    pagos[i].setNombreCuenta(((String) (pago.get("nombreCuenta") != null 
                        ? pago.get("nombreCuenta") : "")).trim());
                    pagos[i].setBcoDestino(((String) (pago.get("codigoBancoDestino") != null 
                        ? pago.get("codigoBancoDestino") : "")).trim());
                    pagos[i].setDescripcionBancoDestino(((String) (pago.get("descripcionBancoDestino") != null 
                        ? pago.get("descripcionBancoDestino") : "")).trim());
                    pagos[i].setMontoDetalle(((Double) pago.get("monto")).doubleValue());
                    pagos[i].setEstado(((String) (pago.get("estado") != null ? pago.get("estado") : "")).trim());
                    pagos[i].setEstadoDetalle(((String) (pago.get("descripcionEstado") != null 
                        ? pago.get("descripcionEstado") : "")).trim());
                    pagos[i].setCodigoRechazo(((String) (pago.get("codigoRechazo") != null 
                        ? pago.get("codigoRechazo") : "")).trim());
                    pagos[i].setTipoPago(((String) (pago.get("tipoPago") != null 
                        ? pago.get("tipoPago") : "")).trim());
                    pagos[i].setGlosaTipoPago(((String) (pago.get("descripcionTipoPago") != null 
                        ? pago.get("descripcionTipoPago") : "")).trim());
                    pagos[i].setRutDestinatario(((String) (pago.get("rutDestinatario") != null 
                        ? pago.get("rutDestinatario") : "")).trim());
                    pagos[i].setDvDestinatario(((String) (pago.get("dvDestinatario") != null 
                        ? pago.get("dvDestinatario") : "")).trim());
                    pagos[i].setNombreDestinatario(((String) (pago.get("nombreDestinatario") != null 
                        ? pago.get("nombreDestinatario") : "")).trim());
                    pagos[i].setNumeroBoletaOFactura(((String) (pago.get("numeroFactura") != null 
                        ? pago.get("numeroFactura") : "")).trim());
                    pagos[i].setNumeroOrdenCompra(((String) (pago.get("numeroOrdenCompra") != null 
                        ? pago.get("numeroOrdenCompra") : "")).trim());
                    pagos[i].setMensajeCorreoBenef(((String) (pago.get("mensajeDestinatario") != null 
                        ? pago.get("mensajeDestinatario") : "")).trim());
                    pagos[i].setCorreoDestinatario(((String) (pago.get("emailDestinatario") != null 
                        ? pago.get("emailDestinatario") : "")).trim());
                    pagos[i].setFechaPago((Date) pago.get("fechaPago"));
                    pagos[i].setCuentaOrigen(((String) (pago.get("cuentaCargo") != null 
                        ? pago.get("cuentaCargo") : "")).trim());
                    pagos[i].setIdentificadorPago(((String) (pago.get("traceNumber") != null 
                        ? pago.get("traceNumber") : "")).trim());
					
					String codigoRechazo = pagos[i].getCodigoRechazo();
                    String glosaRechazo = "" ;
                    if(!codigoRechazo.equals("")){
                        if(codigoRechazo.equals(ERROR_TIMEOUT)){
                            glosaRechazo = TablaValores.getValor(ARCH_PARAM_ERR, codigoRechazo, "Desc");
                        }else{
                            if ("C".equals(String.valueOf(codigoRechazo.charAt(0)))){
                                codigoRechazo = "0" + codigoRechazo.substring(
                                        codigoRechazo.length()-3, codigoRechazo.length()).trim();
                                glosaRechazo = TablaValores.getValor(
                                        TABLA_TTFF_TERCEROS, codigoRechazo, "mensaje");
                            }
                            else{
                                 glosaRechazo = TablaValores.getValor(
                                    TABLA_ERROR_TRF, codigoRechazo, "Desc");
                            }
                        }
                        pagos[i].setGlosaCodigoRechazo(glosaRechazo);
                    }else{
                    	pagos[i].setGlosaCodigoRechazo(" ");
                    }

                }
            }
            if (getLogger().isEnabledFor(Level.INFO)){ getLogger().info("[obtenerPagosNomina][" + numTransferencia + "][BCI_FINOK] pagos [" + (pagos != null ? pagos.length : 0) + "]");}
            return pagos;
        }
        catch(Exception e){
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[obtenerPagosNomina][" + numTransferencia + "][BCI_FINEX][Exception e]" + e.getMessage(), e);}
            throw new GeneralException("ESPECIAL", ErroresUtil.extraeStackTrace(e));
        }
    }

    /**
     * M�todo utilizado para obtener los totales de una n�mina en proceso.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 28/10/2014 Pablo Romero C�ceres (SEnTRA) : versi�n incial.
     * <li>1.1 29/12/2014 Rafael Pizarro (TINet): Se normaliza log de metodo
     * para ajustarse a Normativa Log4j de BCI.</li>
     * </ul>
     * @param nomina Nomina que se consultar�.
     * @return Totales de la n�mina
     * @throws GeneralException en caso de error.
     * @since 3.1
     */
    public TotalTO[] obtenerTotalesNomina(InformacionCargaMasiva nomina) 
        throws GeneralException{
        String numTransferencia = nomina.getNumTransferencia();
        try{
             if(getLogger().isEnabledFor(Level.INFO)){
                 getLogger().info("[obtenerTotalesNomina][" + numTransferencia + "][BCI_INI]");
                 getLogger().info("[obtenerTotalesNomina][" + numTransferencia + "] nomina ["+ nomina.toString() +"]");
            }
            TotalTO[] totales = null;
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(
                TablaValores.getValor("JNDIConfig.parametros","cluster", "param"));
            HashMap parametros = new HashMap();
            parametros.put("numeroTransferencia", nomina.getNumTransferencia());
            parametros.put("tipoTransferencia", nomina.getTipoTransferencia());
            parametros.put("canal", nomina.getCanal());
            parametros.put("origen", nomina.getOrigen());

            if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[obtenerTotalesNomina][" + numTransferencia + "] llamada a sp obtenerTotalesNomina"); }
            List resultado = conector.consultar("banele", "obtenerTotalesNomina", parametros);

            if (resultado != null ){
                totales = new TotalTO[resultado.size()];
                for (int i = 0; i < resultado.size(); i++){
                    HashMap total = (HashMap) resultado.get(i);
                    totales[i] = new TotalTO();
                    totales[i].setTipoPago(((String) total.get("tipoPago")).trim());
                    totales[i].setDescripcionTipoPago(((String) total.get("glosaTipoPago")).trim());
                    totales[i].setTotalRegistros(((Integer) total.get("totalPagos")).intValue());
                    totales[i].setTotalRegistrosAceptados(((Integer) total.get("totalPagosAceptados")).intValue());
                    totales[i].setTotalRegistrosRechazados(
                        ((Integer) total.get("totalPagosRechazados")).intValue());
                    totales[i].setMontoTotal(((Double) total.get("montoTotal")).doubleValue());
                    totales[i].setMontoTotalAceptados(((Double) total.get("montoTotalAceptado")).doubleValue());
                    totales[i].setMontoTotalRechazados(((Double) total.get("montoTotalRechazado")).doubleValue());
                }
            }
            if (getLogger().isEnabledFor(Level.INFO)){ getLogger().info("[obtenerTotalesNomina][" + numTransferencia + "][BCI_FINOK] totales [" + (totales != null ? totales.length : 0) + "]");}
            return totales;
        }
        catch(Exception e){
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[eliminarTransferencia][" + numTransferencia + "][BCI_FINEX][Exception e]" + e.getMessage(), e);}
            throw new GeneralException("ESPECIAL", ErroresUtil.extraeStackTrace(e));
        }
    }

    /**
     * getLogger.
     * @return logger.
     */
    public Logger getLogger(){
        if(log == null){
            log = Logger.getLogger(this.getClass());
        }
        return log;
    }

    /**
     * M�todo consulta las n�minas en linea pendientes de firma para un convenio empresa, en un rango de fecha
     * determinado
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 27/10/2014 H�ctor Hern�ndez Orrego (Sentra): versi�n inicial.</li>
     * <li>1.1 26/01/2015 Sergio Cuevas D�az (Sentra): Se encapsulan parametros de la firma del m�todo.</li>
     * </ul>
     * <p>
     * @param FiltroConsultaTO contenedor de datos para consulta.
     * @return InformacionCargaMasiva[]
     * @throws Exception Exception.
     * 
     * @since 1.0
     */ 
    public InformacionCargaMasiva[] obtieneNominasEnLineaPendientesDeFirma(
        FiltroConsultaTO filtroConsulta) throws Exception {
    
        String rutEmpresa = filtroConsulta.getCuentaOrigen();
        String convenioEmp = filtroConsulta.getConvenio();
        
    try {
            if(getLogger().isEnabledFor(Level.INFO)){
                getLogger().info("[obtieneNominasEnLineaPendientesDeFirma][" + rutEmpresa + "][" + convenioEmp + "][BCI_INI]");
                getLogger().info("[obtieneNominasEnLineaPendientesDeFirma][" + rutEmpresa + "][" + convenioEmp + "] filtroConsulta ["+ filtroConsulta.toString() +"]");
        }
        HashMap parametros = new HashMap();
        String convenio = StringUtil.rellenaPorLaIzquierda(
                filtroConsulta.getConvenio(), LARGO_CONVENIO, ' ');

        String rutEmp = String.valueOf(filtroConsulta.getRutEmpresa());
        
        parametros.put("CHAINED_SUPPORT", "true");
        parametros.put("rut_empresa",  ((rutEmp == null) || (rutEmp.equals("0")))? "" : rutEmp);
        parametros.put("convenio", convenio == null ? "" : convenio);
        parametros.put("fechaInicio", filtroConsulta.getFechaPagoDesde() == null ? "" : 
            FechasUtil.convierteDateAString(filtroConsulta.getFechaPagoDesde(), "MM/dd/yyyy"));
        parametros.put("fechaFin", filtroConsulta.getFechaPagoHasta() == null ? "" : 
            FechasUtil.convierteDateAString(filtroConsulta.getFechaPagoHasta(), "MM/dd/yyyy"));
        parametros.put("tipoTransferencia", filtroConsulta.getTipoTransferencia() == null ? "" : 
            filtroConsulta.getTipoTransferencia());
        parametros.put("canalOrigen", filtroConsulta.getCanal() == null ? "" : filtroConsulta.getCanal());
        parametros.put("origen", filtroConsulta.getOrigen() == null ? "" : filtroConsulta.getOrigen());
        parametros.put("productoPago", filtroConsulta.getProductoPago() == null ? "" : 
            filtroConsulta.getProductoPago());
        parametros.put("trf_num", filtroConsulta.getNumeroTransferencia() == null ? "" : 
            filtroConsulta.getNumeroTransferencia().trim());

        String contexto = TablaValores.getValor("JNDIConfig.parametros", "cluster", "param");
        ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
        List resultado = conector.consultar("banele", "obtieneNominasEnLineaPendientesFirma", parametros);

        InformacionCargaMasiva[] infoCarga = null;
        HashMap datosGenerales = null;

        if (resultado != null && resultado.size() > 0) {
            infoCarga = new InformacionCargaMasiva[resultado.size()];

            for (int i = 0; i < resultado.size(); i++) {
                datosGenerales = (HashMap) resultado.get(i);
                infoCarga[i] = new InformacionCargaMasiva();

                infoCarga[i].setNumTransferencia(datosGenerales.get("numeroTransferencia").toString().trim());
                infoCarga[i].setCtaOrigen(datosGenerales.get("cuentaCargo").toString().trim());
                infoCarga[i].setRutUsuario(Long.parseLong(((String) datosGenerales.get("rutUsuario")).trim()));
                infoCarga[i].setDvUsuario(((String) datosGenerales.get("dvUsuario")).charAt(0));
                infoCarga[i].setCnvEmpresa(datosGenerales.get("convenio").toString().trim());
                infoCarga[i].setRutEmpresa(Long.parseLong(datosGenerales.get("rutEmpresa").toString().trim()));
                infoCarga[i].setDvEmpresa(((String) datosGenerales.get("dvEmpresa")).charAt(0));
                infoCarga[i].setModalidadDeCargo(Integer.parseInt(datosGenerales.get("modalidadCargo")
                    .toString().trim()));
                infoCarga[i].setEtiqueta(datosGenerales.get("nombreNomina").toString().trim());
                infoCarga[i].setFechaPago((Date) datosGenerales.get("fechaPago"));
                infoCarga[i].setFechaEnvio((Date) datosGenerales.get("fechaCreacion"));
                infoCarga[i].setEstado(datosGenerales.get("estado").toString().trim());
                infoCarga[i].setTipoTransferencia(datosGenerales.get("tipoTransferencia").toString().trim());
                infoCarga[i].setCanal(datosGenerales.get("canal").toString().trim());
                infoCarga[i].setNombreEmpresa(datosGenerales.get("razonSocial").toString().trim());

                TotalTO[] totalesPagos =  new TotalTO[1];
                totalesPagos[0] = new TotalTO();

                totalesPagos[0].setTotalRegistrosAceptados(Integer.parseInt((String) datosGenerales.get(
                    "cantidadPagosNomina").toString().trim()));
                totalesPagos[0].setMontoTotalAceptados(new Double(datosGenerales.get("montoNomina")
                    .toString().trim()).doubleValue());

                infoCarga[i].setTotales(totalesPagos);
            }

        }
            if (getLogger().isEnabledFor(Level.INFO)){getLogger().info("[obtieneNominasEnLineaPendientesDeFirma][" + rutEmpresa + "][" + convenioEmp + "][BCI_FINOK] Pagos obtenidos [" + infoCarga + "]");}
        return infoCarga;

    }
    catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[obtieneNominasEnLineaPendientesDeFirma][" + rutEmpresa + "][" + convenioEmp + "][BCI_FINEX][Exception e]" + e.getMessage(), e); }
        throw e instanceof GeneralException ? (GeneralException) e : new GeneralException("ESPECIAL",
            ErroresUtil.extraeStackTrace(e));
    }

}
    /**
     * M�todo utilizado para obtener el encabezado de una n�mina en proceso.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 28/10/2014 Pablo Romero C�ceres (SEnTRA) : versi�n incial.
     * <li>1.1 16/02/2015 Gonzalo Cofre Guzman (SEnTRA) : Se agrega set del canal en la salida.
     * <li>1.2 29/12/2014 Rafael Pizarro (TINet): Se normaliza log de metodo
     * para ajustarse a Normativa Log4j de BCI.</li>
     * <li> 1.3 09/12/2015 Darlyn Delgado Perez (SEnTRA)  - Maria Jose Romero (ing Soft. BCI) :se agregaran par�metros de salida.</li>
     * </ul>
     * @param nomina Nomina que se consultar�.
     * @return encabezado de la n�mina.
     * @throws GeneralException en caso de error.
     * @since 3.1
     */
    public InformacionCargaMasiva obtenerEncabezadoDeNominaEnProceso(InformacionCargaMasiva nomina) 
        throws GeneralException{
        try{
            if(getLogger().isEnabledFor(Level.INFO)){
                getLogger().info("[obtenerEncabezadoDeNominaEnProceso][BCI_INI] nomina ["+ (nomina!=null?nomina.toString():null) +"]");
            }
            if(nomina != null){
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(
                TablaValores.getValor("JNDIConfig.parametros","cluster", "param"));
            HashMap parametros = new HashMap();
            parametros.put("numeroTransferencia", nomina.getNumTransferencia());
            parametros.put("tipoTransferencia", nomina.getTipoTransferencia());
            parametros.put("canal", nomina.getCanal());
            parametros.put("productoPago", nomina.getProducto());

            if(getLogger().isDebugEnabled()){
                getLogger().debug("[obtenerEncabezadoDeNominaEnProceso][" + nomina.getNumTransferencia() + "] Llamada a sp obtenerEncabezadoDeNominaEnProceso");
            }
            List resultado = conector.consultar("banele", "obtenerEncabezadoDeNominaEnProceso", parametros);
            if(getLogger().isDebugEnabled()){
                getLogger().debug("[obtenerEncabezadoDeNominaEnProceso][" + nomina.getNumTransferencia() + "] resultado"+ StringUtil.contenidoDe(resultado));
            }
            if (resultado != null && resultado.size() > 0) {
                HashMap encabezadoNomina = (HashMap) resultado.get(0);
                nomina.setNumTransferencia(((String) encabezadoNomina.get("numeroTransferencia")).trim());
                nomina.setRutEmpresa(
                    new Long(((String) encabezadoNomina.get("rutEmpresa")).trim()).longValue());
                nomina.setDvEmpresa(((String) encabezadoNomina.get("dvEmpresa")).trim().charAt(0));
                nomina.setCnvEmpresa(((String) encabezadoNomina.get("numeroConvenio")).trim());
                nomina.setRutUsuario(
                    new Long(((String) encabezadoNomina.get("rutUsuario")).trim()).longValue());
                nomina.setDvUsuario(((String) encabezadoNomina.get("dvUsuario")).trim().charAt(0));
                nomina.setTipoTransferencia(((String) encabezadoNomina.get("tipoTransferencia")).trim());
                nomina.setCtaOrigen(((String) encabezadoNomina.get("cuentaCargo")).trim());
                nomina.setEtiqueta(((String) encabezadoNomina.get("glosa")).trim());
                nomina.setEstado(((String) encabezadoNomina.get("codigoEstado")).trim());
                nomina.setEstadoProceso(((String) encabezadoNomina.get("codigoEstadoProceso")).trim());
                nomina.setFechaEnvio((Date) encabezadoNomina.get("fechaCreacion"));
                nomina.setFechaPago((Date) encabezadoNomina.get("fechaPago"));
                nomina.setModalidadDeCargo(
                    new Integer(((String) encabezadoNomina.get("indicadorDiferida")).trim()).intValue());
                nomina.setEstadoProcesoCCT(
                    new Integer(((String) encabezadoNomina.get("estadoProcesoCCT")).trim()).intValue());
                nomina.setEstadoProcesoCCA(
                    new Integer(((String) encabezadoNomina.get("estadoProcesoCCA")).trim()).intValue());
                nomina.setEstadoProcesoOPF(
                    new Integer(((String) encabezadoNomina.get("estadoProcesoOPF")).trim()).intValue());

                String estadoOPF = TablaValores.getValor(TABLA_ESTADO_OPF, "estadosOPF_"
                    + ((String) encabezadoNomina.get("estadoProcesoOPF")).trim(), "Desc");
                if(getLogger().isDebugEnabled()){
                    getLogger().debug("[obtenerEncabezadoDeNominaEnProceso][" + nomina.getNumTransferencia() + "] estadoOPF"+ estadoOPF);
                }
                if(estadoOPF!=null){
                    nomina.setDescripcionEstadoProcesoOPF(estadoOPF);
                }
                else{
                    nomina.setDescripcionEstadoProcesoOPF("");
                }

                nomina.setDescripcionEstado(((String) encabezadoNomina.get("glosaEstado")).trim());
                nomina.setDescripcionEstadoProceso(
                    ((String) encabezadoNomina.get("glosaEstadoProceso")).trim());
                nomina.setDescripcionIndicadorModulo(
                    ((String) encabezadoNomina.get("glosaModalidadCargo")).trim());
                nomina.setMontoTotalNominas(
                    ((Double) encabezadoNomina.get("montoTotalAceptado")).doubleValue());
                nomina.setCantidadPagosAceptados(
                    ((Integer) encabezadoNomina.get("totalPagosAceptados")).intValue());
                nomina.setCanal(((String) encabezadoNomina.get("canal")).trim());
                nomina.setNombreEmpresa(((String) encabezadoNomina.get("razonSocial")).trim());
            }
            if(getLogger().isEnabledFor(Level.INFO)){
                getLogger().info("[obtenerEncabezadoDeNominaEnProceso][" + nomina.getNumTransferencia() + "][BCI_FINOK] retornando objeto :" + nomina.toString());
            }
            return nomina;
        }
            else{
                if(getLogger().isDebugEnabled()){
                    getLogger().debug("[obtenerEncabezadoDeNominaEnProceso] objeto de entrada null");
                }
                throw new GeneralException("ESPECIAL");
            }                
        }
        catch(Exception e){
            if(getLogger().isEnabledFor(Level.ERROR)){
                getLogger().error("[obtenerEncabezadoDeNominaEnProceso][BCI_FINEX][Exception]" + e.getMessage(), e); 
            }
            throw new GeneralException("ESPECIAL", ErroresUtil.extraeStackTrace(e));
        }
    }

    /**
     * M�todo consulta el monto transferido a un benefeciario (rut-cuenta) desde una cuenta origen en el d�a
     * consultado.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 27/10/2014 H�ctor Hern�ndez Orrego (Sentra): versi�n inicial.</li>
     * </ul>
     * <p>
     * 
     * @since 1.0
     */
    public double consultaMontoPagadoBeneficiario(long rutBeneficiario, String cuentaDestino, String cuentaOrigen,
        Date fechaPago) throws GeneralException {
        try {
            if(getLogger().isEnabledFor(Level.INFO)){
                getLogger().info("[consultaMontoPagadoBeneficiario][" + rutBeneficiario + "][BCI_INI]");
                getLogger().info("[consultaMontoPagadoBeneficiario][" + rutBeneficiario + "] cuenta Destino ["+ cuentaDestino +"], cuenta Origen ["+ cuentaOrigen +"], fecha Pago ["+ fechaPago +"]");
            }

            HashMap parametros = new HashMap();
            String rutBenef = StringUtil.rellenaPorLaIzquierda(String.valueOf(rutBeneficiario), LARGO_RUT, '0');

            parametros.put("CHAINED_SUPPORT", "true");
            parametros.put("rutBeneficiario", rutBenef == null ? "" : rutBenef);
            parametros.put("cuentaDestino", cuentaDestino == null ? "" : cuentaDestino);
            parametros.put("cuentaOrigen", cuentaOrigen == null ? "" : cuentaOrigen);
            parametros.put("fecha", fechaPago == null ? "" : FechasUtil.convierteDateAString(fechaPago,
                "MM/dd/yyyy"));

            String contexto = TablaValores.getValor("JNDIConfig.parametros", "cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
            double montoPagadoBenef =
                Double.parseDouble(String.valueOf(conector.ejecutar("banele", "consultaMontoPagadoBenef",
                    parametros)));

            if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[consultaMontoPagadoBeneficiario][" + rutBeneficiario + "][BCI_FINOK] montoPagadoBenef[" + montoPagadoBenef + "]");}
            return montoPagadoBenef;

        }
        catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[consultaMontoPagadoBeneficiario][" + rutBeneficiario + "][BCI_FINEX][Exception e]" + e.getMessage(), e); }
            throw e instanceof GeneralException ? (GeneralException) e : new GeneralException("ESPECIAL",
                ErroresUtil.extraeStackTrace(e));
        }
    }

    /**
     * M�todo que actualiza el estado de una pago
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 06/11/2014 Pablo Romero C�ceres (SEnTRA) : versi�n incial.
     * </ul>
     * @param pago Pago con estado a actualizar.
     * @return <b>true</b> si se pudo actualizar.
     * @throws GeneralException en caso de error.
     * @since 3.1
     */
    public boolean actualizarEstadoPago(InfoDetalleNomina pago) throws GeneralException{
        
        String numTransferencia = pago.getNumTransferencia();
        try{
            if(getLogger().isEnabledFor(Level.INFO)){
                getLogger().info("[actualizarEstadoPago][" + numTransferencia + "][BCI_INI]");
                getLogger().info("[actualizarEstadoPago][" + numTransferencia + "] pago [" + pago.toString() + "]");
            }
            HashMap parametros = new HashMap();
            parametros.put("trf_num", pago.getNumTransferencia());
            Integer numeroPago = new Integer((int) pago.getIndicador());
            parametros.put("num_pag", numeroPago);
            parametros.put("nvo_est", pago.getEstado());
            String rutEmpresa = StringUtil.rellenaConCeros(pago.getRutEmpresa(), LARGO_RUT);
            String numeroConvenio = StringUtil.rellenaConEspacios(pago.getNumConvenio(), LARGO_CONVENIO);
            parametros.put("emp_rut", rutEmpresa);
            parametros.put("cnv_num", numeroConvenio);
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(
                TablaValores.getValor("JNDIConfig.parametros", "cluster", "param"));
            
            int respuesta = ((Integer) conector.ejecutar("banele", "actualizarEstadoPago", parametros)).intValue();
            
            if(getLogger().isEnabledFor(Level.INFO)){
                getLogger().info("[actualizarEstadoPago][" + numTransferencia + "][BCI_FINOK] respuesta ["+ respuesta+"]");
            }
            return respuesta == 1;
        }
        catch(Exception e){
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[actualizarEstadoPago][" + numTransferencia + "][BCI_FINEX][Exception e]" + e.getMessage(), e);}
            throw e instanceof GeneralException ? (GeneralException) e : new GeneralException("ESPECIAL",
                ErroresUtil.extraeStackTrace(e));
        }
    }

    /**
     * M�todo que actualiza los totales de una n�mina.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 06-11-2014 Pablo Romero C�ceres (SEnTRA) : versi�n incial.
     * </ul>
     * @param numeroTransferencia N�mero de la Transferencia.
     * @return <b>true</b> si se pudo actualizar.
     * @throws GeneralException en caso de error.
     * @since 3.1
     */
    public boolean actualizarTotalesNomina(String numeroTransferencia) throws GeneralException{
        try{
            if(getLogger().isEnabledFor(Level.INFO)){
                getLogger().info("[actualizarTotalesNomina][" + numeroTransferencia + "][BCI_INI]");
            }
            HashMap parametros = new HashMap();
            parametros.put("numeroTransferencia", numeroTransferencia);

            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(
                TablaValores.getValor("JNDIConfig.parametros", "cluster", "param"));
            String respuesta = (String) conector.ejecutar("banele", "actualizarTotalesNomina", parametros);
            if (respuesta.equals("NEX")){
                throw new GeneralException("ESPECIAL", "No existe en la TOP");
            }
            if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[actualizarTotalesNomina][" + numeroTransferencia + "][BCI_FINOK] respuesta [" + respuesta +"]");}
            return respuesta.equals("OK");
        }
        catch(Exception e){
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[actualizarTotalesNomina][" + numeroTransferencia + "][BCI_FINEX][Exception e]" + e.getMessage(), e); }
            throw e instanceof GeneralException ? (GeneralException) e : new GeneralException("ESPECIAL",
                ErroresUtil.extraeStackTrace(e));
        }
    }

    /**
     * Metodo para actualizar los totales de las nominas observadas al eliminar el registro.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li> 1.0 30/10/2014 Sergio Cuevas Diaz (SEnTRA) : versi�n inicial.
     * </ul>
     * </p>
     * 
     * @param fecha fecha a consultar.
     * @return TotalTO respuesta de la actualizacion de totales.
     * @throws Exception en caso de error.
     * @since 3.3
     */
    public TotalTO[] obtenerResumenNominas(FiltroConsultaTO filtro) throws Exception{
        
    	long rutEmpresa = filtro.getRutEmpresa();
        String convenio = filtro.getConvenio();
        try{
            if(getLogger().isEnabledFor(Level.INFO)){
                getLogger().info("[obtenerResumenNominas][" + rutEmpresa + "][" + convenio + "][BCI_INI]");
                getLogger().info("[obtenerResumenNominas][" + rutEmpresa + "][" + convenio + "] filtro ["+ filtro.toString() +"]");
            }
            
            HashMap parametros = new HashMap();
            parametros.put("CHAINED_SUPPORT", "true");
            parametros.put("fechaPagoDesde",filtro.getFechaPagoDesde() == null ? "" 
                : FechasUtil.convierteDateAString(filtro.getFechaPagoDesde(), "MM/dd/yyyy"));
            parametros.put("fechaPagoHasta",filtro.getFechaPagoHasta() == null ? "" 
                : FechasUtil.convierteDateAString(filtro.getFechaPagoHasta(), "MM/dd/yyyy"));
            parametros.put("tipoTransferencia", filtro.getTipoTransferencia() == null 
                ? "" : filtro.getTipoTransferencia());
            parametros.put("rangoFirmada", new Integer(filtro.getRangoFirmada()));
            parametros.put("rangoEnProceso", new Integer(filtro.getRangoEnProceso()));
            parametros.put("rangoBloqueada", new Integer(filtro.getRangoBloqueada()));
            parametros.put("productoPago", filtro.getProductoPago() == null 
                ? "" : filtro.getProductoPago());
            String contexto = TablaValores.getValor("JNDIConfig.parametros", "cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
            List resultado = conector.consultar("banele", "obtenerResumenNominas", parametros);
            if (resultado != null && resultado.size() > 0) {
                TotalTO[] listaResumen = new TotalTO[resultado.size()];
                for(int i = 0;i < resultado.size();i++){
                    HashMap datosGenerales = (HashMap) resultado.get(i);
                    listaResumen[i]  = new TotalTO();
                    listaResumen[i].setTotalRegistros(((Integer)(datosGenerales.get("cantidad"))).intValue() );
                    listaResumen[i].setMontoTotal(((Double) datosGenerales.get("monto")).doubleValue());
                    listaResumen[i].setTipoPago(((String) datosGenerales.get("codigo")).trim());
                    listaResumen[i].setDescripcionTipoPago(((String) datosGenerales.get("descripcion")).trim());
                }
                if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[obtenerResumenNominas][" + rutEmpresa + "][" + convenio + "][BCI_FINOK] Pagos obtenidos ["+ StringUtil.contenidoDe(listaResumen)+ "]"); }
                return listaResumen;
            }
            if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[obtenerResumenNominas][" + rutEmpresa + "][" + convenio + "][BCI_FINEX] Lista pagos obtenidos es null "); }
            return null;
        }
        catch(Exception e){
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[obtenerResumenNominas][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][Exception e]"+ e.getMessage(), e); }
            throw e instanceof GeneralException ? (GeneralException) e 
                : new GeneralException("ESPECIAL", ErroresUtil.extraeStackTrace(e));
        }
    }

    /**
     * M�todo utilizado para consultar nominas por numero de transferencia o fecha.
     *
     * <p>
     * Registro de versiones:<ul>
     * <li> 1.0 30/10/2014 Sergio Cuevas Diaz (SEnTRA) : versi�n inicial.
     * </ul>
     * </p>
     * 
     * @param filtro objeto con los datos a consultar.
     * @return arreglo con las n�minas obtenidas.
     * @throws Exception en caso de error.
     * @since 3.3
     */
    public InformacionCargaMasiva[] consultarNominasConProblemasPorFiltro(FiltroConsultaTO filtro)throws Exception{
        
    	long rutEmpresa = filtro.getRutEmpresa();
        String convenio = filtro.getConvenio();
        
        try{
            if(getLogger().isEnabledFor(Level.INFO)){
                getLogger().info("[consultarNominasConProblemasPorFiltro][" + rutEmpresa + "][" + convenio + "][BCI_INI]");
                getLogger().info("[consultarNominasConProblemasPorFiltro][" + rutEmpresa + "][" + convenio + "] filtro ["+ filtro.toString() +"]");
            }
            
            HashMap parametros = new HashMap();
            parametros.put("CHAINED_SUPPORT", "true");
            parametros.put("fechaPagoDesde",filtro.getFechaPagoDesde() == null ? "" 
                : FechasUtil.convierteDateAString(filtro.getFechaPagoDesde(), "MM/dd/yyyy"));
            parametros.put("fechaPagoHasta",filtro.getFechaPagoHasta() == null ? "" 
                : FechasUtil.convierteDateAString(filtro.getFechaPagoHasta(), "MM/dd/yyyy"));
            parametros.put("tipoTransferencia", filtro.getTipoTransferencia() == null 
                ? "" : filtro.getTipoTransferencia());
            parametros.put("rangoFirmada", new Integer(filtro.getRangoFirmada()));
            parametros.put("rangoEnProceso", new Integer(filtro.getRangoEnProceso()));
            parametros.put("rangoBloqueada", new Integer(filtro.getRangoBloqueada()));
            parametros.put("productoPago", filtro.getProductoPago() == null 
                ? "" : filtro.getProductoPago());
            parametros.put("pagina", new Integer(filtro.getPagina()));
            parametros.put("cantRegistros", new Integer(filtro.getCantRegistros()));
            String contexto = TablaValores.getValor("JNDIConfig.parametros", "cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
            List resultado = conector.consultar("banele", "consultaNominasREC", parametros);
            InformacionCargaMasiva[] infoCarga = null;
            HashMap datosGenerales = null;
            if (resultado != null && resultado.size() > 0) {
                infoCarga = new InformacionCargaMasiva[resultado.size()];
                for(int i = 0;i < resultado.size();i++){
                    datosGenerales = (HashMap) resultado.get(i);
                    infoCarga[i]  = new InformacionCargaMasiva();
                    infoCarga[i].setNumTransferencia(datosGenerales.get("numeroTransferencia").toString().trim());
                    infoCarga[i].setRutEmpresa(Long.parseLong(((String) datosGenerales.get("rutEmpresa")).trim()));
                    infoCarga[i].setDvEmpresa(((String) datosGenerales.get("dvEmpresa")).charAt(0));
                    infoCarga[i].setCnvEmpresa(datosGenerales.get("numeroConvenio").toString().trim());
                    infoCarga[i].setNombreEmpresa(datosGenerales.get("razonSocial").toString().trim());
                    infoCarga[i].setCtaOrigen(datosGenerales.get("numeroCuenta").toString().trim());
                    infoCarga[i].setNombreArchivo(datosGenerales.get("nombreNomina").toString().trim());
                    infoCarga[i].setFechaPago((Date) datosGenerales.get("fechaPago"));
                    infoCarga[i].setFechaEnvio((Date) datosGenerales.get("fechaCarga"));
                    infoCarga[i].setTipoTransferencia(datosGenerales.get("tipoTransferencia").toString().trim());
                    infoCarga[i].setEstado(datosGenerales.get("codigoEstado").toString().trim());
                    infoCarga[i].setDescripcionEstado(datosGenerales.get("glosaEstado").toString().trim());
                    infoCarga[i].setEstadoProceso(datosGenerales.get("codigoEstadoProceso").toString().trim());
                    infoCarga[i].setDescripcionEstadoProceso(
                        datosGenerales.get("glosaEstadoProceso").toString().trim());
                    infoCarga[i].setModalidadDeCargo(
                        ((Integer)datosGenerales.get("modalidadCargo")).intValue());
                    infoCarga[i].setDescripcionIndicadorModulo(
                        datosGenerales.get("glosaModalidadCargo").toString().trim());
                    infoCarga[i].setEstadoProcesoCCT(((Integer)datosGenerales.get("estadoProcesoCCT")).intValue());
                    infoCarga[i].setEstadoProcesoCCA(((Integer)datosGenerales.get("estadoProcesoCCA")).intValue());
                    infoCarga[i].setEstadoProcesoOPF(((Integer)datosGenerales.get("estadoProcesoOPF")).intValue());
                    String problema=datosGenerales.get("glosaTipoProblema").toString().trim();
                    if (!problema.equals("")){
                        infoCarga[i].setTipoProblema(datosGenerales.get("glosaTipoProblema").toString().trim());
                    }
                    else{
                        int estado = ((Integer) datosGenerales.get("estadoProcesoOPF")).intValue();
                        infoCarga[i].setTipoProblema(TablaValores.getValor(TABLA_ESTADO_OPF, "estadosOPF_"
                            + estado, "Desc"));
                    }
                    TotalTO[] totalesPagos = null;
                    totalesPagos = new TotalTO[1];
                    totalesPagos[0] = new TotalTO();
                    totalesPagos[0].setTotalRegistros(((Integer) datosGenerales.get("cantidadPagos")).intValue());
                    totalesPagos[0].setTotalRegistrosAceptados(
                        ((Integer) datosGenerales.get("cantidadPagosAceptados")).intValue());
                    totalesPagos[0].setTotalRegistrosRechazados(
                        ((Integer) datosGenerales.get("cantidadPagosRechazados")).intValue());
                    totalesPagos[0].setMontoTotal(
                        new Double(datosGenerales.get("montoPago").toString().trim()).doubleValue());
                    totalesPagos[0].setMontoTotalAceptados(
                        new Double(datosGenerales.get("montoPagoAceptado").toString().trim()).doubleValue());
                    totalesPagos[0].setMontoTotalRechazados(
                        new Double(datosGenerales.get("montoPagoRechazado").toString().trim()).doubleValue());
                    infoCarga[i].setTotales(totalesPagos);
                    infoCarga[i].setCantidadPaginas(((Integer) datosGenerales.get("cantidadPagina")).intValue());
                    infoCarga[i].setCantidadNominas(
                        ((Integer) datosGenerales.get("cantidadRegistros")).intValue());
                    infoCarga[i].setTotales(totalesPagos);
                }
            }
            if(getLogger().isEnabledFor(Level.INFO)){ getLogger().info("[consultarNominasConProblemasPorFiltro][" + rutEmpresa + "][" + convenio + "][BCI_FINOK] Pagos obtenidos ["+ infoCarga +"]"); }
            return infoCarga;
        }
        catch(Exception e){
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[consultarNominasConProblemasPorFiltro][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][Exception e]"+ e.getMessage(), e);}
            throw e instanceof GeneralException ? (GeneralException) e 
                : new GeneralException("ESPECIAL", ErroresUtil.extraeStackTrace(e));
        }
    }

    /**
     * M�todo utilizado para consultar nominas por numero de transferencia o fecha.
     *
     * <p>
     * Registro de versiones:<ul>
     * <li> 1.0 30/10/2014 Sergio Cuevas Diaz (SEnTRA) : versi�n inicial.
     * </ul>
     * </p>
     * 
     * @param filtro objeto con los datos a consultar.
     * @return arreglo con las n�minas obtenidas.
     * @throws Exception en caso de error.
     * @since 3.3
     */
    public InformacionCargaMasiva[] consultarNominasEnProcesoPorFiltro(FiltroConsultaTO filtro)throws Exception{
        
    	long rutEmpresa = filtro.getRutEmpresa();
        String convenio = filtro.getConvenio();
        try{
            if(getLogger().isEnabledFor(Level.INFO)){
                getLogger().info("[consultarNominasEnProcesoPorFiltro][" + rutEmpresa + "][" + convenio + "][BCI_INI]");
                getLogger().info("[consultarNominasEnProcesoPorFiltro][" + rutEmpresa + "][" + convenio + "] filtro ["+ filtro.toString() +"]");
            }
            HashMap parametros = new HashMap();
            parametros.put("CHAINED_SUPPORT", "true");
            parametros.put("rutEmpresa", String.valueOf(filtro.getRutEmpresa()).equals("0") 
                ? "" : String.valueOf(filtro.getRutEmpresa()));
            parametros.put("numeroConvenio", filtro.getConvenio() == null ? "" : filtro.getConvenio());
            parametros.put("cuentaCargo", filtro.getCuentaOrigen() == null ? "" : filtro.getCuentaOrigen());
            parametros.put("etiqueta", filtro.getEtiqueta() == null ? "" : filtro.getEtiqueta());
            parametros.put("fechaPagoDesde", filtro.getFechaPagoDesde() == null 
                ? "" : FechasUtil.convierteDateAString(filtro.getFechaPagoDesde(), "MM/dd/yyyy"));
            parametros.put("fechaPagoHasta", filtro.getFechaPagoHasta() == null 
                ? "" : FechasUtil.convierteDateAString(filtro.getFechaPagoHasta(), "MM/dd/yyyy"));
            parametros.put("numeroTransferencia", filtro.getCodigoIndentificacion() == null 
                ? "" : filtro.getCodigoIndentificacion());
            parametros.put("estado", filtro.getEstado() == null ? "" : filtro.getEstado());
            parametros.put("tipoTransferencia", filtro.getTipoTransferencia() == null 
                ? "" : filtro.getTipoTransferencia());
            parametros.put("montoPagarDesde", new Double(filtro.getMontoPagoDesde()));
            parametros.put("montoPagarHasta", new Double(filtro.getMontoPagoHasta()));
            parametros.put("canal", filtro.getCanal() == null ? "" : filtro.getCanal());
            parametros.put("origen", filtro.getOrigen() == null ? "" : filtro.getOrigen());
            parametros.put("productoPago", filtro.getProductoPago() == null 
                ? "" : filtro.getProductoPago());
            parametros.put("pagina", new Integer(filtro.getPagina()));
            parametros.put("cantRegistros", new Integer(filtro.getCantRegistros()));
            String contexto = TablaValores.getValor("JNDIConfig.parametros", "cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
            List resultado = conector.consultar("banele", "consultaNominasPRC", parametros);
            InformacionCargaMasiva[] infoCarga = null;
            HashMap datosGenerales = null;
            if (resultado != null && resultado.size() > 0) {
                infoCarga = new InformacionCargaMasiva[resultado.size()];
                for(int i = 0;i < resultado.size();i++){
                    datosGenerales = (HashMap) resultado.get(i);
                    infoCarga[i]  = new InformacionCargaMasiva();
                    infoCarga[i].setNumTransferencia(datosGenerales.get("numeroTransferencia").toString().trim());
                    infoCarga[i].setRutEmpresa(Long.parseLong(((String) datosGenerales.get("rutEmpresa")).trim()));
                    infoCarga[i].setDvEmpresa(((String) datosGenerales.get("dvEmpresa")).charAt(0));
                    infoCarga[i].setCnvEmpresa(datosGenerales.get("numeroConvenio").toString().trim());
                    infoCarga[i].setNombreEmpresa(datosGenerales.get("razonSocial").toString().trim());
                    infoCarga[i].setCtaOrigen(datosGenerales.get("numeroCuenta").toString().trim());
                    infoCarga[i].setNombreArchivo(datosGenerales.get("nombreNomina").toString().trim());
                    infoCarga[i].setFechaPago((Date) datosGenerales.get("fechaPago"));
                    infoCarga[i].setFechaEnvio((Date) datosGenerales.get("fechaCarga"));
                    infoCarga[i].setTipoTransferencia(datosGenerales.get("tipoTransferencia").toString().trim());
                    infoCarga[i].setEstado(datosGenerales.get("codigoEstado").toString().trim());
                    infoCarga[i].setDescripcionEstado(datosGenerales.get("glosaEstado").toString().trim());
                    infoCarga[i].setEstadoProceso(datosGenerales.get("codigoEstadoProceso").toString().trim());
                    infoCarga[i].setDescripcionEstadoProceso(
                        datosGenerales.get("glosaEstadoProceso").toString().trim());
                    infoCarga[i].setModalidadDeCargo(
                        ((Integer)datosGenerales.get("modalidadCargo")).intValue());
                    infoCarga[i].setDescripcionIndicadorModulo(
                        datosGenerales.get("glosaModalidadCargo").toString().trim());
                    infoCarga[i].setEstadoProcesoOPF(((Integer)datosGenerales.get("estadoProcesoOPF")).intValue());
                    infoCarga[i].setDescripcionEstadoProcesoOPF(
                        TablaValores.getValor(TABLA_ESTADO_OPF, "estadosOPF_" + (String.valueOf((
                            (Integer) datosGenerales.get("estadoProcesoOPF")).intValue())), "Desc"));
                    TotalTO[] totalesPagos = null;
                    totalesPagos = new TotalTO[1];
                    totalesPagos[0] = new TotalTO();
                    totalesPagos[0].setTotalRegistros(((Integer) datosGenerales.get("cantidadPagos")).intValue());
                    totalesPagos[0].setTotalRegistrosAceptados(
                        ((Integer) datosGenerales.get("cantidadPagosAceptados")).intValue());
                    totalesPagos[0].setTotalRegistrosRechazados(
                        ((Integer) datosGenerales.get("cantidadPagosRechazados")).intValue());
                    totalesPagos[0].setMontoTotal(
                        new Double(datosGenerales.get("montoPago").toString().trim()).doubleValue());
                    totalesPagos[0].setMontoTotalAceptados(
                        new Double(datosGenerales.get("montoPagoAceptado").toString().trim()).doubleValue());
                    totalesPagos[0].setMontoTotalRechazados(
                        new Double(datosGenerales.get("montoPagoRechazado").toString().trim()).doubleValue());
                    infoCarga[i].setTotales(totalesPagos);
                    infoCarga[i].setCantidadPaginas(((Integer) datosGenerales.get("cantidadPagina")).intValue());
                    infoCarga[i].setCantidadNominas(
                        ((Integer) datosGenerales.get("cantidadRegistros")).intValue());
                }
            }
            if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[consultarNominasEnProcesoPorFiltro][" + rutEmpresa + "][" + convenio + "][BCI_FINOK] Pagos obtenidos [" + infoCarga+ "]");}
            return infoCarga;
        }
        catch(Exception e){
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[consultarNominasEnProcesoPorFiltro][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][Exception e]"+ e.getMessage(), e); }
            throw e instanceof GeneralException ? (GeneralException) e 
                : new GeneralException("ESPECIAL", ErroresUtil.extraeStackTrace(e));
        }
    }


    /**
     * M�todo que obtiene un listado de los pagos realizados a trav�s de carga masiva, y consultando seg�n ciertos
     * filtros. 
     * <p>
     * <b>Registro de versiones:</b>
     * <ul>
     * <li> 1.0 13-11-2014, Angel Cris�stomo (Imagemaker IT): versi�n inicial</li>
     * <li> 1.1 29/12/2014, Rafael Pizarro (TINet): Se normaliza log de metodo
     * para ajustarse a Normativa Log4j de BCI.</li>
     * </ul>
     * </p> 
     * 
     * @param filtro los filtros a aplicar a la consulta.
     * @param tipoTransferencia tipo transferencia.
     * @param canal canal.
     * @throws Exception exception.
     * @return Listado con los pagos realizados a trav�s de carga masiva.
     * @since 3.2
     */
    public EncabezadoTransferenciaVO[] obtenerEncabezadoPagosRealizados(FiltroConsultarTransferenciasVO filtro,
        String tipoTransferencia, String canal) throws Exception {

    	String rutEmpresa = filtro.getRutOrigen();
        String convenio = filtro.getConvenio();
        try {
            if(getLogger().isEnabledFor(Level.INFO)){
                getLogger().info("[obtenerEncabezadoPagosRealizados][" + rutEmpresa + "][" + convenio + "][BCI_INI]");
                getLogger().info("[obtenerEncabezadoPagosRealizados][" + rutEmpresa + "][" + convenio + "] tipo Transferencia ["+ tipoTransferencia +"], canal ["+ canal +"], filtro ["+ filtro +"]");
            }
            List documento = null;
            HashMap salida = null;
            HashMap parametros = new HashMap();
            EncabezadoTransferenciaVO[] transferenciasPendientesVO = null;
            parametros.put("CHAINED_SUPPORT", "true");
            parametros.put("rutEmpresa", filtro.getRutOrigen());
            parametros.put("convenio", filtro.getConvenio());
            parametros.put("numTransferencia", filtro.getNumeroTransferencia());
            parametros.put("rutDestino", filtro.getRutDestino() == null ? "" :filtro.getRutDestino());
            parametros.put("fechaDesde", filtro.getFechaDesde());
            parametros.put("fechaHasta", filtro.getFechaHasta());
            parametros.put("tipoTransferencia", tipoTransferencia);
            parametros.put("canal", canal);

            String contexto = TablaValores.getValor("JNDIConfig.parametros", "cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
            documento = conector.consultar("banele", "obtenerEncabezadoPagosRealizados", parametros);

            if (documento != null) {
                if (documento.size() > 0) {
                    transferenciasPendientesVO = new EncabezadoTransferenciaVO[documento.size()];
                    if (log.isEnabledFor(Level.INFO)){getLogger().info("[obtenerEncabezadoPagosRealizados][" + rutEmpresa + "][" + convenio + "] pagosPendientes [" + documento.size() + "]"); }
                    for (int i = 0; i < documento.size(); i++) {
                        salida = (HashMap) documento.get(i);
                        transferenciasPendientesVO[i] = new EncabezadoTransferenciaVO();
                        transferenciasPendientesVO[i].setNumeroTransferencia(String.valueOf(
                            salida.get("numeroOperacion")).trim());
                        transferenciasPendientesVO[i].setCantidadDetalles(Integer.parseInt(String.valueOf(
                            salida.get("cantidadDetalles")).trim()));
                        transferenciasPendientesVO[i].setNombreTransferencia(String.valueOf(
                            salida.get("nomTransferencia")).trim());
                        transferenciasPendientesVO[i].setNombreArchivo(String.valueOf(salida.get("nomArchivo"))
                            .trim());
                        transferenciasPendientesVO[i].setCuentaOrigen(StringUtil.sacaCeros(String.valueOf(
                            salida.get("cuentaOrigen")).trim()));
                        transferenciasPendientesVO[i].setMonto(Long.parseLong(String.valueOf(
                            salida.get("montoTransferido")).trim()));
                        String codigoTipo = String.valueOf(salida.get("tipo")).trim();
                        transferenciasPendientesVO[i].setTipo(codigoTipo);
                        transferenciasPendientesVO[i].setGlosaTipo(TablaValores.getValor(TABLA_TRANSFERENCIAS,
                            codigoTipo, "Desc"));
                        String codigoEstado = String.valueOf(salida.get("estado")).trim();
                        transferenciasPendientesVO[i].setEstado(codigoEstado);
                        transferenciasPendientesVO[i].setGlosaEstado(TablaValores.getValor(TABLA_TRANSFERENCIAS,
                            codigoEstado, "Desc"));
                        transferenciasPendientesVO[i].setFechaCreacion((Date) salida.get("fechaPago"));
                        transferenciasPendientesVO[i].setNumeroFirmas(Integer.parseInt(String.valueOf(
                            salida.get("hayFirmas")).trim()));

                    }
                }
            }

            if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[obtenerEncabezadoPagosRealizados][" + rutEmpresa + "][" + convenio + "][BCI_FINOK]transferenciasPendientesVO [" + StringUtil.contenidoDe(transferenciasPendientesVO) + "]");}
            return transferenciasPendientesVO;

        }
        catch (EjecutarException eE) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[obtenerEncabezadoPagosRealizados][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][EjecutarException eE] [" +  eE.getDetalleException() + "]");}
            throw new Exception(eE.getDetalleException());
        }
        catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[obtenerEncabezadoPagosRealizados][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][Exception e][" + ErroresUtil.extraeStackTrace(e) + "]");}
            throw new Exception(e.getMessage());
        }

    }

    /**
     *  M�todo que obtiene un listado de transferencias realizadas, consultando seg�n ciertos criterios.
     * <p>
     * <b>Registro de versiones:</b>
     * <ul>
     * <li> 1.0 13-11-2014, Angel Cris�stomo (Imagemaker IT): versi�n inicial</li>
     * <li> 1.1 29/12/2014 Rafael Pizarro (TINet): Se normaliza log de metodo
     * para ajustarse a Normativa Log4j de BCI.</li>
     * </ul>
     * </p> 
     * 
     * @param filtro los filtros a aplicar a la consulta
     * @param tipoTransferencia tipo transferencia.
     * @param canal canal.
     * @param origen origen.
     * @throws Exception Exception.
     * @return Listado con las transferencias realizadas.
     * @since 3.2
     */
    public DetalleTransferenciaVO[] obtenerDetallePagosRealizados(FiltroConsultarTransferenciasVO filtro,
        String tipoTransferencia, String canal, String origen) throws Exception {

    	String rutEmpresa = filtro.getRutOrigen();
        String convenio = filtro.getConvenio();
        try {
            if(getLogger().isEnabledFor(Level.INFO)){
                getLogger().info("[obtenerDetallePagosRealizados][" + rutEmpresa + "][" + convenio + "][BCI_INI]");
                getLogger().info("[obtenerDetallePagosRealizados][" + rutEmpresa + "][" + convenio + "] tipo transferencia ["+ tipoTransferencia +"], canal ["+ canal +"], origen ["+ origen +"], filtro ["+ filtro.toString() +"]");
            }
            final int tres=3;
            List documento = null;
            HashMap salida = null;
            HashMap parametros = new HashMap();
            DetalleTransferenciaVO[] transferenciasRealizadasVO = null;
            parametros.put("CHAINED_SUPPORT", "true");
            parametros.put("convenio", filtro.getConvenio());
            parametros.put("rutEmpresa", filtro.getRutOrigen());
            parametros.put("rutDestino", filtro.getRutDestino() == null ? "" :filtro.getRutDestino());
            parametros.put("estado", filtro.getEstado());
            parametros.put("numTransferencia", filtro.getNumeroTransferencia());
            parametros.put("tipoTransferencia", tipoTransferencia);
            parametros.put("canal", canal);
            parametros.put("origen", origen);

            String contexto = TablaValores.getValor("JNDIConfig.parametros", "cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
            documento = conector.consultar("banele", "obtenerDetallePagosRealizados", parametros);

            if (documento != null) {
                if (documento.size() > 0) {
                    String codigoRechazo = "";
                    String glosaRechazo = "";
                    String vuelveIntentar = "";
                    transferenciasRealizadasVO = new DetalleTransferenciaVO[documento.size()];
                    if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[obtenerDetallePagosRealizados][" + rutEmpresa + "][" + convenio + "] transferenciasRealizadas [" + documento.size() + "]"); }
                    for (int i = 0; i < documento.size(); i++) {
                        codigoRechazo = "";
                        glosaRechazo = "";
                        vuelveIntentar = "";
                        salida = (HashMap) documento.get(i);
                        transferenciasRealizadasVO[i] = new DetalleTransferenciaVO();
                        transferenciasRealizadasVO[i].setNumeroTransferencia(String.valueOf(
                            salida.get("numeroOperacion")).trim());
                        transferenciasRealizadasVO[i].setNumeroDetalle(String.valueOf(salida.get("numeroDetalle"))
                            .trim());
                        transferenciasRealizadasVO[i].setCuentaOrigen(StringUtil.sacaCeros(String.valueOf(
                            salida.get("cuentaOrigen")).trim()));
                        transferenciasRealizadasVO[i].setMonto(Long.parseLong(String.valueOf(
                            salida.get("montoTransferido")).trim()));
                        transferenciasRealizadasVO[i].setCuentaDestino(StringUtil.sacaCeros(String.valueOf(
                            salida.get("cuentaDestino")).trim()));
                        transferenciasRealizadasVO[i].setNombreDestinatario(String.valueOf(
                            salida.get("nombreDestinatario")).trim());
                        transferenciasRealizadasVO[i].setRutDestino(Long.parseLong(String.valueOf(
                            salida.get("rutDestino")).trim()));
                        transferenciasRealizadasVO[i].setDvDestino(String.valueOf(salida.get("dVDestino")).trim()
                            .charAt(0));
                        String codigoBancoDestino = String.valueOf(salida.get("bancoDestino")).trim();
                        if (codigoBancoDestino.length() > tres) {
                            codigoBancoDestino = codigoBancoDestino.substring(codigoBancoDestino.length() - tres,
                                codigoBancoDestino.length());
                        }
                        transferenciasRealizadasVO[i].setBancoDestino(codigoBancoDestino);
                        String codigoEstado = String.valueOf(salida.get("estado")).trim();
                        transferenciasRealizadasVO[i].setEstado(codigoEstado);
                        transferenciasRealizadasVO[i].setGlosaEstado(TablaValores.getValor(TABLA_TRANSFERENCIAS,
                            codigoEstado, "Desc"));
                        transferenciasRealizadasVO[i].setFechaCreacion((Date) salida.get("fechaCreacion"));
                        transferenciasRealizadasVO[i].setFechaPago((Date) salida.get("fechaPago"));
                        transferenciasRealizadasVO[i].setNombreArchivo(String.valueOf(
                            salida.get("nomTransferencia")).trim());

                        String modalidad = String.valueOf(salida.get("modalidad")).trim();
                        transferenciasRealizadasVO[i].setModalidad(modalidad);
                        transferenciasRealizadasVO[i].setGlosaModalidad(TablaValores.getValor(
                            TABLA_TRANSFERENCIAS, "tipoModalidad", modalidad));
                        transferenciasRealizadasVO[i].setTipoIngreso(String.valueOf(salida.get("modalidad"))
                            .trim());

                        String codigoTipo = String.valueOf(salida.get("tipo")).trim();
                        transferenciasRealizadasVO[i].setTipo(codigoTipo);
                        String glosaTipo = TablaValores.getValor(TABLA_TRANSFERENCIAS, "tipoPagoTransferencia",
                            codigoTipo);
                        if (glosaTipo != null) {
                            transferenciasRealizadasVO[i].setGlosaTipo(glosaTipo);
                        }
                        else {
                            transferenciasRealizadasVO[i].setGlosaTipo("");
                        }

                        transferenciasRealizadasVO[i].setEmailDestino(String.valueOf(salida.get("emailDestino"))
                            .trim());
                        transferenciasRealizadasVO[i].setMensajeEmailDestino(String.valueOf(
                            salida.get("mensajeEmailDestino")).trim());
                        transferenciasRealizadasVO[i].setOrdenCompra(String.valueOf(salida.get("ordenCompra"))
                            .trim());
                        transferenciasRealizadasVO[i].setNumeroFactura(String.valueOf(salida.get("numeroFactura"))
                            .trim());

                        transferenciasRealizadasVO[i].setNombreUsuario(String.valueOf(salida.get("nombreUsuario"))
                            .trim());
                        codigoRechazo = String.valueOf(salida.get("rechazo")).trim();
                        transferenciasRealizadasVO[i].setRechazo(codigoRechazo);
                        transferenciasRealizadasVO[i].setMotivoRechazo(String.valueOf(salida.get("motivoRechazo"))
                            .trim());
                        if (!codigoRechazo.equals("")) {
                            if (codigoRechazo.equals(ERROR_TIMEOUT)) {
                                glosaRechazo = TablaValores.getValor(ARCH_PARAM_ERR, codigoRechazo, "Desc");
                            }
                            else {
                                if ("C".equals(String.valueOf(codigoRechazo.charAt(0)))) {
                                    codigoRechazo = "0"
                                        + codigoRechazo.substring(codigoRechazo.length() - tres,
                                            codigoRechazo.length()).trim();
                                    glosaRechazo = TablaValores.getValor(TABLA_TTFF_TERCEROS, codigoRechazo,
                                        "mensaje");

                                    vuelveIntentar = TablaValores.getValor(TABLA_TTFF_TERCEROS, codigoRechazo,
                                        "vuelveIntentar");
                                }
                                else
                                    glosaRechazo = TablaValores.getValor(TABLA_ERROR_TRF, codigoRechazo, "Desc");
                            }
                            if (glosaRechazo != null) {
                                transferenciasRealizadasVO[i].setMotivoRechazo(glosaRechazo);
                            }
                            if (vuelveIntentar != null) {
                                transferenciasRealizadasVO[i].setVuelveIntentar(vuelveIntentar);
                            }

                        }


                    }
                }
            }

            if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[obtenerDetallePagosRealizados][" + rutEmpresa + "][" + convenio + "][BCI_FINOK] Retornando transferenciasRealizadasVO [" + StringUtil.contenidoDe(transferenciasRealizadasVO) + "]"); }
            return transferenciasRealizadasVO;

        }
        catch (EjecutarException eE) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[obtenerDetallePagosRealizados][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][EjecutarException eE][" + eE.getDetalleException() + "]"); }
            throw new Exception(eE.getDetalleException());
        }
        catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[obtenerDetallePagosRealizados][" + rutEmpresa + "][" + convenio + "][BCI_FINEX][Exception e][" + ErroresUtil.extraeStackTrace(e) + "]");}
            throw new Exception(e.getMessage());
        }

    }

    /**
     *  M�todo que obtiene un listado de transferencias realizadas, consultando seg�n ciertos criterios.
     * <p>
     * <b>Registro de versiones:</b>
     * <ul>
     * <li>1.0 27/10/2014, Darlyn Delgado Perez (SEnTRA): versi�n inicial</li>
     * </ul>
     * </p> 
     * 
     * @param transferenciaIngreso con datos a ingresar el detalle.
     * @throws Exception exception.
     * @return valor del ingreso del detalle.
     * @since 2.9
     */
    public boolean insertarDetalleTransferencia(TransferenciaATercerosTO transferenciaIngreso) 
        throws Exception {
        
        String numTransferencia = transferenciaIngreso.getIdentificador();
        try {
            if(getLogger().isEnabledFor(Level.INFO)){
                getLogger().info("[insertarDetalleTransferencia][" + numTransferencia + "][BCI_INI]");
                getLogger().info("[insertarDetalleTransferencia][" + numTransferencia + "]transferenciaIngreso ["+ transferenciaIngreso.toString() +"]");
            }
            HashMap parametros = new HashMap();
            parametros.put("CHAINED_SUPPORT", "true");
            parametros.put("numero_trf", transferenciaIngreso.getIdentificador());
            parametros.put("convenio", transferenciaIngreso.getNumConvenio());
            parametros.put("rut_empresa", String.valueOf(transferenciaIngreso.getRutEmpresa()));
            parametros.put("dv_empresa", String.valueOf(transferenciaIngreso.getDigitoVerifEmp()));
            parametros.put("cta_destino", transferenciaIngreso.getCtaDestino());
            parametros.put("nom_cuenta", transferenciaIngreso.getNombreCuentaInscrita());
            parametros.put("bco_destino", transferenciaIngreso.getCodBanco1());
            parametros.put("monto", new Double(transferenciaIngreso.getMontoTransfer()));
            parametros.put("estado", transferenciaIngreso.getEstado());
            parametros.put("codigoRechazo", transferenciaIngreso.getCodigoRechazo());
            parametros.put("tipoPago", transferenciaIngreso.getTipoPago());
            parametros.put("rut_destina", String.valueOf(transferenciaIngreso.getRutDestin()));
            parametros.put("dv_destina", String.valueOf(transferenciaIngreso.getDigitoVerificador()));     
            parametros.put("nom_destinarario", transferenciaIngreso.getNombreDestin());
            parametros.put("boleta_factura", transferenciaIngreso.getNumeroBoletaFactura());
            parametros.put("orden_compra", transferenciaIngreso.getNumeroOrdenCompra());      
            parametros.put("mensaje_dest", transferenciaIngreso.getMensajeDestinatario());
            parametros.put("correo_dest", transferenciaIngreso.getEmailDestinatario());
            String contexto = TablaValores.getValor("JNDIConfig.parametros", "cluster", "param");
            if (getLogger().isEnabledFor(Level.INFO)) {
                getLogger().info("[insertarDetalleTransferencia] contexto [" + contexto + "]");
            }
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
            int respuesta = Integer.parseInt(String.valueOf( 
                conector.ejecutar("banele","insertarDetalleTransferencia", parametros)).trim());

            if (respuesta == 1) {
                if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[insertarDetalleTransferencia][" + numTransferencia + "][BCI_FINOK] respuesta  [" + respuesta + "]");}
                return true; 
            }
            else {
                if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[insertarDetalleTransferencia][" + numTransferencia + "][BCI_FINEX] respuesta  [" + respuesta + "]");}
            return false;
            }

        }
        catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[insertarDetalleTransferencia][" + numTransferencia + "][BCI_FINEX][Exception e]" + e.getMessage(),e);}
            throw new Exception(e.getMessage());
        }
    }


    /**
     * Inserta encabezado de una transferencia.
     * <p>
     * <b>Registro de versiones:</b>
     * <ul>
     * <li> 1.0 27-11-2014, Angel Cris�stomo (Imagemaker IT): versi�n inicial</li>
     * </ul>
     * </p> 
     * 
     * @param transferencia Entidad de negocio.
     * @return String
     * @throws GeneralException Exception.
     * @since 3.4
     */
    public String insertarEncabezadoTransferencia(TransferenciaATercerosTO transferencia) throws GeneralException {
        String numeroTransferencia=null;


        String numtransferencia = transferencia.getIdentificador();
        
        try {
            if(getLogger().isEnabledFor(Level.INFO)){
                getLogger().info("[insertarEncabezadoTransferencia][" + numtransferencia + "][BCI_INI]");
                getLogger().info("[insertarEncabezadoTransferencia][" + numtransferencia + "] transferencia ["+ transferencia.toString() +"]");
            }
            HashMap parametros = new HashMap();

            parametros.put("CHAINED_SUPPORT", "true");
            parametros.put("rut_usuario", String.valueOf(transferencia.getRutUsuario()));
            parametros.put("dv_usuario", String.valueOf(transferencia.getDigitoVerifUsu()));
            parametros.put("convenio", transferencia.getNumConvenio());
            parametros.put("rut_empresa", String.valueOf(transferencia.getRutEmpresa()));
            parametros.put("dv_empresa", String.valueOf(transferencia.getDigitoVerifEmp()));
            parametros.put("cuenta_origen", transferencia.getCtaOrigen());
            parametros.put("glosa_transferencia", transferencia.getGlosa());
            parametros.put("estado_trf", transferencia.getEstado());
            parametros.put("fecha_pago", transferencia.getFechaPago());
            parametros.put("tipo_transferencia", transferencia.getTipoTrf());
            parametros.put("codigoRechazo", transferencia.getCodigoRechazo());
            parametros.put("nom_archivo", "");
            parametros.put("separador", "");
            parametros.put("modalidadCargo", new Integer(transferencia.getModalidadCargo()));
            parametros.put("canal", transferencia.getCanal());
            parametros.put("estado_trf_prc", transferencia.getEstadoProceso());
            parametros.put("prc_est_dif", new Integer(transferencia.getModalidadCargo()));
            parametros.put("prc_est_cct", new Integer(0));
            parametros.put("prc_est_cca", new Integer(0));
            parametros.put("prc_est_lbtr", new Integer(0));
            parametros.put("prc_est_cmx", new Integer(0));
            parametros.put("prc_est_opf", new Integer(0));


            String contexto = TablaValores.getValor("JNDIConfig.parametros", "cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
            numeroTransferencia = (String) conector.ejecutar("banele", "insertarEncabezadoTransferencia",
                parametros);

            if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[insertarEncabezadoTransferencia][" + numtransferencia + "][BCI_FINOK]numeroTransferencia ["+ numeroTransferencia +"]");}
            return numeroTransferencia ;
        }

        catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[insertarEncabezadoTransferencia][" + numtransferencia + "][BCI_FINEX][Exception e]" + e.getMessage(),e); }
            throw new GeneralException("ESPECIAL", ErroresUtil.extraeStackTrace(e));
        }
    }

    /**
     * Elimina operacion realizada sobre transferencia.
     * <p>
     * <b>Registro de versiones:</b>
     * <ul>
     * <li> 1.0 28-11-2014, Angel Cris�stomo (Imagemaker IT): versi�n inicial</li>
     * </ul>
     * </p> 
     * 
     * @param numeroTransferencia Numero de transferencia.
     * @return boolean
     * @throws GeneralException Exception de negocio.
     * @since 1.0
     */
    public boolean eliminarOperacionTransferencia(String numeroTransferencia) throws GeneralException {
        try {
            if(getLogger().isEnabledFor(Level.INFO)){
                getLogger().info("[eliminarOperacionTransferencia][" + numeroTransferencia + "][BCI_INI]");
                getLogger().info("[eliminarOperacionTransferencia][" + numeroTransferencia + "] numero Transferencia [" + numeroTransferencia + "]");
            }
            HashMap parametros = new HashMap();
            parametros.put("CHAINED_SUPPORT", "true");
            parametros.put("numeroTransferencia", numeroTransferencia);
            String contexto = TablaValores.getValor("JNDIConfig.parametros", "cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
            int respuesta = ((Integer) conector.ejecutar("banele", "eliminarOperacionTransferencia",
                parametros)).intValue();
            if(getLogger().isEnabledFor(Level.INFO)){
                getLogger().info("[eliminarOperacionTransferencia][" + numeroTransferencia + "][BCI_FInOK] respuesta ["+ respuesta +"]");
            }
            return respuesta == 1;

        }
        catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[eliminarOperacionTransferencia][" + numeroTransferencia + "][BCI_FINEX][Exception e]" + e.getMessage(),e); }
            throw new GeneralException("ESPECIAL", ErroresUtil.extraeStackTrace(e));
        }
    }

    /**
     * Inserta totales de una transferencia.
     * <p>
     * <b>Registro de versiones:</b>
     * <ul>
     * <li> 1.0 28-11-2014, Angel Cris�stomo (Imagemaker IT): versi�n inicial</li>
     * </ul>
     * </p> 
     * 
     * @param numeroTransferencia Numero de transferencia.
     * @return boolean
     * @throws GeneralException Exception de negocio.
     * @since 1.0
     */
    public boolean insertarTotalesTransferencia(String numeroTransferencia) throws GeneralException {
        try {

            if(getLogger().isEnabledFor(Level.INFO)){
                getLogger().info("[insertarTotalesTransferencia][" + numeroTransferencia + "][BCI_INI]");
                getLogger().info("[insertarTotalesTransferencia][" + numeroTransferencia + "] numTransferencia ["+ numeroTransferencia +"]");
            }

            HashMap parametros = new HashMap();
            parametros.put("CHAINED_SUPPORT", "true");
            parametros.put("numeroTransferencia", numeroTransferencia);
            String contexto = TablaValores.getValor("JNDIConfig.parametros", "cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
            int respuesta = ((Integer) conector.ejecutar("banele", "insertarTotalesTransferencia",
                parametros)).intValue();

            if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[insertarTotalesTransferencia][" + numeroTransferencia + "][BCI_FINOK] retornando objeto: " + respuesta); }
            return respuesta == 0;
        }
        catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[insertarTotalesTransferencia][" + numeroTransferencia + "][BCI_FINEX][Exception e]" + e.getMessage(),e);}
            throw new GeneralException("ESPECIAL", ErroresUtil.extraeStackTrace(e));
        }

    }

    /**
     * M�todo que consulta las transferencias de acuerdo al filtro ingresado.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 28/11/2014 Pablo Romero C�ceres (SEnTRA) : versi�n incial.</li>
     * <li>1.1 26/01/2015 Pablo Romero C�ceres (SEnTRA) : Se agrega la obtenci�n del rut del usuario.
     * <li>1.2 26/05/2015 Pablo Romero C�ceres (SEnTRA) : Se agrega el paso del d�gito verificador del destinatario
     * al SP.
     * </ul>
     * @param transferenciasAConsultar Filtro por el cual consultar� las transferencias.
     * @return Transferencias encontradas.
     * @throws GeneralException en caso de error.
     * @since 3.4
     */
    public TransferenciaATercerosTO[] obtieneTransferenciasDePago(FiltroTO transferenciasAConsultar) 
        throws GeneralException{
        
        long rutEmp = transferenciasAConsultar.getRutEmpresa();
        String conv = transferenciasAConsultar.getNumeroConvenio();
        try {
            if(getLogger().isEnabledFor(Level.INFO)){
                getLogger().info("[obtieneTransferenciasDePago][" + rutEmp + "][" + conv + "][BCI_INI]");
                getLogger().info("[obtieneTransferenciasDePago][" + rutEmp + "][" + conv + "] Transferencia a consultar ["+ transferenciasAConsultar.toString() +"]");
            }
            HashMap parametros = new HashMap();
            parametros.put("CHAINED_SUPPORT", "true");
            parametros.put("rutEmpresa", transferenciasAConsultar.getRutEmpresa() == 0 ? "" 
               : StringUtil.rellenaConCeros(
                   String.valueOf(transferenciasAConsultar.getRutEmpresa()), LARGO_RUT));
            parametros.put("numeroConvenio", transferenciasAConsultar.getNumeroConvenio() == null ? "" 
                : StringUtil.rellenaConEspacios(transferenciasAConsultar.getNumeroConvenio(), LARGO_CONVENIO));
            parametros.put("fechaPagoDesde", transferenciasAConsultar.getFechaPagoDesde() == null 
                ? "" : FechasUtil.convierteDateAString(
                transferenciasAConsultar.getFechaPagoDesde(), "yyyyMMdd"));
            parametros.put("fechaPagoHasta", transferenciasAConsultar.getFechaPagoHasta() == null 
                ? "" : FechasUtil.convierteDateAString(
                transferenciasAConsultar.getFechaPagoHasta(), "yyyyMMdd"));
            parametros.put("rutDestinatario", transferenciasAConsultar.getRutDestinatario() == 0 ? "" 
                : StringUtil.rellenaConCeros(
                    String.valueOf(transferenciasAConsultar.getRutDestinatario()), LARGO_RUT));
            parametros.put("dvDestinatario", String.valueOf(RUTUtil.calculaDigitoVerificador(transferenciasAConsultar.getRutDestinatario())));
            parametros.put("estado", transferenciasAConsultar.getEstado() == null 
                ? "" : transferenciasAConsultar.getEstado());
            parametros.put("canal", transferenciasAConsultar.getCanal() == null 
                ? "" : transferenciasAConsultar.getCanal());
            parametros.put("tipoTransferencia", transferenciasAConsultar.getTipoTrf() == null 
                ? "" : transferenciasAConsultar.getTipoTrf());
            parametros.put("traceNumber", transferenciasAConsultar.getTraceNumber() == null 
                ? "" : transferenciasAConsultar.getTraceNumber());
            parametros.put("numeroTransferencia", transferenciasAConsultar.getIdentificador() == null 
                ? "" : transferenciasAConsultar.getIdentificador());
            parametros.put("tip_pag", transferenciasAConsultar.getTipoPago() == null 
                ? "" : transferenciasAConsultar.getTipoPago());
            parametros.put("origen", transferenciasAConsultar.getOrigen() == null 
                ? "" : transferenciasAConsultar.getOrigen());
            parametros.put("productoPago", transferenciasAConsultar.getProductoPago() == null 
                ? "" : transferenciasAConsultar.getProductoPago());
            parametros.put("ctaDestinatario", transferenciasAConsultar.getCuentaDestino() == null 
                ? "" : transferenciasAConsultar.getCuentaDestino());
            parametros.put("cuentaCargo", transferenciasAConsultar.getCuentaOrigen() == null 
                ? "" : transferenciasAConsultar.getCuentaOrigen());
            parametros.put("montoPagarDesde", new Long(transferenciasAConsultar.getMontoTransferDesde()));
            parametros.put("montoPagarHasta", new Long(transferenciasAConsultar.getMontoTransferHasta()));
            parametros.put("cantidadRegistro", new Integer(transferenciasAConsultar.getCantidadRegistros()));
            parametros.put("aliasDestinatario", transferenciasAConsultar.getAliasDestinatario() == null ? "" : transferenciasAConsultar
                .getAliasDestinatario());

            ConectorServicioDePersistenciaDeDatos conector =
                new ConectorServicioDePersistenciaDeDatos(TablaValores.getValor("JNDIConfig.parametros", "cluster", "param"));

            List lista = conector.consultar("banele", "obtieneTransferenciasDePago", parametros);
            TransferenciaATercerosTO[] transferencias = null;
            if (lista != null) {
                transferencias = new TransferenciaATercerosTO[lista.size()];
                for (int i = 0; i < lista.size(); i++) {
                    HashMap datos = (HashMap) lista.get(i);
                    transferencias[i] = new TransferenciaATercerosTO();
                    transferencias[i].setIdentificador((String) datos.get("numeroTransferencia"));
                    transferencias[i].setNumConvenio((String) datos.get("numeroConvenio"));
                    String rutEmpresa = (String) datos.get("rutEmpresa");
                    transferencias[i].setRutEmpresa(Long.parseLong(rutEmpresa == null ? "0" : rutEmpresa.trim()));
                    String dvEmpresa = (String) datos.get("digitoVerificadorEmpresa");
                    transferencias[i].setDigitoVerifEmp(dvEmpresa == null ? ' ' : dvEmpresa.charAt(0));
                    transferencias[i].setCtaDestino((String) datos.get("cuentaDestino"));
                    transferencias[i].setNombreCuentaInscrita((String) datos.get("nombreCuenta"));
                    transferencias[i].setCodBanco1((String) datos.get("codigoBancoDestino"));
                    transferencias[i].setDescripcionBanco((String) datos.get("descripcionBancoDestino"));
                    Long monto = (Long) datos.get("monto");
                    transferencias[i].setMontoTransfer(monto == null ? 0 : monto.longValue());
                    transferencias[i].setEstado((String) datos.get("estado"));
                    transferencias[i].setDescripcionEstado((String) datos.get("descripcionEstado"));
                    transferencias[i].setCodigoRechazo((String) datos.get("codigoRechazo"));
                    transferencias[i].setDescripcionCodigoRechazo((String) datos.get("descripcionCodigoRechazo"));
                    transferencias[i].setTipoPago((String) datos.get("tipoPago"));
                    transferencias[i].setDescripcionTipoPago((String) datos.get("descripcionTipoPago"));
                    String rutDestin = (String) datos.get("rutDestinatario");
                    transferencias[i].setRutDestin(Long.parseLong(rutDestin == null ? "0" : rutDestin.trim()));
                    String dvDestin = (String) datos.get("dvDestinatario");
                    transferencias[i].setDigitoVerificador(dvDestin == null ? ' ' : dvDestin.charAt(0));
                    transferencias[i].setNombreDestin((String) datos.get("nombreDestinatario"));
                    transferencias[i].setNumeroBoletaFactura((String) datos.get("numeroFactura"));
                    transferencias[i].setNumeroOrdenCompra((String) datos.get("numeroOrdenCompra"));
                    transferencias[i].setMensajeDestinatario((String) datos.get("mensajeDestinatario"));
                    transferencias[i].setEmailDestinatario((String) datos.get("emailDestinatario"));
                    transferencias[i].setTraceNumber((String) datos.get("traceNumber"));
                    transferencias[i].setFechaPago((Date) datos.get("fechaPago"));
                    transferencias[i].setCtaOrigen((String) datos.get("cuentaCargo"));
                    Integer modalidadCargo = (Integer) datos.get("modalidadCargo");
                    transferencias[i].setModalidadCargo(modalidadCargo == null ? 0 : modalidadCargo.intValue());
                    transferencias[i].setDescripcionModalidadCargo((String) datos.get("glosaModalidadCargo"));
                    transferencias[i].setGlosa(((String) datos.get("nombreTransferencia")).trim());
                    transferencias[i].setTipoTrf((String) datos.get("tipoTransferencia"));
                    transferencias[i].setDescripcionTipoTrf(
                        TablaValores.getValor(TABLA_TTFF_TERCEROS, "glosaTipoTransferencia", 
                            transferencias[i].getTipoTrf()));
                    transferencias[i].setCanal((String) datos.get("canal"));
                    transferencias[i].setFechaEnvio((Date) datos.get("fechaCreacion"));
                    String rutUsuario = (String) datos.get("rutUsuario");
                    transferencias[i].setRutUsuario(Long.parseLong(rutUsuario == null ? "0" : rutUsuario.trim()));
                    String dvUsuario = (String) datos.get("digitoVerificadorUsuario");
                    transferencias[i].setDigitoVerifUsu(dvUsuario == null ? ' ' : dvUsuario.charAt(0));
                }
            }
            if(getLogger().isEnabledFor(Level.INFO)){getLogger().info("[obtieneTransferenciasDePago][" + rutEmp + "][" + conv + "][BCI_FINOK] Transferencias " + StringUtil.contenidoDe(transferencias)); }
            return transferencias;
        }
        catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[obtieneTransferenciasDePago][" + rutEmp + "][" + conv + "][BCI_FINEX][Exception e]" + e.getMessage(), e); } 
            throw new GeneralException("ESPECIAL", ErroresUtil.extraeStackTrace(e));
        }
    }

    /**
     * M�todo que consulta las transferencias de acuerdo al filtro ingresado.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 29/10/2014 Sergio Cuevas Diaz (SEnTRA) : versi�n incial.
     * <li>1.1 02/06/2016 Gonzalo Cofre G. (SEnTRA) - Daniel Araya (Ing de Soft BCI) : Se agrega el parametro tipoTransferencia a
     * la entrada del sp y se modifica exception.
     * </ul>
     * @param transferenciasAConsultar Filtro por el cual consultar� las transferencias.
     * @return Transferencias encontradas.
     * @throws GeneralException en caso de error.
     * @since 3.5
     */
    public TransferenciaATercerosTO[] obtieneTransferenciasDePagoRecibidas(FiltroTO transferenciasAConsultar) 
        throws GeneralException{
        
    	 long rutEmp = transferenciasAConsultar.getRutEmpresa();
         String conv = transferenciasAConsultar.getNumeroConvenio();
        try{
            if(getLogger().isEnabledFor(Level.INFO)){
                getLogger().info("[obtieneTransferenciasDePagoRecibidas][" + rutEmp + "][" + conv + "][BCI_INI]");
                getLogger().info("[obtieneTransferenciasDePagoRecibidas][" + rutEmp + "][" + conv + "] transferencias a consultar ["+ transferenciasAConsultar.toString() +"]");
            }
            HashMap parametros = new HashMap();
            parametros.put("CHAINED_SUPPORT", "true");
            parametros.put("rutDestinatario", transferenciasAConsultar.getRutDestinatario() == 0 ? "" 
                : StringUtil.rellenaConCeros(
                    String.valueOf(transferenciasAConsultar.getRutDestinatario()), LARGO_RUT));
            parametros.put("ctaDestinatario", transferenciasAConsultar.getCuentaDestino() == null
                ? "" : transferenciasAConsultar.getCuentaDestino());
            parametros.put("fechaPagoDesde", transferenciasAConsultar.getFechaPagoDesde() == null 
                ? "" : FechasUtil.convierteDateAString(
                    transferenciasAConsultar.getFechaPagoDesde(), "yyyyMMdd"));
            parametros.put("fechaPagoHasta", transferenciasAConsultar.getFechaPagoHasta() == null 
                ? "" : FechasUtil.convierteDateAString(
                    transferenciasAConsultar.getFechaPagoHasta(), "yyyyMMdd"));
            parametros.put("estado", transferenciasAConsultar.getEstado() == null 
                ? "" : transferenciasAConsultar.getEstado());
            parametros.put("origen", transferenciasAConsultar.getOrigen() == null
                ? "" : transferenciasAConsultar.getOrigen());
            parametros.put("productoPago", transferenciasAConsultar.getProductoPago() == null
                    ? "" : transferenciasAConsultar.getProductoPago());
            parametros.put("tipoTransferencia", transferenciasAConsultar.getTipoTrf() == null
                    ? "" : transferenciasAConsultar.getTipoTrf());
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(
                TablaValores.getValor("JNDIConfig.parametros", "cluster", "param"));

            List lista = conector.consultar("banele", "obtieneTransferenciasDePagoRecibidas", parametros);
            TransferenciaATercerosTO[] transferencias = null;
            
            if(getLogger().isEnabledFor(Level.INFO)){
                getLogger().info("[obtieneTransferenciasDePagoRecibidas][" + rutEmp + "][" + conv + "] Lista :: "+ StringUtil.contenidoDe(lista));
            }
            if(lista != null) {
                transferencias = new TransferenciaATercerosTO[lista.size()];
                for (int i = 0; i < lista.size(); i++) {
                    HashMap datos = (HashMap) lista.get(i);
                    transferencias[i] = new TransferenciaATercerosTO();
                    transferencias[i].setIdentificador((String) datos.get("numeroTransferencia"));
                    transferencias[i].setNumConvenio((String) datos.get("numeroConvenio"));
                    String rutEmpresa = (String) datos.get("rutEmpresa");
                    transferencias[i].setRutEmpresa(Long.parseLong(rutEmpresa == null ? "0" : rutEmpresa.trim()));
                    String dvEmpresa = (String) datos.get("digitoVerificadorEmpresa");
                    transferencias[i].setDigitoVerifEmp(dvEmpresa == null ? ' ' : dvEmpresa.charAt(0));
                    transferencias[i].setCtaDestino((String) datos.get("cuentaDestino"));
                    transferencias[i].setNombreCuentaInscrita((String) datos.get("nombreCuenta"));
                    transferencias[i].setCodBanco1((String) datos.get("codigoBancoDestino"));
                    transferencias[i].setDescripcionBanco((String) datos.get("descripcionBancoDestino"));
                    Long monto = (Long) datos.get("monto");
                    transferencias[i].setMontoTransfer(monto == null ? 0 : monto.longValue());
                    transferencias[i].setEstado((String) datos.get("estado"));
                    transferencias[i].setDescripcionEstado((String) datos.get("descripcionEstado"));
                    transferencias[i].setCodigoRechazo((String) datos.get("codigoRechazo"));
                    transferencias[i].setDescripcionCodigoRechazo((String) datos.get("descripcionCodigoRechazo"));
                    transferencias[i].setTipoPago((String) datos.get("tipoPago"));
                    transferencias[i].setDescripcionTipoPago((String) datos.get("descripcionTipoPago"));
                    String rutDestin = (String) datos.get("rutDestinatario");
                    transferencias[i].setRutDestin(Long.parseLong(rutDestin == null ? "0" : rutDestin.trim()));
                    String dvDestin = (String) datos.get("dvDestinatario");
                    transferencias[i].setDigitoVerificador(dvDestin == null ? ' ' : dvDestin.charAt(0));
                    transferencias[i].setNombreDestin((String) datos.get("nombreDestinatario"));
                    transferencias[i].setNumeroBoletaFactura((String) datos.get("numeroFactura"));
                    transferencias[i].setNumeroOrdenCompra((String) datos.get("numeroOrdenCompra"));
                    transferencias[i].setMensajeDestinatario((String) datos.get("mensajeDestinatario"));
                    transferencias[i].setEmailDestinatario((String) datos.get("emailDestinatario"));
                    transferencias[i].setTraceNumber((String) datos.get("traceNumber"));
                    transferencias[i].setFechaPago((Date) datos.get("fechaPago"));
                    transferencias[i].setCtaOrigen((String) datos.get("cuentaCargo"));
                    Integer modalidadCargo = (Integer) datos.get("modalidadCargo");
                    transferencias[i].setModalidadCargo(modalidadCargo == null ? 0 : modalidadCargo.intValue());
                    transferencias[i].setDescripcionModalidadCargo((String) datos.get("glosaModalidadCargo"));
                    transferencias[i].setGlosa((String) datos.get("nombreTransferencia"));
                    transferencias[i].setTipoTrf((String) datos.get("tipoTransferencia"));
                    transferencias[i].setDescripcionTipoTrf(
                        TablaValores.getValor(TABLA_TTFF_TERCEROS, "glosaTipoTransferencia", 
                            transferencias[i].getTipoTrf()));
                }
            }
            if(getLogger().isEnabledFor(Level.INFO)){
                getLogger().info("[obtieneTransferenciasDePagoRecibidas][" + rutEmp + "][" + conv + "] [BCI_FINOK] transferencias : " + StringUtil.contenidoDe(transferencias)); 
            }
            return transferencias;
        }
        catch(Exception e){
            if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[obtieneTransferenciasDePagoRecibidas][" + rutEmp + "][" + conv + "][BCI_FINEX][Exception] "+ e.getMessage(), e);
            }
            if(e instanceof GeneralException){
                throw (GeneralException)e;
            }
            else{
            throw new GeneralException("ESPECIAL", ErroresUtil.extraeStackTrace(e));
        }
    }
    }

    /**
     * M�todo que cambia el estado de una transferencia.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 22/12/2014 Pablo Romero C�ceres (SEnTRA) : versi�n incial.
     * </ul>
     * @param numeroTransferencia N�mero de la transferencia.
     * @param numeroLinea N�mero de l�nea.
     * @param codigoEstadoTransaccion C�digo Estado Transacci�n.
     * @param codigoEstado C�digo Estado.
     * @param glosaDetalle Glosa Detalle.
     * @return <b>true</b> si esta correcto.
     * @throws GeneralException en caso de error.
     * @since 3.5
     */
    public boolean cambiaEstadoTransferenciaPago(String numeroTransferencia, int numeroLinea, 
        String codigoEstadoTransaccion, String codigoEstado, String glosaDetalle) throws GeneralException{
        try {
            if(getLogger().isEnabledFor(Level.INFO)){
                getLogger().info("[cambiaEstadoTransferenciaPago][" + numeroTransferencia + "][BCI_INI]");
                getLogger().info("[cambiaEstadoTransferenciaPago][" + numeroTransferencia + "] numeroLinea ["+ numeroLinea +"], codigoEstadoTransaccion ["+ codigoEstadoTransaccion +"], codigoEstado ["+ codigoEstado +"], glosaDetalle ["+ glosaDetalle +"]");
            }
            String contexto = TablaValores.getValor("JNDIConfig.parametros", "cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
            HashMap parametros = new HashMap();
            parametros.put("num_transferencia", numeroTransferencia);
            parametros.put("detalle_trf", new Integer(numeroLinea));
            parametros.put("estado", codigoEstadoTransaccion);
            parametros.put("codigo_estado", codigoEstado);
            parametros.put("glosa_detalle", glosaDetalle);

            int respuesta = 
                ((Integer) conector.ejecutar("banele", "actualizarEstadoPorTransaccion", parametros)).intValue();

            if(getLogger().isEnabledFor(Level.INFO)){
                getLogger().info("[cambiaEstadoTransferenciaPago][" + numeroTransferencia + "][BCI_FINOK] respuesta: " + respuesta); 
            }
            return respuesta == 0;
        }
        catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[cambiaEstadoTransferenciaPago][" + numeroTransferencia + "][BCI_FINEX][Exception e]" + e.getMessage(), e); }
            throw new GeneralException("ESPECIAL", ErroresUtil.extraeStackTrace(e));
        }
    }

    /**
     * M�todo que caduca una transferencia espec�fica.
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 28/12/2015 Jaime Gaete L. (SEnTRA) - Jorge Lara D�az (ing. Soft. BCI): versi�n incial.
     * </ul>
     * @param transferenciaACaducar Objeto con transferencia a caducar.
     * @return <b>true</b> si esta correcta la caducaci�n.
     * @throws GeneralException en caso de error.
     * @since 3.6
     */  
    public boolean caducarNominaDeTransferencia(FiltroTO transferenciaACaducar)
            throws GeneralException{

        try{
            if (getLogger().isInfoEnabled()) {
                getLogger().debug("[caducarNominaDeTransferencia] Inicio transferenciaACaducar[" 
                        + transferenciaACaducar + "]");
            }
            String contexto = TablaValores.getValor("JNDIConfig.parametros", "cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
            HashMap parametros = new HashMap();	      
            parametros.put("CHAINED_SUPPORT", "true");
            parametros.put("rutEmpresa", transferenciaACaducar.getRutEmpresa() == 0 ? ""
                    :StringUtil.rellenaConCeros(String.valueOf(transferenciaACaducar.getRutEmpresa()), LARGO_RUT));
            parametros.put("numeroConvenio", transferenciaACaducar.getNumeroConvenio() == null ? ""
                    :StringUtil.rellenaConEspacios(transferenciaACaducar.getNumeroConvenio(), LARGO_CONVENIO));
            parametros.put("estado", transferenciaACaducar.getEstado() == null ? ""
                    :transferenciaACaducar.getEstado());
            parametros.put("canal", transferenciaACaducar.getCanal() == null ? ""
                    :transferenciaACaducar.getCanal());
            parametros.put("tipoTransferencia", transferenciaACaducar.getTipoTrf() == null ? ""
                    :transferenciaACaducar.getTipoTrf());
            parametros.put("numeroTransferencia", transferenciaACaducar.getIdentificador() == null ? ""
                    :transferenciaACaducar.getIdentificador());
            parametros.put("origen", transferenciaACaducar.getOrigen() == null ? ""
                    :transferenciaACaducar.getOrigen());

            int respuesta = 
                    ((Integer) conector.ejecutar("banele", "caducarNominaDeTransferencia", parametros)).intValue();

            if (getLogger().isDebugEnabled()) {
                getLogger().debug("[caducarNominaDeTransferencia]respuesta: "
                        + respuesta);
            }
            return respuesta == 0;            
        }
        catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[caducarNominaDeTransferencia]" 
                        + "][Exception] error con mensaje " + e.getMessage(), e);
            }
            throw new GeneralException("ESPECIAL", ErroresUtil.extraeStackTrace(e));
        }
    }
    
    
    
    /**
     * M�todo encargado de consultar transferencias que puedan ser posibles duplicados.
     * <p>
     * <b>Registro de versiones:</b>
     * <ul>
     * <li> 1.0 12/04/2016 Jaime Gaete L. (SEnTRA) - Jorge Lara D�az(Ing. Soft. BCI): Versi�n inicial.</li>
     * </ul>
     * <p>
     * 
     * @param transferencia Transferencia original.
     * @param filtroBusqueda Objeto que detalla filtros espec�ficos para la b�squeda  (tipo transferencia, origen, producto de pago)
     * @param agpEstadosPen C�digo que representa agrupaci�n de estados pendientes.
     * @param agpEstadosFin C�digo que representa agrupaci�n de estados finales.
     * @return Arreglo con posibles transferecias duplicadas.
     * @throws GeneralException en caso de error.
     * @since 4.2
     */
    public TransferenciaATercerosTO[]  consultarPosiblesTransferenciasDuplicadas(TransferenciaATercerosTO transferencia, FiltroTO filtroBusqueda, 
            String agpEstadosPen, String agpEstadosFin)
            throws GeneralException{

        TransferenciaATercerosTO[] transferencias = null;
        String numeroTransferencia = "";
        try{
            numeroTransferencia = transferencia.getIdentificador() == null ? "":transferencia.getIdentificador();
            if (getLogger().isInfoEnabled()) {
                getLogger().info("[consultarPosiblesTransferenciasDuplicadas]["+numeroTransferencia+"][BCI_INI]  Inicio transferencia[" + transferencia 
                        + "], filtroBusqueda["+filtroBusqueda+"], agpEstadosPen["+agpEstadosPen+"], agpEstadosFin["+agpEstadosFin+"]");
            }

            HashMap parametros = new HashMap();
            parametros.put("CHAINED_SUPPORT", "true");
            parametros.put("rutEmpresa", transferencia.getRutEmpresa() == 0 ? "":StringUtil.rellenaConCeros(String.valueOf(transferencia.getRutEmpresa()), LARGO_RUT));
            parametros.put("numeroConvenio", transferencia.getNumConvenio() == null ? "":StringUtil.rellenaConEspacios(transferencia.getNumConvenio(), LARGO_CONVENIO));
            parametros.put("numeroTransferencia", numeroTransferencia);
            parametros.put("cuentaOrigen", transferencia.getCtaOrigen() == null ? "":transferencia.getCtaOrigen());
            parametros.put("rutDestinatario", transferencia.getRutDestin() == 0 ? "":StringUtil.rellenaConCeros(String.valueOf(transferencia.getRutDestin()), LARGO_RUT));
            parametros.put("codigoBanco", transferencia.getCodBanco1() == null ? "":transferencia.getCodBanco1());
            parametros.put("cuentaDestino", transferencia.getCtaDestino() == null ? "":transferencia.getCtaDestino());
            parametros.put("monto", new Long(transferencia.getMontoTransfer()));

            parametros.put("tipoTransferencia", filtroBusqueda.getTipoTrf() == null ? "":filtroBusqueda.getTipoTrf());
            parametros.put("origen", filtroBusqueda.getOrigen() == null ? "":filtroBusqueda.getOrigen());
            parametros.put("productoPago", filtroBusqueda.getProductoPago() == null ? "":filtroBusqueda.getProductoPago());

            parametros.put("estadoPen", agpEstadosPen == null ? "":agpEstadosPen);
            parametros.put("estadoFin", agpEstadosFin == null ? "":agpEstadosFin);

            String contexto = TablaValores.getValor("JNDIConfig.parametros", "cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
            List lista = conector.consultar("banele", "consultarPosiblesTransferenciasDuplicadas", parametros);
            if(lista != null) {
                transferencias = new TransferenciaATercerosTO[lista.size()];
                for (int i = 0; i < lista.size(); i++) {
                    HashMap datos = (HashMap) lista.get(i);
                    transferencias[i] = new TransferenciaATercerosTO();
                    transferencias[i].setRutEmpresa(transferencia.getRutEmpresa());
                    transferencias[i].setNumConvenio(transferencia.getNumConvenio());
                    transferencias[i].setIdentificador((String) datos.get("numeroTransferencia"));
                    transferencias[i].setCtaOrigen(transferencia.getCtaOrigen());
                    transferencias[i].setRutDestin(transferencia.getRutDestin());
                    transferencias[i].setDigitoVerificador(transferencia.getDigitoVerificador());
                    transferencias[i].setCtaDestino(transferencia.getCtaDestino());
                    transferencias[i].setMontoTransfer(transferencia.getMontoTransfer());
                    transferencias[i].setEstado((String) datos.get("estado"));
                    transferencias[i].setDescripcionEstado((String) datos.get("descripcionEstado"));
                    transferencias[i].setGlosa((String) datos.get("glosaTransferencia"));
                    transferencias[i].setFechaPago((Date) datos.get("fecha"));
                    transferencias[i].setDescripcionBanco(transferencia.getDescripcionBanco());
                    transferencias[i].setNombreDestin((String) datos.get("nombreDestinatario"));
                }
            }
            if (getLogger().isInfoEnabled()) {
                getLogger().info("[consultarPosiblesTransferenciasDuplicadas]["+numeroTransferencia+"][BCI_FINOK] respuesta: "+  StringUtil.contenidoDe(transferencias));
            }
            return transferencias;
        }
        catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[consultarPosiblesTransferenciasDuplicadas]["+numeroTransferencia+"][BCI_FINEX][Exception] error con mensaje=<" + e.getMessage()+">", e);
            }
            throw new GeneralException("ESPECIAL", ErroresUtil.extraeStackTrace(e));
        }
    }
    
    /**
     * M�todo encargado de consultar n�minas que puedan ser posibles duplicados.
     * <p>
     * <b>Registro de versiones:</b>
     * <ul>
     * <li> 1.0 17/05/2016 Jaime Gaete L. (SEnTRA) - Paola Avalos(Ing. Soft. BCI): Versi�n inicial.</li>
     * </ul>
     * <p>
     * 
     * @param nomina N�mina original.
     * @param filtroBusqueda Objeto que detalla filtros espec�ficos para la b�squeda  (tipo transferencia, origen, producto de pago).
     * @param agpEstadosPen C�digo que representa agrupaci�n de estados pendientes.
     * @param agpEstadosFin C�digo que representa agrupaci�n de estados finales.
     * @return Arreglo con posibles n�minas duplicadas.
     * @throws GeneralException en caso de error.
     * @since 4.5
     */
    public InformacionCargaMasiva[]  consultarPosiblesNominasDuplicadas(InformacionCargaMasiva nomina, FiltroTO filtroBusqueda, 
            String agpEstadosPen, String agpEstadosFin)
            throws GeneralException{

        InformacionCargaMasiva[] nominasDuplicadas = null;
        String numeroTransferencia = "";
        try{
            numeroTransferencia = nomina.getNumTransferencia() == null ? "":nomina.getNumTransferencia();
            if (getLogger().isInfoEnabled()) {
                getLogger().info("[consultarPosiblesNominasDuplicadas]["+numeroTransferencia+"][BCI_INI]  Inicio nomina[" + nomina 
                        + "], filtroBusqueda["+filtroBusqueda+"], agpEstadosPen["+agpEstadosPen+"], agpEstadosFin["+agpEstadosFin+"]");
}

            HashMap parametros = new HashMap();
            parametros.put("CHAINED_SUPPORT", "true");
            parametros.put("rutEmpresa", nomina.getRutEmpresa() == 0 ? "":StringUtil.rellenaConCeros(String.valueOf(nomina.getRutEmpresa()), LARGO_RUT));
            parametros.put("numeroConvenio", nomina.getCnvEmpresa() == null ? "":StringUtil.rellenaConEspacios(nomina.getCnvEmpresa(), LARGO_CONVENIO));
            parametros.put("numeroTransferencia", numeroTransferencia);
            parametros.put("cuentaOrigen", nomina.getCtaOrigen()== null ? "":nomina.getCtaOrigen());
            
            double montoTotal=0;
            int cantidadPagos=0;
            if(nomina.getTotales()!= null && nomina.getTotales().length > 0){
                for(int i = 0;i < nomina.getTotales().length;i++){
                    if(nomina.getTotales()[i].getTipoPago().equals(CODIGO_TOTALIZACION_NOMINA)){
                        montoTotal = nomina.getTotales()[i].getMontoTotal();
                        cantidadPagos = nomina.getTotales()[i].getTotalRegistros();
                    }
                }
            }
            else{
                throw new GeneralException("PAGOS018");
            }
            
            parametros.put("monto", new Double(montoTotal));
            parametros.put("cantidadPagos", new Integer(cantidadPagos));

            parametros.put("tipoTransferencia", filtroBusqueda.getTipoTrf() == null ? "":filtroBusqueda.getTipoTrf());
            parametros.put("origen", filtroBusqueda.getOrigen() == null ? "":filtroBusqueda.getOrigen());
            parametros.put("productoPago", filtroBusqueda.getProductoPago() == null ? "":filtroBusqueda.getProductoPago());
            parametros.put("fechaDesde", filtroBusqueda.getFechaPagoDesde());

            parametros.put("estadoPen", agpEstadosPen == null ? "":agpEstadosPen);
            parametros.put("estadoFin", agpEstadosFin == null ? "":agpEstadosFin);

            String contexto = TablaValores.getValor("JNDIConfig.parametros", "cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
            List lista = conector.consultar("banele", "consultarPosiblesNominasDuplicadas", parametros);
            if(getLogger().isDebugEnabled()){
                getLogger().debug("[consultarPosiblesNominasDuplicadas]["+numeroTransferencia+"] Antes del if list["+StringUtil.contenidoDe(lista)+"]");
            }
            if(lista != null) {
                nominasDuplicadas = new InformacionCargaMasiva[lista.size()];
                if(getLogger().isDebugEnabled()){
                    getLogger().debug("[consultarPosiblesNominasDuplicadas]["+numeroTransferencia+"] Antes del for list.size["+lista.size()+"]");
                }
                for (int i = 0; i < lista.size(); i++) {
                    HashMap datos = (HashMap) lista.get(i);
                    nominasDuplicadas[i] = new InformacionCargaMasiva();
                    nominasDuplicadas[i].setRutEmpresa(nomina.getRutEmpresa());
                    nominasDuplicadas[i].setDvEmpresa(nomina.getDvEmpresa());
                    nominasDuplicadas[i].setCnvEmpresa(nomina.getCnvEmpresa());
                    nominasDuplicadas[i].setNumTransferencia((String) datos.get("numeroTransferencia"));
                    nominasDuplicadas[i].setCtaOrigen((String) datos.get("cuentaCargo"));
                    nominasDuplicadas[i].setMontoTotalNominas(nomina.getMontoTotalNominas());
                    nominasDuplicadas[i].setCantidadPagos(nomina.getCantidadPagos());
                    nominasDuplicadas[i].setEstado((String) datos.get("estado"));
                    nominasDuplicadas[i].setDescripcionEstado((String) datos.get("descripcionEstado"));
                    nominasDuplicadas[i].setEtiqueta((String) datos.get("glosaTransferencia"));
                    nominasDuplicadas[i].setFechaPago((Date) datos.get("fecha"));
                    
                    TotalTO[] totalesPagos =  new TotalTO[1];
                    totalesPagos[0] = new TotalTO();
                    
                    totalesPagos[0].setMontoTotal((((Double)datos.get("montoTotal")).doubleValue()));
                    totalesPagos[0].setTotalRegistros((((Integer)datos.get("cantidadPagos")).intValue()));
                    
                    nominasDuplicadas[i].setTotales(totalesPagos);
                }
            }
            if (getLogger().isInfoEnabled()) {
                getLogger().info("[consultarPosiblesNominasDuplicadas]["+numeroTransferencia+"][BCI_FINOK] respuesta: "+  StringUtil.contenidoDe(nominasDuplicadas));
            }
            return nominasDuplicadas;
        }
        catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[consultarPosiblesNominasDuplicadas]["+numeroTransferencia+"][BCI_FINEX][Exception] error con mensaje=<" + e.getMessage()+">", e);
            }
            throw new GeneralException("ESPECIAL", ErroresUtil.extraeStackTrace(e));
        }
    }

    /**
     * M�todo que permite consultar operaciones de compra y venta de divisas con transferencia. 
     * <p>
     * <b>Registro de versiones:</b>
     * <ul>
     * <li>1.0 05/04/2017 Mauricio Retamal C. (SEnTRA) - Daniel Araya (Ing. Soft. BCI): Versi�n Inicial.
     * </ul>
     * <p>
     * 
     * @param filtro objeto que contiene los filtros para busqueda.
     * @return arreglo con datos obtenidos de consulta.
     * @throws Exception en caso de error.
     * @since 4.7
     */
    public InformacionCargaMasiva[] consultarOperacionesCVD(FiltroConsultaTO filtro)throws Exception{
        if (getLogger().isEnabledFor(Level.INFO)) {
            getLogger().info("[consultarOperacionesCVD][BCI_INI]:Inicio filtro["+filtro+"]");
        }
        try{
            HashMap parametros = new HashMap();
            parametros.put("CHAINED_SUPPORT", "true");

            parametros.put("rutEmpresa", filtro.getRutEmpresa() == 0 ? "":StringUtil.rellenaConCeros(String.valueOf(filtro.getRutEmpresa()), LARGO_RUT));
            parametros.put("numeroConvenio", filtro.getConvenio() == null ? "":StringUtil.rellenaConEspacios(filtro.getConvenio(), LARGO_CONVENIO));
            parametros.put("tipoTransferencia", filtro.getTipoTransferencia() == null ? "" : filtro.getTipoTransferencia());
            parametros.put("fechaInicio",filtro.getFechaPagoDesde() == null ? "" 
                    : FechasUtil.convierteDateAString(filtro.getFechaPagoDesde(), "MM/dd/yyyy"));
            parametros.put("fechaFin",filtro.getFechaPagoHasta() == null ? "" 
                    : FechasUtil.convierteDateAString(filtro.getFechaPagoHasta(), "MM/dd/yyyy"));
            parametros.put("estado", filtro.getEstadoNomina() == null ? "" : filtro.getEstadoNomina());
            parametros.put("numeroTransferencia", filtro.getNumeroTransferencia() == null ? "" : filtro.getNumeroTransferencia());


            String contexto = TablaValores.getValor("JNDIConfig.parametros", "cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
            List resultado = conector.consultar("banele", "consultarOperacionesCVD", parametros);
            InformacionCargaMasiva[] infoCarga = null;
            HashMap datosGenerales = null;
            if (getLogger().isDebugEnabled()) {
                getLogger().debug("[consultarOperacionesCVD]resultado["+StringUtil.contenidoDe(resultado)+"]");
            }
            if (resultado != null && resultado.size() > 0) {
                infoCarga = new InformacionCargaMasiva[resultado.size()];
                if (getLogger().isDebugEnabled()) {
                    getLogger().debug("[consultarOperacionesCVD] resultado.size() [" + resultado.size() + "]");
                }
                for(int i = 0;i < resultado.size();i++){
                    datosGenerales = (HashMap) resultado.get(i);
                    infoCarga[i]  = new InformacionCargaMasiva();
                    infoCarga[i].setRutUsuario(Long.parseLong(((String) datosGenerales.get("rutUsuario").toString().trim())));
                    infoCarga[i].setDvUsuario(((String) datosGenerales.get("dvUsuario")).charAt(0));
                    infoCarga[i].setCnvEmpresa(datosGenerales.get("convenio").toString().trim());
                    infoCarga[i].setRutEmpresa(Long.parseLong(((String) datosGenerales.get("rutEmpresa")).trim()));
                    infoCarga[i].setDvEmpresa(((String) datosGenerales.get("dvEmpresa")).charAt(0));
                    infoCarga[i].setNumTransferencia(datosGenerales.get("numeroTransferencia").toString().trim());
                    infoCarga[i].setCtaOrigen(datosGenerales.get("cuentaCargo").toString().trim());
                    infoCarga[i].setNombreArchivo(datosGenerales.get("nombreTransferencia").toString().trim());
                    infoCarga[i].setFechaPago((Date) datosGenerales.get("fechaPago"));
                    infoCarga[i].setFechaEnvio((Date) datosGenerales.get("fechaCarga"));
                    infoCarga[i].setEstado(datosGenerales.get("estado").toString().trim());
                    infoCarga[i].setTipoTransferencia(datosGenerales.get("tipo").toString().trim());
                    infoCarga[i].setDescripcionTipoTransferencia(TablaValores.getValor("compraVentaDeDivisas.parametros", "tipoProducto"+infoCarga[i].getTipoTransferencia().trim(), "Glosa"));
                    InfoDetalleNomina[] detalleNomina = new InfoDetalleNomina[1];
                    detalleNomina[0]= new InfoDetalleNomina();
                    detalleNomina[0].setIndicador((((Integer) datosGenerales.get("indicadorRegistro")).intValue()));
                    detalleNomina[0].setCtaDestino(datosGenerales.get("cuentaDestino").toString().trim());
                    detalleNomina[0].setNombreCuenta(datosGenerales.get("nombreCuenta").toString().trim());
                    detalleNomina[0].setBcoDestino(datosGenerales.get("codigoBancoDestino").toString().trim());
                    detalleNomina[0].setMontoDetalle((((Double) datosGenerales.get("monto")).doubleValue()));
                    detalleNomina[0].setEstadoDetalle(datosGenerales.get("estadoDetalle").toString().trim());
                    detalleNomina[0].setEstadoNomina(TablaValores.getValor("compraVentaDeDivisas.parametros", "estadosOperaciones",detalleNomina[0].getEstadoDetalle()));
                    detalleNomina[0].setCodigoRechazo(datosGenerales.get("codigoRechazo").toString().trim());
                    detalleNomina[0].setTipoPago(datosGenerales.get("tipoPago").toString().trim());
                    detalleNomina[0].setRutDestinatario(((String) datosGenerales.get("rutDestinatario").toString().trim()));
                    detalleNomina[0].setDvDestinatario(((String) datosGenerales.get("dvDestinatario").toString().trim()));
                    detalleNomina[0].setNombreDestinatario(datosGenerales.get("nombreDestinatario").toString().trim());
                    detalleNomina[0].setClave1(datosGenerales.get("glosa1").toString().trim());
                    detalleNomina[0].setClave2(datosGenerales.get("glosa2").toString().trim());
                    detalleNomina[0].setClave3(datosGenerales.get("glosa3").toString().trim());
                    detalleNomina[0].setClave4(datosGenerales.get("glosa4").toString().trim());
                    detalleNomina[0].setClave5(datosGenerales.get("glosa5").toString().trim());
                    detalleNomina[0].setClave6(datosGenerales.get("glosa6").toString().trim());
                    detalleNomina[0].setClave7(datosGenerales.get("glosa7").toString().trim());
                    detalleNomina[0].setClave8(datosGenerales.get("glosa8").toString().trim());
                    detalleNomina[0].setClave9(datosGenerales.get("glosa9").toString().trim());
                    detalleNomina[0].setClave10(datosGenerales.get("glosa10").toString().trim());
                    
                    infoCarga[i].setDetalleNomina(detalleNomina); 
                }
            }
            if (getLogger().isEnabledFor(Level.INFO)) {
                getLogger().info("[consultarOperacionesCVD][BCI_FINOK] retornando resultado["+infoCarga+"]");
            }
            return infoCarga;
        }
        catch(Exception e){
            if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[consultarOperacionesCVD][BCI_FINEX][Exception] al consultar las operaciones =<" + e.getMessage() + ">", e);
            }
            throw e instanceof GeneralException ? (GeneralException) e: new GeneralException("ESPECIAL", ErroresUtil.extraeStackTrace(e));
        }
  }
    

    /**
     * <p>M�todo que cambia el estado de un detalle de una Transferencia en la tabla de procesos</p>
     * <p>
     * <b>Registro de versiones:</b>
     * <ul>
     * <li>1.0 05/04/2017 Mauricio Retamal C. (SEnTRA) - Daniel Araya (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <p>
     *
     * @param numTransferencia n�mero de la transferencia
     * @throws Exception en caso de error.
     * @return Respuesta de la modificaci�n.
     * @since 4.7
     */
    public boolean cambiaEstadoProceso(String numTransferencia) throws Exception{
        if(getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[cambiaEstadoProceso][BCI_INI] Inicio metodo. numTransferencia[" + numTransferencia + "]");
        }
        try{
            boolean retorno = false;
            int respuesta = 0;
            String contexto = TablaValores.getValor("JNDIConfig.parametros","cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
            HashMap parametros = new HashMap();
            parametros.put("CHAINED_SUPPORT", "true");         
            parametros.put("numTransferencia", numTransferencia);

            respuesta = Integer.parseInt(String.valueOf( conector.ejecutar(BD_BANELE, "cambiaEstadoProceso", parametros)).trim());

            if(getLogger().isEnabledFor(Level.DEBUG)){
                getLogger().debug("[cambiaEstadoProceso][" + numTransferencia + "] respuesta[" + respuesta + "]");
            }

            if (respuesta == 0) {
                retorno = true; 
            }

            if(getLogger().isEnabledFor(Level.INFO)){
                getLogger().info("[cambiaEstadoProceso][" + numTransferencia + "][BCI_FINOK] Fin metodo. retorno[" + retorno + "]");
            }
            return retorno;
        } 
        catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)){
                getLogger().error("[cambiaEstadoProceso][" + numTransferencia + "][BCI_FINEX]["+e.getClass()+"] Error: "+e.getMessage(), e);
            }
            throw e;
        }

    } 

    
    /**
     * <p>Metodo que elimina todas las transferencias identificados por numero de transferencia.</p>
     * <p>
     * <b>Registro de versiones:</b>
     * <ul>
     * <li>1.0 22/10/2018 Bastian Nelson G. (Sermaluc) - Gonzalo mu�oz (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <p>
     *
     * @param numeroDeTransferencia n�mero de la transferencia
     * @throws Exception en caso de error.
     * @since 4.8
     */
    public void eliminarNumeroDeTransferencia(String numeroDeTransferencia) throws Exception {
        if (getLogger().isEnabledFor(Level.INFO)) {
            getLogger().info("[eliminarNumeroDeTransferencia][BCI_INI] Inicio metodo. numTransferencia[" + numeroDeTransferencia + "]");
        }
        try {

            String contexto = TablaValores.getValor("JNDIConfig.parametros", "cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
            HashMap parametros = new HashMap();
            parametros.put("CHAINED_SUPPORT", "true");
            parametros.put("numTransferencia", numeroDeTransferencia);

            conector.ejecutar(BD_BANELE, "eliminarNumeroDeTransferencia", parametros);

            if (getLogger().isEnabledFor(Level.DEBUG)) {
                getLogger().debug("[cambiaEstadoProceso][" + numeroDeTransferencia + "] ");
            }

            if (getLogger().isEnabledFor(Level.INFO)) {
                getLogger().info("[cambiaEstadoProceso][" + numeroDeTransferencia + "][BCI_FINOK] Fin metodo. ");
            }

        }
        catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[cambiaEstadoProceso][" + numeroDeTransferencia + "][BCI_FINEX][" + e.getClass() + "] Error: " + e.getMessage(), e);
            }
            throw e;
        }

    }
    
    
    

}
