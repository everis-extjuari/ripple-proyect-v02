package wcorp.aplicaciones.productos.servicios.transferencias.nominas.ejb;

import java.rmi.RemoteException;
import java.util.Date;
import java.util.HashMap;

import javax.ejb.EJBObject;

import wcorp.aplicaciones.bancaempresas.diccionariodecuentas.vo.CuentasInscritasVO;
import wcorp.aplicaciones.productos.servicios.pagos.to.FiltroTO;
import wcorp.aplicaciones.productos.servicios.transferencias.nominas.to.InstruccionTransferenciaTO;
import wcorp.aplicaciones.productos.servicios.transferencias.nominas.vo.DetalleTransferenciaVO;
import wcorp.aplicaciones.productos.servicios.transferencias.nominas.vo.EncabezadoTransferenciaVO;
import wcorp.aplicaciones.productos.servicios.transferencias.nominas.vo.EstadoFirmaVO;
import wcorp.aplicaciones.productos.servicios.transferencias.nominas.vo.FiltroConsultarTransferenciasVO;
import wcorp.aplicaciones.productos.servicios.transferencias.nominas.vo.InformacionCodigoActivacionVO;
import wcorp.aplicaciones.productos.servicios.transferencias.nominas.vo.TotalPorCuentaVO;
import wcorp.aplicaciones.productos.servicios.transferencias.nominas.vo.TransferenciasEnLineaVO;
import wcorp.aplicaciones.productos.servicios.transferencias.nominas.vo.TransferenciasVO;
import wcorp.aplicaciones.productos.servicios.transferencias.nominas.vo.UsuarioTransferenciaVO;
import wcorp.aplicaciones.productos.servicios.transferencias.to.CabeceraFirmaNominaPnolTO;
import wcorp.aplicaciones.productos.servicios.transferencias.to.NominaPnolTO;
import wcorp.aplicaciones.productos.servicios.transferencias.to.TransferenciaATercerosTO;
import wcorp.bprocess.bancaempresas.transferenciaslbtr.cargamasiva.vo.FiltroConsultaTO;
import wcorp.bprocess.bancaempresas.transferenciaslbtr.cargamasiva.vo.InfoDetalleNomina;
import wcorp.bprocess.bancaempresas.transferenciaslbtr.cargamasiva.vo.InformacionCargaMasiva;
import wcorp.bprocess.bancaempresas.transferenciaslbtr.cargamasiva.vo.TotalTO;
import wcorp.bprocess.cuentas.vo.TransferenciasRecibidasVO;
import wcorp.model.actores.Cliente;
import wcorp.serv.bciexpress.ListaDeteTransf;
import wcorp.serv.bciexpress.ResultObtieneCtasCtes;
import wcorp.util.GeneralException;
import wcorp.util.excepciones.ExcepcionGeneral;
import wcorp.util.journal.Eventos;

/**
 * Interfaz que contiene todos los m�todos que tienen relaci�n directa con el EJB NominaDeTransferencias.
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 25/04/2009 Alejandro Barra (SEnTRA) - ???? (Ing. Soft. BCI): versi�n inicial.</li>
 * <li>1.1 21/04/2016 Jaime Gaete L. (SEnTRA) - Jorge Lara D�az (Ing. Soft. BCI): Se agrega m�todo 
 * {@link #consultarPosiblesTransferenciasDuplicadas(TransferenciaATercerosTO, FiltroTO, String, String)}.</li>
 * <li>1.2 17/05/2016 Jaime Gaete L. (SEnTRA) - Paola Avalos (Ing. Soft. BCI): Se agrega m�todo 
 * {@link #consultarPosiblesNominasDuplicadas(InformacionCargaMasiva, FiltroTO, String, String)}.</li>
 * <li>1.3 05/06/2016 Jaime Gaete L. (SEnTRA) - Daniel Araya (Ing. Soft. BCI): Se agrega m�todo 
 * {@link #realizarTransferenciaDeFondosDetalleNomina(InfoDetalleNomina, Cliente)} para realizar transferencia de fondos para un detalle de n�mina.
 * <li>1.4 05/04/2017 Mauricio Retamal C. (SEnTRA) - Daniel Araya (Ing. Soft. BCI): Se agregan los m�todos {@link #consultarOperacionesCVD(FiltroConsultaTO)} 
 *                                                      y {@link #cambiaEstadoProceso(String)}.
 * </li>
 * </ul>
 * <B>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</B>
 * <p>
 * @see NominaDeTransferenciasBean
 */
public interface NominaDeTransferencias extends EJBObject {

	/**
	 *  @see NominaDeTransferenciasBean#agregarTransferencia(String, String, String, String, String, TransferenciasEnLineaVO)
	 */
	public TransferenciasEnLineaVO[] agregarTransferencia(String rutEmpresa, String dvEmpresa, String convenio, String rutUsu, String dvUsu, TransferenciasEnLineaVO transferencia) throws Exception;

	/**
	 *  @see NominaDeTransferenciasBean#obtieneGruposFrecuentes(String, String, String)
	 */
	public TransferenciasEnLineaVO[] obtieneGruposFrecuentes(String convenio, String rutEmpresa, String nombreGrupo) throws Exception;

	/**
	 * @see NominaDeTransferenciasBean#eliminarTransferencia(String, String, String, int)
	 */
	public boolean eliminarTransferencia(String rutEmpresa, String convenio, String numTransferencia,int  idDetalle) throws Exception;

	/**
	 * @see NominaDeTransferenciasBean#crearGrupo(String, String, String, String, String, String)
	 */
	public boolean crearGrupo(String convenio, String rutEmpresa, String nombreGrupo, String numTransferencia, String rutUsuario, String ipUsuario) throws Exception;

	/**
	 * @see NominaDeTransferenciasBean#cambiaEstadoTransferencia(String, String, String, String, String, String, int, String, String, String)
	 */
	public boolean cambiaEstadoTransferencia(String rutUsuario, String dvUsuario, String convenio, String rutEmpresa, String dvEmpresa, String numTransferencia, int detalleTransferencia, String estado, String codigoRechazo, String encabezadoODetalle) throws Exception;

	/**
	 * @see NominaDeTransferenciasBean#eliminarGrupoDeLaTransferencia(String, String, String)
	 */
	public boolean eliminarGrupoDeLaTransferencia(String convenio, String rutEmpresa, String nombreGrupo) throws Exception;

	/**
	 * @see NominaDeTransferenciasBean#modificaGrupoDeLaTransferencia(String, String, String, String, String, String)
	 */
	public boolean modificaGrupoDeLaTransferencia(String convenio, String rutEmpresa, String nombreGrupo, String numTransferencia, String rutUsu, String ipUsuario) throws Exception;

	/**
	 *@see NominaDeTransferenciasBean#modificaDetalleTransferencia(String, String, DetalleTransferenciaVO)
	 */
	public boolean modificaDetalleTransferencia(String convenio, String rutEmpresa, DetalleTransferenciaVO detalleVO) throws Exception;

	/**
	 *@see NominaDeTransferenciasBean#extraeTransferenciasRealizadas(FiltroConsultarTransferenciasVO, int, int)
	 */
	public DetalleTransferenciaVO[] extraeTransferenciasRealizadas(FiltroConsultarTransferenciasVO filtro, int bloque, int cantidadRegistros) throws Exception;

	/**
	 *@see NominaDeTransferenciasBean#extraePagosRealizadosViaCargaMasiva(FiltroConsultarTransferenciasVO, int, int)
	 */
	public EncabezadoTransferenciaVO[] extraePagosRealizadosViaCargaMasiva(FiltroConsultarTransferenciasVO filtro, int bloque, int cantidadRegistros) throws Exception;

	/**
	 *@see NominaDeTransferenciasBean#extraeTotalesPorCuentaDeTransferenciasRealizadas(FiltroConsultarTransferenciasVO)
	 */
	public TotalPorCuentaVO[] extraeTotalesPorCuentaDeTransferenciasRealizadas(FiltroConsultarTransferenciasVO filtro) throws Exception;

	/**
	 *@see NominaDeTransferenciasBean#extraeDetallesTransferenciaPendiente(String, String, String, int, int)
	 */
	public TransferenciasVO[] extraeDetallesTransferenciaPendiente(String rutEmpresa, String convenio, String  numeroTransferencia, int bloque, int cantidadRegistros) throws Exception;

	/**
	 *@see NominaDeTransferenciasBean#extraeTransferenciasPendientes(FiltroConsultarTransferenciasVO, int, int)
	 */
	public EncabezadoTransferenciaVO[] extraeTransferenciasPendientes(FiltroConsultarTransferenciasVO filtro, int bloque, int cantidadRegistros) throws Exception;

	/**
	 *@see NominaDeTransferenciasBean#extraeTransferenciasPendientesPorArchivo(FiltroConsultarTransferenciasVO, int, int)
	 */
	public EncabezadoTransferenciaVO[] extraeTransferenciasPendientesPorArchivo(FiltroConsultarTransferenciasVO filtro, int bloque, int cantidadRegistros) throws Exception;

	/**
	 *@see NominaDeTransferenciasBean#extraeTotalesPorCuentaDeTransferenciasPendientes(FiltroConsultarTransferenciasVO)
	 */
	public TotalPorCuentaVO[] extraeTotalesPorCuentaDeTransferenciasPendientes(FiltroConsultarTransferenciasVO filtro) throws Exception;
	/**
	 *@see NominaDeTransferenciasBean#extraeDetallesTransferenciasPendientesConReparos(FiltroConsultarTransferenciasVO, int, int)
	 */
	public TransferenciasVO[] extraeDetallesTransferenciasPendientesConReparos(FiltroConsultarTransferenciasVO filtro, int bloque, int cantidadRegistros) throws Exception;

	/**
	 *@see NominaDeTransferenciasBean#extraeTransferenciasRecibidas(FiltroConsultarTransferenciasVO, int, int)
	 */
	public TransferenciasVO[] extraeTransferenciasRecibidas(FiltroConsultarTransferenciasVO filtro, int bloque, int cantidadRegistros) throws Exception;

	/**
	 *@see NominaDeTransferenciasBean#extraeTransfRecibidasEmpBCIEmpBCI(long, char, Date, String, int, int)
	 */
	public TransferenciasRecibidasVO[] extraeTransfRecibidasEmpBCIEmpBCI(long rutEmpresa, char dvEmpresa, Date fechaProceso, String cuentaDestino, int bloque, int cantidadRegistros) throws Exception;

	/**
	 *@see NominaDeTransferenciasBean#extraeDetalleTransferencia(String, String, String, int)
	 */
	public DetalleTransferenciaVO extraeDetalleTransferencia(String rutEmpresa, String convenio, String numeroTransferencia, int numeroDetalle) throws Exception;

	/**
	 * @see NominaDeTransferenciasBean#consultarEventosParaTransferencias(FiltroConsultarTransferenciasVO)
	 */
	public UsuarioTransferenciaVO[] consultarEventosParaTransferencias(FiltroConsultarTransferenciasVO filtro) throws Exception;

	/**
	 * @see NominaDeTransferenciasBean#registraEvento(String, String, String, String, String, String, Date, String, String)
	 */
	public void registraEvento(String rutUsuario, String dvUsuario, String convenio, String rutEmpresa, String dvEmpresa,
			String numOperacion, Date fechaAccion, String tipoAccion) throws Exception;
	
	/**
	 * @see NominaDeTransferenciasBean#cambiaGlosaTransferencia(String, String, String, int, int, String)
	 */
	public boolean cambiaGlosaTransferencia( String convenio, String rutEmpresa, String numTransferencia, int detalleTransferencia, int identificadorGlosa, String detalleGlosa) throws Exception;


	/**
	 *@see NominaDeTransferenciasBean#extraeTransferenciasPendientesPorNumero(FiltroConsultarTransferenciasVO, int, int)
	 */
	public EncabezadoTransferenciaVO[] extraeTransferenciasPendientesPorNumero(FiltroConsultarTransferenciasVO filtro, int bloque, int cantidadRegistros) throws Exception;

	
	/**
	 * @see NominaDeTransferenciasBean#extraeCodigodDeActivacion(long, int, int)
	 */
	public InformacionCodigoActivacionVO[] extraeCodigodDeActivacion(long rut,int bloque, int cantidadRegistros) throws Exception;

	/**
	 * @see NominaDeTransferenciasBean#extraeCuentasNoInscritas(String, String, String)
	 */
	public CuentasInscritasVO[] extraeCuentasNoInscritas(String idTransferencia, String convenio, long rutEmpresa) throws Exception;
	
	/**
	 * @see NominaDeTransferenciasBean#validarCodigoActivacion(String, String, String, String)
	 */
	public boolean validarCodigoActivacion(String idTransferencia, String convenio, long rutEmpresa, String codigo) throws Exception;
	
	/**
	 * @see NominaDeTransferenciasBean#enviarCodigoActivacion(long, char, long, char, String, String, Eventos)
	 */
	public boolean enviarCodigoActivacion(long rutUsuario, char dvUsuario, long rutEmpresa, char dvEmpresa, String convenio, String numTrf, Eventos evento) throws Exception;
	
	/**
     *@see NominaDeTransferenciasBean#extraeTransferenciasPendientesPorResolver(FiltroConsultarTransferenciasVO)
     */
    public TransferenciasVO[] extraeTransferenciasPendientesPorResolver(FiltroConsultarTransferenciasVO filtro) throws Exception;

    /**
     * @see NominaDeTransferenciasBean#extraeTransferenciasPendientesATerceros(long, String, int, int)
     */
    public EncabezadoTransferenciaVO[]
        extraeTransferenciasPendientesAterceros(long rut, String convenio,
                                                int bloque, int cantidadRegistros)
                                                throws ExcepcionGeneral, RemoteException;

    /**
     * @see NominaDeTransferenciasBean#extraeTransferenciasPendientesEPC(long, String, int, int)
     */
    public EncabezadoTransferenciaVO[]
        extraeTransferenciasPendientesEPC(long rut, String convenio,
                                          int bloque, int cantidadRegistros)
                                          throws ExcepcionGeneral, RemoteException;
   /**
     * @see NominaDeTransferenciasBean#obtenerDetalleTransferencia(String, long, String)
     */
    public DetalleTransferenciaVO[]
        obtenerDetalleTransferencia(String numeroTransferencia,
                                    long rutEmpresa, String convenio)
                                    throws ExcepcionGeneral, RemoteException;
    
    /**
     *  @see NominaDeTransferenciasBean#eliminaDetalleCuentasNoInscritas(String, long, char ,String)
     */
    public boolean eliminaDetalleCuentasNoInscritas(String numConvenio, long rutEmpresa, char dvEmpresa, 
            String numTransferencia) throws Exception;
 /**
     * @see NominaDeTransferenciasBean#obtenerInstruccionesTransferenciaSupervisor(long,
     *      int, String)
     */
    public InstruccionTransferenciaTO[] obtenerInstruccionesTransferenciaSupervisor()
            throws Exception;
    /**
     * 
     * @see NominaDeTransferenciasBean#enviarInstruccionSupervisor(InstruccionTransferenciaTO)
     */
    public void enviarInstruccionSupervisor(
            InstruccionTransferenciaTO instruccion) throws Exception;
            
    /**
	 * @see NominaDeTransferenciasBean#eliminarInstruccionTransferenciaSupervisor(long, int, String)
	 */	
	public void eliminarInstruccionTransferenciaSupervisor(long rutEmpresa, int convenio, String numeroTransferencia) throws Exception;
	
/**
     * @see NominaDeTransferenciasBean#insertarAccionBitacoraInstruccionesTransferencia(InstruccionTransferenciaTO)
     */
    public void insertarAccionBitacoraInstruccionesTransferencia(
            InstruccionTransferenciaTO datosAccion) throws Exception;
    
    /**
     * @see NominaDeTransferenciasBean#extraeTransferenciasPendientesCTALBT(FiltroConsultarTransferenciasVO)
     */
    public EncabezadoTransferenciaVO[] extraeTransferenciasPendientesCTALBT(FiltroConsultarTransferenciasVO filtro) throws Exception;

    /**
     * @see NominaDeTransferenciasBean#consultarExisteTransferencia(String, String, String,
     *                                                              String, ListaDeteTransf)
     */
    public EncabezadoTransferenciaVO[] consultarExisteTransferencia(String rutEmpresa, String convenio, String numTTFF,
                           String cuentaOrigen, ListaDeteTransf detalle) throws ExcepcionGeneral, RemoteException;
    
    /**
     *  @see NominaDeTransferenciasBean#obtenerTiempoEsperaNominaTTFF(String)
     */
    public EncabezadoTransferenciaVO obtenerTiempoEsperaNominaTTFF(String numTTFF) 
        throws ExcepcionGeneral, RemoteException;
    
    /**
     * @see NominaDeTransferenciasBean#firmarNominasPNOL(NominaPnolTO[], CabeceraFirmaNominaPnolTO)
     */
    public EstadoFirmaVO[] firmarNominasPNOL(NominaPnolTO[] nominasAFirmar, CabeceraFirmaNominaPnolTO cabecera) throws Exception;
    
    /**
     * @see NominaDeTransferenciasBean#validaFyPoMono(String, String, String, String, String)
     */    
    public String validaFyPoMono(String rutEmpresa, String digitoVerifEmp, String convenioEmpresa, String rutUsu,
            String digitoVerifUsu) throws Exception;
    /**
     * @see NominaDeTransferenciasBean#firmarTransferenciasPNOL(String, String, String, String, String, String[], int, CabeceraFirmaNominaPnolTO)
     */        
    public EstadoFirmaVO[] firmarTransferenciasPNOL(String rutEmpresa, String digitoVerifEmp, String convenioEmpresa,
            String rutUsu, String digitoVerifUsu, String[] transfAFirmar,
            int topeIndustria, CabeceraFirmaNominaPnolTO cabeceraFirma) throws Exception;
    
    /**
     * @see NominaDeTransferenciasBean#creaFiltro(String, String, String, String)
     */    
    public FiltroConsultarTransferenciasVO creaFiltro(String rutEmpresa, String convenioEmpresa, String transfAFirmar, String estado) throws RemoteException;
    
    /**
     * @see NominaDeTransferenciasBean#autorizaPagoFyP(String, String, char, String, char, String)
     */    
    public FiltroConsultarTransferenciasVO autorizaPagoFyP(String numTransferencia, String rutUsu, char digitoVerifUsu,
            String rutEmpresa, char digitoVerifEmp, String convenioEmpresa) throws RemoteException;
    
    /**
     * @see NominaDeTransferenciasBean#validaMontosYTopesEnLinea(String, double, TransferenciasVO)
     */    
    public FiltroConsultarTransferenciasVO validaMontosYTopesEnLinea(String cuentaTrans, double topeIndustria, TransferenciasVO detalle) throws RemoteException;
    
    /**
     * @see NominaDeTransferenciasBean#montoMaximoYAutorizado(String, String, char, String, char, String, int, String, double)
     */    
    public FiltroConsultarTransferenciasVO montoMaximoYAutorizado(String cuentaTrans, String rutUsu, char digitoVerifUsu,
        	String rutEmpresa, char digitoVerifEmp, String convenioEmpresa, int topeIndustria, 
        		String servicioConsMonto, double montoTotalOTB) throws RemoteException;
    /**
     * @see NominaDeTransferenciasBean#rescatarProducto(String, String, String)
     */    
    public String rescatarProducto(String rutUsuario, String convenioEmpresa, String rutEmpresa) throws RemoteException;
    
    /**
     * @see NominaDeTransferenciasBean#consultaTipoApoderado(String, String, String, String, String)
     */    
    public boolean consultaTipoApoderado(String rutEmpresa, String digitoVerifEmp, String convenioEmpresa, String rutUsu,
            String digitoVerifUsu) throws Exception;
    
    /**
     * @see NominaDeTransferenciasBean#obtieneCuentasySaldo(ResultObtieneCtasCtes)
     */    
    public HashMap obtieneCuentasySaldo(ResultObtieneCtasCtes res_ctas) throws Exception;
    /**
     *  @see NominaDeTransferenciasBean#actualizarEstadoNominaEnProceso(InformacionCargaMasiva)
     */
    public boolean actualizarEstadoNominaEnProceso(InformacionCargaMasiva informacionCargaMasiva) throws Exception;
    /**
     *  @see NominaDeTransferenciasBean#obtenerEncabezadoDeNomina(InformacionCargaMasiva)
     */
    public InformacionCargaMasiva obtenerEncabezadoDeNomina(InformacionCargaMasiva info) throws Exception;
    
    /**
     *  @see NominaDeTransferenciasBean#obtenerPagosNomina(InformacionCargaMasiva)
     */
    public InfoDetalleNomina[] obtenerPagosNomina(InformacionCargaMasiva nomina) 
    throws RemoteException, GeneralException;
    
    /**
     *  @see NominaDeTransferenciasBean#obtenerTotalesNomina(InformacionCargaMasiva)
     */
    public TotalTO[] obtenerTotalesNomina(InformacionCargaMasiva nomina) throws RemoteException, GeneralException;
    
    /**
     * @see NominaDeTransferenciasBean#obtieneNominasEnLineaPendientesDeFirma(FiltroConsultaTO)
     */    
    public InformacionCargaMasiva[] obtieneNominasEnLineaPendientesDeFirma(
            FiltroConsultaTO filtroConsulta) throws Exception;

    /**
     *  @see NominaDeTransferenciasBean#obtenerEncabezadoDeNominaEnProceso(InformacionCargaMasiva)
     */
    public InformacionCargaMasiva obtenerEncabezadoDeNominaEnProceso(InformacionCargaMasiva nomina) 
    throws RemoteException, GeneralException;
    /**
     *  @see NominaDeTransferenciasBean#consultaMontoPagadoBeneficiario (long, String, String, Date)
     */
    public double consultaMontoPagadoBeneficiario(long rutBeneficiario, String cuentaDestino, String cuentaOrigen,
        Date fechaPago) throws RemoteException, GeneralException;

    /**
     *  @see NominaDeTransferenciasBean#actualizarEstadoPago(InfoDetalleNomina)
     */
    public boolean actualizarEstadoPago(InfoDetalleNomina pago) throws RemoteException, GeneralException;

    /**
     *  @see NominaDeTransferenciasBean#actualizarTotalesNomina(String)
     */
    public boolean actualizarTotalesNomina(String numeroTransferencia) throws GeneralException, RemoteException;
    
    /**
     *  @see NominaDeTransferenciasBean#obtenerResumenNominas(FiltroConsultaTO)
     */
    public TotalTO[] obtenerResumenNominas(FiltroConsultaTO filtro) throws Exception;
    
    /**
     *  @see NominaDeTransferenciasBean#consultarNominasConProblemasPorFiltro(FiltroConsultaTO)
     */
    public InformacionCargaMasiva[] consultarNominasConProblemasPorFiltro(FiltroConsultaTO filtro)throws Exception;
    
    /**
     *  @see NominaDeTransferenciasBean#consultarNominasEnProcesoPorFiltro(FiltroConsultaTO)
     */
    public InformacionCargaMasiva[] consultarNominasEnProcesoPorFiltro(FiltroConsultaTO filtro)throws Exception;
    
    /**
     * @see NominaDeTransferenciasBean#obtenerEncabezadoPagosRealizados(FiltroConsultarTransferenciasVO, String, String)
     */
    public EncabezadoTransferenciaVO[] obtenerEncabezadoPagosRealizados(FiltroConsultarTransferenciasVO filtro,
            String tipoTransferencia, String canal) throws Exception;
    
    /**
     * @see NominaDeTransferenciasBean#obtenerDetallePagosRealizados(FiltroConsultarTransferenciasVO, String,
     *      String, String)
     */
    public DetalleTransferenciaVO[] obtenerDetallePagosRealizados(FiltroConsultarTransferenciasVO filtro,
            String tipoTransferencia, String canal, String origen) throws Exception;
    
    /**
     * @see NominaDeTransferenciasBean#insertarDetalleTransferencia(TransferenciaATercerosTO)
     */
    public boolean insertarDetalleTransferencia(TransferenciaATercerosTO transferenciaIngreso) 
        throws Exception;

    /**
     * @see NominaDeTransferenciasBean#insertarEncabezadoTransferencia(TransferenciaATercerosTO)
     */
    public String insertarEncabezadoTransferencia(TransferenciaATercerosTO transferencia) throws GeneralException,
    RemoteException;

    /**
     * @see NominaDeTransferenciasBean#eliminarOperacionTransferencia(String)
     */
    public boolean eliminarOperacionTransferencia(String numeroTransferencia) throws GeneralException,
    RemoteException;

    /**
     * @see NominaDeTransferenciasBean#insertarTotalesTransferencia(String)
     */
    public boolean insertarTotalesTransferencia(String numeroTransferencia) throws GeneralException,
    RemoteException;

    /**
     * @see NominaDeTransferenciasBean#obtieneTransferenciasDePago(FiltroTO)
     */
    public TransferenciaATercerosTO[] obtieneTransferenciasDePago(FiltroTO transferenciasAConsultar) 
        throws RemoteException, GeneralException;

    /**
     * @see NominaDeTransferenciasBean#obtieneTransferenciasDePagoRecibidas(FiltroTO)
     */
    public TransferenciaATercerosTO[] obtieneTransferenciasDePagoRecibidas(FiltroTO transferenciasAConsultar) 
        throws RemoteException, GeneralException;


    /**
     * @see NominaDeTransferenciasBean#realizaTransferenciaDeFondos(TransferenciaATercerosTO, Cliente)
     */
    public boolean realizaTransferenciaDeFondos(TransferenciaATercerosTO transferencia, Cliente cliente) 
        throws RemoteException, GeneralException;

    /**
     * @see NominaDeTransferenciasBean#cambiaEstadoTransferenciaPago(String, int, String, String, String)
     */
    public boolean cambiaEstadoTransferenciaPago(String numeroTransferencia, int numeroLinea, 
        String codigoEstadoTransaccion, String codigoEstado, String glosaDetalle) 
            throws GeneralException, RemoteException;
    
    /**
     * @see NominaDeTransferenciasBean#revisarCaducidadDeTransferencia(TransferenciaATercerosTO, String)
     */        
    public boolean revisarCaducidadDeTransferencia(TransferenciaATercerosTO transferencia, String origen
    		, String accion) throws GeneralException, RemoteException;
    
    /**
     * @see NominaDeTransferenciasBean#caducarNominaDeTransferencia(FiltroTO)
     */    
    public boolean caducarNominaDeTransferencia(FiltroTO transferenciaACaducar)
		throws GeneralException, RemoteException;
    
    /**
     * @see NominaDeTransferenciasBean#consultarPosiblesTransferenciasDuplicadas(TransferenciaATercerosTO, FiltroTO, String, String)
     */    
    public TransferenciaATercerosTO[]  consultarPosiblesTransferenciasDuplicadas(TransferenciaATercerosTO transferencia, FiltroTO filtroBusqueda, 
            String agpEstadosPen, String agpEstadosFin) throws GeneralException, RemoteException;    
    
    /**
     * @see NominaDeTransferenciasBean#consultarPosiblesNominasDuplicadas(InformacionCargaMasiva, FiltroTO, String, String)
     */  
    public InformacionCargaMasiva[]  consultarPosiblesNominasDuplicadas(InformacionCargaMasiva nomina, FiltroTO filtroBusqueda, 
            String agpEstadosPen, String agpEstadosFin) throws GeneralException, RemoteException;
    
    /**
     * @see NominaDeTransferenciasBean#realizarTransferenciaDeFondosDetalleNomina(InfoDetalleNomina, Cliente)
     */  
    public boolean realizarTransferenciaDeFondosDetalleNomina(InfoDetalleNomina detalleNomina, Cliente cliente) throws GeneralException, RemoteException;

/**
     * @see NominaDeTransferenciasBean#consultarOperacionesCVD(FiltroConsultaTO)
     */  
    public InformacionCargaMasiva[] consultarOperacionesCVD(FiltroConsultaTO filtroBusqueda) throws Exception, RemoteException;
    
    /**
     * @see NominaDeTransferenciasBean#cambiaEstadoProceso(String)
     */
    public boolean cambiaEstadoProceso(String numTransferencia) throws Exception, RemoteException;

    /**
     * @see NominaDeTransferenciasBean#eliminarNumeroDeTransferencia(String numeroDeTransferencia)
     */
    public void eliminarNumeroDeTransferencia(String numeroDeTransferencia) throws Exception, RemoteException;

}
