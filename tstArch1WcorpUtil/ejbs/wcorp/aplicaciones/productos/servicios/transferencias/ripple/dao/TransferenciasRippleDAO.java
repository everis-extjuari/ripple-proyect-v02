package wcorp.aplicaciones.productos.servicios.transferencias.ripple.dao;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import cl.bci.middleware.aplicacion.persistencia.ConectorServicioDePersistenciaDeDatos;
import cl.bci.middleware.aplicacion.persistencia.ejb.ServicioDatosException;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.sessionfacade.TtffException;
import wcorp.aplicaciones.productos.servicios.transferencias.monedaextranjera.to.RespuestaHorarioTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.BeneficiarioRippleTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.ComisionTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.ConsultaBeneficiariosRippleTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.ConsultaDetalleBeneficiarioRippleTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.DatosxCurrentTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.DetalleBeneficiarioRippleTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.FirmarEliminarTransferenciaRipple;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.MonedasPermitidasTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.NodosRippleTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.ObtenerComisionTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.RegistrarTransferenciaTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.RespuestaRegistroTransferenciaTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.to.RespuestaSpTO;
import wcorp.aplicaciones.productos.servicios.transferencias.ripple.util.HashUtil;
import wcorp.util.StringUtil;
import wcorp.util.TablaValores;

/**
 * Clase DAO de la aplicacion Transferencias Internacionales (RIPPLE) para el
 * acceso a la capa de datos mediante el uso de Persistencia de Datos.
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 27/02/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft. BCI): versi�n inicial.
 * 			- {@link #obtenerNodosRippleDisp()}
 *  		- {@link #obtenerMonedasPermitidas(String)} 
 *          - {@link #obtenerBeneficiariosAutorizadosRipple(ConsultaBeneficiariosRippleTO)} 
 *          - {@link #obtenerComisiones(ConsultaDetalleBeneficiarioRippleTO)}
 *          - {@link #obtenerDetalleBeneficiarioRipple(ConsultaDetalleBeneficiarioRippleTO)}
 *          - {@link #obtenerComisiones(ConsultaBeneficiariosRippleTO)} 
 *          - {@link #registrarTransferencia(ConsultaDetalleBeneficiarioRippleTO)}
 *          - {@link #procesaError(ConsultaDetalleBeneficiarioRippleTO)}
 *          - {@link #consultarLimiteHorarioIngreso}
 *          - {@link #firmarOEliminarTransferenciaRipple}
 *          - {@link #debitarCuentaCorrienteMX}
 *          - {@link #consultarDatosxCurrent}
 *          - {@link #registrarResulatadoxCurrent}
 * </li>
 * </ul>
 * <P>
 * 
 * <B>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</B>
 * <P>
 */

public class TransferenciasRippleDAO {

	/**
	 * Archivo de configuracion acceso a bd.
	 */
	private static final String SQL_MAP_BD_TTFF_RPP= "bciComexRipple";

	/**
	 * Registro de eventos.
	 */
	private transient Logger logger = (Logger) Logger.getLogger(this.getClass());

	/**
	 * Invoca al procedimiento almacenado FBPsrv_shw_cur_rpp.
	 * <p>
	 *
	 * Registro de versiones:
	 * <ul>
	 *
	 * <li>1.0 19/02/2019 Danilo Dominguez (Everis) - Minheli Mejias (Ing. Soft.
	 * BCI) : Versi�n inicial.</li>
	 * </ul>
	 * <p>
	 *
	 * @return NodosRippleTO[]�Salida del SP FBPsrv_shw_cur_rpp.
	 * @throws ServicioDatosException
	 *             En caso de ocurrir un error en el servicio de datos.
	 * @throws TtffException
	 *             En caso de ocurrir un error de negocio.
	 * 
	 * @since 1.0
	 */
	public NodosRippleTO[] obtenerNodosRippleDisp() throws ServicioDatosException, TtffException {

		String nombreMetodo = "obtenerNodosRippleDisp";

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "] [BCI_INI]");
		}

		List documento = null;
		HashMap salida = null;
		NodosRippleTO[] nodos = null;

		HashMap mapa = new HashMap();
		mapa.put("CHAINED_SUPPORT", "true");

		String contexto = TablaValores.getValor("JNDIConfig.parametros", "cluster", "param");

		if (getLogger().isDebugEnabled()) {
			getLogger().debug("obtenerNodosRippleDisp");
			getLogger().debug("[ConectorServicioDePersistenciaDeDatos] - getConnection");
		}

		ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
		documento = conector.consultar(SQL_MAP_BD_TTFF_RPP, "obtenerNodosRippleDisp", mapa);
		if (documento != null) {
			if (getLogger().isDebugEnabled()) {
				getLogger().info("obtenerNodosRippleDisp::Cantidad de registros retornados: " + documento.size());
			}
			if (!documento.isEmpty()) {
				nodos = new NodosRippleTO[documento.size()];
				for (int c = 0; c < documento.size(); c++) {
					NodosRippleTO paso = new NodosRippleTO();
					salida = (HashMap) documento.get(c);
					Integer resultSw = (Integer) salida.get("indicadorResultado");
					String resultMsg = String.valueOf(salida.get("mensajeResultado"));
					procesaError(resultSw, resultMsg, "obtenerNodosRippleDisp");
					paso.setBic(String.valueOf(salida.get("bicBanco")));
					paso.setNombreBanco(String.valueOf(salida.get("nombreBanco")));
					nodos[c] = paso;
				}
			}
		}

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "] [BCI_FINOK][" + StringUtil.contenidoDe(nodos) + "]");
		}
		return nodos;
	}

	/**
	 * Retorna las monedas permitidas para esta operacion
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 19/02/2019 Danilo Dominguez (Everis) - Minheli Mejias (Ing. Soft.
	 * BCI) : Versi�n inicial.</li>
	 * </ul>
	 * <p>
	 * 
	 * @param bic
	 *            codigo del banco
	 * @return MonedasPermitidasTO[]�monedas permitidas.
	 * @throws ServicioDatosException
	 *             En caso de ocurrir un error en el servicio de datos.
	 * @throws TtffException
	 *             En caso de ocurrir un error de negocio.
	 * 
	 * @since 1.0
	 */
	public MonedasPermitidasTO[] obtenerMonedasPermitidas(String bic) throws ServicioDatosException, TtffException {

		String nombreMetodo = "obtenerMonedasPermitidas";

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "] [BCI_INI]");
			getLogger().info("[" + nombreMetodo + "] [Parametro][bic][" + bic + "]");
		}

		List documento = null;
		HashMap salida = null;
		MonedasPermitidasTO[] objeto = null;

		HashMap mapa = new HashMap();
		mapa.put("CHAINED_SUPPORT", "true");

		mapa.put("fld_rpp_bic_bco", bic);

		String contexto = TablaValores.getValor("JNDIConfig.parametros", "cluster", "param");

		if (getLogger().isDebugEnabled()) {
			getLogger().debug("obtenerMonedasPermitidas");
			getLogger().debug("[ConectorServicioDePersistenciaDeDatos] - getConnection");
		}

		ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
		documento = conector.consultar(SQL_MAP_BD_TTFF_RPP, "obtenerMonedasPermitidas", mapa);
		if (documento != null) {
			if (getLogger().isDebugEnabled()) {
				getLogger().info("obtenerMonedasPermitidas::Cantidad de registros retornados: " + documento.size());
			}
			if (!documento.isEmpty()) {
				objeto = new MonedasPermitidasTO[documento.size()];
				for (int c = 0; c < documento.size(); c++) {
					MonedasPermitidasTO paso = new MonedasPermitidasTO();
					salida = (HashMap) documento.get(c);
					Integer resultSw = (Integer) salida.get("indicadorResultado");
					String resultMsg = String.valueOf(salida.get("mensajeResultado"));
					procesaError(resultSw, resultMsg, "obtenerMonedasPermitidas");
					paso.setCodigoIso(String.valueOf(salida.get("codigoIso")));
					paso.setNombreMoneda(String.valueOf(salida.get("nombreMoneda")));
					paso.setMtoMaximo((BigDecimal) (salida.get("mtoMaximo")));
					objeto[c] = paso;
				}
			}
		}

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "] [BCI_FINOK][" + StringUtil.contenidoDe(objeto) + "]");
		}
		return objeto;
	}

	/**
	 * Retorna los beneficiarios para un participante de negocio
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 19/02/2019 Danilo Dominguez (Everis) - Minheli Mejias (Ing. Soft.
	 * BCI) : Versi�n inicial.</li>
	 * </ul>
	 * <p>
	 * 
	 * @param  parIn bean beneficiarios
	 * @return BeneficiarioRippleTO[]�monedas permitidas.
	 * @throws ServicioDatosException
	 *             En caso de ocurrir un error en el servicio de datos.
	 * @throws TtffException
	 *             En caso de ocurrir un error de negocio.
	 * 
	 * @since 1.0
	 */
	public BeneficiarioRippleTO[] obtenerBeneficiariosAutorizadosRipple(ConsultaBeneficiariosRippleTO parIn)
			throws ServicioDatosException, TtffException {

		String nombreMetodo = "obtenerBeneficiariosAutorizadosRipple";

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "] [BCI_INI]");
			getLogger().info("[" + nombreMetodo + "] [Parametro][parIn][" + StringUtil.contenidoDe(parIn) + "]");
		}

		List documento = null;
		HashMap salida = null;
		BeneficiarioRippleTO[] objeto = null;

		HashMap mapa = new HashMap();
		mapa.put("CHAINED_SUPPORT", "true");

		mapa.put("fld_cln_rut_emp", parIn.getRutEmpresa());
		mapa.put("fld_ben_mda_cta", parIn.getMndaCuenta());
		mapa.put("fld_rpp_bic_bco", parIn.getBicBanco());

		String contexto = TablaValores.getValor("JNDIConfig.parametros", "cluster", "param");

		if (getLogger().isDebugEnabled()) {
			getLogger().debug("obtenerBeneficiariosAutorizados");
			getLogger().debug("[ConectorServicioDePersistenciaDeDatos] - getConnection");
		}

		ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
		documento = conector.consultar(SQL_MAP_BD_TTFF_RPP, "obtenerBeneficiariosAutorizadosRipple", mapa);
		if (documento != null) {
			if (getLogger().isDebugEnabled()) {
				getLogger().info("obtenerBeneficiariosAutorizados::Cantidad de registros retornados: " + documento.size());
			}
			if (!documento.isEmpty()) {

				objeto = new BeneficiarioRippleTO[documento.size()];
				for (int c = 0; c < documento.size(); c++) {
					BeneficiarioRippleTO paso = new BeneficiarioRippleTO();
					salida = (HashMap) documento.get(c);

					Integer resultSw = (Integer) salida.get("indicadorResultado");
					String resultMsg = String.valueOf(salida.get("mensajeResultado"));
					procesaError(resultSw, resultMsg, "obtenerBeneficiariosAutorizadosRipple");
					paso.setIdentificacion(String.valueOf(salida.get("identificacion")));
					paso.setNombre(String.valueOf(salida.get("nombre")));
					paso.setCtaCorriente(String.valueOf(salida.get("ctaCorriente")));
					paso.setBicBanco(String.valueOf(salida.get("bicBanco")));
					paso.setPaisBanco(String.valueOf(salida.get("paisBanco")));

					objeto[c] = paso;

				}
			}

		}

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "] [BCI_FINOK][" + StringUtil.contenidoDe(objeto) + "]");
		}
		return objeto;
	}

	/**
	 * Retorna el detalle del beneficiario
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 19/02/2019 Danilo Dominguez (Everis) - Minheli Mejias (Ing. Soft.
	 * BCI) : Versi�n inicial.</li>
	 * </ul>
	 * <p>
	 * 
	 * @param  datos bean con del detalle de los datos del beneficiario
	 * @return DetalleBeneficiarioRippleTO[]�monedas permitidas.
	 * @throws ServicioDatosException
	 *             En caso de ocurrir un error en el servicio de datos.
	 * @throws TtffException
	 *             En caso de ocurrir un error de negocio.
	 * 
	 * @since 1.0
	 */
	public DetalleBeneficiarioRippleTO obtenerDetalleBeneficiarioRipple(ConsultaDetalleBeneficiarioRippleTO datos)
			throws ServicioDatosException, TtffException {

		String nombreMetodo = "obtenerMonedasPermitidas";

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "] [BCI_INI]");
			getLogger().info("[" + nombreMetodo + "] [Parametro][datos][" + StringUtil.contenidoDe(datos) + "]");
		}

		List documento = null;
		HashMap salida = null;
		DetalleBeneficiarioRippleTO objeto = null;

		HashMap mapa = new HashMap();
		mapa.put("CHAINED_SUPPORT", "true");

		mapa.put("fld_cln_rut_emp", "00" + datos.getRutEmpresa() + "-" + datos.getDvEmpresa());
		mapa.put("fld_ben_ide_ben", datos.getIdentificadorBeneficiario());
		mapa.put("fld_ben_mda_cta", datos.getCodIso());
		mapa.put("fld_ben_ctr_ben", datos.getPaisBanco());

		String contexto = TablaValores.getValor("JNDIConfig.parametros", "cluster", "param");

		if (getLogger().isDebugEnabled()) {
			getLogger().debug("obtenerDetalleBeneficiarioRipple");
			getLogger().debug("[ConectorServicioDePersistenciaDeDatos] - getConnection");
		}

		ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
		documento = conector.consultar(SQL_MAP_BD_TTFF_RPP, "obtenerDetalleBeneficiarioRipple", mapa);
		if (documento != null) {
			if (getLogger().isDebugEnabled()) {
				getLogger().info("obtenerBeneficiariosAutorizados::Cantidad de registros retornados: " + documento.size());
			}
			if (!documento.isEmpty()) {
				objeto = new DetalleBeneficiarioRippleTO();
				for (int c = 0; c < documento.size(); c++) {
					salida = (HashMap) documento.get(c);
					Integer resultSw = (Integer) salida.get("indicadorResultado");
					String resultMsg = String.valueOf(salida.get("mensajeResultado"));
					procesaError(resultSw, resultMsg, "obtenerDetalleBeneficiarioRipple");
					objeto.setNombre(String.valueOf(salida.get("nombre")));
					objeto.setNombreCont(String.valueOf(salida.get("nombreCont")));
					objeto.setDireccion(String.valueOf(salida.get("direccion")));
					objeto.setDireccionCont(String.valueOf(salida.get("direccionCont")));
					objeto.setPaisResidencia(String.valueOf(salida.get("paisResidencia")));
					objeto.setNombrePaisRes(String.valueOf(salida.get("nombrePaisRes")));
					objeto.setInfoAdicional(String.valueOf(salida.get("infoAdicional")));
					objeto.setInfoAdicionalCont(String.valueOf(salida.get("infoAdicionalCont")));
					objeto.setCtaCorriente(String.valueOf(salida.get("ctaCorriente")));
					objeto.setBanco(String.valueOf(salida.get("banco")));
					objeto.setNombreBanco(String.valueOf(salida.get("nombreBanco")));
					objeto.setDireccionBanco(String.valueOf(salida.get("direccionBanco")));
					objeto.setEmail(String.valueOf(salida.get("email")));
					objeto.setNombrePaisBco(String.valueOf(salida.get("nombrePaisBco")));
					objeto.setCtaCorrienteBenef(String.valueOf(salida.get("ctaCorrienteBenef")));
					objeto.setNombreLineaUno(String.valueOf(salida.get("nombreLineaUno")));
					objeto.setNombreLineaDos(String.valueOf(salida.get("nombreLineaDos")));
					objeto.setNombreLineaTres(String.valueOf(salida.get("nombreLineaTres")));
					objeto.setNombreLineaCuatro(String.valueOf(salida.get("nombreLineaCuatro")));
				}
			}
		}

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "] [BCI_FINOK][" + StringUtil.contenidoDe(objeto) + "]");
		}
		return objeto;
	}

	/**
	 * Invoca al procedimiento almacenado FBPsrv_shw_cyg_rpp.
	 * <p>
	 *
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 05/03/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft.
	 * BCI): Versi�n inicial.</li>
	 * </ul>
	 * <p>
	 *
	 * @param obtenerComision
	 *            Entrada para el SP FBPsrv_shw_cyg_rpp.
	 * @return ComisionTO Salida del SP FBPsrv_shw_cyg_rpp.
	 * @throws ServicioDatosException
	 *             En caso de ocurrir un error en el servicio de datos.
	 * @throws TtffException
	 *             En caso de ocurrir un error de negocio.
	 * 
	 * @since 1.0
	 */
	public ComisionTO obtenerComisiones(ObtenerComisionTO obtenerComision)
			throws ServicioDatosException, TtffException {

		String nombreMetodo = "obtenerMonedasPermitidas";

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "] [BCI_INI]");
			getLogger().info("[" + nombreMetodo + "] [Parametro][obtenerComision][" + StringUtil.contenidoDe(obtenerComision)
					+ "]");
		}

		List documento = null;
		HashMap salida = null;
		ComisionTO objeto = null;

		HashMap mapa = new HashMap();
		mapa.put("CHAINED_SUPPORT", "true");

		mapa.put("fld_cln_rut_emp", obtenerComision.getRutEmpresa());
		mapa.put("fld_hdr_mda_itf", obtenerComision.getMonedaTransf());
		
//		String monto = obtenerComision.getMontoPago().replaceAll(".","").replaceAll(",",".");
		mapa.put("fld_det_mto_pag", new BigDecimal(obtenerComision.getMontoPago()));
		mapa.put("fld_rpp_bic_bco", obtenerComision.getBicBanco());

		String contexto = TablaValores.getValor("JNDIConfig.parametros", "cluster", "param");

		if (getLogger().isDebugEnabled()) {
			getLogger().debug("obtenerComisiones");
			getLogger().debug("[ConectorServicioDePersistenciaDeDatos] - getConnection");
		}

		ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
		documento = conector.consultar(SQL_MAP_BD_TTFF_RPP, "obtenerComisiones", mapa);
		if (documento != null) {
			if (getLogger().isDebugEnabled()) {
				getLogger().debug("obtenerComisiones::Cantidad de registros retornados: " + documento.size());
			}
			if (!documento.isEmpty()) {
				objeto = new ComisionTO();
				salida = (HashMap) documento.get(0);
				Integer resultSw = (Integer) salida.get("indicadorResultado");
				String resultMsg = String.valueOf(salida.get("mensajeResultado"));
				procesaError(resultSw, resultMsg, "obtenerComisiones");
				objeto.setCodigoIso(String.valueOf(salida.get("codigoIso")));
				objeto.setCtaDebitoComisiones(String.valueOf(salida.get("ctaDebitoComisiones")));
				objeto.setComisionEnvio((BigDecimal) (salida.get("comisionEnvio")));
				objeto.setTipoComision(Integer.parseInt(salida.get("tipoComision").toString()));
				objeto.setDescTipoComision(String.valueOf(salida.get("descTipoComision")));
				objeto.setTipoTarifa(String.valueOf(salida.get("tipoTarifa")));
				objeto.setKey(String.valueOf(salida.get("key")));
			}
		}
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "] [BCI_FINOK][" + StringUtil.contenidoDe(objeto) + "]");
		}
		return objeto;
	}

	/**
	 * Invoca al procedimiento almacenado FBPsrv_reg_itf_rpp.
	 * <p>
	 *
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 07/03/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft.BCI): Versi�n inicial.</li>
	 * </ul>
	 * <p>
	 *
	 * @param regTransferencia
	 *            Entrada para el SP FBPsrv_reg_itf_rpp.
	 * @return RespuestaRegistroTransferenciaTO Salida del SP FBPsrv_reg_itf_rpp.
	 * @throws ServicioDatosException
	 *             En caso de ocurrir un error en el servicio de datos.
	 * @throws TtffException
	 *             En caso de ocurrir un error de negocio.
	 * 
	 * @since 1.0
	 */
	public RespuestaRegistroTransferenciaTO registrarTransferencia(RegistrarTransferenciaTO regTransferencia)
			throws ServicioDatosException, TtffException {

		String nombreMetodo = "obtenerMonedasPermitidas";

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "] [BCI_INI]");
			getLogger().info("[" + nombreMetodo + "] [Parametro][regTransferencia]["
					+ StringUtil.contenidoDe(regTransferencia) + "]");
		}

		List documento = null;
		HashMap salida = null;
		RespuestaRegistroTransferenciaTO objeto = null;

		HashMap mapa = new HashMap();
		mapa.put("CHAINED_SUPPORT", "true");

		mapa.put("fld_hdr_rut_ord", regTransferencia.getRutEmpresa());
		mapa.put("fld_hdr_nom_ord", regTransferencia.getNombreEmpresa());
		mapa.put("fld_hdr_mda_itf", regTransferencia.getMoneda());
		mapa.put("fld_hdr_cta_orf", regTransferencia.getCuentaDebito());
		mapa.put("fld_det_mto_pag", regTransferencia.getMontoPago());
		mapa.put("fld_det_inf_rem", regTransferencia.getRemesa());
		mapa.put("fld_cyg_mda_cta", regTransferencia.getMonedaComision());
		mapa.put("fld_cyg_cta_cte", regTransferencia.getCuentaComision());
		mapa.put("fld_det_cod_ben", regTransferencia.getCodIdentifBenef());
		mapa.put("fld_det_ctr_ben", regTransferencia.getPaisBcoBenef());
		mapa.put("fld_det_cod_cmb", regTransferencia.getCodigoCambio());
		mapa.put("fld_det_hrp_itf", regTransferencia.getHashOperacion());
		mapa.put("fld_cyg_rat_key", regTransferencia.getKeyOperacion());
		mapa.put("fld_det_usr_eve", regTransferencia.getRutUsuario());
		mapa.put("fld_det_nip_usr", regTransferencia.getIpMaquina());

		String contexto = TablaValores.getValor("JNDIConfig.parametros", "cluster", "param");

		if (logger.isDebugEnabled()) {
			logger.debug("registrarTransferencia");
			logger.debug("[ConectorServicioDePersistenciaDeDatos] - getConnection");
		}

		ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
		documento = conector.consultar(SQL_MAP_BD_TTFF_RPP, "registrarTransferencia", mapa);
		if (documento != null) {
			if (logger.isDebugEnabled()) {
				logger.debug("registrarTransferencia::Cantidad de registros retornados: " 
						+ documento.size());
			}		
			if (!documento.isEmpty()) {
				objeto = new RespuestaRegistroTransferenciaTO();
				salida = (HashMap) documento.get(0);
				Integer resultSw = (Integer) salida.get("indicadorResultado");
				String resultMsg = String.valueOf(salida.get("mensajeResultado"));
				procesaError(resultSw, resultMsg, "registrarTransferencia");
				objeto.setIndicadorResultado(resultSw.intValue());
				objeto.setMensajeResultado(resultMsg);
				objeto.setNumeroCorrelativo(Integer.parseInt(salida.get("numeroCorrelativo").toString()));
				objeto.setSystimestamp(String.valueOf(salida.get("systimestamp")));
			}
		}
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "] [BCI_FINOK][" + StringUtil.contenidoDe(objeto) + "]");
		}

		return objeto;
	}

	/**
	 * Procesamiento de errores
	 * <p>
	 *
	 * Registro de versiones:
	 * <ul>
	 *
	 * <li>1.0 07/03/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft.BCI): Versi�n inicial.</li>
	 * </ul>
	 * <p>
	 *
	 * @param resultSw
	 *            obtenido desde la capa de datos.
	 * @param resultMsg
	 *            obtenido desde la capa de datos.
	 * @param metodo
	 *            nombre del metodo.
	 * @throws TtffException
	 *             En caso de ocurrir un error de negocio.
	 * 
	 * @since 1.0
	 */
	private void procesaError(Integer resultSw, String resultMsg, String metodo) throws TtffException {

		String nombreMetodo = "obtenerMonedasPermitidas";

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "] [BCI_INI]");
			getLogger().info("[" + nombreMetodo + "] [Parametro][resultSw][" + resultSw + "] [Parametro][resultMsg]["
					+ resultMsg + "] [Parametro][resultMsg][" + metodo + "]");
		}

		final int errorOK = 0;
		final int errorNegocioDB = 1;
		final int errorNoHayDatosDB = 2;

		final String errorNegocio = "TTFF-023";
		final String errorNoHayDatos = "TTFF-024";
		final String errorSistema = "TTFF-025";
		final String errorIndicadorResultado = "TTFF-026";
		String errorCode = errorSistema;

		if (resultSw == null) {
			getLogger().error(metodo + "::'indicadorResultado' es un valor nulo");
			throw (new TtffException(errorIndicadorResultado));
		}
		if (resultSw.intValue() == errorOK)
			return;
		// Manejo de errores
		if (resultSw.intValue() == errorNegocioDB) {
			errorCode = errorNegocio;
		} 
		if (resultSw.intValue() == errorNoHayDatosDB) {
			errorCode = errorNoHayDatos;
		}
		throw (new TtffException(errorCode, resultMsg));

	}
	
	/**
	* Invoca al procedimiento almacenado FBPsrv_jct_trp_lim.
	* <p>
	*
	* Registro de versiones:
	* <ul>
	* <li>1.0 19/03/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft. BCI): Versi�n inicial.</li>
	</ul>
	* <p>
	*
	* @return RespuestaHorarioTO Salida del SP FBPsrv_jct_trp_lim.
	* @throws ServicioDatosException En caso de ocurrir un error en el servicio de datos.
	* @throws TtffException En caso de ocurrir un error de negocio.
	* 
	* @since 1.0
	*/
	public RespuestaHorarioTO consultarLimiteHorarioIngreso() throws ServicioDatosException, TtffException {
		final String nombreMetodo = "consultarLimiteHorarioIngreso";
		
		if (logger.isEnabledFor(Level.INFO)) {
			logger.info("[" + nombreMetodo + "] [BCI_INI]");
		}
		
		List documento = null;
		HashMap salida = null;	
		RespuestaHorarioTO objeto = null;
	
		HashMap parametros = new HashMap();
		parametros.put("CHAINED_SUPPORT", "true");
		String contexto = TablaValores.getValor("JNDIConfig.parametros", "cluster", "param");
		
		ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);		
		documento = conector.consultar(SQL_MAP_BD_TTFF_RPP, "consultarLimiteHorarioIngreso", parametros);
		
		if (logger.isEnabledFor(Level.DEBUG)) {
			logger.debug("[" + nombreMetodo + "] [documento.size] [" + (documento != null ? documento.size() : 0) + "]");
			logger.debug("[" + nombreMetodo + "] [documento] [" + StringUtil.contenidoDe(documento) + "]");
		}
		
		if (documento != null && documento.size() > 0) {
			objeto = new RespuestaHorarioTO();
			salida = (HashMap) documento.get(0);
			Integer resultSw = (Integer) salida.get("indicadorResultado");			
			String resultMsg = String.valueOf(salida.get("mensajeResultado"));
			procesaError(resultSw, resultMsg, "consultarLimiteHorarioIngreso");
			objeto.setIndicadorResultado(resultSw.intValue());
			objeto.setMensajeResultado(resultMsg);
		}
		
		if (logger.isEnabledFor(Level.INFO)) {
			logger.info("[" + nombreMetodo + "] [BCI_FINOK][" + StringUtil.contenidoDe(objeto) + "]");
		}
		
		return objeto;
	}
	
	/**
	* Invoca al procedimiento almacenado FBPsrv_apr_itf_rpp.
	* <p>
	*
	* Registro de versiones:
	* <ul>
	* <li>1.0 20/03/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft. BCI): Versi�n inicial.</li>
	</ul>
	* <p>
	* @param transferencia Entrada para el SP FBPsrv_apr_itf_rpp.
	* @return RespuestaSpTO Salida del SP FBPsrv_apr_itf_rpp.
	* @throws ServicioDatosException En caso de ocurrir un error en el servicio de datos.
	* @throws TtffException En caso de ocurrir un error de negocio.
	* 
	* @since 1.0
	*/
	public RespuestaSpTO firmarOEliminarTransferenciaRipple(FirmarEliminarTransferenciaRipple transferencia)
			throws ServicioDatosException, TtffException{
		
		String nombreMetodo = "firmarOEliminarTransferenciaRipple";

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "] [BCI_INI]");
			getLogger().info("[" + nombreMetodo + "] [Parametro][transferencia][" + transferencia + "]");
		}
		List documento = null;
		HashMap salida = null;	
		RespuestaSpTO objeto = null;
	
		HashMap mapa = new HashMap();
		mapa.put("CHAINED_SUPPORT", "true");

		mapa.put("fld_rpp_tip_acc", transferencia.getAccion());
		mapa.put("fld_hdr_sys_num", new Integer(transferencia.getCorrelativoFbp()));
		mapa.put("fld_det_SysTms",  transferencia.getSysTimeStamp());
		mapa.put("fld_det_hrp_itf", transferencia.getHash());
		mapa.put("fld_fbp_usr_eve", transferencia.getUsuario());
		mapa.put("fld_fbp_nip_usr", transferencia.getIpTerminal());
		mapa.put("fld_fbp_txt_eve", transferencia.getComentarios());
		
		String contexto = TablaValores.getValor("JNDIConfig.parametros", "cluster", "param");

		ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
		documento = conector.consultar(SQL_MAP_BD_TTFF_RPP, "firmarOEliminarTransferenciaRipple", mapa);
		
		if (logger.isEnabledFor(Level.DEBUG)) {
			logger.debug("[" + nombreMetodo + "] [documento.size] [" + (documento != null ? documento.size() : 0) + "]");
			logger.debug("[" + nombreMetodo + "] [documento] [" + StringUtil.contenidoDe(documento) + "]");
		}
		
		if (documento != null) {
			if (logger.isDebugEnabled()) {
				logger.debug("firmarOEliminarTransferenciaRipple::Cantidad de registros retornados: " 
						+ documento.size());
			}		
			if (!documento.isEmpty()) {
				objeto = new RespuestaSpTO();
				salida = (HashMap) documento.get(0);	
				Integer resultSw = (Integer) salida.get("indicadorResultado");
				String resultMsg = String.valueOf(salida.get("mensajeResultado"));
				procesaError(resultSw, resultMsg, "firmarOEliminarTransferenciaRipple");
				objeto.setIndicadorResultado(resultSw.intValue());
				objeto.setMensajeResultado(resultMsg);
			}
		}
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "] [BCI_FINOK][" + StringUtil.contenidoDe(objeto) + "]");
		}
		
		return objeto;
	}
	
	/**
	* Invoca al procedimiento almacenado FBPsrv_edb_cct_mmx.
	* <p>
	*
	* Registro de versiones:
	* <ul>
	* <li>1.0 20/03/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft. BCI): Versi�n inicial.</li>
	</ul>
	* <p>
	* @param correlativoFBP Entrada para el SP FBPsrv_edb_cct_mmx.
	* @return RespuestaSpTO Salida del SP FBPsrv_edb_cct_mmx.
	* @throws ServicioDatosException En caso de ocurrir un error en el servicio de datos.
	* @throws TtffException En caso de ocurrir un error de negocio.
	* 
	* @since 1.0
	*/
	public RespuestaSpTO debitarCuentaCorrienteMX(int correlativoFBP)
	throws ServicioDatosException, TtffException{
		String nombreMetodo = "debitarCuentaCorrienteMX";

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "] [BCI_INI]");
			getLogger().info("[" + nombreMetodo + "] [Parametro][correlativoFBP][" + correlativoFBP + "]");
		}
		
		List documento = null;
		HashMap salida = null;	
		RespuestaSpTO objeto = null;
	
		HashMap mapa = new HashMap();
		mapa.put("CHAINED_SUPPORT", "true");
		
		mapa.put("fld_hdr_sys_num", new Integer(correlativoFBP));
		
		String contexto = TablaValores.getValor("JNDIConfig.parametros", "cluster", "param");

		ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
		documento = conector.consultar(SQL_MAP_BD_TTFF_RPP, "debitarCuentaCorrienteMX", mapa);
		
		if (logger.isEnabledFor(Level.DEBUG)) {
			logger.debug("[" + nombreMetodo + "] [documento.size] [" + (documento != null ? documento.size() : 0) + "]");
			logger.debug("[" + nombreMetodo + "] [documento] [" + StringUtil.contenidoDe(documento) + "]");
		}
		
		if (documento != null) {
			if (logger.isDebugEnabled()) {
				logger.debug("debitarCuentaCorrienteMX::Cantidad de registros retornados: " 
						+ documento.size());
			}		
			if (!documento.isEmpty()) {
				objeto = new RespuestaSpTO();
				salida = (HashMap) documento.get(0);	
				Integer resultSw = (Integer) salida.get("indicadorResultado");
				String resultMsg = String.valueOf(salida.get("mensajeResultado"));
				procesaError(resultSw, resultMsg, "debitarCuentaCorrienteMX");
				objeto.setIndicadorResultado(resultSw.intValue());
				objeto.setMensajeResultado(resultMsg);
			}
		}
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "] [BCI_FINOK][" + StringUtil.contenidoDe(objeto) + "]");
		}
		return objeto;
	}
	
	
	/**
	* Invoca al procedimiento almacenado FBPsrv_sdt_snd_xcr.
	* <p>
	*
	* Registro de versiones:
	* <ul>
	* <li>1.0 22/03/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft. BCI): Versi�n inicial.</li>
	</ul>
	* <p>
	* @param correlativoFBP Entrada para el SP FBPsrv_sdt_snd_xcr.
	* @return DatosxCurrentTO Salida del SP FBPsrv_sdt_snd_xcr.
	* @throws TtffException En caso de ocurrir un error de negocio.
	* 
	* @since 1.0
	*/
	public DatosxCurrentTO consultarDatosxCurrent (int correlativoFBP)
	throws ServicioDatosException, TtffException{
		String nombreMetodo = "consultarDatosxCurrent";

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "] [BCI_INI]");
			getLogger().info("[" + nombreMetodo + "] [Parametro][correlativoFBP][" + correlativoFBP + "]");
		}
		
		List documento = null;
		HashMap salida = null;	
		DatosxCurrentTO objeto = null;
	
		HashMap mapa = new HashMap();
		mapa.put("CHAINED_SUPPORT", "true");
		
		mapa.put("fld_hdr_sys_num", new Integer(correlativoFBP));
		
		String contexto = TablaValores.getValor("JNDIConfig.parametros", "cluster", "param");

		ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
		documento = conector.consultar(SQL_MAP_BD_TTFF_RPP, "consultarDatosAXCurrent", mapa);
		
		if (logger.isEnabledFor(Level.DEBUG)) {
			logger.debug("[" + nombreMetodo + "] [documento.size] [" + (documento != null ? documento.size() : 0) + "]");
			logger.debug("[" + nombreMetodo + "] [documento] [" + StringUtil.contenidoDe(documento) + "]");
		}
		
		if (documento != null) {
			if (logger.isDebugEnabled()) {
				logger.debug("debitarCuentaCorrienteMX::Cantidad de registros retornados: " 
						+ documento.size());
			}		
			if (!documento.isEmpty()) {
				objeto = new DatosxCurrentTO();
				salida = (HashMap) documento.get(0);	
				Integer resultSw = (Integer) salida.get("indicadorResultado");
				String resultMsg = String.valueOf(salida.get("mensajeResultado"));
				procesaError(resultSw, resultMsg, "consultarDatosAXCurrent");
				objeto.setIndicadorResultado(resultSw.intValue());
				objeto.setMensajeResultado(resultMsg);
				objeto.setNumCtaOrdenante(String.valueOf(salida.get("numCtaOrdenante")));
				objeto.setNombreOrdenante(String.valueOf(salida.get("nombreOrdenante")));
				objeto.setDireccionOrdenante(String.valueOf(salida.get("direccionOrdenante")));
				objeto.setResidenciaOrdenante(String.valueOf(salida.get("direccionOrdenante")));
				objeto.setRutOrdenante(String.valueOf(salida.get("rutOrdenante")));
				objeto.setPaisOrdenante(String.valueOf(salida.get("paisOrdenante")));
				objeto.setTrn(String.valueOf(salida.get("trn")));
				objeto.setNumCtaBenef(String.valueOf(salida.get("numCtaBenef")));
				objeto.setNombreBenef(String.valueOf(salida.get("nombreBenef")));
				objeto.setDireccionBenefL1(String.valueOf(salida.get("direccionBenefL1")));
				objeto.setDireccionBenefL2(String.valueOf(salida.get("direccionBenefL2")));
				objeto.setPaisBenef(String.valueOf(salida.get("paisBenef")));
				objeto.setMonedaTransf(String.valueOf(salida.get("monedaTransf")));
				objeto.setMontoTransf((BigDecimal) (salida.get("montoTransf")));
				objeto.setMontoComision((BigDecimal) (salida.get("montoComision")));
				objeto.setRemesa(String.valueOf(salida.get("remesa")));
				objeto.setBancoBenef(String.valueOf(salida.get("bancoBenef")));
			}
		}
		
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "] [BCI_FINOK][" + StringUtil.contenidoDe(objeto) + "]");
		}
		return objeto;
		
	}
	
	/**
	* Invoca al procedimiento almacenado FBPsrv_rcd_rpo_xcr.
	* <p>
	*
	* Registro de versiones:
	* <ul>
	* <li>1.0 22/03/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft. BCI): Versi�n inicial.</li>
	</ul>
	* <p>
	* @param resultadoxCurrent Entrada para el SP FBPsrv_rcd_rpo_xcr.
	* @return RespuestaSpTO Salida del SP FBPsrv_rcd_rpo_xcr.
	* @throws TtffException En caso de ocurrir un error de negocio.
	* 
	* @since 1.0
	*/
	public RespuestaSpTO registrarResulatadoxCurrent (String correlativoFBP, String resultado, String paymentId)
	throws ServicioDatosException, TtffException{
		
		String nombreMetodo = "registrarResulatadoxCurrent";

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "] [BCI_INI]");
			getLogger().info("[" + nombreMetodo + "] [Parametro][correlativoFBP][" + correlativoFBP + "], [resultado][" + resultado + "], [paymentId][" + paymentId + "] ");
		}
		
		List documento = null;
		HashMap salida = null;	
		RespuestaSpTO objeto = null;
	
		HashMap mapa = new HashMap();
		mapa.put("CHAINED_SUPPORT", "true");
		
		mapa.put("fld_hdr_sys_num", correlativoFBP);
		mapa.put("fld_rpp_rst_pro", resultado);
		mapa.put("fld_rpp_iuu_cod", paymentId);
		mapa.put("fld_fbp_txt_eve", "");
		
		String contexto = TablaValores.getValor("JNDIConfig.parametros", "cluster", "param");

		ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(contexto);
		documento = conector.consultar(SQL_MAP_BD_TTFF_RPP, "registrarResulatadoxCurrent", mapa);
		
		if (logger.isEnabledFor(Level.DEBUG)) {
			logger.debug("[" + nombreMetodo + "] [documento.size] [" + (documento != null ? documento.size() : 0) + "]");
			logger.debug("[" + nombreMetodo + "] [documento] [" + StringUtil.contenidoDe(documento) + "]");
		}
		
		if (documento != null) {
			if (logger.isDebugEnabled()) {
				logger.debug("debitarCuentaCorrienteMX::Cantidad de registros retornados: " 
						+ documento.size());
			}		
			if (!documento.isEmpty()) {
				objeto = new RespuestaSpTO();
				salida = (HashMap) documento.get(0);	
				Integer resultSw = (Integer) salida.get("indicadorResultado");
				String resultMsg = String.valueOf(salida.get("mensajeResultado"));
				procesaError(resultSw, resultMsg, "registrarResulatadoxCurrent");
				objeto.setIndicadorResultado(resultSw.intValue());
				objeto.setMensajeResultado(resultMsg);
			}
		}
		
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[" + nombreMetodo + "] [BCI_FINOK][" + StringUtil.contenidoDe(objeto) + "]");
		}
		return objeto;
	}
	
	/**
	 * <p>
	 * M�todo get del logger.
	 * </p>
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 07/03/2019, Danilo Dom�nguez (Everis) - Minheli Mejias (Ing. Soft.BCI): Versi�n inicial.</li>
	 * </ul>
	 * </p>
	 * 
	 * @return Logger
	 * @since 1.0
	 */
	private Logger getLogger() {
		if (logger == null) {
			logger = Logger.getLogger(HashUtil.class);
		}
		return logger;
	}
}
